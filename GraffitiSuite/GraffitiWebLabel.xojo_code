#tag Class
Protected Class GraffitiWebLabel
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "mousein"
		      MouseEnter()
		    case "mouseout"
		      MouseExit()
		    case "resize"
		      if Parameters.Ubound >= 0 then
		        dim currHeight as Integer = Parameters(0).IntegerValue
		        if currHeight = 0 and mCaption.Len > 0 then Return True
		        mTrueHeight = currHeight
		        Resized()
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "style"
		    dim strExec() as String
		    dim sVal as WebStyle = value
		    
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
		    if not IsNull( value ) then
		      if not IsNull( oldStyle ) then strExec.Append( "theButton.removeClass('" + oldStyle.Name + "');" )
		      
		      if not sVal.isdefaultstyle and sVal.Name <> "_Style" and sVal.Name <> "_DefaultStyle" then
		        strExec.Append( "theButton.addClass('" + sval.Name + "');" )
		      end if
		    end if
		    
		    oldStyle = value
		    
		    Buffer( strExec )
		    
		    Return True
		  case "height"
		    dim strExec() as string
		    strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		    strExec.Append( "theElement.style.maxHeight='" + Str( mMaxHeight ) + "px';" )
		    strExec.Append( "theElement.style.minHeight='" + Str( Value.IntegerValue ) + "px';" )
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  dim strExec() as String
		  
		  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		  strExec.Append( "try {" )
		  strExec.Append( "var newHeight = theElement.height();" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'resize', [newHeight]);" )
		  strExec.Append( "} catch(e) {}" )
		  strExec.Append( "}" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  
		  source.Append( "<div id='" + me.ControlID + "' style='" + _
		  "display:none;" + _
		  "left:" + Str( me.Left ) + "px;" + _
		  "top:" + Str( me.Top ) + "px;" + _
		  "width:" + Str( me.Width ) + "px;" + _
		  "height:auto;" + _
		  "overflow:auto;" + _
		  "min-height:" + Str( me.Height ) + "px !important;'>" + ParseForIcon() + "</div>" )
		  
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebLabel -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebLabel -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ParseForIcon() As String
		  '// Regular Expression for future implementation:
		  '// <(fa-)[\w]*>
		  
		  dim strCapt() as String
		  
		  dim intStart as Integer = 0
		  dim intEnd as Integer = 0
		  
		  intStart = mCaption.InStr( "<fa" )
		  
		  if intStart > 1 then strCapt.Append( EscapeString( mCaption.Left( intStart - 1 ) ) )
		  
		  while intStart > 0
		    intEnd = mCaption.InStr( intStart, ">" )
		    dim strTag as String = Caption.Mid( intStart + 1, (intEnd - intStart) - 1 )
		    strCapt.Append( "<i class='" + EscapeString( strTag ) + "'></i>" )
		    
		    intStart = mCaption.InStr( intEnd, "<fa" )
		    if intStart > intEnd then
		      strCapt.Append( mCaption.Mid( intEnd + 1, (intStart - intEnd) - 1 ) )
		    end if
		  wend
		  
		  strCapt.Append( mCaption.Mid( intEnd + 1, mCaption.Len ) )
		  
		  Return Join( strCapt, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  dim strExec() as String
		  
		  strExec.Append( "var theLabel = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "theLabel.on('resize', function() {" )
		  strExec.Append( "var newHeight = window.GSjQuery('#" + me.ControlID + "').height();" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'resize', [newHeight]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "theLabel.on('mouseenter', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mousein');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "theLabel.on('mouseleave', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseout');" )
		  strExec.Append( "});" )
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  
		  RunBuffer()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Resized()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaption = ReplaceLineEndings( value, "" )
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + "');" )
			  strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
			  strExec.Append( "theElement.empty().html(" + chr(34) + EscapeString( ParseForIcon() ) + chr(34) + ");" )
			  strExec.Append( "try {" )
			  strExec.Append( "var newHeight = theElement.height();" )
			  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'resize', [newHeight]);" )
			  strExec.Append( "} catch(e) {}" )
			  strExec.Append( "}" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Caption As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMaxHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMaxHeight = value
			  
			  dim strExec() as string
			  strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
			  strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
			  if mMaxHeight >= 0 then
			    strExec.Append( "theElement.style.maxHeight='" + Str( mMaxHeight ) + "px';" )
			  else
			    strExec.Append( "theElement.style.removeProperty('max-height');" )
			  end if
			  strExec.Append( "try {" )
			  strExec.Append( "var newHeight = theElement.height();" )
			  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'resize', [newHeight]);" )
			  strExec.Append( "} catch(e) {}" )
			  strExec.Append( "}" )
			  
			  Buffer( strExec )
			  
			End Set
		#tag EndSetter
		MaxHeight As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMaxHeight As Integer = 200
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTrueHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldTextStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTrueHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// mTrueHeight = value
			End Set
		#tag EndSetter
		TrueHeight As Integer
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.weblabel", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAgxSURBVHhe7d1/jBxlGQfw953dys3aopH6I/yBAYQaw6+EQMRKGlsTEGF3D2kUjIoKwRSh7h41BmPrqZFqc7Pbs0RTkRIDTbUtt3sWNagQioKh/ohWEqkYS4io0SJwvdkrtzuPz9w+e3F/Hbd7797N9v1+ks3M89yPzs58Z+Z9d49FAQAAAAAAAAAAAAAAAAAAAAAAwMlCy3JRnJbctGIifuJtUkKDZdPqlckfjf5LykXR8wAMpDdeplXsM1rR+/mfO13a0A6pCml6nJfFUjGXl27P9DQAiXTW40WmWkEXnqVy+YbSgdFDUhvXswC4qcz9WuuPSgndIlUih9aWxnK/lo5RjiyNctOZbTj4hmjlKtL3qTWfGJCOUTFZGrM8OfQu3ugHpAQD+DK9cpmbKE//+cnHpGWM8StAxaGPyyoYRES3yqpRxgNQHe2DaXxLfXNiMHOllMYYHwTy/f/fWumVUjagH8S03h04wV9VWV8YKDXKT+w0+WITIvWw4+hvSNmliiw0VVfCcZUe5uhfLuWMGOkkb8+ElN2joLpP40oFlWCN1rEtM3UbvFG7HK3D2RI/3+AyPiQ7Z77QCtFNfjH3PamMMB4AnvrN7uhGfEAHS0WvIKVafnV2ZRCjUY739dJqqeyUz331wdG/SLkgvH3P8eKMalVPB7R2cjz3qJQL4qayv9BarZWyCe+LY46i2yeLud3SmpFIZY7w/jhHynpEX+AALPCEqNeTWcB8HT/g/Yef0A18GnySn50v7SbxIH6Ed+iwlF1JpDZeJOFsefBD5OhH3MGhBb1uwQfwqvDfmevg8/PdE9evW9V48JfCkgagxi9691EQD1O/v9ppxjt0c3j2Dlw79HZpzZubymxQOvZ7KeekiTy+jX1fyo7w9u3jDX1IyhZmQv5pfr7XTxS2Hqv2llYkAhAqjW97wS941/EhuIXLdreRM5yAjrrJ7LxHxHxQvsPjjLulnBcew3zMTWf/qK7ekpDWnF6fzLyPv/9lXv1QtdMCqWIYcn6O90onEiITgBq/MLKzQpWzeNpzQFpNtKN28IE9mFh/x5xvLPGZ/HNehIFqRmqah2lXcNJ+LJ06PDg6PxGfmORbwiXSaimR/tzd4a2Dv/9UaTXgZ6LVZ/msT4chl2ZkRC4AoRPF7UdLxdw1pOg2abVyuZoO/pEYzN4k9awV6ey5fC8+ymfyOmnV4QHYH2gZnekX8g+XCt4Hud4mX2rCt4SnEqnsjVLOcgcz7+YQ8sDU2SCtJuEspkLBWaUxr6Mr0GKKZABqSoXcjrJSq/gcekRazUh9lw9Q4dTrMm8KS3cwm+SJ3zN8L249VuABGM9ELirty/1dOorrz3OfB6JtaLWLB6HflEolkpnNmvSTvPqOaqdZQHQH/94rwjBLK5IiHYDQqwXvCF8N1vEcfUhazbRKlcv6GJ+Vu3m2X5RuM1KbwwGYVHVmBqJaX8q3hJbvx/MgdBPf5x/iK8svlaPnmJHQwVhQeedUMTcijUiLfABqpgqex4O58/kghmdeS3xWtn09gYJgPR/kr0rZUmls5FCpfJzHH6rlawF8n7+Kk7BayhboTr+QWzMxvv0ZaURe3wQgNDk28ic+iO/hHf0lac3HUR0LziuN5/dJPbcDO32+dK/loO2QzmsjOsRjhQv44N8lnb7RVwGo4R39NaWDi8PBnLRaCkf4/oUrzp7cn39aWvPGQbuNf77tAK+GxyfDfjF36WQxd1hafaUvAxDyx/K/CwdzPFP4urTqEXnhCF8ND/PwoTv8898OAr2GD3I4x6/D4TjMt5VLeHzyZWn1pb4NQA3PFL4YkFrNB/yItMKjczOfle0HjR2YGh85GNennM2/9AlphdO7uzgcF/Bt5TfS6lt9H4DQVNF7gg/4Kl7NB07wXr5831P9ihnhy7Z821nNV5t7wrDxledO+VLfOykCUOMXvMzUg/lfSWkcX21uDsMm5UnhpAoAdA4BsBwCYDkEwHIIgOUQAMst6R+FNnpDauMbZbUjLxe3vySrs7r9Xb3Walv/n1V/FFpn/Ybl0zr2324eiWSm6b187v+z8fui8Aj/lkA2MRJwC7AcAmA5BMByCIDlEADLIQCWi9TrAIn00MWy2hG/MPJbWZ3V7e/qNadMz4X/TaSUTRb7dYBIBQAWPwC4BVgOAbAcAmA5BMByCIDlojMLWL9huTs9sEeqjjhEuxs/bsVNZ9t+vsDSCn5WKuS3S9HE3mkgByAxPdDdp3QF9Cl/PLdLqhm8HVO8OKVaRUhAW3hbvyJVE0wDYVEhAJZDACyHAFgOAbBcpALAs4Snu3nIj9fh/rON3xeFh2xeZODdwIjBNBAWFQJgOQTAcgiA5RAAyyEAlsM0cB7mek4dw7uBECUIgOUQAMshAJZDACyHAFgOAZgPTR8w9ag45Uj9n9XxOkDE4HUAWFQIgOUQAMshAJZDACyHAFgOAbAcAmA5BMByCIDlEADLGQ8AKfWirDbRWl0pq9DCwLWbPtz2fYCQbr9vu2X8zSA3lTmstT5PSjDrGr/gGf3sI+NXAD7LH5VVMIlUyV/2/E+kMsZ4AAKl9ssqmKTVLrV3b0UqY4wHYKqQe0wR/VBKMIL8gNRWKYzqySzAL3of4cXj1QoWirS+caroPS+lUT0JAF+vyH/L8XU8I6j78EboDO+/l0gF6dKYt1daxhmfBTRyB7NJviXcrpVeJy14TfQ3Ir3HPTG99cWffusVafZEzwNQ4yZvPZ3i7pk6oLdKCxo4miaUE7wwuT8fuc8SAgAAAAAAAAAAAAAAAAAAAAAAAIAoU+p/XqAd/UvF7FgAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEYSURBVDhP1ZE9TkJBEMf/OxBD8AgECxu/OhqxktLCBCq8BeHpDezlvVOYaPWwMhyAvMJWYk88gREscMfZZWJ2o4SCwvhLNv/5TmYHf45RRaXT7xJMU91fMYzX92F6o66npIqtvZNHlnFS1JKxEwYOQ1tKGpLdLR0cvy1eiudlF0CqgpnO8/RMGq59MzAJbckdyfRCi39SbV+O1UTlvLejZmRXO8nAraqu5/sP3ABrbEagO4BTSSWaUlwMsODiI8/ufUgIVhAWtrCwF2z51mn4XEyrIqIBVKZ9YuqTMadewycxLYsoq3pmeTYScc/hz7XdTq7kOrVP8FTOXPOZgHiFTQivsIp1V5DfR33prYbZ9uYP2ZO6/x/gC2YDZ1WkJ0JeAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Caption"
			Visible=true
			Group="Behavior"
			InitialValue="Untitled"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxHeight"
			Visible=true
			Group="Behavior"
			InitialValue="200"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TrueHeight"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
