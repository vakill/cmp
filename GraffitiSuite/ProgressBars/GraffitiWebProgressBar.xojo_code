#tag Class
Protected Class GraffitiWebProgressBar
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  Select case Name
		  case "LibrariesLoaded"
		    LibrariesLoaded = True
		  End Select
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "visible"
		    DoVisible()
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' style='display:none;'></div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function ColorToString(c as Color) As String
		  dim r as string = Hex( c.Red )
		  dim g as String = Hex( c.Green )
		  dim b as String = Hex( c.Blue )
		  if r.Len = 1 then r = "0" + r
		  if g.Len = 1 then g = "0" + g
		  if b.Len = 1 then b = "0" + b
		  
		  Return "#" + r + g + b
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoVisible()
		  dim strScript as String
		  
		  if me.Visible = False Then
		    strScript = "window.GSjQuery('#" + me.ControlID + "').css('display', 'none');"
		  Else
		    strScript = "window.GSjQuery('#" + me.ControlID + "').css('display', 'block');"
		  end if
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebProgress -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "progressbar" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwprogress.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwprogress.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebProgress -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded and isShown and not Instantiated then
		    
		    Instantiated = True
		    
		    dim strExec() as String
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').gwProgressBar({" )
		    strExec.Append( "value: " + Str( mProgressValue, "#" ) + "," )
		    strExec.Append( "percentSign: '" + EscapeString( mPercentSign ) + "'," )
		    strExec.Append( "customText: '" + EscapeString( mProgressText ) + "'," )
		    strExec.Append( "progressColor: '" + ColorToString( mProgressColor ) + "'," )
		    strExec.Append( "backgroundColor: '" + ColorToString( mBackgroundColor ) + "'," )
		    strExec.Append( "animationTime: " + Str( mAnimationTime, "#" ) + "," )
		    strExec.Append( "showTip: " + Lowercase( Str( mShowTip ) ) + "," )
		    strExec.Append( "tooltipColor: '" + ColorToString( mTooltipColor ) + "'" )
		    strExec.Append( "});" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		GraffitiWebProgressBar
		Proptietary license available at
		http://www.graffitisuite.com/legal/source-code-eula/
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationTime
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationTime = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('animationTime'," + Str( mAnimationTime, "#" ) + ");"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		AnimationTime As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBackgroundColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBackgroundColor = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('backgroundColor','" + ColorToString( mBackgroundColor ) + "');"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		BackgroundColor As Color
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Instantiated As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return misShown
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if LibrariesLoaded and ControlAvailableInBrowser then
			    misShown = value
			    UpdateOptions()
			  end if
			End Set
		#tag EndSetter
		isShown As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAnimationTime As Integer = 1000
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBackgroundColor As Color = &c444444
	#tag EndProperty

	#tag Property, Flags = &h21
		Private misShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPercentSign As String = """%"""
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgressColor As Color = &c186de2
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgressText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgressValue As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowTip As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipColor As Color = &c4C4C4C
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipTextColor As Color
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPercentSign
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPercentSign = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('percentSign','" + EscapeString( mPercentSign ) + "');"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		PercentSign As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgressColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProgressColor = value
			  
			  dim strScript as string = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('progressColor','" + ColorToString( mProgressColor ) + "');"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		ProgressColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgressText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProgressText = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('customText','" + EscapeString( mProgressText ) + "');"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		ProgressText As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgressValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if value < 0 then value = 0
			  if value > 100 then value = 100
			  mProgressValue = value
			  
			  dim strScript as string = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('value'," + Str( mProgressValue, "#" ) + ");"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		ProgressValue As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowTip
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowTip = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('showTip'," + Lowercase( Str( mShowTip ) ) + ");"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		ShowTip As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipColor = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('tooltipColor','" + ColorToString( mTooltipColor ) + "');"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		TooltipColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipTextColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipTextColor = value
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').data('gwProgressBar').updateOption('tooltipTextColor','" + ColorToString( mTooltipTextColor ) + "');"
			  
			  if isShown and LibrariesLoaded and ControlAvailableInBrowser and Instantiated then
			    me.ExecuteJavaScript( strScript )
			  Else
			    Buffer( strScript )
			  end if
			End Set
		#tag EndSetter
		TooltipTextColor As Color
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.progressbar", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPjSURBVHhe7d2xixxlGMfx55mL5HIpFMHGQgsLUckFBQOijdgJiUfwQFCrgIWFmEvUImoT0ui5F0H8B1RQgnqbShslIBotNF4aG4vUimBxtxvvbh7f1UcQlhPfJ3s3L/t+P0X2eQZuIPv+duae2ZlEAAAAAAAAAAAAAAAAAADTQv01bHbxlTva7cF+b7GHGrnpj+Enb13zNiQcgIMLJ59pTd9WlVt9EzpgIr81rbywfrH3gW/KEgrAgYVTx1XsY29RADN7ctBfyV6Txl/zmL3uFUqh+qpXWUIBSJ/+Q16iEGo272WW2BFANfZz2D3BNWEhK0cAKheaAuYWltL0gR2ZtdLYg95NjunjacnOejdmY7WXvZ4EYDekAGz0V2a8m5jRtRcTfc/bMZEAcAqoHAGoHAGoHAGoHAGoHFPAbkhTQHqDPvdugvR2VTnszRjGwMoxBiIbAahcKABm8ouXKISJ/eplllAA0i8il71EIVT0Gy+zhALQ7Ns8kX4LvOItOpaOyD82w80T3mYJTQH/mHti6bW0h8Nmxl3BHUhH4qGYrm30ezt+QwgAAAAAwL+ErwPMHn3x4abRN9MuRk8JHfh7K/bYQMSutqKnh/3e174tSygAB4+dfNQa/cJbFEC1fWz90/PZaxK6FNyqLnuJQpjpG15miX0ZJHKPlyiF6b1eZQkFICWAc35pgmsSCwCmBgGoXGgKKO2mUDP5LP1FPvJ2aqU3/SFVec7bMdXeFWxiy4PVlZe8nVo8HIqJIwCVIwCVIwCVIwCVm4opoBX7Vk2+9HZqpRHwvvTnUW/H8HBo5RgDkY0AVC4UADP73UsUIromsSOA6vdeoRTBNQkFYGZLnucoUI7RWozWxNssoSlgZPb4qTu1bc+lXcynnXCDSAfSKDa6KXTNmubMjf7XMQAAAACAKoSvA+xfOH1XI+0ZFZtP8yjXATqQFm9gomutNOeury7/7JuzhAIwd2zpfmnsu/Tj+3wTOmVb0uqRjYu9H3zD/xb7LqCxd1j8kqS1+GtN8gW/DRzdmYKSpFPB3j0cqqo3e4lCpHP5LV5miZ0CMDUIQOViU0A1N4W2H6a3qKCbX+yB9Jl9ypsx3BU8YSr27Prqyvvedo6HQzFxBKByBKByBKByBKByTAH/7d1W5CuvO5c+rY+klx1v/2YMrBxjILIRgMpFAzD0V5QjtCaxAJiseYVSmF31KksoAK3ay16iEK1K6B/KnPHXLFs/Xb7W3H3kUqN6SExvS7NEaD+4QSabonZl29qnr/fPX/KtWUJj4JjFRQLQhQsXtr0CAAAAAAAAAAAAAAAAAAAAAAAAANRC5E+1nA1rmSIlgwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADESURBVDhP7ZHNDYJAEIVnuEniTwFSAQ1YgVe52AfSAMECwFKWqw1oA1qHxmS58WRwYiABxJMXv2Sy780mszOz9HNYT5psooQZU7WDAPwo8jQW/S7gBhFUdgM6EeOsemvzzBPp1IlRwCPwSgLMM002OwjXJdFC7SDVqzdrDkfRvSOAcGXwXW0LMPmFSeei+wuA9uDyorYFE8eFyfyXVtxgl6ochTVZJOcXS+zm4zfKLqpI1NY0RxjRAS8dOGEzJKeXf4joCcFjQ6+CbhCIAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AnimationTime"
			Visible=true
			Group="Behavior"
			InitialValue="1000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackgroundColor"
			Visible=true
			Group="Style"
			InitialValue="&c444444"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="isShown"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PercentSign"
			Visible=true
			Group="Behavior"
			InitialValue="%"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressColor"
			Visible=true
			Group="Style"
			InitialValue="&c186de2"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressText"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressValue"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowTip"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipColor"
			Visible=true
			Group="Style"
			InitialValue="&c4c4c4c"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipTextColor"
			Visible=true
			Group="Style"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
