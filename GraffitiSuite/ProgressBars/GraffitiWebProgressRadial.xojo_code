#tag Class
Protected Class GraffitiWebProgressRadial
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "style"
		    Dim st as WebStyle = WebStyle(value)
		    dim strOut() as String
		    
		    strOut.Append( "var thisControl = window.GSjQuery('#" + me.ControlID + "');" )
		    If not self.style.IsDefaultStyle then strOut.Append( "thisControl.removeClass('" + self.Style.Name + "');" )
		    if not st.isdefaultstyle then strOut.Append( "thisControl.addClass('" + st.Name + "');" )
		    
		    Buffer( strOut )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  UpdateOptions()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Return "<div id='" + me.ControlID + "' style='display:none;'></div>"
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  UpdateOptions()
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebRadProgress -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "progressbar" ).Child( "progressbar.min.js" ), "graffitisuite.radialprogress" ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebRadProgress -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ParseForIcon() As String
		  dim strCapt() as String
		  
		  dim intStart as Integer = 0
		  dim intEnd as Integer = 0
		  
		  intStart = mValueCaption.InStr( "<" )
		  
		  if intStart > 1 then strCapt.Append( EscapeString( mValueCaption.Left( intStart - 1 ) ) )
		  
		  while intStart > 0
		    intEnd = mValueCaption.InStr( intStart, ">" )
		    dim strTag as String = mValueCaption.Mid( intStart + 1, (intEnd - intStart) - 1 )
		    if strTag <> "br" and strTag <> "br /" then
		      strCapt.Append( "<i class='" + EscapeString( strTag ) + "'></i>" )
		    else
		      strCapt.Append( "<br />" )
		    end if
		    intStart = mValueCaption.InStr( intEnd, "<" )
		  wend
		  
		  strCapt.Append( mValueCaption.Mid( intEnd + 1, mValueCaption.Len ) )
		  
		  Return Join( strCapt, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if hasLoaded and isShown and not Instantiated then
		    
		    Instantiated = True
		    
		    dim strExec() as String
		    strExec.Append( "window.gCircle" + me.ControlID + " = new ProgressBar.Circle('#" + me.ControlID + "', {" )
		    strExec.Append( "duration: " + Str( mAnimationDuration ) + "," )
		    strExec.Append( "strokeWidth: " + Str( Max( mStrokeWidth, mProgressWidthEnd ), "0.00" ) + "," )
		    strExec.Append( "text: {" )
		    strExec.Append( "autoStyleContainer: false," )
		    strExec.Append( "value: ''" )
		    strExec.Append( "}," )
		    strExec.Append( "easing: 'easeInOut'," )
		    strExec.Append( "from: {" )
		    strExec.Append( "color: '" + ColorToHex( mProgressColor ) + "'," )
		    strExec.Append( "width: " + Str( mStrokeWidth, "0.00" ) )
		    strExec.Append( "}," )
		    strExec.Append( "to: {" )
		    strExec.Append( "color: '" + ColorToHex( mProgressColorEnd ) + "'," )
		    strExec.Append( "width: " + Str( mProgressWidthEnd, "0.00" ) )
		    strExec.Append( "}," )
		    strExec.Append( "step: function(state, circle) {" )
		    strExec.Append( "circle.path.setAttribute('stroke', state.color);" )
		    strExec.Append( "circle.path.setAttribute('stroke-width', state.width);" )
		    strExec.Append( "var value = Math.round(circle.value() * 100);" )
		    strExec.Append( "if (value == 0) {" )
		    if not CustomText then strExec.Append( "circle.setText('');" )
		    strExec.Append( "if (circle.text) {" )
		    strExec.Append( "circle.text.style.color = state.color;" )
		    strExec.Append( "}" )
		    strExec.Append( "} else {" )
		    if not CustomText then strExec.Append( "circle.setText(value);" )
		    strExec.Append( "if (circle.text) {" )
		    strExec.Append( "circle.text.style.color = state.color;" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "var theInstance = window.gCircle" + me.ControlID + ";" )
		    if CustomText then
		      strExec.Append( "theInstance.setText('" + EscapeString( ParseForIcon() ) + "');" )
		    end if
		    
		    strExec.Append( "var theText = theInstance.text;" )
		    strExec.Append( "if (theText) {" )
		    strExec.Append( "theText.style.fontSize = '" + EscapeString( FontSize ) + "';" )
		    strExec.Append( "}" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    me.Value = mValue
		    
		    RunBuffer()
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook


	#tag Note, Name = Source
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		http://kimmobrunfeldt.github.io/progressbar.js/
		
		Licensed under MIT license.
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationDuration
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationDuration = value
			End Set
		#tag EndSetter
		AnimationDuration As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCustomText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCustomText = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		CustomText As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mFontSize
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFontSize = value
			  
			  dim strExec() as String
			  strExec.Append( "var theText = theInstance.text;" )
			  strExec.Append( "if (theText) {" )
			  strExec.Append( "theText.style.fontSize = '" + EscapeString( FontSize ) + "';" )
			  strExec.Append( "}" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		FontSize As string
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Instantiated As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAnimationDuration As Integer = 250
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCustomText As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		#tag Note
			_
		#tag EndNote
		Private mFontSize As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgressColor As Color = &c0FA0CA
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgressColorEnd As Color = &c4286f4
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mProgressWidthEnd As Double = 2.6
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStrokeWidth As Double = 2.6
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValueCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgressColorEnd
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProgressColorEnd = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ProgressColorEnd As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgressColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProgressColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ProgressColorStart As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStrokeWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStrokeWidth = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ProgressWidth As Double
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mProgressWidthEnd
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mProgressWidthEnd = value
			End Set
		#tag EndSetter
		ProgressWidthEnd As Double
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  dim strExec() as String
			  strExec.Append( "var inst = window.gCircle" + me.ControlID + ";" )
			  strExec.Append( "if(inst) {" )
			  strExec.Append( "inst.stop();" )
			  strExec.Append( "inst.animate(" + Str( value / 100 ) + ", {duration: " + Str( mAnimationDuration ) + "}, function() {});" )
			  strExec.Append( "}" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Value As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValueCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValueCaption = value
			  
			  dim strExec() as String
			  strExec.Append( "var theInstance = window.gCircle" + me.ControlID + ";" )
			  strExec.Append( "if (theInstance) {" )
			  strExec.Append( "theInstance.setText('" + EscapeString( ParseForIcon() ) + "');" )
			  strExec.Append( "}" )
			End Set
		#tag EndSetter
		ValueCaption As String
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.radialprogress", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPjSURBVHhe7d2xixxlGMfx55mL5HIpFMHGQgsLUckFBQOijdgJiUfwQFCrgIWFmEvUImoT0ui5F0H8B1RQgnqbShslIBotNF4aG4vUimBxtxvvbh7f1UcQlhPfJ3s3L/t+P0X2eQZuIPv+duae2ZlEAAAAAAAAAAAAAAAAAADTQv01bHbxlTva7cF+b7GHGrnpj+Enb13zNiQcgIMLJ59pTd9WlVt9EzpgIr81rbywfrH3gW/KEgrAgYVTx1XsY29RADN7ctBfyV6Txl/zmL3uFUqh+qpXWUIBSJ/+Q16iEGo272WW2BFANfZz2D3BNWEhK0cAKheaAuYWltL0gR2ZtdLYg95NjunjacnOejdmY7WXvZ4EYDekAGz0V2a8m5jRtRcTfc/bMZEAcAqoHAGoHAGoHAGoHAGoHFPAbkhTQHqDPvdugvR2VTnszRjGwMoxBiIbAahcKABm8ouXKISJ/eplllAA0i8il71EIVT0Gy+zhALQ7Ns8kX4LvOItOpaOyD82w80T3mYJTQH/mHti6bW0h8Nmxl3BHUhH4qGYrm30ezt+QwgAAAAAwL+ErwPMHn3x4abRN9MuRk8JHfh7K/bYQMSutqKnh/3e174tSygAB4+dfNQa/cJbFEC1fWz90/PZaxK6FNyqLnuJQpjpG15miX0ZJHKPlyiF6b1eZQkFICWAc35pgmsSCwCmBgGoXGgKKO2mUDP5LP1FPvJ2aqU3/SFVec7bMdXeFWxiy4PVlZe8nVo8HIqJIwCVIwCVIwCVIwCVm4opoBX7Vk2+9HZqpRHwvvTnUW/H8HBo5RgDkY0AVC4UADP73UsUIromsSOA6vdeoRTBNQkFYGZLnucoUI7RWozWxNssoSlgZPb4qTu1bc+lXcynnXCDSAfSKDa6KXTNmubMjf7XMQAAAACAKoSvA+xfOH1XI+0ZFZtP8yjXATqQFm9gomutNOeury7/7JuzhAIwd2zpfmnsu/Tj+3wTOmVb0uqRjYu9H3zD/xb7LqCxd1j8kqS1+GtN8gW/DRzdmYKSpFPB3j0cqqo3e4lCpHP5LV5miZ0CMDUIQOViU0A1N4W2H6a3qKCbX+yB9Jl9ypsx3BU8YSr27Prqyvvedo6HQzFxBKByBKByBKByBKByTAH/7d1W5CuvO5c+rY+klx1v/2YMrBxjILIRgMpFAzD0V5QjtCaxAJiseYVSmF31KksoAK3ay16iEK1K6B/KnPHXLFs/Xb7W3H3kUqN6SExvS7NEaD+4QSabonZl29qnr/fPX/KtWUJj4JjFRQLQhQsXtr0CAAAAAAAAAAAAAAAAAAAAAAAAANRC5E+1nA1rmSIlgwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADESURBVDhP7ZHNDYJAEIVnuEniTwFSAQ1YgVe52AfSAMECwFKWqw1oA1qHxmS58WRwYiABxJMXv2Sy780mszOz9HNYT5psooQZU7WDAPwo8jQW/S7gBhFUdgM6EeOsemvzzBPp1IlRwCPwSgLMM002OwjXJdFC7SDVqzdrDkfRvSOAcGXwXW0LMPmFSeei+wuA9uDyorYFE8eFyfyXVtxgl6ochTVZJOcXS+zm4zfKLqpI1NY0RxjRAS8dOGEzJKeXf4joCcFjQ6+CbhCIAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AnimationDuration"
			Visible=true
			Group="Behavior"
			InitialValue="250"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomText"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FontSize"
			Visible=true
			Group="Behavior"
			InitialValue="2rem"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressColorEnd"
			Visible=true
			Group="Behavior"
			InitialValue="&c4286f4"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressColorStart"
			Visible=true
			Group="Behavior"
			InitialValue="&c0FA0CA"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressWidth"
			Visible=true
			Group="Behavior"
			InitialValue="2.6"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProgressWidthEnd"
			Visible=true
			Group="Behavior"
			InitialValue="2.6"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ValueCaption"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
