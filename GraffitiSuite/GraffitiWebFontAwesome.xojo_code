#tag Class
Protected Class GraffitiWebFontAwesome
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "click"
		      RaiseEvent Action()
		    case "mousein"
		      if isShown and LibrariesLoaded then RaiseEvent MouseEnter()
		    case "mouseout"
		      if isShown and LibrariesLoaded then RaiseEvent MouseLeave()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "Style"
		    dim strExec() as String
		    strExec.Append( "var faIcon = window.GSjQuery('#" + me.ControlID + "');" )
		    if not IsNull( oldStyle ) then strExec.Append( "faIcon.removeClass('" + oldStyle.Name + "');" )
		    if not IsNull( value ) then strExec.Append( "faIcon.addClass('" + WebStyle( value ).Name + "');" )
		    oldStyle = Value
		    
		    Buffer( Join( strExec, "" ) )
		    
		    Return True
		  case "height"
		    dim strExec() as String
		    strExec.Append( "var theIcon  = window.GSjQuery('#" + me.ControlID + " > i').css('line-height', '" + Str( Value.IntegerValue ) + "px');" )
		    Buffer( strExec )
		  case "visible"
		    Buffer( "GSjQuery('#" + me.ControlID + "').css('display', '" + If( me.Visible, "inline-block", "none" ) + "');" )
		  end select
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' style='display:none;text-align:center;'>" )
		  source.Append( "<i id='" + me.ControlID + "_icon' class='fa' style='vertical-align: middle;width:100%;height:100%;'></i>" )
		  source.append( "</div>" )
		  
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebFontAwesome -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebFontAwesome -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SizeToString(theSize as IconSizes) As String
		  select case theSize
		  case IconSizes.ExtraSmall
		    Return "fa-xs"
		  case IconSizes.Small
		    Return "fa-sm"
		  case IconSizes.Large
		    Return "fa-lg"
		  case IconSizes.TwoX
		    return "fa-2x"
		  case IconSizes.ThreeX
		    Return "fa-3x"
		  case IconSizes.FourX
		    Return "fa-4x"
		  case IconSizes.FiveX
		    Return "fa-5x"
		  case IconSizes.SixX
		    Return "fa-6x"
		  case IconSizes.SevenX
		    Return "fa-7x"
		  case IconSizes.EightX
		    Return "fa-8x"
		  case IconSizes.NineX
		    Return "fa-9x"
		  case IconSizes.TenX
		    Return "fa-10x"
		  end select
		  
		  return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TypeToString(theType as IconTypes) As String
		  select case theType
		  case IconTypes.Brands
		    Return "fab"
		  case IconTypes.Solid
		    Return "fas"
		  case else
		    Return "fa"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  dim strSize as String = SizeToString( mIconSize )
		  dim strExec() as String
		  strExec.Append( "var theControl = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "theControl.removeClass().addClass('" + me.Style.Name + "');" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('click',function () {" + _
		  "Xojo.triggerServerEvent('" + Me.ControlID + "','click',[]);" + _
		  "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('mouseenter', function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mousein', []);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('mouseleave', function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseout', []);" )
		  strExec.Append( "});" )
		  
		  'strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('mouseenter', function() {" )
		  'strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseenter', []);" )
		  'strExec.Append( "});" )
		  '
		  'strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('mouseleave', function() {" )
		  'strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseleave', []);" )
		  'strExec.Append( "});" )
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  RunBuffer()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateStyle()
		  UpdateOptions()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Action()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseLeave()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://fortawesome.github.io/Font-Awesome/
		
		Dual licensed under MIT and SIL OFL 1.1
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconName
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  strExec.Append( "var theIcon = window.GSjQuery('#" + me.ControlID + "_icon');" )
			  strExec.Append( "if (theIcon.length >= 1) {" )
			  if mIconName <> "" then strExec.Append( "theIcon.removeClass('" + mIconName + "');" )
			  
			  mIconName = value
			  
			  if mIconName <> "" then strExec.Append( "theIcon.addClass('" + mIconName + "');" )
			  strExec.Append( "}" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		IconName As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconSize
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  strExec.Append( "var theIcon = window.GSjQuery('#" + me.ControlID + "_icon');" )
			  strExec.Append( "if (theIcon.length >= 1) {" )
			  
			  if mIconSize <> IconSizes.Normal then strExec.Append( "theIcon.removeClass('" + SizeToString( mIconSize ) + "');" )
			  
			  mIconSize = value
			  
			  if mIconSize <> IconSizes.Normal then strExec.Append( "theIcon.addClass('" + SizeToString( mIconSize ) + "');" )
			  strExec.Append( "}" )
			  Buffer( strExec )
			End Set
		#tag EndSetter
		IconSize As IconSizes
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  strExec.Append( "var theIcon = window.GSjQuery('#" + me.ControlID + "_icon');" )
			  strExec.Append( "if (theIcon.length >= 1) {" )
			  
			  strExec.Append( "theIcon.removeClass('" + TypeToString( mIconType ) + "');" )
			  
			  mIconType = value
			  
			  strExec.Append( "theIcon.addClass('" + TypeToString( mIconType ) + "');" )
			  strExec.Append( "}" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		IconType As IconTypes
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mIconName As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconSize As IconSizes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconType As IconTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.fontawesome", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAnTSURBVHhe7d19bFtXFQDwexw7i51mggxpY0xjAtGkQgjGh+qUjbGNoXQdcTq1QnxprJShdR+Nk1VsElAqYIC62O26dVCqMgT7g6UsfqYREV9bNJRk0FLoBCRiQ3RfjIIyWBs/N3be4dzkdEqhKs19n3bOT2p8z0lUPb973r33Je89KyGEEEIIIYQQQgghhBBCCCGEEEIIIYQQ9QL4NXrWb21MqelWx55pLF9c/pvas6fC36kNt9ySaHqp6Y0xp3Gm1Nw8pQa2zfB3IiXUAmjtvON8+7yGq2grrlIIlyuAVtqgVoX0D9Qy/rE5qPCf9OVl2uTfUjQaU2p02so/zd8OVXMm+w5HqVW0bfQP303bfhEoeAN/ex6qE5SfovcxRe0pBXiYXkeSJ2dHpoZ3vco/FbjAC6Ap03MlQGwNIHU6qDSnzaB6AWNqHwLsKz/Wf5SzgWi6se/NgLgBHLWB3sclnDaDahxBjSA6Q2Vrx5OcDURgBdC8tvc6x8HNALCGU95CNYAI/Xax/ynO+CLZ1bcSAPtoz63nlKdQqaEYqJ3Tg7mfccpXvhdAsqu3i4Y73fHXcMpv38YKfsUeyr/IsSeSa7JvggR8kZqfm8/4CxF/SdPiTruYK3LKF74WQCqTfYDm9ds4DA7iDL2zL5QK+e2ccSXVnd1Ch+ZX6b00cio4iA+WrPztHHnOlwJo6drSNgvVvbTDruBUKBDVcBxne44Xd05yalH0+6jC7A4A1cmpcCD+qgHjG48Xtxu9j7PxvACaMr0fpRU6df7pq/jQzI8Gt9No8B3OnBM66j9LR70ewYI/6s+EziLoTGNj2cr9kDOe8LQA9HwPMWVxGC2I36Kh9FaOzoqmrt3U8ef0s0FDR2W8XBd4VgAtXZvbqrHYGJ3/vp5TUTQ+i7MfO2nt/CvHp0muveMScBIDtFfcnZ76CBW+EnecDtNp7b/RaO2NWWh4OOKdr6UboGEkubbvfRy/JrU2+07q/JEod76m97He1xy65kkBJDPZh6O+4xa4FNAZTXX1vPb7iOau7NV0aB2k9/AWTkUb7eu5fe4B11PA3I6MxQ5wWFtQ3QwKZxDgEc7UFse5oVTcMcSREdcjAMZifdysPaC+W7OdT7zY964KINndk6Eh5GoORcD0vtd9wKERVwUACHdxU4TEbR8YF0Bz110fCvs3fYJQH8z1hSHjAnBg9oPcFCFz0xfGBUDno1IAEeGmL8xOA9dvWpaqNB3nSERAKdHSoga2neDwnBmNANT5cvRHTKpy3KhPjAoAlZICiBjTPjFbAyBexC0RFYZ9YroIbOVXER1GfWJUALTqvICbIiIAILgCoAqQESB6jA5Ks0WgvnFDRIppn5hNAaCifuHHkkN9ElwB0CnHMW6KiKA+Mbq9zGwNIAUQOYCBFgBKAUQMjQBGv5o3HQGe51cRHYf4dVHMFoEII9wUUQHqILcWxagAZh0pgKih08DfcHNRjK8KTmWyk3TusZxDESJEPGZb+Qs5XBTTNYCeB37OLRE6ML4dz7gAHIU/4KYIGYAyvlfQ1Y0hqe5ePe+8dz4SoUA8WrLyl3G0aOZTAKFzTxkFwoZqH7eMuCoA22nYR0XwLw5FCFDF93LTiKsCUMXtx2kOuY8jETTEh+zi9pc4MuKuAEgp0bKdTkP+waEIiB55G2BGP7TKlQZ+NffHkdlE+6oyrURXc0YEArdMF+53/Qs5V2cBC6UyvT+i/+1GDoWfUI2UrJwnV2a7ngJOwVhls358CYfCTzHczC3XPCsAe3DXC4DebZg4M0epvtJg/vccuuZ+DbBAZXL8SLwtfSkAXM4p4SFabB8oW/k7OfSEZyPAKXbjyc00R/2FQ+ERml4nEgl1E4ee8bwA1MDuEw7CzRwJL6Capa83vbo/PzWf8I6nU8Ap1cmxo4n2Dn3VkKvHl4h5oPBTJSs/zKGnfCkArTIxdjje1hED/WEQwhg60Fsq5hb1mNvF8K0ANBoJnki0pS9UAP/zYEbx/9Gib5tdzH2DQ1/4WgAanRkM0UgQp5HgA5wS5yZvW/l7uO0b3wtAo5Hg8UR7ukSz2XWcEmeD6oGSlevhyFeBFIBWmRgfja9IvwIK5G8GZ4FK9dtWLsuh7wIrAK06Mf5UvH3V30GpGzglFqBz/XvtQv5uDgMRaAFo1Ymxg3KKeCa4lTp/KweBCbwANH2K2NiefpbWBPLXQw3xbjrPv5ejQIVSABqtCY40tK/8E60JfPn4tVpBp3pZWu33cxg4z64HMJXM9HbTKeIgh0sKLfg22YXcQxyGIvQC0FKZ7PW0JRZtTpxTS8FnSoWcqyt6vRCJAtCaM73XImCRNinFqbpFI94npwdzkficgsgUgMafK1ykjXodp+oOOs56u7hjP4ehi1QBaPqzeVUMLdowo5sdo4uWe4gZ29rxY05EgvfXA7ikP/wZHH2FMdbPQygQy/SlM2qdr0VuBDil+SM9b8cYWDRhvpVTNYmO+n8jqEy5kI/kMxUiWwBa45o739YQj1u0aFrBqZpCnX+MxtiMPZgf51TkRG4KWGhm6P4/Y2OiE1F5dhVscPB5QOiMcudrkS4ArTzwzeegMdZJh5PRI1BC8kzMiXWWirnDHEdWpKeAhc5fl22tVlSR1gTv51Qk0Wj1BwdimZOF+57lVKTVTAHMWb9pWXKmSa8JruFMxOAhhGq3vkmGE5EX+SngNAO7T9iNLavpKPPlCll3cDQWr66upc7XamsEWCDVndV/O+jiMFSo1OPJcqV7aniX0eNaw1SzBaBREQzQW1jHYSio84dt6nw1vOskp2pKTReAluzufYTexMc5DBYqq2TlujmqSaFdEOKV6sTYY/H29GWg4F2cCgaqR6nzQx19vFDzBaBVJ8ateFv6YgB4D6d8RcP+920r9wkOa1pdFIBWnRw/kGhPX0Cz2kpO+QIV7rUL+Q0c1ry6KQCtMjH+k3h7RwutCVZxymP4IHX+rRzUhboqAI3WBD+Nt3WcB6Cu5JQ3EHMlKx/I3TpBqrsC0KqTY7+gNQEtCbz5hHNE9XXbyn+ew7pSlwWg0ZrgiUTbyooCuJZTpr5MC74vcbvu1G0BaJXJ8ScTbR3TCtSHObVY95QKua9xuy7VdQFolcmxUVoTTNGaYFE3pSJAr13I1f1jcOu+ADRaE/ya1gQv05rgnG5KRYTb7EL/Lg7r2pIoAI3WBIcSy9PP0Zrg7DelIm6kOX8PR3VvyRSARmuC3zWu6HiGmme8KZUfxvQ9DpeEJVUAWmVi7On48vQRUHAF9fj5nH4RAT9dKuQf5XjJqPm/Bhpbl002Vef/dlCO4yG1P2/P5YUQQgghhBBCCCGEEEIIIYQQQgghhBBCCCFqjlL/AW7+D0/nvFT3AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADzSURBVDhPYxhwwAilwYDDJ0+OmZk59B8DIx/D/3+bv2+acAYkzulXYMLAyOTLxPD/09+/f1f/2DLpEVgDEMAN4AoocPvPwLQaKMAHFWJg/P+/BET/Z2TsAQsAwX8Ghk+MDP9Cv22YsAvEZwKLAsF/BsZ+ZM0gANKIrBkEQGpAaqFchAGMDIxaUCZBgKwWbgDD//9PoCzCAEktshfmQZkEAbJaZijN8Ofm8QOs6pZaQPdpQ4Wwg/8Mq75v7MuG8hAGgMDvm8fX4DUEqPnbxr5wKA8MUAwAAZyGYNEMAhgGgACGITg0EwScAYWzQRjKHZ6AgQEAil5SlWerb0gAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = IconSizes, Type = Integer, Flags = &h0
		Normal
		  ExtraSmall
		  Small
		  Large
		  TwoX
		  ThreeX
		  FourX
		  FiveX
		  SixX
		  SevenX
		  EightX
		  NineX
		TenX
	#tag EndEnum

	#tag Enum, Name = IconTypes, Type = Integer, Flags = &h0
		Regular
		  Solid
		Brands
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="64"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconName"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconSize"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="IconSizes"
			EditorType="Enum"
			#tag EnumValues
				"0 - Normal"
				"1 - ExtraSmall"
				"2 - Small"
				"3 - Large"
				"4 - TwoX"
				"5 - ThreeX"
				"6 - FourX"
				"7 - FiveX"
				"8 - SixX"
				"9 - SevenX"
				"10 - EightX"
				"11 - NineX"
				"12 - TenX"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconType"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="IconTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - Regular"
				"1 - Solid"
				"2 - Brands"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="64"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
