#tag Class
Protected Class GraffitiWebFieldMask
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "MaskComplete"
		      if UBound( Parameters ) >= 0 then
		        MaskComplete( GetFieldByID( Parameters(0) ) )
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  MaskQueue = new Dictionary
		  
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  //
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  ProcessQueue
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function GetCueText(TextMask as String) As String
		  dim strCurr as String
		  dim strFull as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Len( TextMask )
		  for intCycle = 1 to intMax
		    strCurr = Mid( TextMask, intCycle, 1 )
		    if strCurr = "a" or strCurr = "9" or strCurr = "*" then strCurr = Placeholder
		    if strCurr <> "?" then strFull = strFull + strCurr
		  next
		  
		  Return strFull
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetFieldByID(FieldID as String) As WebTextField
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( MaskedFields )
		  
		  dim currField as WebTextField
		  
		  for intCycle = 0 to intMax
		    currField = MaskedFields(intCycle)
		    if currField.ControlID = FieldID then Return currField
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebFieldMask -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwmaskedinput.min.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebFieldMask -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MaskField(FieldToMask as WebTextField, MaskFormat as String)
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( "window.GSjQuery('#" + FieldToMask.ControlID + "_inner').mask('" + MaskFormat + "',{placeholder:'" + Placeholder + "'," + _
		    "completed:function(){" + _
		    "Xojo.triggerServerEvent('" + self.ControlID + "', 'MaskComplete', ['" + FieldToMask.ControlID + "']);" + _
		    "}" + _
		    "});" )
		    FieldToMask.CueText = GetCueText( MaskFormat )
		    MaskedFields.Append( FieldToMask )
		  Else
		    MaskQueue.Value( FieldToMask ) = MaskFormat
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ProcessQueue()
		  if LibrariesLoaded then
		    dim intCycle as Integer
		    dim intMax as Integer = MaskQueue.Count - 1
		    
		    dim fldKey as WebTextField
		    dim strVal as String
		    
		    for intCycle = 0 to intMax
		      fldKey = MaskQueue.Key( intCycle )
		      strVal = MaskQueue.Value( fldKey )
		      MaskField( fldKey, strVal )
		    next
		    
		    MaskQueue.Clear
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event MaskComplete(TheField as WebTextField)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook


	#tag Note, Name = Source
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		http://digitalbush.com/projects/masked-input-plugin/
		
		Licensed under MIT license.
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  ProcessQueue()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private MaskedFields() As WebTextField
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MaskQueue As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Placeholder As String = "_"
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webfieldmask", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAcSSURBVHhe7d1bbBRlFAfw78wspbttjXcuxruB8uItsUF88PLitTcFCQ9e4ovBRGCLkJiYIPHBSGVXMZGY+OAlMVHRdikqPnghUcA+KIkPNMR4V0Q0MbS7pXTnO57pnCaYbne2F7XznfPDMt93ZkpnZ/7zzcx23TVKKaWUUkoppZRSSimllHIa8PR/l+nIPoFgTpgg6gNAyXo4BBaLnvEHPSwfrTN1v/65u3swWuK/d07bpqZT5tRiC6lFCNho6cuz0Ejr3QBoGtBgXbjccCH/5Ng3JMAcCkAXcrMq2siDYOAQLdyPxvb7gd1X7NtxjGfPmobWdQsC37sRjNdCG6mFfu7V9HObeHZVpd7cnNmucRIXgIrQfG4A91rPe/3ku9t/4OqU1d+98WLP2vsMwm20ZW7g8pRpAKZhRgE4DR2pr9Bk53Bvvj+qxEt3ZFtospaO8AejyswkKQAeT50R7kT6+oJ26jNmy5bqj4/mp9u7toXLz9bOTxrnAjCOdujmzKETh9Od2eVc+oewHs4HMJu4JJKzARgDsAQsfFbf0bWGK2PCflgP53NJrLkZAMSfEfE4NYphLypOExifHuQbdI3xUNhNd3atCvthfWz+9NF6YTFaT/NjVEqeZFysrMymm07Zi8oeNANCM913N9Oq304rv4CXqJF9nDL/NHdqQnuZbjHxA7rPH6B7/4GUxYHBOu9Hsys/zIskWmKuVivJtHfdSo+gk5oPR5VZ9RLt/Z5SIfch952U6ACMm9++/hIf/PBi7pGoMiMvBhh0jxSe/577TnMiAOMaOjfcYtF7hx7UmVyqGQ31f3lg7yn2PPcxl0RwKgChTOeGaw3CLnpol3KpBvidAVxZ6nnuSy6I4VwAQnTFfxdN+qJeTVpLvbk93BZlprdCc9LowIEj85YuX0D3+ddxaXKIO0uFfJ574jj7RFBg7DZuVlXrcq5yNgDRVTzu5+4kcL+Uq/3JOBuAMQgFblUWN18ApwNgAaoe3Ras6KM/5HQAfDS/c7MiH/2q8yVw+xRgg/AXNZOLmy+A0wGAeR6/xLQy8NFyUyy3R4AYGIDoxx+SvQFSvpPPhE6F7ACUR3UE4KlIqCOA8BFAaQCk0wAIpwEQTgMgnAZAOA2AcBoA4TQAwmkAhNMACKcBEE4DIJwGQDgNgHBuB6CsL/mK4/QGwnmB+Bd8xNERQLjEHyHpjmw3INzN3YnAXMatidB8y60JEPDd4d68828h58YREu7kyb6qqbT8+JcQOkQKpwEQTgMgnAZAOA2AcMm/DezcmAW0K7hbAazkRgW4ixsTIHj7h3u2O//mUU4/U5bpzF5Fe/IQdycAxCuLhfzX3BVJ9inA82blU0qSTHQAIDD6BhE8lckr6wjAU5nKvgaAp0ooDYBwGgDhNADCaQCE0wAIpwEQTgMgnAZAOA2AcBoA4TQAwmkAhNMACKcBEE4DIJwGQDgNgHAaAOE0AMI5HQAoB+Jf9BnH7RHA01f9xnE7ACn9ZNA4bp8CRnUEiOP4KUD/z584bo8AJnUJNysCA5dyUyynAzCKwRA3Kxr1q8+XwPERAM7nZkVx8yVw+xoAzCpuVRY3XwC3A4BwFrcqQ3M2t8Ry9i1iGu/qOtemzHHuTsorm/OG9uT+4K44zo4AQQrXcbOqWpdzlZMjQLo1uxx8OMDdWBjg9cN9+YPcFcW5ADS0rluAnn/EAJzBpXiIJ8AGS4p9O45xRQynTgFNbeuXWi/10ZR2foiWD7+vqW3TUq6I4fM08TId2U3WeC8DmKrP/k2Gvu98a3B13bLlZnTg4H4uOy/Rp4D5rdkrfN90UvNReigXRtXZgD/RXy8EgekZ6ct/E9XclJwAbNniNX411ByAXQYAzXTevpMO2+t5bk0Qzat0pD/A3dogHqCf8x4aOOxbMzB0TeOA2brVmV8zz5kAZDq6jtAOGjSAJ6gbftpTmjZ6BhAztJoZWtMLqD7t9UWL64d353fQqeIJ+mee4vJ0IP35hSYlBChBODUwQuvZiGAaAKG+VMhdzsvOeXMpAP/ir25xc6k3382dsesFeujbuDvrSr25xIysbj8VbMwIHZX3nr7zQ2E/rFNzJKrI5W4AEN8KAlg23JN7myv/ENbD+eFyXBLJvVMAmn10hbbzZCH3Jldi1bd3raYjYS1tjRu5NCNJOgU4EQC6KvuNJns9a14r7s5/ElWnrqEte7P1zP3UvA0MLIyqU6cBmIapBIDuFo7SivdTs5+O9k/paJ/1J25oVFhBo8JN1GyhFWuh28dF0Zx4GoBpoCvzt8OP66T/inQrNUQXaUU6CofQBkUAb5B2+680/6gpe0dL7z8bHvH/qcwdjy00KbuIblMpCLAY0TaB5zfQ6NNIGzEdLkPBrKf2GXQbuGbsm5RSSimllFJKKaWUUkoppZT6/xjzNwp3F3hImVLkAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = Mask24HrTime, Type = String, Dynamic = False, Default = \"99:99\?:99", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskCAZipCode, Type = String, Dynamic = False, Default = \"a9a 9a9", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskCreditCard, Type = String, Dynamic = False, Default = \"9999 9999 9999 99\?99", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskDate, Type = String, Dynamic = False, Default = \"99/99/9999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskDateDashed, Type = String, Dynamic = False, Default = \"9999-99-99", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskDEPostCode, Type = String, Dynamic = False, Default = \"99999\?-99999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskPhone, Type = String, Dynamic = False, Default = \"(999) 999-9999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskPhoneExt, Type = String, Dynamic = False, Default = \"(999) 999-9999\? x99999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskPhoneWithCountry, Type = String, Dynamic = False, Default = \"+999 (999) 999-9999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskSSN, Type = String, Dynamic = False, Default = \"999-99-9999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskTaxID, Type = String, Dynamic = False, Default = \"99-9999999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskUKPostCode, Type = String, Dynamic = False, Default = \"**** ***", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MaskUSZipCode, Type = String, Dynamic = False, Default = \"99999\?-9999", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADFSURBVDhPY6A64AgoCOMKKHCDchlAbJAYlIsBGKE0HHAFFPYx/Ge0gHIhgPH/iW8b+ougPOIAZ0DhbBCGcnECJiiNAYBO+wzCUC5OgNMAYgFWAzh88uSA4SANwmA2HoDVACYWJg0Ghv9WIAxh4wbMUBoF/L5x4i6rpqUsMCCufdswYRpUGCugTRiQAig2ACMlgpItEwMj/38GhgIQH6hgwj+G/x9/bJiwCqwADWC4gOk/UwEw+lwYGf4XgjCIDRYbpICBAQBwtS/bwNtTjAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Placeholder"
			Visible=true
			Group="Behavior"
			InitialValue="_"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
