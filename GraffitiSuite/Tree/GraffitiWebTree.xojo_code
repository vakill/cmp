#tag Class
Protected Class GraffitiWebTree
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  if IsNull( Session ) then Return
		  
		  dim intCurr as Integer
		  if not IsNull( Session.GraffitiSuiteInstances ) then
		    if Session.GraffitiSuiteInstances.HasKey( JavascriptNamespace ) Then
		      intCurr = Session.GraffitiSuiteInstances.Value( JavascriptNamespace ).IntegerValue - 1
		    else
		      intCurr = 0
		    end if
		    Session.GraffitiSuiteInstances.Value( JavascriptNamespace ) = intCurr
		  end if
		  
		  if IsLibraryRegistered( Session, JavascriptNamespace, "jquery.minicolors-2.1.2.js" ) and intCurr = 0 then UnregisterLibrary( Session, JavascriptNamespace, "jquery.minicolors-2.1.2.js" )
		  
		  RaiseEvent Close
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "rightclick"
		      if Parameters.Ubound >= 2 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        dim intX as Integer = Parameters(1)
		        dim intY as Integer = Parameters(2)
		        if not IsNull( theItem ) then
		          RaiseEvent ContextClick( theItem, intX, intY )
		        end if
		      ElseIf Parameters.Ubound = 1 then
		        dim intX as Integer = Parameters(0)
		        dim intY as Integer = Parameters(1)
		        RaiseEvent ContextClick( nil, intX, intY )
		      end if
		    case "activate"
		      dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		      mSelectedItem = theItem
		      if not IsNull( theItem ) then ItemSelected( theItem )
		    case "create"
		      UpdateIconStyle()
		      UpdateItemSelStyle()
		      UpdateItemStyles()
		    case "click"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        if not IsNull( theItem ) then 
		          if Parameters.Ubound >= 1 then
		            if InStr( Parameters(1), "fancytree-custom-icon" ) > 0 then
		              ItemIconClicked( theItem )
		            else
		              ItemClicked( theItem )
		            end if
		          else
		            ItemClicked( theItem )
		          end if
		        end if
		      end if
		    case "deactivate"
		      mSelectedItem = nil
		    case "dblclick"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        if not IsNull( theItem ) then ItemDoubleClicked( theItem )
		      end if
		    case "editEnd"
		      if Parameters.Ubound >= 1 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        if not IsNull( theItem ) then
		          theItem.Caption = Parameters(1).StringValue
		          ItemEdited( theItem )
		        end if
		      end if
		    case "expand"
		      if Parameters.Ubound >= 1 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        theItem.Expanded = Parameters(1).BooleanValue
		        if not IsNull( theItem ) then ItemExpanded( theItem )
		      end if
		    case "collapse"
		      if Parameters.Ubound >= 1 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        theItem.Expanded = Parameters(1).BooleanValue
		        if not IsNull( theItem ) then ItemCollapsed( theItem )
		      end if
		    case "keypress"
		      if Parameters.Ubound >= 1 then
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0), Items )
		        if not IsNull( theItem ) then ItemKeyPressed( theItem, Parameters(1) )
		      end if
		    case "select"
		      if Parameters.Ubound >= 1 then 
		        dim theItem as GraffitiWebTreeItem = FindItemByUUID( Parameters(0).StringValue, Items() )
		        if not IsNull( theItem ) then
		          theItem.CheckValue = Parameters(1).BooleanValue
		          if Parameters(1).BooleanValue then
		            ItemChecked( theItem )
		          else
		            ItemUnchecked( theItem )
		          end if
		        end if
		      end if
		    case "treeblur"
		      UpdateItemSelStyle()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  dim strExec() as String
		  
		  select case Name
		  case "style"
		    if not IsNull( Value ) then
		      dim vStyle as WebStyle = WebStyle( Value )
		      
		      if vStyle.Name = "_DefaultStyle" then
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + "').removeClass().addClass('fancytree-" + Theme + "');" )
		      else
		        if LastStyle <> "" and LastStyle <> "_DefaultStyle" then
		          strExec.Append( "window.GSjQuery('#" + me.ControlID + "').removeClass('" + LastStyle + "');" )
		        end if
		        
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + "').addClass('" + vStyle.Name + "');" )
		      end if
		      
		      LastStyle = vStyle.Name
		      
		    end if
		    
		    Buffer( strExec )
		    Return True
		  case "enabled"
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('" + if( value, "enable", "disable" ) + "');" )
		    Buffer( strExec )
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  DoResize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' class='fancytree-default' style='display:none;'></div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  //
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddItem(theItem as GraffitiWebTreeItem, theParent as GraffitiWebTreeItem = Nil, AddToParentItem as Boolean = True)
		  dim strOut() as String
		  if IsNull( theParent ) then
		    strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode');" )
		    Items.Append( theItem )
		  else
		    strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theParent.UUID + "');" )
		    if AddToParentItem then theParent.Children.Append( theItem )
		    theItem.Parent = theParent
		  end if
		  strOut.Append( "var childNode = rootNode.addChildren(" + ItemToString( theItem ) + ");" )
		  
		  if theItem.Lazy then
		    dim emptyChild as new GraffitiWebTreeItem( ". . ." )
		    strOut.Append( "var emptyChild = childNode.addChildren(" + ItemToString( emptyChild ) + ");" )
		  end if
		  
		  Buffer( strOut )
		  
		  for intCycle as Integer = 0 to theItem.Children.Ubound
		    AddItem( theItem.Children(intCycle), theItem, False )
		  next
		  
		  if not LockUpdate then
		    UpdateItemStyles()
		    UpdateItemSelStyle()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function AddItemJS(theItem as GraffitiWebTreeItem) As String
		  dim theParent as GraffitiWebTreeItem = theItem.Parent
		  
		  dim strOut() as String
		  if IsNull( theParent ) then
		    strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode');" )
		  else
		    strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theParent.UUID + "');" )
		  end if
		  strOut.Append( "var childNode = rootNode.addChildren(" + ItemToString( theItem ) + ");" )
		  
		  if theItem.Lazy then
		    dim emptyChild as new GraffitiWebTreeItem( ". . ." )
		    strOut.Append( "var emptyChild = childNode.addChildren(" + ItemToString( emptyChild ) + ");" )
		  end if
		  
		  for intCycle as Integer = 0 to theItem.Children.Ubound
		    strOut.Append( AddItemJS( theItem.Children(intCycle) ) )
		  next
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CheckAll()
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode').visit(function(node){" )
		  strOut.Append( "if(node.checkbox == false || typeof(node.checkbox) == 'undefined') {" )
		  strOut.Append( "node.setSelected(true);" )
		  strOut.Append( "}" )
		  strOut.Append( "});" )
		  
		  Buffer( strOut )
		  
		  do_CheckAll( Items, True )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CheckItem(theItem as GraffitiWebTreeItem, theValue as Boolean = True)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "rootNode.setSelected(" + Str( theValue ).Lowercase + ");" )
		  
		  Buffer( strOut )
		  
		  theItem.CheckValue = theValue
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CollapseAll()
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode').visit(function(node){" )
		  strOut.Append( "node.setExpanded(false);" )
		  strOut.Append( "});" )
		  
		  Buffer( strOut )
		  
		  do_CollapseExpandAll( Items, False )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CollapseItem(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  theItem.Expanded = False
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.setExpanded(false);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub CompareChecked(theItems() as GraffitiWebTreeItem)
		  dim currItem as GraffitiWebTreeItem
		  
		  dim intMax as Integer = lastChecked.Ubound
		  for intCycle as Integer = 0 to intMax
		    currItem = lastChecked(intCycle)
		    if theItems.IndexOf( currItem ) < 0 then ItemUnchecked( currItem  )
		  next
		  
		  intMax = theItems.Ubound
		  for intCycle as Integer = 0 to intMax
		    currItem = theItems(intCycle)
		    if lastChecked.IndexOf( currItem ) < 0 then ItemChecked( currItem )
		  next
		  
		  lastChecked = theItems
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CountItems() As Integer
		  Return Items.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAll()
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode').visit(function(node){" )
		  strOut.Append( "node.setActive(false);" )
		  strOut.Append( "});" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DisableItem(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.unselectable = true;" )
		  
		  Buffer( strOut )
		  
		  theItem.Enabled = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DoInsertItem(theItem as GraffitiWebTreeItem, beforeItem as GraffitiWebTreeItem, fromItems() as GraffitiWebTreeItem) As Boolean
		  for intCycle as Integer = 0 to fromItems.Ubound
		    dim currItem as GraffitiWebTreeItem = fromItems(intCycle)
		    if currItem = beforeItem then
		      fromItems.Insert( intCycle, theItem )
		      Return True
		    end if
		    
		    if currItem.Children.Ubound >= 0 then 
		      dim blnRet as Boolean = DoInsertItem( theItem, beforeItem, currItem.Children )
		      if blnRet = True then Return blnRet
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DoRemove(theItem as GraffitiWebTreeItem, fromItems() as GraffitiWebTreeItem) As Boolean
		  for intCycle as Integer = 0 to fromItems.Ubound
		    dim currItem as GraffitiWebTreeItem = fromItems(intCycle)
		    if currItem = theItem then
		      fromItems.Remove( intCycle )
		      Return True
		    end if
		    
		    if currItem.Children.Ubound >= 0 then 
		      dim blnRet as Boolean = DoRemove( theItem, currItem.Children )
		      if blnRet = True then Return blnRet
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoResize()
		  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" + _
		  "window.GSjQuery('#" + me.ControlID + "_inner').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub do_CheckAll(inItems() as GraffitiWebTreeItem, newValue as Boolean)
		  for each ti as GraffitiWebTreeItem in inItems
		    ti.CheckValue = newValue
		    if ti.Children.Ubound >= 0 then do_CheckAll( ti.Children, newValue )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub do_CollapseExpandAll(inItems() as GraffitiWebTreeItem, newValue as Boolean)
		  for each ti as GraffitiWebTreeItem in inItems
		    ti.Expanded = newValue
		    if ti.Children.Ubound >= 0 then do_CollapseExpandAll( ti.Children, newValue )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditItem(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.editStart();" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditItemEnd(theItem as GraffitiWebTreeItem, ApplyChanges as Boolean)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.editEnd(" + Str( ApplyChanges ).Lowercase + ");" )
		  strOut.Append( "theNode.render(true);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EnableItem(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.unselectable = false;" )
		  
		  Buffer( strOut )
		  
		  theItem.Enabled = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExpandAll()
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode').visit(function(node){" )
		  strOut.Append( "node.setExpanded(true);" )
		  strOut.Append( "});" )
		  
		  Buffer( strOut )
		  
		  do_CollapseExpandAll( Items, True )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExpandItem(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  theItem.Expanded = True
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.setExpanded(true);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(itemText as String) As GraffitiWebTreeItem
		  for intCycle as Integer = 0 to Items.Ubound
		    dim currItem as GraffitiWebTreeItem = Items(intCycle)
		    if currItem.Caption = itemText then
		      Return currItem
		    end if
		    
		    if currItem.Children.Ubound >= 0 then 
		      dim retItem as GraffitiWebTreeItem = FindItemByText( itemText, currItem.Children )
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FindItemByText(itemText as String, inItems() as GraffitiWebTreeItem) As GraffitiWebTreeItem
		  for intCycle as Integer = 0 to InItems.Ubound
		    dim currItem as GraffitiWebTreeItem = InItems(intCycle)
		    if currItem.Caption = itemText then
		      Return currItem
		    end if
		    
		    if currItem.Children.Ubound >= 0 then 
		      dim retItem as GraffitiWebTreeItem = FindItemByText( itemText, currItem.Children )
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FindItemByUUID(theUUID as String, InItems() as GraffitiWebTreeItem) As GraffitiWebTreeItem
		  for intCycle as Integer = 0 to InItems.Ubound
		    dim currItem as GraffitiWebTreeItem = InItems(intCycle)
		    if currItem.UUID = theUUID then
		      Return currItem
		    end if
		    
		    if currItem.Children.Ubound >= 0 then 
		      dim retItem as GraffitiWebTreeItem = FindItemByUUID( theUUID, currItem.Children )
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetDepth(theItem as GraffitiWebTreeItem) As Integer
		  if IsNull( theItem ) then Return -1
		  
		  Return CountFields( GetIndexPath( theItem, "." ), "." )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIndexOf(theItem as GraffitiWebTreeItem) As Integer
		  if IsNull( theItem ) then Return -1
		  
		  dim inArray() as GraffitiWebTreeItem
		  if IsNull( theItem.Parent ) then
		    inArray = Items
		  else
		    inArray = theItem.Parent.Children
		  end if
		  
		  for intCycle as Integer = 0 to inArray.Ubound
		    if theItem = inArray(intCycle) then Return intCycle
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIndexPath(theItem as GraffitiWebTreeItem, Separator as String) As String
		  if IsNull( theItem ) then Return ""
		  
		  dim strOut as String = Str( GetIndexOf( theItem ) )
		  dim nextParent as GraffitiWebTreeItem = theItem.Parent
		  while not IsNull( nextParent )
		    strOut = Str( GetIndexOf( nextParent ) ) + Separator + strOut
		    nextParent = nextParent.Parent
		  wend
		  
		  Return strOut
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetPath(theItem as GraffitiWebTreeItem, Separator as String) As String
		  if IsNull( theItem ) then Return ""
		  
		  dim strOut as String = theItem.Caption
		  dim nextParent as GraffitiWebTreeItem = theItem.Parent
		  while not IsNull( nextParent )
		    strOut = nextParent.Caption + Separator + strOut
		    nextParent = nextParent.Parent
		  wend
		  
		  Return strOut
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GlyphMap() As String
		  dim strExec() as String
		  
		  strExec.Append( "glyph: {" )
		  strExec.Append( "map: {" )
		  strExec.Append( "checkbox: 'fa fa-fw " + EscapeString( mIconCheckbox ) + "'," )
		  strExec.Append( "checkboxSelected: 'fa fa-fw " + EscapeString( mIconCheckboxChecked ) + "'," )
		  strExec.Append( "checkboxUnknown: 'fa fa-fw " + EscapeString( mIconCheckboxUnknown ) + "'," )
		  strExec.Append( "expanderClosed: 'fa fa-fw " + EscapeString( mIconExpanderClosed ) + "'," )
		  strExec.Append( "expanderOpen: 'fa fa-fw " + EscapeString( mIconExpanderOpen ) + "'," )
		  strExec.Append( "folder: 'fa fa-fw " + EscapeString( mIconExpanderClosed ) + "'," )
		  strExec.Append( "folderOpen: 'fa fa-fw " + EscapeString( mIconExpanderOpen ) + "'," )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function HasChildren(theItem as GraffitiWebTreeItem) As Boolean
		  if IsNull( theItem ) then Return false
		  
		  if theItem.Children.Ubound >= 0 then Return True
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HideCheckbox(theItem as GraffitiWebTreeItem)
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.checkbox = false;" )
		  strOut.Append( "theNode.render(true);" )
		  
		  theItem.Checkbox = False
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebTree -->" )
		  
		  strOut.Append( AddjQuery( CurrentSession ) )
		  strOut.Append( AddjQueryUI( CurrentSession ) )
		  strOut.Append( AddFontAwesome5( CurrentSession ) )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "tree" ).Child( "jquery.fancytree-all.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "default.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "bootstrap.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "base.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "lion.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "vista.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "win7.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "win8.ui.fancytree.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tree" ).Child( "xp.ui.fancytree.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebTree -->" )
		  
		  Return Join( strOut )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InsertItem(theItem as GraffitiWebTreeItem, insertBefore as GraffitiWebTreeItem)
		  if not DoInsertItem( theItem, insertBefore, Items ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode');" )
		  strOut.Append( "var beforeNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + insertBefore.UUID + "');" )
		  strOut.Append( "var childNode = rootNode.addChildren(" + ItemToString( theItem ) + ", beforeNode);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsChildOf(theItem as GraffitiWebTreeItem, theParent as GraffitiWebTreeItem) As Boolean
		  if isNull( theItem ) or IsNull( theParent ) then Return False
		  
		  if theItem.Parent = theParent then Return True
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsFirstChild(theItem as GraffitiWebTreeItem) As Boolean
		  if isNull( theItem ) then Return False
		  
		  dim arrInArray() as GraffitiWebTreeItem
		  if IsNull( theItem.Parent ) then
		    arrInArray = Items()
		  else
		    arrInArray = theItem.Parent.Children
		  end if
		  
		  
		  if arrInArray.Ubound >= 0 then
		    if arrInArray(0) = theItem then Return True
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsLastChild(theItem as GraffitiWebTreeItem) As Boolean
		  if isNull( theItem ) then Return False
		  
		  dim arrInArray() as GraffitiWebTreeItem
		  if IsNull( theItem.Parent ) then
		    arrInArray = Items()
		  else
		    arrInArray = theItem.Parent.Children
		  end if
		  
		  
		  if arrInArray.Ubound >= 0 then
		    if arrInArray(arrInArray.Ubound) = theItem then Return True
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsSiblingOf(theItem as GraffitiWebTreeItem, theSibling as GraffitiWebTreeItem) As Boolean
		  if isNull( theItem ) or IsNull( theSibling ) then Return False
		  
		  dim arrInArray() as GraffitiWebTreeItem
		  if IsNull( theItem.Parent ) then
		    arrInArray = Items()
		  else
		    arrInArray = theItem.Parent.Children
		  end if
		  
		  for intCycle as Integer = 0 to arrInArray.Ubound
		    if arrInArray(intCycle) = theSibling then Return True
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsTopLevel(theItem as GraffitiWebTreeItem) As Boolean
		  if isNull( theItem ) then Return False
		  
		  dim arrInArray() as GraffitiWebTreeItem
		  if IsNull( theItem.Parent ) then
		    Return True
		  else
		    Return False
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemToString(theItem as GraffitiWebTreeItem) As String
		  dim strOut() as String
		  
		  strOut.Append( "{" )
		  strOut.Append( "key: '" + theItem.UUID + "'," )
		  strOut.Append( "title: '" + EscapeString( theItem.Caption ) + "'," )
		  
		  if not IsNull( theItem.Style ) then strOut.Append( "customStyle: '" + theItem.Style.Name + "'," )
		  
		  if theItem.Lazy then strOut.Append( "isLazy: " + Str( theItem.Lazy ).Lowercase + "," )
		  strOut.Append( "hasExpander: " + Str( theItem.HasExpander ).Lowercase + "," )
		  
		  strOut.Append( "checkbox: " + Str( theItem.Checkbox ).Lowercase + "," )
		  if theItem.CheckValue and not theItem.Checkbox then
		    strOut.Append( "selected: true," )
		    lastChecked.Append( theItem )
		  else
		    strOut.Append( "selected: " + Str( theItem.CheckValue ).Lowercase + "," )
		  end if
		  
		  if theItem.Icon <> "" then strOut.Append( "icon: '" + EscapeString( theItem.Icon ) + "'," )
		  
		  'if not IsNull( theItem.Style ) then strOut.Append( "titleClasses: '" + theItem.Style.Name + "'," )
		  strOut.Append( "}" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveTo(theItem as GraffitiWebTreeItem, newParent as GraffitiWebTreeItem, atIndex as Integer = -1)
		  if IsNull( theItem ) then Return
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "var newParent = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + newParent.UUID + "');" )
		  
		  if IsNull( theItem.Parent ) then
		    call DoRemove( theItem, Items )
		  else
		    call DoRemove( theItem, theItem.Parent.Children )
		  end if
		  
		  theItem.Parent = newParent
		  
		  select case atIndex
		  case is < 0, is > theItem.Parent.Children.Ubound
		    strOut.Append( "theNode.moveTo(newParent, 'child');" )
		    theItem.Parent.Children.Append( theItem )
		  case 0
		    strOut.Append( "theNode.moveTo(newParent, 'firstChild');" )
		    theItem.Parent.Children.Insert( 0, theItem )
		  case else
		    if atIndex >= 0 and atIndex <= newParent.Children.Ubound then
		      dim targetNode as GraffitiWebTreeItem = newParent.Children( atIndex )
		      strOut.Append( "var targetNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + targetNode.UUID + "');" )
		      strOut.Append( "theNode.moveTo(targetNode, 'before');" )
		      theItem.Parent.Children.Insert( atIndex, theItem )
		    end if
		  end select
		  
		  Buffer( strOut )
		  
		  if not LockUpdate then
		    UpdateItemStyles()
		    UpdateItemSelStyle()
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll(fromItem as GraffitiWebTreeItem = Nil)
		  dim strOut() as String
		  if IsNull( fromItem ) then
		    ReDim Items(-1)
		    strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode');" )
		  else
		    ReDim fromItem.Children(-1)
		    strOut.Append( "var rootNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + fromItem.UUID + "');" )
		  end if
		  strOut.Append( "rootNode.removeChildren();" )
		  
		  if not IsNull( fromItem ) then
		    if fromItem.Lazy then
		      dim emptyChild as new GraffitiWebTreeItem( ". . ." )
		      strOut.Append( "var emptyChild = rootNode.addChildren(" + ItemToString( emptyChild ) + ");" )
		    end if
		  end if
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(theItem as GraffitiWebTreeItem, includeChildren as Boolean = True)
		  if IsNull( theItem ) then Return
		  
		  if not DoRemove( theItem, Items ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  if not includeChildren then
		    strOut.Append( "while( theNode.hasChildren() ) {" )
		    strOut.Append( "theNode.getFirstChild().moveTo(theNode.parent, 'child');" )
		    strOut.Append( "}" )
		  end if
		  strOut.Append( "theNode.remove();" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollTo(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "if( typeof(theNode) !== 'undefined') {" )
		  strOut.Append( "theNode.makeVisible();" )
		  strOut.Append( "}" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectItem(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then return
		  
		  dim strOut() as String
		  strOut.Append( "var tree = window.GSjQuery('#" + me.ControlID + "').fancytree('getTree');" )
		  strOut.Append( "var node = tree.getNodeByKey('" + theItem.UUID + "');" )
		  strOut.Append( "node.setSelected(true);" )
		  strOut.Append( "node.setActive(true);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowCheckbox(theItem as GraffitiWebTreeItem)
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "theNode.checkbox = true;" )
		  strOut.Append( "theNode.render(true);" )
		  
		  theItem.Checkbox = True
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UncheckAll()
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('getRootNode').visit(function(node){" )
		  strOut.Append( "if(node.checkbox == false || typeof(node.checkbox) == 'undefined') {" )
		  strOut.Append( "node.setSelected(false);" )
		  strOut.Append( "}" )
		  strOut.Append( "});" )
		  
		  Buffer( strOut )
		  
		  do_CheckAll( Items, False )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateCaption(theItem as GraffitiWebTreeItem, newCaption as String)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "if( typeof(theNode) !== 'undefined') {" )
		  strOut.Append( "theNode.setTitle('" + EscapeString( newCaption ) + "');" )
		  strOut.Append( "theNode.render(true);" )
		  strOut.Append( "}" )
		  
		  theItem.Caption = newCaption
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateIcon(theItem as GraffitiWebTreeItem, newIcon as String)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "if( typeof(theNode) !== 'undefined') {" )
		  strOut.Append( "theNode.icon = '" + EscapeString( newIcon ) + "';" )
		  strOut.Append( "theNode.render(true);" )
		  strOut.Append( "}" )
		  
		  theItem.Icon = newIcon
		  
		  Buffer( strOut )
		  
		  UpdateIconStyle
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateIconStyle()
		  if IsNull( IconStyle ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var tree = window.GSjQuery('#" + me.ControlID + "').fancytree('getTree');" )
		  strOut.Append( "tree.visit(function(node){" )
		  strOut.Append( "node.data.iconStyle = '" + IconStyle.Name + "';" )
		  strOut.Append( "node.render(true);" )
		  strOut.Append( "});" )
		  strOut.Append( "tree.render(true);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateItemSelStyle()
		  if IsNull( ItemSelStyle ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var tree = window.GSjQuery('#" + me.ControlID + "').fancytree('getTree');" )
		  strOut.Append( "tree.visit(function(node){" )
		  strOut.Append( "node.data.itemSelStyle = '" + ItemSelStyle.Name + "';" )
		  strOut.Append( "});" )
		  strOut.Append( "tree.render(true);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateItemStyle(theItem as GraffitiWebTreeItem)
		  if IsNull( theItem ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theNode = window.GSjQuery('#" + me.ControlID + "').fancytree('getNodeByKey','" + theItem.UUID + "');" )
		  strOut.Append( "if( typeof(theNode) !== 'undefined') {" )
		  strOut.Append( "theNode.data.customStyle = '" + if( IsNull( theItem.Style ), "", theItem.Style.Name ) + "';" )
		  strOut.Append( "theNode.render(true);" )
		  strOut.Append( "}" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateItemStyles()
		  if IsNull( ItemStyle ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var tree = window.GSjQuery('#" + me.ControlID + "').fancytree('getTree');" )
		  strOut.Append( "tree.visit(function(node){" )
		  strOut.Append( "node.data.itemStyle = '" + ItemStyle.Name + "';" )
		  strOut.Append( "});" )
		  strOut.Append( "tree.render(true);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if hasLoaded and LibrariesLoaded then
		    
		    dim strExec() as String
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree({" )
		    strExec.Append( "extensions: ['glyph'" + if( mAllowEdit, ", 'edit'", "" ) + "]," )
		    strExec.Append( "source: []," )
		    strExec.Append( "cookieId: 'gwtree-" + me.Name + "-" + me.controlid + "'," )
		    strExec.Append( "idPrefix: 'gwtree-" + me.Name + "-" + me.controlid + "-'," )
		    strExec.Append( "checkbox: " + Str( mCheckboxes ).Lowercase + "," )
		    strExec.Append( "autoScroll: true," )
		    strExec.Append( "selectMode: 2," )
		    strExec.Append( "focusOnSelect: true," )
		    strExec.Append( "quickSearch: true," )
		    strExec.Append( "activeVisible: true," )
		    strExec.Append( "autoActivate: true," )
		    strExec.Append( "debugLevel: 0," )
		    strExec.Append( "generateIds: true," )
		    strExec.Append( "idPrefix: ''," )
		    strExec.Append( "tabbable: true," )
		    strExec.Append( GlyphMap + "," )
		    'strExec.Append( "dnd: {" )
		    'strExec.Append( "autoExpandMS: 400," )
		    'strExec.Append( "focusOnClick: true," )
		    'strExec.Append( "preventVoidMoves: true," )
		    'strExec.Append( "preventRecursiveMoves: true," )
		    'strExec.Append( "dragStart: function(node, data) {" )
		    'strExec.Append( "return true;" )
		    'strExec.Append( "}," )
		    'strExec.Append( "dragEnter: function(node, data) {" )
		    'strExec.Append( "return true;" )
		    'strExec.Append( "}," )
		    'strExec.Append( "dragDrop: function(node, data) {" )
		    'strExec.Append( "data.otherNode.moveTo(node, data.hitMode);" )
		    'strExec.Append( "}" )
		    'strExec.Append( "}," )
		    
		    strExec.Append( "edit: {" )
		    strExec.Append( "close: function (event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'editEnd', [data.node.key,data.node.title]);" )
		    strExec.Append( "data.node.render(true);" )
		    strExec.Append( "}" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "create: function(event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'create', []);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "click: function(event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'click', [data.node.key,event.target.className]);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "dblclick: function(event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'dblclick', [data.node.key,data.targetType]);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "beforeExpand: function(event, data) {" )
		    strExec.Append( "if( typeof event.originalEvent !== 'undefined' && event.originalEvent.type == 'dblclick' ) {" )
		    strExec.Append( "if( data.node.data.hasExpander == false ) { return false; }" )
		    strExec.Append( "}" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "collapse: function(event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'collapse', [data.node.key,data.node.expanded]);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "expand: function(event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'expand', [data.node.key,data.node.expanded]);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "select: function(event, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'select', [data.node.key,data.node.selected]);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "activate: function(event, data) {" )
		    strExec.Append( "if( typeof(data.node) !== 'undefined' ) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'activate', [data.node.key]);" )
		    strExec.Append( "data.node.render(true);" )
		    strExec.Append( "}" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "deactivate: function(event, data) {" )
		    strExec.Append( "if( typeof(data.node) !== 'undefined' ) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'deactivate', []);" )
		    strExec.Append( "data.node.render(true);" )
		    strExec.Append( "}" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "keydown: function(event, data) {" )
		    strExec.Append( "var theContainer = window.GSjQuery('#" + me.ControlID + "').find('.fancytree-container');" )
		    strExec.Append( "if (theContainer.hasClass('fancytree-rtl')) {" )
		    strExec.Append( "var KC = window.GSjQuery.ui.keyCode," )
		    strExec.Append( "oe = event.originalEvent;" )
		    strExec.Append( "switch( event.which ) {" )
		    strExec.Append( "case KC.LEFT:" )
		    strExec.Append( "oe.keyCode = KC.RIGHT;" )
		    strExec.Append( "oe.which = KC.RIGHT;" )
		    strExec.Append( "break;" )
		    strExec.Append( "case KC.RIGHT:" )
		    strExec.Append( "oe.keyCode = KC.LEFT;" )
		    strExec.Append( "oe.which = KC.LEFT;" )
		    strExec.Append( "break;" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "blur: function(event, data) {" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "focus: function(event, data) {" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "renderNode: function(event, data) {" )
		    strExec.Append( "var node = data.node;" )
		    strExec.Append( "var span = window.GSjQuery(node.span);" )
		    
		    strExec.Append( "if( node.data.hasExpander == false ) {" )
		    strExec.Append( "var theNode = window.GSjQuery(node.li).find('.fancytree-node');" )
		    strExec.Append( "var theButton = theNode.find('.fancytree-expander');" )
		    strExec.Append( "theButton.css('display', 'none');" )
		    strExec.Append( "}" )
		    
		    strExec.Append( "window.GSjQuery(node.li).off('contextmenu').on('contextmenu', function (e) {" )
		    strExec.Append( "var theTarget = window.GSjQuery(this);" )
		    strExec.Append( "var menuX = e.pageX;" )
		    strExec.Append( "var menuY = e.pageY;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'rightclick', [theTarget[0].id, menuX, menuY]);" )
		    strExec.Append( "e.preventDefault();" )
		    strExec.Append( "e.stopPropagation();" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "if( node.data.isLazy ) {" )
		    strExec.Append( "var firstChild = node.getFirstChild();" )
		    strExec.Append( "try {" )
		    strExec.Append( "var firstChildSpan = window.GSjQuery(firstChild.span);" )
		    strExec.Append( "firstChildSpan.closest('li').css('display','none');" )
		    strExec.Append( "} catch(error) {}" )
		    strExec.Append( "}" )
		    strExec.Append( "if (typeof(node.data.itemStyle) !== 'undefined' && !node.isActive()) {" )
		    strExec.Append( "if (typeof(node.data.itemSelStyle) !== 'undefined') {node.removeClass(node.data.itemSelStyle);}" )
		    strExec.Append( "span.addClass(node.data.itemStyle);" )
		    strExec.Append( "}" )
		    strExec.Append( "if (typeof(node.data.itemSelStyle) !== 'undefined' && node.isActive()) {" )
		    strExec.Append( "if (typeof(node.data.itemStyle) !== 'undefined') {node.removeClass(node.data.itemStyle);}" )
		    strExec.Append( "node.addClass(node.data.itemSelStyle);" )
		    strExec.Append( "}" )
		    strExec.Append( "if (typeof(node.data.iconStyle) !== 'undefined') {" )
		    strExec.Append( "var nodeIcon = span.find('.fancytree-custom-icon');" )
		    strExec.Append( "if( !nodeIcon.hasClass(node.data.iconStyle) ) {" )
		    strExec.Append( "nodeIcon.addClass(node.data.iconStyle);" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    strExec.Append( "if (typeof(node.icon) == 'undefined' && !node.data.lazy) {" )
		    strExec.Append( "span.find('> span.fancytree-icon').css('display','none');" )
		    strExec.Append( "} else {" )
		    strExec.Append( "span.find('> span.fancytree-icon').css('display','block');" )
		    strExec.Append( "}" )
		    strExec.Append( "if (typeof(node.data.customStyle) !== 'undefined') {" )
		    strExec.Append( "span.find('> span.fancytree-title').addClass(node.data.customStyle);" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    
		    strExec.Append( "}).on('contextmenu', function (e) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'rightClick', [e.pageX, e.pageY]);" )
		    'strExec.Append( "e.preventDefault();" )
		    'strExec.Append( "e.stopPropagation();" )
		    strExec.Append( "});" )
		    
		    if isInit then
		      dim intMax as Integer = Items.Ubound
		      for intCycle as Integer = 0 to intMax
		        strExec.Append( AddItemJS( Items(intCycle) ) )
		      next
		    end if
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		    
		    Theme = mTheme
		    
		    isInit = True
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Close()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ContextClick(theItem as GraffitiWebTreeItem, mouseX as Integer, mouseY as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemChecked(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemClicked(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemCollapsed(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemDoubleClicked(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemEdited(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemExpanded(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemIconClicked(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemKeyPressed(theItem as GraffitiWebTreeItem, theKey as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemSelected(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemUnchecked(theItem as GraffitiWebTreeItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/mar10/fancytree
		
		Licensed under MIT.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAllowEdit
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAllowEdit = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AllowEdit As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mCheckboxes
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCheckboxes = value
			  
			  dim strExec() as String
			  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('option','checkbox'," + Str( mCheckboxes ).Lowercase + ");" )
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Private Checkboxes As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private ExecutionBuffer() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconCheckbox
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconCheckbox = value
			End Set
		#tag EndSetter
		IconCheckbox As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconCheckboxChecked
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconCheckboxChecked = value
			End Set
		#tag EndSetter
		IconCheckboxChecked As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconCheckboxUnknown
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconCheckboxUnknown = value
			End Set
		#tag EndSetter
		IconCheckboxUnknown As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconExpanderClosed
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconExpanderClosed = value
			End Set
		#tag EndSetter
		IconExpanderClosed As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconExpanderOpen
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconExpanderOpen = value
			End Set
		#tag EndSetter
		IconExpanderOpen As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIconStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconStyle = value
			  
			  UpdateIconStyle()
			End Set
		#tag EndSetter
		IconStyle As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isInit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Items() As GraffitiWebTreeItem
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mItemSelStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mItemSelStyle = value
			  
			  UpdateItemSelStyle()
			End Set
		#tag EndSetter
		ItemSelStyle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mItemStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mItemStyle = value
			  
			  UpdateItemStyles()
			End Set
		#tag EndSetter
		ItemStyle As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastChecked() As GraffitiWebTreeItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private LastStyle As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLockUpdate = value
			  
			  if not value then
			    UpdateItemStyles()
			    UpdateItemSelStyle()
			  end if
			End Set
		#tag EndSetter
		LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAllowEdit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCheckboxes As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconCheckbox As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconCheckboxChecked As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconCheckboxUnknown As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconExpanderClosed As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconExpanderOpen As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mItemSelStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mItemStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRightToLeft As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectedItem As GraffitiWebTreeItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTheme As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRightToLeft
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRightToLeft = value
			  
			  dim strOut() as String
			  strOut.Append( "var theContainer = window.GSjQuery('#" + me.ControlID + "').find('.fancytree-container');" )
			  if value then
			    strOut.Append( "theContainer.attr('DIR','RTL').addClass('fancytree-rtl');" )
			  else
			    strOut.Append( "theContainer.attr('DIR','LTR');" )
			    strOut.Append( "if (theContainer.hasClass('fancytree-rtl')) {" )
			    strOut.Append( "theContainer.removeClass('fancytree-rtl');" )
			    strOut.Append( "}" )
			  end if
			  
			  Buffer( strOut )
			End Set
		#tag EndSetter
		RightToLeft As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelectedItem
			End Get
		#tag EndGetter
		SelectedItem As GraffitiWebTreeItem
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTheme
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if value = "" then value = "default"
			  
			  dim strOut() as String
			  strOut.Append( "var theTree = window.GSjQuery('#" + me.ControlID + "');" )
			  
			  select case value
			  case "default"
			    strOut.Append( "if (theTree.hasClass('fancytree-base')) {" )
			    strOut.Append( "theTree.removeClass('fancytree-base');" )
			    strOut.Append( "}" )
			    strOut.Append( "theTree.fancytree('option','glyph',{map:{" )
			    strOut.Append( "checkbox: 'fa fa-fw " + EscapeString( mIconCheckbox ) + "'," )
			    strOut.Append( "checkboxSelected: 'fa fa-fw " + EscapeString( mIconCheckboxChecked ) + "'," )
			    strOut.Append( "checkboxUnknown: 'fa fa-fw " + EscapeString( mIconCheckboxUnknown ) + "'," )
			    strOut.Append( "expanderClosed: 'fa fa-fw " + EscapeString( mIconExpanderClosed ) + "'," )
			    strOut.Append( "expanderOpen: 'fa fa-fw " + EscapeString( mIconExpanderOpen ) + "'," )
			    strOut.Append( "folder: 'fa fa-fw " + EscapeString( mIconExpanderClosed ) + "'," )
			    strOut.Append( "folderOpen: 'fa fa-fw " + EscapeString( mIconExpanderOpen ) + "'," )
			    strOut.Append( "}});" )
			  case else
			    strOut.Append( "theTree.find('.fancytree-expander').each( function(index) {" )
			    strOut.Append( "window.GSjQuery(this).removeClass('" + IconExpanderOpen + "').removeClass('" + IconExpanderClosed + "');" )
			    strOut.Append( "});" )
			    strOut.Append( "window.GSjQuery('#" + me.ControlID + "').fancytree('option','glyph',{map:{" )
			    strOut.Append( "expanderClosed: 'fa fa-fw'," )
			    strOut.Append( "expanderOpen: 'fa fa-fw'," )
			    strOut.Append( "}});" )
			    strOut.Append( "if (!theTree.hasClass('fancytree-base')) {" )
			    strOut.Append( "window.GSjQuery('#" + me.ControlID + "').addClass('fancytree-base');" )
			    strOut.Append( "}" )
			  end select
			  
			  if mTheme <> "" then
			    strOut.Append( "if (theTree.hasClass('fancytree-" + mTheme + "')) {" )
			    strOut.Append( "window.GSjQuery('#" + me.ControlID + "').removeClass('fancytree-" + mTheme + "');" )
			    strOut.Append( "}" )
			  end if
			  
			  mTheme = value
			  
			  if mTheme <> "" then
			    strOut.Append( "if (!theTree.hasClass('fancytree-" + mTheme + "')) {" )
			    strOut.Append( "window.GSjQuery('#" + me.ControlID + "').addClass('fancytree-" + mTheme + "');" )
			    strOut.Append( "}" )
			  end if
			  
			  strOut.Append( "var rootNode = theTree.fancytree('getRootNode');" )
			  strOut.Append( "rootNode.render(true,true);" )
			  
			  
			  Buffer( strOut )
			End Set
		#tag EndSetter
		Theme As String
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.tree", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAS1BMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHImFnc+AAAAGHRSTlMAAwQNEBEbHDBMTpiam6CirbrV1+nx9fdURYfPAAABTklEQVR42u3aWY6DQAwE0AxZGQhLQxjf/6RzgF4SS3Y1ClWfyFE/QUA29OnEMPo8hlUSWYcHZPlmkmymHwCgsL7IBDj/Uoz/VRjLgMEdsJYBqztA3uQ4gE+PE0AAAQQQQAAB3ws4bkdUvSmt3pZXH0yqj2bl4bTBjOdjejwfMeN55naEv6UggAACCCCAAAL8AJe2C5s4ZAtde3m7/H0W18y3cr/3FPf0hb7xvAgg4Zxd/yWQvDKCZhFQQvoqPAWWPvn/F2BS98KMBMzx+leBJn4i/WIBbQTosYAuAixYQIgAf1jApn75ZJ39A6wbEgIIIIAAAggggAACCCBg1wDtPmHjeu0+YfN67cdo63rt53jzeu2GBPN67ZYM83rtGG1evxtA7ofuxwkggAACCCCAgN0AjtsRVW9Kq7fl1QeT6qOZep+wQ712n7B3PXOw/APQTjX4ylXGlAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAGFBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHJe7vx/AAAAB3RSTlMAJyjDxeTmPpvf9QAAADZJREFUCFtjYGBgMC9mgIACdnQGXIq9AIXBlF4OBKUCDKzlYBAAEseFgIZAENBYNOReAkUwAAACGhRCcoXG+AAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AllowEdit"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconCheckbox"
			Visible=true
			Group="Behavior"
			InitialValue="fa-square-o"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconCheckboxChecked"
			Visible=true
			Group="Behavior"
			InitialValue="fa-check-square-o"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconCheckboxUnknown"
			Visible=true
			Group="Behavior"
			InitialValue="fa-minus-square-o"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconExpanderClosed"
			Visible=true
			Group="Behavior"
			InitialValue="fa-caret-right"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconExpanderOpen"
			Visible=true
			Group="Behavior"
			InitialValue="fa-caret-down"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockUpdate"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RightToLeft"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Theme"
			Visible=true
			Group="Behavior"
			InitialValue="bootstrap"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
