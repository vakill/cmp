#tag Class
Protected Class GraffitiWebTreeItem
	#tag Method, Flags = &h0
		Sub Constructor(theCaption as String)
		  Caption = theCaption
		  
		  mUUID = GraffitiControlWrapper.GenerateUUID()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(theCaption as String, CheckboxValue as Boolean)
		  Caption = theCaption
		  Checkbox = True
		  CheckValue = CheckboxValue
		  
		  mUUID = GraffitiControlWrapper.GenerateUUID()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(theCaption as String, theIcon as String)
		  Caption = theCaption
		  Icon = theIcon
		  
		  mUUID = GraffitiControlWrapper.GenerateUUID()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(theCaption as String, theIcon as String, theTag as Variant)
		  Caption = theCaption
		  Icon = theIcon
		  Tag = theTag
		  
		  mUUID = GraffitiControlWrapper.GenerateUUID()
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Caption As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Checkbox As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		CheckValue As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Children() As GraffitiWebTreeItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Enabled As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Expanded As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		HasExpander As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		Icon As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Lazy As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mUUID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Parent As GraffitiWebTreeItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Style As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mUUID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		UUID As String
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Checkbox"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CheckValue"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Lazy"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UUID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasExpander"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Expanded"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
