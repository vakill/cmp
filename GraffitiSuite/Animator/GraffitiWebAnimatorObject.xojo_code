#tag Class
Protected Class GraffitiWebAnimatorObject
	#tag Method, Flags = &h0
		Function AddStyle(theStyle as WebStyle, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( theStyle ) then Return self
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = ".addClass('" + theStyle.Name + "', " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Blind(Operation as GraffitiWebAnimator.EffectOperation, Direction as GraffitiWebAnimator.EffectDirection, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('blind', {direction:'" + myAnimator.DirectionToString( Direction ) + "'}, " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Bounce(Operation as GraffitiWebAnimator.EffectOperation,  BounceDistance as Integer = 20, BounceCount as Integer = 3, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('bounce', {distance: " + Str( BounceDistance ) + ",times:'" + Str( BounceCount ) + "'}, " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Clip(Operation as GraffitiWebAnimator.EffectOperation, Direction as GraffitiWebAnimator.EffectDirection, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('clip', {direction: '" + Str( Direction ) + "'}, " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(theAnimator as GraffitiWebAnimator, theControl as WebControl)
		  myAnimator = theAnimator
		  myControl = theControl
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Drop(Operation as GraffitiWebAnimator.EffectOperation, Direction as GraffitiWebAnimator.EffectDirection, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('drop', {direction:'" + myAnimator.DirectionToString( Direction ) + "'}, " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Explode(Operation as GraffitiWebAnimator.EffectOperation, Pieces as Integer = 9, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('explode', {pieces:" + str( Pieces ) + "}, " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Fade(Operation as GraffitiWebAnimator.EffectOperation, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('fade', " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Fold(Operation as GraffitiWebAnimator.EffectOperation,  FoldedSize as Integer = 15, HorizontalFirst as Boolean = False, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('fold', " + _
		  "{size: " + Str( FoldedSize ) + ", horizFirst: " + if( HorizontalFirst, "true", "false" ) + "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Highlight(Operation as GraffitiWebAnimator.EffectOperation,  HighlightColor as Color = &cffff99, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  If IsNull( myControl ) then Return Self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('highlight', " + _
		  "{color: '#" + Right( Str( HighlightColor ), 6 ) + "'}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Puff(Operation as GraffitiWebAnimator.EffectOperation,  PuffSizePercent as Integer = 150, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('puff', " + _
		  "{percent: " + Str( PuffSizePercent ) + "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Pulsate(Operation as GraffitiWebAnimator.EffectOperation,  PulseCount as Integer = 5, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return Self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('pulsate', " + _
		  "{times: " + Str( PulseCount ) + "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RemoveStyle(theStyle as WebStyle, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( theStyle ) or IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = ".removeClass('" + theStyle.Name + "', " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Scale(Operation as GraffitiWebAnimator.EffectOperation, Direction as GraffitiWebAnimator.EffectScaleDirection = GraffitiWebAnimator.EffectScaleDirection.Both,  OriginX as GraffitiWebAnimator.EffectScaleOriginX = GraffitiWebAnimator.EffectScaleOriginX.Middle,  OriginY as GraffitiWebAnimator.EffectScaleOriginY = GraffitiWebAnimator.EffectScaleOriginY.Center,  ScaleType as GraffitiWebAnimator.EffectScaleType = GraffitiWebAnimator.EffectScaleType.Both, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strDirection as String
		  select case Direction
		  case GraffitiWebAnimator.EffectScaleDirection.Both
		    strDirection = "both"
		  case GraffitiWebAnimator.EffectScaleDirection.Horizontal
		    strDirection = "horizontal"
		  case else
		    strDirection = "vertical"
		  end select
		  
		  dim strOriginX as String
		  Select case OriginX
		  case GraffitiWebAnimator.EffectScaleOriginX.Left
		    strOriginX = "left"
		  case GraffitiWebAnimator.EffectScaleOriginX.Middle
		    strOriginX = "middle"
		  case else
		    strOriginX = "right"
		  End Select
		  
		  dim strOriginY as String
		  select case OriginY
		  case GraffitiWebAnimator.EffectScaleOriginY.Bottom
		    strOriginY = "bottom"
		  case GraffitiWebAnimator.EffectScaleOriginY.Center
		    strOriginY = "center"
		  case else
		    strOriginY = "top"
		  end select
		  
		  dim strScaleType as String
		  select case ScaleType
		  case GraffitiWebAnimator.EffectScaleType.Both
		    strScaleType = "both"
		  case GraffitiWebAnimator.EffectScaleType.Box
		    strScaleType = "box"
		  case else
		    strScaleType = "content"
		  end select
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('scale', {" + _
		  "direction: '" + strDirection + "', " + _
		  "origin: ['" + strOriginX + "', '" + strOriginY + "']," + _
		  "scale: '" + strScaleType + "'" + _
		  "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Shake(Operation as GraffitiWebAnimator.EffectOperation, Direction as GraffitiWebAnimator.EffectDirection = GraffitiWebAnimator.EffectDirection.Left, Distance as Integer = 20, Times as Integer = 3, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return Self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('shake', {" + _
		  "direction:'" + myAnimator.DirectionToString( Direction ) + _
		  "distance: " + Str( Distance ) + "," + _
		  "times: " + Str( Times ) + _
		  "'}, " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Size(Operation as GraffitiWebAnimator.EffectOperation, ToWidth as Integer, ToHeight as Integer, OriginX as GraffitiWebAnimator.EffectScaleOriginX = GraffitiWebAnimator.EffectScaleOriginX.Left, OriginY as GraffitiWebAnimator.EffectScaleOriginY = GraffitiWebAnimator.EffectScaleOriginY.Top, ScaleType as GraffitiWebAnimator.EffectScaleType = GraffitiWebAnimator.EffectScaleType.Both, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strOriginX as String
		  Select case OriginX
		  case GraffitiWebAnimator.EffectScaleOriginX.Left
		    strOriginX = "left"
		  case GraffitiWebAnimator.EffectScaleOriginX.Middle
		    strOriginX = "middle"
		  case else
		    strOriginX = "right"
		  End Select
		  
		  dim strOriginY as String
		  select case OriginY
		  case GraffitiWebAnimator.EffectScaleOriginY.Bottom
		    strOriginY = "bottom"
		  case GraffitiWebAnimator.EffectScaleOriginY.Center
		    strOriginY = "center"
		  case else
		    strOriginY = "top"
		  end select
		  
		  dim strScaleType as String
		  select case ScaleType
		  case GraffitiWebAnimator.EffectScaleType.Both
		    strScaleType = "both"
		  case GraffitiWebAnimator.EffectScaleType.Box
		    strScaleType = "box"
		  case else
		    strScaleType = "content"
		  end select
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('size', {" + _
		  "to: {width: " + str( ToWidth ) + ", height: " + Str( ToHeight ) + "}, " + _
		  "origin: ['" + strOriginX + "', '" + strOriginY + "']," + _
		  "scale: '" + strScaleType + "'" + _
		  "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Slide(Operation as GraffitiWebAnimator.EffectOperation, Direction as GraffitiWebAnimator.EffectDirection, Distance as Integer, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = "." + myAnimator.OperationToString( Operation ) + "('slide', {" + _
		  "direction: '" + myAnimator.DirectionToString( Direction ) + "', " + _
		  "distance: '" + Str( Distance ) + "'" + _
		  "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SwitchStyle(removeStyle as WebStyle, addStyle as WebStyle, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( addStyle ) or IsNull( myControl ) or IsNull( removeStyle ) then Return Self
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = ".switchClass('" + removeStyle.Name + "', '" + addStyle.Name + "', " + Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TransferTo(Target as WebControl, Duration as Integer = 1000) As GraffitiWebAnimatorObject
		  if IsNull( myControl ) or IsNull( Target ) then Return self
		  
		  dim strObject as String = "window.GSjQuery('#" + myControl.ControlID + "')"
		  dim strCommand as String = ".effect('transfer', {" + _
		  "to: window.GSjQuery('#" + Target.ControlID + "')" + _
		  "}," + _
		  Str( Duration ) + ")"
		  
		  myAnimator.Animate( strObject, strCommand )
		  
		  Return self
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private myAnimator As GraffitiWebAnimator
	#tag EndProperty

	#tag Property, Flags = &h21
		Private myControl As WebControl
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
