#tag Class
Protected Class GraffitiWebAnimator
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Animate(strObject as String, strCommand as String)
		  Buffer( strObject + strCommand + ";" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DirectionToString(theDir as EffectDirection) As String
		  select case theDir
		  case EffectDirection.Down
		    Return "down"
		  case EffectDirection.Horizontal
		    Return "horizontal"
		  case EffectDirection.Left
		    Return "left"
		  case EffectDirection.Right
		    Return "right"
		  case EffectDirection.Up
		    Return "up"
		  case EffectDirection.Vertical
		    Return "vertical"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EffectColorPropertyToString(effectCP as EffectColorProperty) As String
		  select case effectCP
		  case EffectColorProperty.BackgroundColor
		    Return "backgroundColor"
		  case EffectColorProperty.BorderBottomColor
		    return "borderBottomColor"
		  case EffectColorProperty.BorderLeftColor
		    Return "borderLeftColor"
		  case EffectColorProperty.BorderRightColor
		    Return "borderRightColor"
		  case EffectColorProperty.BorderTopColor
		    Return "borderTopColor"
		  case EffectColorProperty.ForeColor
		    Return "color"
		  case EffectColorProperty.ColumnRuleColor
		    Return "columnRuleColor"
		  case EffectColorProperty.OutlineColor
		    Return "outlineColor"
		  case EffectColorProperty.TextDecorationColor
		    Return "textDecorationColor"
		  case EffectColorProperty.TextEmphasisColor
		    Return "textEmphasisColor"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebAnimator -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddjQueryUI( CurrentSession ) )
		    
		    if not IsLibraryRegistered( CurrentSession, "graffitisuite", "jquery.easing.js" ) then
		      try
		        
		        dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		        if strOffload.LenB > 0 then
		          strOut.Append( "<script src='" + strOffload + "/jQuery/jquery.easing.js'></script>" )
		        else
		          dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "jQuery" ).Child( "jquery.easing.js" )
		          dim jsFile as WebFile = WebFile.Open( theFile, False )
		          strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		          if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		          Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		        end if
		        
		        RegisterLibrary( CurrentSession, "graffitisuite", "jquery.easing.js" )
		        
		      catch
		        
		      end try
		    end if
		  end if
		  
		  strOut.Append( "<style>.ui-effects-transfer {border: 1px dotted black;}</style>" )
		  
		  strOut.Append( "<!-- END GraffitiWebAnimator -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function NewAnimatedObject(theControl as WebControl) As GraffitiWebAnimatorObject
		  Return new GraffitiWebAnimatorObject( self, theControl )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function OperationToString(theOp as EffectOperation) As String
		  select case theOp
		  case EffectOperation.Hide
		    Return "hide"
		  case EffectOperation.Show
		    Return "show"
		  case EffectOperation.Toggle
		    Return "toggle"
		  case else
		    Return "effect"
		  end select
		End Function
	#tag EndMethod


	#tag Note, Name = Easing CheatSheet
		
		http://easings.net/
	#tag EndNote

	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		
		Copyright jQuery Foundation and other contributors, https://jquery.org/
		
		This software consists of voluntary contributions made by many
		individuals. For exact contribution history, see the revision history
		available at https://github.com/jquery/jquery-ui
		
		The following license applies to all parts of this software except as
		documented below:
		
		====
		
		Permission is hereby granted, free of charge, to any person obtaining
		a copy of this software and associated documentation files (the
		"Software"), to deal in the Software without restriction, including
		without limitation the rights to use, copy, modify, merge, publish,
		distribute, sublicense, and/or sell copies of the Software, and to
		permit persons to whom the Software is furnished to do so, subject to
		the following conditions:
		
		The above copyright notice and this permission notice shall be
		included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
		EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
		MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
		NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
		LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
		OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
		WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
		
		====
		
		Copyright and related rights for sample code are waived via CC0. Sample
		code is defined as all source code contained within the demos directory.
		
		CC0: http://creativecommons.org/publicdomain/zero/1.0/
		
		====
		
		All files located in the node_modules and external directories are
		externally maintained libraries used by this software which have their
		own licenses; we recommend you read them, as their terms may differ from
		the terms above.
		jQuery UI
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean = False
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.animator", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACx\rjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAu2SURBVHhe7d17cFTlFQDw79zdPHZDQGgVW9Gh\rpSg6I1Zti1bEirRakM0GEmdKLf2jah0VyCYwarTSVq12GnZDtA9staWOWCY8dkE6YAWhQss4tqPO\riDCt1o4yI9OqJSG7gex+p+cmJ4ogJHv3Ppfzm2H2O2c34T7O/e53s/ehhBBCCCGEEEIIIYQQQggh\rhBBCCCGEEEKUC+DXU0djY2hM95k1vbVV0UIhHw2FwtlsrqtLPfNYlj9xSinbAqiMN58bRrwSDeML\rgDgRlZoIqCbSHEf4I8fqQ4UHFcIBWij/VArfoJ95w1Cwr6fy7e2qs7PAnysrZVMAY65bMLK3qmIO\rAl5FM3UVzdrn+K3SYX9xbAf6p5WxvTeT/Au/E3jBLgDqzquPnN1gADbQrNA/d1DP8BotuKcLBVh1\reOOyf3E6kBwrgGisaZYC4xLacrpRwVraat7mt0pWU99yhkZ9B03+ApqB0zjtCUS1GVA/mt3QvolT\rgWJ/AdBWGekbtwUUXMMZcym9g6Dm5tKpFzljSeWchRPDOrSAJptWvs96L8SdaEBHbn2ykzOBYPtC\rjMabl9PLwoHoI9QTfEA9Qaw3ndzJqeG75ZaKyIGapQBwD2f87CUA1dqzPvknjn3NgQJI7Kdf+1kO\rj4E9AEasZ/2ybZwYUrSu+SYFeB/9zrM5FRQrwmFs7VqTep9jX3KiB6Ax0slgnrbkmUNtIZH6BeNA\rV3TQFNZzKnCo1/svHTkszqbbV3LKdwx+dRHQ4bl6lrbsazlxnJq6xDylK14J8so30Tjo07SIf0fz\r+ku1dKkHy3po3k0UqM3RusRMjj5EPUgKAZ6i/egYTgUfqFsjL3f/vbqu+auc8Q1vqxJgE63w6/vb\rtIVQQaylVlN/XGaooC8yQO2KxFvmcMoXPBgDfAJUN9P+soXGBpM4U+6+l00nn+C2p/yxXwL161No\r5Zser443N3PbU74cmJwKaMEvi9Qlvs+hZ/yxC3AD4r9pbt9VCO+ar4j6gAKDYqyi3mcUvY6k/Odp\r4s+lhXIuLZow/6SjNOC3eten/sCh68q5AA7TWt9Ch5xbNIS2HE63vcH5oTU2hqJ9Z12D5p+zEa41\rB3D8jjMAv5ldn9rMkavKsABwtdL4ZLZq/2a7vsOvnt10hRFS86gYbqLhfCWn7bQfCvlLezZ2HODY\rNeVUAGsRsC23PrWbY9vVzF44FkPhO6mZGMjYCDGTzaTiHLkm+AWAKqNBtVn6ksmiqvjiCQbqO2nX\rcDOn7HIvHR4+yG1XBLoAUOvm3Ib2FIeui9YnLkINKSqEqzlVMlQ4pdSvzYsRyMNA8yQT6jJnebny\rTTRweyWXSU6n6fkNp0qH6gFuuSJwBUCj+ldCvfkJtL/8I6c8R1usuSu4eyAqDR2Sfr3/K3CXBK8H\rMNR9hzY/8h+OfIP23Q8jqBuo2TuQsY5+zwOfii2p5dBRgSsAoEFfNN70DQ59pf90MMS5HFpGA7Ox\rOSO/iENHBXIMQJO9paau5Wsc+Iq5a6Ld1HGnxBWr/3c0NoY4dExAC8DsJvH56ljLNA59hQaGj9Bg\rroNDS2gscHokf07JhTSUwBaAyTBwR3W8eSqHvpLNJBfR8XBJp4oDohTAUGgGXvDjmTamsJE3/2JY\ryjWH4yOxhKOnxQW+AEw0E89F6hOXcegb3es6/kEvPxmIrAFDzeOmI8qiAGjYHKFBwaZIPPEVzvhG\rNr2MCgBLuCoKGkY0LDidA9uVRwEQOnQaQ0WQrr7+znM45RO0J1dGSb1AIV/hWC9QNgVgAlCfgXDf\rDzn0jVx62a9oQPgyh0UDhR9dZmezsiqAfqgu4ZavAOokN4uGChw70rG9AFDhPm56xfKW5qRs5f5V\rtHS6OSwK7d5GR+Mtl3JoK/t7AK1+wS1P0Aw9yU1/6ews0GDgaY6Khqiv5KatbC+A3IZUB3UDXl3F\re3dPJrmV2/5DRyrcsuKL/Gor208I+VBsSW0U8lciGNWccYwB2A2hqpcPrXnId98SHm3UrLtG91Uc\rsXa1MKrd2Uzyco5s41wBiE8UqWv+Gx2tFD1QpaOID3LppO3XS5bfUYD/7eHXopgDwUhsyQnuu2Cd\rFIDLAND887AlCIXx3LSNFIDLQIHlAjDMK5hsJgXgMlT6PW4WTYOWAgg61KGD3CwaaCUFEHShMFov\rACW7gMArIFg+2xdBWfpT8slIAbjMKBQsb8UGWu89TkQKwGXagBPeHW0oCNDFTdtIAbgMlLJ840iN\rWnqAoMP+u49YQ0cQ73DTNlIALqMe4HxuFgmzTtyaXgrATY23jaASmMJRsV7nV1tJAbgomq+yfGoX\rotrLTVtJAbhJq+u4VTRQ8Co3bSUF4CJUYPn0bq2UI7fAkQJwSSTW0gCgLF7ggb1OPahKCsAlANry\rTbDp0HEXN20nBeCCaF3Td6gCruCwaLT/f4GbtpMCcAHt+1u5aUneCK3ipu2kABwWqUs0lXIndFT4\r3JF1P7N8FtFQpACc1JAwH1Nb0tYPWjm29Zt8WwAjYi0X1NS3TOcwkCJ98Jh5qxcOi4fqUPbg+5av\rJhoOXxYAdZtztYGvIeLWSLw5EM/fO1Y0nriXDvtu5NASBOxQO1aWfNu5k/FdAZgrn7aaNRyaX57M\roCJ4isNAiNQ3N9KU38+hNYjaKBRKutHUcDh+G7JiHLvyB1ERXBieNKU2v3f3s5zyrZrZCyfTdrWJ\rJrqCU9YALM9uaDcfouUo3/QAJ1r5g0AZLfSZhzj0pUis6Us6FF5FE2sO/kqBhQI4vvWbfNEDDLXy\rB9FnplZMmnJG397dvrlP8KCa+uZv04p/BhScyalStPZuSD7DbUdR7+qt4a78j0G1TlUat2c7297l\rjKci8cSPacX/gMNSvZRNJ7/Mbcd52gNYWvkmUOej1jdWnHf5W/l9ux05UWI4ojMXn1lxwWWP0zzc\rxqmSgdbf7du3+00OHedZD2B55R+DBsu/1Tp0vxOnS53QVUvD0dHd99B/3kr7JdueIYQK23Lp1BIO\rXeFNATQkItE+9T4tPNtuHoGID1bmq5Yd3PTwB5xyBB3i3Q7mij/hI/It20Vdv+u3vfWkAKrntEwz\rNO7g0D6IR2iOVkABV/RsbH+NsyUbVbfotCPKmEcFe6t5SMpp+1A3Bjp0cc/GNkfO+jkZD3cB1u6U\rMWyodqGhtlLPsK03nfwzzSryO8NSG1t0XgGMqQgwnRaSo7drpWm8NZdJreDQVZ4VQFXdovEhMJ6n\rSbD9pgfHQfOSPHxVIbxKM7zHvMbOUNitAbpovxs10BiplB5F7w8+OfQ8WjJn8U87C/GubCb1U45c\r51kBmKL1TZdQ57eNBoO2X/UaBIjqoVwmWdK3haXytABMNbHFM9DQgfzCpySIP6ct/w6OPOP5n4J7\rNrQ9RwtjFv07xKmyZx6x+GHlmzzvAQb13woV9WoaaU/gVHlCtSSbSbZx5DnfFIApUr9gHGB4NU2W\rL58AUqIsDTRvy6bbV3LsC776Oji/98WuvmmTnwwfqhxLh4iO3BzZC3SksbWgINabTm3jlG/4qgc4\r2sCFFJiiKRzHqUCikf4DNNK364si2/mqBzhaft9f94yYPO2JvNZjqUov5nRg0Fa/HZWe15tp/z2n\rfMm3PcDRzJNDNepWUODYkzPsQivevJFTay6denQg42+BKIBB5m5BAd7t6J+QLaKu/j3aZXVUoO44\rmFn+P077XqAKYFB1vOkGUMZ8mvhZnPIQvk2Hrh3ZcG2H6vzREU4GRiALYFDlnIUTQ4XwfOoR5lPo\r7tPCEJ+mFb8qm3bn1C2nBLoAjmY+MxAQZyAYM2imrua0fVC9iUrtpG5+J3XznUHq5k+mbArgYxqX\rjojmu6YqNC5ExMm09iYDgPk9/rDml35mL33+dfO2LAbgnjzqnYczy9/it8tKeRbAJ2lsDEUOjx8b\rVqo2H9IjDXrVWKilBZAzb+AcMvJdBV15MFf11gHzAU/8U0IIIYQQQgghhBBCCCGEEEIIIYQQQggh\rhBABoNT/AWm4wH83Ej97AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEjSURBVDhP3ZI9TgMxEIXnGYrsInIC4BQ00NARCgRuID2IlGg3iFugKD0/PaEJFRISJeIiXIAtIioPM2ZkdtlIiAqJT7I8O16/ebaH/hzY3CDbK9YdsBUIXSA8z6bjR1tqkQQyP3xjCicgHMnoWfoT5lcCH88TcjarUteRu42bmV5k1yBQ6DPxFQErTO4u98W2/Z5IDnI/ZAuF0KtX6/jiUMVVeHY/2rR0JDmIVW0w4cCykffpeCL5iZTb0Pux9O/IfTlSl+rGUpEvBz8g51u2sEFLYJ7Fzu7pGph2RKSKx6nREMh8eUnOPdVtqiAWFx70JUB8belEo5GW9sszBi401mpgrnRjXJRLlBfox7hGqxNjRbhzCVf1m8GV9MLNd+v/BqIP+RFha84/7GgAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag Enum, Name = EffectColorProperty, Type = Integer, Flags = &h0
		BackgroundColor
		  BorderBottomColor
		  BorderLeftColor
		  BorderRightColor
		  BorderTopColor
		  ForeColor
		  ColumnRuleColor
		  TextDecorationColor
		  TextEmphasisColor
		OutlineColor
	#tag EndEnum

	#tag Enum, Name = EffectDirection, Type = Integer, Flags = &h0
		Up
		  Down
		  Left
		  Right
		  Vertical
		Horizontal
	#tag EndEnum

	#tag Enum, Name = EffectOperation, Type = Integer, Flags = &h0
		Show
		  Hide
		  Toggle
		Apply
	#tag EndEnum

	#tag Enum, Name = EffectScaleDirection, Type = Integer, Flags = &h0
		Both
		  Vertical
		Horizontal
	#tag EndEnum

	#tag Enum, Name = EffectScaleOriginX, Type = Integer, Flags = &h0
		Left
		  Right
		Middle
	#tag EndEnum

	#tag Enum, Name = EffectScaleOriginY, Type = Integer, Flags = &h0
		Bottom
		  Top
		Center
	#tag EndEnum

	#tag Enum, Name = EffectScaleType, Type = Integer, Flags = &h0
		Both
		  Box
		Content
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
