#tag Class
Protected Class GraffitiWebNotifier
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case name
		    case "LibrariesLoaded"
		      if UBound( Parameters ) >= 0 then
		        mSupported = Parameters(0).BooleanValue
		      end if
		      
		      LibrariesLoaded = True
		    case "onClick"
		      if UBound( Parameters ) >= 0 then
		        NotificationClicked( Parameters(0).StringValue )
		      end if
		    case "onError"
		      if UBound( Parameters ) >= 0 then
		        NotificationError( Parameters(0).StringValue )
		      end if
		    case "onAccessChange"
		      if UBound( Parameters ) >= 0 then
		        mCanNotify = Parameters(0).BooleanValue
		        AccessChanged()
		      end if
		      isReady = True
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  //
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  //
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  InitClass()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CloseAll()
		  Buffer( "Push.clear();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseNotification(notificationName as String)
		  if notificationName.Len = 0 then
		    CloseAll()
		  else
		    Buffer( "Push.close('" + EscapeString( notificationName ) + "');" )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  InitClass()
		  
		  hasLoaded = True
		  LibrariesLoaded = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebNotifier -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "push" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "push.min.js" ), "graffitisuite" ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebNotifier -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub InitClass()
		  dim strExec() as String
		  
		  strExec.Append( "Push.Permission.request(" )
		  strExec.Append( "function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onAccessChange', [Push.Permission.has()]);" )
		  strExec.Append( "}," )
		  strExec.Append( "function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onAccessChange', [Push.Permission.has()]);" )
		  strExec.Append( "}" )
		  strExec.Append( ");" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowNotification(notificationName as String, Title as String, Body as String, CloseDelay as Integer = 0, Icon as String = "", Silent as Boolean = True, ParamArray Vibrations as Integer)
		  #Pragma Unused silent
		  
		  dim strExec() as String
		  
		  strExec.Append( "Push.create('" + EscapeString( Title ) + "', {" )
		  if Body.Len > 0 then strExec.Append( "body: '" + EscapeString( Body ) + "'," )
		  if Icon.Len > 0 then strExec.Append( "icon: '" + EscapeString( Icon ) + "'," )
		  if CloseDelay > 0 then
		    strExec.Append( "timeout: " + Str( CloseDelay ) + "," )
		  else
		    strExec.Append( "requireInteraction: true," )
		  end if
		  strExec.Append( "tag: '" + EscapeString( notificationName ) + "'," )
		  
		  dim strVibes() as String
		  for each theTime as Integer in Vibrations
		    strVibes.Append( Str( theTime ) )
		  next
		  if strVibes.Ubound >= 0 then
		    strExec.Append( "vibrate: [" + Join( strVibes, "," ) + "]," )
		  end if
		  
		  strExec.Append( "onClick: function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onClick', ['" + EscapeString( notificationName ) + "']);" )
		  strExec.Append( "window.focus();" )
		  strExec.Append( "this.close();" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "onError: function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onError', ['" + EscapeString( notificationName ) + "']);" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "});" )
		  
		  Buffer( strExec )
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event AccessChanged()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event NotificationClicked(notificationName as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event NotificationError(notificationName as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		Source URL: https://pushjs.org/
		
		The MIT License (MIT)
		Copyright (c) 2016 Tyler Nickerson
		
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  if not Supported then Return false
			  
			  return mCanNotify
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		CanNotify As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return misReady
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  misReady = value
			  
			  if value and Supported and LibrariesLoaded then
			    RunBuffer()
			  end if
			End Set
		#tag EndSetter
		Private isReady As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  if value and Supported and isReady then
			    RunBuffer()
			  end if
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCanNotify As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private misReady As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSupported As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSupported
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		Supported As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.notifier", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAQDSURBVHhe7d2xixxlGMfx933nSLhrxNZOgqBYpEmTwjqNyKqI+BcIRoUVQTSVlaDs3oHEylZFCZfdSypJlUZBECuTQsHCYGMjmJ0j5ObxfXefFME7czf7vrszPt9Ps+/z3h177PObeWdnZ3cdAAAAAAAAAAAAAAAAAOD/wuttUadeevspHeKY7t17/I67/uFMy2KKBWDrxeFZETdy4s557x/TaZyAOPnZ+fB5fXW0rVPZFQnA5mD4pnf+Uy2xpLghfVtPxxe0zKrS22zS7r6ScC1mK+gUluS9O7Px9PnN+7e/u6FT2WRvUnWwcSn+yxtaIpO4q35v84WLT2iZTf6t1LtzOkJ2p7M/ttkDEJP6rA6RmffyjA6zYZ3uk/h0SkfZEIBeEQKAvAhAnwhLgG2S/8QdAeiTAt0iAL3CQaBtHAMgNwLQJ54lwDiWANNE2AMgMwLQKywBtnEQiNwIQJ9wIsg6lgDbPK8GGpe/XQSgTzgRZB0BMI4lwDZOBFlHAJAZAegTzgQaxzEAciMAxhEA4wiAcQTAOAJgHAEwjgAYRwCMIwDGEQDjCIBxBMA4AmAcATCOABhHAIwjAMYRAOMIgHEEwDgCYBwBMI4AGJf9jQZbg3dEh4bI5bgt/SCuuRUO5K5O5hF8kBA31KbyEsKf9dWPf9efZEEAliDi/nCueb2e7lzTqd5hCVhCTPqwz81PCEBbIl/tT8dfa9VbBKA1f0sHvUYAWhIhALZ5ua+jXuNZQEvi/Mv1ZLSr5UO2BsP34zOE5+LD6+fv6Z9/sIO+t3/+9a9pLn3s42I8//zHeZ1+J5ZHfBxcI+HS/t7oppZZHHpHyzATgKZ5pd7buaLlQzYHw09iN9/VMpvZZJy9XywBbVUhezP+S9yqvtRhVgSgJWlW/dg13+ggKwLQVjh8nS5BRP6qJzt7WmZFAFqqVrgHiHf0RrwpcmxFANryq3rsms/uTreLrP8JAeiwuMn/NJvsXNSyCALQVeJ+rYJ7TatiCEAXxeaHyj3/9+74ts4UQwC6ZoXNTwhAEekUbwsrbn5CAArwbb7aZQ3NTwhACfNXd05gTc1PCEARJ1gCUvMP1tP8hACUcNwl4EHzr6+n+QkBKOIYe4AOND8pEYBf9NauR32xQ0ean+QPgMiPOsJhxH3fleYn2QPQVOGyDu06+rt9RrPp+HxXmp9kD8D+7uimiPtIS6P+tQT8lq4hnE3G2S8TW1aRg8B6Ov6gcc2rcXhnMWPM/GLPuBg6+SLuEy/Exj951AWk6/boo9UlnR68dSa4U2e17A5pFlff6rV9cbio05U+cVw9eCoXFvMi8ffS38yvBKqcbDQ36ivbNgMOAAAAAAAAAAAAAAAAAAA6xrl/AIfBH+IkOJK1AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACXSURBVDhPYxhwwAilGbj9C4v/MzJIQ7k4wb8/fyf82DLpEZSLMIAroOg/lIkX/GP4F/5jw4RVUC4DE5QmG4waQLQB//thmPEfwz2oIBgQNuA/w3EgsYPhP6MFGKMBolzwj4FBAJhiLP8z/r/6fdOEM1BhMGCG0gwsGhaWjP8ZXwOZT5AxSBPQlnNA9pvvG/pzgPTwAgwMANalMPbYp7uCAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="CanNotify"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Supported"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
