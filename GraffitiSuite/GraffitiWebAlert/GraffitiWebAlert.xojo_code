#tag Class
Protected Class GraffitiWebAlert
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "buttonClick"
		      if Parameters.Ubound < 1 then Return true
		      
		      dim messageName as String = Parameters(0)
		      dim buttonName as String = Parameters(1)
		      
		      dim theMessage as GraffitiWebAlertMessage
		      dim theButton as GraffitiWebAlertButton
		      if GetObjects( messageName, buttonName, theMessage, theButton ) then
		        ButtonClick_O( theMessage, theButton )
		      end if
		      ButtonClick( messageName, buttonName )
		      
		      RemoveMessage( messageName )
		    case "onClose"
		      if Parameters.Ubound < 0 then Return True
		      
		      dim messageName as String = Parameters(0)
		      dim theMessage as GraffitiWebAlertMessage = GetMessage( messageName )
		      
		      if not IsNull( theMessage ) then
		        MessageClosed_O( theMessage )
		      end if
		      MessageClosed( messageName )
		      
		      RemoveMessage( messageName )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function ButtonsToString(m as GraffitiWebAlertMessage) As String
		  if IsNull( m ) then Return""
		  
		  dim strExec() as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = m.Buttons.Ubound
		  dim currButton as GraffitiWebAlertButton
		  for intCycle = 0 to intMax
		    currButton = m.Buttons(intCycle)
		    strExec.Append( "Noty.button('" + EscapeString( if( currButton.Icon.Len > 0, "<i class='" + currButton.Icon + "'></i> ", "" ) + currButton.Caption ) + "', '" )
		    if not IsNull( currButton.Style ) then
		      strExec.Append( currButton.Style.Name )
		    end if
		    strExec.Append( "', function(n) {" )
		    strExec.Append( "Xojo.triggerServerEvent( '" + me.ControlID + "', 'buttonClick', ['" + EscapeString( m.Name ) + "','" + EscapeString( currButton.Name ) + "', '" + EscapeString( currButton.Caption ) + "']);" )
		    strExec.Append( "n.close();" )
		    strExec.Append( "})" )
		    if intCycle < intMax then strExec.Append( "," )
		  Next
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseAll()
		  Buffer( "Noty.closeAll();" )
		  
		  ReDim Messages(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseMessage(theMessage as GraffitiWebAlertMessage)
		  CloseMessage( theMessage.Name )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseMessage(messageName as String)
		  dim strExec() as String
		  strExec.Append( "if (typeof(window.gwNoty_" + SafifyString( messageName ) + ") != 'undefined') {" )
		  strExec.Append( "window.gwNoty_" + SafifyString( messageName ) + ".close()" )
		  strExec.Append( "};" )
		  Buffer( strExec )
		  
		  RemoveMessage( messageName )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  hasLoaded = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetButtonFromMessage(theMessage as GraffitiWebAlertMessage, buttonName as String) As GraffitiWebAlertButton
		  if IsNull( theMessage ) then Return nil
		  
		  dim intButtonMax as Integer = theMessage.Buttons.Ubound
		  dim currButton as GraffitiWebAlertButton
		  
		  for intButton as Integer = 0 to intButtonMax
		    currButton = theMessage.Buttons(intButton)
		    if currButton.Name = buttonName then Return currButton
		  next
		  
		  Return Nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetMessage(messageName as String) As GraffitiWebAlertMessage
		  dim intMax as Integer = messages.Ubound
		  
		  dim currMessage as GraffitiWebAlertMessage
		  
		  for intCycle as Integer = 0 to intMax
		    currMessage = Messages( intCycle )
		    if currMessage.Name = messageName then Return currMessage
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetObjects(messageName as String, buttonName as String, byRef theMessage as GraffitiWebAlertMessage, byRef theButton as GraffitiWebAlertButton) As Boolean
		  theMessage = GetMessage( messageName )
		  if not IsNull( theMessage ) then theButton = GetButtonFromMessage( theMessage, buttonName )
		  
		  if IsNull( theMessage ) or IsNull( theButton ) then Return False
		  Return True
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebAlert -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "noty" ).Child( "noty.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "noty" ).Child( "noty.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "noty" ).Child( "animate.css" ), JavascriptNamespace ) )
		    dim themesDir as FolderItem = fScripts.Child( "noty" ).Child( "themes" )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "bootstrap-v3.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "bootstrap-v4.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "light.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "metroui.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "mint.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "nest.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "relax.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "semanticui.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, themesDir.Child( "sunset.css" ), JavascriptNamespace ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebAlert -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function PositionToString(thePosition as MessagePositions) As String
		  Select case thePosition
		  case MessagePositions.Bottom
		    Return "bottom"
		  case MessagePositions.BottomCenter
		    Return "bottomCenter"
		  case MessagePositions.BottomLeft
		    Return "bottomLeft"
		  case MessagePositions.BottomRight
		    Return "bottomRight"
		  case MessagePositions.CenterLeft
		    Return "centerLeft"
		  case MessagePositions.CenterRight
		    Return "centerRight"
		  case MessagePositions.Top
		    Return "top"
		  case MessagePositions.TopCenter
		    Return "topCenter"
		  case MessagePositions.TopLeft
		    Return "topLeft"
		  case MessagePositions.TopRight
		    Return "topRight"
		  case else
		    Return "center"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RemoveMessage(messageName as String)
		  dim intMax as Integer = messages.Ubound
		  dim currMessage as GraffitiWebAlertMessage
		  
		  for intCycle as Integer = intMax DownTo 0
		    currMessage = Messages(intCycle)
		    if currMessage.Name = messageName then Messages.Remove( intCycle )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SafifyString(theString as String) As String
		  Dim reg as new RegEx
		  reg.searchPattern = "[^a-zA-Z0-9]"
		  reg.replacementPattern = ""
		  reg.Options.ReplaceAllMatches = True
		  
		  return reg.replace( theString )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Show(theMessage as GraffitiWebAlertMessage)
		  if IsNull( theMessage ) then Return
		  
		  dim strType, strLayout, strClose as String
		  
		  dim strExec() as String
		  strExec.Append( "window.gwNoty_" + SafifyString( theMessage.Name ) + " = new Noty({" )
		  strExec.Append( "id: 'gwNoty_message_" + SafifyString( theMessage.Name ) + "'," )
		  strExec.Append( "queue: '" + SafifyString( theMessage.Name ) + "'," )
		  
		  'if IsNull( theMessage.Style ) then
		  strExec.Append( "theme: '" + ThemeToString( theMessage.Theme ) + "'," )
		  'else
		  'strExec.Append( "theme: '" + EscapeString( theMessage.Style.Name ) + "'," )
		  'end if
		  
		  if theMessage.Icon.Len = 0 then
		    strExec.Append( "text: '" + EscapeString( ReplaceLineEndings( theMessage.Content, "<br />" ) ) + "'," )
		  else
		    strExec.Append( "text: '" + EscapeString( "<i class='" + theMessage.Icon + "'></i> " + ReplaceLineEndings( theMessage.Content, "<br />" ) ) + "'," )
		  end if
		  
		  strExec.Append( "type: '" + TypeToString( theMessage.Type ) + "'," )
		  strExec.Append( "layout: '" + PositionToString( theMessage.Position ) + "'," )
		  strExec.Append( "timeout: " + if( theMessage.Timeout > 0, Str( theMessage.Timeout, "#" ), "false" ) + "," )
		  strExec.Append( "modal: " + Str( theMessage.Modal ).Lowercase + "," )
		  
		  if theMessage.Buttons.Ubound >= 0 then
		    strExec.Append( "buttons: [" + ButtonsToString( theMessage ) + "]," )
		  else
		    strExec.Append( "buttons: false," )
		  end if
		  
		  if theMessage.CloseButton then
		    strExec.Append( "closeWith: ['button']," )
		  elseif theMessage.Buttons.Ubound < 0 then
		    strExec.Append( "closeWith: ['click']," )
		  else
		    strExec.Append( "closeWith: false," )
		  end if
		  strExec.Append( "})" )
		  
		  strExec.Append( ".on('onClose', function() {" )
		  if not IsNull( theMessage.ContentContainer ) then
		    strExec.Append( "var thisContainer = window.GSjQuery('#" + theMessage.ContentContainer.ControlID + "').detach();" )
		    strExec.Append( "thisContainer.appendTo( window.GSjQuery('body') ).css('position', 'absolute').css('display', 'none');" )
		  end if
		  strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onClose', ['" + EscapeString( theMessage.Name ) + "']);" )
		  strExec.Append( "})" )
		  
		  if not IsNull( theMessage.ContentContainer ) then
		    strExec.Append( ".on('onShow', function() {" )
		    strExec.Append( "window.GSjQuery('.noty_layout').css('width', 'auto').css('height', 'auto');" )
		    strExec.Append( "window.GSjQuery('#" + theMessage.ContentContainer.ControlID + "_alert').css('padding', 0);" )
		    strExec.Append( "var thisContainer = window.GSjQuery('#" + theMessage.ContentContainer.ControlID + "').detach();" )
		    strExec.Append( "var thisAlert = window.GSjQuery('#gwNoty_message_" + SafifyString( theMessage.Name ) + " .noty_body');" )
		    strExec.Append( "thisAlert.css('padding', '0px' );" )
		    strExec.Append( "thisAlert.empty().append(thisContainer);" )
		    strExec.Append( "thisContainer.css('position','relative');" )
		    strExec.Append( "thisContainer.css('left','0');" )
		    strExec.Append( "thisContainer.css('top','0');" )
		    strExec.Append( "thisContainer.css('display', 'block');" )
		    strExec.Append( "})" )
		  end if
		  
		  if not IsNull( theMessage.Style ) then
		    strExec.Append( ".on('onShow', function() {" )
		    strExec.Append( "var thisDialog = window.GSjQuery('#gwNoty_message_" + SafifyString( theMessage.Name ) + "');" )
		    strExec.Append( "thisDialog.addClass('" + theMessage.Style.Name + "');" )
		    strExec.Append( "})" )
		  end if
		  
		  strExec.Append( ".show();" )
		  
		  Buffer( strExec )
		  
		  Messages.Append( theMessage )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ThemeToString(theTheme as MessageThemes) As String
		  select case theTheme
		  case MessageThemes.Bootstrap3
		    Return "bootstrap-v3"
		  case MessageThemes.Bootstrap4
		    Return "bootstrap-v4"
		  case MessageThemes.Light
		    Return "light"
		  case MessageThemes.MetroUI
		    Return "metroui"
		  case MessageThemes.Mint
		    Return "mint"
		  case MessageThemes.Relax
		    Return "relax"
		  case MessageThemes.SemanticUI
		    Return "semanticui"
		  case MessageThemes.Sunset
		    Return "sunset"
		  end select
		  
		  Return "nest"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TypeToString(theType as MessageTypes) As String
		  Select case theType
		  case MessageTypes.Alert
		    Return "alert"
		  case MessageTypes.Error
		    Return "error"
		  Case MessageTypes.Success
		    Return "success"
		  Case MessageTypes.Warning
		    Return "warning"
		  case MessageTypes.Info
		    Return "info"
		  case else
		    Return "alert"
		  End Select
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event ButtonClick(messageName as String, buttonName as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ButtonClick_O(theMesage as GraffitiWebAlertMessage, theButton as GraffitiWebAlertButton)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MessageClosed(messageName as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MessageClosed_O(theMessage as GraffitiWebAlertMessage)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		http://needim.github.io/noty
		https://github.com/needim/noty
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  Buffer( "Noty.setMaxVisible( 999 );" )
			  
			  RunBuffer()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Messages() As GraffitiWebAlertMessage
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webalert", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jqDefaults, Type = String, Dynamic = False, Default = \"window.GSjQuery.notyfy(.defaults \x3D {\n    layout: \'top\'\x2C\n    type: \'alert\'\x2C\n    text: \'\'\x2C // can be html or string\n    dismissQueue: true\x2C // If you want to use queue feature set this true\n    template: \'<div class\x3D\"notyfy_message\"><span class\x3D\"notyfy_text\"></span><div class\x3D\"notyfy_close\"></div></div>\'\x2C\n    animation: {\n        open: {height: \'toggle\'}\x2C\n        close: {height: \'toggle\'}\x2C\n        easing: \'swing\'\x2C\n        speed: 500 // opening & closing animation speed\n    }\x2C\n    timeout: false\x2C // delay for closing event. Set false for sticky notifications\n    force: false\x2C // adds notification to the beginning of queue when set to true\n    modal: false\x2C\n    maxVisible: 5\x2C // you can set max visible notification for dismissQueue true option\x2C\n    killer: false\x2C // for close all notifications before show\n    closeWith: [\'click\']\x2C // [\'click\'\x2C \'button\'\x2C \'hover\'\x2C \'backdrop\'] // backdrop click will close all open notifications\n    callback: {\n        onShow: function() {}\x2C\n        afterShow: function() {}\x2C\n        onClose: function() {}\x2C\n        afterClose: function() {}\n    }\x2C\n    buttons: false // an array of buttons\n};", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACx\rjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAo2SURBVHhe7d1/bBt3FQDw77vLxbHdLqPVxGAS\raMBG0QaokbapiHUTEqK0S+OUhNJNGn8gKiZWaift2B9MXRFCMBQ7G6OgIoQAsVJSJXa7MgTiR8uP\rDqE14oe2VohfQgJN01aSJbZT29/Hu/Rl6qqNpr773n3PeR+p8r2v28Z39+59353tixJCCCGEEEII\rIYQQQgghhBBCCCGEEEII0SmAH1eW4R+6qxsnrj7veL2u21Wt1mZn1ZMHq/zsitKxCZDK7Xm7A7pP\rabVeAfYphLcAYC89RX8ge+FvvUoDEWdBwQwCzihU07R1phHhdN3DaXWkVOO/11E6JgEym/dci92t\rIVqlAdp56wHUWn4qFKjUnwDVCe3CRH1y7CQPJ16yE2B436rMwuwwOjBEK7KZR81D/CsqZ4KS7Ei1\rPPYMjyZSIhNg1baRda0WFmgHfJJWIdZ1oGnjtKPU2Hyl9AQPJUqiEiA7OHqzRszTi/4ED1kDUZ12\rUBfnj45/n4cSIREJkP1I/ibddPbSsf5xHrLWYiI4qjg/VUxEIlifAOncyG56kSVaTFS1okw4BLpV\rmD/22PM8YiVrN2qqv/AOOpJKAHAXDyUOVYMXHIV5m/sDKxMgnct/is7H6aiHHh5KNDqF/HaP5xXO\rTXx5hoesYV0CZAYKB6ixv4/DjkHV4DlwcEd1qvQHHrKCPQnwwdFsJqsP0Uvq55GOQ5VgFpCSoFL6\rEQ/FzuXHWHXnRm7s9vA4zfd38lBHoqMtRdXtHm/dbf9pnHnaigtIsSdAz0D+dgfgKdr5N/DQCgD9\rXe/c4DTPnvolD8Qm1gRIbcu/zUWHjnz1Jh5aMWid7+hat+G/zTOnfsdDsYivB9i0K5VJeSfpFdzK\rI2agOkFz7z9pRX/FI8sDaj0i3kOVyX8H0RgNsKM+NfYDDiMXWwJkBkYm6acPcmgG4iFquO7m6Ipl\rB/bcjaCNX9Gjs4MPzE+WfsFhpBx+jBTt/K8a3/kEAf7Ni20Bz/spLxqlNRzO9udv4jBSkSdAZrBw\rL+38+zk0CpC67gCajYVILkRRGb4GHTjAYaQiTYD01r1vRlTjHBpHc3+gBECNgf79FQHYSJXxIY4i\rE2kCALTGQcEbOIxCNz+2paurFe2laFCf78nt3sBRJCJLgEwuv5NWcJjDSCxeeAkA0YuuAjBHOf47\rn5GJJAF6to2+lX5UpCu2CIIlgMYIp4BXwG3pwcIXODAukgRwWrpAD5kLUXSC9gCuo2N5NxI0PLBq\raNc1HBplPAHSWwrX0eS/m8OIBTuCEd0YKgAB5bUann/QGGe+AngYyYq8Fmo4AzWBqOOpAD7qX/K9\rWx403jAbTYBs/2feCOjkOYxewOsA6Ab794GASp/3FowfPEYTQLtdBVqR2N5wCtoDOOjElwAEUBXW\rbt27mkMjzE4BiDF/ijdoDxDfFLAIYFUNmh/lyAhjCZAZKGwGgGs5jEXQHoC2TqwVwIcKjL5nYiwB\rEGCIF+MT8DoA9S+xJwCA2rI69+lQv+d4MUMJQLsfMdKrfq8FA04BNAfEOwWwFnbneDF0RhKgJ1cY\r9ucvDuMUrAcImkAhocPJ2DRgJAFo7v0wL8YqcA8AdlQAULhZDQ8bOZsyMwUgrOeluCW+B7gAIL1w\rnZFtGn4CbNqVosblvRzFb3hf21UALakAPnBUHy+GKvQESKe7bDn6lwSYBsCSCkAMVdXwK4A2k6nt\rWt1YaHsnAiprKgA1AglJAEMvtF1N3Wr/KAY7zgIWJaYCoIr16t+lNOq2dyKiRd9OBtWdGd4T+rYN\rPQFMf5HiSnkBPtUT9CNlYQPdDP2KYOgJgIv34bMHppz2m0CLzgJ8rYZrfwIALt6M0Ro6wEe7rZoC\rCCidgApg2RSAgT7YaVETSADUGl4MTfgVwLIpwA3wqSDqZ6yqAPSKEtEDvMyLVqCK1P6VwIAfKQsd\r4DleCo2BCoBW3Qgp0BRgWRNICfkSL4Ym/AqAYFcCBDiVA5suBRNHOS/yYmjCrwCAs7xoBSdIBbDk\rAyGv0JCECqDsuhee224PsM+hbA59+wShUy37KwDVgNBfZBBtTwFDs1aVf18te5X9CYCgpnnRCu12\r8r2N9t9DMAEV/lF9Z3+dw9CEngCuRqsSANp8c6rpuLfwoh0QjGxX4MfQ9A7svroBbujnq0HQNPCE\ro/ApDi9LK/VuSpwc9QA38lDsaB3ytXLxUQ5DE3oC+DIDhbM2bbxOoFFvrFfGr+xWd8tgpsu1rA/o\rBPXu80a2qanTnJ/xowgD4q/VxIE5jkJlJAG8Rv0IL4oQaIApXgydkQSYOf71c9S1THIoAsKWuQQw\r0gT6srmRHX73zWGsNKqPuVr/mcPL0l1uBlCP0ubZzkPxofJfrZRu5yh0xhJA7dzpZZ5fNUM/Ic0j\rsUCFj9TKpc9yuGyp/tHrXRf/xmFs6JR0tF4uFjkMnakmUKmDBxsI8VcAOp9v6w2UhWNjf+fFWJks\r/z5zCUBcDcYyd0VA/JrpRDSaAHNHx56lPuBbHIor1HLR+AFkNAF8UgXa5B/9k+PGexDjCSBVoD1R\rHP0+4wngc1XXGC+K5XksiqPfF0kCzJUfeQ4Q93Ao/g86bX226v1rhEPjIkkA33ylNIaofsyheB2g\rsKAmJlocGhdZAvhcVxUox5scikvQAfKVann8JxxGItIEmJssnqGGMLabR9uMSv90rVJ8gMPIRJoA\rvlq59Dglwfc4FD5UDUSI5BdpXSryBPDVysV7qdz9nEOz2rxj6er+kbt40Th0cHu9Uvwth5Ey92bQ\rZfi3kteOexIi+OgYldcvgoJTHF6WRpV1FN5PyfN+HjIGFdxXK499g8PIxZYAvsxgvk9pOEkbOstD\rK83D1XJxPy/HItYE8KUH8v2gnCl6JVb8KvvIoHq8Winu4ig2sfQAF6tVxo9pUHfSFgn0a16TBR6y\rYef7Yq8AS7q37b3B1c3DNFfbdqPJkOHOarn0TQ5iZ00C+NZs2nVVLeUdBlCbeKhzINbRge21qeJR\rHrGCVQmwJDNQKFJj2EkXjH6DtD61qbHfc2wNKxPA5//KGXooUSIk+htGiLi/Vik9zKF1rO28G2ef\r/kvjPR866Onz/o2R7Pqi5rLgM1rB9nql9F0esJK1FeBi6a2FQQB4kF7trTxkL1Q1VFiko/5zPGK1\rRCTAkvTgyDAg5ullv4+H7ME73vNUcfZIKfRbuZiSqARYslgRHOUnwkYeig/v+O5mamzm+Jes+lr8\rciQyAZakB0dvoR2Qo06Lpgj1Lh42D1WLttwUaFWe13NT6smDVX4mcRKdABdbTAaNg7R3+qlfuJmH\rQ0Pd/Av0/54ArWmnVxO90y/WMQnwKkOFdM8C9oEL62nP9Slw/MfraQcu5za2C+h/mwjUaTrSpxW0\rpjWq0wuVR//Bz3eUzkyA13PHvq7smrk1gM21TeWu9e++7aAz5+9wrb0X6+mXXzL1PXwhhBBCCCGE\rEEIIIYQQQgghhBBCCCGEEEIIc5T6HyEpAyIFL2I6AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEaSURBVDhP1ZIxTgMxEEX/d1IEkHICoKBC0FJARQcNgjTAAeijBUSPqFFIjQQHCEJaKiQuwAm4ASfYYkXlj9eZONlESYt4jf+3d8aznsGfQ1trLB1nO47c92Bb9F8/eX9gRzPUErSOuutsNh4JHtrWEOkb1EWZ9z9sJ+FsHQW/j4Il3Hn483hIrgruZbmTHUQ/QUrgmo0sBG+ZxXTpodQ25G7NJlICCKem5kPsVe9jLjJOEMo0tRA5bJiMpAQCCpMRym23OtmZ2bmMK4A+TUTCP286cNdspLpkuqUpAaGeyYinfy3zhyuzkfDNk8lEbQ5WTi6vRd5HE3ovokidEQblW2/Y1glmJrF6ZdLdBLlWeVGFoOdF0/ivAX4BHm5QESINiFEAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag Enum, Name = MessagePositions, Type = Integer, Flags = &h0
		Top
		  TopLeft
		  TopCenter
		  TopRight
		  Center
		  CenterLeft
		  CenterRight
		  Bottom
		  BottomLeft
		  BottomCenter
		BottomRight
	#tag EndEnum

	#tag Enum, Name = MessageThemes, Type = Integer, Flags = &h0
		Bootstrap3
		  Bootstrap4
		  Light
		  MetroUI
		  Mint
		  Nest
		  Relax
		  SemanticUI
		Sunset
	#tag EndEnum

	#tag Enum, Name = MessageTypes, Type = Integer, Flags = &h0
		Info
		  Alert
		  Success
		  Warning
		Error
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
