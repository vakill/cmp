#tag Class
Protected Class GraffitiWebAlertMessage
	#tag Method, Flags = &h0
		Sub Constructor(msgName as String, msgContent as String)
		  Name = msgName
		  Content = msgContent
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(msgName as String, msgContent as String, msgType as GraffitiWebAlert.MessageTypes)
		  Name = msgName
		  Content = msgContent
		  Type = msgType
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(msgName as String, msgContent as String,msgType as GraffitiWebAlert.MessageTypes, msgTheme as GraffitiWebAlert.MessageThemes, msgTimeout as Integer, msgPosition as GraffitiWebAlert.MessagePositions, msgProgressbar as Boolean = True)
		  Name = msgName
		  Content = msgContent
		  Type = msgType
		  Theme = msgTheme
		  Timeout = msgTimeout
		  Modal = True
		  Progressbar = msgProgressbar
		  Position = msgPosition
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Buttons() As GraffitiWebAlertButton
	#tag EndProperty

	#tag Property, Flags = &h0
		CanOverflow As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		CloseButton As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Content As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ContentContainer As WebContainer
	#tag EndProperty

	#tag Property, Flags = &h0
		Icon As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Modal As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Name As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Position As GraffitiWebAlert.MessagePositions
	#tag EndProperty

	#tag Property, Flags = &h0
		Progressbar As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		Style As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty

	#tag Property, Flags = &h0
		Theme As GraffitiWebAlert.MessageThemes
	#tag EndProperty

	#tag Property, Flags = &h0
		Timeout As Integer = 10000
	#tag EndProperty

	#tag Property, Flags = &h0
		Type As GraffitiWebAlert.MessageTypes
	#tag EndProperty

	#tag Property, Flags = &h0
		VisibilityControl As Boolean = True
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="CanOverflow"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CloseButton"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Content"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Modal"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Progressbar"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Timeout"
			Group="Behavior"
			InitialValue="10000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VisibilityControl"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Position"
			Group="Behavior"
			Type="GraffitiWebAlert.MessagePositions"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Theme"
			Group="Behavior"
			Type="GraffitiWebAlert.MessageThemes"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			Type="GraffitiWebAlert.MessageTypes"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
