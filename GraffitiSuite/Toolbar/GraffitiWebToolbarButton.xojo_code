#tag Class
Protected Class GraffitiWebToolbarButton
	#tag Method, Flags = &h0
		Sub Constructor(isSpacer as Boolean, isFlex as Boolean = False)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  Spacer = isSpacer
		  Flex = isFlex
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(isPicture as Boolean, thePicture as Picture, size as Integer)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  isImage = isPicture
		  ImageString = EncodeBase64( thePicture.GetData( picture.FormatPNG, Picture.QualityHigh ), 0 )
		  imageSize = size
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(isPicture as Boolean, PictureURL as String, size as Integer)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  isImage = isPicture
		  ImageString = PictureURL
		  imageSize = size
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(buttonCaption as String, buttonState as Boolean = False)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  mCaption = buttonCaption
		  mCaptionAlignment = GraffitiWebToolbar.Alignments.Center
		  mStyle = GraffitiWebToolbar.ButtonStyles.Light
		  mState = buttonState
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(buttonCaption as String, buttonIcon as String, buttonState as Boolean = False)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  mCaption = buttonCaption
		  mIcon = buttonIcon
		  mCaptionAlignment = GraffitiWebToolbar.Alignments.Center
		  mStyle = GraffitiWebToolbar.ButtonStyles.Light
		  mState = buttonState
		End Sub
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBorderStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBorderStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "borderstyle" )
			End Set
		#tag EndSetter
		BorderStyle As GraffitiWebToolbar.BorderStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBorderType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBorderType = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "bordertype" )
			End Set
		#tag EndSetter
		BorderType As GraffitiWebToolbar.BorderTypes
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaption = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "caption" )
			End Set
		#tag EndSetter
		Caption As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCaptionAlignment
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaptionAlignment = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "alignment" )
			End Set
		#tag EndSetter
		CaptionAlignment As GraffitiWebToolbar.Alignments
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCaptionStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaptionStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "captionstyle" )
			End Set
		#tag EndSetter
		CaptionStyle As GraffitiWebToolbar.TextStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCornerStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCornerStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "corners" )
			End Set
		#tag EndSetter
		CornerStyle As GraffitiWebToolbar.Corners
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mEnabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEnabled = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "enabled" )
			End Set
		#tag EndSetter
		Enabled As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Flex As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mIcon
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIcon = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "icon" )
			End Set
		#tag EndSetter
		Icon As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mIconPosition
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mIconPosition = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "iconposition" )
			End Set
		#tag EndSetter
		IconPosition As GraffitiWebToolbar.IconPositions
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		ID As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		imageSize As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ImageString As String
	#tag EndProperty

	#tag Property, Flags = &h0
		isImage As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderStyle As GraffitiWebToolbar.BorderStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderType As GraffitiWebToolbar.BorderTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaptionAlignment As GraffitiWebToolbar.Alignments
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaptionStyle As GraffitiWebToolbar.TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCornerStyle As GraffitiWebToolbar.Corners
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnabled As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIcon As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIconPosition As GraffitiWebToolbar.IconPositions
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		mObserver As GraffitiUpdateInterface
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mState As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyle As GraffitiWebToolbar.ButtonStyles
	#tag EndProperty

	#tag Property, Flags = &h0
		Spacer As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mState
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mState = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "state" )
			End Set
		#tag EndSetter
		State As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "style" )
			End Set
		#tag EndSetter
		Style As GraffitiWebToolbar.ButtonStyles
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Spacer"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="State"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Flex"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ImageString"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="isImage"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="imageSize"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderStyle"
			Group="Behavior"
			Type="GraffitiWebToolbar.BorderStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderType"
			Group="Behavior"
			Type="GraffitiWebToolbar.BorderTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - None"
				"1 - All"
				"2 - Top"
				"3 - Right"
				"4 - Bottom"
				"5 - Left"
				"6 - NoTop"
				"7 - NoRight"
				"8 - NoBottom"
				"9 - NoLeft"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CaptionAlignment"
			Group="Behavior"
			Type="GraffitiWebToolbar.Alignments"
			EditorType="Enum"
			#tag EnumValues
				"0 - Left"
				"1 - Center"
				"2 - Right"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CaptionStyle"
			Group="Behavior"
			Type="GraffitiWebToolbar.TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CornerStyle"
			Group="Behavior"
			Type="GraffitiWebToolbar.Corners"
			EditorType="Enum"
			#tag EnumValues
				"0 - None"
				"1 - Rounded"
				"2 - Top"
				"3 - Right"
				"4 - Bottom"
				"5 - Left"
				"6 - Circle"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconPosition"
			Group="Behavior"
			Type="GraffitiWebToolbar.IconPositions"
			EditorType="Enum"
			#tag EnumValues
				"0 - Left"
				"1 - Top"
				"2 - Right"
				"3 - Bottom"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Style"
			Group="Behavior"
			Type="GraffitiWebToolbar.ButtonStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Primary"
				"1 - Secondary"
				"2 - Success"
				"3 - Danger"
				"4 - Warning"
				"5 - Info"
				"6 - Light"
				"7 - Dark"
				"8 - Link"
			#tag EndEnumValues
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
