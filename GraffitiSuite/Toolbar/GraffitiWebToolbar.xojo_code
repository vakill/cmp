#tag Class
Protected Class GraffitiWebToolbar
Inherits GraffitiControlWrapper
Implements GraffitiUpdateInterface
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "buttonClick"
		      if Parameters.Ubound < 6 then Return True
		      
		      dim buttonID as String = Parameters(0)
		      dim mouseX as Integer = Parameters(1).IntegerValue
		      dim mouseY as Integer = Parameters(2).IntegerValue
		      dim buttonLeft as Integer = Parameters(3).IntegerValue
		      dim buttonTop as Integer = Parameters(4).IntegerValue
		      dim buttonWidth as Integer = Parameters(5).IntegerValue
		      dim buttonHeight as Integer = Parameters(6).IntegerValue
		      
		      dim r as new REALbasic.Rect( buttonLeft, buttonTop, buttonWidth, buttonHeight )
		      
		      dim theButton as GraffitiWebToolbarButton = getButtonByID( buttonID )
		      
		      if not IsNull( theButton ) then Action( theButton, mouseX, mouseY, r )
		    case "buttonenter"
		      if Parameters.Ubound < 0 then return true
		      dim theButton as GraffitiWebToolbarButton = getButtonByID( Parameters(0) )
		      if not IsNull( theButton ) then ButtonEnter( theButton )
		    case "buttonexit"
		      if Parameters.Ubound < 0 then return true
		      dim theButton as GraffitiWebToolbarButton = getButtonByID( Parameters(0) )
		      if not IsNull( theButton ) then ButtonExit( theButton )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		  
		  select case Name
		  case "Style"
		    UpdateOptions()
		    Return True
		  case "Enabled"
		    if LibrariesLoaded and isShown then
		      
		      dim strExec() as String
		      strExec.Append( "var myElement = window.GSjQuery('#" + me.ControlID + "');" )
		      strExec.Append( "myElement.prop('disabled', " + Str( not Value.BooleanValue ).Lowercase + ");" )
		      Buffer( strExec )
		      
		      Return True
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// Do not implement
		  
		  dim strExec() as String
		  strExec.Append( "var myGroup = GSjQuery('#" + me.ControlID + "_group');" )
		  if me.Width > me.Height then
		    strExec.Append( "myGroup.removeClass('btn-group-vertical').addClass('btn-group');" )
		  else
		    strExec.Append( "myGroup.removeClass('btn-group').addClass('btn-group-vertical');" )
		  end if
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' class='blue " + BGStyleToString( mToolbarStyle ) + if(not IsNull( me.Style ), " " + me.Style.Name, "" ) + "' style='display:none;'>" )
		  if me.Width > me.Height then
		    source.Append( "<div id='" + me.ControlID + "_group' class='btn-group' role='group' style='width:100%;height:100%;'></div>" )
		  else
		    source.Append( "<div id='" + me.ControlID + "_group' class='btn-group-vertical justify-content-start' role='group' style='width:100%;height:100%;'></div>" )
		  end if
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddButton(newButton as GraffitiWebToolbarButton)
		  if IsNull( newButton ) then Return
		  
		  dim strExec() as String
		  strExec.Append( "var parentElement = GSjQuery('#" + me.ControlID + "_group');" )
		  strExec.Append( "parentElement.append('" + EscapeString( ButtonToString( newButton ) ) + "');" )
		  
		  strExec.Append( "var buttonElement = GSjQuery('#" + newButton.ID + "');" )
		  
		  strExec.Append( "buttonElement.on('click', function(e){" )
		  strExec.Append( "var thisButton = GSjQuery(this);" )
		  strExec.Append( "var position = thisButton.offset();" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonClick', ['" + newButton.ID + "', e.pageX, e.pageY, position.left, position.top, thisButton.width(), thisButton.height()]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "buttonElement.on('mouseenter', function(e){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonEnter', ['" + newButton.ID + "']);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "buttonElement.on('mouseleave', function(e){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonExit', ['" + newButton.ID + "']);" )
		  strExec.Append( "});" )
		  
		  Buffer( strExec )
		  
		  newButton.mObserver = me
		  buttons.Append( newButton )
		  
		  mLastButton = Buttons.Ubound
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function AlignmentToString(theAlignment as Alignments) As String
		  select case theAlignment
		  case Alignments.Center
		    Return "text-center"
		  case Alignments.Left
		    return "text-left"
		  case Alignments.Right
		    Return "text-right"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allAlignments() As String()
		  return Array( "text-center", "text-left", "text-right" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allBorderStyles() As String()
		  Return Array( "border-primary", "border-secondary", "border-success", "border-danger", "border-warning", _
		  "border-info", "border-light", "border-dark", "border-white" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allBorderTypes() As String()
		  Return Array( "border", "border-top", "border-right", "border-bottom", "border-left", _
		  "border-0", "border-top-0", "border-right-0", "border-bottom-0", "border-left-0" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allButtonStyles() As String()
		  return Array( "btn-danger", "btn-dark", "btn-info", "btn-light", "btn-link", _
		  "btn-primary", "btn-secondary", "btn-success", "btn-warning" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allCornerStyles() As String()
		  Return Array( "rounded", "rounded-top", "rounded-right", "rounded-bottom", "rounded-left", "rounded-circle", "rounded-0" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allTextStyles() As String()
		  return Array( "text-danger", "text-dark", "text-info", "text-light", "text-muted", "text-primary", "text-secondary", _
		  "text-success", "text-warning", "text-white" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ApplyBGStyle(toElement as String, newStyle as BGStyles)
		  dim arrStyles() as String = Array( "bg-primary", "bg-secondary", "bg-success", "bg-danger", "bg-warning", "bg-info", "bg-light", "bg-dark", "bg-white" )
		  
		  dim theStyle as String = BGStyleToString( newStyle )
		  
		  dim strExec() as String
		  if toElement.len > 0 then
		    strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " " + toElement + "')" )
		  else
		    strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + "')" )
		  end if
		  
		  for each s as String in arrStyles
		    strExec.Append( ".removeClass('" + s + "')" )
		  next
		  strExec.Append( ".addClass('" + theStyle + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ApplyTextStyle(toElement as String, newStyle as TextStyles)
		  dim arrStyles() as String = Array( "text-primary", "text-secondary", "text-success", "text-danger", "text-warning", "text-info", "text-light", "text-dark", "text-muted", "text-white" )
		  
		  dim theStyle as String = TextStyleToString( newStyle )
		  
		  dim strExec() as String
		  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " " + toElement + "')" )
		  
		  for each s as String in arrStyles
		    strExec.Append( ".removeClass('" + s + "')" )
		  next
		  strExec.Append( ".addClass('" + theStyle + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function BGStyleToString(theStyle as BGStyles) As String
		  select case theStyle
		  case BGStyles.Danger
		    Return "bg-danger"
		  case BGStyles.Dark
		    Return "bg-dark"
		  case BGStyles.Info
		    Return "bg-info"
		  case BGStyles.Light
		    Return "bg-light"
		  case BGStyles.Primary
		    Return "bg-primary"
		  case BGStyles.Secondary
		    Return "bg-secondary"
		  case BGStyles.Success
		    Return "bg-success"
		  case BGStyles.Warning
		    Return "bg-warning"
		  case BGStyles.White
		    Return "bg-white"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function borderStyleToString(theStyle as BorderStyles) As String
		  Select case theStyle
		  case BorderStyles.Danger
		    Return "border-danger"
		  case BorderStyles.Dark
		    Return "border-dark"
		  case BorderStyles.Info
		    Return "border-info"
		  case BorderStyles.Light
		    Return "border-light"
		  case BorderStyles.Primary
		    Return "border-primary"
		  case BorderStyles.Secondary
		    Return "border-secondary"
		  case BorderStyles.Success
		    Return "border-success"
		  case BorderStyles.Warning
		    Return "border-warning"
		  case BorderStyles.White
		    Return "border-white"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function borderTypeToString(theType as BorderTypes) As String
		  select case theType
		  case BorderTypes.All
		    Return "border"
		  case BorderTypes.Bottom
		    Return "border-bottom"
		  case BorderTypes.Left
		    Return "border-left"
		  case BorderTypes.NoBottom
		    Return "border-bottom-0"
		  case BorderTypes.NoLeft
		    Return "border-left-0"
		  case BorderTypes.None
		    Return "border-0"
		  case BorderTypes.NoRight
		    Return "border-right-0"
		  case BorderTypes.NoTop
		    Return "border-top-0"
		  case BorderTypes.Right
		    Return "border-right"
		  case BorderTypes.Top
		    Return "border-top"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Button(index as Integer) As GraffitiWebToolbarButton
		  if index < 0 or index > Buttons.Ubound then Return nil
		  
		  Return buttons(index)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ButtonCount() As Integer
		  Return buttons.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ButtonStyleToString(theStyle as ButtonStyles) As String
		  select case theStyle
		  case ButtonStyles.Danger
		    return "btn-danger"
		  case ButtonStyles.Dark
		    return "btn-dark"
		  case ButtonStyles.Info
		    return "btn-info"
		  case ButtonStyles.Light
		    return "btn-light"
		  Case ButtonStyles.Link
		    return "btn-link"
		  case ButtonStyles.Primary
		    return "btn-primary"
		  case ButtonStyles.Secondary
		    return "btn-secondary"
		  case ButtonStyles.Success
		    return "btn-success"
		  case ButtonStyles.Warning
		    return "btn-warning"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ButtonToString(theButton as GraffitiWebToolbarButton) As String
		  dim strAlignment as String = AlignmentToString( theButton.CaptionAlignment )
		  dim strCaptionStyle as String = TextStyleToString( theButton.CaptionStyle )
		  dim strButtonStyle as String = ButtonStyleToString( theButton.Style )
		  
		  if theButton.Spacer and not theButton.Flex then
		    return "<span id='" + theButton.ID + "' style='width:0.25em;height:0.25em;'></span>"
		  ElseIf theButton.Spacer and theButton.Flex then
		    return "<span id='" + theButton.ID + "' class='m" + If( me.Width > me.Height, "r", "b" ) + "-auto'></span>"
		  elseif theButton.isImage then
		    dim strImage as String = "<img src='" + theButton.ImageString + "' "
		    if me.Width > me.Height then
		      strImage = strImage + " style='width:" + Str( theButton.imageSize ) + "px;'/>"
		    else
		      strImage = strImage + " style='height:" + Str( theButton.imageSize ) + "px;width:100%;'/>"
		    end if
		    Return strImage
		  end if
		  
		  dim strOut() as String
		  strOut.Append( "<button id='" + theButton.ID + "' type='button' class='btn btn-sm rounded-0" + _
		  " " + strAlignment + _
		  " " + strButtonStyle + _
		  if( theButton.State, " active", "" ) + "'" + _
		  if( not theButton.Enabled, " disabled", "" ) + ">" )
		  
		  dim strIcon as String = "<i id='" + theButton.ID + "_icon' class='" + theButton.Icon + If( theButton.Icon.Len = 0, " d-none", "" ) + "'></i>"
		  dim strCaption as String = "<span id='" + theButton.ID + "_caption' class='" + If( theButton.Caption.Len = 0, " d-none", "" ) + "'>" + EscapeString( theButton.Caption ) + "</span>"
		  dim strSpacer as String = "<span id='" + theButton.ID + "_spacer' style='height:1px;width:1px'>"
		  
		  select case theButton.IconPosition
		  case IconPositions.Left, IconPositions.Right
		    if strIcon.len > 0 and strCaption.len > 0 then strSpacer = strSpacer + "&nbsp;"
		  case else
		    if strIcon.len > 0 and strCaption.len > 0 then strSpacer = strSpacer + "<br />"
		  end select
		  
		  strSpacer = strSpacer + "</span>"
		  
		  select case theButton.IconPosition
		  case IconPositions.Left
		    strOut.Append( strIcon )
		    if strIcon.len > 0 and strCaption.len > 0 then strOut.Append( strSpacer )
		    strOut.Append( strCaption )
		  case IconPositions.Right
		    strOut.Append( strCaption )
		    if strIcon.len > 0 and strCaption.len > 0 then strOut.Append( strSpacer )
		    strOut.Append( strIcon )
		  case IconPositions.Top
		    strOut.Append( strIcon )
		    if strIcon.len > 0 and strCaption.len > 0 then strOut.Append( strSpacer )
		    strOut.Append( strCaption )
		  case IconPositions.Bottom
		    strOut.Append( strCaption )
		    if strIcon.len > 0 and strCaption.len > 0 then strOut.Append( strSpacer )
		    strOut.Append( strIcon )
		  end select
		  
		  strOut.Append( "</button>" )
		  
		  Return Join( strOut )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  mToolbarStyle = BGStyles.White
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function cornersToString(theCorners as Corners) As String
		  select case theCorners
		  case Corners.Bottom
		    Return "rounded-bottom"
		  case Corners.Circle
		    Return "rounded-circle"
		  case Corners.Left
		    Return "rounded-left"
		  case Corners.None
		    Return "rounded-0"
		  case Corners.Right
		    Return "rounded-right"
		  case Corners.Rounded
		    Return "rounded"
		  case Corners.Top
		    Return "rounded-top"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function getButtonByID(theID as String) As GraffitiWebToolbarButton
		  for each b as GraffitiWebToolbarButton in Buttons
		    if b.ID = theID then Return b
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateAlignment(theButton as GraffitiWebToolbarButton) As String
		  dim strExec() as String
		  
		  dim strAlignment as String = AlignmentToString( theButton.CaptionAlignment )
		  strExec.Append( "buttonElement.removeClass('" + Join( allAlignments, " " ) + "').addClass('" + AlignmentToString( theButton.CaptionAlignment ) + "');" )
		  
		  Return Join( strExec )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateButtonBorder(theButton as GraffitiWebToolbarButton) As String
		  dim strExec() as String
		  strExec.Append( "buttonElement.removeClass('" + Join( allButtonStyles, " " ) + "')" )
		  strExec.Append( ".removeClass('" + Join( allBorderTypes, " " ) + "')" )
		  strExec.Append( ".removeClass('" + Join( allCornerStyles, " " ) + "')" )
		  strExec.Append( ".addClass('" + BorderTypeToString( theButton.BorderType ) + "')" )
		  strExec.Append( ".addClass('" + BorderStyleToString( theButton.BorderStyle ) + "')" )
		  strExec.Append( ".addClass('" + cornersToString( theButton.CornerStyle ) + "');" )
		  
		  Buffer( Join( strExec ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateButtonEnabled(theButton as GraffitiWebToolbarButton) As String
		  if theButton.Enabled then
		    Return "buttonElement.prop('disabled', false);"
		  else
		    Return "buttonElement.prop('disabled', true);"
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateCaption(theButton as GraffitiWebToolbarButton) As String
		  dim strExec() as String
		  strExec.Append( "captionElement.html('" + EscapeString( theButton.Caption ) + "');" )
		  strExec.Append( "captionElement." + If( theButton.Caption.Len > 0, "removeClass('d-none')", "addClass('d-none')" ) + ";" )
		  if theButton.Caption.Len > 0 and theButton.Icon.Len > 0 then
		    strExec.Append( "spacerElement.removeClass('d-none');" )
		  else
		    strExec.Append( "spacerElement.addClass('d-none');" )
		  end if
		  
		  Return Join( strExec )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateCaptionStyle(theButton as GraffitiWebToolbarButton) As String
		  return "buttonElement.removeClass('" + Join( allTextStyles, " " ) + "').addClass('" + TextStyleToString( theButton.CaptionStyle ) + "');"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateIcon(theButton as GraffitiWebToolbarButton) As String
		  dim strExec() as String
		  strExec.Append( "iconElement.removeClass().addClass('" + EscapeString( theButton.Icon ) + "');" )
		  strExec.Append( "iconElement." + If( theButton.Icon.Len > 0, "removeClass('d-none')", "addClass('d-none')" ) + ";" )
		  if theButton.Caption.Len > 0 and theButton.Icon.Len > 0 then
		    strExec.Append( "spacerElement.removeClass('d-none');" )
		  else
		    strExec.Append( "spacerElement.addClass('d-none');" )
		  end if
		  
		  return Join( strExec )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateIconPosition(theButton as GraffitiWebToolbarButton) As String
		  dim strExec() as String
		  strExec.Append( "captionElement.detach();" )
		  strExec.Append( "iconElement.detach();" )
		  
		  select case theButton.IconPosition
		  case IconPositions.Bottom, IconPositions.Right
		    strExec.Append( "captionElement.insertBefore(spacerElement);" )
		    strExec.Append( "iconElement.insertAfter(spacerElement);" )
		  case IconPositions.Left, IconPositions.Top
		    strExec.Append( "iconElement.insertBefore(spacerElement);" )
		    strExec.Append( "captionElement.insertAfter(spacerElement);" )
		  end select
		  
		  Return Join( strExec )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateState(theButton as GraffitiWebToolbarButton) As String
		  dim strExec() as String
		  strExec.Append( "buttonElement.removeClass('active');" )
		  if theButton.State then strExec.Append( "buttonElement.addClass('active');" )
		  
		  Return Join( strExec )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function get_UpdateStyle(theButton as GraffitiWebToolbarButton) As String
		  return "buttonElement.removeClass('" + Join( allButtonStyles, " " ) + "').addClass('" + ButtonStyleToString( theButton.Style ) + "');"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebPagination -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebPagination -->" )
		  
		  Return Join( strOut )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InsertButton(atIndex as Integer, newButton as GraffitiWebToolbarButton)
		  if IsNull( newButton ) then Return
		  
		  newButton.mObserver = me
		  
		  dim insertBefore as String
		  if atIndex < 0 then
		    if Buttons.Ubound >= 0 then
		      dim firstButton as GraffitiWebToolbarButton = Buttons(0)
		      insertBefore = firstButton.ID
		    else
		      AddButton( newButton )
		    end if
		  elseif atIndex > Buttons.Ubound then
		    AddButton( newButton )
		  else
		    dim beforeButton as GraffitiWebToolbarButton = Buttons(atIndex)
		    insertBefore = beforeButton.ID
		  end if
		  
		  dim strExec() as String
		  strExec.Append( "var parentElement = GSjQuery('#" + me.ControlID + "_group');" )
		  strExec.Append( "var newElement = GSjQuery('" + EscapeString( ButtonToString( newButton ) ) + "').insertBefore('#" + insertBefore + "');" )
		  
		  strExec.Append( "var newButton = GSjQuery('#" + newButton.ID + "').on('click', function(){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonClick', ['" + newButton.ID + "']);" )
		  strExec.Append( "});" )
		  
		  Buffer( strExec )
		  
		  buttons.Insert( atIndex, newButton )
		  
		  mLastButton = atIndex
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LastButton() As GraffitiWebToolbarButton
		  if mLastButton < 0 or mLastButton > buttons.Ubound then Return nil
		  Return Buttons(mLastButton)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  for intCycle as Integer = Buttons.Ubound DownTo 0
		    RemoveButton( intCycle )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveButton(theButton as GraffitiWebToolbarButton)
		  if IsNull( theButton ) then Return
		  
		  dim buttonIndex as Integer = Buttons.IndexOf( theButton )
		  
		  buttons.Remove( buttonIndex )
		  
		  dim strExec() as String
		  strExec.Append( "var myElement = GSjQuery('#" + me.ControlID + "_group');" )
		  strExec.Append( "var myButton = myElement.find('#" + theButton.ID + "');" )
		  strExec.Append( "myButton.remove();" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveButton(index as Integer)
		  if index < 0 or index > Buttons.Ubound then Return
		  
		  dim theButton as GraffitiWebToolbarButton = Buttons(index)
		  
		  RemoveButton( theButton )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TextStyleToString(theStyle as TextStyles) As String
		  select case theStyle
		  case TextStyles.Danger
		    return "text-danger"
		  case TextStyles.Dark
		    return "text-dark"
		  case TextStyles.Info
		    return "text-info"
		  case TextStyles.Light
		    return "text-light"
		  case TextStyles.Muted
		    return "text-muted"
		  case TextStyles.Primary
		    return "text-primary"
		  case TextStyles.Secondary
		    return "text-secondary"
		  case TextStyles.Success
		    return "text-success"
		  case TextStyles.Warning
		    return "text-warning"
		  case TextStyles.White
		    return "text-white"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    strExec.Append( "var myButton = window.GSjQuery('#" + me.ControlID + " a.button');" )
		    
		    strExec.Append( "myButton.on('click touchend', function() { " )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonClick');" )
		    strExec.Append( "});" )
		    
		    me.ExecuteJavaScript( Join( strExec ) )
		    
		    RunBuffer()
		    
		    Buffer( "window.GSjQuery('#" + me.ControlID + "').css('display','block');" )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateProperty(item as Variant, identifier as String)
		  // Part of the GraffitiUpdateInterface interface.
		  
		  dim theButton as GraffitiWebToolbarButton = item
		  if IsNull( theButton ) then Return
		  
		  if theButton.Spacer then Return
		  
		  dim strExec() as String
		  
		  strExec.Append( "var myElement = GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "var buttonElement = myElement.find('#" + theButton.ID + "');" )
		  strExec.Append( "var captionElement = myElement.find('#" + theButton.ID + "_caption');" )
		  strExec.Append( "var spacerElement = myElement.find('#" + theButton.ID + "_spacer');" )
		  strExec.Append( "var iconElement = myElement.find('#" + theButton.ID + "_icon');" )
		  
		  select case identifier
		  case "iconposition"
		    strExec.Append( get_UpdateIconPosition( theButton ) )
		  case "state"
		    strExec.Append( get_UpdateState( theButton ) )
		  case "style"
		    strExec.Append( get_UpdateStyle( theButton ) )
		  case "caption"
		    strExec.Append( get_UpdateCaption( theButton ) )
		  case "captionstyle"
		    strExec.Append( get_UpdateCaptionStyle( theButton ) )
		  case "icon"
		    strExec.Append( get_UpdateIcon( theButton ) )
		  case "alignment"
		    strExec.Append( get_UpdateAlignment( theButton ) )
		  case "enabled"
		    strExec.Append( get_UpdateButtonEnabled( theButton ) )
		  case "bordertype", "borderstyle", "corners"
		    strExec.Append( get_UpdateButtonBorder( theButton ) )
		  end select
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdatePropertyMultiple(identifier as String, item as Variant, modifier as Variant)
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Action(ClickedButton as GraffitiWebToolbarButton, mouseX as Integer, mouseY as Integer, buttonDimensions as REALbasic.Rect)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ButtonEnter(theButton as GraffitiWebToolbarButton)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ButtonExit(theButton as GraffitiWebToolbarButton)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://getbootstrap.com/docs/4.0/components/
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private Buttons() As GraffitiWebToolbarButton
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLastButton As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolbarStyle As BGStyles
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mToolbarStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mToolbarStyle = value
			  
			  ApplyBGStyle( "", mToolbarStyle )
			End Set
		#tag EndSetter
		ToolbarStyle As BGStyles
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.toolbar", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANWSURBVHhe7drPa1RXFMDxex5GMoM/cFX8B1QQF8ZQ0G7cFn9NFiJ00YI7u1BnSsGdK0EQZ9SN4EJXuhE00fZfKEUTXRSllOLWpYiQ0Zjk9NzMQUUmM5O8e6Xyvp+QvHvPvLx3f5x3Z2BuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwJclfsxq66FT2xY2jF+04qSITPSiI1BdsBY+DSqP52fap2Ok98JIpH6sdc3+f9KuMxFENnp8KNXwxP7Oblx8e+7179dfeXgk9UbrvB0OWkMnbHC39KLDadDn1uTZDSFceDPd/sfD2WVPgNpU63hYDldFwnYPrYtNyrMgerI73XnkoVXVGs1vrWs3rXO7PbQuds+XoQhnuvfbdz20qs1Hf925JEu3bET3e2h9VJftvs3ug841j2RV+DGL+OSnmPzIrrHbJrTj1YHieWUnP1ppt7V/pR9DLMpip/TkRyKF/VytT50dfaUsIWsCxGU/xeR/JAfGG62WV/rqvS4HvFpabL+/fa2q3vjlJ3tr+96rSajKZS9mlTUBbPgmvZCMPdn7vNhXoZr8nmbgNVWXSq82nxNNP3b9ZE0Ae3pyLGMDE0DX8iFzRMM+uEoodngxHQmbNh1u7fJaNplXAPzfkQAVRwJUHAlQcSRAxZEAFUcCVBwJUHEkQMWRABWXNwHi9/npDbtm+nsO64eERS8l9V40y3U/lTcBRJ54KR0NT73Un2r6e8ZNKQOo6t9eTEaDvnr3sPOvV7PJvAKEWS+lU+icl/or4m6exFQee6mvQkLyBLCx+9NLWYkfc5Fao/VXis0ZkT1pv3VnOke8uqraseZDETns1VLiTqTuTHtPLPYi/Vk/b1s/f/BqSTq/VOied/euvPBANrk/BNqg6Un7/cPr6xYnPxRjp7w6mJ23cn5p1m6J7R88+dH42NjPdtIdr5Zhy76c+BKTH+VeAT6IO3XiZo01fl+/sPKebst69/7a98jVppqnw7Lss17uterIm0LNnE3m3NvpdtvrI6sfbf6oEr6zgd1rfR15U6jEPY9heW7+m/lL4caN9x4GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADA1ymE/wAD4NYVgbLoPQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACNSURBVDhPYxgFgwAwQmkw4Awo+ggU4Pv/n6EJxGdkZKj7z8DwieHfP2dGJqbTYLH//0u+buzvBbFBgAlKgwFIM5hm/M8LwjCx/0wMSiA2CPxnZJCGMsGAGUqDAYu6JRNQ47l/DP9PAG1+AtT85P9/xt0/NvZPZdWw4AcqOQE0YsfvGyfuQnSMgmEBGBgA0MwnM4Q1jVQAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Alignments, Type = Integer, Flags = &h0
		Left
		  Center
		Right
	#tag EndEnum

	#tag Enum, Name = BGStyles, Type = Integer, Flags = &h0
		Light
		  White
		  Dark
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum

	#tag Enum, Name = BorderStyles, Type = Integer, Flags = &h0
		Dark
		  Light
		  White
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum

	#tag Enum, Name = BorderTypes, Type = Integer, Flags = &h0
		None
		  All
		  Top
		  Right
		  Bottom
		  Left
		  NoTop
		  NoRight
		  NoBottom
		NoLeft
	#tag EndEnum

	#tag Enum, Name = ButtonStyles, Type = Integer, Flags = &h0
		Primary
		  Secondary
		  Success
		  Danger
		  Warning
		  Info
		  Light
		  Dark
		Link
	#tag EndEnum

	#tag Enum, Name = Corners, Type = Integer, Flags = &h0
		None
		  Rounded
		  Top
		  Right
		  Bottom
		  Left
		Circle
	#tag EndEnum

	#tag Enum, Name = IconPositions, Flags = &h0
		Left
		  Top
		  Right
		Bottom
	#tag EndEnum

	#tag Enum, Name = TextStyles, Type = Integer, Flags = &h0
		Dark
		  Light
		  White
		  Muted
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ToolbarStyle"
			Visible=true
			Group="Behavior"
			Type="BGStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Light"
				"1 - White"
				"2 - Dark"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
