#tag Class
Protected Class GraffitiWebCard
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "buttonClick"
		      Action()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		  
		  select case Name
		  case "Style"
		    UpdateOptions()
		    Return True
		  case "Enabled"
		    if LibrariesLoaded and isShown then
		      
		      dim strExec() as String
		      strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
		      strExec.Append( "theField.prop('disabled', " + Str( not Value.BooleanValue ).Lowercase + ");" )
		      Buffer( strExec )
		      
		      Return True
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  dim theAlign as String = "text-left"
		  select case Alignment
		  case Alignments.Left
		    theAlign = "text-left"
		  case Alignments.Right
		    theAlign = "text-right"
		  end select
		  
		  source.Append( "<style>#" + me.ControlID + "{overflow-x:hidden;overflow-y:auto;box-sizing:border-box}</style>" )
		  source.Append( "<div style='display:none;' id='" + me.ControlID + "' class='" + if(not IsNull( me.Style ), " " + me.Style.Name, "" ) + "'>" )
		  source.Append( "<div class='card " + theAlign + "' style='width:100%;height:100%;box-sizing:border-box'>" )
		  source.Append( "<div class='card-header' style='display:" + If( mHeader.Len > 0, "block", "none" ) + "'>" + EscapeString( mHeader ) + "</div>" )
		  source.Append( "<img class='card-img-top' style='display:none;' src='#'>" )
		  source.Append( "<div class='card-body'>" )
		  source.Append( "<h5 class='card-title' style='display:" + If( mTitle.Len > 0, "block", "none" ) + "'>" + EscapeString( mTitle ) + "</h5>" )
		  source.Append( "<h6 class='card-subtitle mb-2 text-muted' style='display:" + If( mSubtitle.Len > 0, "block", "none" ) + "'>" + EscapeString( mSubtitle ) + "</h6>" )
		  source.Append( "<p class='card-text' style='display:" + If( mBody.Len > 0, "block", "none" ) + "'>" + EscapeString( mBody ) + "</p>" )
		  source.Append( "<a href='#' class='btn btn-primary' style='margin-top:16px;display:" + If( mButtonCaption.Len > 0, "block", "none" ) + "'>" + EscapeString( mButtonCaption ) + "</a>" )
		  source.Append( "</div>" )
		  source.Append( "<div class='card-footer' style='display:" + If( mFooter.Len > 0, "block", "none" ) + "'>" + EscapeString( mFooter ) + "</div>" )
		  source.Append( "</div>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub ApplyBGStyle(toElement as String, newStyle as BGStyles)
		  dim arrStyles() as String = Array( "bg-primary", "bg-secondary", "bg-success", "bg-danger", "bg-warning", "bg-info", "bg-light", "bg-dark", "bg-white" )
		  
		  dim theStyle as String = BGStyleToString( newStyle )
		  
		  dim strExec() as String
		  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " " + toElement + "')" )
		  
		  for each s as String in arrStyles
		    strExec.Append( ".removeClass('" + s + "')" )
		  next
		  strExec.Append( ".addClass('" + theStyle + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ApplyTextStyle(toElement as String, newStyle as TextStyles)
		  dim arrStyles() as String = Array( "text-primary", "text-secondary", "text-success", "text-danger", "text-warning", "text-info", "text-light", "text-dark", "text-muted", "text-white" )
		  
		  dim theStyle as String = TextStyleToString( newStyle )
		  
		  dim strExec() as String
		  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " " + toElement + "')" )
		  
		  for each s as String in arrStyles
		    strExec.Append( ".removeClass('" + s + "')" )
		  next
		  strExec.Append( ".addClass('" + theStyle + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function BGStyleToString(theStyle as BGStyles) As String
		  select case theStyle
		  case BGStyles.Danger
		    Return "bg-danger"
		  case BGStyles.Dark
		    Return "bg-dark"
		  case BGStyles.Info
		    Return "bg-info"
		  case BGStyles.Light
		    Return "bg-light"
		  case BGStyles.Primary
		    Return "bg-primary"
		  case BGStyles.Secondary
		    Return "bg-secondary"
		  case BGStyles.Success
		    Return "bg-success"
		  case BGStyles.Warning
		    Return "bg-warning"
		  case BGStyles.White
		    Return "bg-white"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebPagination -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebPagination -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TextStyleToString(theStyle as TextStyles) As String
		  select case theStyle
		  case TextStyles.Danger
		    return "text-danger"
		  case TextStyles.Dark
		    return "text-dark"
		  case TextStyles.Info
		    return "text-info"
		  case TextStyles.Light
		    return "text-light"
		  case TextStyles.Muted
		    return "text-muted"
		  case TextStyles.Primary
		    return "text-primary"
		  case TextStyles.Secondary
		    return "text-secondary"
		  case TextStyles.Success
		    return "text-success"
		  case TextStyles.Warning
		    return "text-warning"
		  case TextStyles.White
		    return "text-white"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    strExec.Append( "var myButton = window.GSjQuery('#" + me.ControlID + " a.btn');" )
		    
		    strExec.Append( "myButton.on('click', function() { " )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonClick');" )
		    strExec.Append( "});" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		    
		    Buffer( "window.GSjQuery('#" + me.ControlID + "').css('display','block');" )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Action()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://getbootstrap.com/docs/4.0/components/
		
		MIT License
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAlignment
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAlignment = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " div.card');" )
			  strExec.Append( "theElement.removeClass('text-center').removeClass('text-right');" )
			  select case mAlignment
			  case Alignments.Center
			    strExec.Append( "theElement.addClass('text-center');" )
			  case Alignments.Right
			    strExec.Append( "theElement.addClass('text-right');" )
			  end select
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Alignment As Alignments
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBody
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBody = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " p.card-text');" )
			  strExec.Append( "theElement.html('" + EscapeString( ReplaceLineEndings( mBody, "<br />" )  ) + "');" )
			  strExec.Append( "theElement.css('display', '" + If( mBody <> "", "block", "none" ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Body As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBodyTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBodyTextStyle = value
			  
			  ApplyTextStyle( "p.card-text", mBodyTextStyle )
			End Set
		#tag EndSetter
		BodyTextStyle As TextStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mButtonCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mButtonCaption = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " a.btn');" )
			  strExec.Append( "theElement.html('" + EscapeString( ReplaceLineEndings( mButtonCaption, "<br />" ) ) + "');" )
			  strExec.Append( "theElement.css('display', '" + If( mButtonCaption <> "", "block", "none" ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		ButtonCaption As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mButtonStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mButtonStyle = value
			  
			  dim allStyles() as String = Array( "btn-primary", "btn-secondary", "btn-success", "btn-danger", "btn-warning", "btn-info", "btn-light", "btn-dark", "btn-link" )
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " a.btn');" )
			  
			  strExec.Append( "theElement" )
			  for each s as String in allStyles
			    strExec.Append( ".removeClass('" + s + "')" )
			  next
			  strExec.Append( ";" )
			  
			  strExec.Append( "theElement.addClass('" )
			  select case mButtonStyle
			  case ButtonStyles.Danger
			    strExec.Append( "btn-danger" )
			  case ButtonStyles.Dark
			    strExec.Append( "btn-dark" )
			  case ButtonStyles.Info
			    strExec.Append( "btn-info" )
			  case ButtonStyles.Light
			    strExec.Append( "btn-light" )
			  Case ButtonStyles.Link
			    strExec.Append( "btn-link" )
			  case ButtonStyles.Primary
			    strExec.Append( "btn-primary" )
			  case ButtonStyles.Secondary
			    strExec.Append( "btn-secondary" )
			  case ButtonStyles.Success
			    strExec.Append( "btn-success" )
			  case ButtonStyles.Warning
			    strExec.Append( "btn-warning" )
			  end select
			  strExec.Append( "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		ButtonStyle As ButtonStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCardStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCardStyle = value
			  
			  ApplyBGStyle( "div.card", mCardStyle )
			End Set
		#tag EndSetter
		CardStyle As BGStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFooter
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFooter = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " div.card-footer');" )
			  strExec.Append( "theElement.html('" + EscapeString( ReplaceLineEndings( mFooter, "<br />" ) ) + "');" )
			  strExec.Append( "theElement.css('display', '" + If( mFooter <> "", "block", "none" ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Footer As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFooterBGStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFooterBGStyle = value
			  
			  ApplyBGStyle( "div.card-footer", mFooterBGStyle )
			End Set
		#tag EndSetter
		FooterBGStyle As BGStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFooterTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFooterTextStyle = value
			  
			  ApplyTextStyle( "div.card-footer", mFooterTextStyle )
			End Set
		#tag EndSetter
		FooterTextStyle As TextStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHasImage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHasImage = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " img.card-img-top');" )
			  strExec.Append( "theElement.css('display', '" + If( mHasImage, "block", "none" ) + "');" )
			  
			  strExec.Append( "var cardElement = window.GSjQuery('#" + me.ControlID + "').css('overflow-y', 'hidden').css('overflow-y','auto');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		HasImage As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeader = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " div.card-header');" )
			  strExec.Append( "theElement.html('" + EscapeString( ReplaceLineEndings( mHeader, "<br />" ) ) + "');" )
			  strExec.Append( "theElement.css('display', '" + If( mHeader <> "", "block", "none" ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Header As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHeaderBGStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeaderBGStyle = value
			  
			  ApplyBGStyle( "div.card-header", mHeaderBGStyle )
			End Set
		#tag EndSetter
		HeaderBGStyle As BGStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHeaderTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeaderTextStyle = value
			  
			  ApplyTextStyle( "div.card-header", mHeaderTextStyle )
			End Set
		#tag EndSetter
		HeaderTextStyle As TextStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mImage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mImage = value
			  ImageCache = nil
			  
			  dim theURL as String
			  select case mImage.Type
			  case Variant.TypeString
			    theURL = mImage
			  case Variant.TypeObject
			    if mImage isa Picture then
			      dim p as Picture = mImage
			      theURL = "data:image/png;base64," + EncodeBase64( p.GetData( Picture.FormatPNG ), 0 )
			    ElseIf mImage isa WebPicture then
			      dim p as WebPicture = mImage
			      theURL = "data:image/png;base64," + EncodeBase64( p.Data, 0 )
			    elseif mImage isa WebFile then
			      dim wf as WebFile = mImage
			      theURL = wf.URL
			    ElseIf mImage isa FolderItem then
			      dim f as FolderItem = mImage
			      dim wf as WebFile = WebFile.Open( f, false )
			      ImageCache = wf
			      theURL = wf.URL
			    end if
			  end select
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " img.card-img-top');" )
			  strExec.Append( "theElement.prop('src', '" + EscapeString( theURL ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Image As Variant
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private ImageCache As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAlignment As Alignments
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBody As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBodyTextStyle As TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mButtonCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mButtonStyle As ButtonStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCardStyle As BGStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFooter As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFooterBGStyle As BGStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFooterTextStyle As TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHasImage As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeader As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeaderBGStyle As BGStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeaderTextStyle As TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mImage As Variant
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSubtitle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSubtitleTextStyle As TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTitle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTitleTextStyle As TextStyles
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSubtitle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSubtitle = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " h6.card-subtitle');" )
			  strExec.Append( "theElement.html('" + EscapeString( ReplaceLineEndings( mSubtitle, "<br />" ) ) + "');" )
			  strExec.Append( "theElement.css('display', '" + If( mSubtitle <> "", "block", "none" ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Subtitle As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSubtitleTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSubtitleTextStyle = value
			  
			  ApplyTextStyle( "h6.card-subtitle", mSubtitleTextStyle )
			End Set
		#tag EndSetter
		SubtitleTextStyle As TextStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTitle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTitle = value
			  
			  dim strExec() as String
			  strExec.Append( "var theElement = window.GSjQuery('#" + me.ControlID + " h5.card-title');" )
			  strExec.Append( "theElement.html('" + EscapeString( ReplaceLineEndings( mTitle, "<br />" ) ) + "');" )
			  strExec.Append( "theElement.css('display', '" + If( mTitle <> "", "block", "none" ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Title As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTitleTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTitleTextStyle = value
			  
			  ApplyTextStyle( "h5.card-title", mTitleTextStyle )
			End Set
		#tag EndSetter
		TitleTextStyle As TextStyles
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.card", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAJAAAACACAYAAADkkOAjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjIxMTM0NEJFNjhENTExRTg5MkM1QkI0MkM2QkQyRTIzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjIxMTM0NEJGNjhENTExRTg5MkM1QkI0MkM2QkQyRTIzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MjExMzQ0QkM2OEQ1MTFFODkyQzVCQjQyQzZCRDJFMjMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MjExMzQ0QkQ2OEQ1MTFFODkyQzVCQjQyQzZCRDJFMjMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4DrEOlAAAG2klEQVR42uydfWhWVRzHz2NPkpMsG6ZpUZLMzd5IK6NX+6MItlUWCUGLMvujgqgMDKG0VyiWhUFBG/5R/RFlRGlQU+gdmb2s0rYiJUrN7Q9Lsxy9bLffl3ueWk97nufeu3vPzjn3+4HvP9vdef3unHPveSsEQaAIScoEFgGhgQgNRGggQgMRQgMRGojQQCQXFCcvXl7pd3WiFlGraL5ouqieRZYL9okGRJ+KNoo2iAZHNVCFVmmpaLVoFssyl9RrzRO1ifaIVonWiYJqXdhk0XpRB81DRgAvdOqWaEolA8E874gWs7xIBZpFm7VX/mOggm6ezmYZkRrAIy+UvFMy0M2iJSwbEhH0UjeWDFSnB8yExOFB0SQY6AoOmEnCgXUrDNTKsiAJaYGBzozw4BbRKbrLK1Bea4Ku6y0RfLEAD8+I8OAyUa8q+4hEvCTQdb0swrMzYKCjIzzYx3LNHVHqfGqpS4riSpK/lqgWBc7GkzFBAxEaiNBAhAYieaSY4oicsAUihAYiNBChgQgNRAgNRGggQgMRGogQGojQQIQGIt5QtDRd32jhiJGfRMeo8HiZuVqEBvofu0VPit4Q7ajy3BwVboa8S3Q8q5Bd2G+iFaIG0Zoa5lH692v08yv035OcGggHFy0SPa4qnIBVhUH9d+eJvmdV5s9A2Hd0luiTMYbzpehcxb1ruTIQzuDDnvz+lMLr1+HtyyCt3aJTRYcp97ctIw+n6Tw5baDrRDtTDnOnDjdtcF7kV6JhDxoM5GG7Cs+DctZAr4s2ZRT2Jh1+2l2tb/S6aiD8B6zMOI6VKbcWPm4oCFw1UHea7q/y37WVw1s/B9FvehYPDWQ4vvcNxfNeimEVPKz3gqsG+tHBeHyce2t01UADDsbT6ZmJYJ6OtAIzPZnq4hvN+aKvOdqxowWa7lk8NJDh+GYaiuc4Vq2fBrrIUDwXs2r9NFCzZ/HQQIbjW2jgjQbhn8Oq9dNAWE7wSMZxPKrjIR4aCFwtuiSjsBEuL8zz3ED4jP6K6OSUwz1R9JLyc+qBBioDF7q+JpqWUngIB/d5HptBWrGC4HQVfnTNYpVgUYffbSA9ceOq3RrUXXX3eH4d/k50pWjbGMLAEk0sIpudURpxc02vgbJAPNsNpSdqXDUxNZXxl6hL9IEKl55OFK3Vlf6R6D7Rs6I/YoSJMG4VPSQ6UoUbEO/QYWDv2AWiy1LIo6kViX0G05NanrI2EFq3ddoge0dpirGJsEn0lOhOUbv+2a4qYZ6gwo2F94hOGlEg+Fn5nrKZ2mA3jWFsZKqFHjaYntRWbGbZhWHDX5se61TiKNFjolvKxmPo0jCBid0W+1V4JRXuNWvUXdbIgnhOdK/oQJV48Gb2ogrvh00y6DdFYDA9gc0GQpfVIno74vPzRMtF1+ruqBYH9Ztce4zm+HI90C7SQPYbCF3Wwwn+bpIeuyzULQ4mRaeIftFdIIT1zh+q+DtZwf2iBxK8qZroxgoRu5Y00hM1rnEx0A7dovxp4WeLifoNJs43qCZlZj1QU8S3qzTSEzWucfkO9ISl5lH6Da095t9gRWJDxulC+B2G0hMnLuMt0O8qXMx1QNkLukQseT1CEetaoC7LzaP0eGozq95OA613JN+vsurtNJAr/9ldrHr7DPSDMrfva6wgnbtZ/XYZqNuxvHez+u0y0DbH8v4Fq9++LswldrH6aaA8pdd7A7k2KN0T8TnMvZ2hsluRaFJFnZetNhroV8cMdDDic1hLhJNghzxoMIZ0XpbaaKBBxwrzUMTnfDxYoc9GAx1yrBCjGn7YQwMN22gg1wgUscpAUx3Lez2r3y4DzXEs71HTyzMSDRnItSNVoqa3wUMDzbXRQDc49N+KfLdFfLbTwda1Vstr5RmJcDV2VbzsQCEuidGyYJH/txztmHkLe1rZfz4h0reWVW+ngXC4QZfFbzjYoIhT7Kex6u00EMDpDx+LLrUsr9gn3yNawGpPj6z2xs/WLRGmAd5V4cTleGz1OVw0S4XXajayut0xUIlGVhy7MEJoIEIDERqI0ECE0ECEBiI0EKGBCA3EtcEkMQEMtJ/lQBLyMww0wHIgCdkLA/WwHEhCemCgDSwHkpCNMBDupuhnWZC43VfJQLjTYjXLg8RkFbxT+g7UoVsiQqLwlgpvYfrnQyI2218v+pxlQ2oAj2Bb1NBIAwGcl3OhCm//I6RSy7NIjThbqXwqA4dEXSO6XfH7EPkXeOE2UbMqu4lgtEX1aJqeET2vwovacO/XfBXe/lfHsswFOOsJZ2l/hjctFV4aOOoJdIUg4FQYSQ5n4wkNRGggQgMRGogQGojQQIQGIjQQITQQyZa/BRgAExgtXcsu1mIAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMyNkZCMTQxNjhENTExRTg5M0JBRDA4RjUyM0Y2ODYxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMyNkZCMTQyNjhENTExRTg5M0JBRDA4RjUyM0Y2ODYxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzI2RkIxM0Y2OEQ1MTFFODkzQkFEMDhGNTIzRjY4NjEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzI2RkIxNDA2OEQ1MTFFODkzQkFEMDhGNTIzRjY4NjEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7CB/+eAAAApklEQVR42mL8//8/AyWAkWIDuAKKlgPpcBCbRL0gm5ezQDWzAfEfEg0A6f3JArX5N7leYELj3wbiPiC+Q64BHkBcDKVhgAvqSmTMhewPlEBFo0HgG7EuWAPE6kDsCMRqUD6yC7iQ+FhdAHL6IyT+FSAOweKCb7hc8AFN4QdSw2AeEJ9G4psREwYs0BQFMjUYikkB/0BeWAE1hIGMpLyCccBzI0CAAQDXBSguSl932gAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Alignments, Type = Integer, Flags = &h0
		Left
		  Center
		Right
	#tag EndEnum

	#tag Enum, Name = BGStyles, Type = Integer, Flags = &h0
		Light
		  White
		  Dark
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum

	#tag Enum, Name = ButtonStyles, Type = Integer, Flags = &h0
		Primary
		  Secondary
		  Success
		  Danger
		  Warning
		  Info
		  Light
		  Dark
		Link
	#tag EndEnum

	#tag Enum, Name = TextStyles, Type = Integer, Flags = &h0
		Dark
		  Light
		  White
		  Muted
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Alignment"
			Visible=true
			Group="Content"
			InitialValue="1"
			Type="Alignments"
			EditorType="Enum"
			#tag EnumValues
				"0 - Left"
				"1 - Center"
				"2 - Right"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Header"
			Visible=true
			Group="Content"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Footer"
			Visible=true
			Group="Content"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasImage"
			Visible=true
			Group="Content"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Visible=true
			Group="Content"
			InitialValue="Untitled"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Subtitle"
			Visible=true
			Group="Content"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Body"
			Visible=true
			Group="Content"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ButtonCaption"
			Visible=true
			Group="Content"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CardStyle"
			Visible=true
			Group="Appearance"
			Type="BGStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Light"
				"1 - White"
				"2 - Dark"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderBGStyle"
			Visible=true
			Group="Appearance"
			Type="BGStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Light"
				"1 - White"
				"2 - Dark"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderTextStyle"
			Visible=true
			Group="Appearance"
			Type="TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="FooterBGStyle"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="BGStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Light"
				"1 - White"
				"2 - Dark"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="FooterTextStyle"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TitleTextStyle"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="SubtitleTextStyle"
			Visible=true
			Group="Appearance"
			InitialValue="3"
			Type="TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BodyTextStyle"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="ButtonStyle"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="ButtonStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Primary"
				"1 - Secondary"
				"2 - Success"
				"3 - Danger"
				"4 - Warning"
				"5 - Info"
				"6 - Light"
				"7 - Dark"
				"8 - Link"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
