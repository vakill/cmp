#tag Class
Protected Class GraffitiWebWall
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "resized"
		      
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '// 
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  Buffer( "window.GSjQuery('#" + me.ControlID + "_inner').masonry();" )
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<style>" + ReplaceAll( cssMain, "ContID", self.ControlID ) + "</style>" )
		  
		  source.Append( "<div id='" + me.ControlID + "'>" )
		  source.Append( "<div id='" + me.ControlID + "_inner'></div>" )
		  source.append( "</div>")
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateDisplay()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddPanel(thePanel as GraffitiWebWallPanel)
		  if IsNull( thePanel  ) then Return
		  if IsNull( thePanel.Child ) then Return
		  
		  dim strExec() as String
		  
		  strExec.Append( "var wallInner = window.GSjQuery('#" + me.ControlID + "_inner').append('" + EscapeString( PreparePanel( thePanel ) ) + "');" )
		  
		  strExec.Append( "function append" + thePanel.Child.ControlID + " () {" )
		  strExec.Append( "var theElement = window.GSjQuery('#" + thePanel.Child.ControlID + "');" )
		  strExec.Append( "if (typeof(theElement) !== 'undefined') {" )
		  strExec.Append( "theElement.detach().appendTo(window.GSjQuery('#" + thePanel.Child.ControlID + "cell')).css('left','0px').css('top','0px');" )
		  
		  strExec.Append( "var theCell = window.GSjQuery('#" + thePanel.Child.ControlID + "cell');" )
		  strExec.Append( "window.GSjQuery(window).resize(function() {" )
		  strExec.Append( "var theItem = window.GSjQuery('#" + thePanel.Child.ControlID + "cell');" )
		  strExec.Append( "var thePanel = window.GSjQuery('#" + thePanel.Child.ControlID + "');" )
		  strExec.Append( "thePanel.css('width', theItem.width()).css('height', theItem.height());" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "} else {" )
		  strExec.Append( "setInterval( append" + thePanel.Child.ControlID + ", 10 );" )
		  strExec.Append( "}" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "append" + thePanel.Child.ControlID + "();" )
		  
		  if not LockUpdate then
		    Buffer( strExec )
		    UpdateDisplay()
		  else
		    UpdateBuffer.Append( Join( strExec, "" ) )
		  end if
		  
		  Panels.Append( thePanel )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColorToString(c as Color) As String
		  Return "rgba(" + _
		  Str( c.Red, "###" ) + "," + _
		  Str( c.Green, "###" ) + "," + _
		  Str( c.Blue, "###" ) + "," + _
		  Str( 1 - (c.Alpha / 255), "0.00" ) + ")"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIndex(thePanel as GraffitiWebWallPanel) As Integer
		  if IsNull( thePanel ) then Return - 1
		  
		  if not IsNull( thePanel ) then Return panels.IndexOf( thePanel )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetPanel(panelIndex as Integer) As GraffitiWebWallPanel
		  if panelIndex >= 0 and panelIndex <= Panels.Ubound then Return Panels( panelIndex )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebWallr -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwwall.min.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebWall -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InsertPanel(AtIndex as Integer, thePanel as GraffitiWebWallPanel)
		  if AtIndex >= PanelCount then
		    AddPanel( thePanel )
		    Return
		  end if
		  
		  if IsNull( thePanel  ) then Return
		  if IsNull( thePanel.Child ) then Return
		  
		  if AtIndex < 0 then Return
		  
		  dim beforePanel as GraffitiWebWallPanel = Panels(AtIndex)
		  
		  if IsNull( beforePanel ) then Return
		  if IsNull( beforePanel.Child ) then Return
		  
		  dim strExec() as String
		  
		  strExec.Append( "window.GSjQuery('" + EscapeString( PreparePanel( thePanel ) ) + "').insertBefore('#" + beforePanel.Child.ControlID + "cell');" )
		  
		  strExec.Append( "function insert" + thePanel.Child.ControlID + " () {" )
		  strExec.Append( "var theElement = window.GSjQuery('#" + thePanel.Child.ControlID + "');" )
		  strExec.Append( "if (typeof(theElement) !== 'undefined') {" )
		  strExec.Append( "theElement.detach().appendTo(window.GSjQuery('#" + thePanel.Child.ControlID + "cell')).css('left','0px').css('top','0px');" )
		  strExec.Append( "} else {" )
		  strExec.Append( "setInterval( append" + thePanel.Child.ControlID + ", 10 );" )
		  strExec.Append( "}" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "insert" + thePanel.Child.ControlID + "();" )
		  
		  Buffer( strExec )
		  
		  if not LockUpdate then UpdateDisplay()
		  
		  Panels.Insert( AtIndex, thePanel )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function OverflowToString(theValue as GraffitiWebWallPanel.OverflowVisibility) As String
		  Select case theValue
		  case GraffitiWebWallPanel.OverflowVisibility.Auto
		    Return "auto"
		  case GraffitiWebWallPanel.OverflowVisibility.Hidden
		    Return "hidden"
		  case GraffitiWebWallPanel.OverflowVisibility.Scroll
		    Return "scroll"
		  case else
		    Return "visible"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PanelCount() As Integer
		  Return Panels.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function PreparePanel(ChildPanel as GraffitiWebWallPanel) As String
		  dim strOut() as String
		  strOut.Append( "<div id='" + ChildPanel.Child.ControlID + "cell' class='graffitiwebwallcell' " )
		  strOut.Append( "style='" )
		  if ChildPanel.PercentWidth > 0 then
		    strOut.Append( "width:" + Str( ChildPanel.PercentWidth ) + "%;" )
		  else
		    strOut.Append( "width:" + Str( ChildPanel.Child.Width ) + "px;" )
		  end if
		  strOut.Append( "height: " + Str( ChildPanel.Child.Height ) + "px;" )
		  strOut.Append( "background-color: " + ColorToString( ChildPanel.Background ) + ";" )
		  strOut.Append( "overflow: " + OverflowToString( ChildPanel.OverFlow ) + ";" )
		  strOut.Append( "position: absolute;'" )
		  strOut.Append( "data-width='" + Str( ChildPanel.Child.Width ) + "'; " )
		  strOut.Append( "data-height='" + Str( ChildPanel.Child.Height ) + "'></div>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  LockUpdate = True
		  
		  for intCycle as Integer = PanelCount - 1 DownTo 0
		    RemovePanel( intCycle )
		  next
		  
		  LockUpdate = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemovePanel(thePanel as GraffitiWebWallPanel)
		  if not IsNull( thePanel ) then RemovePanel( Panels.IndexOf( thePanel ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemovePanel(panelIndex as Integer)
		  if panelIndex < 0 or panelIndex > Panels.Ubound then Return
		  
		  dim strExec() as String
		  
		  dim thePanel as GraffitiWebWallPanel = Panels( panelIndex )
		  if IsNull( thePanel  ) then Return
		  if IsNull( thePanel.Child ) then Return
		  
		  strExec.Append( "window.GSjQuery('#" + thePanel.Child.ControlID + "').detach().css('left', '-" + Str( thePanel.Child.Width ) + "px').css('top', '-" + Str( thePanel.Child.Height ) + "px');" )
		  strExec.Append( "window.GSjQuery('#" + thePanel.Child.ControlID + "cell').detach();" )
		  
		  Buffer( strExec )
		  
		  Panels.Remove( panelIndex )
		  
		  if not LockUpdate then UpdateDisplay()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateDisplay()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').masonry('reloadItems');" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').masonry('layout');" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').masonry();" )
		  Buffer( strExec )
		  
		  GutterHeight = mGutterHeight
		  GutterWidth = mGutterWidth
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if hasLoaded then
		    dim strExec() as String
		    
		    dim intCycle as Integer
		    dim intMax as Integer = UBound( Panels )
		    
		    dim currChild as GraffitiWebWallPanel
		    for intCycle = 0 to intMax
		      currChild = Panels(intCycle)
		      strExec.Append( "var theOld" + currChild.Child.ControlID + " = window.GSjQuery('#" + currChild.Child.ControlID + "');" )
		      strExec.Append( "theOld" + currChild.Child.ControlID + ".detach().appendTo('body');" )
		      strExec.Append( "var oldElement = window.GSjQuery('#" + currChild.Child.ControlID + "cell');" )
		      strExec.Append( "if (typeof(oldElement) !== 'undefined') {" )
		      strExec.Append( "oldElement.detach();" )
		      strExec.Append( "}" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').append('" + EscapeString( PreparePanel( currChild ) ) + "');" )
		      strExec.Append( "var element_" + me.ControlID + " = window.GSjQuery('#" + currChild.Child.controlID + "');" )
		      strExec.Append( "if( typeof(element_" + me.ControlID + ") !== 'undefined') {" )
		      strExec.Append( "thisCell = window.GSjQuery('#" + currChild.Child.ControlID + "cell');" )
		      strExec.Append( "window.GSjQuery('#" + currChild.Child.ControlID + "cell').append(element_" + me.ControlID + ");" )
		      strExec.Append( "}" )
		      strExec.Append( "window.GSjQuery('#" + currChild.Child.ControlID + "').css('position', 'relative').css('left', '0px').css('right', '0px').css('top', '0px');" )
		    next
		    
		    strExec.Append( "window.GSjQuery('.graffitiwebwallcell').css('margin-bottom', '" + Str( GutterHeight, "#" ) + "px').css('margin-left','" + Str( GutterWidth ) + "px');" )
		    
		    strExec.Append( "window.masonry_" + me.ControlID + " = window.GSjQuery('#" + me.ControlID + "_inner').masonry({" + _
		    "percentPosition: true," + _
		    "selector: '.graffitiwebwallcell'," + _
		    "columnWidth: 1," + _
		    "gutter: " + Str( mGutterWidth, "#" ) + _
		    "});" )
		    
		    RunBuffer()
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://masonry.desandro.com/
		
		Dual licensed under MIT.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBackgroundColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBackgroundColor = value
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + "').css('background-color', '" + ColorToRGBAString( value ) + "');" )
			  
			End Set
		#tag EndSetter
		BackgroundColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBackgroundImage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBackgroundImage = value
			  
			  dim strValue as String
			  if not IsNull( value ) then
			    strValue = "data:image/png;base64," + ReplaceLineEndings( EncodeBase64( value.GetData( Picture.FormatPNG, 100 ) ), "" )
			    strValue = "url(\'" + strValue + "\')"
			  else
			    strValue = ""
			  end if
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + "').css('background-image', '" + strValue + "').css('background-size', 'cover');" )
			End Set
		#tag EndSetter
		BackgroundImage As Picture
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mGutterHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mGutterHeight = value
			  
			  dim strExec() as String
			  strExec.Append( "window.GSjQuery('.graffitiwebwallcell').css('margin-bottom', '" + Str( mGutterHeight, "#" ) + "px');" )
			  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').masonry();" )
			  
			  Buffer( strExec )
			  
			End Set
		#tag EndSetter
		GutterHeight As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mGutterWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mGutterWidth = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		GutterWidth As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isInit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLockUpdate = value
			  
			  
			  if not mLockUpdate then
			    Buffer( UpdateBuffer )
			    UpdateDisplay()
			  end if
			End Set
		#tag EndSetter
		LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mBackgroundColor As Color
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBackgroundImage As Picture
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mGutterHeight As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mGutterWidth As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Panels() As GraffitiWebWallPanel
	#tag EndProperty

	#tag Property, Flags = &h21
		Private UpdateBuffer() As String
	#tag EndProperty


	#tag Constant, Name = cssMain, Type = String, Dynamic = False, Default = \"#ContID {\roverflow-x: hidden;\roverflow-y: auto;\r}\r.graffitiwebcell {\rposition: absolute;\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = htmlCells, Type = String, Dynamic = False, Default = \"<div id\x3D\'{ID}\' class\x3D\'graffitiwebwallcell\' style\x3D\'width:{width}px; height: {height}px; background-color: {color};position: absolute;\xE2\x80\x99 data-width\x3D\'{width}\' data-height\x3D\'{height}\'></div>", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.wall", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAARjSURBVHhe7d1PaFxVFAbwc95rsEmxsVj8s7AoXVSK4p8uRLGICxdKzLTQuAi6E1woSqaUuotZ1YVNArooXbgWahlrpC4Ug4JUaulGRUWx7UIQsYrVzHRsfMc79myUN5nb3DfJnXu+H5R37l2k73t8fZOhmRcCAAAAAAAAAAAAAAAAAAAAAACAVLAeK3fj+IHr/9jQvkWXa4av5NK+tPk8fTyzrFvBUsryf5UWYOPYi9s4z6eZ6VH3pe/Q7fVyxv053nxn9tWry2uTUpaVVFaATbWpSWF+043XXd2JgxB9kTFPLjUOf6lbPaWUpZdKCjA8vn8fZ3JMlzE6z8wPuAv3s667SimLj0yPqzZae+kGyoqjuozV7VLI6zp3lVIWX8EF+Iuyg0y8RZfxYnpq4576w7oqlVIWX8EFcLej3TpGj0me1LFUSll8BRdASHboGD0Wuk3HUill8RV+ByDequMA4BEdSqWUxVdwAWCwoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGoQDGBRdARH7XcQBIW4dSKWXxFX4HYPpBp0FwUY/lUsriKbgALLSoY/Q444aOpVLK4iu4AH8X2Rs6Rs3d3j9aasx+oMtSKWXxFVyA9sLhc8L0gi6jJESXOKO6LrtKKYuvXI9Blr859fmGHQ/+ykyP61Y8hH6UnPe1GrOndWdFKWXxUUkBOpa/PXV66M6H3nNneat7hVr3D1kKyS/uNf1INrT8TPP4/Ne67SWlLL305SFRWyYOjrYut+/iPL9Zt9ZOUQjncqHZmD+rO0FSygIAAAAAANCnt4Eje6fuce9b7xbhNX++bkHub+08TPFK/mnz5Gs/6faqpZSlTKUFGK7V9xDLNBPfq1vrTN5253NoNe+jU8qyksoKMFybOsTML+syIiIF0+TlxtxbutFTSll6qaQA7oK94i7YtC6jxEXx2NK78x/qsquUsvgI/t/AkfH6fbFfsA7JuOej1lPK4iv8J4IyelanyPGuf1/XV5JSFk/BBRDp/EKFAcE0plOplLL4Ci4Ak2zTMXruXEd1LJVSFl/hLwHMm3SKn/CQTuVSyuIpvAAw0FAA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA41AA44ILICJ9+dRqPwjLnzqWSimLr/A7ANNXOkWPib/TsVxKWTwFF4AL+kTH+HGxoFOplLL4quTTwSO1+vfuK23XZZTc7f1o68Tcc7rsKqUsPir5JlAKedodKnl+fT8I0aLvBUspi49KCtBamPtMCn7End453YqG+9fyfuu3i0/osqeUsvio9BExHcN768+7mo65s72fmW/S7bUlckGYFlnoWPPE3EndvWYpZemm8gL8x8REZQ+j9rZzp9DMTKGr6qSUBQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABhfRPwgSsyzHd4EsAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACzSURBVDhPYxhwwAilGbgCClOBlCaEB5T4z/D068b+XlziUC6yAUX/oUwk8M+dgYFpJ5SDBP65f9swYReIxQTm4wD/GBgEoEwUgCyO1wBiANyA/wz/dwKJ4zAM5v/5dwKXOFQbchgUuKE57QPIn7jEoVyEAZwBRR+BHD4oFwz+//tnysDEtBeb+PdNE86A2HAvoCsCgf9MDEq4xKFM6gbiNSgTDICJ4hM0ELGKQ7mjgIGBAQCbqFqjDR6giQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="BackgroundColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackgroundImage"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GutterHeight"
			Visible=true
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GutterWidth"
			Visible=true
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockUpdate"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
