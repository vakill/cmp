#tag Class
Protected Class GraffitiWebWallPanel
	#tag Method, Flags = &h0
		Sub Constructor(BGColor as Color, myChild as WebControl)
		  Background = BGColor
		  Child = myChild
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Background As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		Child As WebControl
	#tag EndProperty

	#tag Property, Flags = &h0
		Overflow As OverflowVisibility
	#tag EndProperty

	#tag Property, Flags = &h0
		PercentWidth As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty


	#tag Enum, Name = OverflowVisibility, Type = Integer, Flags = &h0
		Visible
		  Auto
		  Hidden
		Scroll
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Background"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
