#tag Class
Protected Class GraffitiWebSlider
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "change"
		      if Parameters.Ubound >= 0 then
		        mValue = Parameters(0).DoubleValue
		        UpdateStyles()
		        RaiseEvent Change()
		      end if
		    case "changeRange"
		      if Parameters.Ubound >= 1 then
		        mValue = Parameters(0).doubleValue
		        mValueHigh = Parameters(1).doubleValue
		        UpdateStyles()
		        RaiseEvent Change()
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    if Value.BooleanValue then
		      Buffer( "window.slider" + me.ControlID + ".enable();" )
		    else
		      Buffer( "window.slider" + me.ControlID + ".disable();" )
		    end if
		    
		    Return True
		  case "style"
		    dim strExec() as String
		    dim sVal as WebStyle = value
		    
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "_inner');" )
		    if not IsNull( value ) then
		      if not IsNull( oldStyle ) then strExec.Append( "theButton.removeClass('" + oldStyle.Name + "');" )
		      
		      if not sVal.isdefaultstyle and sVal.Name <> "_Style" and sVal.Name <> "_DefaultStyle" then
		        strExec.Append( "theButton.addClass('" + sval.Name + "');" )
		      end if
		    end if
		    
		    oldStyle = value
		    
		    Buffer( strExec )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim strExec() as String
		  strExec.Append( "<div id='" + me.ControlID + "' class='graffitiWebSlider' style='overflow:visible;box-sizing:border-box;display:none;'>" )
		  strExec.Append( "<input type='text' name='" + me.Name + "' id='" + me.ControlID + "_inner' style='width:100%;height:100%;'>" )
		  strExec.Append( "</div>" )
		  
		  Return Join( strExec )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  '// Do not implement
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddRangeStyle(startValue as Double, endValue as Double, Optional theStyle as WebStyle)
		  dim d as new Dictionary
		  d.Value( "start" ) = startValue
		  d.Value( "end" ) = endValue
		  d.Value( "style" )  = theStyle
		  
		  rangeStyles.Append( d )
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddTick(tickPosition as Integer, tickLabel as String)
		  dim d as new Dictionary
		  d.Value( "pos" ) = tickPosition
		  d.Value( "label" ) = tickLabel
		  Ticks.Append( d )
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function BuildFormatString() As String
		  if Precision <= 0 then Return "0"
		  
		  dim strRet as String = "0."
		  for intCycle as Integer = 1 to mPrecision
		    strRet = strRet + "0"
		  next
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub getTicks(byRef tickVals as String, byRef tickLabels as String)
		  dim tickPos() as String
		  dim tickLabel() as String
		  
		  dim intCycle as Integer = 0
		  dim intMax as Integer = ticks.Ubound
		  
		  dim sortedDics() as Dictionary = SortTickDictionary()
		  dim currTick as Dictionary
		  
		  for intCycle = 0 to intMax
		    currTick = Ticks(intCycle)
		    tickPos.Append( Str( currTick.Value( "pos" ) ) )
		    tickLabel.Append( "'" + currTick.Value( "label" ) + "'" )
		  next
		  
		  tickVals = "[" + Join( tickPos, "," ) + "]"
		  tickLabels = "[" + Join( tickLabel, "," ) + "]"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function HandleStyleToString() As String
		  select case mHandleStyle
		  case HandleStyles.Square
		    Return "square"
		  case HandleStyles.Triangle
		    Return "triangle"
		  case else
		    Return "round"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebSlider -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "slider" ).Child( "bootstrap-slider.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "slider" ).Child( "bootstrap-slider.min.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<style>.graffitiWebSlider div.slider {overflow:visible;}</style>" )
		  
		  strOut.Append( "<!-- END GraffitiWebSlider -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function OptionsToString() As String
		  dim formatString as String = BuildFormatString()
		  
		  dim strExec() as String
		  strExec.Append( "{" )
		  strExec.Append( "id: '" + me.Name + "'," )
		  strExec.Append( "enabled: " + Str( Enabled ).Lowercase + "," )
		  strExec.Append( "precision: " + Str( mPrecision ) + "," )
		  strExec.Append( "step: " + Str( mStepValue, formatString ) + "," )
		  strExec.Append( "min: " + Str( mMinValue, formatString ) + "," )
		  strExec.Append( "max: " + Str( mMaxValue, formatString ) + "," )
		  strExec.Append( "orientation: '" + if( me.Width > me.Height, "horizontal", "vertical" ) + "'," )
		  strExec.Append( "handle: '" + HandleStyleToString() + "'," )
		  strExec.Append( "reversed: " + Str( mReversed ).Lowercase + "," )
		  strExec.Append( "scale: '" + if( mLogarithmicScale, "logarithmic", "linear" ) + "'," )
		  strExec.Append( "focus: " + Str( mAcceptFocus ).Lowercase + "," )
		  strExec.Append( "range: " + Str( mRange ).Lowercase + "," )
		  strExec.Append( "tooltip: '" + TooltipToString() + "'," )
		  strExec.Append( "ticks_snap_bounds: " + Str( mTicksSnapRadius, formatString ) + "," )
		  
		  if Range then
		    strExec.Append( "value: [" + Str( mValue, formatString ) + "," + Str( mValueHigh, formatString ) + "]," )
		    strExec.Append( "tooltip_split: true," )
		  else
		    strExec.Append( "value: " + Str( mValue, formatString ) + "," )
		    strExec.Append( "tooltip_split: false," )
		  end if
		  
		  if rangeStyles.Ubound >= 0 then
		    strExec.Append( "rangeHighlights: [" + rangeStylesToString() + "]," )
		  end if
		  
		  if Ticks.Ubound >= 0 then
		    dim tickVal, tickLabel as String
		    getTicks( tickVal, tickLabel )
		    
		    strExec.Append( "ticks: " + tickVal + "," )
		    strExec.Append( "ticks_positions: " + tickVal + "," )
		    strExec.Append( "ticks_labels: " + tickLabel + "," )
		  end if
		  
		  dim ttPos as String
		  select case TooltipPosition
		  case TooltipPositions.TopLeft
		    ttPos = if( me.Width > me.Height, "top", "left" )
		  case else
		    ttPos = if( me.Width > me.Height, "bottom", "right" )
		  end select
		  strExec.Append( "tooltip_position: '" + ttPos + "'" )
		  
		  strExec.Append( "}" )
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RangeStylesToString() As String
		  dim strOut() as String
		  
		  dim formatString as String = BuildFormatString()
		  
		  dim intCycle as Integer
		  dim intMax as Integer = rangeStyles.Ubound
		  
		  dim currDic as Dictionary
		  for intCycle = 0 to intMax
		    currDic = rangeStyles(intCycle)
		    strOut.Append( "{'start':" + Str( currDic.Value( "start" ).DoubleValue, formatString ) + "," + _
		    "'end':" + Str( currDic.Value( "end" ).DoubleValue, formatString ) + _
		    if( currDic.Value( "style" ) <> nil, "," + "'class':'" + WebStyle( currDic.Value( "style" ) ).Name + "'", "" ) + _
		    "}" )
		  next
		  
		  Return Join( strOut, "," )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAllRangeStyles()
		  ReDim rangeStyles(-1)
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAllTicks()
		  redim ticks(-1)
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveRangeStyle(rangeStart as Double)
		  dim intCycle as Integer
		  dim intMax as Integer = rangeStyles.Ubound
		  
		  dim currDic as Dictionary
		  for intCycle = 0 to intMax
		    if currDic.Value( "start" ) = rangeStart then
		      rangeStyles.Remove( intCycle )
		      Return
		    end if
		  next
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveTick(tickPositon as Integer)
		  dim intCycle as Integer
		  dim intMax as Integer = ticks.Ubound
		  
		  dim currTick as Dictionary
		  
		  for intCycle = 0 to intMax
		    currTick = ticks(intCycle)
		    if currTick.Value( "pos" ).IntegerValue = tickPositon then
		      ticks.Remove(intCycle)
		      Return
		    end if
		  next
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SortTickDictionary() As Dictionary()
		  dim intPos() as Integer
		  dim dicHold() as Dictionary
		  
		  dim intCycle as Integer
		  dim intMax as Integer = ticks.Ubound
		  
		  dim currDic as Dictionary
		  
		  for intCycle = 0 to intMax
		    currDic = Ticks(intCycle)
		    intPos.Append( currDic.Value( "pos" ) )
		    dicHold.Append( currDic )
		  next
		  
		  intPos.SortWith( dicHold )
		  
		  Return dicHold
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TooltipToString() As String
		  select case TooltipVisibility
		  case TooltipVisibilities.Always
		    Return "always"
		  case TooltipVisibilities.Hide
		    Return "hide"
		  case else
		    Return "show"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  dim strExec() as String
		  
		  if not hasLoaded then Return
		  
		  strExec.Append( "if( typeof(window.slider" + me.ControlID + ") !== 'undefined' ) {" )
		  strExec.Append( "window.slider" + me.ControlID + ".destroy();" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "window.slider" + me.ControlID + " = new Slider('#" + me.ControlID + "_inner', " + OptionsToString() + ");" )
		  
		  strExec.Append( "window.slider" + me.ControlID + ".on('" + if( mRealtimeChange, "change", "slideStop" ) + "', function(e) {" )
		  strExec.Append( "var oldVal = e.oldValue;" )
		  strExec.Append( "var newVal = e.newValue;" )
		  strExec.Append( "if( Array.isArray(newVal) == true ) {" )
		  strExec.Append( "if( newVal[0] != oldVal[0] || newVal[1] != oldVal[1] ) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'changeRange', newVal);" )
		  strExec.Append( "}" )
		  strExec.Append( "} else {" )
		  strExec.Append( "if( oldVal != newVal ) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'change', [newVal]);" )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  strExec.Append( "});" )
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  
		  RunBuffer()
		  
		  UpdateStyles()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateStyles()
		  StyleHandle = mStyleSliderHandle
		  StyleSelection = mStyleSliderSelection
		  StyleTickLabel = mStyleTickLabel
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Change()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		https://github.com/seiyria/bootstrap-slider
		
		bootstrap-slider is released under the MIT License
		Copyright (c) 2017 Kyle Kemp, Rohit Kalkur, and contributors
		
		Permission is hereby granted, free of charge, to any person
		obtaining a copy of this software and associated documentation
		files (the "Software"), to deal in the Software without
		restriction, including without limitation the rights to use,
		copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the
		Software is furnished to do so, subject to the following
		conditions:
		
		The above copyright notice and this permission notice shall be
		included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
		EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
		OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
		NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
		HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
		WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
		FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
		OTHER DEALINGS IN THE SOFTWARE.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAcceptFocus
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAcceptFocus = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AcceptFocus As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHandleStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHandleStyle = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		HandleStyle As HandleStyles
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLogarithmicScale
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLogarithmicScale = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		LogarithmicScale As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAcceptFocus As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMaxValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMaxValue = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		MaxValue As Double
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mHandleStyle As HandleStyles
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMinValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMinValue = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		MinValue As Double
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLogarithmicScale As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMaxValue As Double = 10
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMinValue As Double = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPrecision As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRange As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRealtimeChange As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReversed As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStepValue As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleSliderHandle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleSliderSelection As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleTickLabel As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTicksSnapRadius As Double = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipPosition As TooltipPositions
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipVisibility As TooltipVisibilities
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Double = 2
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValueHigh As Double = 7
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPrecision
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPrecision = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Precision As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRange
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRange = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Range As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private rangeStyles() As Dictionary
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRealtimeChange
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRealtimeChange = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		RealtimeChange As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mReversed
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReversed = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Reversed As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStepValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStepValue = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StepValue As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleSliderHandle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleSliderHandle ) then Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-handle').removeClass('" + mStyleSliderHandle.Name + "');" )
			  
			  mStyleSliderHandle = value
			  
			  if not IsNull( mStyleSliderHandle ) then Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-handle').addClass('" + value.Name + "');" )
			End Set
		#tag EndSetter
		StyleHandle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleSliderSelection
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleSliderSelection ) then
			    Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-selection').removeClass('" + mStyleSliderSelection.Name + "');" )
			    Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-tick').removeClass('" + mStyleSliderSelection.Name + "');" )
			  end if
			  
			  mStyleSliderSelection = value
			  
			  if not IsNull( mStyleSliderSelection ) then
			    Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-selection').addClass('" + value.Name + "');" )
			    Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-tick.in-selection').addClass('" + value.Name + "');" )
			  end if
			End Set
		#tag EndSetter
		StyleSelection As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleTickLabel
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleTickLabel ) then Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-tick-label').removeClass('" + mStyleTickLabel.Name + "');" )
			  
			  mStyleTickLabel = value
			  
			  if not IsNull( mStyleTickLabel ) then Buffer( "window.GSjQuery('#" + me.ControlID + "').find('.slider-tick-label').addClass('" + value.Name + "');" )
			End Set
		#tag EndSetter
		StyleTickLabel As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Ticks() As Dictionary
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTicksSnapRadius
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTicksSnapRadius = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TicksSnapRadius As Double
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipPosition
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipPosition = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TooltipPosition As TooltipPositions
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipVisibility
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipVisibility = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		TooltipVisibility As TooltipVisibilities
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Value As Double
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValueHigh
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValueHigh = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ValueHigh As Double
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webslider", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAASISURBVHhe7d1NiBtlGAfw95nNbjexKrTWgyxWD9oIsmIv3YII9aA9NXFhPVXwA49FklBaLN5qqaWZ1A8KQkUFvbTiZipeLAXBigve/GoWih+nivSi7WZsNzuPb/DxUrYh2TzTvLPv/wchz/NA393t/JlkMjO7BgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsobk2RmFcuUVZjNHRA/KaGDM/CsFdLo9H56SEdyCUwHIl2tvkOHXpB0aGzoSN+uHpIVVOBOAO/e8um0lGGtJq2YsWSlePfvWorRwk0CeR24lCJ6UUlVa664XzgTA7q83SaUrrXXXCXcCACOBAHhO/U1gvlT5WMq+xVFjb6FUOWCIjspID/PBdtR4cy3fl3PILMTNxrvSqVAPQKFcZSn7w/y73UAPpB0Au/5vdv2tMs0kNnzcBmC/tCrwEuA5BMBzCIDnEADPIQCeS+Mo4Dsp+8PmejsKn0j/KKB6wf60G2SaSfbw6nzcDA9Kq8KZk0FpB0A6uAleAjyHAHjOmQAwm1hKVWmtu144EwAKgm+kVJXWuuvFmDyP3HLr28u54swUGdouo6Gx4VP2XfN70sIqnAlAV6e18Hlu2wxZU3br3W1H3RNLgz/I/MLM78RRo2Z7AAAAAAAAAPiPM2cDXVYoVV83xNPMZlJGgyH6m9j81I7CIzJxBgLQw8bd+7asbBg/R2Qek9FQbIC+jKPwGWmdgLOBPSST4+9rbfwuu9bThXLlbWmdgAD0wIZ3SqmHzeNSOQEB6IEM3SOlGibaIqUTEADPIQCeS+Gq4IrqvWujQBR8tDRf/3Pg+xz7YBdcjJthcWJ2/0O5pFOWcX8o+Lk9X/9COhVpXBau/p92uxHTrqWo/lWaAcg/W50jNqdl3BfcHArqEADPIQCeQwA8hwB4Tj8AzDcy/7hdVvvavR4pwNnAHtI8DJR25PAS4DkEwHMIQC/MiVR62OivOQQEoAcm+l5KNUSsvuYwEIBemA9LpYf4mFROcOrmUNd0FhcuTjy88xIT7yJDeRmvCbO5TAE/154/8bWMnIDDwD5Nzta2JmZ5QtqB5MYmrrbPHP9DWgAAAAAAgBHK3GFgoVR7nimZ7n6mJqOB2H90xSTJD+2zJ1Svrs2qzASg+4clOxR8SkSPymgobMwHcTN8SVpvZeajYLvxT2pt/C6b/BfvKFf2SuutTARg0+59d9mN/5S0auxeYEZKb2UiADcK4/dJqYtpSipv4Wyg5xAAz6kfBRRK1RekVNGOwg83zlaLSWIuykgPm8iuX87vqe0g4kdk6qyEzKV/muEFaVXoB0D5StpcjjcnCd2bagBKlcP2TeYhmToLN4eCOgTAcwiA5xAAzyEAnkMAPJeJs4Fpfw4gnZewB/AcAuA5BMBzmQjAtc/Cln3S/2UNZLrrei0zewA2XJdSTcD8iZTeyszNoZ3Wwrnx4sxme+CyQ0Zrxsx/2eS/vBQ1zsvIW5m5KPR/k3MH7g+Wl6elHRgTX4lz1380Z05ekxEAAAAAAAAAAAAAAAAAAAAAAAAAQMYZ8y9JkQ6AehGBDwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAERSURBVDhPYxj6gBFKgwGnX4EJIxNjFIj9/9//Zd83TTgDlsAD4AZw+RcdA1KPgSJhYIH/DKuApCyYjQwY/zV82zBhF5THwAylGZg1zT8zMTB8YWBkdAPxGRn+z//H+H/Xf8b/J1Dwn38X/tw6+RGsCQhQvIANcAUUuP1jYBBg+PPvxI8tkx5BheEAbgBHQAHE6UgA6KIPQHIniP2f4f8cIN6NbhBQDQQwMTBaoGPG/4y6UGkGIJsPLMbEJAYVAgOCXuD0L2pkZPzPC3TDDuTAgwEMA7gCClMZ/jMmgtj////LIxSV6NEI1Pv/EyMDozuYzfD/GtDp8BAHA7RoxHABt39h8X8GxmAQmxgXDHnAwAAAmmRrKKWcPaQAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag Enum, Name = HandleStyles, Type = Integer, Flags = &h0
		Round
		  Square
		Triangle
	#tag EndEnum

	#tag Enum, Name = TooltipPositions, Type = Integer, Flags = &h0
		TopLeft
		BottomRight
	#tag EndEnum

	#tag Enum, Name = TooltipVisibilities, Type = Integer, Flags = &h0
		Show
		  Hide
		Always
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HandleStyle"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="HandleStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Round"
				"1 - Square"
				"2 - Triangle"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="40"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LogarithmicScale"
			Visible=true
			Group="Values"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxValue"
			Visible=true
			Group="Values"
			InitialValue="100"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinValue"
			Visible=true
			Group="Values"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Precision"
			Visible=true
			Group="Values"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Range"
			Visible=true
			Group="Values"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RealtimeChange"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Reversed"
			Visible=true
			Group="Appearance"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StepValue"
			Visible=true
			Group="Values"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TicksSnapRadius"
			Visible=true
			Group="Ticks"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipPosition"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="TooltipPositions"
			EditorType="Enum"
			#tag EnumValues
				"0 - TopLeft"
				"1 - BottomRight"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipVisibility"
			Visible=true
			Group="Appearance"
			InitialValue="0"
			Type="TooltipVisibilities"
			EditorType="Enum"
			#tag EnumValues
				"0 - Show"
				"1 - Hide"
				"2 - Always"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=true
			Group="Values"
			InitialValue="2"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ValueHigh"
			Visible=true
			Group="Values"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="200"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
