#tag Class
Protected Class GraffitiWebFavIcon
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  FlashTimer = new Timer
		  FlashTimer.Mode = 0
		  AddHandler FlashTimer.Action, AddressOf TimerAction
		  
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  // 
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AttachTo(Target as WebImageView)
		  if Session.Browser = WebSession.BrowserType.Safari and NthField( Session.BrowserVersion, ".", 1 ).Val <= 11 then Return
		  
		  if not IsNull( Target ) then
		    dim strExec() as String
		    strExec.Append( "var newTarget = window.GSjQuery('#" + Target.ControlID + " > img')[0];" )
		    strExec.Append( "window.gsfavicon_" + Target.ControlID + " = new Favico({" )
		    strExec.Append( "animation:'popFade'," )
		    strExec.Append( "position:'down'," )
		    strExec.Append( "element: newTarget" )
		    strExec.Append( "});" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EndFlash()
		  FlashTimer.Mode = 0
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Flash(HowOften as Integer, IconCount as Integer, IconShape as string =  "circle", BGColor as Color = &cdd0000)
		  if not isShown and not LibrariesLoaded then Return
		  
		  if HowOften = 0 or IconCount = 0 then Return
		  
		  FlashCount = IconCount
		  FlashBG = BGColor
		  FlashShape = IconShape
		  
		  FlashTimer.Period = HowOften
		  FlashTimer.Mode = 2
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebFavIcon -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "favico.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebFavIcon -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetIconCount(NewCount as Integer, Animation as String = "none", IconShape as String = "circle", IconBG as Color = &cdd0000, Target as WebImageView = Nil)
		  if Session.Browser = WebSession.BrowserType.Safari and NthField( Session.BrowserVersion, ".", 1 ).Val <= 11 then Return
		  
		  if not isShown or not LibrariesLoaded then Return
		  
		  dim strBG as String = "#" + Mid( Str( IconBG ), 5, 6 )
		  
		  dim strExec() as String
		  if IsNull( Target ) then
		    strExec.Append( "window.gsfavicon.badge(" + Str( NewCount, "#" ) + ",{" )
		  else
		    strExec.Append( "window.gsfavicon_" + Target.ControlID + ".badge(" + Str( NewCount, "#" ) + ",{" )
		  end if
		  strExec.Append( "animation:'" + Animation + "'," )
		  strExec.Append( "type:'" + IconShape + "'," )
		  strExec.Append( "bgColor:'" + strBG + "'," )
		  strExec.Append( "});" )
		  
		  Buffer( strExec )
		  
		  LastCount = NewCount
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub TimerAction(sender as Timer)
		  #Pragma Unused sender
		  
		  if LastCount <> FlashCount then
		    me.SetIconCount( FlashCount, "none", FlashShape, FlashBG )
		  Else
		    me.SetIconCount( 0 )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( "if( typeof( window.gsfavicon ) == 'undefined' ) {" + _
		    "window.gsfavicon = new Favico({" + _
		    "animation:'popFade'," + _
		    "position:'down'" + _
		    "});" + _
		    "}" )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		http://lab.ejci.net/favico.js/
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private FlashBG As Color = &cdd0000
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FlashCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FlashShape As String = """circle"""
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FlashTimer As Timer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FlashVisible As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private LastCount As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webfavicon", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAe/SURBVHhe7d1/jBxlGQfw55ndcuxe668WRWmiQawQAqjVlBoUrxpF097tgTE0lD8kqGkTuNulQAhGBI3R0Nst/mGQg8QmGI+e2t0DlGIU20ajYChWo9QaTGMMxHi9NL3bvZbb9+GZuwdjT+9u93ZmZ9b5fpKy877TbGfn/c7zvnN77BIAAAAAAAAAAAAAAAAAAAAAAABAA7Kf2Xn+ys/ecp41IYbYHgOT6Svk9OFLzPIRffpuv0+IJljkkGMqTpdLB/w+iIdAA5DJ5YeZ+GZr/m9CX6lWil+zFkQsZY8ty+QKe3TwP2/NhTH1rLj4Snn1xd+gEsRAIBUg05fXks8PWrMhzvHV02NDB60JEfHssSXMdKdtNozZ3W6bEKGWK0DXlvxFqRQfs2ZTqiumV9HodyatCRFouQKk0nSRbTata7rrfNuEiLRcAfzbPp0C9lmzKTNE7z1TLv7FmoHq7r/trc7VNwh772GhNdq1Ro9z9lFE3qwv/ITOQ+N6i3pC70xOMMm4Xg4ve+Kemyw/8OfZJ4mp7t58j2O+iVgu1SH8Q9qlvnFq7P6jtrspLQege8utl0sq/XtrNqVrxYo3TYx+66Q1W5LpHfygFrQN5MkGHfANOrjrbFfTRGhcH57TP8+yx89W9w39xO/290Wpu3fwE4692zXIn7QuIzOcSq2f+tGuI9bRsEDuArJ9+b/rCV9rzYboSX6+Vimut+ayZLbkrySPr9UXcZ2+kgutO3B6rCf1pI+w8MhUZeiX1t025+YGNnrk3aHD5f+QbSGHquXiR227YYEEQG8Dv663gXdbsyHiZKA2Vvq2NRuW7S28X1iu00O/VgflEutuGyE5qv8ZSdV5ZPKJ4ovWHYruvvxljuhOPbc3WNeC/KqlF5Q/xTUlkAD4srn8r/TpPmzNpYxoWrfadkPOzQ1+Tq+C7br5sbmeGBCqaAUa1iniSesJRPeWwUudxwUd+Jusa2lC9WqlmLZWwwILQFdu57s9qn+XiT9uXQuQx6oTb9hGB+7VNeBS7vEy/ae264vbrgeqC5540qrgz73DtYkTD9OBPdNzvc2bW9zRtqYG/nVRB+B1mVzhF/qkPdY8i66+v1+rlLZZc0FzK3i3Qzd36MnomHcTNQindAE67LF7uNE7iWx//gpy3KsrzK0tTWmdEQB6VOepG635X/SO4m2OUzv1RBR0URnITykjI3RAA/EDz+OXyMkr9Xr9JKXPuYDZraU6r/XfLdVBv0pfazABX2YAYnOSdQ3xZb2dPK63XTs7fvB9TFdr9XpQQ/+0MB/x0unjHrlfa4XYy54UdX9/YIPfgshPdDZ323qtGjqHsv8WcddcL7RLpAHI9N+xVueFvTplXGZd0GbRVgA38wUthaH9AAeWFmkAmOTTtgkRaW8AdMRta5Y2MrYJEYl8EQjRanMAIn9DDeZBBUg4BCDhEICEa3MAGIuAmEEFSDgEIOHaGwBMALGDCpBwCEDCBR+Axcs8JoGYCaMCYJA7CKaAhAshAIsVABSHuEEFSLj2BmDeL4RA9IIPAAa5o2AKSDgEIOGiDQDj7eGooQIkXAgBWOyqxhUfN6gACYcAJBwCkHAhBGCRaR4rgNgJPgCy2DA7e4S4aPMUgLuAuMEaIOEQgIRDABIuhABgnu8kqAD/N6RmG01pbwDw7l94mA/bVlOSUwFEzgjJn0ToKW2USWhEHx/Tvv2697eazKP+R73O/eXOIiLjzLSsr+IL/qNi+wr79WDmfaHBHD3Jj9TKxX9/r6D/AZGhfEagyJT+W8/oqztMjg478V44/fjQ32zvorL9gx8Q8jaSk416bFfplfVO2xVPQnvrKXfX6R/vfsl6mhJ8AHL5p5j4U9Y8S6gBEDkmzGMe0/6pfcWfWW/LsrnCZj3wG/RMXW9dseBXMmZXqpZ3P21dy9LRAdCT8LIuK37oREanK7sPWXcoVm4urHFp2SHEA3rMb7HuttNy/6RHXJqqFH9uXS3pyDWAX96Z5MZapfiOarl0a9iD75t8ovgv/bfuq71x1QVE7i4diFdsV3uIjNWl3lOrlDYHNfi+jgqAXvHD4tyHtIpsmiqXHrXu9tpz77SW3W/64dNjKehBHbc9YTimr/m+lEtdXK2U+k5XHgj8+4oiDQCL/NE2F+SXeX34Kp3x3q4n/Yu1sd2/m9sTNRY9lpIOzLscy1Z/TrYdrRGq6d3I9/yFdLVcXKev+Z7lfiVcI6JdAyzynYNaYv+p+3Zp2b3fumJvZW7gEkdeTtcJmzTcm/QOYukLzB9wloPs+JBjd7Ad09l/CiEAhZ/qk15jzbPMD4Bv/jeOafon9OTtqp7zj100OnrGujvPNbd0ZTPpHr0YVru6rNaCsZo8Pw8yrou4cX2d+scbr+0b8r+fMDLBB6BPA8CNB8C3KldYN8N0hb9dO2+yTA899OrsDghdLAIA0enI20AIDgKQcGEEQCs9dIoQAoDx7ySYAhIOAUg4BCDhgg8Afu2ro6ACJBwCkHAIQMIhAAkXRgCwCOwgqAAJF3wABAWgk6ACJBwCkHBYBCZc4AFgXviToJhoxjYhJkJYBNKYbZ1NxGkyovmfOWBBgf9SqC/bN/A+4fSF1pzl6u7I6cdLf7UmAAAAAAAAAAAAAAAAAAAAAAAAAAAEhug1j8eOTX7xdcUAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADLSURBVDhPYxg8gMMnT47bv7AYyiUaMEJpBq6Awj4gt/Dfnz/yID4jM0syI+N/XrAkw/8d3zZM2AVigSxiYmEu+LahvwjEhxvAEVAQxvSfqYCB8f8JkEFQYTj4z/D/GuN/xo9AHZYg/rcNfWC9TCACBfxntICyUAAjA6MWTDMywDSARDBYDfj//wmQ6P/H8C8cSKcBQ3AVVAbIZPgEkoNyEQAUC1z+Rce4AgrcoEIoABR9uOTAAGYAlEs0gHsByPjwn/H/VSh35AAGBgAE6Tg/R2nmRAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
