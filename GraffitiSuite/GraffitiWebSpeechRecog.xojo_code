#tag Class
Protected Class GraffitiWebSpeechRecog
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  select case Name
		  case "functionSupport"
		    if Parameters.Ubound >= 0 then
		      IsSupported = Parameters(0).BooleanValue
		      
		      me.ExecuteJavaScript( Join( JSBuffer, "" ) )
		      IsReady = True
		    end if
		  case "speechResult"
		    if Parameters.Ubound >= 1 then
		      isListening = False
		      SpeechComplete( Parameters(0).StringValue, Parameters(1).DoubleValue )
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  if hasSpeechAPI( Session ) then
		    dim strExec() as String
		    strExec.Append( "window.GraffitiWebSpeechSupport = false;" )
		    strExec.Append( "window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.msSpeechRecognition || null;" )
		    strExec.Append( "if (window.SpeechRecognition === null) {" )
		    strExec.Append( "window.GraffitiWebSpeechSupport = false;" )
		    strExec.Append( "} else {" )
		    strExec.Append( "window.GraffitiWebSpeechSupport = true;" )
		    strExec.Append( "}" )
		    
		    strExec.Append( "if( window.GraffitiWebSpeechSupport ) {" )
		    strExec.Append( "window.speech" + me.ControlID + " = new window.SpeechRecognition();" )
		    strExec.Append( "}" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'functionSupport',[window.GraffitiWebSpeechSupport]);" )
		    
		    Return Join( strExec, "" )
		  else
		    IsSupported = False
		  end if
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  if IsSupported then
		    dim strExec() as String
		    strExec.Append( "if(window.GraffitiWebSpeechSupport) {" )
		    
		    strExec.Append( "window.speech" + me.ControlID + ".onresult = function(event) {" )
		    strExec.Append( "for (var i = event.resultIndex; i < event.results.length; i++) {" )
		    strExec.Append( "if (event.results[i].isFinal) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'speechResult',[event.results[i][0].transcript,event.results[i][0].confidence]);" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    strExec.Append( "};" )
		    
		    strExec.Append( "window.speech" + me.ControlID + ".onerror = function(event) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'speechError', [event.message]);" )
		    strExec.Append( "}" )
		    
		    strExec.Append( "}" )
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  'dim s as WebSession = Session
		  'break
		  if Session.Browser = WebSession.BrowserType.Chrome or Session.Browser = WebSession.BrowserType.ChromeOS then
		    hasLoaded = True
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function hasSpeechAPI(SessionObj as WebSession) As Boolean
		  if not Session.Secure then Return False
		  if SessionObj.Browser <> WebSession.BrowserType.Chrome and _
		  SessionObj.Browser <> WebSession.BrowserType.ChromeOS then Return False
		  select case SessionObj.Platform
		  case WebSession.PlatformType.Windows, _
		    WebSession.PlatformType.Macintosh, _
		    WebSession.PlatformType.Linux, _
		    WebSession.PlatformType.AndroidPhone, _
		    WebSession.PlatformType.AndroidTablet
		    Return True
		  end select
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Start(userLanguage as String = "")
		  if IsSupported and not isListening then
		    dim strExec() as String
		    strExec.Append( "if(window.GraffitiWebSpeechSupport) {" )
		    if userLanguage  <> "" then strExec.Append( "window.speech" + me.ControlID + ".lang = '" + EscapeString( userLanguage ) + "';" )
		    strExec.Append( "window.speech" + me.ControlID + ".start();" )
		    strExec.Append( "}" )
		    
		    Buffer( Join( strExec, "" ) )
		    
		    IsListening = True
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Stop()
		  if IsSupported then
		    dim strExec() as String
		    strExec.Append( "if(window.GraffitiWebSpeechSupport) {" )
		    strExec.Append( "window.speech" + me.ControlID + ".stop();" )
		    strExec.Append( "}" )
		    
		    Buffer( Join( strExec, "" ) )
		    
		    isListening = False
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Error(Message as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SpeechComplete(Content as String, Confidence as Double)
	#tag EndHook


	#tag Note, Name = Source Material
		Implementation completed by Anthony G. Cyphers, taken from documentation available at:
		https://developer.mozilla.org/en-US/docs/Web/API/Detecting_device_orientation
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mContinuous
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'if Session.Browser = WebSession.BrowserType.Chrome or Session.Browser = WebSession.BrowserType.ChromeOS then
			  if IsSupported then
			    mContinuous = value
			    dim strExec() as String
			    strExec.Append( "if(window.GraffitiWebSpeechSupport) {" )
			    strExec.Append( "window.speech" + me.ControlID + ".continuous = " + Lowercase( Str( Value ) ) + ";" )
			    strExec.Append( "}" )
			  end if
			  'end if
			End Set
		#tag EndSetter
		Continuous As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isListening As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private IsReady As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		IsSupported As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private JSBuffer() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mContinuous As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRealTimeCapture As Boolean = False
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webspeech", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAqwSURBVHhe7Z0NbBTXEcff7N3Zd2cMRCngplU/0gpIo+arCgEKBbWBJhh8ZwStAIWqUkqkKmq4sx2gtAWqKIUQ+0hSWjWiiiqHqolTznZtGqSghIjUkDat1DaRidI2bT4IIUlbjO/s+9jprD2OoLe3tsG+9/b2/SS0M3O2ebvvv/Nmd9+7FRqNRqPRaDQajUaj0Wg0Go1Go9FoyhrgrWcIrm74JOREjTDyNSB8swTm02AY79ChOO3zmW+feyrxAf+oJ/CEAML1sduECbUCsJZ2+dMctgVRPEsH5bCBcPh8Z/MrHC5byloA4WjsWwJFowCYzaHxgaKNfndPqr35JY6UHWUpgGAk/nXase8DiGs5dFlQVngsnT9/t+h6NMWhsqHsBBCOxOmMF3vZnUDwJZ/p39DXufcUB8qCshJAOBp/iDbfGfYmHsoEZw001vd3PvgMh1xP2QiAOj9Bm83D3mSCOQSxOJ1MnOCAqzF462pC0djdtClB51uAH0w4GKqNfYwDrsb1ArAu8UDAI+yWBhBXkwwOsudq3J8BTPFDtkoLiCWhSKxEWWfycLUAqAPuouv0m9ktPSC2iE2bAuy5EvcKYMkOP6X+77EnBfr/a8Jnpmxl15W4VgCh6eci1AMfZ1caKMQ6Nl2Jm4eAFbyVCoC4pmrV5gm54ygDFwsAbmdDOmgYyrRlvLhSANV1TXPozPsouyqwiLeuw5UCyEBGpc6nZIQ1bLkOVwoADEOtA45CC6CUgKnYAQfQAiglIEQlm6qgWnvGjCsFoJk4tAA8jhaAx9EC8DhaAB5HC8DjaAF4HC0Aj6MF4HG0ADyOFoDH0QLwOFoAHkcLwONoAXgcLQCPowXgcaQvDw9GY0uoETupKTcIxH+QJg+kO5p/wh/bEo7EtgiA3ewqQaq9xfFYVkbu+ZQPjL20n/NoP7PU/iOpXHWT6Nol9VtHpGaAqrrGWw0Bz4GApXT0pgPAjQC4PxSJ388/Uh4sa6jyCd9x6vw15H2COv8ztP122N/39NDnEpEqADTMH7F5EQBiW6j+XunLviaKcJUZo1xr930Ci0P18bVsS0FuDYB4HVsFgMhcz6b7QSi6n8JEqfspVwAAFWwVgCj8bJYDRfeTlC51ebm+CvA4WgAeRwtAOoBsSEELwONoAXgcLQCPI1cAiANsFWAIqGazAAQ4x6YS0CDu2B4EnMamDZhnQwpSBeB04EwTZ7BZiInvsKUEgKO1B4p+oQWYYpBNKUgVADidyWAUFQAaPqUEIECM0h68io1CwMMCoBTwHluFQPEMgEbuDJtqgFC0PVfWNVWDw3BGFD8GJUB2EfgGbwsAhKICGDy07+80fPyFXfkg/patAjIiU/zsJ0wQr7EpBckCwKICID7PW3sQu9mSDviMom3JGr6b2LQFsl4WAMDLbBUC4upQfUPR7wGmKwFVBPBcf7L5XbYLABRfZbMAFPheujvxFrtSkCoAn+l7gU17TLOWrQIG2luO0zAg/c0dpoCfsmkPiNvYKgThD2xJQ6oAht+/g2+zWwCAWMmmLXQG3cemJPD5gfbmJ9kpIBhtXABCzGLXBpT+NjLZRSB1IhQtoEgCX6iMNlrTp2wZaE8cIxG0slt6UOxhyxZAs/jZTxgojrIpDekCAIAkm7b4hNnEpi1+URmjgvBVdksGmvhgqiNxmN0CptVuvYLSv8MLJfCN/s7Es+xIQ7oAUsnmbkTh9LrWu6pWNRadUtXXvvt9RGMjicDkUAnAJ9KdCUdhZgKZ7ZT+p7Jrg7PwS4V0ATA/460tppF3fClDurP5JBiwkd3JBbEnFXhzA3u2VNfdM4c6v4FdWwDxAJtSUUIAVM07VtI0TKwLrm78Iru29CdbDiKiNe16Mm+tPpWqmLpUtLU5PsDJGcZ2Nu1BPNTfkVDiRpYSAhjoaLFuCDlmASNvtoi1O4pPriTSHYlfI8BiMif+5gpiS6q9Za1o25XhiC3h+thGEHAHu7bQ544LX0qJKkMAFVU+6+1fxc9eEPPC2b5fsleUdLL594Y/u5DMfcORy4Pqk5dNwHVU8DmmdItgNL6Iru1/wa4tlKUO9ne0SK/+R/DxVjq5U7/rC8xZYFJHf4VDdnwuMHf+tGzviSPs25J55cVUtrfnSHD2ol+ZAqcCiEuZe/8WmuZOKvY25npP/JVjRQnVNV1lAHaROX04Yk8eYG2+t+d9dqXjuJ5NBuFI7EUa9B1fBUdnUYzS/djP8DWxUCgPy8DE5fS3b6Gio4aqsBra/aG1B0PzEoae6cPrAPgMfXY0ldz3x6HfHSOhSOwo1SpfZtceNLenOvYptexNOQEEI5sXG2A8z64T22hMvqwFolNWxj9yfjqkxePN/RwaN1X1DTNNE1spyyznkD0ojqU6WpaypwzKDAEj5E6d+Jd/7nwfFUpLOFSMWwNz5s/KnjpxyQ+FMq/2pMSfe7LsjptQXcMtlC26qPPnccgeFHlhmGto6DrNEWVQLgOMEIrGD1Lj1rNbFBoOukODufUfPP1ISecJUsG3zkDRSkdw1JOIrvk30GXfqAWsDJTLACPkZi3vDIQy1qNUx1XCNO7Ozvp9tf5rFv4n19szarF2uVgpPzB3wX0kzr3U+aNfRaH4AV1B7GdPOZTNABZT1mybYWYHk9RKx5tAH4J4XBjGbuv2MkcmjiU7/OHpfdvoP/kuqS7I0dHYSXXKLraVRGkBDLGsoSoUxkOjFlkXQFX9YWHigXTlm92irc3xxs1oVK6KfdbwixWAYgsdLsfpXReCAPF0sjnBrrKoLwAmHI0lqblRdscKFXjYTYroAjPf1f+bh8c0mTQYiS8EgSvo6KygYvRGDo8dxDsp7f+cPaVxjQAsQtHY49Qhjg9inLCu92mH3yXjLHlnEYbsEB2GGVSpzaS/PYP8mXRULnnNPv3Nr6WTLW3sKo+rBGBB1XfcQHyAxmGlClgUeBQA7x3vDSTZuE4AFta47DNgD7V+NYfkgTiAVB+kOxMPc8RVuFIAI4SjDd9ANB+gS8GZHCotKJ7Mi/yWwY6HXueI63C1ACysqVeDgcw3aUfuoH83cHhSsZ7oUb3QmupocXwo5QZcL4ALCdY1fAkMk4Qw9Dx+Ql/nSp1uje2tfsi09rXvV+Zp3uVSVgL4kJWbwsFAVR2YcBNV9zcDwmLa0/EWjdZ09ZPU8ydNgBesdQjD4fKiPAVgQ0U0PtsvzAV0nfZjuoKYwuGLoLO8h+qJ+32ioseabMrhssYzAhghHI2dp92uYvciSAC70x2Jbex6AmWmhJUOKC56Ov3Z8gzeEwA6ZD303vHwngCcHuFStciWZ/BgBvBeJzvhwQzgvXHeCS9mAO/tswOuOhtCkXiUzt9G6sXrqOkVtPUNFW4AcjrVmuw59D1/YC0VO41CHA1WBJr+3bbnv8M/oD6uEYD1Zg1AUfTLGFQBBf4p3Z5w/F4glXBPOjTFDraUxppBFI7EKUu5A3cIYO2OCkr917KnPDQUuOZ1N+4QgLUiF/Gf7LkA/BsbyuOeIQDgMbYUB3M+MJ9gR3mUXRjy/2R7e44F5sy/goQwn0Mq8hoK352p9oRrHh277qZIdXTrlSYMXC9MQ622G3CmP9k86SuTNBqNRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajUaj0WgKEeJ/pWECL4gS0igAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEMSURBVDhPY6AUMENprIAjoCCMTcNC+veNE3ehQhgApwGcAYVXmRiYshgYGGNZ1C2Z/tw8fgAqhQKYoDQKANnMyMCoBeUyMDIy1EGZGACrAaQA6hvA5V90DMpEAVwBBW7AcNkB5cIBFhf8l4UyUMA/BgYBxv+MfFAuHGAY8J+R4RPjP4Z7/xn+X4MKMQDZc5j/M8r+Z/x/FSoEBxjRyKZh+RsY7J0M//7HA02/xcDIsIaR4T/QW4zt///+jf9z6+RHqFIwwDAAmGjOgQwBRuN0oCZpoIscGP8zWTIw/i/6vmniOagy4gBXQGEfKE1AuVgBI5RGASCNwMCwgHIhgPH//G8b+mdDecMHMDAAAGXpRqlufz7HAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Continuous"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsSupported"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
