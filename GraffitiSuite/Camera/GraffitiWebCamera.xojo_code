#tag Class
Protected Class GraffitiWebCamera
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "isSupported"
		      ReDim Devices(-1)
		      if Parameters.Ubound >= 0 then Supported = Parameters(0).BooleanValue
		    case "playing"
		      isPlaying = True
		    case "error"
		      isPlaying = False
		      if Parameters.Ubound >= 0 then StreamError( Parameters(0).StringValue )
		    case "frameRequest"
		      if Parameters.Ubound >= 0 then FrameReceived( Parameters(0) )
		    case "deviceStart"
		      ReDim devices(-1)
		    case "deviceFound"
		      if Parameters.Ubound >= 1 then
		        if not DeviceExists( Parameters(0).StringValue ) then Devices.Append( new GraffitiWebCameraDevice( Parameters(0).StringValue, Parameters(1).StringValue ) )
		      end if
		    case "deviceEnd"
		      DevicesLoaded()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '// 
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  source.Append( "<video style='display:none;' id='" + me.ControlID + "' width='" + Str( Width ) + "' height='" + Str( me.Height ) + "' src='' autoplay controls playsinline></video>" )
		  source.Append( "<canvas id='" + me.ControlID + "_canvas' style='display:none;'></canvas>" )
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  hasLoaded = True
		  LibrariesLoaded = True
		  
		  UpdateOptions()
		  
		  if IsNull( me.Parent ) then
		    dim strExec() as String
		    strExec.Append( "var theBody = window.GSjQuery('body');" )
		    strExec.Append( "var theVideo = window.GSjQuery('<video id=\'" + me.ControlID + "\' width=\'100\' height=\'100\' autoplay style=\'display:none;\'></video>').appendTo(theBody);" )
		    strExec.Append( "var theCanvas = window.GSjQuery('<canvas id=\'" + me.ControlID + "_canvas\' style=\'display:none;\'></canvas>').appendTo(theBody);" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DeviceExists(deviceId as String) As Boolean
		  for each d as GraffitiWebCameraDevice in Devices
		    if d.ID = deviceId then Return True
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCamera -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    if Session.Browser = WebSession.BrowserType.InternetExplorer then
		      if NthField( Session.BrowserVersion, ".", 1 ) = "11" then strOut.Append( AddPromisePolyfill( CurrentSession ) )
		    end if
		    
		    strOut.Append( AddjQuery( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCamera -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Pause()
		  if Supported and isPlaying then
		    
		    dim strExec() as string
		    strExec.Append( "var videoElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "videoElement." + If( isPaused, "play", "pause" ) + "();" )
		    Buffer( strExec )
		    
		    isPaused = not isPaused
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Play(fromDevice as GraffitiWebCameraDevice)
		  if isPlaying or isPaused then Return
		  
		  isPaused = False
		  
		  if Supported then
		    dim strExec() as String
		    
		    if not IsNull( fromDevice ) then
		      strExec.Append( "var constraints = {video:{deviceId:{exact: '" + fromDevice.ID + "'}},audio:false};" )
		    else
		      strExec.Append( "var constraints = {video:true,audio:false};" )
		    end if
		    
		    strExec.Append( "navigator.mediaDevices.getUserMedia(constraints).then(" )
		    strExec.Append( "function(stream) {" )
		    strExec.Append( "window.stream" + me.ControlID + " = stream;" )
		    strExec.Append( "var videoElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "videoElement.srcObject = stream;" )
		    strExec.Append( "videoElement.play();" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'playing');" )
		    strExec.Append( "}).catch(function(error){" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'error', [error]);" )
		    strExec.Append( "});" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RequestFrame()
		  if Supported then
		    if isPlaying then
		      dim strExec() as String
		      strExec.Append( "var myCanvas = window.GSjQuery('#" + me.ControlID + "_canvas')[0];" )
		      strExec.Append( "var myVideo = window.GSjQuery('#" + me.ControlID + "')[0];" )
		      strExec.Append( "var scaleFactor = window.devicePixelRatio;" )
		      strExec.Append( "myCanvas.width = myVideo.videoWidth / scaleFactor;" )
		      strExec.Append( "myCanvas.height = myVideo.videoHeight / scaleFactor;" )
		      strExec.Append( "myCanvas.getContext('2d').drawImage(myVideo, 0, 0, myCanvas.width, myCanvas.height);" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'frameRequest', [myCanvas.toDataURL()]);" )
		      
		      Buffer( strExec )
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Snapshot(fromDevice as GraffitiWebCameraDevice, Quality as Double = 0.5)
		  if Supported then
		    dim strExec() as String
		    
		    if not IsNull( fromDevice ) then
		      strExec.Append( "var constraints = {video:{deviceId:{exact: '" + fromDevice.ID + "'}},audio:false};" )
		    else
		      strExec.Append( "var constraints = {video:true,audio:false};" )
		    end if
		    
		    strExec.Append( "navigator.mediaDevices.getUserMedia(constraints).then(" )
		    strExec.Append( "function(stream) {" )
		    strExec.Append( "window.stream" + me.ControlID + " = stream;" )
		    strExec.Append( "var videoElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "videoElement.srcObject = stream;" )
		    strExec.Append( "videoElement.play();" )
		    
		    strExec.Append( "videoElement.addEventListener('loadeddata',function(){" )
		    strExec.Append( "var myCanvas = window.GSjQuery('#" + me.ControlID + "_canvas')[0];" )
		    strExec.Append( "var myVideo = window.GSjQuery('#" + me.ControlID + "')[0];" )
		    strExec.Append( "var scaleFactor = window.devicePixelRatio;" )
		    strExec.Append( "myCanvas.width = myVideo.videoWidth / scaleFactor;" )
		    strExec.Append( "myCanvas.height = myVideo.videoHeight / scaleFactor;" )
		    strExec.Append( "myCanvas.getContext('2d').drawImage(myVideo, 0, 0, myCanvas.width, myCanvas.height);" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'frameRequest', [myCanvas.toDataURL('image/jpeg'," + Str( Quality, "#.#" ) + ")]);" )
		    strExec.Append( "})" )
		    
		    strExec.Append( "}).catch(function(error){" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'error', [error]);" )
		    strExec.Append( "});" )
		    
		    'strExec.Append( "navigator.getUserMedia({video:true,audio:false}," )
		    'strExec.Append( "function(stream) {" )
		    'strExec.Append( "if(!stream.stop && stream.getTracks) {" )
		    'strExec.Append( "stream.stop = function(){" )
		    'strExec.Append( "this.getTracks().forEach(function (track) {" )
		    'strExec.Append( "track.stop();" )
		    'strExec.Append( "});" )
		    'strExec.Append( "};" )
		    '
		    'strExec.Append( "window.stream" + me.ControlID + " = stream;" )
		    'strExec.Append( "var videoElement = document.getElementById('" + me.ControlID + "');" )
		    'strExec.Append( "var createSrc = window.URL ? window.URL.createObjectURL : function(stream) {return stream;};" )
		    'strExec.Append( "videoElement.src = createSrc(window.stream" + me.ControlID + ");" )
		    'strExec.Append( "videoElement.addEventListener('loadeddata',function(){" )
		    'strExec.Append( "var myCanvas = window.GSjQuery('#" + me.ControlID + "_canvas')[0];" )
		    'strExec.Append( "var myVideo = window.GSjQuery('#" + me.ControlID + "')[0];" )
		    'strExec.Append( "var scaleFactor = window.devicePixelRatio;" )
		    'strExec.Append( "myCanvas.width = myVideo.videoWidth / scaleFactor;" )
		    'strExec.Append( "myCanvas.height = myVideo.videoHeight / scaleFactor;" )
		    'strExec.Append( "myCanvas.getContext('2d').drawImage(myVideo, 0, 0, myCanvas.width, myCanvas.height);" )
		    'strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'frameRequest', [myCanvas.toDataURL('image/jpeg'," + Str( Quality, "#.#" ) + ")]);" )
		    'strExec.Append( "window.stream" + me.ControlID + ".stop();" )
		    'strExec.Append( "}, false);" )
		    'strExec.Append( "videoElement.play();" )
		    'strExec.Append( "}" )
		    'strExec.Append( "}," )
		    'strExec.Append( "function(error) {" )
		    'strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'streamError', [error.code]);" )
		    'strExec.Append( "});" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Stop()
		  if Supported and isPlaying then
		    Pause()
		    
		    dim strExec() as String
		    strExec.Append( "try {" )
		    strExec.Append( "const video = document.querySelector('#" + me.ControlID + "');" )
		    strExec.Append( "window.stream" + me.ControlID + ".stop();" )
		    strExec.Append( "} catch (e) {" )
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		    
		    isPlaying = False
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  ReDim Devices(-1)
		  
		  dim strExec() as String
		  
		  strExec.Append( "function hasGetUserMedia() {" )
		  strExec.Append( "return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "function getDevices(deviceInfos) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'deviceStart');" )
		  strExec.Append( "for (var i = 0; i !== deviceInfos.length; ++i) {" )
		  strExec.Append( "var deviceInfo = deviceInfos[i];" )
		  strExec.Append( "if (deviceInfo.kind === 'videoinput') {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'deviceFound', [deviceInfo.deviceId, deviceInfo.label || 'camera ' + i]);" )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'deviceEnd');" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "function handleError(error) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'error', [error]);" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "if (hasGetUserMedia()) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'isSupported', [true]);" )
		  strExec.Append( "navigator.mediaDevices.enumerateDevices().then(getDevices).catch(handleError);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'isSupported', [false]);" )
		  strExec.Append( "}" )
		  
		  'strExec.Append( "navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || null;" )
		  'strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'isSupported', [navigator.getUserMedia !== null]);" )
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  
		  RunBuffer()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event DevicesLoaded()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event FrameReceived(frameData as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event StreamError(errorMessage as String)
	#tag EndHook


	#tag Note, Name = Resources
		http://dev.w3.org/2011/webrtc/editor/getusermedia.html
		http://www.chromestatus.com/feature/6067380039974912
		https://webrtc.github.io/samples/
		https://www.html5rocks.com/en/tutorials/webrtc/basics/#toc-mediastream
		https://www.html5rocks.com/en/tutorials/getusermedia/intro/
		https://developer.microsoft.com/en-us/microsoft-edge/platform/status/mediacaptureandstreams
		https://blogs.windows.com/msedgedev/2015/05/13/announcing-media-capture-functionality-in-microsoft-edge/
		https://dev.opera.com/blog/webcam-orientation-preview/
	#tag EndNote


	#tag Property, Flags = &h0
		Devices() As GraffitiWebCameraDevice
	#tag EndProperty

	#tag Property, Flags = &h0
		isPaused As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		isPlaying As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Supported As Boolean
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webcamera", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAn+SURBVHhe7d17bBxHGQDwmV3H9p1ruS1SaJAQQlUT0qR/UFEUEwFpEaQ0ie9cKUhVWhJeUiUe8l1i0oSWqk1paFLfOaEUIRA09CEoJT47SUlAIpQiuSgIpBJQE2gB8chDJCVx7Dsnd/vxzd4XE3cfvlvv2Tve7yc5O99cbnd88+3srO92TzDGGGOMMcYYY4wxxhhjjDHGGGOMMcYYmyskLUPxtq7e9pGm8esoZA3QXm45eWZo5wiF0zbtBGi+s/cGs1LZIiWswNW9m6pZQ8HfAOThilnefnHv7j9TZSDTSoBkKrsB1/B9CtlsAPGpscHcUxTVzaRl3dq6s+tw8YNqxGaNFOnmxZ1/ufTa8B+opi6BRoC27o3zAeDvWGyt1tQGn3McN/lLCkMEK6SUCymIoxL+/u8aHeg7TXHNAiVAIpV9VEqxhcKaYQLsKQ7mN1AYmkQq8xS+AOspjCUAsb04mNtKYc0MWtYFs+ZDVGQREbRPAiUASFhERRYRQfsk0CEgmc4CFesSjUMAbJWmeYCCyAGrUu0Ty8LlPFVRjU1cgrwDu2ybHbsYK+Tq7s94JQCIC3jK1E6RdtrSmbtByKcpdAiSAIEOARrz3HviaqZHgAO4xYcpDA+Ir+IIsIoiV7jtczj6XE2hlhoxAsxoAswmCXLT6GBfH4Va4kNAQHiOfFb3zreBsKgUmpjMAWBOHPsrEspUDE0DDgHWD3G1v6MgHHiSqxZ4HgQVSWW1rFRUodoWe4k7iFpal+tw30fFQv4JO9ZcoivTLQ25l0KHSMwBsJPuGS3kn6GQhSiR7klJYRQodOA5AKsbJ0DMcQLEHCdAzHEC6MQy51EpNA04DRRP4snYr6nMQmQALMfT3M9T6MB/Co45Pg1kdeMEiLm4HQL+LQT8A5cnBNg/J+1aKa7DnwVYwh/5Tly+w67XDM8B3L0CAIdwrDtYHMi/QnW+Et2ZZcISt0spV2K4rFobfZwAV8BOf7YJmraNDO08RlWBtHf1Liob5QekkOpCmEjjBEDYsMOGBdtGh/KHqSoUbd09t1lg3I8v2K1UFTmcAAD3jQ3mH6OoIZKpzGY8F/86hZES39NAgBJYcm2jO19R21DbUtukKq1pnwA4FP1RGLC8ONT3AlU1nL2tJvkBe9ua0/oQAHgqZ0njg+OFx1+nqtp8eH1ra3vHfGkY81UIlnW6NHLutHhpT117dUt60/UGWC/L6inkrIvdHEBacFutk71kV/a92NVr8DdejcfwW6h6MoAjuNZ9OKzsHxvK/Z5qfbV1ZW4FQ/6CwlkVswSwNowV+vdQ4CnR1fM+KY1e/E0/QVW1AfE8gLWzONT/W6rxlEz3rMejaeCbNIQlTpPA3TV1fjrzMA7zR+rufAWfo56r1kE1nqgtu6uRXjRMALDgEuygwJN9vaCQD1AYmFqHWheFnqptgtA/t99o2iUATvx2FA/k/0WhKzxXf672q4WnptaFSfAcha5Um1TbKNSGXgkAMNpcbvF9kRPd2Yewx+6i0JdhiMUWiOW44l9RlSdMgrsSKVy3j+ZyaQfOHS5QqAWtJoEg4LvFQv5zFDpgu1bjYl81mgwATkpDbB4byDtubNXWnf0o7r0/o3Aqa3CytZ/KDjhn+A4eNj5L4YyKwSTQ+ikVvHyFlpNgth4Uxrxb3DpfqVhwMxVr4bqN/5uyjZGiUQIAFDuufpECBzzufwYXzrduQbwxrwnWFQd2/JNqJjTf+aUb2rqy63B49x3a32IZbctVtY3VS9J0oE0CgJAvij0Pef6lDh/3ONWDL55/IX+WAhseKnKJdPbNJqvpOBjiGRw3W+ihmoAUa6nohG2026oJfUYAAO8bIa5+MCml+BhFVxoYG8xPdIa6vyEeo9Xf7zPY6YFvFoHH+JVqmxQ6+bU1YjRKAHGCSg5J8/wKKk6CSfETKtrAgiew826kcFq8tmnzaWvU6JMApvR5UaX9ps5bmc2XJj4Chnv/Utx1vYfuurlv0+bb1mjRZw7gu1fB26lwpfHzP/rGxLuEloDFVAyJ6zZt/m2NFm0SwAQ4R0UHnHI7bv2GnTB5wmiJpVQKhds2L/Nra9ToMwJI8H7P3RCOmyTj8b8DJ3z2jSraUpmbpIR77AfC4rLNy3zbGjH6zAGk9PwmEtwbXTsDJ3zr8JSvBFK+ilGoX2bhtU2bT1ujRp8EsHw+dQOmunW9l7rO8Wvmt02/tkbMnBgBSoXHh0FA9SqfGYB7/ym1TQqdeAQIH07q/G+HDvIQlRoPxEEquZqyrRGiTQLgpO7mljUbPY/jII1vU7HhQFY8t6XaqNpKYeTpcwhAhiE+TkUH+zAAVsO/wAoAvlcq7PIc/v3aGEVaJQDOru6ggivLFI9gB52hMHRq3ZYJX6PQg38bo0arBJBSrmpNb+qk0GF8b/8bUohPUhg6tW61DQodVNtUGynUgmYjgGpw5ctUdKXe/QNh9VAYGpz591z5zqKbqdoWRdolAO6H6bZU9iMUuCoW+nfhTC28kQDXVSzkdlHkqtommaZQGxomgPo7i5hyTxsb7HvaErACD9xHqKp++Fy1DrUuqvFUS5uiSMsEUB/+SKYy36LQU6mQfwmH7ferG1iDgFepekrq/6rnqOeqdVC1J9UW1SYKtVL3p0iVaFwapsDWsUJ+OwVTuqpr442WhNVCQif2sno7t/qevhSn8N/TONQPGyD3Xxjq+5NdX4NkOrMFV/AohbMqdheHKrjn3T06kHuWwhmlvj8ZQETm1vhxujZwguqAZHd2E4UzBvf83ih1flDaJ4ANxM5a5gRhSaayuC2p3WVgbuZGAihS3ptIZQ+1relZQjWhU+tW28AD571Upb25kwBIzcTBNI7iaJDrWHXfNVQ9bWpduM4+tW5dZ/tetJ8EegGAc1LIR8pSDF0s5I5TdV2a09mFTSC68LTwfillB1VHVizPAmqBk7Wj+JseMizrYBnM18evveqE4yqj9Q+2tpy9sKBJVq63DHE7nhKuxE4P9YOkjcYJUAdMCnW52An7FQCxAIf2a+0HNBbL08CgVIfjzxJ8xZbMhc4PKrYJwKpCPwTgA9pcGKkj7LCbqOgQiTkAf3No4/C3h7PQcQLEHCdAzHECxBwnQMxxAsRcoNPARCrzXx3eHIkT9eZXcTBf942vgo0AUv6VSiwqAvZJoATAYWPKe+uymSUFvEzFugRKANOqPElFFhGmZX2TinUxaVmXi8d+c6ZpYed/pBRaXQg5V4EUXxgd7A90j+JACaCUjw0fMd/TOYKHA/X1qmyWWEJsLBVygb+tJHACKOXXhofNRZ0/x5FAXWCxqFrLZsg+C8SnS4O55ykOJNBpoJtr1m7uKJbGl0rT9LyBIps+qFROJVpbjr7548e0uRchY4wxxhhjjDHGGGOMMcYYY4wxxhhjjDHGGk2I/wGFyo2+gVT5ZQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEdSURBVDhP1ZFPTsJQEMa/adxAYr2AHAC4gByA6M6eQeMB4AAqFwDiEhLdu+rChYkHkAsge+MFgMRuTMdv2gHbWHFp/CXT+eZP5r3XwZ8j7lGPehd0zTz6lcV7PJqaCLIwQyYKHKfQmZlpWsuLZRSXrrDnnjl9oQ08tHiAj3SWPNy8empL/bR/5PLrCbWovxTF3EOo6CpQPJlOBV1RCbMCUUE7iYcHprdP4KSQn46ZNfAGtykkNDNtuU0963UK/6CI3gukK6L7Aj0zbTkvlqgcwBPWdA2FnEPk0LTnvlE5wDbAk4dUbwyeTVvOyyV+uIG0oME1Tx1D9C7XzFXAnpxa1JsX17gLDrtK4lE71w4HPBZXtQtbMQecePi/AT4BSj9fnvSlfdwAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="isPlaying"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Supported"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="isPaused"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
