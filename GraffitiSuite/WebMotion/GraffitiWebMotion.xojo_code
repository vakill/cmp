#tag Class
Protected Class GraffitiWebMotion
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  select case Name
		  case "functionSupport"
		    if Parameters.Ubound >= 1 then
		      MotionSupported = Parameters(0).BooleanValue
		      OrientationSupported = Parameters(1).BooleanValue
		      SupportChecked()
		    end if
		  case "orientationChange"
		    if Parameters.Ubound >= 3 then
		      OrientationChanged( Parameters(0).BooleanValue, new GraffitiWebMotionPoint( Parameters(2).DoubleValue, Parameters(3).DoubleValue, Parameters(1).DoubleValue ) )
		    end if
		  case "motionChange"
		    if Parameters.Ubound >= 9 then
		      Motion( new GraffitiWebMotionPoint( Parameters(0), Parameters(1), Parameters(2) ), _
		      new GraffitiWebMotionPoint( Parameters(3), Parameters(4), Parameters(5) ), _
		      new GraffitiWebMotionPoint( Parameters(7), Parameters(8), Parameters(6) ), _
		      Parameters(9) )
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  dim strExec() as String
		  strExec.Append( "function graffitiOrientation_" + me.ControlID + "(event) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'orientationChange',[event.absolute,event.alpha,event.beta,event.gamma]);" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "function graffitiMotion_" + me.ControlID + "(event) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'motionChange',[" )
		  strExec.Append( "event.acceleration.x,event.acceleration.y,event.acceleration.z," )
		  strExec.Append( "event.accelerationIncludingGravity.x,event.accelerationIncludingGravity.y,event.accelerationIncludingGravity.z," )
		  strExec.Append( "event.rotationRate.alpha,event.rotationRate.beta,event.rotationRate.gamma," )
		  strExec.Append( "event.interval]);" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "var " + me.ControlID + "orientationSupport = false;" )
		  strExec.Append( "var " + me.ControlID + "motionSupport = false;" )
		  strExec.Append( "if (window.DeviceOrientationEvent) {" )
		  strExec.Append( me.ControlID + "orientationSupport = true;" )
		  strExec.Append( "window.addEventListener('deviceorientation', graffitiOrientation_" + me.ControlID + ", true);" )
		  strExec.Append( "} else {" )
		  strExec.Append( me.ControlID + "orientationSupport = false;" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "if(window.DeviceMotionEvent) {" )
		  strExec.Append( me.ControlID + "motionSupport = true;" )
		  strExec.Append( "window.addEventListener('devicemotion', graffitiMotion_" + me.ControlID + ", true);" )
		  strExec.Append( "} else {" )
		  strExec.Append( me.ControlID + "motionSupport = false;" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','functionSupport',[" + me.ControlID + "motionSupport," + me.ControlID + "orientationSupport]);" )
		  Return Join( strExec, "" )
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  
		End Sub
	#tag EndEvent


	#tag Hook, Flags = &h0
		Event Motion(Acceleration as GraffitiWebMotionPoint, AccelerationIncGravity as GraffitiWebMotionPoint, RotationRate as GraffitiWebMotionPoint, Interval As Double)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event OrientationChanged(Absolute As Boolean, Orientation as GraffitiWebMotionPoint)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SupportChecked()
	#tag EndHook


	#tag Note, Name = Source Material
		Implementation completed by Anthony G. Cyphers, taken from documentation available at:
		https://developer.mozilla.org/en-US/docs/Web/API/Detecting_device_orientation
		
	#tag EndNote


	#tag Property, Flags = &h0
		MotionSupported As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		OrientationSupported As Boolean = False
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.weborientation", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAP3SURBVHhe7d1PiFtVFMfxe94MOhOndKWgiFqQCgoqdCM6LnRVpJ1EVAR3blxYsWaGbgRRCyIITUAqLkTcqAun6oQp7nUqilJx4aa6sbhwUVyIdWKdSY4nyYGq038kMfOu5/uBvHfOTWYScn/vXxZJAgAAAAAAAAAAAAAAAAAAAAAAAADkRnydnWuq9Se0kPuTpjusvX4wOkGqXU3yoxVfp2463l5tfun3ZCW7AFxdPXhLkYq3ReRBHyoF1fRWu9V4yttsZBWAHQsHb9uUYs0m/1ofKpvV9ZXGgtdZKHydhU4x1dvyyzr5Pftna/VXvM5CNnuA2YcXD4imo96W2maxufvPj17/wdtSy2cP0NW9XpVe0ZnK5jCQTQBE0l1ell5Wr9XXpVepLaqXQ7E/PmOHkNe8vThJ07Z8wW6Vfj8MTa31VqPmXanFCYDqy+1W8yVvL8me64St7ht0QyAA4zdqAPo0fe7VxYneaYsd3g0nowBkdRk4MrGt+nK3USc/M7ECgC0IQHAEILj8TwJVT9vy5KCZIEn32OIG7/6Jq4Dxu1AANOm37bliPr175Hcfmqi5av2Rrsgxb8/jKmAyROW97Zr8nrOt5oc22595m6WsA6AqHS+3j8ron09sI04CgyMAwRGA4AhAcAQgOAIQHAEIjgAERwCCIwDBEYDgCEBwBCA4AhAcAQiOAARHAIIjAMERgOAIQHAEIDgCEBwBCI4ABEcAgiMAwRGA4AhAcAQgOAIQHAEIjgAERwCCIwDBEYDgCEBwBCA4AhAcAQgu7wAU+pBX22Jm37M32Wp+0OXp//CDEWt2x1deT4yo2sYjj9k7eKMPncd3BY/fWH4xZFL4rmDkImYAVE93U1rqaOcBVVlIXX3R7wkn3CHA/sk77Wk9kI412z7UV6kt7Umpu2xvyS4fGh6HgHJSTWem07lD/578nvWVIydt4p7xNoxQAZCkH/y28sYv3m6x3mp+YiH5xtsQYp0DiFzJ5E7+52e2UTYBsC3zZy+Hpqq9D24u52ZfD02TnvWy9DLaA+h3XgxNktzt5YU9Wp+157n0Y66APc8pL0svmwCIyKdeDk9StbJQf9K7LWY30lF7nuu8HUGx6kXpZXMZ2FOp1k9ZEnZ7OzQ7nBxuX/XT4bS83P/NoZ37l3ZtTHVftbfj8f4DRqH6pp1MPu1d6WUVgJna4rztsta8HQO1Ez7ZacWtg35Emr6w6/97vctCVlcBf6w0TnQ73Xk7mfveh0Yke2wxlsm313R85tzGXm+zkdUe4O8q1eeet/zuszPu2+2ka86HJ0dsyjX9KpLWVOT99seNZb8HAAAAAAAAAAAAAAAAAAAAAAAAAADgP5LSX5sxBitUFOWzAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAC6SURBVDhPYxj6gBFKowBO/6JGRsb/vFAuw///jJ+/b+yrh3JRADOURgFsmpb7gWZbMvxnBCJGd0ZGBjdmDfPrf26cuApVAgdMUBoF/Gf4f+0fw7/wf4z/Jvxn+JcKFFgFlSIOcPkXHYMywYAroLCPI6AgDMpFAVhdQAoYeAOwR2NA0UfG/wxX/zMw7AYqcAUGq+w/xv/FPzZMwAhMHC74/wRotCUTw/9PIBoYlXwMf/6dgEqOAhTAwAAAVj40jRnRq1sAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MotionSupported"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OrientationSupported"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
