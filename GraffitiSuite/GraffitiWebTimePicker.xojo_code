#tag Class
Protected Class GraffitiWebTimePicker
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "valueChange"
		      if Parameters.Ubound >= 2 then
		        dim intHour as Integer = Parameters(0).IntegerValue
		        dim intMinute as Integer = Parameters(1).IntegerValue
		        dim strMeridiem as String = Parameters(2).StringValue
		        
		        dim d as new Date
		        d.Hour = intHour
		        d.Minute = intMinute
		        if mTwelveHour then
		          if strMeridiem = mMeridiemHigh and d.Hour < 12 then d.Hour = d.Hour + 12
		          if strMeridiem = mMeridiemLow and d.Hour = 12 then d.Hour = d.Hour - 12
		        end if
		        mValue = d
		        ValueChange()
		      end if
		    case "onClick"
		      if Enabled and Visible then
		        ShowPicker()
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  dim strExec() as String
		  
		  select case Name
		  case "style"
		    if not IsNull( Value ) then
		      dim vStyle as WebStyle = WebStyle( Value )
		      
		      if vStyle.Name = "_DefaultStyle" then
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').removeClass();" )
		      else
		        if LastStyle <> "" and LastStyle <> "_DefaultStyle" then
		          strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').removeClass('" + LastStyle + "');" )
		        end if
		        
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').addClass('" + vStyle.Name + "');" )
		      end if
		      
		      LastStyle = vStyle.Name
		      
		    end if
		    
		    Buffer( strExec )
		    
		    Return True
		  case "enabled"
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').prop('disabled', " + Lowercase( Str( not Value.BooleanValue ) ) + ");" )
		    if not IsNull( me.DisabledStyle ) then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').removeClass('" + if( not Value.BooleanValue, me.Style.Name, me.DisabledStyle.Name ) + "');" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').addClass('" + if( not Value.BooleanValue, me.DisabledStyle.Name, me.Style.Name ) + "');" )
		    end if
		    
		    Buffer( strExec )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' style='overflow:visible;margin:0;padding:0;display:none;'>" )
		  source.Append( "<input id='" + me.ControlID + "_inner' type='text' pattern='\b((1[0-2]|0?[1-9]):([0-5][0-9])([AaPp][Mm]))' value='" + DateToTime( Value ) + "' style='width:100%;height:100%;' readonly>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Clear()
		  me.Value = nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DateToTime(d as Date) As String
		  if IsNull( d ) then Return ""
		  
		  dim intHour as Integer = d.Hour
		  dim blnPM as Boolean = False
		  if intHour >= 12 then
		    if intHour > 12 then intHour = intHour - 12
		    blnPM = True
		  end if
		  
		  Return Str( intHour, "0#" ) + ":" + Str( d.Minute, "00" ) + if( blnPM, MeridiemHigh, MeridiemLow )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HidePicker()
		  dim strExec as String = "window.GSjQuery('#" + me.ControlID + "_inner').clockpicker('hide');"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebTimePicker -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddMoment( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "clockpicker" ).Child( "jquery.clockpicker.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "clockpicker" ).Child( "gwtimepick.css" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwmaskedinput.min.js" ), "graffitisuite.webfieldmask" ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebTimePicker -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function PositionToString() As String
		  dim strPlace, strAlign as String
		  
		  select case mPosition
		  case PositionBottom
		    strPlace = "bottom"
		    strAlign = "top"
		  case PositionTop
		    strPlace = "top"
		    strAlign = "bottom"
		  case PositionLeft
		    strPlace = "left"
		    strAlign = "right"
		  case PositionRight
		    strPlace = "right"
		    strAlign = "left"
		  end select
		  
		  Return "placement:'" + strPlace + "'," + _
		  "align:'" + strAlign + "',"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowPicker()
		  dim strExec as String = "window.GSjQuery('#" + me.ControlID + "_inner').clockpicker('show');"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    if IsInit then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').clockpicker('remove');" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').unbind();" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').clockpicker({" )
		    strExec.Append( PositionToString() )
		    if mTwelveHour then strExec.Append( "twelvehour:true," )
		    strExec.Append( "autoclose:" + Lowercase( Str( AutoClose ) ) + "," )
		    strExec.Append( "default:'" + DateToTime( Value ) + "'," )
		    strExec.Append( "donetext:'" + EscapeString( mDoneText ) + "'," )
		    strExec.Append( "meridiemLow: '" + EscapeString( mMeridiemLow ) + "'," )
		    strExec.Append( "meridiemHigh: '" + EscapeString( mMeridiemHigh ) + "'," )
		    
		    strExec.Append( "afterDone: function (hours, minutes, meridiem) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'valueChange', [hours, minutes, meridiem]);" )
		    strExec.Append( "}," )
		    
		    strExec.Append( "afterShow: function() {" )
		    strExec.Append( "var maxZ = Math.max.apply(null,window.GSjQuery.map(window.GSjQuery('*'), function(e,n){" )
		    strExec.Append( "if(window.GSjQuery(e).css('position')=='absolute')" )
		    strExec.Append( "return parseInt(window.GSjQuery(e).css('z-index'))||1 ;" )
		    strExec.Append( "})" )
		    strExec.Append( ");" )
		    strExec.Append( "window.GSjQuery('.clockpicker-popover').css('z-index', maxZ + 1);" )
		    strExec.Append( "if( window.GSjQuery('#" + me.ControlID + "').parents('.palette').length > 0 ) {window.GSjQuery('#" + me.ControlID + "').parents('.palette').css('z-index', maxZ - 1);}" )
		    strExec.Append( "if( window.GSjQuery('#" + me.ControlID + "').parents('.modal').length > 0 ) {window.GSjQuery('#" + me.ControlID + "').parents('.modal').css('z-index', maxZ - 1);}" )
		    strExec.Append( "if( window.GSjQuery('#" + me.ControlID + "').parents('.sheet').length > 0 ) {window.GSjQuery('#" + me.ControlID + "').parents('.sheet').css('z-index', maxZ - 1);}" )
		    
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    
		    'strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').change(function() {" )
		    'strExec.Append( "var myValue = this.value;" )
		    'if mTwelveHour then
		    'strExec.Append( "var theTime = moment(myValue, ['hh:mmA']);" )
		    'strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'valuechange', [theTime.format('HH:mm')]);" )
		    'else
		    'strExec.Append( "var theTime = moment(myValue, ['hh:mm']);" )
		    'strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'valuechange', [theTime.format('HH:mm')]);" )
		    'end if
		    'strExec.Append( "});" )
		    
		    #if XojoVersion < 2015.4 then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('click', function () {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onClick' );" )
		      strExec.Append( "});" )
		    #endif
		    
		    ManualEntry = mManualEntry
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').mask('99:99?aaaaaaaaaaaaaaaaaaaaa');" )
		    
		    RunBuffer()
		    
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		    
		    isInit = True
		    
		    Value = mValue
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/weareoutman/clockpicker
		
		Licensed under MIT.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAutoClose
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAutoClose = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AutoClose As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		DisabledStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDoneText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDoneText = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		DoneText As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isInit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private LastStyle As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  me.ExecuteJavaScript( "if( typeof( window.GSjQuery ) == 'undefined') {if( typeof($) !== 'undefined') {window.GSjQuery = $.noConflict();}}" )
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mManualEntry
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mManualEntry = value
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + "_inner').prop('readonly', " + Lowercase( Str( not value ) ) + ");" )
			End Set
		#tag EndSetter
		ManualEntry As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAutoClose As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDoneText As String = "Done"
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mMeridiemHigh
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMeridiemHigh = value
			  
			  UpdateOptions()
			  
			  'me.value = mValue
			End Set
		#tag EndSetter
		MeridiemHigh As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mMeridiemLow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMeridiemLow = value
			  
			  UpdateOptions()
			  
			  'me.Value = mValue
			End Set
		#tag EndSetter
		MeridiemLow As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mManualEntry As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMeridiemHigh As String = "PM"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMeridiemLow As String = "AM"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPosition As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTwelveHour As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Date
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPosition
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPosition = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Position As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTwelveHour
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTwelveHour = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TwelveHour As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  dim strExec() as String
			  dim strValue as String
			  if IsNull( value ) then
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').val('');" )
			  else
			    strValue = Str( value.Hour, "00" ) + ":" + Str( value.Minute, "00" )
			    
			    if not TwelveHour then
			      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').val('" + strValue + "');" )
			    else
			      dim intHours as Integer = value.Hour
			      dim strPeriod as String = mMeridiemLow
			      if intHours >= 12 then
			        strPeriod = mMeridiemHigh
			        if intHours > 12 then intHours = intHours - 12
			      end if
			      
			      strValue = Str( intHours, "00" ) + ":" + Str( Value.Minute, "00" ) + strPeriod
			      
			      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').val('" + EscapeString( strValue ) + "');" )
			    end if
			  end if
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Value As Date
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.timepicker", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA2wSURBVHhe7Z1/kBtlGcffZze5uyQtre0MMNSBERDqgJV2rFgG5IcDlEKb3NEDOiLiD35UaZukrfYPsRRREZrkCghYVKYqRbhySfpDHGYQWmHaEWkRhp8qAh1Bppb2rndJ7nJ5H5+9e44pYmF3s7vZTd/PzN0+z5O7ZPd9v+/zvu9m912hUCgUCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFIpmAXh7eNH5sD6+unXikBaeoOuhUqnc1yc2ry3xq4cVTSuA1sSyEzSQM4QU0wXgDIFwLABOoJfoB2Kjf/UBqojYBwJ6EbBXoNhFpbMLEXZWwrhLbMiV+e+aiqYRQHTOsqOxpTafDilOlTcdQEzmlxwBhXgBUGyVOnRXejLbOBx4gi2AzpXjooN9najBfDqQORx1H8R/oNC6SWQbSoXMsxwNJIEUwLiO9NRaDVNUAdfQITT0GKjb2KkJkRko5tZzKFAESgCx9qWnSsQk7fQ3OeQbEMVODWV2YGPXAxwKBIEQQOzS5ClyWFtObf1rHPItI0LQRHYgnw2EEHwvgEgivYR2MkdmoLIVKeFBkLXUwKY73uWIL/FtobbOTZ1ILSkHAJdwKHBQNtijCUz6eXzgSwFEEsnraT5OrR7aOBRoaAp5f1s4nNrX/dNeDvkG3wkgGk/dTQP7hew2DZQNXgYNF5Tyub9yyBf4RwDnL41FY/JB2qW5HGk6KBP0AZIIirnfc6jh6LxtKC2J9EktYdxC/f05HGpKqLW1Unb7Snjq6e9UX9nhixNIDc8AbfHkWQBaN+3IURxyD8Q3EcR7dNj76fP2I0raahFqmRPJn0jbCbQ9kUolzP/hGtQl3FwuZley2zAaKoDWjuTxek3bRnsxhUPOgThE75sn61nKLM9Bufpc/x/u3DP64kcTu3TZNKwNTxdCO41EcT4V0in8kqPQeyfLhewadhtC4wQwe1FrtDVsVP4XOFI/1KTp/bolQr7SMj4vulcN8St1EWlfOlNI2UGF1UEp/CQOO4IEWFDJZ37Hruc0TADReLqHPr2dXQfAHMrQ6vLG29/mgCtEE+lvUPpeBiA+w6G6odnBeQM9uSfY9ZSGCIAq/0765BvYrQ8Ud0kdVld6Mm9yxBMi8dR1IOC7dBzHc8g21BXs0Wry3IFNXS9yyDM8F0C0PXWVQFjHrm2o0F7UUCwZKGYf55D3XLIyGg0dyJJ13WigDhC30fTwbPY8w9NpYGTe8mMEyI3UciIcsge1+vJp4+PVn9/6Okcaw2tbq9VXtm8OnXT6i9QlzKL2ZFxxZA+A48Inz5LVV7d7erGJpxmAUv/D9Imd7NoEri4VMnVnEKcZn1gxeRiH1pMQLuCQLaSonVEprNnOrutovHWdaCJ5bX2Vj7ulwHP8WPkGBwq37qV5/YXUNd3PIVtoQjO++fQMTzJAW8fS4zSJL5EZHY1YBHG7rNWuqGy+4y2OOMfZK9uiE/vOoxT8fmOQQuyvFLJPsWuZSCJ1M3VzN7JrGQT8UTmf+z67ruKJAKLxVBcV8BJ2LUFTrnekLs8c7Olyvr+fn4pEquINADiSI++DiA+Ui7kr2bUMHfM9dMzXs2sNFFUtXJ3Sv8Hciat6cL0LiFycmmK38g1oB7/qSuUT0WH48v+rfANqGXWNVWhEv5BqchO71gARrlXDKfZcxf0xQBjtHwiKa9yc5qGQh54FAbSwZZvSOG0BfYqtL31IgMkJF6/4BLuu4aoAYnMXHwWoJdm1BKXgX5WK2V+wG0x+mxmQGtrLfiAiQ+FB17OAqwKQeihFB2L9XAOK18stg7a7DT9R6el6mg7I1oAQUKQmz1s+nl1XcLcLQLR3FS+IJaL77n72Ak+pkLuFMtof2TUPwLgyDF/Gniu4JgAaBc+hAdbR7JqG5tG/KRWym9ltGlDHH7BpCRTg4BdmH8Y1ASDAfDYtoenaajabipGuAHEju6YBEBePT3zH0fscD8YlAVD1I1qeRhkDv4FHVj/PbtNBfXoXm5aoYUuCTcdxRQBtiVSn0X+xaxodZFO2/jEGNuaeIJE/yq5pqDm51g24IgAQcBGbpkGBT/YX1rzMbtNC8/tH2DQNCJwjOjtd+ebWnS4AYTpb5hm9fq/pCYXtHCdAZHCK9TI1gfMCmL2olQYun2PPNFitFdhsavo25N6jbsDyLAc0MYNNR3FcAJFIyI5S/+TKN31+BcR/2DKPnaxqAuczgLSjVHyOjcMCDYVxGZk1QAREALZ2FA4rAQwUcy/QoPcAu+YITAZAYfnsnwDZGAEcdBGI56B4hi1zgGiJdi6zXrYfg+MFAGD9wshS6F+NuWO2ho27NxLEX9gyDchhx88IOi4AHFmHzzw0Iu4V3d01dj0FNRcyoElAiP1smqZW1f0vAMCRxRhNY6cgHKOBXQAKsHzcIGQAMoDFLsBOQTiFLhuXATQj81kEQExi0zGoATpLNJGWtDH/vii2lorZutcFaEukF2gCPklvaPTrupCoIxi2ZlQybfFD+0QFegr9PuSCFKVC1vHyGcP4upx2YAu75kDxPSqr29hzBMcPMJJI99KbHsHux0LToV3lQq6us1yReHodVeZV7DqGmwJoa09doSE8yK5J8NpSIXcfO47g/BhAWEttVMIT2bQPiNPZCgwgrQ2WDRCNxS2cxfkxAIIlAdDfWy6IZsCO8GnSspdNx3A+AwD2sWkKyhj1Z4AAQoNf68KXEIQMIKyNbmkqNi6xxLHFFoIDfpYN08jWmv8zANWo5Z1EoZ/Gpi0oi7hwBTHuZsMdAGayZZpy7Aj/C4AmW7vYNI0UWJ8AJK6gzGMssfJ3R35Q7KCdcu3mTGMZXBoDWFoVjWZLz4t1qyrsOobj05zYvNS5qIGla+Cp8h4zbq1mt+kZPWchLK0fTGW0jsroanYdw/EMEAJpOQPQHP6CCfElh81gUEO0vhqqjcxqBscF0Ftcs5/k+hq7pqkK7VI2m5vZi1rpt+VjRZQ72XQUFwaBhA21oo1CCSJtbWFjrUHLdx5XWoaCkQEYy7d0A8BFbZcsPpbdpoUK3LrQEZ9y615JVwQQrlY2sGkJTQ8tZ7MpiSRSxqqolgUgAVy7ZN4VAfRuuWcf5fQeds0D4gbjgY/sNR2Awt5aCbWACcCARva2soAuZFNmgei89HQqlAXsmofS/+CmzD/ZcxzXBDBwZP8GygJ2Hrd6Hc2Tz2S7eQBha5EIN9O/gWsCEGvXVhGsnewYg3bK07Xy3CaSSC8kAdi6wdPN9G/gngAIXYL1GyBG+XwknvoJ24GGT/vaEzTiz9xM/wauCqB/Y+Ylmt//kl1L0LRwRTSeDvzpYePRd7QxTv5Ypqaj3QZkGlcFYFBHFjBODq2PtS8+ld3AEU2k7yUh23vuodH6XVof8WBcF0B9WUBMkhh6IIjfE0TiyR/SxvYy8l60fgPXBWCgi1CGTctQ/zltSGjrjWVdOeR7aPySBNDq+Tr5Di9av4EnAugv3PYyIC5j1zLGaeJoVWw1BlQc8i2RePrHtL+2ZzEo8KVSeHeaXddx/HqAj4IK51FK67PZtQG+LTVtQaUn4+lDFcwSSaTuAwHfYtcm8sJSoesxdlzHkwwwhq6LFFXiMLs2gGM0iVsjiaSvVhGNzV08jdL+4/VWPqK43cvKN/A0AxhQK7mBCupOdusACxJhcaWYdffavY8hFk8tRYC6Vzdz4gYZO3h+e/TwKzv+HJo66wRSnuV1hD4ITCX5fj089YsDxnty0DNiHalzQyfPupcGKN/mkH1QVFHAZcOvbvdczJ5ngDFoPPA4jQfOY7de3qJW2FXOr+6iQ6JZp3sYJ6foA2iUX89Y5oMgYEc5n3P1lO+haJgAjKXkpaZvoxGzY0/iRMR/U/fyMP3kB4qZJzlcN8aFKqDrHbSvHeSeNRp1Bmr5C8uFzL3sek7DBGAQbU/OEBK2URqNcchBcDc1rbwEsQNr+Mzgppxxybc5rlwaix0QMxHkTESY7WCm+l9uKhWyq9huCA0VgEEknpwLQsvTnrg6HqG0/S6liGfokPfSQfeikPvJ3gcCoyNrGuDYE8ThZKrwOscnJkBxV6mYXcRew2i4AAyM7/81gQ/R7hzDoSYHbiwVMrew01B8IQCDlo7ln9bl8EPUf7uyHJp/cP4e/3rwjQAMJs1edES5NfyQkyNs34BYQQ0uL+ezlp8Z4Ca+EsAY0XgqSwNDTx6b5hFP0zgjVc5nrK0N6AG+FIDByBo6xpU0Dk4TGwFNTVeVi7mb2PUdnp8JNEv11R1/q067cG1YDhlLo1m+lbrx4LNSwOWVYu7XHPAlvs0ABxOZl2oHgBW0t8aNFf4GRRkFZqnVe/Ls33oJhADGiLSnOwExSbt9Bof8A1d8OCyyxjMBOOp7AiWAMUYygmbcZQNf4lDj4IpvGW7N9G65dR9HA0MgBTBGpH3pTKqABI20qIsQ3q0zhKJGJZcHKQoDsj8vNq8t8SuBI9ACOJgRMUhsp9qZS+MFx68kptH8HnrfrSAlVXop0JV+ME0jgA8wPxVpG8QZoMN0qrkZAjRj+ymqQDNLsw2isSAjiJ3U0ncJqO2SKHYOFte8wa83Fc0pgENx9spQbFL/JMDhycNCn2ysvq2h1m9UuJThvZXIgfea6ZnFCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFAqFQqFQKA4XhPgvZ9VrB0D4dosAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAFlSURBVDhP1ZK/SgNBEMZnNimSiPEFjIKlYqdgsLAzjeg16gOIrdz5pxR8AL07LAXzALG5iKDJM9jqA4hPEPCS4rLjzGazySHYij84dndm9tubbxf+HLRjjvKuv6YQtzRglVC/DZK4ZVM/yAmUdk4WsFi4Q8CGDY0g+gSkozSJuzbiUHYcb35mzTrviEjrdZ1li0h0LnkC9VDx/G1TPIUTUMWCzycvE+jjNIlO+4/xKwtefrWjGz0cbrJQD0hd2XKHE+Aj9gmoM90vEq7IOHi6/SDAJjdcF39M0jIRQJxnQ95lKkUlzz8gpDmTY8RMMypYMgGLEyCAHn+zZs5FCnCDBTsmyfDaiU3jbqHsBS9iIGXZqvyyDTvG+X4S5oTcHyBQyGpVuQm5ERs2VLyAc9jgmnsbcuTewcxecEaI19IOEpieuaGa+MM9ttJ2eDiKTcgJCGIgorrgaU3WbCR7Q83fXuO/BuAbWrR4TCVR4rAAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PositionBottom, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionLeft, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionRight, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTop, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoClose"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoneText"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ManualEntry"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Position"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TwelveHour"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MeridiemHigh"
			Visible=true
			Group="Behavior"
			InitialValue="PM"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MeridiemLow"
			Visible=true
			Group="Behavior"
			InitialValue="AM"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
