#tag Class
Protected Class GraffitiWebAccordion
Inherits GraffitiControlWrapper
	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "expanded"
		      if Parameters.Ubound >= 0 then
		        dim intIndex as Integer = NthField( Parameters(0), "_", 3 ).Val
		        if intIndex >= 0 and intIndex <= UBound( Items ) then
		          dim currItem as GraffitiWebAccordionItem = Items(intIndex)
		          
		          currItem.Child.Width = me.Width
		          currItem.Child.Left = 0
		          currItem.Child.Top = 0
		          
		          currItem.Expanded = True
		          
		          UpdateStyles()
		          
		          HeaderExpanded( currItem )
		        end if
		      end if
		      
		    case "collapsed"
		      if Parameters.Ubound >= 0 then
		        dim intIndex as Integer = NthField( Parameters(0), "_", 3 ).Val
		        if intIndex >= 0 and intIndex <= UBound( Items ) then
		          dim currItem as GraffitiWebAccordionItem = Items(intIndex)
		          
		          currItem.Expanded = False
		          
		          UpdateStyles()
		          
		          HeaderCollapsed( currItem )
		        end if
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  Select case Name
		  Case "style"
		    dim strScript as String
		    if lastStyle <> WebStyle(Value).Name Then
		      if WebStyle(Value).Name = "_DefaultStyle" Then
		        strScript = strScript + "window.GSjQuery('#" + me.ControlID + "').find('.gwaccontainer').removeClass('" + lastStyle + "');" + _
		        "window.GSjQuery('#" + me.ControlID + "').find('.gwaccontainer').addClass('styledefault');"
		        strScript = strScript + "window.GSjQuery('#" + me.ControlID + "').removeClass('" + lastStyle + "');" + _
		        "window.GSjQuery('#" + me.ControlID + "').addClass('styledefault');"
		      else
		        strScript = strScript + "window.GSjQuery('#" + me.ControlID + "').find('.gwaccontainer').removeClass('" + lastStyle + "');" + _
		        "window.GSjQuery('#" + me.ControlID + "').find('.gwaccontainer').addClass('" + WebStyle(Value).Name + "');"
		        strScript = strScript + "window.GSjQuery('#" + me.ControlID + "').removeClass('" + lastStyle + "');" + _
		        "window.GSjQuery('#" + me.ControlID + "').addClass('" + WebStyle(Value).Name + "');"
		      end if
		      
		      Buffer( strScript )
		      
		      Return True
		      
		    end if
		  End Select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  dim strScript as String = "window.GSjQuery('" + me.ControlID + "').css('width', '" + Str( me.Width, "#" ) + "px').css('height', '" + Str( me.Height, "#" ) + "px');"
		  
		  Buffer( strScript )
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id=""" + me.ControlID + """ style=""overflow:auto;display:none;"">" )
		  source.Append( ItemsToString() )
		  source.Append( "</div>" )
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddItem(newItem as GraffitiWebAccordionItem)
		  if not IsNull( newItem ) then
		    Items.Append( newItem )
		    
		    if not IsNull( newItem.Child ) then
		      dim strID as String = newItem.Child.ControlID
		      dim strScript as String = "window.GSjQuery('" + strID + "').addClass('accordionWebCC');"
		      
		      Buffer( strScript )
		    end if
		    
		    UpdateOptions()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CollapseItem(ItemIndex as Integer)
		  dim theItem as GraffitiWebAccordionItem = GetItem( ItemIndex )
		  if theItem.Expanded then ToggleItem( ItemIndex )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DetachAll()
		  dim intMax as Integer = UBound( Items )
		  for intCycle as Integer = 0 to intMax
		    DetachContainer( Items( intCycle ) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DetachContainer(theItem as GraffitiWebAccordionItem)
		  if not IsNull( theItem ) then
		    if not IsNull( theItem.Child ) then
		      
		      dim strExec() as String
		      strExec.Append( "var itemContainer = window.GSjQuery('#" + theItem.Child.ControlID + "');" )
		      strExec.Append( "itemContainer.detach().appendTo('body');" )
		      strExec.Append( "itemContainer.css('display', 'none');" )
		      
		      Buffer( Join( strExec, "" ) )
		      
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoRemove(ItemIndex as Integer)
		  dim currItem as GraffitiWebAccordionItem = Items( ItemIndex )
		  if not IsNull( currItem.Child ) then
		    DetachContainer( currItem )
		  end if
		  Items.Remove( ItemIndex )
		  UpdateOptions()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExpandItem(ItemIndex as Integer)
		  dim theItem as GraffitiWebAccordionItem = GetItem( ItemIndex )
		  if not theItem.Expanded then ToggleItem( ItemIndex )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(ItemText as String) As GraffitiWebAccordionItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebAccordionItem
		  dim retItem as GraffitiWebAccordionItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Caption = ItemText then Return currItem
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(ItemText as String) As Integer
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebAccordionItem
		  dim retItem as GraffitiWebAccordionItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Caption = ItemText then Return intCycle
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetItem(ItemIndex as Integer) As GraffitiWebAccordionItem
		  if ItemIndex >= 0 and ItemIndex <= Items.Ubound then Return Items(ItemIndex)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebAccordion -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "accordion" ).Child( "gwaccordion.css" ), JavascriptNamespace ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebAccordion -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ItemCount() As Integer
		  Return Items.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemsToString() As String
		  dim strBuffer() as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebAccordionItem
		  
		  strBuffer.Append(  "<section class=""gwaccontainer"">" )
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    
		    if IsNull( currItem ) then Continue
		    if IsNull( currItem.Child ) then Continue
		    
		    strBuffer.Append( "<div>" )
		    dim strIcon as String = if( currItem.Expanded, mExpandedIcon, mCollapsedIcon )
		    if currItem.Expanded then
		      strBuffer.Append( "<input class=""accheader"" id=""gwac_" + me.ControlID + "_" + Str( intCycle, "#" ) + """ name=""accordion-" + me.ControlID + """ type=""checkbox"" data-height=""" + Str( currItem.Child.Height, "#" ) + """ checked />" )
		    else
		      strBuffer.Append( "<input class=""accheader"" id=""gwac_" + me.ControlID + "_" + Str( intCycle, "#" ) + """ name=""accordion-" + me.ControlID + """ type=""checkbox"" data-height=""" + Str( currItem.Child.Height, "#" ) + """ />" )
		    end if
		    strBuffer.Append( "<label for=""gwac_" + me.ControlID + "_" + Str( intCycle, "#" ) + """>" )
		    
		    if mExpandedIcon <> "" and mCollapsedIcon <> "" then
		      strBuffer.Append( "<i style=""padding-right: 5px;"" class=""fa " + strIcon + """></i>" )
		    end if
		    
		    strBuffer.Append( EscapeString( currItem.Caption ) + "</label>" + _
		    "<article>" + _
		    "</article>" + _
		    "</div>" )
		    
		  next
		  strBuffer.Append( "</section>" )
		  
		  Return Join( strBuffer, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_Find(ItemText as String, ChildItems() as GraffitiWebAccordionItem) As GraffitiWebAccordionItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( ChildItems )
		  
		  dim currItem as GraffitiWebAccordionItem
		  dim retItem as GraffitiWebAccordionItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Caption = ItemText then Return currItem
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_Remove(ItemText as String, ChildItems() as GraffitiWebAccordionItem) As Integer
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( ChildItems )
		  
		  dim currItem as GraffitiWebAccordionItem
		  dim retItem as GraffitiWebAccordionItem
		  
		  for intCycle = 0 to intMax
		    currItem = ChildItems(intCycle)
		    if currItem.Caption = ItemText then
		      items.Remove( intCycle )
		      Return 1
		    end if
		  next
		  
		  Return 0
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemIndex as Integer)
		  if ItemIndex <= UBound( Items ) and ItemIndex >= 0 then
		    DoRemove( ItemIndex )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemText as String)
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebAccordionItem
		  dim intRet as Integer
		  
		  for intCycle = intMax DownTo 0
		    currItem = Items(intCycle)
		    if currItem.Caption = ItemText then
		      DoRemove( intCycle )
		      Return
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToggleItem(ItemIndex as Integer)
		  dim strScript as String = "var thisInput = window.GSjQuery('input#gwac_" + me.ControlID + "_" + Str( ItemIndex, "#" ) + "');" + _
		  "thisInput.click();"
		  
		  dim theItem as GraffitiWebAccordionItem = me.Items( ItemIndex )
		  theItem.Expanded = not theItem.Expanded
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Update()
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    
		    DetachAll()
		    
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').empty();" )
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').html('" + ItemsToString() + "');" )
		    
		    dim currItem as GraffitiWebAccordionItem
		    dim currCont as WebContainer
		    
		    dim strExec() as String
		    
		    dim intCycle as Integer
		    dim intMax as Integer = UBound( Items )
		    
		    for intCycle = 0 to intMax
		      currItem = Items(intCycle)
		      currCont = currItem.Child
		      strExec.Append( "var thisInput = window.GSjQuery('#" + me.ControlID + " input#gwac_" + me.ControlID + "_" + Str( intCycle, "#" ) + "');" )
		      strExec.Append( "var sibArt = window.GSjQuery('#" + me.ControlID + " input#gwac_" + me.ControlID + "_" + Str( intCycle, "#" ) + "').nextUntil('article').last().next();" )
		      if not IsNull( currCont ) then
		        strExec.Append( "window.GSjQuery('#" + currCont.ControlID + "').appendTo(sibArt);" )
		        strExec.Append( "window.GSjQuery('#" + currCont.ControlID + "').width('100%');" )
		        strExec.Append( "window.GSjQuery('#" + currCont.ControlID + "').position('relative');" )
		        strExec.Append( "window.GSjQuery('#" + currCont.ControlID + "').css('left','0');" )
		        strExec.Append( "window.GSjQuery('#" + currCont.ControlID + "').css('top','0');" )
		        strExec.Append( "window.GSjQuery('#" + currCont.ControlID + "').css('display','block');" )
		      end if
		      
		      if currItem.Expanded then
		        strExec.Append( "sibArt.height(thisInput.attr('data-height'));")
		      end if
		      me.ExecuteJavaScript( Join( strExec ) )
		    next
		    
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + " .gwaccontainer .accheader').change(function() {" + _
		    "var sibArt = window.GSjQuery(this).nextUntil('article').last().next();" + _
		    "sibArt.height(window.GSjQuery(this).attr('data-height'));" + _
		    "if(window.GSjQuery(this).is(':checked')) {" + _
		    "Xojo.triggerServerEvent('" + self.ControlID + "', 'expanded', [window.GSjQuery(this).attr('id'),sibArt.position().left,sibArt.position().top]);" + _
		    "} else {" + _
		    "sibArt.height(0);" + _
		    "Xojo.triggerServerEvent('" + self.ControlID + "', 'collapsed', [window.GSjQuery(this).attr('id'),sibArt.position().left,sibArt.position().top]);" + _
		    "}" + _
		    "});" )
		    me.ExecuteJavaScript( "window.GSjQuery('" + me.ControlID + "').css('width', '" + Str( me.Width, "#" ) + "px').css('height', '" + Str( me.Height, "#" ) + "px');" )
		    
		    if me.Style.Name <> "_DefaultStyle" then
		      me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').find('.gwaccontainer').addClass('" + me.Style.Name + "');" )
		      me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').addClass('" + me.Style.Name + "');" )
		      lastStyle = me.Style.Name
		    else
		      me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').find('.gwaccontainer').addClass('styledefault');" )
		      me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').addClass('styledefault');" )
		      lastStyle = "styledefault"
		    end if
		    
		    UpdateStyles()
		    
		    RunBuffer()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateStyles()
		  dim strExec() as String
		  dim currItem as GraffitiWebAccordionItem
		  for intCycle as Integer = 0 to UBound( Items )
		    currItem = Items(intCycle)
		    
		    if IsNull( currItem ) then Continue
		    if IsNull( currItem.Child ) then Continue
		    
		    strExec.Append( "var thisInput = window.GSjQuery('#gwac_" + me.ControlID + "_" + Str( intCycle, "#" ) + "');" )
		    strExec.Append( "var thisLabel = thisInput.next('label');" )
		    strExec.Append( "var thisIcon = thisLabel.find('i');" )
		    
		    if not IsNull( mStyleHeader ) and not IsNull( mStyleHeaderInactive ) then
		      strExec.Append( "var isChecked = thisInput.prop('checked');" )
		      strExec.Append( "if(isChecked){" )
		      strExec.Append( "if(thisLabel.hasClass('" + mStyleHeaderInactive.Name + "')){" )
		      strExec.Append( "thisLabel.removeClass('" + mStyleHeaderInactive.Name + "');" )
		      strExec.Append( "}" )
		      strExec.Append( "thisLabel.addClass('" + mStyleHeader.Name + "');" )
		      strExec.Append( "}else{" )
		      strExec.Append( "if(thisLabel.hasClass('" + mStyleHeader.Name + "')){" )
		      strExec.Append( "thisLabel.removeClass('" + mStyleHeader.Name + "'); " )
		      strExec.Append( "}" )
		      strExec.Append( "thisLabel.addClass('" + mStyleHeaderInactive.Name + "');" )
		      strExec.Append( "}" )
		    end if
		    
		    if mExpandedIcon <> "" and mCollapsedIcon <> "" then
		      strExec.Append( "thisIcon" + _
		      if( mExpandedIcon <> "", ".removeClass('" + mExpandedIcon + "')", "" ) + _
		      if( mCollapsedIcon <> "", ".removeClass('" + mCollapsedIcon + "')", "" ) + ";" )
		      if currItem.Expanded then
		        if mExpandedIcon <> "" then strExec.Append( "thisIcon.addClass('" + mExpandedIcon + "');" )
		      else
		        if mCollapsedIcon <> "" then strExec.Append( "thisIcon.addClass('" + mCollapsedIcon + "');" )
		      end if
		    end if
		  next
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event HeaderCollapsed(CollapsedItem as GraffitiWebAccordionItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event HeaderExpanded(ExpandedItem as GraffitiWebAccordionItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCollapsedIcon
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCollapsedIcon = value
			End Set
		#tag EndSetter
		CollapsedIcon As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mExpandedIcon
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mExpandedIcon = value
			End Set
		#tag EndSetter
		ExpandedIcon As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Items() As GraffitiWebAccordionItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastStyle As String = "styledefault"
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			  
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCollapsedIcon As String = "fa-plus-square"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mExpandedIcon As String = "fa-minus-square"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleHeaderInactive As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleHeader = value
			  
			  UpdateStyles()
			End Set
		#tag EndSetter
		StyleHeaderActive As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleHeaderInactive
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleHeaderInactive = value
			  
			  UpdateStyles()
			End Set
		#tag EndSetter
		StyleHeaderInactive As WebStyle
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.accordion", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACx\rjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPDSURBVHhe7d1BaFx1EMfxmdfdmk3QgyCCoOBB\rtpc20IOYQz2oIAg1sZqTelIQBEu3Ej00WIJ4apNUwZ70qJfa0kQQvIi1h0a8uIrQ4MUeRfBgTTfJ\rbt74onPw1rxZuu/R//dzyH/+AxsC89uFt++fXQEAAAAAAAAAAAAAAAAA3C3U17Cx2XcfyXd69/gW\rI5RJc3vz0uIN34aEAzAx03klN/1QVe73FipgIn9muRzfWF36zFulhALQmnn7mIpd9C1qwMxe6q0s\rl55J5ms5Zu95hbpQnfeqlFAAimf/QS9RE2p2yMtSYq8AqrHH4c4JzoRBJo4AJI4AJI4AJI4AJI4A\rJC4agN98RX2EZhIKgIl97iVqIjqTfb6WMri+9k3jwNTDKnLYW6iQiXzau7x8wrelDHU7ePyFzqTk\r2aHiT+B2cCV0Sxva3bh49idvAAAAAMBtDHUZOHH0+IO2L5sUyfZ7CyOVb+tO3t348qPfvVFaOADj\rM5354uHv+xbVmr91eekDr0sJvRPYmu68qapnfYvqPdVoP/HHYH3tB9/vWfRm0Fu+oj5CMwkFoHj2\rH/ASNRGdCecBEkcAEkcAEkcAEkcAEkcAEkcAEhcKgImte4maMJHQTGKvALmc9wp1YfKxV6XETgWv\rr33fbE/1ReVpb6FKJqd6K0uLvitlqNvB8vzcveM6OGKajXkHI6SWb96yxlVZPXPTWwAAAABwG0Nd\rBo5NnziiqpNqymVgBUxky0y7m6uL33mrtHAAWjMnPyke/JpvUaH//j186XXflhI8FXxyQVU6vkXF\riifi4UZ7KhusX/vWW3sWuhegaq96iZqIziR4O1gf9QK1EZsJ5wESRwASRwASRwASRwASRwASFw2A\r+Yr6CM0kFAAz+9lL1IRJbCaxAGgW+jQK3DlFAEb3CSGD69d+abandr+x8hlR4fOBqmTyd/Hzjd7K\r8ui+OPL/9h+be6zZ7xOCCvSbze3tS2d+9S0AAAAA7MFQl4Gt6c6LqvqkiUx4CyNUDG/D8vxqb/Xc\rF94qLRaA06ezVvevr1T0We+gQib2dW/yvudkYSH31p6F3goe7948z/DrY3cWuzPxbSnBu4F21AvU\rRmwmwQDoQ16gNmIzCQYAdwsCkDgCkDgCkDgCkDgCkLhoADZ9RX2EZhILgAlfV143wZPaoQDkau94\riZrIVea8LCV4KnjtRtZ+/EqmelBMHxCN/R4MyaQvaj/uWP7y1sq5K94tZehTwf+anSUAVbhwYccr\rAAAAAAAAAAAAAAAAAAAAAAAAAEAqRP4BxEXdbT4xoAkAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAB8SURBVDhPYxhwwAilGTj9ixoZGf/zQrl4wf//jJ+/b+yrB7HhBnAFFP2HMokC3zb0gfXCDeAIKAiDMokCPzZMWAWiES7wLzoGZRIFvm3sswLRTGAeBYCKXqA0ECmORs6Awh2M/xn5oFy84D/j/0/fN/R7gNgUe2EUMDAAABjHMAwmilUpAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="CollapsedIcon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ExpandedIcon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
