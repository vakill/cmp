#tag Class
Protected Class GraffitiWebAccordionItem
	#tag Method, Flags = &h0
		Sub Constructor(ItemText as String, IsExpanded as Boolean, ItemChild as WebContainer)
		  Caption = ItemText
		  if not IsNull( ItemChild ) then
		    Child = ItemChild
		    InitW = Child.Width
		    InitH = Child.Height
		  end if
		  Expanded = IsExpanded
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Caption As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Child As WebContainer
	#tag EndProperty

	#tag Property, Flags = &h0
		Expanded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private InitH As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private InitW As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Expanded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
