#tag Class
Protected Class GraffitiWebChart
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "animprogress"
		      misAnimating = True
		    case "animcomplete"
		      if UBound( Parameters ) >= 0 then
		        mImageData = Parameters(0)
		        misAnimating = False
		        AnimationComplete()
		      end if
		    case "graphclick"
		      
		      if Parameters.Ubound < 0 then Return True
		      if CountFields( Parameters(Parameters.Ubound), "," ) < 2 then Return True
		      
		      if isAnimating then Return true
		      
		      dim pClicked() as GraffitiWebChartDataset
		      
		      dim strCurr as String
		      
		      dim intCycle as Integer
		      dim intMax as Integer = UBound( Parameters )
		      for intCycle = 0 to intMax
		        strCurr = Parameters(intCycle)
		        if CountFields( strCurr, "," ) >= 3 then
		          dim dsFind as GraffitiWebChartDataset
		          Select case ChartType
		          case ChartTypes.Polar, ChartTypes.Pie, ChartTypes.Doughnut
		            dsFind = FindDataset_Single( NthField( strCurr, ",", 2 ), NthField( strCurr, ",", 3 ).Val )
		          case else
		            dsFind = FindDataset_Multiple( NthField( strCurr, ",", 1 ), NthField( strCurr, ",", 3 ).Val )
		          End Select
		          if not IsNull( dsFind ) then pClicked.Append( dsFind )
		        end if
		      next
		      PointsClicked( pClicked, NthField( Parameters(Parameters.Ubound ), ",", 2 ) )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '// 
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  DoResize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "'>" )
		  source.Append( "<canvas class=""graffitiwebchart"" id=""" + me.ControlID + "_chart"" style='width:100%; height: 100%;background:" + ColorToRGBAString( mBackgroundColor ) + "'></canvas>" )
		  source.Append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddDataset(newDataset as GraffitiWebChartDataset, newIndex as Integer = -1)
		  if not IsNull( newDataset ) then
		    if newIndex < 0 then
		      Datasets.Append( newDataset )
		    else
		      Datasets.Insert( newIndex, newDataset )
		    end if
		    
		    UpdateOptions()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DataToString_Multiple() As String
		  dim strRet as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Datasets )
		  
		  dim currSet as GraffitiWebChartDataset
		  
		  for intCycle = 0 to intMax
		    currSet = Datasets(intCycle)
		    strRet = strRet + "{label:'" + EscapeString( currSet.Label ) + _
		    "',fillColor: '" + ColorToRGBAString( currSet.FillColor ) + _
		    "',strokeColor: '" + ColorToRGBAString( currSet.StrokeColor ) + _
		    "',highlightFill: '" + ColorToRGBAString( currSet.HighlightFill ) + _
		    "',highlightStroke: '" + ColorToRGBAString( currSet.HighlightStroke ) + _
		    "',pointColor: '" + ColorToRGBAString( currSet.PointColor ) + _
		    "',pointStrokeColor: '" + ColorToRGBAString( currSet.PointStrokeColor ) + _
		    "',pointHighlightFill: '" + ColorToRGBAString( currSet.PointHighlightFill ) + _
		    "',pointHighlightStroke: '" + ColorToRGBAString( currSet.PointHighlightStroke ) + _
		    "',color: '" + ColorToRGBAString( currSet.FillColor ) + _
		    "',highlight: '" + ColorToRGBAString( currSet.HighlightFill ) + _
		    "',value: " + Str( currSet.DatasetValue, "#" ) + _
		    ",data: [" + JoinDataPoints( currSet.Data ) + "]" + _
		    "}"
		    if intCycle < intMax then strRet = strRet + ","
		  next
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DataToString_Single() As String
		  dim strRet as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Datasets )
		  
		  dim currSet as GraffitiWebChartDataset
		  
		  for intCycle = 0 to intMax
		    currSet = Datasets(intCycle)
		    strRet = strRet + "{label:'" + EscapeString( currSet.Label ) + _
		    "',highlight: '" + ColorToRGBAString( currSet.HighlightFill ) + _
		    "',color: '" + ColorToRGBAString( currSet.FillColor ) + _
		    "',value: " + Str( currSet.DatasetValue ) + _
		    "}"
		    if intCycle < intMax then strRet = strRet + ","
		  next
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteAll()
		  LockUpdate = True
		  
		  ReDim Datasets(-1)
		  ReDim Labels(-1)
		  'UpdateOptions()
		  
		  'dim strOut() as String
		  'strOut.Append( "if(typeof(window.gchart" + me.ControlID + ") !== 'undefined') {" )
		  'for each d as GraffitiWebChartDataset in Datasets
		  'strOut.Append( "window.gchart" + me.ControlID + ".removeData();" )
		  'next
		  'strOut.Append( "window.gchart" + me.ControlID + ".clear();" )
		  'strOut.Append( "}" )
		  'Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DoResize()
		  if LibrariesLoaded then
		    'me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').width(" + Str( me.Width, "#" ) + ")" + _
		    '".height(" + Str( me.Height, "#" ) + ");" )
		    'me.ExecuteJavaScript( "window.gchart" + me.ControlID + ".resize();" )
		  end if
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EasingToString(theEasing as Easings) As String
		  select case theEasing
		  case Easings.linear
		    Return "linear"
		  case Easings.easeInBack
		    Return "easeInBack"
		  case Easings.easeInBounce
		    Return "easeInBounce"
		  case Easings.easeInCirc
		    Return "easeInCirc"
		  case Easings.easeInCubic
		    Return "easeInCubic"
		  case Easings.easeInElastic
		    Return "easeInElastic"
		  case Easings.easeInExpo
		    Return "easeInExpo"
		  case Easings.easeInQuad
		    Return "easeInQuad"
		  case Easings.easeInQuart
		    Return "easeInQuart"
		  case Easings.easeInQuint
		    Return "easeInQuint"
		  case Easings.easeInSine
		    Return "easeInSine"
		    
		  case Easings.easeOutBack
		    Return "easeOutBack"
		  case Easings.easeOutBounce
		    Return "easeOutBounce"
		  case Easings.easeOutCirc
		    Return "easeOutCirc"
		  case Easings.easeOutCubic
		    Return "easeOutCubic"
		  case Easings.easeOutElastic
		    Return "easeOutElastic"
		  case Easings.easeOutExpo
		    Return "easeOutExpo"
		  case Easings.easeOutQuad
		    Return "easeOutQuad"
		  case Easings.easeOutQuart
		    Return "easeOutQuart"
		  case Easings.easeOutQuint
		    Return "easeOutQuint"
		  case Easings.easeOutSine
		    Return "easeOutSine"
		    
		  case Easings.easeInOutBack
		    Return "easeInOutBack"
		  case Easings.easeInOutBounce
		    Return "easeInOutBounce"
		  case Easings.easeInOutCirc
		    Return "easeInOutCirc"
		  case Easings.easeInOutCubic
		    Return "easeInOutCubic"
		  case Easings.easeInOutElastic
		    Return "easeInOutElastic"
		  case Easings.easeInOutExpo
		    Return "easeInOutExpo"
		  case Easings.easeInOutQuad
		    Return "easeInOutQuad"
		  case Easings.easeInOutQuart
		    Return "easeInOutQuart"
		  case Easings.easeInOutQuint
		    Return "easeInOutQuint"
		  case Easings.easeInOutSine
		    Return "easeInOutSine"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FindDataset_Multiple(sDatasetLabel as String, dValue as Double) As GraffitiWebChartDataset
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Datasets )
		  
		  dim currDS as GraffitiWebChartDataset
		  
		  for intCycle = 0 to intMax
		    currDS = Datasets(intCycle)
		    if sDatasetLabel = currDS.Label and me.InArray( currDS.Data, dValue ) then
		      Return currDS
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FindDataset_Single(sPointLabel as String, dValue as Double) As GraffitiWebChartDataset
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Datasets )
		  
		  dim currDS as GraffitiWebChartDataset
		  
		  for intCycle = 0 to intMax
		    currDS = Datasets(intCycle)
		    if currDS.DatasetValue = dValue and sPointLabel = currDS.Label then
		      Return currDS
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FontStyleToString(theStyle as FontStyles) As String
		  select case theStyle
		  case FontStyles.Bold
		    Return "bold"
		  case FontStyles.Italic
		    Return "italic"
		  case FontStyles.BoldItalic
		    Return "bold italic"
		  end select
		  
		  Return "normal"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetChartType() As String
		  select case ChartType
		  case ChartTypes.Doughnut
		    Return "Doughnut"
		  case ChartTypes.Line
		    Return "Line"
		  case ChartTypes.Pie
		    Return "Pie"
		  case ChartTypes.Polar
		    Return "PolarArea"
		  case ChartTypes.Radar
		    Return "Radar"
		  case ChartTypes.StackedBar
		    Return "StackedBar"
		  case else
		    Return "Bar"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebChart -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gchart" ).Child( "Chart.Core.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gchart" ).Child( "Chart.StackedBar.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebChart -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function InArray(TheArray() as Double, dValue as Double) As Boolean
		  for intCycle as Integer = 0 to UBound( TheArray )
		    if TheArray(intCycle) = dValue then Return True
		  next
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function JoinDataPoints(Points() as Double) As String
		  dim strRet as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Points )
		  for intCycle = 0 to intMax
		    strRet = strRet + Str( Points(intCycle), "#######0.0#######" )
		    if intCycle < intMax then strRet = strRet + ","
		  next
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function LabelsToString() As String
		  dim strOut() as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Labels.Ubound
		  
		  dim currLabel as String
		  
		  for intCycle = 0 to intMax
		    currLabel = ReplaceLineEndings( EscapeString( Labels(intCycle) ), EndOfLine )
		    currLabel = currLabel.ReplaceAll( "/n", EndOfLine )
		    currLabel = currLabel.ReplaceAll( "<br>", EndOfLine )
		    currLabel = currLabel.ReplaceAll( "<br/>", EndOfLine )
		    currLabel = currLabel.ReplaceAll( "<br />", EndOfLine )
		    
		    dim labelParts() as String = Split( currLabel, EndOfLine )
		    if labelParts.Ubound > 0 then
		      strOut.Append( "['" + Join( labelParts, "','" )  + "']" )
		    else
		      strOut.Append( "'" + currLabel + "'" )
		    end if
		    if intCycle < intMax then strOut.Append( "," )
		  next
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function OptionsToString() As String
		  dim strRet() as String
		  strRet.Append( "{" )
		  strRet.Append( "responsive: true," )
		  strRet.Append( "animation: " + Str( mAnimation ).Lowercase + "," )
		  strRet.Append( "animationStep: " + Str( AnimationSteps ) + "," )
		  strRet.Append( "animationEasing: '" + EasingToString( mAnimationEasing ) + "'," )
		  
		  strRet.Append( "scaleShowLabels: " + Str( mLabelsVisible ).Lowercase + "," )
		  if mLabelsVisible then
		    if mLabelFontFamily.Len > 0 then strRet.Append( "scaleFontFamily: '" + EscapeString( mLabelFontFamily ) + "'," )
		    if mLabelFontSize > 0 then strRet.Append( "scaleFontSize: " + Str( mLabelFontSize ) + "," )
		    if mLabelFontStyle <> FontStyles.Normal then strRet.Append( "scaleFontStyle: '" + FontStyleToString( mLabelFontStyle ) + "'," )
		    strRet.Append( "scaleFontColor: '" + ColorToRGBAString( mLabelFontColor ) + "'," )
		  end if
		  
		  if mScaleOverride then
		    strRet.Append( "scaleOverride: true," )
		    strRet.Append( "scaleStartValue: " + Str( mStartValue ) + "," )
		    strRet.Append( "scaleSteps: " + Str( mScaleSteps ) + "," )
		    strRet.Append( "scaleStepWidth: " + Str( mScaleStepWidth ) + "," )
		  end if
		  
		  strRet.Append( "animationSteps: " + Str( mAnimationSteps, "#" ) + "," )
		  strRet.Append( "bezierCurve: " + Lowercase( Str( mBezierCurve ) ) + "," )
		  strRet.Append( "bezierCurveTension: " + Str( mBezierCurveTension ) + "," )
		  strRet.Append( "datasetFill: " + Lowercase( Str( mDatasetFill ) ) + "," )
		  strRet.Append( "datasetStroke: " + Lowercase( Str( mDatasetStroke ) ) + "," )
		  strRet.Append( "datasetStrokeWidth: " + Str( mDatasetStrokeWidth ) + "," )
		  strRet.Append( "scaleGridLineColor: '" + ColorToRGBAString( mGridLineColor ) + "'," )
		  strRet.Append( "scaleShowGridLines: " + Lowercase( Str( mShowGridLines ).Lowercase ) + "," )
		  strRet.Append( "scaleGridLineWidth: " + Str(  mGridLineWidth ) + "," )
		  strRet.Append( "maintainAspectRatio: false," )
		  strRet.Append( "pointDotRadius: " + Str( mPointDotRadius ) + "," )
		  strRet.Append( "pointDotStrokeWidth: " + Str( mPointDotStrokeWidth ) + "," )
		  strRet.Append( "pointHitDetectionRadius: " + Str( mPointHitDetectionRadius ) + "," )
		  strRet.Append( "showTooltips: " + Lowercase( Str( mShowTooltips ) ) + "," )
		  strRet.Append( "pointDot: " + Lowercase( Str( mShowPointDot ) ) + "," )
		  strRet.Append( "tooltipFillColor: '" + ColorToRGBAString( mTooltipFillColor ) + "'," )
		  strRet.Append( "tooltipFontColor: '" + ColorToRGBAString( mTooltipFontColor ) + "'," )
		  strRet.Append( "tooltipTitleFontColor: '" + ColorToRGBAString( mTooltipTitleFontColor ) + "'," )
		  if instr( mTooltipTemplate, "function" ) > 0 then
		    strRet.Append( "tooltipTemplate: " + EscapeString( TooltipTemplate ) + "," )
		  else
		    strRet.Append( "tooltipTemplate: '" + EscapeString( TooltipTemplate ) + "'," )
		  end if
		  if InStr( mMultiTooltipTemplate, "function" ) > 0 then
		    strRet.Append( "multiTooltipTemplate: " + EscapeString( MultiTooltipTemplate ) + "," )
		  else
		    strRet.Append( "multiTooltipTemplate: '" + EscapeString( MultiTooltipTemplate ) + "'," )
		  end if
		  
		  '"animation: " + Lowercase( Str( mAnimation ) ) + "," + _
		  
		  '"onAnimationProgress: function(){" + _
		  '"Xojo.triggerServerEvent('" + self.ControlID + "', 'animprogress',[]);" + _
		  '"}," + _
		  strRet.Append( "onAnimationComplete: function(){" )
		  strRet.Append( "var chartURL = document.getElementById('" + me.ControlID + "_chart').toDataURL();" )
		  strRet.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'animcomplete',[chartURL]);" )
		  strRet.Append( "}" )
		  strRet.Append( "}" )
		  
		  Return Join( strRet, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveDataset(DataIndex as Integer)
		  me.Datasets.Remove( DataIndex )
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBarFill(DatasetIndex as Integer, BarIndex as Integer, NewColor as Color)
		  dim strOut() as String
		  strOut.Append( "if (" + Str( DatasetIndex, "#" ) + " < window.gchart" + me.ControlID + ".datasets.length) {" )
		  strOut.Append( "if (" + Str( BarIndex, "#" ) + " < window.gchart" + me.ControlID + ".datasets[" + Str( DatasetIndex, "#" ) + "].bars.length) {" )
		  strOut.Append( "window.gchart" + me.ControlID + ".datasets[" + Str( DatasetIndex, "#" ) + "]" )
		  strout.Append( ".bars[" + Str( BarIndex, "#" ) + "]" )
		  strOut.Append( ".fillColor = '" + ColorToRGBAString( NewColor ) + "';" )
		  strOut.Append( "window.gchart" + me.ControlID + ".update();" )
		  strOut.Append( "}}" )
		  
		  Buffer( Join( strOut, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBarHighlightFill(DatasetIndex as Integer, BarIndex as Integer, NewColor as Color)
		  dim strOut() as String
		  strOut.Append( "window.gchart" + me.ControlID + ".datasets[" + Str( DatasetIndex, "#" ) + "]" )
		  strout.Append( ".bars[" + Str( BarIndex, "#" ) + "]" )
		  strOut.Append( ".highlightFill = '" + ColorToRGBAString( NewColor ) + "';" )
		  strOut.Append( "window.gchart" + me.ControlID + ".update();" )
		  
		  Buffer( Join( strOut, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBarHighlightStroke(DatasetIndex as Integer, BarIndex as Integer, NewColor as Color)
		  dim strOut() as String
		  strOut.Append( "window.gchart" + me.ControlID + ".datasets[" + Str( DatasetIndex, "#" ) + "]" )
		  strout.Append( ".bars[" + Str( BarIndex, "#" ) + "]" )
		  strOut.Append( ".highlightStroke = '" + ColorToRGBAString( NewColor ) + "';" )
		  strOut.Append( "window.gchart" + me.ControlID + ".update();" )
		  
		  Buffer( Join( strOut, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBarStroke(DatasetIndex as Integer, BarIndex as Integer, NewColor as Color)
		  dim strOut() as String
		  strOut.Append( "window.gchart" + me.ControlID + ".datasets[" + Str( DatasetIndex, "#" ) + "]" )
		  strout.Append( ".bars[" + Str( BarIndex, "#" ) + "]" )
		  strOut.Append( ".strokeColor = '" + ColorToRGBAString( NewColor ) + "';" )
		  strOut.Append( "window.gchart" + me.ControlID + ".update();" )
		  
		  Buffer( Join( strOut, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded and not LockUpdate then
		    
		    if not me.Visible or not me.Page.Visible then Return
		    
		    dim strExec() as String
		    strExec.Append( "if (typeof(window.gchart" + me.ControlID + ") != 'undefined'){" )
		    'strExec.Append( "window.gchart" + me.ControlID + ".removeData();" )
		    strExec.Append( "window.gchart" + me.ControlID + ".destroy();" )
		    strExec.Append( "}" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').unbind('click');" )
		    Buffer( strExec )
		    
		    ReDim strExec(-1)
		    
		    dim strData as String = "var data = ["
		    
		    select case ChartType
		    case ChartTypes.Doughnut, ChartTypes.Pie, ChartTypes.Polar
		      strData = "var data = [" + DataToString_Single() + "];"
		    case else
		      'strData = "var data = {" + "labels: ['" + Join( Labels, "','" ) + "']," + _
		      strData = "var data = {" + "labels: [" + LabelsToString() + "]," + _
		      "datasets: [" + DataToString_Multiple() + "]};"
		    end select
		    
		    dim strOptions as String = OptionsToString()
		    
		    dim strType as String = GetChartType()
		    
		    strExec.Append( strData + "var ctx = window.GSjQuery('#" + me.ControlID + "_chart')[0].getContext('2d');" + _
		    "window.gchart" + me.ControlID + " = new Chart(ctx)." + strType + "(data," + strOptions + ");" )
		    
		    'strExec.Append( "window.onresize = function(event){" )
		    'strExec.Append( "var width = window.GSjQuery('#" + me.ControlID + "_chart').parent().width();" )
		    'strExec.Append( "window.GSjQuery('" + me.ControlID + "_chart').attr('width',width);" )
		    'strExec.Append( "new Chart(ctx)." + strType + "(data," + strOptions + ");" )
		    'strExec.Append( "};" )
		    
		    select case ChartType
		    case ChartTypes.Line, ChartTypes.Radar
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('click', function(evt){" + _
		      "var activePoints = window.gchart" + me.ControlID + ".getPointsAtEvent(evt);" + _
		      "var strPoints = [];" + _
		      "if (activePoints.length >= 0) {" + _
		      "for (i = 0; i < activePoints.length; i++) {" + _
		      "strPoints[i] = activePoints[i].datasetLabel + ',' + activePoints[i].label + ',' + activePoints[i].value + ',' + activePoints[i].x + ',' + activePoints[i].y;" + _
		      "if (i < activePoints.length) {" + _
		      "}" + _
		      "}" + _
		      "Xojo.triggerServerEvent('" + self.ControlID + "', 'graphclick', strPoints);" + _
		      "}" + _
		      "});" )
		    case ChartTypes.Bar, ChartTypes.StackedBar
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('click', function(evt){" + _
		      "var activePoints = window.gchart" + me.ControlID + ".getBarsAtEvent(evt);" + _
		      "var strPoints = [];" + _
		      "if (activePoints.length >= 0) {" + _
		      "for (i = 0; i < activePoints.length; i++) {" + _
		      "strPoints[i] = activePoints[i].datasetLabel + ',' + activePoints[i].label + ',' + activePoints[i].value + ',' + activePoints[i].x + ',' + activePoints[i].y;" + _
		      "if (i < activePoints.length) {" + _
		      "}" + _
		      "}" + _
		      "Xojo.triggerServerEvent('" + self.ControlID + "', 'graphclick', strPoints);" + _
		      "}" + _
		      "});" )
		    case ChartTypes.Polar, ChartTypes.Pie, ChartTypes.Doughnut
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('click', function(evt){" + _
		      "var activePoints = window.gchart" + me.ControlID + ".getSegmentsAtEvent(evt);" + _
		      "var strPoints = [];" + _
		      "if (activePoints.length >= 0) {" + _
		      "for (i = 0; i < activePoints.length; i++) {" + _
		      "strPoints[i] = activePoints[i].datasetLabel + ',' + activePoints[i].label + ',' + activePoints[i].value + ',' + activePoints[i].x + ',' + activePoints[i].y;" + _
		      "if (i < activePoints.length) {" + _
		      "}" + _
		      "}" + _
		      "Xojo.triggerServerEvent('" + self.ControlID + "', 'graphclick', strPoints);" + _
		      "}" + _
		      "});" )
		    end select
		    
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		    RunBuffer()
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event AnimationComplete()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PointsClicked(Datasets() as GraffitiWebChartDataset, XAxis as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://www.chartjs.org/
		
		Licensed under MIT
		http://opensource.org/licenses/MIT
		
		Documentation
		https://github.com/chartjs/Chart.js/tree/v1.1.0/docs
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimation
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimation = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Animation As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationEasing
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationEasing = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AnimationEasing As Easings
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationSteps
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationSteps = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AnimationSteps As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBackgroundColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBackgroundColor = value
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + " canvas').css('background-color', '" + ColorToRGBAString( mBackgroundColor ) + "');" )
			End Set
		#tag EndSetter
		BackgroundColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBezierCurve
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBezierCurve = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		BezierCurve As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBezierCurveTension
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBezierCurveTension = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		BezierCurveTension As Double
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mChartType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mChartType = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ChartType As ChartTypes
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDatasetFill
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDatasetFill = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		DatasetFill As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Datasets() As GraffitiWebChartDataset
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDatasetStroke
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDatasetStroke = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		DatasetStroke As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDatasetStrokeWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDatasetStrokeWidth = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		DatasetStrokeWidth As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mGridLineColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mGridLineColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		GridLineColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mGridLineWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mGridLineWidth = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		GridLineWidth As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  try
			    dim pRet as Picture = Picture.FromData( DecodeBase64( mImageData.NthField( ",", 2 ) ) )
			    Return pRet
			  catch
			    dim pRet as new Picture( 20, 20 )
			    Return pRet
			  end try
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		Image As Picture
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mImageData
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		ImageData As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return misAnimating
			End Get
		#tag EndGetter
		isAnimating As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsBar As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsCore As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsDoughnut As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsLegend As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsLine As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsPolar As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsRadar As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsStackedBar As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelFontColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelFontColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelFontColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelFontFamily
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelFontFamily = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelFontFamily As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelFontSize
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelFontSize = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelFontSize As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelFontStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelFontStyle = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelFontStyle As FontStyles
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Labels() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelsVisible
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelsVisible = value
			End Set
		#tag EndSetter
		LabelsVisible As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLockUpdate = value
			  
			  if not mLockUpdate then UpdateOptions()
			End Set
		#tag EndSetter
		LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAnimation As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAnimationEasing As Easings
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAnimationSteps As Integer = 60
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBackgroundColor As Color = &cFFFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBezierCurve As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBezierCurveTension As Double = 0.4
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mChartType As ChartTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDatasetFill As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDatasetStroke As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDatasetStrokeWidth As Integer = 2
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mGridLineColor As Color = &c000000dd
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mGridLineWidth As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mImageData As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private misAnimating As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelFontColor As Color
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelFontFamily As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelFontSize As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelFontStyle As FontStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelsVisible As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMultiTooltipTemplate As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPointDotRadius As Integer = 4
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPointDotStrokeWidth As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPointHitDetectionRadius As Integer = 20
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mScaleOverride As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mScaleSteps As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mScaleStepWidth As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowGridLines As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowPointDot As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowTooltips As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStartValue As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipFillColor As Color = &c0000004F
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipFontColor As Color = &cFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipTemplate As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTooltipTitleFontColor As Color = &cFFFFFF
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMultiTooltipTemplate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMultiTooltipTemplate = value
			End Set
		#tag EndSetter
		MultiTooltipTemplate As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPointDotRadius
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPointDotRadius = value
			  
			  UpdateOptions ()
			End Set
		#tag EndSetter
		PointDotRadius As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPointDotStrokeWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPointDotStrokeWidth = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		PointDotStrokeWidth As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPointHitDetectionRadius
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPointHitDetectionRadius = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		PointHitDetectionRadius As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mScaleOverride
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mScaleOverride = value
			End Set
		#tag EndSetter
		ScaleOverride As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStartValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStartValue = value
			End Set
		#tag EndSetter
		ScaleStart As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mScaleSteps
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mScaleSteps = value
			End Set
		#tag EndSetter
		ScaleSteps As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mScaleStepWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mScaleStepWidth = value
			End Set
		#tag EndSetter
		ScaleStepWidth As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowGridLines
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowGridLines = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowGridLines As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowPointDot
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowPointDot = value
			  
			  UpdateOptions
			End Set
		#tag EndSetter
		ShowPointDot As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowTooltips
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowTooltips = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowTooltips As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipFillColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipFillColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TooltipFillColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipFontColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipFontColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TooltipFontColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipTemplate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipTemplate = value
			End Set
		#tag EndSetter
		TooltipTemplate As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTooltipTitleFontColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTooltipTitleFontColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TooltipTitleFontColor As Color
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.chart", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAJFSURBVHhe7dxNihNBHEDxqs6gM+YAegc3gqdwIRNxo+DXCWQyuPAOg7ZH8GvhRjKz8CCDeArXEwVhyij/TZiOpJPY2nnvB0W6FtWQ5NFN0SRJkiRJkiRJkiRJkiRJkrQtcrzOGd49uB6HF5x9fPUlDrUFGgO4MjoscXhBTuXh2XH9PqbquSpeBWUAcAYAZwBwBgBnAHBuAzu2tz9+PfvQ78e0lelJfTkON8YrwL+Q86WVxl9gAHAGAGcAcAYAZwBwvd8G7t4Z38vn6VFMWylVevt9Un+IaSd+bwNzfhzTVqbHLxu/r3X0/gowKGln9oHeWmX8WhunwfIWAGcAcAYAZwBwBgBnAHAGAGcAcAYAZwBwBgBnAHAGANf7x8HD0fhBSfldTFv503vZG42PcsrPYtpOLjemk/o0ZnN8HKz/igHAGQCcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAXGdPA4e3n149rwYHMW2lVOl00Y84fRq4ns6uAGVncG32xp+vNEq6GafRhnkLgDMAOAOAMwA4A4AzADgDgDMAOAOAMwA4A4AzADgDgDMAOAOAMwA4A4AzADgDgDMAOAOAMwA4A4AzADgDgDMAOAOA6zSAktLnVUYsX6hpzTIjli/UtGaZEcub5epH05plRpxho3r/X8Faj7cAOAOAMwA4A4AzADgDgGu9DVT/lFLefDupn8R0jlcAOAOAMwA4A4AzADgDgGveBu4fNm4Z1FNV/jqdvPgUM0mSJEmSJEmSJEmSJEmStHVS+gkNlORHX0nzFgAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAC1SURBVDhPYxgcgNu/sBjKJBkwgYj/DIzBYB4BALKIK6CwD8oFA7AB2ABnQOFsEIZywQBiEWMhlAsGOA1g/M+oDcJQLk6A0wAY4AgoCOMKKPqP7nQYQDGA06/ABKSBwydPDipEEKAYwMjEGMXEwLSSgYXJAipEEBD0AiFAXQP+//u/7B/Dv3CGP/9OMDD+awBjIBskBpID8cHySIARRHD5Fx0D80gA/xn/X/2+oT8Vyh26gIEBAEtIM+WT/d50AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag Enum, Name = ChartTypes, Type = Integer, Flags = &h0
		Bar
		  Line
		  Radar
		  Doughnut
		  Pie
		  StackedBar
		Polar
	#tag EndEnum

	#tag Enum, Name = Easings, Type = Integer, Flags = &h0
		linear
		  easeInBack
		  easeInBounce
		  easeInCirc
		  easeInCubic
		  easeInElastic
		  easeInExpo
		  easeInQuad
		  easeInQuart
		  easeInQuint
		  easeInSine
		  easeOutBack
		  easeOutBounce
		  easeOutCirc
		  easeOutCubic
		  easeOutElastic
		  easeOutExpo
		  easeOutQuad
		  easeOutQuart
		  easeOutQuint
		  easeOutSine
		  easeInOutBack
		  easeInOutBounce
		  easeInOutCirc
		  easeInOutCubic
		  easeInOutElastic
		  easeInOutExpo
		  easeInOutQuad
		  easeInOutQuart
		  easeInOutQuint
		easeInOutSine
	#tag EndEnum

	#tag Enum, Name = FontStyles, Type = Integer, Flags = &h0
		Normal
		  Bold
		  Italic
		BoldItalic
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Animation"
			Visible=true
			Group="Global"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AnimationEasing"
			Group="Behavior"
			Type="Easings"
			EditorType="Enum"
			#tag EnumValues
				"0 - linear"
				"1 - easeInBack"
				"2 - easeInBounce"
				"3 - easeInCirc"
				"4 - easeInCubic"
				"5 - easeInElastic"
				"6 - easeInExpo"
				"7 - easeInQuad"
				"8 - easeInQuart"
				"9 - easeInQuint"
				"10 - easeInSine"
				"11 - easeOutBack"
				"12 - easeOutBounce"
				"13 - easeOutCirc"
				"14 - easeOutCubic"
				"15 - easeOutElastic"
				"16 - easeOutExpo"
				"17 - easeOutQuad"
				"18 - easeOutQuart"
				"19 - easeOutQuint"
				"20 - easeOutSine"
				"21 - easeInOutBack"
				"22 - easeInOutBounce"
				"23 - easeInOutCirc"
				"24 - easeInOutCubic"
				"25 - easeInOutElastic"
				"26 - easeInOutExpo"
				"27 - easeInOutQuad"
				"28 - easeInOutQuart"
				"29 - easeInOutQuint"
				"30 - easeInOutSine"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="AnimationSteps"
			Visible=true
			Group="Global"
			InitialValue="60"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BezierCurve"
			Visible=true
			Group="Line Chart"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BezierCurveTension"
			Visible=true
			Group="Line Chart"
			InitialValue="0.4"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ChartType"
			Visible=true
			Group="Global"
			Type="ChartTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - Bar"
				"1 - Line"
				"2 - Radar"
				"3 - Doughnut"
				"4 - Pie"
				"5 - StackedBar"
				"6 - Polar"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="DatasetFill"
			Visible=true
			Group="Line Chart"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DatasetStroke"
			Visible=true
			Group="Line Chart"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DatasetStrokeWidth"
			Visible=true
			Group="Line Chart"
			InitialValue="2"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GridLineColor"
			Visible=true
			Group="Global"
			InitialValue="&c000000dd"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GridLineWidth"
			Visible=true
			Group="Global"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Image"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ImageData"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="isAnimating"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelFontColor"
			Visible=true
			Group="Labels"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelFontFamily"
			Visible=true
			Group="Labels"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelFontSize"
			Visible=true
			Group="Labels"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelFontStyle"
			Visible=true
			Group="Labels"
			Type="FontStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Normal"
				"1 - Bold"
				"2 - Italic"
				"3 - BoldItalic"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelsVisible"
			Visible=true
			Group="Labels"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockUpdate"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MultiTooltipTemplate"
			Visible=true
			Group="Behavior"
			InitialValue="<%= label %> - <%= value %>"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointDotRadius"
			Visible=true
			Group="Line Chart"
			InitialValue="4"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointDotStrokeWidth"
			Visible=true
			Group="Line Chart"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointHitDetectionRadius"
			Visible=true
			Group="Line Chart"
			InitialValue="20"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScaleOverride"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScaleStart"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScaleSteps"
			Visible=true
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScaleStepWidth"
			Visible=true
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowGridLines"
			Visible=true
			Group="Global"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowPointDot"
			Visible=true
			Group="Line Chart"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowTooltips"
			Visible=true
			Group="Global"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipFillColor"
			Visible=true
			Group="Global"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipFontColor"
			Visible=true
			Group="Global"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipTemplate"
			Visible=true
			Group="Behavior"
			InitialValue="<%if (label){%><%=label %><%}%>"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TooltipTitleFontColor"
			Visible=true
			Group="Global"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackgroundColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
