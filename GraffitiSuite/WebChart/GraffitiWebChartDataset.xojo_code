#tag Class
Protected Class GraffitiWebChartDataset
	#tag Method, Flags = &h0
		Sub Constructor(sLabel as String, cFill as Color, cStroke as Color, cHighlightFill as Color, cHighlightStroke as Color, cPoint as Color, cPointStroke as Color, cPointHighlight as Color, cPointHighlightStroke as Color, DataPoints() as Double)
		  Label = sLabel
		  FillColor = cFill
		  StrokeColor = cStroke
		  HighlightFill = cHighlightFill
		  HighlightStroke = cHighlightStroke
		  PointColor = cPoint
		  PointStrokeColor = cPointStroke
		  PointHighlightFill = cPointHighlight
		  PointHighlightStroke = cPointHighlightStroke
		  Data = DataPoints
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(sLabel as String, nValue as Integer, cFill as Color, cHighlight as Color)
		  Label = sLabel
		  DatasetValue = nValue
		  FillColor = cFill
		  HighlightFill = cHighlight
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Data() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		DatasetValue As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		FillColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		HighlightFill As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		HighlightStroke As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		Label As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PointColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		PointHighlightFill As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		PointHighlightStroke As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		PointStrokeColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		StrokeColor As Color
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="DatasetValue"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FillColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HighlightFill"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HighlightStroke"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Label"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointHighlightFill"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointHighlightStroke"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PointStrokeColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StrokeColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
