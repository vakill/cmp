#tag Class
Protected Class GraffitiWebTabBar
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  if not IsNull( Session ) then
		    Session.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_widgetCalendar').detach();" )
		  end if
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "tabClick"
		      if UBound( Parameters ) < 0 then Return True
		      dim tabIndex as Integer = Parameters(0).IntegerValue
		      mValue = tabIndex
		      UpdateTabStyles()
		      SelectionChanged()
		    case "tabClose"
		      if UBound( Parameters ) < 0 then Return True
		      dim tabIndex as Integer = Parameters(0).IntegerValue
		      if CloseClicked( tabIndex ) then
		        RemoveTab( tabIndex )
		      end if
		    case else
		      'break
		    end select
		  Catch
		    
		  End Try
		  
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  Select Case Name
		  case "enabled"
		    Buffer( "window.GSjQuery('#" + me.ControlID + "').prop('disabled'," + Lowercase( Str( not value.BooleanValue ) ) + ");" )
		    
		    Return True
		  case "style"
		    if not Value isa WebStyle then Return false
		    UpdateTabStyles()
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' style='overflow:visible;display:none;'>" )
		  source.Append( "<div class='tabs-x align-left tabs-above tab-bordered' style='width:100%;height:100%;'>" )
		  source.Append( "<ul class='nav nav-tabs'>" )
		  source.Append( "</ul>" )
		  source.Append( "</div>" )
		  source.Append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddTab(newTab as GraffitiWebTabBarItem)
		  if not IsNull( newTab ) then
		    
		    TabItems.Append( newTab )
		    
		    dim strExec() as String
		    strExec.Append( "var $newTab = window.GSjQuery(""" + TabToString( newTab ) + """);" )
		    strExec.Append( "var $theTabBar = window.GSjQuery('#" + me.ControlID + " > .tabs-x > ul');" )
		    strExec.Append( "$theTabBar.append($newTab);" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_tab" + Str( UBound( TabItems ), "#" ) + "').on('click', function (e) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'tabClick', [window.GSjQuery(e.target).data('index')]);" )
		    'strExec.Append( "e.stopImmediatePropogation();" )
		    strExec.Append( "e.preventDefault();" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_tab" + Str( UBound( TabItems ), "#" ) + " button').on('click', function (e) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'tabClose', [window.GSjQuery(e.target.parentNode).data('index')]);" )
		    'strExec.Append( "e.stopImmediatePropogation();" )
		    strExec.Append( "});" )
		    
		    Buffer( strExec )
		    
		    me.Style = me.Style
		    me.ActiveTabStyle = mActiveTabStyle
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ChildrenToString(theItem as GraffitiWebTabBarItem, itemID as Integer) As String
		  dim strOut() as string
		  
		  if UBound( theItem.Children ) < 0 then Return ""
		  
		  strOut.Append( "<ul class='dropdown-menu' role='menu' aria-labelledby='" + me.ControlID + "_tab" + Str( itemID, "#" ) + "'>" )
		  for intCycle as Integer = 0 to UBound( theItem.Children )
		    dim currItem as GraffitiWebTabBarItem = theItem.Children(intCycle)
		    strOut.Append( "<li><a href='#' tabindex='-1' role='tab' data-toggle='tab'>" + EscapeString( currItem.Caption ) + "</a></li>" )
		  next
		  strOut.Append( "</ul>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebTabBar -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "tabbar" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "bootstrap-tabs-x.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "bootstrap-tabs-x-bs4.min.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebTabBar -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function InitTabs() As String
		  dim strExec() as String
		  
		  dim intMax as Integer = UBound( TabItems )
		  for intCycle as Integer = 0 to intMax
		    dim currTab as GraffitiWebTabBarItem = TabItems(intCycle)
		    
		    if IsNull( currTab ) then Continue
		    
		    dim strSrc as String
		    'if UBound( currTab.Children ) >= 0 then
		    'strSrc = "<li class='dropdown'><a href='#' id='" + me.ControlID + "_tab" + Str( intCycle, "#" ) + "' role='tab'  data-index='" + Str( intCycle, "#" ) + "' data-toggle='dropdown' class='dropdown-toggle'>" + EscapeString( currTab.Caption )
		    'strSrc = strSrc + "<span class='caret'></span>"
		    'strSrc = strSrc + "</a>" + ChildrenToString( currTab, intCycle ) + "</li>"
		    'else
		    strSrc = "<li><a href='#' id='" + me.ControlID + "_tab" + Str( intCycle, "#" ) + "' role='tab'  data-index='" + Str( intCycle, "#" ) + "' data-toggle='tab'>" + EscapeString( currTab.Caption )
		    if currTab.CanClose then strSrc = strSrc + "<button class='close closeTab' type='button'>×</button>"
		    strSrc = strSrc + "</a></li>"
		    'end if
		    
		    strExec.Append( "var $newTab = window.GSjQuery(" + chr(34) + strSrc + chr(34) + ");" )
		    strExec.Append( "var $theTabBar = window.GSjQuery('#" + me.ControlID + " > .tabs-x > ul');" )
		    strExec.Append( "$theTabBar.append($newTab);" )
		    
		  next
		  
		  Return( Join( strExec, "" ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  dim intMax as Integer = TabItems.Ubound
		  for intCycle as Integer = intMax DownTo 0
		    RemoveTab( intCycle )
		    TabItems.Remove( intCycle )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveTab(tabIndex as Integer)
		  if tabIndex < 0 or tabIndex > TabItems.Ubound then Return
		  
		  if IsNull( TabItems(tabIndex) ) then return
		  
		  dim strExec() as String
		  strExec.Append( "var $tabLink = window.GSjQuery('#" + me.ControlID + "_tab" + Str( tabIndex, "#" ) + "');" )
		  strExec.Append( "if (typeof($tabLink) !== 'undefined') {" )
		  strExec.Append( "var $tabLI = $tabLink.closest('li');" )
		  strExec.Append( "if (typeof($tabLI) !== 'undefined') {" )
		  strExec.Append( "$tabLI.remove();" )
		  strExec.Append( "}}" )
		  
		  TabItems(tabIndex) = Nil
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TabCount() As Integer
		  Return TabItems.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TabToString(theTab as GraffitiWebTabBarItem) As String
		  dim strOut() as String
		  strOut.Append( "<li class='nav-item'><a class='nav-link' href='#' id='" + me.ControlID + "_tab" + Str( UBound( TabItems ), "#" ) + "' data-index='" + Str( UBound( TabItems ), "#" ) + "' data-toggle='tab'>" )
		  if theTab.Icon <> "" then strOut.Append( "<i class='" + theTab.Icon + "'></i>" )
		  strOut.Append( EscapeString( theTab.Caption ) )
		  if theTab.CanClose then strOut.Append( "<button class='close closeTab' type='button'>×</button>" )
		  strOut.Append( "</a></li>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    'strExec.Append( InitTabs )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .tabs-x').tabsX({" )
		    strExec.Append( "maxTitleLength: 10" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .nav-tabs [data-toggle=""tab""]').on('tabsX:click', function (event) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'tabClick', [window.GSjQuery(event.target).data('index')]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .nav-tabs [data-toggle=""tab""] button.close').on('click', function (event) {" )
		    strExec.Append( "var thisTab = window.GSjQuery(event.currentTarget.parentElement);" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'tabClose', [thisTab.data('index')]);" )
		    strExec.Append( "});" )
		    
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		    
		    RunBuffer()
		    
		    me.ActiveTabStyle = ActiveTabStyle
		    me.Style = me.Style
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateTabStyles()
		  dim strExec() as String
		  strExec.Append( "var myElement = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "myElement.find('a.nav-link')" )
		  strExec.Append( ".removeClass().addClass('nav-link')" )
		  if Style isa WebStyle then strExec.Append( ".addClass('" + Style.Name + "')" )
		  strExec.Append( ";" )
		  
		  strExec.Append( "myElement.find('a[data-index=\'" + Str( mValue ) + "\']')" )
		  if mActiveTabStyle isa WebStyle then
		    strExec.Append( ".addClass('" + mActiveTabStyle.Name + "')" )
		  else
		    strExec.Append( ".addClass('active')" )
		  end if
		  
		  strExec.Append( ";" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event CloseClicked(tabIndex as Integer) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionChanged()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/kartik-v/bootstrap-tabs-x
		
		Source Javascript: BSD-3 Clause License.  Modified heavily for Xojo
		implementation pursuant to licensing requirements.
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mActiveTabStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mActiveTabStyle = value
			  
			  UpdateTabStyles()
			End Set
		#tag EndSetter
		ActiveTabStyle As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mActiveTabStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTabAlignment As Orientations
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTabAlignment
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTabAlignment = value
			  
			  dim strExec() as String
			  strExec.Append( "window.GSjQuery('#" + me.ControlID + " > .tabs-x')" + _
			  ".removeClass('tabs-left')" + _
			  ".removeClass('tabs-right')" + _
			  ".removeClass('tabs-above')" + _
			  ".removeClass('tabs-below');" )
			  
			  select case value
			  case Orientations.Top
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + " div.tabs-x')" + _
			    ".addClass('tabs-above');" )
			    
			  case Orientations.Bottom
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + " div.tabs-x')" + _
			    ".addClass('tabs-below');" )
			    
			  case Orientations.Left
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + " div.tabs-x')" + _
			    ".addClass('tabs-left');" )
			    
			  case Orientations.Right
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + " div.tabs-x')" + _
			    ".addClass('tabs-right');" )
			    
			  end select
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Orientation As Orientations
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		TabItems() As GraffitiWebTabBarItem
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if value >= 0 and value <= UBound( TabItems ) then
			    mValue = value
			    
			    dim strExec() as String
			    strExec.Append( "var myElement = window.GSjQuery('#" + me.ControlID + "');" )
			    strExec.Append( "myElement.find('li').removeClass('active');" )
			    
			    strExec.Append( "var myTabLink = myElement.find('[data-index=\'" + Str( mValue ) + "\']');" )
			    strExec.Append( "var myTab = myTabLink.parent();" )
			    strExec.Append( "myTab.addClass('active');" )
			    
			    Buffer( strExec )
			    
			    UpdateTabStyles()
			  end if
			End Set
		#tag EndSetter
		Value As Integer
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.tabbar", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAFVBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHICqkQOAAAABnRSTlMAAhweUrK189/aAAAAL0lEQVQIW2NgwACiaWAQyKBmbGzslpaWxGAGFDVLS0tGZQABiQyQgUCQhLACAwAA05UQTlSdYbUAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAFVBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHICqkQOAAAABnRSTlMAAhweUrK189/aAAAAL0lEQVQIW2NgwACiaWAQyKBmbGzslpaWxGAGFDVLS0tGZQABiQyQgUCQhLACAwAA05UQTlSdYbUAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Orientations, Type = Integer, Flags = &h0
		Top
		  Bottom
		  Left
		Right
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="33"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Orientation"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Orientations"
			EditorType="Enum"
			#tag EnumValues
				"0 - Top"
				"1 - Bottom"
				"2 - Left"
				"3 - Right"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
