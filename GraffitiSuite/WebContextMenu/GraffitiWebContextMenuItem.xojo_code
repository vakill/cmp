#tag Class
Protected Class GraffitiWebContextMenuItem
	#tag Method, Flags = &h0
		Sub Constructor(Separator as Boolean)
		  IsSeparator = Separator
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(MenuID as String)
		  me.ID = MenuID
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ItemID as String, ItemCaption as String, ItemIcon as String = "", ItemDisabled as Boolean = False, ItemAccessKey as String = "", ItemStyle as WebStyle = Nil)
		  me.ID = ItemID
		  me.Caption = ItemCaption
		  me.Icon = ItemIcon
		  me.Disabled = ItemDisabled
		  me.AccessKey = ItemAccessKey
		  me.Style = ItemStyle
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ItemID as String, ItemCaption as String, ItemStyle as WebStyle)
		  me.ID = ItemID
		  me.Caption = ItemCaption
		  me.Style = ItemStyle
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		AccessKey As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Caption As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Children() As GraffitiWebContextMenuItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Disabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Icon As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		IsSeparator As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Style As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AccessKey"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Disabled"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsSeparator"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
