#tag Class
Protected Class GraffitiWebContextMenu
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "menuShow"
		      if Parameters.Ubound >= 0 then MenuShown( Parameters(0) )
		    case "menuHide"
		      if Parameters.Ubound >= 0 then MenuHidden( Parameters(0) )
		    case "menuAction"
		      if Parameters.Ubound >= 1 then MenuAction( Parameters(0), Parameters(1) )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddMenu(MenuID as String, theMenu as GraffitiWebContextMenuItem)
		  AddMenu( Nil, MenuID, TriggerNone, theMenu )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddMenu(theControl as WebControl, MenuID as String, Trigger as Integer, theMenu as GraffitiWebContextMenuItem)
		  dim strExec() as String
		  dim strSelector as String
		  
		  if IsNull( theControl ) and Trigger = TriggerNone then
		    dim pageID as String
		    'if not IsNull( me.Page ) then
		    'pageID = me.Page.ControlID
		    'else
		    pageID = "body"
		    'end if
		    strExec.Append( "var newDiv = window.GSjQuery('<div id=\'gwContext_" + me.ControlID + "_" + EscapeString( MenuID ) + "\'></div>').appendTo('" + EscapeString( pageID ) + "');" )
		    strSelector = "#gwContext_" + me.ControlID + "_" + EscapeString( MenuID )
		  ElseIf not IsNull( theControl ) then
		    strSelector = "#" + theControl.ControlID
		    if theControl isa WebButton then strSelector = strSelector + "_inner"
		  else
		    Return
		  end if
		  
		  strExec.Append( "window.GSjQuery.contextMenu({" )
		  
		  strExec.Append( "selector: '" + strSelector + "'," )
		  strExec.Append( "appendTo: 'body'," )
		  if not IsNull( theMenu.Style ) then strExec.Append( "className: '" + theMenu.Style.Name + "'," )
		  
		  select case Trigger
		  case TriggerNone
		    strExec.Append( "trigger: 'none'," )
		  case TriggerRight
		    strExec.Append( "trigger: 'right'," )
		  case TriggerLeft
		    strExec.Append( "trigger: 'left'," )
		  case TriggerHover
		    strExec.Append( "trigger: 'hover'," )
		  end select
		  
		  strExec.Append( "zIndex: 11000," )
		  
		  strExec.Append( "items: " + ItemsToString( theMenu.Children ) + "," )
		  
		  strExec.Append( "reposition: false," )
		  strExec.Append( "delay: 250," )
		  strExec.Append( "autoHide: false," )
		  strExec.Append( "events: {" )
		  strExec.Append( "show : function(options) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'menuShown', ['" + EscapeString( MenuID ) + "']);" )
		  strExec.Append( "}," )
		  strExec.Append( "hide : function(options) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'menuHidden', ['" + EscapeString( MenuID ) + "']);" )
		  strExec.Append( "}}," )
		  strExec.Append( "callback: function(itemKey, opt) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'menuAction', ['" + EscapeString( MenuID ) + "',itemKey]);" )
		  strExec.Append( "}" )
		  strExec.Append( "});" )
		  
		  if Trigger = TriggerRight then
		    strExec.Append( "window.GSjQuery('" + strSelector + "').unbind('contextmenu');" )
		    strExec.Append( "window.GSjQuery('body').unbind('contextmenu');" )
		  end if
		  
		  Menus.Append( theMenu )
		  
		  Buffer( strExec )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  LibrariesLoaded = True
		  hasLoaded = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HideMenu(MenuID as String)
		  dim strExec() as String
		  
		  dim strSelector as String = "#gwContext_" + me.ControlID + "_" + EscapeString( MenuID )
		  
		  strExec.Append( "window.GSjQuery('#" + strSelector + "').contextMenu('hide');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HideMenu(theControl as WebControl)
		  dim strExec() as String
		  
		  dim strSelector as String = "#" + theControl.ControlID
		  if theControl isa WebButton then
		    strSelector = strSelector + "_inner"
		  end if
		  
		  strExec.Append( "window.GSjQuery('#" + strSelector + "').contextMenu('hide');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebContextMenu -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    'strOut.Append( "<style>.context-menu-item.fa,.context-menu-item.fas{font-weight:normal}</style>" )
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddjQueryUI( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "contextmenu" ).Child( "jquery.contextMenu.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "jQuery" ).Child( "jquery.ui.position.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.child( "contextmenu" ).Child( "jquery.contextMenu.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebContextMenu -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemsToString(Items() as GraffitiWebContextMenuItem) As String
		  dim strExec() as String
		  
		  strExec.Append( "{" )
		  
		  dim intMax as Integer = Items.Ubound
		  for intCycle as Integer = 0 to intMax
		    dim currItem as GraffitiWebContextMenuItem = Items(intCycle)
		    if currItem.IsSeparator then
		      strExec.Append( "'" + GenerateUUID() + "': '---------'" )
		    else
		      strExec.Append( "'" + EscapeString( currItem.ID ) + "': {" )
		      strExec.Append( "name: '" + EscapeString( currItem.Caption ) + "', " )
		      if not IsNull( currItem.Style ) then strExec.Append( "className: '" + currItem.Style.Name + "'," )
		      if currItem.AccessKey <> "" then strExec.Append( "accesskey: '" + EscapeString( currItem.AccessKey ) + "'," )
		      if currItem.Icon <> "" then strExec.Append( "icon: '<i class=\'" + currItem.Icon + "\'></i>', " )
		      if currItem.Disabled then strExec.Append( "disabled: " + Str( currItem.Disabled ).Lowercase + "," )
		      if currItem.Children.Ubound >= 0 then
		        strExec.Append( "items: " + ItemsToString( currItem.Children ) )
		      end if
		      strExec.Append( "}" )
		    end if
		    if intCycle < intMax then strExec.Append( "," )
		  next
		  
		  strExec.Append( "}" )
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowMenu(MenuID as String, X as Integer = -1, Y as Integer = -1)
		  dim strExec() as String
		  
		  dim strSelector as String = "#gwContext_" + me.ControlID + "_" + EscapeString( MenuID )
		  
		  dim strPos as String
		  if X >= 0 and Y >= 0 then strPos = "{x: " + Str( X ) + ", y: " + Str( Y ) + "}"
		  
		  strExec.Append( "window.GSjQuery('" + strSelector + "').contextMenu(" + strPos + ");" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowMenu(theControl as WebControl, X as Integer = -1, Y as Integer = -1)
		  dim strExec() as String
		  
		  dim strSelector as String = "#" + theControl.ControlID
		  if theControl isa WebButton then
		    strSelector = strSelector + "_inner"
		  end if
		  
		  dim strPos as String
		  if X >= 0 and Y >= 0 then strPos = "{x: " + Str( X ) + ", y: " + Str( Y ) + "}"
		  
		  strExec.Append( "window.GSjQuery('" + strSelector + "').contextMenu(" + strPos + ");" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Unbind(theControl as WebControl)
		  if IsNull( theControl ) then Return
		  
		  dim strExec() as String
		  
		  strExec.Append( "window.GSjQuery.contextMenu('destroy', '#" + theControl.ControlID + "');" )
		  
		  Buffer( strExec )
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event MenuAction(MenuID as String, ItemID as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MenuHidden(MenuID as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MenuShown(MenuID as String)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		https://swisnl.github.io/jQuery-contextMenu/
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  RunBuffer()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Menus() As GraffitiWebContextMenuItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.contextmenu", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAaeSURBVHhe7d1fbFN1FAfwc9oO1oKIENGEmJiIYWCMojMBH5QHNQTD/umiJj7oC77In25iSIxRYyLqWItME+WFFxNJIFs3MUFJFF/YCENDkD9VojHGBMefGGQtdNs9/i47+rKu2/11vYH+vp+E3XPOxna55/S27N7bSwAAAAAAAAAAAAAAAAAAAAAAAFAtWJdlm9Wy8V4Ny1bo3vmLhtbmPZtccNUbW6hp1ZmJbeQrawASzckHRKiThOqZ+VYtl0+kYNZs0HzPt4d7Uge1Oi3xxvZtTF4rMd+jpapktvtlYjnKTO25nvRxLQdmPQDxpuSrTNylacUIyfZ8Jr1F00nFn04uphreb/5BD2rJGWYbbTDb6GNNA4noMhB/d282dFrTijJD9lqiafNTmk6KY5x2sfk+vxe2T8FWAxAdi73hb3JNQxB5R4Oiahs2PWa2QqumDuLYeE+CsxoAs7HrNQqJlPx5HOHlGrrLsidWA2B2OfdpGBKOzW1pq9OkiMjdGjjLtid2ewCoGhgAx2EAHIcBcBwGwHEYAMdhAByHAXAcBsBxGADHWQ2AkPyjYWhGx2hYwwmYKK+hs2x7YjUALPyDhqEQkXNXe1N/aDqBR3xaQ2eZB8GPGgZiuwc4pGFI+BsNiop59JOGzhKh7zQMJKrLQEazA4didStXM3Hlj8KJ/J6Q2Lp89nBBKxMUsv3nY3Wr/jaPgjVacor/gMz3pl/WNBDrF4FcE32BxNujaUWYXf+37EUbLvZ1TPn8ls+kPjJfn9TUHUJ7rvfCknnQlCfe0NZAEak3K7JCS+VjOkXMg/me1F6tTNvcpk3LxiT6hPkeK8wE3a7lKmRehwkdy/el+rQAAAAAAAAAMJWyfg+wYM2GeVdnx943oX9RwsPXizNAmE+a/+MOep68d+3L9FktT0uiqX29kDT6v5dgkju0XI2OmT+DtddGt1460HV5vBSc9QDEm9pbWGSn+Q6LtTTzhEYkQsl8T+oTrUyutXVWonDXHrM+zVpxg9Cf5gGzMZ/p7NZKIFYDMKe5fZHneSeYeZGWKko8Xpnv6zyiaVHxxrbPmGm9pk4RkaFIJHL/cE/nkJamzepYgGn+trCa72P2tmtYVG1j26OuNt/n98LviaaB2B0MYnpIo3Awl/x5EZJlGrrLP/Zhwe6EEOKwr8NPlLo41DwHztjb09ysTE/CGwCoHhgAx2EAHIcBcBwGwHEYAMdhAByHAXAcBsBxGADH2Q2A/2bOIStQdEzDIqTE5xxh2RO7AWAa1CgUInSx0N0x6dujRzw+paG7LHtiNQBCdFjDUDBTyYtRzfo4PwC2PbG7OPTMwMFY3apnmKjyp1wJXfGIGkaz/ZOe9jSS7T8XW7oqYgblcS05xTT/RD6TbtE0EOsXgUJjr5iPJc/SKZ/8Juy9WOq9Af6T7029ZRY7xzOXyJHxXtgp++LQRFNyCwnXm+80cyeJCJ00H4/lro18SAe6ro0XpyfRmFxrFmvN+qww/7zQzloKn/gnhR7LZdId4zkAAAAAAADAlMr+PUC8se1dJlkuTCVu6hSQ0K9m1U5Ha0Y6ruzrOq/VaaltbHuOmZ5kkTqzTrdpueqw0BkhPpXvTb2pJSvWA+DfzNGsQJqpcrdsE5ILEaLkcCb9uZZKijcld5v1eUlTJ5htdMo8AJO5zI6Sb6Y5GbsBWL++JjE01z8As2S8UEFCY6NMywuZ1M9aKco0/wPT/Nc1dc3Z3KIry2nXrhHNp83qWEBiaE6nWVS++T6maHSK29TGGzbXO9x83xLtSWB2B4OEH9EoLCt1WZR5zsfFoZY9sRsAyytRbZnnqQWz1yUn3eOY1yJLNXSXZU9sDwfP1mVoampK3Kyaw7yR9Q3Lqie2AwBVAgPgOAyA4zAAjsMAOA4D4DgMgOMwAI7DADgOA+A4qwEQoVDvHGp+4vCV7tQZTSZg5qyGzrLtie0ewL8qJTzCJa98FfKcv3WsYdUTqwGoqZGtQvSXppXH3K5RUfme9ICZgk81dY7fC78nmgZiNQCX96UvmV3OJv9tyrVUMf7dQHOZzimnOzf/lqT52v2aOsPvgd8LvydaCsTq8nDfaLb/ZHzZ6t0ejS00Ixhlpjv1U2UTkqz58HWUvedzvTum19Tj34+OZge+iC1bdcFks8zfn2/WKT7+yepjmn6cmL6K8ezmXG/HUS0HVvZZwf9rbbUepgn27p2Zt3yZyXW60czUNgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgJsL0b+j09zsU7dhdQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADvSURBVDhPYxhwwAilGbj9C4v/MzJIg9j//vydwMTCpAGU9gBLogHG/wxPv27s7wWzwSJAwBVQ9B/KBIL//Qz/GS2AspZQAQzwbUMfWC8TmIcG/v9n/Pyf8f8nKBcvYIHSDP///TP9z8SgBGL/2DhhFYdPntx/FiYLsCQaYPzHcA/KRHiBM6DoI5DDB2Iz/v9f8o+RwZWRgdEdLIkGgH799H1DHz+IDfcCTDMIgAKT8T8jnI8OkNUiXOBf1MjI+J8Xwvu/AyglD2RoQvioABRG3zf21YPYWAORFAB3AcXRCAoYKBOcUPBFI7LaUcDAAAA/AkklhjA5ZgAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowStyleProperty, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = TriggerHover, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TriggerLeft, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TriggerNone, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = TriggerRight, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
