#tag Class
Protected Class GraffitiWebTooltip
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  if not IsNull( Session ) then UnbindAll()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  if not IsLibraryRegistered( Session, JavascriptNamespace, "gwTooltipCSS" ) Then
		    RegisterLibrary( Session, JavascriptNamespace, "gwTooltipCSS" )
		    source.Append( "<style>" + cssDisplay + "</style>" )
		  end if
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Bind(BindTo as WebControl, Title as String, HTMLContent as String, TipStyle as Integer, ShowOn as Integer = 0, IsRounded as Boolean = False, HasShadow as Boolean = False, Position as Integer = 0, PositionX as Integer = 0, PositionY as Integer = 0, ShowDelay as Integer = 1000)
		  dim strBind() as String
		  
		  dim strTheName as String = BindTo.ControlID
		  if BindTo isa WebTextField or BindTo isa WebButton or BindTo isa WebTextArea then
		    strTheName = strTheName + "_inner"
		  end if
		  
		  strBind.Append( "var myElement = window.GSjQuery('#" + strTheName + "');" )
		  strBind.Append( "if( typeof(myElement.qtip) == 'function' ) {" )
		  strBind.Append( "var qTipFor_" + strTheName + " = myElement.qtip({" )
		  strBind.Append( "id: '" + strTheName + "'," )
		  strBind.Append( "prerender: true," )
		  strBind.Append( "content: {" )
		  
		  dim strMy, strAt as String 
		  Select case Position
		  case PositionBottomCenter
		    strMy = "top center"
		    strAt = "bottom center"
		  case PositionTopCenter
		    strMy = "bottom center"
		    strAt = "top center"
		  case PositionTopLeft
		    strMy = "bottom right"
		    strAt = "top left"
		  case PositionTopRight
		    strMy = "bottom left"
		    strAt = "top right"
		  case PositionCenterLeft
		    strMy = "right center"
		    strAt = "left center"
		  case PositionCenterRight
		    strMy = "left center"
		    strAt = "right center"
		  case PositionBottomLeft
		    strMy = "top right"
		    strAt = "bottom left"
		  case PositionBottomRight
		    strMy = "top left"
		    strAt = "bottom right"
		  end select
		  
		  strBind.Append( "text: '" + EscapeString( HTMLContent ) + "'," )
		  
		  dim strShow() as String = Split( ShowOnToString( ShowOn ), "," )
		  if ShowOn = ShowOnClick then
		    strBind.Append(  "button: true," )
		  else
		    strBind.Append(  "button: false," )
		  end if
		  strBind.Append(  "title: '" + EscapeString( Title ) + "'" )
		  strBind.Append( "}," )
		  strBind.Append( "position: {" )
		  if Position = -1 Then
		    strBind.Append( "target: [" + Str( PositionX, "#" ) + "," + Str( PositionY, "#" ) + "]" )
		  else
		    strBind.Append( "my: '" + strMy + "'," )
		    strBind.Append( "at: '" + strAt + "'," )
		    strBind.Append( "target: myElement" )
		  end if
		  strBind.Append( "}," )
		  strBind.Append( "show: {" )
		  strBind.Append( "delay: " + Str( ShowDelay ) + "," )
		  strBind.Append( "event: " + strShow(0) + "" )
		  strBind.Append( "}," )
		  strBind.Append( "hide: " + strShow(1) + "," )
		  strBind.Append( "style: '" + me.StyleToString( TipStyle, IsRounded, HasShadow ) + "'," )
		  
		  strBind.Append( "events: {" ) '// Start Events
		  strBind.Append( "show: function(event,api) {" ) '// Start Show
		  strBind.Append( "var maxZ = Math.max.apply(null,window.GSjQuery.map(window.GSjQuery('*'), function(e,n){" ) '// Begin maxZ function
		  strBind.Append( "if(window.GSjQuery(e).css('position')=='absolute') return parseInt(window.GSjQuery(e).css('z-index'))||1 ;" )
		  strBind.Append( "}));" ) '// End maxZ function
		  strBind.Append( "var tooltip = api.elements.tooltip;" ) '// Get the tooltip DIV evlement
		  strBind.Append( "tooltip.css('z-index', maxZ + 100);" ) '// Apply new Z-Index
		  strBind.Append( "if( window.GSjQuery('#" + me.ControlID + "_inner').parents('.palette').length > 0 ) {" ) '// Begin IF
		  strBind.Append( "tooltip.css('z-index', maxZ + 99);" )
		  strBind.Append( "window.GSjQuery('#" + me.ControlID + "_inner').parents('.palette').css('z-index', maxZ - 1);" )
		  strBind.Append( "}" ) '// End IF
		  strBind.Append( "}" ) '// End Show
		  strBind.Append( "}" ) '// End Events
		  
		  strBind.Append( "}).qtip('api');" )
		  strBind.Append( "}" )
		  
		  BoundControls.Append( BindTo )
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strBind, "" ) )
		  Else
		    BindingCache.Append( Join( strBind, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BindWithContainer(BindTo as WebControl, Title as String, ContainerContent as WebContainer, TipStyle as Integer, ShowOn as Integer = 0, IsRounded as Boolean = False, HasShadow as Boolean = False, Position as Integer = 0, ShowDelay as Integer = 1000)
		  dim strBind() as String
		  
		  dim strTheName as String = BindTo.ControlID
		  if BindTo isa WebTextField or BindTo isa WebButton or BindTo isa WebTextArea then
		    strTheName = strTheName + "_inner"
		  end if
		  
		  strBind.Append( "" )
		  strBind.Append( "var myElement = window.GSjQuery('#" + strTheName + "');" )
		  strBind.Append( "myElement.attr('gwTTContainer', '" + ContainerContent.ControlID + "');" )
		  strBind.Append( "if( typeof(myElement.qtip) == 'function' ) {" )
		  strBind.Append( "myElement.qtip({" )
		  strBind.Append( "id: '" + strTheName + "'," )
		  strBind.Append( "prerender: true," )
		  strBind.Append( "content: {" )
		  
		  dim strMy, strAt as String
		  Select case Position
		  case PositionBottomCenter
		    strMy = "top center"
		    strAt = "bottom center"
		  case PositionTopCenter
		    strMy = "bottom center"
		    strAt = "top center"
		  case PositionTopLeft
		    strMy = "bottom right"
		    strAt = "top left"
		  case PositionTopRight
		    strMy = "bottom left"
		    strAt = "top right"
		  case PositionCenterLeft
		    strMy = "right center"
		    strAt = "left center"
		  case PositionCenterRight
		    strMy = "left center"
		    strAt = "right center"
		  case PositionBottomLeft
		    strMy = "top right"
		    strAt = "bottom left"
		  case PositionBottomRight
		    strMy = "top left"
		    strAt = "bottom right"
		  end select
		  
		  strBind.Append( "text: window.GSjQuery('#" + ContainerContent.ControlID + "')," )
		  
		  dim strShow() as String = Split( ShowOnToString( ShowOn ), "," )
		  if ShowOn = ShowOnClick then
		    strBind.Append(  "button: true," )
		  else
		    strBind.Append(  "button: false," )
		  end if
		  strBind.Append(  "title: '" + EscapeString( Title ) + "'" )
		  strBind.Append( "}," )
		  strBind.Append( "position: {" )
		  strBind.Append( "my: '" + strMy + "'," )
		  strBind.Append( "at: '" + strAt + "'," )
		  strBind.Append( "target: myElement" )
		  strBind.Append( "}," )
		  strBind.Append( "show: {" )
		  strBind.Append( "delay: " + Str( ShowDelay ) + "," )
		  strBind.Append( "event: " + strShow(0) + "" )
		  strBind.Append( "}," )
		  strBind.Append( "hide: " + strShow(1) + "," )
		  strbind.Append( "events: {" )
		  strBind.Append( "show: function(event, api) {" )
		  strBind.Append( "var thisContainer = window.GSjQuery('#" + ContainerContent.ControlID + "');" )
		  strBind.Append( "thisContainer.css('position','initial');" )
		  strBind.Append( "thisContainer.css('left','0');" )
		  strBind.Append( "thisContainer.css('top','0');" )
		  strBind.Append( "var thisParent = thisContainer.parent();" )
		  strBind.Append( "thisParent.css('overflow','visible');" )
		  
		  strBind.Append( "var maxZ = Math.max.apply(null,window.GSjQuery.map(window.GSjQuery('*'), function(e,n){" ) '// Begin maxZ function
		  strBind.Append( "if(window.GSjQuery(e).css('position')=='absolute') return parseInt(window.GSjQuery(e).css('z-index'))||1 ;" )
		  strBind.Append( "}));" ) '// End maxZ function
		  strBind.Append( "var tooltip = api.elements.tooltip;" ) '// Get the tooltip DIV evlement
		  strBind.Append( "tooltip.css('z-index', maxZ + 100);" ) '// Apply new Z-Index
		  strBind.Append( "if( window.GSjQuery('#" + me.ControlID + "_inner').parents('.palette').length > 0 ) {" ) '// Begin IF
		  strBind.Append( "tooltip.css('z-index', maxZ + 99);" )
		  strBind.Append( "window.GSjQuery('#" + me.ControlID + "_inner').parents('.palette').css('z-index', maxZ - 1);" )
		  strBind.Append( "}" ) '// End IF
		  
		  strBind.Append( "}" )
		  strBind.Append( "}," )
		  
		  strBind.Append( "style: {" )
		  strBind.Append( "classes: '" + me.StyleToString( TipStyle, IsRounded, HasShadow ) + "'," )
		  strBind.Append( "width: '" + Str( ContainerContent.Width + 5, "#" ) + "px'," )
		  strBind.Append( "height: '" + Str( ContainerContent.Height + 22, "#" ) + "px'" )
		  strBind.Append( "}," )
		  
		  strBind.Append( "});" )
		  strBind.Append( "}" )
		  
		  'BindTo.HelpTag = "container"
		  
		  BoundControls.Append( BindTo )
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strBind, "" ) )
		  Else
		    BindingCache.Append( Join( strBind, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColorToString(c as Color) As String
		  Return "rgba(" + _
		  Str( c.Red, "###" ) + "," + _
		  Str( c.Green, "###" ) + "," + _
		  Str( c.Blue, "###" ) + "," + _
		  Str( 1 - (c.Alpha / 255) ) + ")"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  hasLoaded = True
		  LibrariesLoaded = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Hide(theControl as WebControl)
		  if not IsNull( theControl ) Then
		    
		    dim strTheName as String = theControl.ControlID
		    if theControl isa WebTextField or theControl isa WebButton or theControl isa WebTextArea then
		      strTheName = strTheName + "_inner"
		    end if
		    
		    me.ExecuteJavaScript( "var item = window.GSjQuery('#" + strTheName + "');" )
		    me.ExecuteJavaScript( "var theAPI = item.qtip('api');" )
		    me.ExecuteJavaScript( "window.GSjQuery('#" + strTheName + "').qtip('toggle',false);" )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebTooltip -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddjQueryUI( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "tooltip" ).Child( "jquery.qtip.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tooltip" ).Child( "jquery.qtip.min.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebTooltip -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Show(theControl as WebControl)
		  if not IsNull( theControl ) Then
		    
		    dim strTheName as String = theControl.ControlID
		    if theControl isa WebTextField or theControl isa WebButton or theControl isa WebTextArea then
		      strTheName = strTheName + "_inner"
		    end if
		    
		    me.ExecuteJavaScript( "var item = window.GSjQuery('#" + strTheName + "');" )
		    me.ExecuteJavaScript( "var theAPI = item.qtip('api');" )
		    me.ExecuteJavaScript( "window.GSjQuery('#" + strTheName + "').qtip('toggle',true);" )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ShowOnToString(ShowOn as Integer) As String
		  Select case ShowOn
		  case ShowOnClick
		    Return "'click','false'"
		  case ShowOnFocus
		    Return "'focus','blur'"
		  case ShowOnMethod
		    Return "false,false"
		  case else
		    Return "'mouseenter','mouseleave'"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function StyleToString(TheStyle as Integer, IsRounded as Boolean = false, HasShadow as Boolean = false) As String
		  dim strRet() as String
		  
		  Select case TheStyle
		  case StyleBlue
		    strRet.Append( "qtip-blue" )
		  case StyleBootStrap
		    strRet.Append( "qtip-bootstrap" )
		  case StyleDark
		    strRet.Append( "qtip-dark" )
		  case StyleGreen
		    strRet.Append( "qtip-green" )
		  case StyleJTools
		    strRet.Append( "qtip-jtools" )
		  case StyleLight
		    strRet.Append( "qtip-light" )
		  case StyleRed
		    strRet.Append( "qtip-red" )
		  case StyleTipped
		    strRet.Append( "qtip-tipped" )
		  case StyleTipsy
		    strRet.Append( "qtip-tipsy" )
		  case StyleYouTube
		    strRet.Append( "qtip-youtube" )
		  case else
		    strRet.Append( "qtip-plain" )
		  End Select
		  
		  if HasShadow then strRet.Append( "qtip-shadow" )
		  if IsRounded then strRet.Append( "qtip-rounded" )
		  
		  
		  Return Join( strRet, " " )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Unbind(theControl as WebControl)
		  if LibrariesLoaded then
		    
		    dim strTheName as String = theControl.ControlID
		    if theControl isa WebTextField or theControl isa WebButton or theControl isa WebTextArea then
		      strTheName = strTheName + "_inner"
		    end if
		    
		    dim strUnbind() as String
		    
		    dim intCycle as Integer
		    dim intMax as Integer = UBound( BoundControls )
		    for intCycle = intMax downto 0
		      if theControl = BoundControls(intCycle) then
		        BoundControls.Remove( intCycle )
		        
		        strUnbind.Append( "" )
		        strUnbind.Append( "var theObject = window.GSjQuery('#" + strTheName + "');" )
		        strUnbind.Append( "if( theObject.data('qtip') ) {" )
		        strUnbind.Append( "if( theObject.attr('gwTTContainer') != 'undefined' ) {" )
		        strUnbind.Append( "var theContainer = window.GSjQuery('#' + theObject.attr('gwTTContainer')).detach();" )
		        strUnbind.Append( "theContainer.css('position','absolute').css('left','-20000px');" )
		        if not IsNull( theControl.Parent ) then strUnbind.Append( "var theParent = window.GSjQuery('#" + theControl.Parent.ControlID + "').append(theContainer);" )
		        strUnbind.Append( "}" )
		        strUnbind.Append( "theObject.qtip('destroy',true);" )
		        strUnbind.Append( "theObject.removeData('hasqtip');" )
		        strUnbind.Append( "theObject.removeAttr('data-hasqtip');" )
		        strUnbind.Append( "}" )
		        
		        exit
		      end if
		    next
		    
		    me.ExecuteJavaScript( Join( strUnbind, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UnbindAll()
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( BoundControls )
		  
		  for intCycle = intMax DownTo 0
		    Unbind( BoundControls(intCycle) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UnbindMultiple(ParamArray theControls as WebControl)
		  for each theControl as WebControl in theControls
		    Unbind( theControl )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded Then
		    me.ExecuteJavaScript( Join( BindingCache, "" ) )
		    
		    redim BindingCache(-1)
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://qtip2.com/
		
		Licensed under MIT.
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private BindingCache() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private BoundControls() As WebControl
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			  
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean = False
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = cssDisplay, Type = String, Dynamic = False, Default = \".qtip {\nmin-width: 90px !important;\nmin-height: 40px !important;\nmax-width: 900px !important;\nmax-height: 900px !important;\n}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.tooltip", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAkcSURBVHhe7d19bFtXFQDwc57tJHbSlU1sLdOkoTGtyR8rAzYtqQRiBcbGoHa6deJLUG0IMaGtsaMlqiY+ikAM1sTpOlYYYzC1IJaWxk5bBgxaoEEJYx9MHVNalVWijK+NfqWx08Z+h/PIQVSorPV79304Pj8pefccJ3Hse9699zl5z6CUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKKaWUUqreoGznlWR332VUrV4BSG9CwkvBsi/lh8ofsBCIkojQQgRJzrVwzuYnYYqQphDwJHEbgaaI20DwCn/tIYzZh6ZPLTwEO9eV/n0H80jdF0Dzyp4r0Lau5wfyVgC6hh/SNdxeJDcbRUR/4599iIvkAG/3xsgemxrdsF9urkt1VwAL02vecBrjy5Hs5byXLuc9tENuCgkd5pFijItizOKimC7m98kNdaEuCqD1Q/cssmNWN/+6GR6m3y/pSCKg5/lJ3VqF2PCpwvo/SjqyIl0AqUzvp3nYvZX38hslVVd4nbEH0B5OxHH4xLb8EUlHSuQKoO22tRfblZm7+Mm7CxEXS7r+ETxUQdh4ujB4QDKREJkCaE6veXMMrD5A/AyHdTE1ucGF/TiQ/VB5dOgZSYUq9Ce6tbv3Epuoj3+RXkk1ihHbsgdmtg/9RuJQxGQbimR3bh0PjT/izn+npBpJBxLeEW/vTFUmJ34uucCFMgKk0tkPEOL9fOdXS6qh8UJ3Egj7y6ODo5IKTKAFcOGq/oUzs6fX86HcpySlzsCHkI8mZyq9R36y8YSkfBdYAbRk1nQhWY/xyr5dUuosuAheArRWl0cGficpXwVSADzk38mr+0clVOdCUEWk1dOF/BbJ+Mb3RWAynf0q7/Vfl1CdDwSLP62ML+mKV/aP75GsL3wtgFQmt547v09CVSNEeBcXwZVcBCOSMs63Akhlsg/yQ+iRULnERbA0saRr8ez+8V2SMsqXAkhmsg/wSj8rofIK4dp4e2dbZXLiKckYY7wAkityn+Vh/8sSKkN4h1qWaO+C2cnxX0nKCKMF4LzAgxZ+X0Jl3rsT7Z1/mZ2ceE5iz4wdBs69pm//gSv1jZJS/ihVwVpq6n8N+HDDDO585xU+7Xz/pSyqDknbMyNTQEsm9xEL8EsSKp/xGuuqeMeyE5XJ8QlJueZ9Crjp7uZUS/xl/lHOf92qACFWrp4eefBFCV3xPAWkmhN92vnhsO34PdJ0zdMIcMFt2Ysqs/AnLsVWSamA8YLwSi8LQk8jwGwF+rXzwxWD6hppuuJtCiD9u3748O7kintdT8GuCyDZnVuFCBdJqMKEldulVTPXBYA2fViaKnzvkW3N3BXAJ7/QwnP/SolUyBAw2AJoOXb8OmmqKEBItqZzrorAVQEgxa6VpooI2+U04G4KsEgLIGoQLpdWTdyNAEBvkaaKjktkWxN3IwDp4V/kEAVXAIRwoTRVdARXAKgjQORgkAVAgEelqSKCR2VXF6BwtwZAek1aKjr+IduauJ0CXpWmigz8uzRq4nIEQC2AiOFD8+BGALDp99JSkYF/lUZN3E0BMRyXpoqOX8i2Jq4KYDo+owUQMaU4BVcAsPXhk3zg8axEKmQE3Pnb8mUJa+KuAByEw9JSIUOXw7/DdQFYVXhMmipkloWurx/gugBO7hx8jYee70moQuJcePLk9sFJCWvmfgpgRKSjQMiQYIM0XfFUADPFob0Eejp4aIiGS6ODz0vkiqcCcNhV+JxzVSsJVYBsQE97v8Pz2cHVA+PHEh1dp7n53rmMCgJPv1+ZKeYfl9A1YxeISKazzyLi2yVUfiIYLxUHl0nkiecp4D/Qgjt4c2ouUn4iJGNXXzNWAKWR/AtI5BSB8tfaciH/tLQ9M3qRqNn9E/vi7Z1NCNiIl3/3H9GmUjG/ViIjjF8mrjI5sTve3nU5Ly7eJillAAFsLhfzxs/GNl4AjsrkeFGLwKiRcmHQl5NxfSkAhxaBGXy49xTv+R+U0DjfCsDhFEFiSeciQNSTSV1wXmrnzl8loS98LQAHLwx3xTs6j/LC8GZJqfNB0M+d3y+Rb4y9EHQuqUzPjXx33+UPvaLY6+DF3jEAe3W5MFSUlK+MvQ5wLqXC0M/sRFMXN3fMZdT/IqCfxizoCqrzHYGNAGeSt5D5Bjeb5zINjuAkd74z5D8smcAENgKcqVTMf8eqwGX8yJ+QVMPiIf8HZMU7wuh8RygjwJlSmRw/B42HCJ7jVf4DM6P5H0oqFKEWQDLTk0awChI2BB7qf8kD78ZyYWC7pEIVyhTwX9at0pj/iEbRtt9XLuRviErnO0IeAbJHEHD+XmyC4GXe44eR0PO/bvkltAJIrei5BSxrp4TzBne486LXMCIOT48M7JZ0ZIVWAMlMbiff+S0S1jeCCV7QjUEM95ZHgn8DaC9CKYDkit7r0SLP73YRBu7oV3kPf4n39DG0YKzUau2FLQPTcnPdCacA0tktPER+TELfOH9JA8Q/84O8gA+7FiBSGwG2IQFvaQF3ZBs/AxxSmTcl/pYyt0tOm49Ny7xnH+bvPWgjHOTf92DT6dLB47s2zavL4wReAKlM783cNT+W0Dfcket5xX2vhOr/CPwwkMC+X5p+4b7HT2jnn59AC4CH/i/y8LpUQuN4mH8BbHhHqTiwWVLqHAKbAlLd2Zt4z3xSQuO487eUmxbcCVvXOSepqPMUSAG0Zfo6qjS7mxdSiyVlDHf8P/nzfeVi/luSUjXwfQpw3lKWO995YcR453PvfzORoKu0893zfQRIZnK7+U5ukNAMonE+drtvejS/RzLKJV8LIJXOPcH34PoNjc6ixMf2znBv7L1zG51vBZDMZL/NK35DJzLQYf60KUH2puPFDcfmcsoEXwogmc6tQ4TPS+jFM/yxqVQY1CuR+MR4AbSmsx8l9HbVEB7mnT8UOefB+f6KYaMzXgA87w/zT3VxMgP9Ggh3xKi6Y2p0w35JKp8ZLwAe/jfz8P9xCV8X7+lPAlqjELd3lLflX5G0CpD5AujuvQ6Jzn7+OsHTgPRbPoSbaGpq2nV069eOyy0qJMYLwJHsznZyJ99ONlQxBvu4518sXVzaB488MitfopRSSimllFJKKaWUUkoppZTyF8C/AJfb2LcntE/kAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAECSURBVDhP1ZGxTgJBEIb/f6FQEnwCYm1tpaG3MsHGtzAGfACD9gZiSYwP4IXkSKy0sVGegVZ8Ao1A4e14uw7HXnLkWvmanfln5t9JBpsP9c1RO2kfiZgmKXWXi/CLtK+zuP/kGwJyBm4QYrqpeqhSHsEYtN3QKDPwv8JEqbCjUiECfBL2dGlivOpIfy4bdvget6WyMli3dhFBb7CBfGhUTtCbGQh4r2E5RKQRKvriZzJ+qe4dNAjuq1SIQO7mce9M09UVltRanSnIhqZ/+JX5ZmmHi7j/oKonZ7DdurgicelOZUSuv0e9Gy2tJTPYOj7fZbUyMILnJEmixePtu5b+NcAvM8pRACN8p3YAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PositionBottomCenter, Type = Double, Dynamic = False, Default = \"6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionBottomLeft, Type = Double, Dynamic = False, Default = \"5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionBottomRight, Type = Double, Dynamic = False, Default = \"7", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionCenterLeft, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionCenterRight, Type = Double, Dynamic = False, Default = \"4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionCoords, Type = Double, Dynamic = False, Default = \"-1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopCenter, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopLeft, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopRight, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowOnClick, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ShowOnFocus, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ShowOnHover, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ShowOnMethod, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleBlue, Type = Double, Dynamic = False, Default = \"5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleBootStrap, Type = Double, Dynamic = False, Default = \"6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleDark, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleGreen, Type = Double, Dynamic = False, Default = \"4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleJTools, Type = Double, Dynamic = False, Default = \"9", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleLight, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StylePlain, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleRed, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleTipped, Type = Double, Dynamic = False, Default = \"10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleTipsy, Type = Double, Dynamic = False, Default = \"7", Scope = Public
	#tag EndConstant

	#tag Constant, Name = StyleYouTube, Type = Double, Dynamic = False, Default = \"8", Scope = Public
	#tag EndConstant


	#tag Enum, Name = Positions, Type = Integer, Flags = &h0
		Coords
		  TopLeft
		  TopRight
		  TopCenter
		  CenterLeft
		  CenterRight
		  BottomLeft
		  BottomCenter
		BottomRight
	#tag EndEnum

	#tag Enum, Name = TipStyles, Type = Integer, Flags = &h0
		Plain
		  Light
		  Dark
		  Red
		  Green
		  Blue
		  Bootstrap
		  Tipsy
		  YouTube
		  JTools
		Tipped
	#tag EndEnum

	#tag Enum, Name = Triggers, Type = Integer, Flags = &h0
		Hover
		  Click
		  Focus
		Manual
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
