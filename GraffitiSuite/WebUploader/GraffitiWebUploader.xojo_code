#tag Class
Protected Class GraffitiWebUploader
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "fileAdded"
		      if Parameters.Ubound >= 2 then
		        dim theFile as new GraffitiWebFile( Parameters(0), Parameters(1), Parameters(2), "" )
		        Files.Append( theFile )
		        if not FileExclude( theFile ) then
		          Buffer( "window.dropzoneUpload" + me.ControlID + "('" + EscapeString( theFile.Name ) + "');" )
		        else
		          Buffer( "window.dropzoneRemove" + me.ControlID + "('" + EscapeString( theFile.Name ) + "');" )
		          me.Remove( theFile )
		        end if
		      end if
		      
		    case "fileRemoved"
		      if Parameters.Ubound >= 2 then
		        dim intFile as Integer = FindFileIndex( Parameters(0) )
		        if intFile >= 0 and intFile <= Files.Ubound then
		          dim theFile as GraffitiWebFile = Files(intFile)
		          files.Remove( intFile )
		          FileRemoved( theFile )
		        end if
		      end if
		      
		    case "fileAvailable"
		      if Parameters.Ubound >= 1 then
		        dim theFile as GraffitiWebFile = FindFile( Parameters(0) )
		        if not IsNull( theFile ) then
		          theFile.Data = Parameters(1)
		          FileAvailable( theFile )
		        end if
		      end if
		    case "mouseIn"
		      MouseEnter()
		    case "mouseOut"
		      MouseExit()
		    end select
		    
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    if Value.BooleanValue then
		      Buffer( "window.dropzone" + me.ControlID + ".enable();" )
		    else
		      Buffer( "window.dropzone" + me.ControlID + ".disable();" )
		    end if
		    
		    Return True
		  case "style"
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  source.Append( "<div id='" + me.ControlID + "' style='overflow:auto;display:none;' class='dropzone'><div class='dz-message' style='width:100%;text-align:center;margin-top:22%;'>Drop files here<br />Or click to select</div></div>" )
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  if Session.Browser = WebSession.BrowserType.InternetExplorer then
		    if Session.RenderingEngine <> WebSession.EngineType.EdgeHTML then
		      Return ReplaceAll( jsUploadOldIE, "#ID#", me.ControlID )
		    end if
		  end if
		  
		  Return ReplaceAll( jsUpload, "#ID#", me.ControlID )
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function FindFile(theFilename as String) As GraffitiWebFile
		  dim intMax as Integer = Files.Ubound
		  for intCycle as Integer = 0 to intMax
		    dim currFile as GraffitiWebFile = Files(intCycle)
		    if currFile.Name = theFilename then Return currFile
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FindFileIndex(theFilename as String) As Integer
		  dim intMax as Integer = Files.Ubound
		  for intCycle as Integer = 0 to intMax
		    dim currFile as GraffitiWebFile = Files(intCycle)
		    if currFile.Name = theFilename then Return intCycle
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebDropzone -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "dropzone" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "dropzone.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "dropzone.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebButton -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Remove(theFile as GraffitiWebFile)
		  if not IsNull( theFile ) then Remove( theFile.Name )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Remove(theFile as String)
		  dim strExec() as String
		  strExec.Append( "function dropzoneRemoveFile(filename) {" )
		  strExec.Append( "var theFiles = window.dropzone" + me.ControlID + ".files;" )
		  strExec.Append( "for (var i = 0; i < theFiles.length; i++) {" )
		  strExec.Append( "if (theFiles[i].name == filename) {" )
		  strExec.Append( "window.dropzone" + me.ControlID + ".removeFile(theFiles[i]);" )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "dropzoneRemoveFile('" + EscapeString( theFile ) + "');" )
		  
		  Buffer( strExec )
		  
		  dim theFileIndex as Integer = FindFileIndex( theFile )
		  if theFileIndex >= 0 and theFileIndex <= Files.Ubound then Files.Remove( theFileIndex )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  dim strExec() as String
		  strExec.Append( "window.dropzone" + me.ControlID + ".removeAllFiles(true);" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOption(OptionName as String, OptionValue as String)
		  if isShown and LibrariesLoaded then
		    Buffer( "window.dropzone" + me.ControlID + ".options." + OptionName + " = " + OptionValue + ";" )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  dim strExec() as String
		  
		  strExec.Append( "window.dropzone" + me.ControlID + " = new Dropzone('div#" + me.ControlID + "', {" )
		  strExec.Append( "parallelUploads:0," )
		  strExec.Append( "uploadMultiple:true," )
		  strExec.Append( "addRemoveLinks:" + Str( AllowRemove ).Lowercase + "," )
		  if ThumbnailWidth > 0 then
		    strExec.Append( "thumbnailWidth:" + Str( ThumbnailWidth ) + "," )
		  else
		    strExec.Append( "thumbnailWidth:null," )
		  end if
		  if ThumbnailHeight > 0 then strExec.Append( "thumbnailHeight:" + Str( ThumbnailHeight ) + "" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.dropzone" + me.ControlID + ".on('addedfile', function (theFile) {" )
		  strExec.Append( "window.dropzoneCache" + me.ControlID + ".push(theFile);" )
		  strExec.Append( "if(theFile.type !== '') {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'fileAdded', [theFile.name, theFile.type, theFile.size]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'fileAdded', [theFile.name, 'unknown', theFile.size]);" )
		  strExec.Append( "}" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.dropzone" + me.ControlID + ".on('removedfile', function (theFile) {" )
		  strExec.Append( "if(theFile.type !== '') {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'fileRemoved', [theFile.name, theFile.type, theFile.size]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'fileRemoved', [theFile.name, 'unknown', theFile.size]);" )
		  strExec.Append( "}" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('mouseenter', function (e) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseIn');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('mouseout', function (e) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseOut');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + " .dz-message').html('" + EscapeString( mPromptText ) + "');" )
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  
		  RunBuffer()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event FileAvailable(theFile as GraffitiWebFile)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event FileExclude(theFile as GraffitiWebFile) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event FileRemoved(theFile as GraffitiWebFile)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		https://github.com/enyo/dropzone
		
		(The MIT License)
		
		Copyright (c) 2012 Matias Meno <m@tias.me>
		Logo & Website Design (c) 2015 "1910" www.weare1910.com
		
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAllowRemove
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAllowRemove = value
			  
			  UpdateOption( "addRemoveLinks", Str( value ).Lowercase )
			End Set
		#tag EndSetter
		AllowRemove As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Files() As GraffitiWebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  if Value then UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAllowRemove As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDefaultMessage As String = """Drop Files Here"""
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPromptText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mThumbnailHeight As Integer = 120
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mThumbnailWidth As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mUploadsDirectory As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldTextStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPromptText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'if value = "" then value = "Drop files here<br />Or click to select"
			  
			  mPromptText = value
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + " .dz-message').html('" + EscapeString( value ) + "');" )
			End Set
		#tag EndSetter
		PromptText As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mThumbnailHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mThumbnailHeight = value
			  
			  UpdateOption( "thumbnailHeight", if( value <= 0, "null", Str( value ) ) )
			End Set
		#tag EndSetter
		ThumbnailHeight As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mThumbnailWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mThumbnailWidth = value
			  
			  UpdateOption( "thumbnailwidth", if( value <= 0, "null", Str( value ) ) )
			End Set
		#tag EndSetter
		ThumbnailWidth As Integer
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.dropzone", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsUpload, Type = String, Dynamic = False, Default = \"window.dropzoneCache#ID# \x3D new Array();\r\rwindow.dropzoneRemove#ID# \x3D function(fileName) {\r\tvar cacheLength \x3D window.dropzoneCache#ID#.length;\r\tfor (var intCycle \x3D 0; intCycle < cacheLength; intCycle++) {\r\t\tvar currItem \x3D window.dropzoneCache#ID#[intCycle]\r\t\tif (currItem.name \x3D\x3D fileName) {\r\t\t\twindow.dropzoneCache#ID#.splice(intCycle\x2C 1);\r\t\t\tbreak;\r\t\t}\r\t}\r};\r\rwindow.dropzoneUpload#ID# \x3D function(fileName) {\r\tvar cacheLength \x3D window.dropzoneCache#ID#.length;\r\tfor (var intCycle \x3D 0; intCycle < cacheLength; intCycle++) {\r\t\tvar currItem \x3D window.dropzoneCache#ID#[intCycle]\r\t\tif (currItem.name \x3D\x3D fileName) {\r\t\t\tvar reader \x3D new FileReader();\r\t\t\treader.fileName \x3D currItem.name;\r\t\t\treader.fileType \x3D currItem.type;\r\t\t\treader.fileSize \x3D currItem.size;\r\t\t\treader.onload \x3D (event) \x3D> {\r\t\t\t\tvar reader \x3D event.target;\r\t\t\t\tif (typeof reader.fileType \x3D\x3D\x3D \'undefined\') { reader.fileType\x3D\'application/octet-stream\'; }\r\t\t\t\tif (reader.fileType.length \x3D\x3D 0) { reader.fileType \x3D \'application/octet-stream\'; }\r\t\t\t\tXojo.triggerServerEvent(\'#ID#\'\x2C \'fileAvailable\'\x2C [reader.fileName\x2Cevent.target.result]);\r\t\t\t};\r\t\t\treader.readAsDataURL(currItem);\r\r\t\t\twindow.dropzoneCache#ID#.splice(intCycle\x2C 1);\r\t\t\tbreak;\r\t\t}\r\t}\r};", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsUploadChunk_WorkInProgress, Type = String, Dynamic = False, Default = \"window.dropzoneCache#ID# \x3D new Array();\r\rwindow.dropzoneRemove#ID# \x3D function(fileName) {\r\tvar cacheLength \x3D window.dropzoneCache#ID#.length;\r\tfor (var intCycle \x3D 0; intCycle < cacheLength; intCycle++) {\r\t\tvar currItem \x3D window.dropzoneCache#ID#[intCycle]\r\t\tif (currItem.name \x3D\x3D fileName) {\r\t\t\twindow.dropzoneCache#ID#.splice(intCycle\x2C 1);\r\t\t\tbreak;\r\t\t}\r\t}\r};\r\rwindow.dropzoneUpload#ID# \x3D function(fileName) {\r\tvar cacheLength \x3D window.dropzoneCache#ID#.length;\r\tfor (var intCycle \x3D 0; intCycle < cacheLength; intCycle++) {\r\t\tvar currItem \x3D window.dropzoneCache#ID#[intCycle]\r\t\tif (currItem.name \x3D\x3D fileName) {\r    \t\tXojo.triggerServerEvent(\'#ID#\'\x2C \'fileStart\'\x2C [reader.fileName]);\r\t\t\tparseFile(currItem\x2C function(name\x2C chunk\x2C currOffset\x2C fileSize) {\r\t\t\t\tXojo.triggerServerEvent(\'#ID#\'\x2C \'fileChunk\'\x2C [name\x2C chunk\x2C currOffset\x2C fileSize]);\r\t\t\t}\x2C function(name) {\r\t\t\t\tXojo.triggerServerEvent(\'#ID#\'\x2C \'fileDone\'\x2C [name]);\r\t\t\t}\x2C function(name\x2C theError) {\r\t\t\t\tXojo.triggerServerEvent(\'#ID#\'\x2C \'fileError\'\x2C [name\x2C theError]);\r\t\t\t});\r\r\t\t\tbreak;\r\t\t}\r\t}\r};\r\rfunction parseFile_#ID#(file\x2C callbackChunk\x2C callbackComplete\x2C callbackError) {\r    var fileSize   \x3D file.size;\r    var chunkSize  \x3D 64 * 1024; // bytes\r    var offset     \x3D 0;\r    var self       \x3D this; // we need a reference to the current object\r    var chunkReaderBlock \x3D null;\r\r    var readEventHandler \x3D function(evt) {\r        if (evt.target.error \x3D\x3D null) {\r            offset +\x3D evt.target.result.length;\r            callbackChunk(file.name\x2C evt.target.result\x2C offset\x2C fileSize); // callback for handling read chunk\r        } else {\r            callbackError(file.name\x2C evt.target.error);\r            return;\r        }\r        if (offset >\x3D fileSize) {\r            callbackComplete(file.name);\r            return;\r        }\r\r        // of to the next chunk\r        chunkReaderBlock(offset\x2C chunkSize\x2C file);\r    }\r\r    chunkReaderBlock \x3D function(_offset\x2C length\x2C _file) {\r        var r \x3D new FileReader();\r        var blob \x3D _file.slice(_offset\x2C length + _offset);\r        r.onload \x3D readEventHandler;\r        r.readAsText(blob);\r    }\r\r    // now let\'s start the read with the first block\r    chunkReaderBlock(offset\x2C chunkSize\x2C file);\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsUploadOldIE, Type = String, Dynamic = False, Default = \"window.dropzoneCache#ID# \x3D [];\r\rwindow.dropzoneRemove#ID# \x3D function(fileName) {\r\tvar cacheLength \x3D window.dropzoneCache#ID#.length;\r\tfor (var intCycle \x3D 0; intCycle < cacheLength; intCycle++) {\r\t\tvar currItem \x3D window.dropzoneCache#ID#[intCycle]\r\t\tif (currItem.name \x3D\x3D fileName) {\r\t\t\twindow.dropzoneCache#ID#.splice(intCycle\x2C 1);\r\t\t\tbreak;\r\t\t}\r\t}\r};\r\rwindow.dropzoneUpload#ID# \x3D function(fileName) {\r\tvar cacheLength \x3D window.dropzoneCache#ID#.length;\r\tfor (var intCycle \x3D 0; intCycle < cacheLength; intCycle++) {\r\t\tvar currItem \x3D window.dropzoneCache#ID#[intCycle]\r\t\tif (currItem.name \x3D\x3D fileName) {\r\t\t\tvar reader \x3D new FileReader();\r\t\t\treader.fileName \x3D currItem.name;\r\t\t\treader.fileType \x3D currItem.type;\r\t\t\treader.fileSize \x3D currItem.size;\r\t\t\treader.onloadend \x3D function(e) {\r\t\t\t\tvar reader \x3D e.target;\r\t\t\t\tif (typeof reader.fileType \x3D\x3D\x3D \'undefined\') { reader.fileType\x3D\'application/octet-stream\'; }\r\t\t\t\tif (reader.fileType.length \x3D\x3D 0) { reader.fileType \x3D \'application/octet-stream\'; }\r\t\t\t\tXojo.triggerServerEvent(\'#ID#\'\x2C \'fileAvailable\'\x2C [reader.fileName\x2Ce.target.result]);\r\t\t\t};\r\t\t\treader.readAsDataURL(currItem);\r\r\t\t\twindow.dropzoneCache#ID#.splice(intCycle\x2C 1);\r\t\t\tbreak;\r\t\t}\r\t}\r};", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAawSURBVHhe7d1baFxFGAfwmZNYs5tKrX0QEW8vthXxLoitdx8K1e6WEgVFitIL4oPZ9MEHH3oBEcTuBlsrlgoFb/hidxOFqBQRoRWKfbAKDYiKFWtBfWi7m0ub8/nN7ncgbZM1mzNnd84334+wZ2bO5qS789+ZObsnjRJCCCGEEEIIIYQQQgghhBBCCCEEO5q2XunJFx7SSr+AD/4mBfCjDqe2V4ffOkW7veJdADJrB17SoHZTlcAJpYK1tfLO76nBG14FYObObwClTuGosNq3EHgTgGadH/ExBF4EYC6dH/EtBOwD0ErnR3wKAesAzKfzI76EgG0A4nR+xIcQsAyAjc6PcA8BuwDY7PwI5xAEtGUhic438FVyNajw82x+y93UxAabACTV+REcAUwIPuMWAhZTQNKdPx0o+Eur4Aku00HqA9DOzo9wCkGqA9CJzo9wCUFqA9DJzo9wCEEqA+BC50fSHoLUBcClzo+kOQSpCoCLnR9JawhSEwCXOz+SxhCkIgBp6PxI2kLgfADS1PmRNIXA6QCksfMjaQmBswFIc+dH0hACJwPAofMjrofAuQBw6vyIyyFwKgAcOz/iagicCQB2/hrs/ApVufo9BLVyvFI8QfWOc+eCEFD9VOLs+kDBM1R2gksBuIpKrAEOdlR0gkuXhH1JW95AH6WSE5wJQBjqd6jI2bdjQ8UhKjvBmQBMDO/8NVTwMEB9JBhvtPKAZwB/A8De2vmzq6jJGc6+E6j6ti6gUksyk6f3aq3XU9UeUM/XJs59TLXWjOyaoJJz3A3APGVyhf1JBEADPFutlD6iKhsuLQKdNhWokIqsSADmCgIJgM80zgFUZEUCMFchyAjgNVkDeC7UMgV4TYcSgFTQ5vMWMVcMRwCeQ3VS+AUAeJ6uJYXjFMBytZ4UhiOATAGtYBcALSNAS2QN4Dl+ATCf3Is54xgAmQJawDAAsghsBb8AyCKwJbII9BzDEUCmgFZwHAFkCmgBuwAA00u3ksJwBOB58WZSGK4BZARoBbsABCH8QEWrwkAdoyIr7AJQHRr8EIeAn6hqhfm9volPB3+hKiv8pgDUHU6tw26z8Fu4UMUJ5fWxSmkzNbBj9XcDM7nCNq30LaDVMnzZ4LHrX3iDZWhU6uV6G97U72MK2mzNS820N8p6Wtm0R/chtXLxRtw0ne8X972yaHJ86gaqtqw6/Ob/TifZNf2rVaD34L8EH6F5D4LWIPXrErAcvS/ReIMKH0m93XzhDZajNcvF958GW6p491H8OUfGDhTfpmYrLnhS56s3N/BYqGAQ++hWamqHbRiC7VTumEy+cBTTeSdV22FY6XBz7cDgSarHEn8K6OvrwnTuaXPnG9syqwvXUrkjsrmBDW3ufONJFepBKscWOwDZyeuKOGzdTNX26tb71KZNl1GtrXpy/Q/gxPQGVdtL66d6cwUr/9mUjUXgvbRtO1wVrMqcWjii+rYupKa2MFNeoPUIzp+LqantcMpdTsVY4gdAq7uo1BEYgkezk2dGMmv676GmRGXzW9bjem0Ef3KWmjoCp9xlVIzFxghwOW07R6sVOgiOZPOF3T35gZXUahWe4azD4x/Alfp+/IHd1Nw5oK1MfbHPArL5gUtOWzoP/sSHdhgXp2dwkTbv/58HT8p68XYJPkmP4Euuh5rdAKpSqxTzVJs3pgHwgKUA2JgCRIpJADwnAfCcBMBzEgDPSQAsq39qp+AQnkOepaa5qd8fDjW+v30kAJbgufCoBvX4WLm0rFYurahVSldg23u0uylQep+5v/k+8/3mOOZ4tDtREgBLghBerFaKB6laN1YubgCAr6g6I7N/rLxzI1XrzHHM8aiaKAmADaC+qQ6Vvqbaxd6l7Wxm3F8/Hh6XqomRANigZ78QVQfqZyrOqOn+Jse1RQJgAYB+kIqXguAOKs2syf6mx7VEAmCB1ur2bL5wwTxuLMq9fCV24w6qziLc0bjfhczxzHGpmhj5MMgmCF8NVfD++NTZf7JB79MQ6H58gm+jvbPCJ/CYDqFUC6uf9HQtXBKo8DmcG16j3TOTTwM9J58GChtsBKDpKlckRMMfVIolfgBAOfWHEH2B8+5xKsYSOwBaefEHH50CoE4ugPADqsbSRdt5Ozd6+Lfupff34ClLIhdjikvhq39jtTJo5c/Qxw6AcX708MGu5feZDy9W4ojQ1mv0vQLqOwigb7xc+oJaYot9Gnix7Nr+a3TYtZSqwoJQh7XM+Pnj/47sOk1NQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEENMo9R/HDpsaYresDwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADwSURBVDhPYxhwwAylMQCHT54cq5bVSjYNi1e/b5y4CxXGAIxQGgWANDOyMG9nZGDU+s/A8ImR4V/otw0TdkGlUQCGAciaoUIM+AxhgtJwANQ8C1kzCABt4fvPwLQaykUBWL0AAlz+RceAspZA649/29hnBRXGABguIBVQbADcC5z+RY2MjP95odzrUFqT8T/D0/+MDJ9AbIgQA8O3Df1FUCbCAK6AImBgIwGg36EskCpLKAsM/jH8C/+xYcIqEBu3F0CaoPg/w/9rQANXgTCYjQTgBgAldoJtxYIZ/zN+BCqRBWEw+8+/E2BNwwEwMAAALydQObMnV6kAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AllowRemove"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PromptText"
			Visible=true
			Group="Behavior"
			InitialValue="Drop files here<br />Or click to select"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThumbnailHeight"
			Visible=true
			Group="Behavior"
			InitialValue="120"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThumbnailWidth"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
