#tag Class
Protected Class GraffitiWebCalendarEvent
	#tag Method, Flags = &h0
		Sub Constructor(sTitle as String, dStart as Date, dEnd as Date = Nil)
		  Title = sTitle
		  StartDate = dStart
		  if not IsNull( dEnd ) then
		    EndDate = dEnd
		  else
		    EndDate = StartDate
		  end if
		  RepeatFrequency = RepeatNever
		  AllDay = True
		  
		  mID = GraffitiControlWrapper.GenerateUUID()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(sTitle as String, dStart as Date, dEnd as Date, iFrequency as Integer, iPeriod as Integer = 0, BGColor as Color = &c1111ff, Border as Color = &c1111ff, ForeColor as Color = &cffffff)
		  Title = sTitle
		  BackgroundColor = BGColor
		  BorderColor = Border
		  TextColor = ForeColor
		  StartDate = dStart
		  EndDate = dEnd
		  RepeatFrequency = iFrequency
		  RepeatPeriod = iPeriod
		  AllDay = False
		  
		  mID = GraffitiControlWrapper.GenerateUUID()
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		AllDay As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		BackgroundColor As Color = &c3a87ad
	#tag EndProperty

	#tag Property, Flags = &h0
		BorderColor As Color = &c3a87ad
	#tag EndProperty

	#tag Property, Flags = &h0
		CanMove As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		CanResize As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		Description As String
	#tag EndProperty

	#tag Property, Flags = &h0
		EndDate As Date
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		ID As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Location As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mID As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTitle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RepeatFrequency As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		RepeatPeriod As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		StartDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty

	#tag Property, Flags = &h0
		TextColor As Color = &cFFFFFF
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTitle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'value = ReplaceLineEndings( value, "" )
			  
			  mTitle = value
			End Set
		#tag EndSetter
		Title As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		URL As String
	#tag EndProperty


	#tag Constant, Name = RepeatDaily, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RepeatMonthly, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RepeatNever, Type = Double, Dynamic = False, Default = \"-1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RepeatWeekly, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RepeatYearly, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AllDay"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackgroundColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CanMove"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CanResize"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Description"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Location"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RepeatFrequency"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RepeatPeriod"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="URL"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
