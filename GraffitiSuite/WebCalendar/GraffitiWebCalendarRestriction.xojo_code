#tag Class
Protected Class GraffitiWebCalendarRestriction
	#tag Method, Flags = &h0
		Sub Constructor(RestrictionID as String, StartTime as Date, EndTime as Date, ParamArray Days as Integer)
		  ID = RestrictionID
		  
		  TimeStart = StartTime
		  if IsNull( TimeStart ) then TimeStart = new date
		  if IsNull( EndTime ) then
		    TimeEnd = TimeStart
		  else
		    TimeEnd = EndTime
		  end if
		  
		  for each i as Integer in Days
		    select case i
		    case 0
		      Sunday = True
		    case 1
		      Monday = True
		    case 2
		      Tuesday = True
		    case 3
		      Wednesday = True
		    case 4
		      Thursday = True
		    case 5
		      Friday = True
		    case 6
		      Saturday = True
		    end select
		  next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Friday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Monday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Saturday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Sunday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Thursday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeEnd As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeStart As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		Tuesday As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Wednesday As Boolean
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Friday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Monday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Saturday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Sunday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Thursday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Tuesday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Wednesday"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
