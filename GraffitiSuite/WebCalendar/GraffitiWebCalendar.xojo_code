#tag Class
Protected Class GraffitiWebCalendar
Inherits GraffitiControlWrapper
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "LanguagesLoaded"
		      dim strParam() as String
		      dim intMax as Integer = Parameters.Ubound
		      for intCycle as Integer = 0 to intMax
		        dim langCode as String = Parameters(intCycle)
		        strParam.Append( langCode )
		      next
		      ReDim Languages(-1)
		      Languages = strParam
		      LanguagesLoaded()
		    case "init"
		      AddRepeatingEvents()
		      
		      for intCycle as Integer = 0 to UBound( ExternalSources )
		        'me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('addEventSource', '" + ExternalSources(intCycle) + "');" )
		      next
		      
		      RunBuffer()
		    case "googleCalendarError"
		      if Parameters.Ubound >= 2 then GoogleCalendarError( Parameters(0), Parameters(1), Parameters(2) )
		    case "Selected"
		      if Parameters.Ubound < 16 then Return True
		      
		      dim dStart as date = new Date(Parameters(1).IntegerValue, Parameters(2).IntegerValue, Parameters(3).IntegerValue, Parameters(4).IntegerValue, Parameters(5).IntegerValue )
		      dim startHasTime as Boolean = Parameters(13).BooleanValue
		      
		      dim dEnd as date = new Date(Parameters(7).IntegerValue, Parameters(8).IntegerValue, Parameters(9).IntegerValue, Parameters(10).IntegerValue, Parameters(11).IntegerValue )
		      if dEnd.Hour = 0 and dEnd.Minute = 0 and dEnd.Second = 0 then
		        dEnd.Day = dEnd.Day - 1
		        dEnd.Hour = 23
		        dEnd.Minute = 59
		        dEnd.Second = 59
		      end if
		      dim endHasTime as Boolean = Parameters(14).BooleanValue
		      
		      dim posX as Integer = Parameters(15)
		      dim posY as Integer = Parameters(16)
		      
		      DateSelected( dStart, dEnd, startHasTime, endHasTime, posX, posY )
		      
		    case "EventClick"
		      
		      if Parameters.Ubound < 1 then Return True
		      dim currEvent as GraffitiWebCalendarEvent = FindEvent( Parameters(0).StringValue )
		      
		      if IsNull( currEvent ) then Return True
		      
		      if currEvent.Title = Parameters(1).StringValue then
		        EventSelected( currEvent )
		      end if
		      
		    case "EventHover"
		      if Parameters.Ubound < 3 then Return True
		      
		      dim currEvent as GraffitiWebCalendarEvent = FindEvent( Parameters(0).StringValue )
		      
		      if IsNull( currEvent ) then
		        dim tempEvent as new GraffitiWebCalendarEvent( Parameters(1), nil, nil, -1 )
		        EventMouseEnter( tempEvent, Parameters(2).IntegerValue, Parameters(3).IntegerValue )
		      else
		        if currEvent.Title = Parameters(1).StringValue then EventMouseEnter( currEvent, Parameters(2).IntegerValue, Parameters(3).IntegerValue )
		      end if
		      
		    case "EventMouseOut"
		      if Parameters.Ubound < 1 then Return True
		      
		      dim currEvent as GraffitiWebCalendarEvent = FindEvent( Parameters(0).StringValue )
		      
		      if IsNull( currEvent ) then Return True
		      
		      if currEvent.Title = Parameters(1).StringValue then
		        EventMouseLeave( currEvent )
		      end if
		      
		    case "EventDrop", "EventResize"
		      '[0] event.id
		      '[1] event.allDay
		      '[2] event.start.year()
		      '[3] event.start.month() + 1
		      '[4] event.start.date()
		      '[5] event.start.hasTime()
		      '-- if event.start.hasTime
		      '[6] event.start.hour()
		      '[7] event.start.minute()
		      '[8] event.start.second()
		      '[9] if event.end <> nil
		      '[10] event.end.year()
		      '[11] event.end.month() + 1
		      '[12] event.end.date()
		      '[13] event.end.hasTime()
		      '-- if event.end.hasTime()
		      '[14] event.end.hour()
		      '[15] event.end.minute()
		      '[16] event.end.second()
		      
		      if Parameters.Ubound < 16 then Return True
		      
		      dim currEvent as GraffitiWebCalendarEvent = FindEvent( Parameters(0).StringValue )
		      currEvent.AllDay = Parameters(1).BooleanValue
		      
		      dim newStart, newEnd as Date
		      
		      newStart = new Date( Parameters(2), Parameters(3), Parameters(4) )
		      if Parameters(5).BooleanValue then
		        newStart.Hour = Parameters(6).IntegerValue
		        newStart.Minute = Parameters(7).IntegerValue
		        newStart.Second = Parameters(8).IntegerValue
		      ElseIf not IsNull( currEvent.StartDate ) then
		        newStart.Hour = currEvent.StartDate.Hour
		        newStart.Minute = currEvent.StartDate.Minute
		        newStart.Second = currEvent.StartDate.Second
		      end if
		      
		      if parameters(9).BooleanValue then
		        newEnd = new Date( Parameters(10).IntegerValue, Parameters(11).IntegerValue, Parameters(12).IntegerValue )
		        if Parameters(13).BooleanValue then
		          newEnd.Hour = Parameters(14).IntegerValue
		          newEnd.Minute = Parameters(15).IntegerValue
		          newEnd.Second = Parameters(16).IntegerValue
		        elseif not IsNull( currEvent.endDate ) then
		          newEnd.Hour = currEvent.endDate.Hour
		          newEnd.Minute = currEvent.endDate.Minute
		          newEnd.Second = currEvent.endDate.Second
		        else
		          newEnd.Hour = 23
		          newEnd.Minute = 59
		          newEnd.Second = 59
		        end if
		      else
		        newEnd = nil
		      end if
		      
		      currEvent.StartDate = newStart
		      currEvent.EndDate = newEnd
		      
		      EventChanged(currEvent)
		    case "ViewChanged"
		      if Parameters.Ubound < 12 then Return True
		      dim intIndex as Views = GetView( Parameters(0).StringValue )
		      dim newStart as new Date( Parameters(1), Parameters(2), Parameters(3), Parameters(4), Parameters(5), Parameters(6) )
		      dim newEnd as new Date( Parameters(7),Parameters(8), Parameters(9), Parameters(10), Parameters(11), Parameters(12) )
		      if newEnd.Month > newStart.Month + 1 then
		        newStart.Month = newStart.Month + 1
		        newStart.Day = newStart.Day / 2
		      end if
		      'if intIndex <> Views.Month then
		      mViewDate = newStart
		      mCurrentView = intIndex
		      ViewChanged( intIndex, newStart, newEnd )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  Select Case Name
		  Case "Style"
		    Dim st as WebStyle = WebStyle(value)
		    dim strOut() as String
		    
		    strOut.Append( "var thisControl = window.GSjQuery('#" + me.ControlID + "');" )
		    If not self.style.IsDefaultStyle then strOut.Append( "thisControl.removeClass('" + self.Style.Name + "');" )
		    if not st.isdefaultstyle then strOut.Append( "thisControl.addClass('" + st.Name + "');" )
		    
		    Buffer( strOut )
		  case "height", "width"
		    dim strExec() as string
		    
		    strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		    
		    if name = "width" then
		      strExec.Append( "theElement.style.width='" + Str( value.IntegerValue ) + "px';" )
		      strExec.Append( "theElement.style.maxWidth='" + Str( value.IntegerValue ) + "px';" )
		      strExec.Append( "theElement.style.minWidth='" + Str( Value.IntegerValue ) + "px';" )
		    else
		      strExec.Append( "theElement.style.height='" + Str( value.IntegerValue ) + "px';" )
		      strExec.Append( "theElement.style.maxHeight='" + Str( value.IntegerValue ) + "px';" )
		      strExec.Append( "theElement.style.minHeight='" + Str( Value.IntegerValue ) + "px';" )
		    End If
		    
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		    
		    UpdateHeight()
		    
		    Return True
		  End Select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		  
		  UpdateOptions()
		  
		  UpdateHeight()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  UpdateHeight()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<style>" + ReplaceAll( cssMain, "{ContID}", me.ControlID ) + "</style>" )
		  
		  source.Append( "<div id='" + me.ControlID + "' class='" + if( me.Style.IsDefaultStyle, "", me.Style.Name ) + "' style='display:none;'>" )
		  'source.Append( "<div id=""" + me.ControlID + """ style=""margin-top:5px;""></div>" )
		  source.append( "</div>")
		  source.Append( "<script>Xojo.triggerServerEvent('" + me.ControlID + "', 'LibrariesLoaded', []);</script>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  Shown()
		  
		  UpdateHeight()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddEvent(newEvent as GraffitiWebCalendarEvent)
		  dim strExec() as String
		  
		  if not IsNull( newEvent ) then
		    me.Events.Append( newEvent )
		    
		    if mLockUpdate then Return
		    
		    if newEvent.RepeatFrequency <> newEvent.RepeatNever then
		      strExec.Append( PrepareEvent( newEvent ) )
		    else
		      strExec.Append( "var newEvent = " + EventToString( newEvent ) + ";" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('renderEvent', newEvent, true);" )
		    end if
		    
		  end if
		  
		  Buffer( Join( strExec, "" ) )
		  
		  RerenderEvents()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddExternalSource(GoogleCalendarID as String, GoogleCalendarKey as String, BGColor as Color = &c3a87ad, BorderColor as Color = &c3a87ad, TextColor as Color = &cFFFFFF)
		  if GoogleCalendarID <> "" then
		    dim dSource as new Dictionary
		    dSource.Value( "id" ) = GoogleCalendarID
		    dSource.Value( "key" ) = GoogleCalendarKey
		    dSource.Value( "bg" ) = BGColor
		    dSource.Value( "border" ) = borderColor
		    dSource.Value( "text" ) = TextColor
		    ExternalSources.Append( dSource )
		    
		    doAddSource( dSource )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AddRepeatingEvents()
		  dim strExec() as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( RepeatingBuffer )
		  for intCycle = 0 to intMax
		    strExec.Append( RepeatingBuffer(intCycle) )
		  next
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddRestriction(theRestriction as GraffitiWebCalendarRestriction)
		  if IsNull( theRestriction ) then Return
		  
		  Restrictions.Append( theRestriction )
		  
		  UpdateRestrictions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColorToString(c as Color) As String
		  dim strRet as String = "rgba(" + _
		  Str( c.Red, "#" ) + "," + _
		  Str( c.Green, "#" ) + "," + _
		  Str( c.Blue, "#" ) + "," + _
		  Str( 1 - (c.Alpha / 255), "0.00" ) + ")"
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DateToString(dConvert as Date, IncludeTime as Boolean = True) As String
		  dim strRet as String
		  if not IsNull( dConvert ) then
		    strRet = Str( dConvert.Year, "0000" ) + "-" + _
		    Str( dConvert.Month, "00" ) + "-" + _
		    Str( dConvert.Day, "00" )
		    if IncludeTime then
		      strRet = strRet + "T" + _
		      Str( dConvert.Hour, "00" ) + ":" + _
		      Str( dConvert.Minute, "00" ) + ":" + _
		      Str( dConvert.Second, "00" )
		    end if
		  end if
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DateToTime24(d as Date, default as String = "00:00") As String
		  if IsNull( d ) then Return default
		  
		  Return Str( d.Hour, "0#" ) + ":" + Str( d.Minute, "00" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAll()
		  dim strExec() as String = Array( "window.GSjQuery('" + me.ControlID + "').fullCalendar('unselect');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub doAddSource(theSource as Dictionary)
		  if IsNull( theSource ) then Return
		  if not theSource.HasKey( "id" ) or _
		  not theSource.HasKey( "key" ) or _
		  not theSource.HasKey( "bg" ) or _
		  not theSource.HasKey( "border" ) or _
		  not theSource.HasKey( "text" ) then Return
		  
		  
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('addEventSource', {" )
		  strExec.Append( "googleCalendarId: '" + theSource.Value( "id" ).StringValue + "'," )
		  strExec.Append( "googleCalendarApiKey: '" + theSource.Value( "key" ).StringValue + "'," )
		  strExec.Append( "backgroundColor: '" + ColorToString( theSource.Value( "bg" ).ColorValue ) + "'," )
		  strExec.Append( "borderColor: '" + ColorToString( theSource.Value( "border" ).ColorValue ) + "'," )
		  strExec.Append( "textColor: '" + ColorToString( theSource.Value( "text" ).ColorValue ) + "'" )
		  strExec.Append( "}" )
		  strExec.Append( ");" )
		  
		  Buffer( Join( strExec, "" ) )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EventsToString() As String
		  ReDim RepeatingBuffer(-1)
		  dim strEvents() as String = Array( "events: [" )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( me.Events )
		  
		  dim currEvent as GraffitiWebCalendarEvent
		  dim strRep as String
		  for intCycle = 0 to intMax
		    currEvent = me.Events( intCycle )
		    if currEvent.RepeatFrequency = GraffitiWebCalendarEvent.RepeatNever then
		      strEvents.Append( EventToString( currEvent ) )
		      strEvents.Append( if( intCycle < intMax, ",", "" ) )
		    else
		      RepeatingBuffer.Append( PrepareEvent( currEvent ) )
		    end if
		  next
		  
		  strEvents.Append( "]" )
		  
		  Return Join( strEvents, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EventToString(currEvent as GraffitiWebCalendarEvent) As String
		  dim strEvent() as String
		  
		  dim trueEnd as Date = currEvent.EndDate
		  'if currEvent.AllDay and not IsNull( currEvent.EndDate ) then
		  'trueEnd = new Date( currEvent.EndDate.Year, currEvent.EndDate.Month, currEvent.EndDate.Day, 23, 59, 59 )
		  'end if
		  
		  strEvent.Append( "{" + _
		  "id: '" + currEvent.ID + "'," + _
		  "title: '" + EscapeString( currEvent.Title ) + "'," + _
		  "start: moment('" + me.DateToString( currEvent.StartDate, not currEvent.AllDay ) + "')," )
		  if not currEvent.AllDay then
		    if not IsNull( trueEnd ) then
		      strEvent.Append( "end: moment('" + me.DateToString( trueEnd ) + "')," )
		    elseif not IsNull( currEvent.EndDate ) then
		      strEvent.Append( "end: moment('" + me.DateToString( currEvent.EndDate ) + "')," )
		    else
		      dim newEnd as new Date( currEvent.StartDate )
		      newEnd.Hour = 23
		      newEnd.Minute = 59
		      newEnd.Second = 59
		      strEvent.Append( "end: moment('" + me.DateToString( newEnd ) + "')," )
		    end if
		  end if
		  strEvent.Append( "textColor: '" + ColorToString( currEvent.TextColor ) + "'," + _
		  "backgroundColor: '" + ColorToString( currEvent.BackgroundColor ) + "'," + _
		  "borderColor: '" + ColorToString( currEvent.BorderColor ) + "'," )
		  strEvent.Append( "eventStartEditable: " + Lowercase( Str( currEvent.CanMove ) ) + "," )
		  strEvent.Append( "eventDurationEitable: " + Lowercase( Str( currEvent.CanResize ) ) + "," )
		  strEvent.Append( "description: '" + EscapeString( currEvent.Description ) + "'," )
		  strEvent.Append( "location: '" + EscapeString( currEvent.Location ) + "'," )
		  strEvent.Append( "allDay: " + Lowercase( Str( currEvent.AllDay ) ) )
		  strEvent.Append( "}" )
		  
		  Return Join( strEvent, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindEvent(theID as String) As GraffitiWebCalendarEvent
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Events )
		  
		  dim currEvent as GraffitiWebCalendarEvent
		  
		  for intCycle = 0 to intMax
		    currEvent = Events(intCycle)
		    if currevent.ID = theID then
		      Return currEvent
		    end if
		  next
		  
		  Return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindEventIndex(theID as String) As Integer
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Events )
		  
		  for intCycle = 0 to intMax
		    if Events(intCycle).ID = theID then
		      Return intCycle
		    end if
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetDefaultDate() As String
		  if IsNull( mViewDate ) then mViewDate = new date
		  
		  Return DateToString( mViewDate, False )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetHeaderOptions() As String
		  dim strRet() as String
		  strRet.Append( "header: {" )
		  if ShowYearButtons and ShowTodayButton then
		    strRet.Append( "left: 'prevYear,nextYear today'," )
		  ElseIf ShowYearButtons and not ShowTodayButton then
		    strRet.Append( "left: 'prevYear,nextYear'," )
		  ElseIf not ShowYearButtons and ShowTodayButton then
		    strRet.Append( "left: 'today'," )
		  Else
		    strRet.Append( "left: ''," )
		  end if
		  
		  if ShowTitle and ShowNavButtons then
		    strRet.Append( "center: 'prev title next'," )
		  ElseIf ShowTitle and not ShowNavButtons then
		    strRet.Append( "center: 'title'," )
		  end if
		  
		  strRet.Append( if( ShowMonthButton and ShowWeekButton and ShowDayButton, "right: 'month,agendaWeek,agendaDay'", _
		  if( ShowMonthButton and ShowWeekButton and not ShowDayButton, "right: 'month,agendaWeek'", _
		  if( ShowMonthButton and not ShowWeekButton and not ShowDayButton, "right: 'month'", _
		  if( not ShowMonthButton and not ShowWeekButton and ShowDayButton, "right: 'agendaDay'", _
		  if( not ShowMonthButton and ShowWeekButton and not ShowDayButton, "right: 'agendaWeek'", _
		  if( not ShowMonthButton and ShowWeekButton and ShowDayButton, "right: 'agendaDay,agendaWeek'", "right: ''" ) ) ) ) ) ) )
		  
		  strRet.Append( "}," )
		  
		  Return Join( strRet, "" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetView(theView as String) As Views
		  Select Case theView
		  case "month"
		    Return Views.Month
		  case "agenda"
		    Return Views.Agenda
		  Case "agendaWeek"
		    Return Views.AgendaWeek
		  case "agendaDay"
		    Return Views.AgendaDay
		  case "basic"
		    Return Views.Basic
		  case "basicWeek"
		    Return Views.BasicWeek
		  case "basicDay"
		    Return Views.BasicDay
		  case "list"
		    Return Views.List
		  case "listDay"
		    Return Views.ListDay
		  case "listWeek"
		    Return Views.ListWeek
		  case "listMonth"
		    Return Views.ListMonth
		  case "listYear"
		    Return Views.ListYear
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetViewString(theView as Views) As String
		  Select Case theView
		  case Views.Month
		    Return "month"
		  case Views.Agenda
		    Return "agenda"
		  Case Views.AgendaWeek
		    Return "agendaWeek"
		  case Views.AgendaDay
		    Return "agendaDay"
		  case Views.Basic
		    Return "basic"
		  case Views.BasicDay
		    Return "basicDay"
		  case Views.BasicWeek
		    Return "basicWeek"
		  case Views.List
		    Return "list"
		  case Views.ListDay
		    Return "listDay"
		  case Views.ListWeek
		    Return "listWeek"
		  case Views.ListMonth
		    Return "listMonth"
		  case Views.ListYear
		    Return "listYear"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub GoToDate(theDate as Date)
		  dim strExec() as String
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('gotoDate', moment('" + me.DateToString( theDate ) + "');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCalendar -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddjQueryUI( CurrentSession ) )
		    strOut.Append( AddMoment( CurrentSession ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "fullcalendar" ).Child( "fullcalendar.min.css" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "fullcalendar" ).Child( "fullcalendar.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "fullcalendar" ).Child( "locale-all.js" ), JavascriptNamespace ) )
		    
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "emtopx.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCalendar -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ICSExport(Domain as String) As String
		  dim strOut() as String
		  strOut.Append( "BEGIN:VCALENDAR" )
		  strOut.Append( "VERSION:2.0" )
		  strOut.Append( "PRODID:GraffitiWebCalendar" )
		  
		  dim currEvent as GraffitiWebCalendarEvent
		  
		  dim intMax as Integer = Events.Ubound
		  for intCycle as Integer = 0 to intMax
		    currEvent = Events(intCycle)
		    if IsNull( currEvent ) then Continue
		    strOut.Append( "BEGIN:VEVENT" )
		    strOut.Append( "UID:" + ICS_DateTimeToString( currEvent.StartDate ) + "@" + Domain )
		    if currEvent.Title.Len > 0 then strOut.Append( "SUMMARY:" + currEvent.Title )
		    if currEvent.Description.Len > 0 then strOut.Append( "DESCRIPTION:" + currEvent.Description )
		    if currEvent.Location.Len > 0 then strOut.Append( "LOCATION:" + currEvent.Location )
		    if currEvent.URL.Len > 0 then strOut.Append( "URL:" + currEvent.URL )
		    strOut.Append( "COLOR:" + ColorToHex( currEvent.BackgroundColor ) )
		    strOut.Append( "X-RSCV-TEXTCOLOR:" + ColorToHex( currEvent.TextColor ) )
		    strOut.Append( "X-RSCV-BORDERCOLOR:" + ColorToHex( currEvent.BorderColor ) )
		    if currEvent.CanMove then strOut.Append( "X-RSCV-CANMOVE:" + Str( currEvent.CanMove ) )
		    if currEvent.CanResize then strOut.Append( "X-RSCV-CANRESIZE:" + Str( currEvent.CanResize ) )
		    
		    strOut.Append( "DTSTAMP:" + ICS_DateTimeToString( currEvent.StartDate ) )
		    strOut.Append( "DTSTART:" + ICS_DateTimeToString( currEvent.StartDate ) )
		    
		    if currEvent.RepeatFrequency = currEvent.RepeatNever then
		      if currEvent.AllDay then
		        dim endDate as new Date( currEvent.EndDate.Year, currEvent.EndDate.Month, currEvent.EndDate.Day + 1, 0, 0, 0 )
		        strOut.Append( "DTEND:" + ICS_DateTimeToString( endDate ) )
		      else
		        strOut.Append( "DTEND:" + ICS_DateTimeToString( currEvent.EndDate ) )
		      end if
		    else
		      strOut.Append( "RRULE:FREQ=" + ICS_FrequencyToString( currEvent.RepeatFrequency ) + _
		      ";INTERVAL=" + Str( currEvent.RepeatPeriod ) + _
		      ";UNTIL=" + ICS_DateTimeToString( currEvent.EndDate ) )
		      
		    end if
		    strOut.Append( "END:VEVENT" )
		  next
		  
		  strOut.Append( "END:VCALENDAR" )
		  
		  Return Join( strOut, EndOfLine.Windows )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ICSImport(theICS as String)
		  dim arrLines() as String = Split( ReplaceLineEndings( theICS, EndOfLine.Windows ), EndOfLine.Windows )
		  
		  dim currEvent as GraffitiWebCalendarEvent
		  dim currLine as String
		  dim intMax as Integer = arrLines.Ubound
		  for intCycle as Integer = 0 to intMax
		    currLine = arrLines(intCycle)
		    if currLine = "BEGIN:VEVENT" then
		      currEvent = new GraffitiWebCalendarEvent( "", new Date, new Date )
		    elseif currLine = "END:VEVENT" then
		      Events.Append( currEvent )
		      currEvent = nil
		    else
		      if CountFields( currLine, ":" ) <> 2 then Continue
		      dim strKey as String = NthField( currLine, ":", 1 )
		      dim strValue as String = NthField( currLine, ":", 2 )
		      
		      select case strKey
		      case "SUMMARY"
		        currEvent.Title = strValue
		      case "DESCRIPTION"
		        currEvent.Description = strValue
		      case "LOCATION"
		        currEvent.Location = strValue
		      case "URL"
		        currEvent.URL = strValue
		      case "COLOR", "X-RSCV-COLOR"
		        currEvent.BackgroundColor = ColorFromHex( strValue )
		      case "X-RSCV-TEXTCOLOR"
		        currEvent.TextColor = ColorFromHex( strValue )
		      case "X-RSCV-BORDERCOLOR"
		        currEvent.BorderColor = ColorFromHex( strValue )
		      case "X-RSCV-CANMOVE"
		        if strValue = "True" then currEvent.CanMove = True
		      case "X-RSCV-CANRESIZE"
		        if strValue = "True" then currEvent.CanResize = True
		      case "DTSTART"
		        currEvent.StartDate = ICS_StringToDateTime( strValue )
		      case "DTEND"
		        currEvent.EndDate = ICS_StringToDateTime( strValue )
		      case "RRULE"
		        ICS_RRuleToEvent( strValue, currEvent )
		      end select
		    end if
		  next
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ICS_DateTimeToString(theDate as Date) As String
		  Return Format( theDate.Year, "0000" ) + _
		  Format( theDate.Month, "00" ) + _
		  Format( theDate.Day, "00" ) + _
		  "T" + _
		  Format( theDate.Hour, "00" ) + _
		  Format( theDate.Minute, "00" ) + _
		  Format( theDate.Second, "00" ) + _
		  "Z"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ICS_FrequencyToString(theFrequency as Integer) As String
		  select case theFrequency
		  case GraffitiWebCalendarEvent.RepeatDaily
		    Return "DAILY"
		  case GraffitiWebCalendarEvent.RepeatWeekly
		    Return "WEEKLY"
		  case GraffitiWebCalendarEvent.RepeatMonthly
		    Return "MONTHLY"
		  case GraffitiWebCalendarEvent.RepeatYearly
		    Return "YEARLY"
		  end select
		  
		  Return "DAILY"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ICS_RRuleToEvent(theRule as String, theEvent as GraffitiWebCalendarEvent)
		  dim currProp, strKey, strValue as String
		  dim intMax as Integer = CountFields( theRule, ";" )
		  for intCycle as Integer = 1 to intMax
		    currProp = NthField( theRule, ";", intCycle )
		    
		    strKey = NthField( currProp, "=", 1 )
		    strValue = NthField( currProp, "=", 2 )
		    
		    select case strKey
		    case "FREQ"
		      theEvent.RepeatFrequency = ICS_StringToFrequency( strValue )
		    case "INTERVAL"
		      theEvent.RepeatPeriod = Val( strValue )
		    case "UNTIL"
		      theEvent.EndDate = ICS_StringToDateTime( strValue )
		    end select
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ICS_StringToDateTime(s as String) As Date
		  if CountFields( s, "T" ) <= 1 then Return new Date
		  
		  s = ReplaceAll( s, "Z", "" )
		  dim strDate as String = NthField( s, "T", 1 )
		  dim strTime as String = NthField( s, "T", 2 )
		  
		  dim intYear as Integer = Val( Left( strDate, 4 ) )
		  dim intMonth as Integer = Val( Mid( strDate, 5, 2 ) )
		  dim intDay as Integer = Val( Right( strDate, 2 ) )
		  
		  dim intHour as Integer = Val( Left( strTime, 2 ) )
		  dim intMinute as Integer = Val( Mid( strTime, 3, 2 ) )
		  dim intSecond as Integer = Val( Right( strTime, 2 ) )
		  
		  return new Date( intYear, intMonth, intDay, intHour, intMinute, intSecond )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ICS_StringToFrequency(theFrequency as String) As Integer
		  select case theFrequency
		  case "DAILY"
		    return GraffitiWebCalendarEvent.RepeatDaily
		  case "WEEKLY"
		    return GraffitiWebCalendarEvent.RepeatWeekly
		  case "MONTHLY"
		    return GraffitiWebCalendarEvent.RepeatMonthly
		  case "YEARLY"
		    return GraffitiWebCalendarEvent.RepeatYearly
		  end select
		  
		  Return GraffitiWebCalendarEvent.RepeatNever
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function IntToFreq(iFreq as Integer) As String
		  Select Case iFreq
		  case GraffitiWebCalendarEvent.RepeatDaily
		    Return "days"
		  case GraffitiWebCalendarEvent.RepeatWeekly
		    Return "days"
		  case GraffitiWebCalendarEvent.RepeatMonthly
		    Return "month"
		  case GraffitiWebCalendarEvent.RepeatYearly
		    Return "year"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadTheme(themeURL as String)
		  CustomThemeURL = themeURL
		  
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#gwcal_" + me.ControlID + "_theme').attr('href','" + themeURL + "');;" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveNext()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('next');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveNextYear()
		  dim strExec() as String = Array( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('nextYear');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MovePrev()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('prev');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MovePrevYear()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('prevYear');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveToDate(theDate as Date)
		  ViewDate = theDate
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MoveToToday()
		  dim strExec() as String
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('today');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function PrepareEvent(currEvent as GraffitiWebCalendarEvent) As String
		  dim strRet as String = ReplaceAll( jsRepeatSource, "{ContID}", me.ControlID )
		  strRet = ReplaceAll( strRet, "{Title}", EscapeString( currEvent.Title ) )
		  
		  strRet = ReplaceAll( strRet, "{StartYear}", Str( currEvent.StartDate.Year, "#" ) )
		  strRet = ReplaceAll( strRet, "{StartMonth}", Str( currEvent.StartDate.Month, "#" ) )
		  strRet = ReplaceAll( strRet, "{StartDay}", Str( currEvent.StartDate.Day, "#" ) )
		  
		  strRet = ReplaceAll( strRet, "{EndYear}", Str( currEvent.EndDate.Year, "#" ) )
		  strRet = ReplaceAll( strRet, "{EndMonth}", Str( currEvent.EndDate.Month, "#" ) )
		  strRet = ReplaceAll( strRet, "{EndDay}", Str( currEvent.EndDate.Day, "#" ) )
		  
		  strRet = ReplaceAll( strRet, "{StartHour}", Str( currEvent.StartDate.Hour, "#" ) )
		  strRet = ReplaceAll( strRet, "{StartMin}", Str( currEvent.StartDate.Minute, "#" ) )
		  
		  strRet = ReplaceAll( strRet, "{EndHour}", Str( currEvent.EndDate.Hour, "#" ) )
		  strRet = ReplaceAll( strRet, "{EndMin}", Str( currEvent.EndDate.Minute, "#" ) )
		  
		  strRet = ReplaceAll( strRet, "{RepeatID}", currEvent.ID )
		  
		  strRet = ReplaceAll( strRet, "{Color}", "'black'" )
		  strRet = ReplaceAll( strRet, "{BGColor}", "'" + ColorToString( currEvent.BackgroundColor ) + "'" )
		  strRet = ReplaceAll( strRet, "{BorderColor}", "'" + ColorToString( currEvent.BorderColor ) + "'" )
		  strRet = ReplaceAll( strRet, "{TextColor}", "'" + ColorToString( currEvent.TextColor ) + "'" )
		  
		  strRet = ReplaceAll( strRet, "{RepStep}", me.IntToFreq( currEvent.RepeatFrequency ) )
		  dim intRepPeriod as Integer = currEvent.RepeatPeriod
		  if currEvent.RepeatFrequency = currEvent.RepeatWeekly then intRepPeriod = intRepPeriod * 7
		  strRet = ReplaceAll( strRet, "{StepCount}", Str( intRepPeriod, "#" ) )
		  
		  strRet = ReplaceAll( strRet, "{Description}", currEvent.Description )
		  strRet = ReplaceAll( strRet, "{Location}", currEvent.Location )
		  
		  Return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefetchEvents()
		  dim strExec() as String = Array( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('refetchEvents');" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAllEvents()
		  ReDim Events(-1)
		  
		  if mLockUpdate then Return
		  
		  dim strExec() as String = Array( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('removeEvents');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAllRestrictions()
		  ReDim Restrictions(-1)
		  
		  UpdateRestrictions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveEvent(theEvent as GraffitiWebCalendarEvent)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('removeEvents', '" + Str( theEvent.ID, "#" ) + "');" )
		  
		  dim intRem as Integer = FindEventIndex( theEvent.ID )
		  if intRem >= 0 and intRem <= UBound( Events ) Then
		    Events.Remove( intRem )
		  end if
		  
		  if mLockUpdate then Return
		  
		  Buffer( strExec )
		  
		  RerenderEvents()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveExternalSource(sourceID as String)
		  dim strCurr as String
		  
		  dim strExec() as String
		  
		  for intCycle as Integer = 0 to ExternalSources.Ubound
		    dim dCurr as Dictionary = ExternalSources(intCycle)
		    
		    if IsNull( dCurr ) then Continue
		    if not dCurr.HasKey( "id" ) then Continue
		    
		    if dCurr.Value( "id" ).StringValue = sourceID Then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('removeEventSource', '" + sourceID + "');" )
		      ExternalSources.Remove( intCycle )
		      Return
		    end if
		  next
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveRestriction(theID as String)
		  dim currRes as GraffitiWebCalendarRestriction
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Restrictions.Ubound
		  for intCycle = intMax DownTo 0
		    currRes = Restrictions(intCycle)
		    if currRes.ID = theID then
		      Restrictions.Remove( intCycle )
		      Return
		    end if
		  next
		  
		  UpdateRestrictions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RerenderEvents()
		  'dim strExec() as String 
		  'strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('rerenderEvents');" )
		  'strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('unselect');" )
		  '
		  'Buffer( strExec )
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RestrictionsToString() As String
		  if RestrictView then
		    dim strExec() as String
		    
		    strExec.Append( "businessHours: [" )
		    
		    dim currRes as GraffitiWebCalendarRestriction
		    
		    dim intCycle as Integer
		    dim intMax as Integer = Restrictions.Ubound
		    for intCycle = 0 to intMax
		      currRes = Restrictions(intCycle)
		      
		      if IsNull( currRes ) then Continue
		      
		      dim strDOW() as String
		      if currRes.Sunday then strDOW.Append( "0" )
		      if currRes.Monday then strDOW.Append( "1" )
		      if currRes.Tuesday then strDOW.Append( "2" )
		      if currRes.Wednesday then strDOW.Append( "3" )
		      if currRes.Thursday then strDOW.Append( "4" )
		      if currRes.Friday then strDOW.Append( "5" )
		      if currRes.Saturday then strDOW.Append( "6" )
		      
		      
		      strExec.Append( "{" )
		      strExec.Append( "dow: [" + Join( strDOW, "," ) + "]," )
		      strExec.Append( "start: '" + DateToTime24( currRes.TimeStart ) + "'," )
		      strExec.Append( "end: '" + DateToTime24( currRes.TimeEnd ) + "'" )
		      strExec.Append( "}" )
		      
		      if intCycle < intMax then strExec.Append( "," )
		      
		    next
		    
		    strExec.Append( "]," )
		    
		    Return Join( strExec, "" )
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollTime(d as Date)
		  dim dTarget as new Date( d )
		  if dTarget.Minute < 30 then
		    dTarget.Minute = 0
		  else
		    dTarget.Minute = 30
		  end if
		  
		  select case mCurrentView
		  case Views.AgendaDay, views.AgendaWeek, views.Agenda
		    dim strExec() as String
		    
		    dim timeTarget as String = DateToTime24( dTarget ) + ":00"
		    strExec.Append( "var scrollArea = window.GSjQuery('#" + me.ControlID + " .fc-scroller');" )
		    strExec.Append( "if (typeof scrollArea !== 'undefined') {" )
		    strExec.Append( "var targetSlat = window.GSjQuery('#" + me.ControlID + " tr[data-time=""" + timeTarget + """]');" )
		    strExec.Append( "if (typeof targetSlat !== 'undefined') {" )
		    strExec.Append( "var targetTop = targetSlat.offset().top - scrollArea.offset().top;" )
		    strExec.Append( "scrollArea.animate({scrollTop: targetTop}, 100);" )
		    strExec.Append( "}}" )
		    
		    Buffer( strExec )
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectDates(StartDate as Date, EndDate as Date = Nil)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('" + me.ControlID + "').fullCalendar('removeEvents'," )
		  strExec.Append( "moment('" + DateToString( StartDate ) + "')" )
		  if not IsNull( EndDate ) then strExec.Append( ",moment('" + DateToString( EndDate ) + "')" )
		  strExec.Append( ");" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function StringToDate(FromString as String) As Date
		  dim dRet as new Date
		  
		  dim strDate as String = NthField( FromString, "T", 1 )
		  dim strTime as String = if( CountFields( FromString, "T" ) = 2, NthField( FromString, "T", 2 ), "00:00:00Z" + Str( dRet.GMTOffset, "#.##" ) )
		  
		  dRet.Year = Val( NthField( strDate, "/", 3 ) )
		  dRet.Month = Val( NthField( strDate, "/", 1 ) )
		  dRet.Day = Val( NthField( strDate, "/", 2 ) )
		  
		  dim strOffset as String = Str( dRet.GMTOffset, "-#.00" )
		  dim dblOffset as Double
		  if CountFields( strTime, "Z" ) > 1 then
		    strOffset = NthField( strTime, "Z", 2 )
		    dblOffset = Floor( Val( strOffset ) ) + ((Val( strOffset ) - Floor( Val( strOffset ) ) ) * 60)
		    dRet.GMTOffset = dblOffset
		    strTime = Left( strTime, len( strTime ) - (len( strOffset ) + 1) )
		  end if
		  dRet.Hour = Val( NthField( strTime, ":", 1 ) )
		  dRet.Minute = Val( NthField( strTime, ":", 2 ) )
		  dRet.Second = Val( NthField( strTime, ":", 3 ) )
		  dRet.GMTOffset = dblOffset
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateEvent(theEvent as GraffitiWebCalendarEvent)
		  if not IsNull( theEvent ) Then
		    dim strExec() as String
		    
		    if theEvent.RepeatFrequency >= theEvent.RepeatDaily then
		      strExec.Append( PrepareEvent( theEvent ) )
		      
		      RemoveEvent( theEvent )
		      
		      Events.Append( theEvent )
		    else
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('clientEvents', function (eventToUpdate) {" )
		      strExec.Append( "if (eventToUpdate.id == '" + Str( theEvent.ID, "#" ) + "') {" )
		      strExec.Append( "eventToUpdate.start = moment('" + me.DateToString( theEvent.StartDate, True ) + "');" )
		      strExec.Append( "eventToUpdate.end = moment('" + me.DateToString( theEvent.EndDate, True ) + "');" )
		      strExec.Append( "eventToUpdate.title = '" + EscapeString( theEvent.Title ) + "';" )
		      strExec.Append( "eventToUpdate.description = '" + EscapeString( theEvent.Description ) + "';" )
		      strExec.Append( "eventToUpdate.location = '" + EscapeString( theEvent.Location ) + "';" )
		      strExec.Append( "eventToUpdate.allDay = " + Lowercase( Str( theEvent.AllDay ) ) + ";" )
		      strExec.Append( "eventToUpdate.backgroundColor = '" + ColorToString( theEvent.BackgroundColor ) + "';" )
		      strExec.Append( "eventToUpdate.borderColor = '" + ColorToString( theEvent.BorderColor ) + "';" )
		      strExec.Append( "eventToUpdate.textColor = '" + ColorToString( theEvent.TextColor ) + "';" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('updateEvent', eventToUpdate);" )
		      strExec.Append( "}" )
		      strExec.Append( "});" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('rerenderEvents');" )
		    
		    Buffer( Join( strExec, "" ) )
		  end if
		  
		  RerenderEvents()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateHeight()
		  if not isShown then Return
		  dim strExec() as String
		  strExec.Append( "var myInner = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "if( typeof(myInner) != 'undefined' ) {" )
		  strExec.Append( "if( typeof(myInner.fullCalendar) == 'function' ) {" )
		  strExec.Append( "var parentPx = myInner.getDefaultPx(2);" )
		  strExec.Append( "var toolbar = window.GSjQuery('#" + me.ControlID + " .fc-toolbar');" )
		  strExec.Append( "if( typeof(toolbar) != 'undefined' ) {" )
		  strExec.Append( "var toolHeight = toolbar.height();" )
		  strExec.Append( "var fullHeight = (myInner.height() - toolHeight);" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('option', 'contentHeight', fullHeight - 5);" )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if not hasLoaded or not LibrariesLoaded then Return
		  
		  dim strExec() as String
		  
		  if FirstLoad then
		    strExec.Append( "window.GSjQuery('head').append('<link rel=\'stylesheet\' id=\'gwcal_" + me.ControlID + "_theme\' href=\'\'>' );" )
		    
		    strExec.Append( "suppLang = [];" )
		    strExec.Append( "window.GSjQuery.each(window.GSjQuery.fullCalendar.locales, function(langCode) {" )
		    strExec.Append( "suppLang.push(langCode);" )
		    strExec.Append( "});" )
		    strExec.Append( "Xojo.triggerServerEvent('" + Me.ControlID + "','LanguagesLoaded', suppLang);" )
		    
		    FirstLoad = False
		  Else
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('destroy');" )
		  end if
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar({" )
		  strExec.Append( GetHeaderOptions() )
		  strExec.Append( "theme: " + Lowercase( Str( CustomTheme ) ) + "," )
		  if CustomTheme then
		    strExec.Append( "themeButtonIcons: {" )
		    strExec.Append( "prev: 'carat-1-w'," )
		    strExec.Append( "next: 'carat-1-e'," )
		    strExec.Append( "prevYear: 'seek-prev'," )
		    strExec.Append( "nextYear: 'seek-next'" )
		    strExec.Append( "}," )
		  end if
		  
		  strExec.Append( "locale: '" + Language + "'," )
		  strExec.Append( "defaultDate: '" + GetDefaultDate() + "'," )
		  strExec.Append( "defaultView: '" + GetViewString( mCurrentView ) + "'," )
		  strExec.Append( "editable: " + Lowercase( Str( Editable ) ) + "," )
		  strExec.Append( "eventLimit: true," )
		  strExec.Append( "eventLimitClick: 'popover'," )
		  strExec.Append( "eventLimitText: 'more'," )
		  strExec.Append( "height: '100%'," )
		  strExec.Append( "selectable: true," )
		  strExec.Append( "selectionHelper: true," )
		  strExec.Append( "eventRenderWait: 250," )
		  
		  strExec.Append( "viewRender: function( view, element ) {" )
		  strExec.Append( "var didChange = true;" )
		  strExec.Append( "if (window.oldView_" + me.ControlID + "_name == view.name && " + _
		  "window.oldView_" + me.ControlID + "_start == view.start.unix()) {" )
		  strExec.Append( "didChange = false;" )
		  strExec.Append( "return;" )
		  strExec.Append( "}" )
		  strExec.Append( "window.oldView_" + me.ControlID + "_name = view.name;" )
		  strExec.Append( "window.oldView_" + me.ControlID + "_start = view.start.unix();" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','ViewChanged', [view.name,view.start.year(),view.start.month() + 1,view.start.date(), view.start.hour(),view.start.minute(),view.start.second(),view.end.year(),view.end.month() + 1,view.end.date(), view.end.hour(),view.end.minute(),view.end.second(),view.start.hasTime(),view.end.hasTime()]);" )
		  strExec.Append( "}," )
		  
		  'strExec.Append( "dayClick: function (date, jsEvent, view) {" )
		  'strExec.Append( "console.log(jsEvent);" )
		  ''strExec.Append( "element.bind('contextmenu', function(e) {" )
		  ''strExec.Append( "e.preventDefault();" )
		  ''strExec.Append( "xojo.triggerServerEvent('" + me.ControlID + "', 'contextClick', [event.id, event.
		  ''strExec.Append( "});" )
		  'strExec.Append( "}," )
		  
		  strExec.Append( "select: function (start, end, jsEvent, view) {" )
		  strExec.Append( "if( typeof(jsEvent) != 'undefined' ) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','Selected', ['month',start.year(),start.month() + 1,start.date(), start.hour(),start.minute(),start.second(),end.year(),end.month() + 1,end.date(), end.hour(),end.minute(),end.second(),start.hasTime(),end.hasTime(),jsEvent.pageX,jsEvent.pageY]);" )
		  strExec.Append( "}}," )
		  
		  strExec.Append( "googleCalendarError: function(e) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'googleCalendarError', [e.domain,e.reason,e.message]);" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventDragStart: function(event,jsevent,ui,view) {" )
		  strExec.Append( "}," )
		  strExec.Append( "eventDragStop: function(event,jsevent,ui,view) {" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventClick: function(data,jsEvent,view) {" )
		  strExec.Append( "window.graffwebcal" + me.ControlID + "clickevent = data;" )
		  strExec.Append( "if( typeof(data.url) !== 'undefined') {" )
		  strExec.Append( "window.open(data.url, 'gcalevent', 'width=700,height=600');" )
		  strExec.Append( "return false;" )
		  strExec.Append( "} else {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','EventClick', [data.id,data.title]);" )
		  strExec.Append( "}" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventMouseover: function( data, jsEvent, view ) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','EventHover', [data.id,data.title,jsEvent.pageX,jsEvent.pageY]);" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ) {" )
		  strExec.Append( "var ret = [event.id,event.allDay,event.start.year(),event.start.month() + 1,event.start.date(),event.start.hasTime()];" )
		  strExec.Append( "if (event.start.hasTime()) {" )
		  strExec.Append( "ret = ret.concat([event.start.hour(),event.start.minute(),event.start.second()]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "ret = ret.concat([null,null,null]);" )
		  strExec.Append( "}" )
		  strExec.Append( "if (event.end != null) {" )
		  strExec.Append( "ret = ret.concat([true,event.end.year(),event.end.month() + 1,event.end.date(),event.end.hasTime()]);" )
		  strExec.Append( "if (event.end.hasTime()) {" )
		  strExec.Append( "ret = ret.concat([event.end.hour(),event.end.minute(),event.end.second()]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "ret = ret.concat([null,null,null]);" )
		  strExec.Append( "}" )
		  strExec.Append( "} else {" )
		  strExec.Append( "ret = ret.concat([false,null,null,null,false,null,null,null]);" )
		  strExec.Append( "}" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','EventDrop', ret);" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('rerenderEvents');" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) {" )
		  strExec.Append( "var ret = [event.id,event.allDay,event.start.year(),event.start.month() + 1,event.start.date(),event.start.hasTime()];" )
		  strExec.Append( "if (event.start.hasTime()) {" )
		  strExec.Append( "ret = ret.concat([event.start.hour(),event.start.minute(),event.start.second()]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "ret = ret.concat([null,null,null]);" )
		  strExec.Append( "}" )
		  strExec.Append( "if (event.end != null) {" )
		  strExec.Append( "ret = ret.concat([true,event.end.year(),event.end.month() + 1,event.end.date(),event.end.hasTime()]);" )
		  strExec.Append( "if (event.end.hasTime()) {" )
		  strExec.Append( "ret = ret.concat([event.end.hour(),event.end.minute(),event.end.second()]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "ret = ret.concat([null,null,null]);" )
		  strExec.Append( "}" )
		  strExec.Append( "} else {" )
		  strExec.Append( "ret = ret.concat([false,null,null,null,false,null,null,null]);" )
		  strExec.Append( "}" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','EventResize', ret);" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('rerenderEvents');" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventMouseout: function( data, jsEvent, view ) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','EventMouseOut', [data.id,data.title]);" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "eventRender: function(event, element, view) {" )
		  strExec.Append( "if (!window.gwcal_" + me.ControlID + "_hidePast) { return true; }" )
		  strExec.Append( "var endDate = moment(event.end);" )
		  strExec.Append( "var nowDate = moment();" )
		  strExec.Append( "if (endDate.diff(nowDate, 'seconds') <= 0) { return false; }" )
		  strExec.Append( "}," )
		  
		  strExec.Append( "});" )
		  
		  dim intMax as Integer = Events.Ubound
		  for intCycle as Integer = 0 to intMax
		    dim newEvent as GraffitiWebCalendarEvent = Events(intCycle)
		    if newEvent.RepeatFrequency <> newEvent.RepeatNever then
		      strExec.Append( PrepareEvent( newEvent ) )
		    else
		      strExec.Append( "var newEvent = " + EventToString( newEvent ) + ";" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('renderEvent', newEvent, true);" )
		    end if
		  next
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  
		  RunBuffer()
		  
		  intMax = ExternalSources.Ubound
		  for intCycle as Integer = 0 to intMax
		    doAddSource( ExternalSources(intCycle) )
		  next
		  
		  if me.CustomThemeUrl.Len > 0 then LoadTheme( CustomThemeURL )
		  
		  UpdateHeight()
		  
		  UpdateRestrictions()
		  
		  MoveToDate( mViewDate )
		  
		  'RerenderEvents()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateRestrictions()
		  dim strExec() as String
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('option', {" )
		  
		  if RestrictView then
		    if not IsNull( TimeMinimum ) and not IsNull( TimeMaximum ) then
		      strExec.Append( "minTime: '" + DateToTime24( TimeMinimum, "00:00" ) + "'," )
		      strExec.Append( "maxTime: '" + DateToTime24( TimeMaximum, "23:59" ) + "'," )
		    else
		      strExec.Append( "minTime: '00:00'," )
		      strExec.Append( "maxTime: '23:59'," )
		    end if
		    
		    if Restrictions.Ubound >= 0 then
		      strExec.Append( RestrictionsToString )
		      if RestrictEventDisplay then
		        strExec.Append( "eventConstraint: 'businessHours'," )
		      else
		        strExec.Append( "eventConstraint: null," )
		      end if
		      if RestrictSelection then
		        strExec.Append( "selectConstraint: 'businessHours'," )
		      else
		        strExec.Append( "selectConstraint: null," )
		      end if
		    end if
		  else
		    strExec.Append( "minTime: '00:00'," )
		    strExec.Append( "maxTime: '23:59'," )
		    
		    strExec.Append( "eventConstraint: null," )
		    strExec.Append( "selectConstraint: null," )
		  end if
		  
		  strExec.Append( "});" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event DateSelected(DateStart as Date, DateEnd as Date, StartTime as Boolean, EndTime as Boolean, mouseX as Integer, mouseY as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event EventChanged(theEvent as GraffitiWebCalendarEvent)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event EventMouseEnter(theEvent as GraffitiWebCalendarEvent, X as Integer, Y as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event EventMouseLeave(theEvent as GraffitiWebCalendarEvent)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event EventSelected(theEvent as GraffitiWebCalendarEvent)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event GoogleCalendarError(Domain as String, Reason as String, Message as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LanguagesLoaded()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ViewChanged(NewView as GraffitiWebCalendar.Views, StartDate as Date, EndDate as Date)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://fullcalendar.io/
		
		Licensed under MIT.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCurrentView
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCurrentView = value
			  
			  'dim oldMinTime as Date = TimeMinimum
			  'dim oldMaxTime as Date = TimeMaximum
			  'dim oldRestrictions() as GraffitiWebCalendarRestriction = Restrictions
			  'oldMinTime = nil
			  'oldMaxTime = nil
			  'ReDim Restrictions(-1)
			  
			  dim oldRestrict as Boolean = mRestrictView
			  RestrictView = False
			  
			  dim strExec() as String = Array( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('changeView','" + _
			  GetViewString( mCurrentView ) + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			  
			  RestrictView = oldRestrict
			End Set
		#tag EndSetter
		CalendarView As Views
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCustomTheme
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCustomTheme = value
			  
			  if LibrariesLoaded then
			    dim strOpt as String = if( value, "addClass", "removeClass" )
			    
			    dim strExec() as String
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('theme'," + Lowercase( Str( value ) ) + ");" )
			    
			    UpdateOptions()
			    
			    if not IsNull( StyleToolbar ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-toolbar')." + strOpt + "('" + StyleToolbar.Name + "');" )
			    if not IsNull( StyleToolbarButtons ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-toolbar button')." + strOpt + "('" + StyleToolbarButtons.Name + "');" )
			    if not IsNull( StyleSundayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-sun')." + strOpt + "('" + StyleSundayHeader.Name + "');" )
			    if not IsNull( StyleMondayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-mon')." + strOpt + "('" + StyleMondayHeader.Name + "');" )
			    if not IsNull( StyleTuesdayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-tue')." + strOpt + "('" + StyleTuesdayHeader.Name + "');" )
			    if not IsNull( StyleWednesdayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-wed')." + strOpt + "('" + StyleWednesdayHeader.Name + "');" )
			    if not IsNull( StyleThursdayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-thu')." + strOpt + "('" + StyleThursdayHeader.Name + "');" )
			    if not IsNull( StyleFridayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-fri')." + strOpt + "('" + StyleFridayHeader.Name + "');" )
			    if not IsNull( StyleSaturdayHeader ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-sat')." + strOpt + "('" + StyleSaturdayHeader.Name + "');" )
			    
			    Buffer( Join( strExec, "" ) )
			  end if
			End Set
		#tag EndSetter
		CustomTheme As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private CustomThemeURL As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mEditable
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEditable = value
			  
			  
			End Set
		#tag EndSetter
		Editable As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private EditShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Events() As GraffitiWebCalendarEvent
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ExternalSources() As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FirstLoad As Boolean = True
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHidePastEvents
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHidePastEvents = value
			  
			  Buffer( "window.gwcal_" + me.ControlID + "_hidePast = " + Str( mHidePastEvents ).Lowercase + ";" )
			  RerenderEvents()
			End Set
		#tag EndSetter
		HidePastEvents As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isInit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLanguage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLanguage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Language As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Languages() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			  
			  UpdateHeight()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLockUpdate = value
			  
			  if not mLockUpdate then UpdateOptions()
			End Set
		#tag EndSetter
		LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCurrentView As Views
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCustomTheme As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEditable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHidePastEvents As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLanguage As String = "en"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRestrictEventDisplay As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRestrictSelection As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRestrictView As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowDayButton As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowMonthButton As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowNavButtons As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowTitle As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowTodayButton As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowWeekButton As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowYearButtons As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleDayFuture As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleDayPast As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleDayToday As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleFridayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleMondayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleSaturdayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleSundayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleThursdayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleToolbarButtons As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleTuesdayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleWednesdayHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTimeMaximum As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTimeMinimum As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolbarStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mViewDate As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private RepeatingBuffer() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mRestrictEventDisplay
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRestrictEventDisplay = value
			  
			  UpdateRestrictions()
			End Set
		#tag EndSetter
		RestrictEventDisplay As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Restrictions() As GraffitiWebCalendarRestriction
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mRestrictSelection
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRestrictSelection = value
			  
			  UpdateRestrictions()
			End Set
		#tag EndSetter
		RestrictSelection As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRestrictView
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRestrictView = value
			  
			  UpdateRestrictions()
			  'UpdateOptions()
			End Set
		#tag EndSetter
		RestrictView As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowDayButton
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowDayButton = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowDayButton As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowMonthButton
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowMonthButton = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowMonthButton As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowNavButtons
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowNavButtons = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowNavButtons As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowTitle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowTitle = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowTitle As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowTodayButton
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowTodayButton = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowTodayButton As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowWeekButton
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowWeekButton = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowWeekButton As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowYearButtons
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowYearButtons = value
			End Set
		#tag EndSetter
		ShowYearButtons As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleDayFuture
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleDayFuture ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-future').removeClass('" + mStyleDayFuture.Name + "');" )
			  mStyleDayFuture = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-Future').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleDayFuture As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleDayPast
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleDayPast ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-past').removeClass('" + mStyleDayPast.Name + "');" )
			  mStyleDayPast = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-past').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleDayPast As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleDayToday
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleDayToday ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-today').removeClass('" + mStyleDayPast.Name + "');" )
			  mStyleDayToday = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-today').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleDayToday As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleFridayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleFridayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-fri').removeClass('" + mStyleFridayHeader.Name + "');" )
			  mStyleFridayHeader = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-fri').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleFridayHeader As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleMondayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleMondayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-mon').removeClass('" + mStyleMondayHeader.Name + "');" )
			  mStyleMondayHeader = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-mon').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleMondayHeader As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleSaturdayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleSaturdayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-sat').removeClass('" + mStyleSaturdayHeader.Name + "');" )
			  mStyleSaturdayHeader = value
			  if not IsNull( value ) Then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-sat').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleSaturdayHeader As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleSundayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleSundayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-sun').removeClass('" + mStyleSundayHeader.Name + "');" )
			  mStyleSundayHeader = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-sun').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleSundayHeader As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleThursdayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleThursdayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-thu').removeClass('" + mStyleThursdayHeader.Name + "');" )
			  mStyleThursdayHeader = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-thu').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleThursdayHeader As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mToolbarStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mToolbarStyle ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-toolbar').removeClass('" + mToolbarStyle.Name + "');" )
			  mToolbarStyle = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-toolbar').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleToolbar As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleToolbarButtons
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleToolbarButtons ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-toolbar button').removeClass('" + mStyleToolbarButtons.Name + "');" )
			  mStyleToolbarButtons = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-toolbar button').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleToolbarButtons As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleTuesdayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleTuesdayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-tue').removeClass('" + mStyleTuesdayHeader.Name + "');" )
			  mStyleTuesdayHeader = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-tue').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleTuesdayHeader As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleWednesdayHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  
			  if not IsNull( mStyleWednesdayHeader ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-wed').removeClass('" + mStyleWednesdayHeader.Name + "');" )
			  mStyleWednesdayHeader = value
			  if not IsNull( value ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + " .fc-wed').addClass('" + value.Name + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		StyleWednesdayHeader As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private SupportedLanguages() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTimeMaximum
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTimeMaximum = value
			  
			  UpdateRestrictions()
			  'UpdateOptions()
			End Set
		#tag EndSetter
		TimeMaximum As Date
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTimeMinimum
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTimeMinimum = value
			  
			  UpdateRestrictions()
			  'UpdateOptions()
			End Set
		#tag EndSetter
		TimeMinimum As Date
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mViewDate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( value ) then
			    mViewDate = value
			  else
			    mViewDate = new date
			  end if
			  
			  dim strExec() as String
			  strExec.Append( "var theMoment = moment('" + me.DateToString( mViewDate ) + "');" )
			  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').fullCalendar('gotoDate', theMoment);" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		ViewDate As Date
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = cssMain, Type = String, Dynamic = False, Default = \".{ContID}{margin:40px 10px;padding:0;}\r.fc-toolbar{margin: 0px !important;}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = cssOld, Type = String, Dynamic = False, Default = \"/*!\r * FullCalendar v2.1.1 Stylesheet\r * Docs & License: http://arshaw.com/fullcalendar/\r * (c) 2013 Adam Shaw\r */.fc{direction:ltr;text-align:left}.fc-rtl{text-align:right}body .fc{font-size:1em}.fc-unthemed .fc-popover\x2C.fc-unthemed .fc-row\x2C.fc-unthemed hr\x2C.fc-unthemed tbody\x2C.fc-unthemed td\x2C.fc-unthemed th\x2C.fc-unthemed thead{border-color:#ddd}.fc-unthemed .fc-popover{background-color:#fff}.fc-unthemed .fc-popover .fc-header\x2C.fc-unthemed hr{background:#eee}.fc-unthemed .fc-popover .fc-header .fc-close{color:#666}.fc-unthemed .fc-today{background:#fcf8e3}.fc-highlight{background:#bce8f1;opacity:.3;filter:alpha(opacity\x3D30)}.fc-icon{display:inline-block;font-size:2em;line-height:.5em;height:.5em;font-family:\"Courier New\"\x2CCourier\x2Cmonospace}.fc-icon-left-single-arrow:after{content:\"\\02039\";font-weight:700}.fc-icon-right-single-arrow:after{content:\"\\0203A\";font-weight:700}.fc-icon-left-double-arrow:after{content:\"\\000AB\"}.fc-icon-right-double-arrow:after{content:\"\\000BB\"}.fc-icon-x:after{content:\"\\000D7\"}.fc button{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;margin:0;height:2.1em;padding:0 .6em;font-size:1em;white-space:nowrap;cursor:pointer}.fc button::-moz-focus-inner{margin:0;padding:0}.fc-state-default{border:1px solid}.fc-state-default.fc-corner-left{border-top-left-radius:4px;border-bottom-left-radius:4px}.fc-state-default.fc-corner-right{border-top-right-radius:4px;border-bottom-right-radius:4px}.fc button .fc-icon{position:relative;top:.05em;margin:0 .1em}.fc-state-default{background-color:#f5f5f5;background-image:-moz-linear-gradient(top\x2C#fff\x2C#e6e6e6);background-image:-webkit-gradient(linear\x2C0 0\x2C0 100%\x2Cfrom(#fff)\x2Cto(#e6e6e6));background-image:-webkit-linear-gradient(top\x2C#fff\x2C#e6e6e6);background-image:-o-linear-gradient(top\x2C#fff\x2C#e6e6e6);background-image:linear-gradient(to bottom\x2C#fff\x2C#e6e6e6);background-repeat:repeat-x;border-color:#e6e6e6 #e6e6e6 #bfbfbf;border-color:rgba(0\x2C0\x2C0\x2C.1) rgba(0\x2C0\x2C0\x2C.1) rgba(0\x2C0\x2C0\x2C.25);color:#333;text-shadow:0 1px 1px rgba(255\x2C255\x2C255\x2C.75);box-shadow:inset 0 1px 0 rgba(255\x2C255\x2C255\x2C.2)\x2C0 1px 2px rgba(0\x2C0\x2C0\x2C.05)}.fc-state-active\x2C.fc-state-disabled\x2C.fc-state-down\x2C.fc-state-hover{color:#333;background-color:#e6e6e6}.fc-state-hover{color:#333;text-decoration:none;background-position:0 -15px;-webkit-transition:background-position .1s linear;-moz-transition:background-position .1s linear;-o-transition:background-position .1s linear;transition:background-position .1s linear}.fc-state-active\x2C.fc-state-down{background-color:#ccc;background-image:none;box-shadow:inset 0 2px 4px rgba(0\x2C0\x2C0\x2C.15)\x2C0 1px 2px rgba(0\x2C0\x2C0\x2C.05)}.fc-state-disabled{cursor:default;background-image:none;opacity:.65;filter:alpha(opacity\x3D65);box-shadow:none}.fc-button-group{display:inline-block}.fc .fc-button-group>*{float:left;margin:0 0 0 -1px}.fc .fc-button-group>:first-child{margin-left:0}.fc-popover{position:absolute;box-shadow:0 2px 6px rgba(0\x2C0\x2C0\x2C.15)}.fc-popover .fc-header{padding:2px 4px}.fc-popover .fc-header .fc-title{margin:0 2px}.fc-popover .fc-header .fc-close{cursor:pointer}.fc-ltr .fc-popover .fc-header .fc-title\x2C.fc-rtl .fc-popover .fc-header .fc-close{float:left}.fc-ltr .fc-popover .fc-header .fc-close\x2C.fc-rtl .fc-popover .fc-header .fc-title{float:right}.fc-unthemed .fc-popover{border-width:1px;border-style:solid}.fc-unthemed .fc-popover .fc-header .fc-close{font-size:25px;margin-top:4px}.fc-popover>.ui-widget-header+.ui-widget-content{border-top:0}.fc hr{height:0;margin:0;padding:0 0 2px;border-style:solid;border-width:1px 0}.fc-clear{clear:both}.fc-bg\x2C.fc-helper-skeleton\x2C.fc-highlight-skeleton{position:absolute;top:0;left:0;right:0}.fc-bg{bottom:0}.fc-bg table{height:100%}.fc table{width:100%;table-layout:fixed;border-collapse:collapse;border-spacing:0;font-size:1em}.fc th{text-align:center}.fc td\x2C.fc th{border-style:solid;border-width:1px;padding:0;vertical-align:top}.fc td.fc-today{border-style:double}.fc .fc-row{border-style:solid;border-width:0}.fc-row table{border-left:0 hidden transparent;border-right:0 hidden transparent;border-bottom:0 hidden transparent}.fc-row:first-child table{border-top:0 hidden transparent}.fc-row{position:relative}.fc-row .fc-bg{z-index:1}.fc-row .fc-highlight-skeleton{z-index:2;bottom:0}.fc-row .fc-highlight-skeleton table{height:100%}.fc-row .fc-highlight-skeleton td{border-color:transparent}.fc-row .fc-content-skeleton{position:relative;z-index:3;padding-bottom:2px}.fc-row .fc-helper-skeleton{z-index:4}.fc-row .fc-content-skeleton td\x2C.fc-row .fc-helper-skeleton td{background:0 0;border-color:transparent;border-bottom:0}.fc-row .fc-content-skeleton tbody td\x2C.fc-row .fc-helper-skeleton tbody td{border-top:0}.fc-scroller{overflow-y:scroll;overflow-x:hidden}.fc-scroller>*{position:relative;width:100%;overflow:hidden}.fc-event{position:relative;display:block;font-size:.85em;line-height:1.3;border-radius:3px;border:1px solid #3a87ad;background-color:#3a87ad;font-weight:400}.fc-event\x2C.fc-event:hover\x2C.ui-widget .fc-event{color:#fff;text-decoration:none}.fc-event.fc-draggable\x2C.fc-event[href]{cursor:pointer}.fc-day-grid-event{margin:1px 2px 0;padding:0 1px}.fc-ltr .fc-day-grid-event.fc-not-start\x2C.fc-rtl .fc-day-grid-event.fc-not-end{margin-left:0;border-left-width:0;padding-left:1px;border-top-left-radius:0;border-bottom-left-radius:0}.fc-ltr .fc-day-grid-event.fc-not-end\x2C.fc-rtl .fc-day-grid-event.fc-not-start{margin-right:0;border-right-width:0;padding-right:1px;border-top-right-radius:0;border-bottom-right-radius:0}.fc-day-grid-event>.fc-content{white-space:nowrap;overflow:hidden}.fc-day-grid-event .fc-time{font-weight:700}.fc-day-grid-event .fc-resizer{position:absolute;top:0;bottom:0;width:7px}.fc-ltr .fc-day-grid-event .fc-resizer{right:-3px;cursor:e-resize}.fc-rtl .fc-day-grid-event .fc-resizer{left:-3px;cursor:w-resize}a.fc-more{margin:1px 3px;font-size:.85em;cursor:pointer;text-decoration:none}a.fc-more:hover{text-decoration:underline}.fc-limited{display:none}.fc-day-grid .fc-row{z-index:1}.fc-more-popover{z-index:2;width:220px}.fc-more-popover .fc-event-container{padding:10px}.fc-toolbar{text-align:center;margin-bottom:1em}.fc-toolbar .fc-left{float:left}.fc-toolbar .fc-right{float:right}.fc-toolbar .fc-center{display:inline-block}.fc .fc-toolbar>*>*{float:left;margin-left:.75em}.fc .fc-toolbar>*>:first-child{margin-left:0}.fc-toolbar h2{margin:0}.fc-toolbar button{position:relative}.fc-toolbar .fc-state-hover\x2C.fc-toolbar .ui-state-hover{z-index:2}.fc-toolbar .fc-state-down{z-index:3}.fc-toolbar .fc-state-active\x2C.fc-toolbar .ui-state-active{z-index:4}.fc-toolbar button:focus{z-index:5}.fc-view-container *\x2C.fc-view-container :after\x2C.fc-view-container :before{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}.fc-view\x2C.fc-view>table{position:relative;z-index:1}.fc-basicDay-view .fc-content-skeleton\x2C.fc-basicWeek-view .fc-content-skeleton{padding-top:1px;padding-bottom:1em}.fc-basic-view tbody .fc-row{min-height:1em}.fc-row.fc-rigid{overflow:hidden}.fc-row.fc-rigid .fc-content-skeleton{position:absolute;top:0;left:0;right:0}.fc-basic-view .fc-day-number\x2C.fc-basic-view .fc-week-number{padding:0 2px}.fc-basic-view td.fc-day-number\x2C.fc-basic-view td.fc-week-number span{padding-top:2px;padding-bottom:2px}.fc-basic-view .fc-week-number{text-align:center}.fc-basic-view .fc-week-number span{display:inline-block;min-width:1.25em}.fc-ltr .fc-basic-view .fc-day-number{text-align:right}.fc-rtl .fc-basic-view .fc-day-number{text-align:left}.fc-day-number.fc-other-month{opacity:.3;filter:alpha(opacity\x3D30)}.fc-agenda-view .fc-day-grid{position:relative;z-index:2}.fc-agenda-view .fc-day-grid .fc-row{min-height:3em}.fc-agenda-view .fc-day-grid .fc-row .fc-content-skeleton{padding-top:1px;padding-bottom:1em}.fc .fc-axis{vertical-align:middle;padding:0 4px;white-space:nowrap}.fc-ltr .fc-axis{text-align:right}.fc-rtl .fc-axis{text-align:left}.ui-widget td.fc-axis{font-weight:400}.fc-time-grid\x2C.fc-time-grid-container{position:relative;z-index:1}.fc-time-grid{min-height:100%}.fc-time-grid table{border:0 hidden transparent}.fc-time-grid>.fc-bg{z-index:1}.fc-time-grid .fc-slats\x2C.fc-time-grid>hr{position:relative;z-index:2}.fc-time-grid .fc-highlight-skeleton{z-index:3}.fc-time-grid .fc-content-skeleton{position:absolute;z-index:4;top:0;left:0;right:0}.fc-time-grid>.fc-helper-skeleton{z-index:5}.fc-slats td{height:1.5em;border-bottom:0}.fc-slats .fc-minor td{border-top-style:dotted}.fc-slats .ui-widget-content{background:0 0}.fc-time-grid .fc-highlight-container{position:relative}.fc-time-grid .fc-highlight{position:absolute;left:0;right:0}.fc-time-grid .fc-event-container{position:relative}.fc-ltr .fc-time-grid .fc-event-container{margin:0 2.5% 0 2px}.fc-rtl .fc-time-grid .fc-event-container{margin:0 2px 0 2.5%}.fc-time-grid .fc-event{position:absolute;z-index:1}.fc-time-grid-event.fc-not-start{border-top-width:0;padding-top:1px;border-top-left-radius:0;border-top-right-radius:0}.fc-time-grid-event.fc-not-end{border-bottom-width:0;padding-bottom:1px;border-bottom-left-radius:0;border-bottom-right-radius:0}.fc-time-grid-event{overflow:hidden}.fc-time-grid-event>.fc-content{position:relative;z-index:2}.fc-time-grid-event .fc-time\x2C.fc-time-grid-event .fc-title{padding:0 1px}.fc-time-grid-event .fc-time{font-size:.85em;white-space:nowrap}.fc-time-grid-event .fc-bg{z-index:1;background:#fff;opacity:.25;filter:alpha(opacity\x3D25)}.fc-time-grid-event.fc-short .fc-content{white-space:nowrap}.fc-time-grid-event.fc-short .fc-time\x2C.fc-time-grid-event.fc-short .fc-title{display:inline-block;vertical-align:top}.fc-time-grid-event.fc-short .fc-time span{display:none}.fc-time-grid-event.fc-short .fc-time:before{content:attr(data-start)}.fc-time-grid-event.fc-short .fc-time:after{content:\"\\000A0-\\000A0\"}.fc-time-grid-event.fc-short .fc-title{font-size:.85em;padding:0}.fc-time-grid-event .fc-resizer{position:absolute;z-index:3;left:0;right:0;bottom:0;height:8px;overflow:hidden;line-height:8px;font-size:11px;font-family:monospace;text-align:center;cursor:s-resize}.fc-time-grid-event .fc-resizer:after{content:\"\x3D\"}\r", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.calendar", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsRepeatSource, Type = String, Dynamic = False, Default = \"    window.GSjQuery(\'#{ContID}\').fullCalendar( \'addEventSource\'\x2C function (start\x2C end\x2C timezone\x2C callback) {\r        var thisView \x3D window.GSjQuery(\'#{ContID}\').fullCalendar(\'getView\');\r        var viewStart \x3D thisView.intervalStart;\r        var viewEnd \x3D thisView.intervalEnd;\r        var eventStart \x3D moment(\'{StartYear}-{StartMonth}-{StartDay}\'\x2C \'YYYY-MM-DD\');\r        var eventEnd \x3D moment(\'{EndYear}-{EndMonth}-{EndDay}\'\x2C \'YYYY-MM-DD\');\r        var moment_i \x3D moment(eventStart);\r        var moment_f \x3D moment(eventEnd);\r        var events \x3D [];\r        var repCount \x3D 0;\r        while (moment_i <\x3D viewEnd) {\r            if (moment_i >\x3D eventStart && moment_i <\x3D eventEnd) {\r                var moment_ti \x3D moment_i.clone();\r                var moment_tf \x3D moment_i.clone();\r                moment_ti.set(\'hour\'\x2C {StartHour});\r                moment_ti.set(\'minutes\'\x2C {StartMin});\r                moment_tf.set(\'hour\'\x2C {EndHour});\r                moment_tf.set(\'minutes\'\x2C {EndMin});\r                events.push({\r                    id: \'{RepeatID}\'\x2C\r                    title: \'{Title}\'\x2C\r                    start: moment_ti.format()\x2C\r                    end: moment_tf.format()\x2C\r                    allDay: false\x2C\r                    color: {Color}\x2C\r                    backgroundColor: {BGColor}\x2C\r                    borderColor: {BorderColor}\x2C\r                    textColor: {TextColor}\x2C\r                    description: \'{Description}\'\x2C\r                    location: \'{Location}\'\r                });\r            }\r            moment_i.add({StepCount}\x2C \'{RepStep}\');\r            repCount +\x3D 1;\r        }\r        callback(events);\r    });", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAdTSURBVHhe7d1paFxVFAfwe27edDKTVoVqW6t1wyWKgrVVEEVRP7i2mbihqFSliFCXzhQrKK74wYVmCnVBROrS1lKVJgFBEK0iUty+qGgrKFYQFEFRMjOhk3nH8+JpTEwmTd4yL+39/yB999y0Ny/v/d+WuZMaAAAAAAAAAAAAAAA4qJEuZ5TZy9ec1rBDncZ484zvz9JuYyzVZZV/a/PNroH+dd9qb+raC6XzDfHR1DDztGsYkakYa3+1tb2fDby74XftnlFmVADyy4u3saWirNQZ2tUUG/M1+Vyu9pc3aldrXXZ3Npf1HpK9vFLWd772NsVsPrDMT1T6yzu0a0aYEQHIXVk8ymTMq2ToEu2aMjb8vqmbFbV3yr9oV+LyXaVL5Yh/STbfIu2aMgnCM7W+nrVapi71AAQ7nzzziRxJx2rX9DHv4SFzXitC0N5dvMEyvaFlKBKCTRKCW7RMldVlejx6LdLODwT/PhgnYfnCmiXWj/515N7g5lxX8VEtU5VqAHLdpVWyMS7WMpJgnGA8LZPB/ho5Z2a0ioSIHplVKJ2sZWpSDQD5pqTNWMQ93mjtV685VvbajVrGwjMm2cBOQWoByHevPkuOphO0nJDc6f8mi5/3fcgN369Bf1My3vC4CZAnjiu0OZmRdQ0+mPmvoHMSUxkzUandBHYsX30TW7tJy3FkZ79S6y3fpuWIXKG4UZ4WbtVyHGK+qdJX3qJlbPKF4rMyepMjlityLF1Y7V33pXaMkCeGDbKV79JynGpmcI558/kBLVsutTNAw9rmz85yVz/Rzg8M98vntRynQbRAmzGbZFymjRPt/AB5Vh4Xm8sOZhNa36lJLQCWuenNFBMNanNCk33eGs5rM14cXLInxsRVbY5D7O/V5oQ8a3LaTEWqN4FJkPuG/350HCfiRC6XbNPdBwddAERWl7FipnZtHlRSDAAlc6Qm9D3J4Z9IsNKWWgDkOn5gHVF0cAYg9utarrC6y/h0FVk6U3bzQjl1ZuSrZOTxTJbkybk0WE4aPLmO76719nRqOU6uUNolK36KlqmTR9an5enkfi3HmH11qdP3zXdaToKH5Kioy1h7ZXvV5fsLbh4bMvgeufvYYcluq2xf982/fzc+sZ0BOpbdM1+elfvI2F7Z+Sula6nkayGROUK+mcNkp3dIX3Z/O99dcnCQyRHRoWTo8GDbycci2V7nS/0QM3+d7yqu178cm1h2RnZZ8US2bZ/KCi/XLkgC0b35rtI2rWIRSwDaLG2WlYv2ih5MDZnrcoXiU1pFFjkA+cLqO2SlztESWkAuCWuzXfcep2UkMZwB7FXagBbyjHeNNiOJHgA2i7UFLeQblqes6KIHgMzR2oIWkierOdqMJJabQDhwIQBJm/S1wPQhAI6bqQE4UpfN7O/zrcXmKG2N0/D4EG3OSJFfC8gXSqzNeDG/QB4/Vx81oSLDmSyzf6esduqTKf+PmVc12hrvaTmsbcgeaojeJaK52hUfNn3Vvp6CVqHN3ADA5GIKAO4BHIcAOA4BcBwC4DgEwHEIgOMQAMchAI5L9gdBzCurVbtVq2lr7/C7raHXtQxeA79lsGK3axlaLuc/TJb++zUtDXt5dZA/1iq0fIf/hmzSZVoaJm9RbaDxp5bTlsv7zxHRCi3HOhB+Ekiywyq95abvAN6fXHfpOmIzMgmSyVxf297zppah5bqKT8iGfVDL4F1fF1X61n2oZWj5QrF/dAA8j+f+/Vb5Dy2nTdbzlaQDgEuA4xAAxyEAjkMAHIcAOA4BcBwC4DgEwHEIgOMQAMchAI5DAByHADgOAXBcwvMBzEfaCo/MhdqKZ7xhfLwhOkaL2MZlw6ePeRdQxHFlvE4Zb+LfqYx3BjkO8wEgDgiA45K9BJBdUfXO2azVtLU3Pr/W+o2ROYW+bbthsO3st7QMLVf/9HEy/ICWcjuQuaTiLYl8H5Cv79wuo41MCbNedt4ALQ4/JWzvzpcxJxBzAjEnEJKDADgOAXAcAuA4BMBxCIDjEADHIQCOQwAchwA4DgFwHALgOATAcQiA4xAAxyEAjkt0Qggb/laboZGh07QZy3j7JDTuIhl35D9zimHchTLeYdoeC7OCHYcZQRAHBMBxmBQqMCkUnIUAOA4BcBwC4DgEwHEIgOMQAMchAI5DAByHADgOAXAcAuA4BMBxCIDjEADHxTAfoFiTYdq1HIvNj9oKj8wJ2opnvH2SGJd4gfyR16r5uKO/dnhvV3t7rtV2aNED0FX83hCdpCW0CBuzsdbbc7uWoUW+BLChndqEFiKmHdqMJHIA5BSyRZvQKsx7qmfODv0LOEdr02Vo9d07f8iccu4CScJS7YKEyUG3qv7ik19pGUnkAAQkBO9ICBbLmnVqFySEmR+r9pU3aBlZLAEISAi2Sgh8aV4gQcDjZcxkx39PbO6r9ZfXa1csIj8F/N+cwqq5Q/6sC4j4VHk6kMciCI2J2dJP7De+GOxb/7H2AgAAAAAAAAAAAAAAAEyFMf8ADIg4b5ngA0cAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAFtSURBVDhPxVKxTgJBEJ05CAqVFY2xv5PW5I7KTqmEBq3s/AAOP8D4A5y9vYXG5KwtjBX3A0RMtEI7Gy3ABHPrvHE5ICQmRhNfMjfzXt7s7e4s/TlKjdYWwlIF+HKjtWvpHHI2K2A0xBER1wpu8DDuJ4/FndYGE58xOeuirYzvk661K9hmKtbbx0JcYvOsguFVrZGJ1qROpA4M0fXoqnOkHkG2g4JXvZHlKmLoCV1i5n1ZvwpN+JNkCW4y02bO9e8++gl85OAzC9nuAcLSLzBV5RNaNofpAoa6w7jDKaV7CNQiRsgTXTXx2Q7Fwg5+iuwO8l7QzrvBCxunIkco5z2/LLWf83xH9Ap0aHKcbbHfLtwBG359j0/ODac9BGpMAXmiI8NnWxTZGEv1cCDswo4NK2KEgY4PyMZKzZTNof5AMHMHPBjGUTvl9BKBGs3IE1018dkGxa/vYPoSG+GpnA+P5lsYNm+jOKpZ+u8g+gTHvqwrTdq77wAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowStyleProperty, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Views, Type = Integer, Flags = &h0
		Month
		  Agenda
		  AgendaWeek
		  AgendaDay
		  Basic
		  BasicWeek
		  BasicDay
		  List
		  ListYear
		  ListMonth
		  ListWeek
		ListDay
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="CalendarView"
			Visible=true
			Group="Settings"
			Type="Views"
			EditorType="Enum"
			#tag EnumValues
				"0 - Month"
				"1 - Agenda"
				"2 - AgendaWeek"
				"3 - AgendaDay"
				"4 - Basic"
				"5 - BasicWeek"
				"6 - BasicDay"
				"7 - List"
				"8 - ListYear"
				"9 - ListMonth"
				"10 - ListWeek"
				"11 - ListDay"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Group="Behavior"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CustomTheme"
			Visible=true
			Group="Settings"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Editable"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="200"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Language"
			Visible=true
			Group="Behavior"
			InitialValue="en"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockUpdate"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RestrictView"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowDayButton"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowMonthButton"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowNavButtons"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowTitle"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowTodayButton"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowWeekButton"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowYearButtons"
			Visible=true
			Group="Settings"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="200"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HidePastEvents"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RestrictEventDisplay"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RestrictSelection"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
