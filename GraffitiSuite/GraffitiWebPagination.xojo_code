#tag Class
Protected Class GraffitiWebPagination
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "pageClick"
		      if Parameters.Ubound >= 0 then
		        mCurrentPage = Parameters(0).IntegerValue
		        PageChanged()
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		  
		  select case Name
		  case "Style"
		    UpdateOptions()
		    Return True
		  case "Enabled"
		    if isInit and LibrariesLoaded and isShown then
		      
		      dim newVal as Boolean = Value
		      Buffer( "window.GSjQuery('#" + me.ControlID + "_inner').twbsPagination('" + if( newVal, "enable", "disable" ) + "');" )
		      
		      Return True
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  if not IsLibraryRegistered( Session, JavascriptNamespace, "graffitiPaginationCSS", 1 ) then
		    source.Append( "<style>" + cssDisplay + "</style>" )
		    RegisterLibrary( Session, JavascriptNamespace, "graffitiPaginationCSS" )
		  end if
		  
		  source.Append( "<div id='" + me.ControlID + "' style='display:none;' class='graffitiWebPagination" + if(not IsNull( me.Style ), " " + me.Style.Name, "" ) + "'>" )
		  source.Append( "<ul id='" + me.ControlID + "_inner' class='pagination-sm justify-content-center' style='display:flex !important;'></ul>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebPagination -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "jquery.twbsPagination.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebPagination -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    strExec.Append( "var myElement = window.GSjQuery('#" + me.ControlID + "_inner');" )
		    
		    if isInit then
		      strExec.Append( "myElement.twbsPagination('destroy');" )
		    end if
		    
		    strExec.Append( "myElement.twbsPagination({" )
		    strExec.Append( "startPage: " + Str( mCurrentPage ) + "," )
		    strExec.Append( "totalPages: " + Str( mTotalPages ) + "," )
		    strExec.Append( "visiblePages: " + Str( mVisiblePages ) + "," )
		    strExec.Append( "first: '" + EscapeString( mTemplateFirstPage ) + "'," )
		    strExec.Append( "prev: '" + EscapeString( mTemplatePrevPage ) + "'," )
		    strExec.Append( "next: '" + EscapeString( mTemplateNextPage ) + "'," )
		    strExec.Append( "last: '" + EscapeString( mTemplateLastPage ) + "'," )
		    
		    if IsNull( me.Style ) or me.Style.Name = "_DefaultStyle" then
		      strExec.Append( "paginationClass: 'pagination'," )
		    else
		      strExec.Append( "paginationClass: '" + me.Style.Name + "'," )
		    end if
		    if not IsNull( mStyleButtonNext ) then strExec.Append( "nextClass: '" + mStyleButtonNext.Name + "'," )
		    if not IsNull( mStyleButtonPrev ) then strExec.Append( "prevClass: '" + mStyleButtonPrev.Name + "'," )
		    if not IsNull( mStyleButtonLast ) then strExec.Append( "lastClass: '" + mStyleButtonLast.Name + "'," )
		    if not IsNull( mStyleButtonFirst ) then strExec.Append( "firstClass: '" + mStyleButtonFirst.Name + "'," )
		    if not IsNull( mStyleButtonPage ) then strExec.Append( "pageClass: '" + mStyleButtonPage.Name + "'," )
		    if not IsNull( mStyleButtonActive ) then strExec.Append( "activeClass: '" + mStyleButtonActive.Name + "'," )
		    if not IsNull( mStyleButtonDisabled ) then strExec.Append( "disabledClass: '" + mStyleButtonDisabled.Name + "'," )
		    if not IsNull( mStyleButtonText ) then strExec.Append( "anchorClass: '" + mStyleButtonText.Name + "'," )
		    
		    strExec.Append( "onPageClick: function (event, page) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'pageClick', [page]);" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "myElement.twbsPagination('" + if( me.Enabled, "enable", "disable" ) + "');" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		    
		    isInit = True
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PageChanged()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/esimakin/twbs-pagination
		
		Licensed under Apache2
		
		Copyright 2014-2015 &copy; Eugene Simakin
		
		Licensed under the Apache License, Version 2.0 (the "License"); 
		you may not use this file except in compliance with the License. 
		You may obtain a copy of the License at
		
		http://www.apache.org/licenses/LICENSE-2.0
		
		Unless required by applicable law or agreed to in writing, 
		software distributed under the License is distributed on an "AS IS" BASIS, 
		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
		See the License for the specific language governing permissions and 
		limitations under the License.
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCurrentPage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if value <= 0 then value = 1
			  if value > mTotalPages then value = mTotalPages
			  
			  mCurrentPage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		CurrentPage As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isInit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCurrentPage As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonActive As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonDisabled As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonFirst As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonLast As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonNext As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonPage As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonPrev As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleButtonText As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTemplateFirstPage As String = "<i class=""fas fa-step-backward""></i>"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTemplateLastPage As String = "<i class=""fas fa-step-forward""></i>"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTemplateNextPage As String = "<i class=""fas fa-caret-right""></i>"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTemplatePrevPage As String = "<i class=""fas fa-caret-left""></i>"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTotalPages As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisiblePages As Integer = 10
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonActive
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonActive = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonActive As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonDisabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonDisabled = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonDisabled As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonFirst
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonFirst = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonFirst As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonLast
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonLast = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonLast As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonNext
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonNext = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonNext As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonPage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonPage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonPage As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonPrev
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonPrev = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonPrev As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleButtonText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleButtonText = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleButtonText As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTemplateFirstPage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTemplateFirstPage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TemplateFirstPage As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTemplateLastPage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTemplateLastPage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TemplateLastPage As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTemplateNextPage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTemplateNextPage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TemplateNextPage As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTemplatePrevPage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTemplatePrevPage = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TemplatePrevPage As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTotalPages
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if value <= 0 then value = 1
			  if mCurrentPage > value then mCurrentPage = value
			  
			  mTotalPages = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TotalPages As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mVisiblePages
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if value <= 0 then value = 1
			  if value > TotalPages then value = TotalPages
			  
			  mVisiblePages = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		VisiblePages As Integer
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = cssDisplay, Type = String, Dynamic = False, Default = \".graffitiWebPagination {\r  overflow-x: hidden;\r  overflow-y: hidden;\r}\r\r.graffitiWebPagination:hover {\r  overflow-x: auto !important;\r}\r\r.pagination {\r  display: table !important;\r}\r.pagination>li {\r  display: table-cell !important;\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.pagination", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAMzElEQVR4nO2dT4hkVxXGf6domqY7hCHIMAwSBg0SVGIQM4iKBBcSImZ6JXEVca2LdDOL4CJIkCBSI4J/UBFcRd2EGuNKQsgihCxiUBxD1JBFCMMgYQjDVDMMTR0X790659z3qt6rnoDgvR80VfXqvXu/e+93/t33akYUpaJcTP7XBCr+t6gCKBxVAIWjCqBwVAEUjiqAwlEFUDiqAApHFUDhqAIoHFUAhaMKoHBUARSOKoDCUQVQOKoACkcVQOGoAigcVQCFowqgcFQBFI4qgMJRBVA4qgAKRxVA4agCKBxVAIWjCqBwVAEUjiqAwlEFUDiqAApHFUDhqAIoHFUAhaMKoHBUARSOKoDCUQVQOKoACkcVQOGoAigcVQCFowqgcFQBFI4qgMJRBVA4tk5y0d7+wTbINpmAVBURARRVQQRUQSR97hxfgBwDt+ez6eJDGE/L73ACbNOM705FvgC9PZ9dun3nzAx7+wdbINuqutXM2UmggAB6DHLrJHMom/yHEXv7h6eA+4HPq+qngHtAtpoFbdrpG0zzlfZ8pzdA/g28BrwJenU+u3RHQtjbP7wXeAD4LPBx4JTnkbjmXBr+kvM8Bv6jyj9EeAO4Mp9Nb9wZv4MdVT4G8qAID6lyVoSdfI6Ma3NdotT9rEcicg30LyCvA2/PZ9PjsXxGC2Bv//B+4AlVHhfhXGPliUyy+uZ9mmAvCn/MBqntQOQ68Gfgl8Cr89l0Y2vb2z+YqMrDInxXVb8Ccrdf475F3xDvADPgJ/PZ9N2TNLC3f3AK5HHgCRqBbp+UTM94FsArwG+AP85n0w/GtDNKALsXDu8HnhHhUWDXqzV/75XauHwyVZulpXDQimcBXAG5KMKLm7qzvf3Dx4DvAw/641FwwXLCBA59bnEM+jzIxU1FsLd/uAU8A/ptkNPr+02WLr188/eQvIUgwlXgx8Cvxnirwfi4e+HwIyI8CTymqruJoJE2MpFsGoANLh3z7rb9BhGZiPCACD8Dzgzx8tjbPzgPPAX6QP5dLr6+cORF2ohRiWu/NJItkH1V/d7uhYO7NuGoqt8AvgNyWp3NeU9pn5vXVWJNHje9bz6n3ErPqupFVX1098LBYI43KAAR/ZIq3wLd7oubfoJ7hr2iTRNMGnwSharep8rTbSI3iL39w7tBnlDlfBxPareXQeDWeCV/rrR/eTsKsC3CYyLs7+0fjOX4UeBp4K44J4olcnke5c+zEGvXSfa5eW2N87QIT4pw7xC3tQPYvXAwAfkmLlZ1RZAPxqu6ebVF9telgcvyGuuDrwJnh8i37XwC9LwIk9yrtG25V3GW7ceRkr/8HNoxyPK79tgZkC+CjPICqjwCnPaL3RzPhdbl77lHTkm4vk3fHp8D+fQQt7UCEJEJcD65fO03J3wOkBbcJ3z2nuU5SeUpB4DgUXZBB8m3HM+AnDNuJrrVfI23a8mOZpfl4aD1BueA04yACJ8Rkd1m7Lkoc3FKr8HkPEwU0Xs4TEAfaox4NYZixIRlPLYFTIsW3b9m1qXZq08S7XjyBMkNt8e3VWXU5AK7wClzmz4exhO7iZ0X5jKGBsH6Cc4W6pSqjs0DTgO99b4XhOfoBZ2PAfIS0eY3emHODhU+a9XRdrCT3psrBG9piYCPS3kdawKKltp4CB8Olq57bIm01fzlMdJ4NDz9RHdjfhKgVTbSumgSH0wMTb8iMnYjbVtVJ5Fb8lA+BzLOideqPCol2N6L+L8WO0PEBkJAVFk3WTLCRiSWNJ6wz65jSCB8154xxD3004gzJpcWXmwX0sdMX82k72JszfnbvPTNwxqGnWQ5Vh6uF7FQYIaRW7sfu3lgE27qc5jgiCogWm1zzOp72wSKSrbvTCCWJ6T3EoQTY9+A74osl+/MU3nr8VYXKw8bW0zQfO6Szm3G3a0exvBbtYB98J7BHe051kmeR+Q9EYMurG8Dwtx7H6G+WOsFE8OCt4R8P2Escs9hydTyjN52u7V1WtQ4Xru2WyGcjCNuHuPYfdveK/oQ1B2vD53RA99RDpA6i1u6Zq3JSroLGycwWlNilJeMPoZvCm+VcTLME8TwFSuF3PNEYfrrUn+bWlrkmW82RR5WIcWcys6LiewqdPcTuhhIAlP8zJOhvljvY1dfNtu1sthWn8seg+jWjU9XuJ6/JVE+2TMhRA+nYTJ9crgZLAeJiytBuD6B9h7A5zmeb7omfu4TSxdD+wDhc8qmU+c+rq2qu2PClCcqeYJD+DwOvgLxVYm1HznGvvz7/GaVF0vDKyaPmySBfrGbV59TxBwqX3zLa1iek3h5L+bbTuMZwgb3yn3Jlgh5i+mrvfOESTJF23n+ujHEu9x87E8T0MdjRQsreEdekIeNTZF7PZ8s2/H+a9yRnkU+mVdamwTmO3rRuvJ4bm7cx884Wabcxor67hbmbQ8hr1C6/K3tdfHSv9qkdpO3kbTiVZkY+xY5zmXO1SenKRwMX/uhhID199Gjq/J3CjVTqU/M7FprJ5FNMX0tb88yxOvkLn3czmP66jIwJondVy/0rjjGYNW48lI7D6cxGTRDiXMVw9wYIxoMAXlcSQlX33ZpHEC0OIubeS2dJ5jJOwxydxx9gglpoXxy2o3tccIjh26yG8d54nIls057zZO7rgdIoo79W+hj+f34/Ylxt4OXk5esN5Hwbj6v/f2Wqp1jQogWmhasHcbG89ufTPr7A91yM1+I2EbXevymkLU1Fvn5PinsVjDdG29pt9MsPJ8z805WFQ3zG7ERBNEt9e/wtWcTH/r0LXUntGkjWZiPV2nhxiFe2+2rWxl0ra67edXbU3tdumZ8ntJv0eZZ8pLNb7j5PqMQc0/c3/c6jPAAeTz333U7iVl+PN6/t93nXnWDybVr8z5zwcbSzU9MDEl91ufH1jcX62AWG69Pffv2fP8xV8qTvphXeW8QK4s7SAJbd7nwlmnuJR+gubNYp/ozfa3eX1K5knLUM4GqugAWeRjKLToKziYv8erLCxK3/kpAFjCOowgLL4K4QyeOkyXSXaPrWnKseJKB+WRWBp8OHhCALES47gnmVmwJWE7Qk9FedeYlm1u0Y2DUU63ALeAotZ14RfFppy/fZ8tsOa6YQ0SxOEEfgd4aR1FuiHDcl0fk+Y9Zs+0WrquKvEjieQro9SFmYzaCrkS32nUpacKTUOImR1S3T/Tskai8duU28PYIboBcB70aQ0vuoqMnGtoj96Vhd4dzmaRdBQYnuG3vn6pyy2K2hT0LS/mWdsz0c275RpBP1M0o5e9D3IYeCFkAL1iHqcwwgr4M8e48d+/Nef7BCxtYDBkC8C/Qt4bIt+28oypXYm7SvevY9TZxF64rkIbPip26IxH+BvL+GI4gL4twM/d8q16z8eFDUvemWjckt3N+DfSNocfr1wrg6PJ0ATwP8go0uYDfD2heu3G/IR4Tk9XlVazPRbgBPD32p1jz2fQ9Ef6gyrupjRyxVDIhepHmcTdeG4S6AF4FeX7sL3COLk9fU+X3zbUpyc0NxPfZ9QLpnH7vZGVuK5JbwC8Y4UVHbATxngjPishbIiy6mzvm9r1nSBPXd0PJtZ0d1+vAlOYXLpvgT6C/pnXJMS7mD6Ama8rvBvofqli4ir8lAFXeBH40n01HhigSlx+AvtQkZvlTxs37FBr8bmnLpjes2l+oIm4BM9Dn5rNLN4d4DQrg6PL0WJWXVXlKVV8BjvsXMbklb815XRtdlxPNQkTeBvkh6E83/VXQfDY9EpGfA8+Cvhe3gH2/Ubwxy055gXkwH6fbhXtVhIuqvLgJv5bj+6pyCDwH+kGfCHzYiomsJYPtmctr4tYvN1T5LfDMfHZplEA3+W3glqreJ8IjqnIB+KQI99BuJvkEJw3GBhIH175fgN5sFp4XaXKNv85n00HVruZ4sAPyBeDrwMPAOdC7QSaWeBl8ItX3PU018j7oFVV5AfQlEXnzpL9kbn/sckaVL4vo11quZ1XZyefHc/T84uZPk4+AXgNeB/kd8Np8Nr02ltNGvw5uB7Gtyq4I26rN07j5PnZ0bcusGVP9EgvgdlNOneznzV1+BxOQHWAHdBtkYpz6uNHL13hzLKK3VeXo6PLmP1rt53i4Bey08ziheYZ/LY81SPsRt1S52eZto7GxACr+v1D/hZDCUQVQOKoACkcVQOGoAigcVQCFowqgcFQBFI4qgMJRBVA4qgAKRxVA4agCKBxVAIWjCqBwVAEUjiqAwlEFUDiqAApHFUDhqAIoHFUAhaMKoHBUARSOKoDCUQVQOKoACkcVQOGoAigcVQCFowqgcFQBFI4qgMJRBVA4qgAKRxVA4agCKBxVAIWjCqBwVAEUjiqAwlEFUDiqAApHFUDhqAIoHFUAhaMKoHD8F5i5+/2ebUkFAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA3UlEQVQ4je3SoW6TAQBF4e82f5qx/KloEEsVGjVJkDwAlTzHBAK5oBA8AJonwPIMUxMEQRAIBFmapmmapWl6EeMNagjh+nOOuak6ZZOT6H9DkEfLq4EsEiMOre+JKRaYYoOfmOHiD3fXWu0+vT8OeIIPeNx2l+QN5niNc/qFvKWvyEuq9TnJO6yGJPO2L9ogezxtzRLP2koy0kXrOS6TSKwxYjVpe59klZD0gDu6wS4JuiXbxK/EkR7pmu5hwLfWMumIA26SnOFr22mSdes2yTU+EvjxECL/n/gXCH4DdF5YZa9nrisAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TemplateFirstPage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TemplateLastPage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TemplateNextPage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TemplatePrevPage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CurrentPage"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TotalPages"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VisiblePages"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
