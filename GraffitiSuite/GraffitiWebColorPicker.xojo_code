#tag Class
Protected Class GraffitiWebColorPicker
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "valuechange"
		      if UBound( Parameters ) = 3 then
		        mColorValue = RGB( Parameters(0), Parameters(1), Parameters(2), 255 - (Parameters(3).DoubleValue * 255) )
		      Else
		        mColorValue = RGB( Parameters(0), Parameters(1), Parameters(2) )
		      end if
		      mHexValue = ColorToHex( mColorValue )
		      ValueChange
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  'DoResize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  'if not IsLibraryRegistered( Session, JavascriptNamespace, "jqGWMiniColorsCSS", 1 ) then
		  'source.Append( "<style>" + cssDisplay + "</style>" )
		  RegisterLibrary( Session, JavascriptNamespace, "jqGWMiniColorsCSS" )
		  'end if
		  
		  source.Append( "<div id='" + me.ControlID + "' style='overflow:visible;display:none;' class='GWCPSelector" + if(not IsNull( me.Style ), " " + me.Style.Name, "" ) + "'>" )
		  source.Append( "<input id='" + me.ControlID + "_inner' type='hidden' value='#" + Hex( ColorValue ) + "' data-opacity='1'>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function ColorToString(c as Color) As String
		  Return "rgba(" + _
		  Str( c.Red, "###" ) + "," + _
		  Str( c.Green, "###" ) + "," + _
		  Str( c.Blue, "###" ) + "," + _
		  Str( 1 - (c.Alpha / 255), "0.00" ) + ")"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoResize()
		  if not me.Inline Then
		    dim strScript as String = "window.GSjQuery('#" + me.ControlID + "').find('.minicolors').width(" + Str( me.Width - 2, "#" ) + ").height(" + Str( me.Height - 2, "#" ) + ");" + _
		    "window.GSjQuery('#" + me.ControlID + "').find('.minicolors').find('.minicolors-swatch').width(" + Str( me.Width - 2, "#" ) + ").height(" + Str( me.Height - 2, "#" ) + ");"
		    
		    Buffer( strScript )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebColorPicker -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "colorpicker" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwcolorpicker.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwcolorpicker.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebColorPicker -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function PositionToString() As String
		  Select case mPosition
		  case PositionBottomLeft
		    Return "bottom left"
		  case PositionBottomRight
		    Return "bottom right"
		  case PositionTopLeft
		    Return "top left"
		  case else
		    Return "top right"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SelectorToString() As String
		  Select Case Selector
		  case SelectorHue
		    Return "hue"
		  case SelectorBrightness
		    Return "brightness"
		  case SelectorSaturation
		    Return "saturation"
		  case SelectorWheel
		    Return "wheel"
		  End Select
		  
		  Return "hue"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').minicolors({" )
		    strExec.Append( "changeDelay: 200," )
		    if ShowOpacity then strExec.Append( "opacity: true," )
		    strExec.Append( "control: '" + SelectorToString + "'," )
		    if ColorValue.Red = 0 and ColorValue.Green = 0 and ColorValue.Blue = 0 Then
		      strExec.Append( "defaultValue: '#" + mHexValue + "'," )
		    else
		      strExec.Append( "defaultValue: '" + ColorToString( mColorValue ) + "'," )
		    end if
		    if Inline then
		      strExec.Append( "inline: true," )
		    else
		      strExec.Append( "inline: false," )
		      strExec.Append( "position: '" + PositionToString + "'," )
		    end if
		    strExec.Append( "letterCase: 'lowercase'," )
		    strExec.Append( "opacity: " + Lowercase( Str( mShowOpacity ) ) + "," )
		    strExec.Append( "change: function (hex, opacity) {" )
		    if not Inline then
		      strExec.Append( "if(window.gswcolor_" + me.ControlID + "_shown === true) {" )
		    end if
		    strExec.Append( "var myRgb = window.GSjQuery('#" + me.ControlID + "_inner').minicolors('rgbObject');" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'valuechange', [myRgb.r, myRgb.g, myRgb.b, myRgb.a]);" )
		    if not Inline then
		      strExec.Append( "}" )
		    end if
		    strExec.Append( "}," )
		    strExec.Append( "theme: 'default'," )
		    strExec.Append( "show: function () {" )
		    strExec.Append( "window.gswcolor_" + me.ControlID + "_shown = true;" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').css('z-index',10000);" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onshow', []);" )
		    strExec.Append( "}," )
		    strExec.Append( "hide: function () {" )
		    strExec.Append( "window.gswcolor_" + me.ControlID + "_shown = false;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onhide', []);" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .minicolors-swatch').css('border','0px');" )
		    'strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').minicolors('value','" + Left( mHexValue, 7 ) + "');" )
		    'strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').minicolors('opacity'," + Str( 1 - mColorValue.Alpha / 255, "0.00" ) + ");" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://labs.abeautifulsite.net/jquery-minicolors/
		
		Licensed under MIT.
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private ChangeInternal As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mColorValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mColorValue = value
			  
			  mHexValue = ColorToHex( mColorValue )
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "_inner').minicolors('value','" + Left( mHexValue, 7 ) + "');" + _
			  "window.GSjQuery('#" + me.ControlID + "_inner').minicolors('opacity'," + Str( 1 - (mColorValue.Alpha / 255), "0.00" ) + ");"
			  
			  Buffer( strScript )
			  
			  ValueChange()
			End Set
		#tag EndSetter
		ColorValue As Color
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		FieldType As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHexValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHexValue = value
			  
			  dim v as Variant = Val( "&h" + Mid( value, 2 ) )
			  mColorValue = v.ColorValue
			  
			  dim strScript as String = "window.GSjQuery('#" + me.ControlID + "_inner').minicolors('value','" + Left( mHexValue, 7 ) + "');" + _
			  "window.GSjQuery('#" + me.ControlID + "_inner').minicolors('opacity'," + Str( 1 - (mColorValue.Alpha / 255), "0.00" ) + ");"
			  
			  Buffer( strScript )
			  
			  ValueChange()
			End Set
		#tag EndSetter
		HexValue As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mInline
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mInline = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Inline As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mColorValue As Color
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHexValue As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mInline As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPosition As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelector As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowOpacity As Boolean = True
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPosition
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPosition = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Position As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelector
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelector = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Selector As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowOpacity
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowOpacity = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowOpacity As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = cssDisplay, Type = String, Dynamic = False, Default = \"/* .GWCPSelector > div {\n    width: 100% !important;\n    height: 100% !important;\n    border: 1px solid rgba(255\x2C 255\x2C 255\x2C 0.5);\n    outline: 1px solid #000;\nposition: relative;\n} */\n\n/* .GWCPSelector > div > span {\n    width: 100% !important;\n    height: 100% !important;\nposition: relative;\nleft: 2px;\nright: 4px;\ntop: 2px;\nbottom: 4px;\n} */", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.colorpicker", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAqmSURBVHhe7Z1tjFRXGcfPc2crO7NWm2oKtUJbWtOgRP1gUj9AcaHaVro7Q6Npo4nvMVpRd2ZBaWsLUotW3JmFtljBlkbFpFa7M7MrYoGFUmqhSmurYoh+8SUt1YQoZWaWwr2Pz5l5wKUsu/ft3Hvn3vP7cs//WcJu7v9/nnPuvNwrNBqNRqPRaDQajUaj0Wg0Go1Go9HEGuCjJkA6b+q/FE5YFwsDZgiAGQbgq8ISh0m/kkpZLx39eekI/1Pl6AAERDrb1wMCbqDhh8n0S1vViUEUu8iYrQbC1mPVgYNcVoIOgGLS2f5bAawVdKpncskZKB6jwNxbLw8c4Iqv6AAoojNbuBkA76JZ/04ueYK6wuaGeWypGNlY55Iv6AAoIJMtLKMzu5alj+CBlNXx8Veraw9xwTM6AD6TyRXW0eErLeU/1An+baDxsVr1ezu45AkdAB9J5/KbqeV/iqVC8CSCmN8YKu3jgmsMPmo8ks7mHwrGfAl0gAVb0ovzl3DBNToAPkAzfxMAfIZlMICYTTHYwso1KT5qXEJr/oM08z/PMlhAXNZx1fv/e/LQPtdLgd4DeCCTzW+ga/QvsgwFFHi4cVFtlti48QSXHKGXAJeQ+feHbb6Eus+MzCtvXMHSMYntAJ3ZwkwDsHmtbiEsH6sU/9H8gQ0yub4HaO7cyjJ06NLwz41K0dULTokMQPPNGBN/CSDeJbU8gZaFvceHS39t/oNJoDWfzBeRMf8UYFpza8ODf2Jpm8QtAa83X0LjOSkDtk3ryV/JpQlprvkRNF+ChiHfaHJMogIwkfmnAXFFKiVGzxWCKGz4Jgfn8cARiQnApOafBmYaKdj7+hCQ+d+PtvkEwHQeOSIRAbBnfgvaFE2nEDx7KgSZbEGa/4XmD6PNDD46IvabQCfmnwnWEHEYwLiFC9EGcaxeKaVZ2SbWHcC9+RLoahvzCQQY46EjYhsAb+a3IYiHeeSIWAYgceZLAF7mkSNiFwA2fyRR5ktQ2H4lczyxCsA48+dyKTHQhvVXPHREbALQMt9KpPmSzGsnt/LQEbEJAFi4AQASaT6ieOLItvuOsnREjJYAzPAgeaCQb1C5Ij4BsIwVKMR/WCUGOfsb1WKVpWNiE4BGdWA/BaCHhsdblWRgGNa9PHRFjJYAIcbKxb2WBR9imQRW1YYGR3nsili+F9DVm+9GAzydmKhD3e6hRrn4OZauie2bQV3ZwiIE4cu3Z6JGc92vFK9j6YlYLQHjqVWKO+lMLWYZG1DgjsYbzpd7HV+I9fcCThza95fUVVe/BwDmcKmtoba/o3HeP68Xj204ySXPxPrzAOlsYQ2AuI1lW0Mzf2ejXLqWpW/ENgDafHvEMgCxMh9xtFEpLWLpO7HbBMbLfKHUfEmsOkDMzN9Fl3oLWSojNh0gXmt+MOZLYtEBYrbh200bvm6Wymn7DuDFfABrEaBQsrt2Q3PmB2i+pK0D4Ml8CxfKN1LkK4YAoHSjZYfmizzlYNr+eNo2AJ7MR+iuVUu7WIra0MBomCGgtv9rMv+DLAOlLQPgxXxL4AdqlYHdLE8TWghQNCzTCO17h20XAE/mW7BgrFx6kuVZhBICEGkjhc/JD7VyJVDaKgCezEfrmrHqwB6W5ySMENCl2AW0J9kfRgjaJgDe2r6YP1YZfIrllIQUgumGhXuCDkFbBMCT+YY1T35UjKVtQtoTzDJM3B5kCCIfAM/mPz74NEvHhLQneIf8gktQIYh0ANK5/D1hmX+KUJYDgLkyBGLByg4uKSOyAUhn898CAbezdIRf5p9ChkAgfIJlIMgQdF549CMslRHJAFDbv5tOwB0sHeG3+ZLm7WIAN7EMBET8F5gdU161eCVyAaC2v5ra/jdYOkKV+SlD/EEOWxX1IIojCFauUV37EpeUEakA0Mz/JrX9O1k6Qp358CL1404uKadlvnnjWHndM1xSSmQCQGv+Kpr5d7F0hDLzU/CCfKWOS8oJ2nxJJD4PkMkVVtJhVUs5Q5354vd0erq4pJwwzJeE3gEyvXk566NlvgHPJ8F8SagdIJMt3El/wWqWjrBMMn9Yycx/jk7L+VxSTpjmS0LrANT274ia+UYKDrgxn0z8CV22Ob5mD9t8SSgdgGb+7fSb72HpCGXmG3CANqFv4pJtUOCPG+VS80UiuorJ0f8x1PzBFETBfEngAcjk8rfRr13D0hHqzBe/A4A3c8k2ZOKPGpXiJ1k2aT4jGIxJ79gRFfMlgQaA2r58tMm3W8oZCtv+b+kkXMAl29DMf4Rm/qdZnkEmm5cPiB6h4VnnN0rmSwLbA9BJ+TodIma+eNad+WLzucyX1CulrbQUXCc/7sWlJlEzXxJIB6A1/2v0m1zcywbRMnG+ojV/P5l0IZdsQ5u9hxuV0mdZTkpntm8+CKMsf08UzZcoDwCt+cvp13yXpX3Ie8vCaxSZv49MeQuXbEMz3/FtWeTDqUBgD5rmyNjI+r9zOTIoDYDrp2ijMMn9BYra/jMg4K1csg3N4E204QvnAZEKUbYH6Mrm+12ZL/CkOvPhN+7Mx41xNF+ipAPQbv9GOgy3lANQnCDzu9W0ffE0XepdxCUn/KBeLrbDI2NcoaQD0CWS8w0f4muRMx/xwTibL/E9ALTpuZnarNOnWB6nDd9CRW1/r8uZv4Eu56L9pDAf8D0AtON1dqdOxDG6zl+kaOY/RWuc88epIT5AM/9LrGKN/wEAsP96uvxenIXXqjEf9tDf4vxRaijup5m/lFXs8X8PgOI8Hk0NiqUqzE+lxJN0nX8xl5ywvl4pfpnHiUDJJtAumAL5YUvfaJkPu6kPvY1L9kFcR23/q6wSQ6gBEMLy7U6lTfMNkN/5v6RVccQgtf0+HieKcDsAtQAeeoLb/ijtQN/OJfsglmjm51klDgUBQOXvL4zntPkCZnLJCQM08ws8TiQhLwHeaLV9sdON+SjNLxeXsUws/gcgoPnP5u+g685ZXLKNNL+hzW/Slh1gWm7ZFbTb307mO/4KtTb/TFQEQGkPmHZT3+yUMLfT8LJWxT7a/LNREAB1/jfNNw0yHy7nkm20+RPTNkvAtJ7+y1MmPEH5ms0l22jzz43v0zWdK2yn/9TW7VcRxTb6C0wQaJE06c8xyS06okmmydppAOBqOlzZUvbR5k+OrwHILMlfLxBcPcVaBdr8qfF1CaAZHZmTrc23h28ByOT6bwAR+G3VJkSbbx/fAoACl/MwVLT5zvAlAJnevsW0mQj0PvcToc13jj8dAEB+5y9UtPnu8ByA5kfAAeaxDAVtvnt86ADo6pZufqHN94anAKSz/T3U/+ULNOFg4Uptvjc8BQDAknf3Chy64thtotldr5Zc3WJG839cvxKY7s0vAQMeZ+kYMvGHhoWPsjw3YFCXb2FZWIMO/Ft9aPBlLmk84j4AufwLIODdLG0j76yRsoy1x6oDB7mkCRFXAUgvKXwUUPyMpS0QcYthGN+pDQ38kUuaCOAuANnCQQAxh+UU4KNgmmtqw+tf5IImQrjaBALAT3k4Gb8QgO+tl0u3aPOji+s9QCbXT9f/eDfLcWBZWLC6Xi0+zwVNhHEdAMmZIcAqNZTV9fLAgZbWJAIZgnRv3/tYajQajUaj0Wg0Go1Go9FoNBqNRqPRaDSaaCDE/wAmKmhJuzJvcAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEWSURBVDhPY6AUMENpogBHQEEYm4aF9O8bJ+5ChRgYoTRewOGTJ8fIwrydkYFRC8T//5+h6fvGvnoQG68BXP5FK/8z/v8EZFrBNMPAtw19YL1MYB4WwBVQ2Ac0PgyoMQVdM9AJT6As3Ab8//d/GZSJAv4zMHz6x/i/GMrF7gWYn6FcoCJkF/xz/7Zhwi4oB9MFyJr///nrCcZAW8GSYMDoAWWAAYoB6Jp/bJn0iImZpRvMZ/h/DYT//fk7AcSHARQD0DVDYoHBg5HhXyjMNSBxsGIogIcBKJEwMTCt/MfwL/zHhgmrkDUj+xkdYIQB03+mYM6Awh3EaAYBuAEgjVCmLON/Rj5iNKMAoK2zOf0KTKDcIQMYGACTT3bP4Pi8TwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PositionBottomLeft, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionBottomRight, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopLeft, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopRight, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SelectorBrightness, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SelectorHue, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SelectorSaturation, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SelectorWheel, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="ColorValue"
			Visible=true
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FieldType"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HexValue"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Inline"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Position"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - BottomLeft"
				"1 - BottomRight"
				"2 - TopLeft"
				"3 - TopRight"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Selector"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - SelectorHue"
				"1 - SelectorBrightness"
				"2 - SelectorSaturation"
				"3 - SelectorWheel"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowOpacity"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
