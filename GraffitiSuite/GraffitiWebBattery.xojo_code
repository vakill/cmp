#tag Class
Protected Class GraffitiWebBattery
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  select case Name
		  case "functionSupport"
		    if Parameters.Ubound >= 0 then
		      IsSupported = Parameters(0).BooleanValue
		      if IsSupported then RunBuffer()
		    end if
		  case "batteryCharging"
		    if Parameters.Ubound < 0 then Return True
		    if IsSupported and ControlAvailableInBrowser then Updated()
		  case "batteryLevel"
		    if Parameters.Ubound < 0 then Return True
		    Level = Parameters(0).IntegerValue
		    if IsSupported and ControlAvailableInBrowser then Updated()
		  case "batteryChargeTime"
		    if Parameters.Ubound < 0 then Return True
		    ChargeTime = Val( if( IsNumeric( Parameters(0).StringValue ), Parameters(2).StringValue, "-1" ) )
		    if IsSupported and ControlAvailableInBrowser then Updated()
		  case "batteryDischargeTime"
		    if Parameters.Ubound < 0 then Return True
		    DischargeTime = Val( if( IsNumeric( Parameters(0).StringValue ), Parameters(3).StringValue, "-1" ) )
		    if IsSupported and ControlAvailableInBrowser then Updated()
		  case "batteryUpdate"
		    if Parameters.Ubound = 3 then
		      Charging = Parameters(0).BooleanValue
		      Level = Parameters(1).IntegerValue
		      ChargeTime = Val( if( IsNumeric( Parameters(2).StringValue ), Parameters(2).StringValue, "-1" ) )
		      DischargeTime = Val( if( IsNumeric( Parameters(3).StringValue ), Parameters(3).StringValue, "-1" ) )
		      if IsSupported and ControlAvailableInBrowser then Updated()
		    end if
		  end select
		  
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  dim strExec() as String
		  
		  strExec.Append( "window.gsReadBattery = function(b) {" )
		  
		  strExec.Append( "battery = b || battery;" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'batteryUpdate',[battery.charging, battery.level * 100, battery.chargingTime, battery.dischargingTime]);" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "window.initBattery = function(b) {" )
		  
		  strExec.Append( "battery = b || battery;" )
		  
		  strExec.Append( "battery.addEventListener('chargingchange', function(){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'batteryCharging',[battery.charging]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "battery.addEventListener('levelchange', function(){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'batteryLevel',[battery.level * 100]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "battery.addEventListener('chargingtimechange', function(){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'batteryChargeTime',[battery.chargingTime]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "battery.addEventListener('dischargingtimechange', function(){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'batteryDischargeTime',[battery.dischargingTime]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "};" )
		  
		  strExec.Append( "var supported = false;" )
		  strExec.Append( "if (navigator.battery) {" )
		  strExec.Append( "supported = true;" )
		  strExec.Append( "window.gsReadBattery(navigator.battery);" )
		  strExec.Append( "window.gsInitBattery(navigator.battery);" )
		  strExec.Append( "} else if (navigator.getBattery) {" )
		  strExec.Append( "supported = true;" )
		  strExec.Append( "navigator.getBattery().then(window.gsReadBattery);" )
		  strExec.Append( "navigator.getBattery().then(window.gsInitBattery);" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'functionSupport',[supported]);" )
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  if IsSupported then Update()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Update()
		  if IsSupported and ControlAvailableInBrowser then
		    dim strExec() as String
		    
		    strExec.Append( "if (navigator.battery) {" )
		    strExec.Append( "window.gsInitBatter(navigator.battery);" )
		    strExec.Append( "window.gsReadBattery(navigator.battery);" )
		    strExec.Append( "} else if (navigator.getBattery) {" )
		    strExec.Append( "navigator.getBattery().then(window.gsInitBatter);" )
		    strExec.Append( "navigator.getBattery().then(window.gsReadBattery);" )
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Updated()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Implementation completed by Anthony G. Cyphers, taken from documentation available at:
		https://developer.mozilla.org/en-US/docs/Web/API/Detecting_device_orientation
		
		For GraffitiSuite Source Code EULA, please visit:
		https://graffitisuite.com/legal/source-eula/
	#tag EndNote


	#tag Property, Flags = &h0
		ChargeTime As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		Charging As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		DischargeTime As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		IsSupported As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Level As Integer = -1
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webbattery", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAR5SURBVHhe7d1BaFxFHMfx+b9UbZZAQZEoVDwoWhQsYkGKehFBrEI2tVZBvCj1aM1aLYViEUXUNn1BQUTRg4hSWs1uBHPwIIJULArtTWyPamPRgtLsqyZ9f2fTP8jzvbcll5Cd+X5gs/OfIaf57byZx/LWAQAAAAAAAAAAAAAAAAAAAAAAAAAGjdj7kpGtrQ2LudsiqveKuGv88Dobwur2h1M3p05nkySZnp+ePGP9l3QxAPv2JY3jf+13Iq2lGoNLXeb/vtLtHHz1Ykd/0mg+d4e6/D1xcrv1IQCqOiuXDz3ZPXxgzroqJX7yP2TywyMiD+hC/r6VtRI/+bdYG4Hx1/ctjebEHisrJfaOYMnekW17rraihACEr5Evnm9au8RvAltq7QLf+bqo+91KrGK5uG3+k3ynlWWqn3U76cNWFVQHQN3z/hhxwCoMgOHmxAm/n7vNyv/Ro912epcVBZWXAJX8pDUxIPxq/bE1S9TJVdYsYQ8QAX8aGLJmCQGIwxp7LyEAUVBWgJipSu0KUHkKUJc3s/ZUx8qCtc3W3Ym6G63ECtLL9MvsSPqLlQWNsYndTuQ1KwtU9UzWSUetLFh2APxxY78/buyyEitInD4x304/srKgfwDc2axzsPIkwCUgDuwBYiainAKCJ1L4dleBuqHhsda7w81nd17R3HWD9S4hAKHI65d5H461Ph47xCVTQy4/5fcLh4bHX1jfGyIAofCza61LE9ku+cI3Iw+1NhCAQGifmz2VRK7Ph/QDAhAMWV4AekQ2E4BwLD8AHgEIhSzzEmAIQChUzvuNwNe1rxrcCh4g/W4F99MYn9joA3LcygJWgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMgRgMjxpdAB0vf5AM2JHaqyycoCEdd7NkDlcwJZAQLhP8U3+Yl+uurlhysnv4cARI4ARI4AREzVnSYAsVL9J1F9nABESX9QcffMz6RfEYAY+LXe/znqX2+pyCPddropa6fHekMEIBQqpfs5/5FzvcfF+9cz2fTkEetcQgBCIS63Volf7hetWUIAQqH1AfCDtQFY/rOCxycek9xttBIrSZN2NjP5nVUFw2Otl0XcXisLVHUu66TXWlmw7ABgdfIBeMkH4EUri1R/7nbS66wq4BIQCD/59ZcAEfYA4ZML1qhCAIKnF+pPAVq/CSQAwei7AtSqDIC4pPZnxrA6+bP+ldYsEZE/rVlSeQrwuqrusP/PGauxmqk+6jeB260qU53xp4AxqwrqAoCAaK47s5n0TSsL2ANEIFf3hTVLCEDg1Okbf3+enrKyJOn9pJi1ERp1x7J2utuqSonfIT7lNwG/WY1A+E38CckXd1hZa2jhx29/WnPz5kPi3Hp//rvV+jHI1L2TjZ7buvDJ26etp1bhl6YaY637neiDPj33+aFRP7jOhrCK+ev8WT9fv/rWrP/of5rNTH1vQwAAAAAAAAAAAAAAAAAAAAAAAAAAICjO/QtSwW0fSkul6QAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADMSURBVDhPYxj6gJEroDAVSGtCuMSDf3/+TvixZdIjRi7/wsf/GP8XQ8WJAoz/mbQZGf/zftvQXwQ0oOgYA+O/Bob/TA1QebzgP+P/q/8Z/u9mZGBM+v/nbxoTSPAfA4MAAyODJTGY8T+jNuM/hntAmo+RmSUZbACp4PumCWeA/jgB8gZZBiADsgzgCCgIY/jPEPqPAegKUCD++/sngoGFyQIqjxcAbfwACjMmBkYLcCxwBhReBQbIR6g8keC/LDBAm4AGzIYKjGDAwAAAjxlD1ZPrtKEAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="ChargeTime"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Charging"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="DischargeTime"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsSupported"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Level"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
