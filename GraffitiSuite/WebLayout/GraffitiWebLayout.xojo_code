#tag Class
Protected Class GraffitiWebLayout
Inherits GraffitiControlWrapper
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "panelResize"
		      PanelResized( GetPanel( Parameters(0).StringValue ), Parameters(1).IntegerValue, Parameters(2).IntegerValue )
		      dim theContainer as WebControl = GetPanel( Parameters(0).StringValue ).ContainerPanel
		      FixPanelSize( Parameters(0).StringValue )
		      
		    case "panelShow"
		      PanelShown( GetPanel( Parameters(0).StringValue ) )
		    case "panelHide"
		      PanelHidden( GetPanel( Parameters(0).StringValue ) )
		    case "panelOpen"
		      PanelOpened( GetPanel( Parameters(0).StringValue ) )
		    case "panelClose"
		      PanelClosed( GetPanel( Parameters(0).StringValue ) )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  Select case Name
		  case "Style"
		    Dim st as WebStyle = WebStyle(value)
		    dim strOut() as String
		    
		    strOut.Append( "var thisControl = window.GSjQuery('#" + me.ControlID + "');" )
		    If not self.style.IsDefaultStyle then strOut.Append( "thisControl.removeClass('" + self.Style.Name + "');" )
		    if not st.isdefaultstyle then strOut.Append( "thisControl.addClass('" + st.Name + "');" )
		    
		    Buffer( strOut )
		    
		    Return True
		  End Select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		  
		  Open()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "'" )
		  if not self.Style.IsDefaultStyle Then
		    source.Append( " class='" + self.Style.Name + "'" )
		    lastStyle = self.Style.Name
		  end if
		  source.Append( ">" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  updateOptions()
		  
		  Shown()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ClosePanel(thePanel as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.close('" + strPanel + "');"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FixPanelSize(thePanel as String)
		  dim strExec() as String
		  strExec.Append( "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" )
		  strExec.Append( "myLayout.sizeContent('" + thePanel + "');" )
		  
		  if LibrariesLoaded and isShown and ControlAvailableInBrowser then
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		  else
		    InitBuffer.Append( Join( strExec, EndOfLine ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetPanel(Position as String) As GraffitiWebLayoutPanel
		  dim pnl as GraffitiWebLayoutPanel
		  Select Case Position
		  case "north"
		    pnl = PanelNorth
		  case "south"
		    pnl = PanelSouth
		  case "east"
		    pnl = PanelEast
		  case "west"
		    pnl = PanelWest
		  case "center"
		    pnl = PanelCenter
		  end select
		  
		  Return pnl
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HidePanel(thePanel as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.hide('" + strPanel + "');"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebLayout -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "layout" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwlayout.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwlayout.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebLayout -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenPanel(thePanel as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.open('" + strPanel + "');"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function PrepOptions(thePanel as GraffitiWebLayoutPanel, Position as String) As String
		  dim strExec() as String
		  if not IsNull( thePanel ) Then
		    if not IsNull( thePanel.ContainerPanel ) Then
		      strExec.Append( Lowercase( Position ) + ": {" )
		      
		      if Position <> "center" Then
		        strExec.Append( "size: " + if( thePanel.SizePercentage, "'", "" ) + Str( thePanel.InitialSize, "#" ) + if( thePanel.SizePercentage, "%'", "" ) + "," )
		        if thePanel.MinSize > 0 then strExec.Append( "minSize: " + if( thePanel.SizePercentage, "'", "" ) + Str( thePanel.MinSize, "#" ) + if( thePanel.SizePercentage, "%'", "" ) + "," )
		        if thePanel.MaxSize > 0 then strExec.Append( "maxSize: " + if( thePanel.SizePercentage, "'", "" ) + Str( thePanel.MaxSize, "#" ) + if( thePanel.SizePercentage, "%", "" ) + "," )
		        strExec.Append( "closable: " + Lowercase( Str( thePanel.Closable ) ) + "," )
		        strExec.Append( "resizable: " + Lowercase( Str( thePanel.Resizable ) ) + "," )
		        strExec.Append( "autoResize: " + Lowercase( Str( thePanel.Resizable ) ) + "," )
		        strExec.Append( "autoReopen: true," )
		        strExec.Append( "slidable: " + Lowercase( Str( thePanel.Slidable ) ) + "," )
		        strExec.Append( "initClosed: " + Lowercase( Str( thePanel.InitialClosed ) ) + "," )
		        strExec.Append( "initHidden: " + Lowercase( Str( thePanel.InitialHidden ) ) + "," )
		        strExec.Append( "onshow_end: function(paneName, paneElement, paneState, paneOptions, layoutName) {" )
		        strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'panelShow', [paneName]);" )
		        'strExec.Append( "window.GSjQuery(window).trigger('resize');" )
		        strExec.Append( "}," )
		        strExec.Append( "onhide_end: function(paneName, paneElement, paneState, paneOptions, layoutName) {" )
		        strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'panelHide', [paneName]);" )
		        'strExec.Append( "window.GSjQuery(window).trigger('resize');" )
		        strExec.Append( "}," )
		        strExec.Append( "onopen_end: function(paneName, paneElement, paneState, paneOptions, layoutName) {" )
		        strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'panelOpen', [paneName]);" )
		        'strExec.Append( "window.GSjQuery(window).trigger('resize');" )
		        strExec.Append( "}," )
		        strExec.Append( "onclose_end: function(paneName, paneElement, paneState, paneOptions, layoutName) {" )
		        strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'panelClose', [paneName]);" )
		        'strExec.Append( "window.GSjQuery(window).trigger('resize');" )
		        strExec.Append( "}," )
		      else
		        if thePanel.MinSize > 0 then
		          strExec.Append( "minWidth: " + if( thePanel.SizePercentage, "'", "" ) + Str( thePanel.MinSize, "#" ) + if( thePanel.SizePercentage, "%'", "" ) + "," )
		          strExec.Append( "minHeight: " + if( thePanel.SizePercentage, "'", "" ) + Str( thePanel.MinSize, "#" ) + if( thePanel.SizePercentage, "%'", "" ) + "," )
		        end if
		      end if
		      
		      strExec.Append( "onresize: function(paneName, paneElement, paneState, paneOptions, layoutName) {" )
		      strExec.Append( "window.GSjQuery('#" + thePanel.ContainerPanel.ControlID + "').trigger('resize');" )
		      strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'panelResize', [paneName, window.GSjQuery(paneElement).width(), window.GSjQuery(paneElement).height()]);" )
		      strExec.Append( "}" )
		      strExec.Append( "}," )
		    end if
		  end if
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub PrepPanel(OldPanel as GraffitiWebLayoutPanel, NewPanel as GraffitiWebLayoutPanel, PanelPosition as String)
		  dim strExec() as String
		  
		  if not IsNull( OldPanel ) Then
		    if not IsNull( OldPanel.ContainerPanel ) Then
		      strExec.Append( "var old" + PanelPosition + " = window.GSjQuery('#" + OldPanel.ContainerPanel.ControlID + "').detach();" )
		      strExec.Append( "window.GSjQuery('#" + me.Page.ControlID + "').append(old" + PanelPosition + ");" )
		      strExec.Append( "window.GSjQuery('#" + OldPanel.ContainerPanel.ControlID + "').removeClass('ui-layout-" + Lowercase( PanelPosition ) + "');" )
		    end if
		  end if
		  
		  if not IsNull( NewPanel ) Then
		    if not IsNull( NewPanel.ContainerPanel ) Then
		      
		      strExec.Append( "var new" + PanelPosition + " = window.GSjQuery('#" + NewPanel.ContainerPanel.ControlID + "').detach();" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').append(new" + PanelPosition + ");" )
		      strExec.Append( "window.GSjQuery('#" + NewPanel.ContainerPanel.ControlID + "').addClass('ui-layout-" + Lowercase( PanelPosition ) + "');" )
		      
		      if not IsNull( NewPanel.ContainerPanel.Style ) Then
		        if NewPanel.ContainerPanel.Style.Name = "_DefaultStyle" then
		          if not IsNull( me.Style ) then strExec.Append( "window.GSjQuery('#" + NewPanel.ContainerPanel.ControlID + "').addClass('" + me.Style.Name + "');" )
		        end if
		      else
		        if not IsNull( me.Style ) then strExec.Append( "window.GSjQuery('#" + NewPanel.ContainerPanel.ControlID + "').addClass('" + me.Style.Name + "');" )
		      end if
		      
		      if NewPanel.AllowOverflow Then
		        strExec.Append( "window.GSjQuery('#" + NewPanel.ContainerPanel.ControlID + "').css('overflow','auto');" )
		      Else
		        strExec.Append( "window.GSjQuery('#" + NewPanel.ContainerPanel.ControlID + "').css('overflow','hidden');" )
		      end if
		    end if
		  end if
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  else
		    InitBuffer.Append( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ResizePanel(thePanel as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.resizeContent('" + strPanel + "');"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowPanel(thePanel as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.show('" + strPanel + "');"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SizePanel(thePanel as Integer, PixelSize as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.sizePane('" + strPanel + "', " + Str( PixelSize, "#" ) + ");"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TogglePanel(ThePanel as Integer)
		  dim strPanel as String
		  
		  Select case ThePanel
		  case PanelPositionCenter
		    strPanel = "center"
		  case PanelPositionNorth
		    strPanel = "north"
		  case PanelPositionEast
		    strPanel = "east"
		  case PanelPositionSouth
		    strPanel = "south"
		  case PanelPositionWest
		    strPanel = "west"
		  end select
		  
		  dim strExec as String = "var myLayout = window.GSjQuery('#" + me.ControlID + "').layout();" + _
		  "myLayout.toggle('" + strPanel + "');"
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( strExec )
		  Else
		    InitBuffer.Append( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    
		    dim strExec() as String
		    
		    if IsNull( PanelCenter ) Then Return
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').layout({" )
		    strExec.Append( "triggerEventsOnLoad: true," )
		    'strExec.Append( "maskIframesOnResize: true," )
		    
		    if not IsNull( PanelCenter ) then
		      if not IsNull( PanelCenter.IFrame ) Then strExec.Append( "center__maskIframesOnResize: '#" + PanelCenter.IFrame.ControlID + "'," )
		    end if
		    if not IsNull( PanelEast ) then
		      if not IsNull( PanelEast.IFrame ) Then strExec.Append( "east__maskIframesOnResize: '#" + PanelEast.IFrame.ControlID + "'," )
		    end if
		    if not IsNull( PanelWest ) then
		      if not IsNull( PanelWest.IFrame ) Then strExec.Append( "west__maskIframesOnResize: '#" + PanelWest.IFrame.ControlID + "'," )
		    end if
		    if not IsNull( PanelNorth ) then
		      if not IsNull( PanelNorth.IFrame ) Then strExec.Append( "north__maskIframesOnResize: '#" + PanelNorth.IFrame.ControlID + "'," )
		    end if
		    if not IsNull( PanelSouth ) then
		      if not IsNull( PanelSouth.IFrame ) Then strExec.Append( "south__maskIframesOnResize: '#" + PanelSouth.IFrame.ControlID + "'," )
		    end if
		    
		    dim strCenter as string = PrepOptions( PanelCenter, "center" )
		    dim strNorth as String = PrepOptions( PanelNorth, "north" )
		    dim strSouth as String = PrepOptions( PanelSouth, "south" )
		    dim strEast as String = PrepOptions( PanelEast, "east" )
		    dim strWest as String = PrepOptions( PanelWest, "west" )
		    if strCenter <> "" then strExec.Append( strCenter )
		    if strNorth <> "" then strExec.Append( strNorth )
		    if strSouth <> "" then strExec.Append( strSouth )
		    if strEast <> "" then strExec.Append( strEast )
		    if strWest <> "" then strExec.Append( strWest )
		    strExec.Append( "applyDefaultStyles: false," )
		    strExec.Append( "});" )
		    
		    if UBound( StyleBuffer ) >= 0 Then
		      strExec.Append( Join( StyleBuffer, "" ) )
		      ReDim StyleBuffer(-1)
		    end if
		    
		    strExec.Append( "window.GSjQuery(window).trigger('resize');" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').resize();" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').layout('resizeAll');" )
		    
		    if UBound( InitBuffer ) >= 0 Then
		      me.ExecuteJavaScript( Join( InitBuffer, "" ) )
		      ReDim InitBuffer(-1)
		    end if
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PanelClosed(thePanel as GraffitiWebLayoutPanel)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PanelHidden(thePanel as GraffitiWebLayoutPanel)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PanelOpened(thePanel as GraffitiWebLayoutPanel)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PanelResized(thePanel as GraffitiWebLayoutPanel, newWidth as Integer, newHeight as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PanelShown(thePanel as GraffitiWebLayoutPanel)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://layout.jquery-dev.com/
		
		License under MIT.
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private InitBuffer() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected jsFile As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastResize As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastStyle As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCustomStyle As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPanelCenter As GraffitiWebLayoutPanel
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPanelEast As GraffitiWebLayoutPanel
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPanelNorth As GraffitiWebLayoutPanel
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPanelSouth As GraffitiWebLayoutPanel
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPanelWest As GraffitiWebLayoutPanel
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReady As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleCloseHandleEW As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleCloseHandleNS As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleResizeHandleEW As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleResizeHandleNS As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyleCloseHandleEW As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyleCloseHandleNS As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyleResizeHandleEW As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyleResizeHandleNS As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPanelCenter
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldPanel as GraffitiWebLayoutPanel = mPanelCenter
			  mPanelCenter = value
			  
			  PrepPanel( oldPanel, mPanelCenter, "Center" )
			End Set
		#tag EndSetter
		PanelCenter As GraffitiWebLayoutPanel
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPanelEast
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldPanel as GraffitiWebLayoutPanel = mPanelEast
			  mPanelEast = value
			  
			  PrepPanel( oldPanel, mPanelEast, "East" )
			End Set
		#tag EndSetter
		PanelEast As GraffitiWebLayoutPanel
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPanelNorth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldPanel as GraffitiWebLayoutPanel = mPanelNorth
			  mPanelNorth = value
			  
			  PrepPanel( oldPanel, mPanelNorth, "North" )
			End Set
		#tag EndSetter
		PanelNorth As GraffitiWebLayoutPanel
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPanelSouth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldPanel as GraffitiWebLayoutPanel = mPanelSouth
			  mPanelSouth = value
			  
			  PrepPanel( oldPanel, mPanelSouth, "South" )
			End Set
		#tag EndSetter
		PanelSouth As GraffitiWebLayoutPanel
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPanelWest
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldPanel as GraffitiWebLayoutPanel = mPanelWest
			  mPanelWest = value
			  
			  PrepPanel( oldPanel, mPanelWest, "West" )
			End Set
		#tag EndSetter
		PanelWest As GraffitiWebLayoutPanel
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private StyleBuffer() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleCloseHandleEW
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleCloseHandleEW ) Then
			    oldStyleCloseHandleEW = mStyleCloseHandleEW.Name
			  end if
			  
			  mStyleCloseHandleEW = value
			  
			  if not IsNull( mStyleCloseHandleEW ) Then
			    dim strExec() as String
			    
			    strExec.Append( "window.GSjQuery('.ui-layout-toggler-east, .ui-layout-toggler-west')" + if( oldStyleCloseHandleEW <> "", ".removeClass('" + oldStyleCloseHandleEW + "')", "" ) + ".addClass('" + mStyleCloseHandleEW.Name + "');" )
			    if not mStyleCloseHandleEW.IsDefaultStyle Then
			      if LibrariesLoaded then
			        me.ExecuteJavaScript( Join( strExec, "" ) )
			      Else
			        me.StyleBuffer.Append( Join( strExec, "" ) )
			      end if
			    end if
			  end if
			End Set
		#tag EndSetter
		StyleCloseHandleEW As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleCloseHandleNS
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleCloseHandleNS ) Then
			    oldStyleCloseHandleNS = mStyleCloseHandleNS.Name
			  end if
			  
			  mStyleCloseHandleNS = value
			  
			  if not IsNull( mStyleCloseHandleNS ) Then
			    dim strExec() as String
			    
			    strExec.Append( "window.GSjQuery('.ui-layout-toggler-north, .ui-layout-toggler-south')" + if( oldStyleCloseHandleNS <> "", ".removeClass('" + oldStyleCloseHandleNS + "')", "" ) + ".addClass('" + mStyleCloseHandleNS.Name + "');" )
			    if not mStyleCloseHandleNS.IsDefaultStyle Then
			      if LibrariesLoaded then
			        me.ExecuteJavaScript( Join( strExec, "" ) )
			      Else
			        me.StyleBuffer.Append( Join( strExec, "" ) )
			      end if
			    end if
			  end if
			End Set
		#tag EndSetter
		StyleCloseHandleNS As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleResizeHandleEW
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleResizeHandleEW ) Then
			    oldStyleResizeHandleEW = me.mStyleResizeHandleEW.Name
			  end if
			  
			  me.mStyleResizeHandleEW = value
			  
			  if not IsNull( me.mStyleResizeHandleEW ) then
			    dim strExec() as String
			    strExec.Append( "window.GSjQuery('.ui-layout-resizer-east, .ui-layout-resizer-west')" + if( oldStyleResizeHandleEW <> "", ".removeClass('" + oldStyleResizeHandleEW + "')", "" ) + ".addClass('" + me.mStyleResizeHandleEW.Name + "');" )
			    if not mStyleResizeHandleEW.IsDefaultStyle Then
			      if LibrariesLoaded then
			        me.ExecuteJavaScript( Join( strExec, "" ) )
			      Else
			        me.StyleBuffer.Append( Join( strExec, "" ) )
			      end if
			    end if
			  end if
			End Set
		#tag EndSetter
		StyleResizeHandleEW As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleResizeHandleNS
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mStyleResizeHandleNS ) Then
			    oldStyleResizeHandleNS = me.mStyleResizeHandleNS.Name
			  end if
			  
			  me.mStyleResizeHandleNS = value
			  
			  if not IsNull( me.mStyleResizeHandleNS ) then
			    dim strExec() as String
			    strExec.Append( "window.GSjQuery('.ui-layout-resizer-north, .ui-layout-resizer-south')" + if( oldStyleResizeHandleNS <> "", ".removeClass('" + oldStyleResizeHandleNS + "')", "" ) + ".addClass('" + me.mStyleResizeHandleNS.Name + "');" )
			    if not mStyleResizeHandleNS.IsDefaultStyle Then
			      if LibrariesLoaded then
			        me.ExecuteJavaScript( Join( strExec, "" ) )
			      Else
			        me.StyleBuffer.Append( Join( strExec, "" ) )
			      end if
			    end if
			  end if
			End Set
		#tag EndSetter
		StyleResizeHandleNS As WebStyle
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.jqlayout", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPbSURBVHhe7d1NixxlFMXx59YI2h0/gCIIScCtMX4Bv0DsHsGVSrLIPvSo4EIhCK5k0oh7MehCkCSduDch2WtchICIC9eCq+nZ2HWtMXdTdFUNU1P9ev4/qO7nLOaF7jP03Hp6phIAAAAAAAAAANhqFvclvcHoeiw7dXh3vJDPuy36ux9f9PzftyN2xsz+mk5ufBOxpLIA/eGex7I77vn07ngnEiqcGY7e92TfReyMu98sfviuRCzJ4h6iKIA4CiCOAoijAOKWOgXMbOe1SKjwXMoHxQO/H7EzTVPAiQtgyT84mIy/j1jSG46+tGQfRUTHilm+8vk6Ujz2vxSP/RsRSxgDUYsCiKMA4iiAOAogjgKIW+55gJSfj4QKWcqGZjaO2JlOzwO0xnbwsdgOxtJRAHEUQBwFEEcBxFVPAYO9z2I5x813a3edkj82t9sRyyy9VNy+/Cxsvunkxjux7Ex/cO1CSjuXIs6z9HmsqtwqnoDfYl32/7uC929GKqndXqzTdtuxvzt6vWjP44ibbUUjbdtt+ia8BIijAOIogDgKII4CiFubAhQTxO/rdsS3ttXWZww0vzC9M66eY1eg8R3OjIHYFhRAHAUQRwHEUQBxFEAcY2CNxjFwQTz5r4eT8cWIcxgD0TkKII4CiKMA4iiAOAogjgK04Z57ngatDvef47OsBc4D1FjUdnBvMPrWzC5HLOE8AJaOAoijAOIogDgKII4CiKMA4iiAOAogjgKIowDiKIC4dfoXMZdmKXsa65WzNPskS3Y1YhmbQQvx007K/1iXo/bJ3zK8BIijAOIogDgKII4CiKMA4pZWgKM3fM58drbrw91H8SUqVX3MqY8tuvTN0k4ELcpxl1lputzqKnAiCGuFAoijAOIogDgKIG7rp4DCnbhfD+5vJrNXI5WsYgpQKMDGYAzE0lEAcRRAHAUQRwHEVU4BveGHDVfFzD+tnQKSPyg69XXEOYeT/eqrip7C0RSQe6q90ukmMUt/e8r2I84pftO/Fct5Zl+5p4eRSrI8/+fg3vh+xJLKAjSNG62t6DIrm2RRI23TeM5LgDgKII4CiKMA4iiAOAog7uRjoPvV6TT7IVLJC/38i8zsWkR0bHpgL8ZyTu9M/qjNLu2JC9C07dj4/3Vxak3vcG67Tc9LgDgKII4CiKMA4iiAuBZjYPozVtUsnYtV2dFlVtx2I6GCmb9V3NT/sWvTY2/pleL2+WehrNMxsDW2g4/FdjCWjgKIowDiKIA4CiCuZgoY/RjLTk0n43djiQq9wd6wGAXfi9gZ9/SkmAKuRwQAAAAAAAAAAICGlP4DyTNJ/jCHRfEAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAFqSURBVDhPxVKxTgJhDG5PiXqTO3HSRIzunuy4conRB9C4A0/gC4C3m/gA6ACy6a43OLiQ6OJC2HUQTTB//VrvECSXmDD4Jf2v7bX9v7Y/zQrWYzGsHJjh6Pn9KrpXPQt+WNl1RMse0cugFV1bAb9c7SH9FuoKscTqy4TQfho7aDeKKDQbrIAQnwvLlt0uHPwWFuoPWqc1FcQcfce6k58C7Los/Ko60HPsolSE6EaY8sk/sr4Rq1+15807Dpb+RytqJpYNGAl1v1wLUGhTyB2D8gh/mwHThQ4MrXQTzwjTDNCzH1YbiaV2Xlkl1hQmGDjCENM12roAJJs/AxMMkt6tf+0ZRQLVPQKrcq2iM1B7HHN65Ao7TWJeW1gPhsOn+M58G9vYhjxgZXEqoNXxxNtDoWKuEKx+PsYde4lLYbWLXi+Z5BCc8SqzYSyEIiSWdLDWQvIGSnBGb+1GXX1ZwGVnTFwCo6mN/AeIvgA8YJ78TMEUWwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PanelPositionCenter, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PanelPositionEast, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PanelPositionNorth, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PanelPositionSouth, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PanelPositionWest, Type = Double, Dynamic = False, Default = \"4", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Group="Behavior"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
