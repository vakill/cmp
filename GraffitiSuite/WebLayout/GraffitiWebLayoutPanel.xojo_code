#tag Class
Protected Class GraffitiWebLayoutPanel
	#tag Method, Flags = &h0
		Sub Constructor(theContainer as WebControl, initSize as Integer = 0, SizeMin as Integer = 50, SizeMax as Integer = 0, IsClosable as Boolean = true, IsResizable as Boolean = True, IsSlidable as Boolean = True, IsHidden as Boolean = False, IsClosed as Boolean = False, CanOverflow as Boolean = False, PercentSize as Boolean = False)
		  ContainerPanel = theContainer
		  InitialSize = initSize
		  MinSize = SizeMin
		  MaxSize = SizeMax
		  Closable = IsClosable
		  Resizable = IsResizable
		  Slidable = IsSlidable
		  InitialClosed = IsClosed
		  InitialHidden = IsHidden
		  AllowOverflow = CanOverflow
		  SizePercentage = PercentSize
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		AllowOverflow As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Closable As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ContainerPanel As WebControl
	#tag EndProperty

	#tag Property, Flags = &h0
		IFrame As WebControl
	#tag EndProperty

	#tag Property, Flags = &h0
		InitialClosed As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		InitialHidden As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		InitialSize As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		MaxSize As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		MinSize As Integer = 50
	#tag EndProperty

	#tag Property, Flags = &h0
		Resizable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		SizePercentage As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Slidable As Boolean = True
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="AllowOverflow"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Closable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialClosed"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialHidden"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialSize"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxSize"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinSize"
			Group="Behavior"
			InitialValue="50"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Resizable"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SizePercentage"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Slidable"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
