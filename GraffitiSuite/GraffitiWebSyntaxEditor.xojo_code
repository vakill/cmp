#tag Class
Protected Class GraffitiWebSyntaxEditor
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Hide this.  For some reason, setting me.ContextMenu fails every time,
		  '// regardless of whether the control is completely instantiated.
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  select case Name
		  case "LibrariesLoaded"
		    LibrariesLoaded = True
		  case "rightclick"
		    '// X, Y, CTRL, Shift
		    ContextClick( Parameters(0), Parameters(1), Parameters(2).BooleanValue, Parameters(3).BooleanValue )
		  case "onChange"
		    if UBound( Parameters ) >= 0 then 
		      if Parameters(0) <> lastText then
		        mText = Parameters(0).StringValue
		        lastText = Parameters(0).StringValue
		        if UBound( Parameters )>= 1 then mLineCount = Parameters(1).IntegerValue
		        TextChange()
		      end if
		    end if
		  case "selChange"
		    if UBound( Parameters ) >= 0 then mSelStart = Parameters(0).IntegerValue
		    if UBound( Parameters ) >= 1 then mSelEnd = Parameters(1).IntegerValue
		    if UBound( Parameters ) >= 2 then mSelText = Parameters(2).StringValue
		    SelChange()
		  case "cursorChange"
		    if UBound( Parameters ) >= 0 then mCursorRow = Parameters(0).IntegerValue
		    if UBound( Parameters ) >= 1 then mCursorColumn = Parameters(1).IntegerValue
		    SelChange()
		  case "setBreakpoint"
		    if UBound( Parameters ) >= 0 then BreakpointSet( Parameters(0).IntegerValue )
		  case "clearBreakpoint"
		    if UBound( Parameters ) >= 0 then BreakpointCleared( Parameters(0).IntegerValue )
		  case "onScroll"
		    if UBound( Parameters ) >= 0 then mFirstVisibleRow = Parameters(0).IntegerValue
		    if UBound( Parameters ) >= 1 then mLastVisibleRow = Parameters(1).IntegerValue
		  case "undoManager"
		    if UBound( Parameters ) >= 0 then mCanUndo = Parameters(0).BooleanValue
		    if UBound( Parameters ) >= 1 then mCanRedo = Parameters(1).BooleanValue
		    UndoManager()
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LoadThemes()
		  LoadModes()
		  
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  Resize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<textarea id='" + me.ControlID + "_inner' style='display:none;'></textarea>" )
		  source.Append( "<div id='" + me.ControlID + "' style='display:none;'>" )
		  source.Append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AnnotationAdd(Row as Integer, theText as String, theType as String)
		  dim d as new Dictionary
		  d.Value( "Row" ) = Row
		  d.Value( "Text" ) = theText
		  d.Value( "Type" ) = theType
		  Annotations.Append( d )
		  
		  SetAnnotations()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AnnotationRemove(Row as Integer)
		  dim currD as Dictionary
		  for intCycle as Integer = 0 to UBound( Annotations )
		    currD = Annotations(intCycle)
		    if currD.Value( "Row" ).IntegerValue = Row then
		      Annotations.Remove(intCycle)
		      Return
		    end if
		  next
		  
		  SetAnnotations()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AnnotationRemoveAll()
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.session.clearAnnotations();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BreakpointAdd(Row as Integer)
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.session.setBreakpoint(" + Str( row ) + ");"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BreakpointRemove(Row as Integer)
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.session.clearBreakpoint(" + Str( row ) + ");"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub BreakpointRemoveAll()
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.session.clearBreakpoints();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAll()
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.clearSelection();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Find(theText as String, intStartPosition as Integer = 0, blnBackward as Boolean = false, blnWrap as Boolean = False, blnCaseSensitive as Boolean = False, blnWholeWord as Boolean = False, blnRegEx as Boolean = False, blnSkipCurrentLine as Boolean = False)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".find('" + EscapeString( theText ) + "',{" + _
		  "start: " + Str( intStartPosition, "#" ) + "," + _
		  "backwards: " + Lowercase( Str( blnBackward ) ) + "," + _
		  "wrap: " + Lowercase( Str( blnWrap ) ) + "," + _
		  "regExp: " + Lowercase( Str( blnRegEx ) ) + "," + _
		  "caseSensitive: " + Str( blnCaseSensitive ).Lowercase + "," + _
		  "skipCurrent: " + Lowercase( Str( blnSkipCurrentLine ) ) + "});"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FindNext()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".findNext();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FindPrevious()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".findPrevious();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GotoLine(theLine as Integer)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".gotoLine(" + Str( theLine, "#" ) + ");"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebSyntax -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "ace" ).Child( "ace.combined.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "ace" ).Child( "ace.breakpoint.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebSyntax -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Insert(theText as String)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".insert('" + EscapeString( theText ) + "');"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LoadModes()
		  AvailableModes = Array( "ABAP", "ActionScript", "ADA", "Apache_Conf", "AsciiDoc", "Assembly_x86", "AutoHotKey", "BatchFile", _
		  "C9Search", "C_Cpp", "Cirru", "Clojure", "Cobol", "coffee", "ColdFusion", "CSharp", "CSS", "Curly", _
		  "D", "Dart", "Diff", "Dockerfile", "Dot", "Eiffel", "EJS", "Erlang", "Forth", "FTL", "Gcode", "Gherkin", _
		  "Gitignore", "Glsl", "golang", "Groovy", "HAML", "Handlebars", "Haskell", "haXe", "HTML", "HTML_Ruby", _
		  "INI", "Io", "Jack", "Jade", "Java", "JavaScript", "JSON", "JSONiq", "JSP", "JSX", "Julia", "LaTeX", _
		  "LESS", "Liquid", "Lisp", "LiveScript", "LogiQL", "LSL", "Lua", "LuaPage", "Lucene", "Makefile", _
		  "Markdown", "MATLAB", "MEL", "MUSHCode", "MySQL", "Nix", "ObjectiveC", "OCaml", "Pascal", "Perl", _
		  "pgSQL", "PHP", "Powershell", "Praat", "Prolog", "Properties", "Protobuf", "Python", "R", "RDoc", _
		  "RHTML", "Ruby", "Rust", "SASS", "SCAD", "Scala", "Scheme", "SCSS", "SH", "SJS", "Smarty", "Soy_Template", _
		  "Space", "SQL", "Stylus", "SVG", "Tcl", "Tex", "Text", "Textile", "Toml", "Twig", "Typescript", "Vala", _
		  "VBScript", "Velocity", "Verilog", "VHDL", "XML", "Xojo", "XQuery", "YAML" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LoadThemes()
		  AvailableThemes = Array( "Ambiance", "Chaos", "Chrome", "Clouds", "Clouds Midnight", "Cobalt", _
		  "Crimson Editor", "Dawn", "Dreamweaver", "Eclipse", "GitHub", "Idle Fingers", "Katzenmilch", _
		  "KR Theme", "Kuroir", "Merbivore", "Merbivore Soft", "Mono Industrial", "Monokai", "Pastel on Dark", _
		  "Solarized Dark", "Solarized Light", "Terminal", "TextMate", "Tomorrow", "Tomorrow Night", _
		  "Tomorrow Night Blue", "Tomorrow Night Bright", "Tomorrow Night Eighties", "Twilight", "Vibrant Ink", _
		  "XCode" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Redo()
		  if not CanRedo then Return
		  
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".session.getUndoManager().redo();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Replace(replaceWith as String)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".replace('" + EscapeString( replaceWith ) + "');"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReplaceAll(strReplace as String)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".replaceAll('" + EscapeString( strReplace ) + "');"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ReplaceAll(strFind as String, strReplace as String)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".find('" + EscapeString( strFind ) + "');" + _
		  "window.gwSyntaxEditor_" + me.ControlID + ".replaceAll('" + EscapeString( strReplace ) + "');"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ResetUndoStack()
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.session.getUndoManager().reset();" + _
		  "editor.session.getUndoManager().markClean();" + _
		  "Xojo.triggerServerEvent('" + me.ControlID + "','undoManager',[editor.session.getUndoManager().hasUndo(),editor.session.getUndoManager().hasRedo()]);"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Resize()
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "editor.resize(true);"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollToLine(Row as Integer)
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".scrollToLine(" + Str( Row, "#" ) + ",false,true);"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectAll()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".selectAll();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectionIndent()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".indent();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectionOutdent()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".blockOutdent();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectionToggleComment()
		  dim strScript as String = "try { window.gwSyntaxEditor_" + me.ControlID + ".toggleCommentLines();} catch(err){ }"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectionToLowercase()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".toLowerCase();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectionToUppercase()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".toUpperCase();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectLine(Row as Integer)
		  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
		  "var newRange = new window.gwSyntaxEditorRange(" + Str( Row, "#" ) + ", 0, " + Str( Row + 1, "#" ) + ", 0);" + _
		  "editor.getSelection().setSelectionRange(newRange);"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetAnnotations()
		  dim strExec() as String
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "editor.session.setAnnotations([" )
		  
		  dim currAnn as Dictionary
		  dim intMax as Integer = UBound( Annotations )
		  for intCycle as Integer = 0 to intMax
		    currAnn = Annotations(intCycle)
		    strExec.Append( "{" )
		    strExec.Append( "row: " + currAnn.Value( "Row" ).StringValue + "," )
		    strExec.Append( "column: 0," )
		    strExec.Append( "text: '" + EscapeString( currAnn.Value( "Text" ) ) + "'," )
		    strExec.Append( "type: '" + currAnn.Value( "Type" ) + "'" )
		    strExec.Append( "}" )
		    if intCycle < intMax then strExec.Append( "," )
		  next
		  strExec.Append( "]);")
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFocus()
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".focus();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Undo()
		  if not CanUndo then Return
		  
		  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".session.getUndoManager().undo();"
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  dim strExec() as String
		  
		  strExec.Append( "function initRange_" + me.ControlID + "() {" )
		  strExec.Append( "if(!window.gwSyntaxEditorRange) {" )
		  strExec.Append( "setTimeout(initRange_" + me.ControlID + ", 100);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "if (!window.gwSyntaxEditor_" + me.ControlID + ") {" )
		  strExec.Append( "initEditor_" + me.ControlID + "();" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','rangeLoaded',[]);" )
		  strExec.Append( "}" )
		  strExec.Append( "}" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "function initEditor_" + me.ControlID + "() {" )
		  strExec.Append( "var rangeLoaded = false;" )
		  strExec.Append( "if(!window.ace || !window.ace.edit || !window.GSjQuery) {" )
		  strExec.Append( "setTimeout(initEditor_" + me.ControlID + ", 100);" )
		  strExec.Append( "return;" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "if(!window.gwSyntaxEditorRange) {window.gwSyntaxEditorRange = require('ace/range').Range;}" )
		  strExec.Append( "if(!window.gwSyntaxEditorRange) {" )
		  strExec.Append( "setTimeout(initRange_" + me.ControlID + ", 100);" )
		  strExec.Append( "return;" )
		  strExec.Append( "}" )
		  
		  strExec.Append( "window.gwSyntaxEditor_" + me.ControlID + " = window.ace.edit('" + me.ControlID + "');" )
		  
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "editor.$blockScrolling = Infinity" ) '// This removes a console message from the Ace Editor devs.  Nothing important.
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').mouseup(function(e){" )
		  strExec.Append( "if(e.which == 3){" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','rightclick',[e.clientX,e.clientY,e.ctrlKey,e.shiftKey]);" )
		  strExec.Append( "}" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "editor.on('guttermousedown', function(e){" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "var target = e.domEvent.target;" )
		  strExec.Append( "if (target.className.indexOf('ace_gutter-cell') == -1)" )
		  strExec.Append( "return;" )
		  strExec.Append( "if (!editor.isFocused())" )
		  strExec.Append( "return;" )
		  strExec.Append( "if (e.clientX > 25 + target.getBoundingClientRect().left)" )
		  strExec.Append( "return;" )
		  strExec.Append( "var row = e.getDocumentPosition().row;" )
		  strExec.Append( "if (e.editor.session.$breakpoints[row]) {" )
		  strExec.Append( "e.editor.session.clearBreakpoint(row);" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','clearBreakpoint',[row]);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "e.editor.session.setBreakpoint(row);" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','setBreakpoint',[row]);" )
		  strExec.Append( "}" )
		  strExec.Append( "e.stop();" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "editor.session.on('change', function(e) {" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').val(editor.getValue()).trigger('change');" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','onChange',[editor.getValue(),editor.session.getLength()]);" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','onScroll',[editor.getFirstVisibleRow(), editor.getLastVisibleRow()]);" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','undoManager',[editor.session.getUndoManager().hasUndo(),editor.session.getUndoManager().hasRedo()]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "editor.getSession().selection.on('changeSelection', function(e) {" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "var newRange = editor.getSelectionRange();" )
		  strExec.Append( "var absPosStart = editor.session.doc.positionToIndex(newRange.start);" )
		  strExec.Append( "var absPosEnd = editor.session.doc.positionToIndex(newRange.end);" )
		  strExec.Append( "var selText = editor.session.getTextRange(newRange);" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','selChange',[absPosStart, absPosEnd, selText]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "editor.getSession().selection.on('changeCursor', function(e) {" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "var newRange = editor.getSelectionRange();" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','cursorChange',[newRange.start.row, newRange.start.column]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('touchstart', function(e) {" )
		  strExec.Append( "var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];" )
		  strExec.Append( "var elm = window.GSjQuery(this).offset();" )
		  strExec.Append( "var x = touch.pageX - elm.left;" )
		  strExec.Append( "var y = touch.pageY - elm.top;" )
		  strExec.Append( "window.scrollStart_" + me.ControlID + "_X = x;" )
		  strExec.Append( "window.scrollStart_" + me.ControlID + "_Y = y;" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').on('touchmove', function(e) {" )
		  strExec.Append( "e.preventDefault();" )
		  strExec.Append( "var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0], ")
		  strExec.Append( "elm = window.GSjQuery(this).offset(), " )
		  strExec.Append( "x = touch.pageX - elm.left," )
		  strExec.Append( "y = touch.pageY - elm.top;" )
		  strExec.Append( "if(x < window.GSjQuery(this).width() && x > 0 && y < window.GSjQuery(this).height() && y > 0){" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + "," )
		  strExec.Append( "xDiff = x - window.scrollStart_" + me.ControlID + "_X," )
		  strExec.Append( "yDiff = y - window.scrollStart_" + me.ControlID + "_Y;" )
		  strExec.Append( "editor.renderer.scrollBy(-xDiff, -yDiff);" )
		  strExec.Append( "window.scrollStart_" + me.ControlID + "_X = x;" )
		  strExec.Append( "window.scrollStart_" + me.ControlID + "_Y = y;" )
		  strExec.Append( "}" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "editor.getSession().selection.on('scroll', function(e) {" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','onScroll',[editor.getFirstVisibleRow(), editor.getLastVisibleRow()]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "editor.getSession().selection.on('scroll', function(e) {" )
		  strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','onScroll',[editor.getFirstVisibleRow(), editor.getLastVisibleRow()]);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','executeBuffer',[]);" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','rangeLoaded',[]);" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "if( !window.ace || !window.ace.edit ) {" )
		  strExec.Append( "setTimeout(initEditor_" + me.ControlID + ", 100);" )
		  strExec.Append( "} else {" )
		  strExec.Append( "initEditor_" + me.ControlID + "();" )
		  strExec.Append( "}" )
		  
		  me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		  
		  RunBuffer()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event BreakpointCleared(Row as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event BreakpointSet(Row as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ContextClick(X as Integer, Y as Integer, CtrlKey as Boolean, ShiftKey as Boolean)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Ready()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelChange()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TextChange()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event UndoManager()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Ace Editor
		BSD License
		http://ace.c9.io/#nav=about
		
	#tag EndNote


	#tag Property, Flags = &h0
		Annotations() As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		AvailableModes() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		AvailableThemes() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCanRedo
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		CanRedo As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCanUndo
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		CanUndo As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCodeFolding
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCodeFolding = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".setShowFoldWidgets(" + Lowercase( Str( mCodeFolding ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		CodeFolding As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCursorColumn
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		CursorColumn As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCursorRow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		CursorRow As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mFirstVisibleRow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		FirstVisibleRow As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mFontSize
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFontSize = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".setFontSize(" + Str( mFontSize, "#" ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		FontSize As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHighlightActiveLine
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHighlightActiveLine = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".setHighlightActiveLine(" + Lowercase( Str( mHighlightActiveLine ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		HighlightActiveLine As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastText As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLastVisibleRow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		LastVisibleRow As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLineCount
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		LineCount As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mCanRedo As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCanUndo As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCodeFolding As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCursorColumn As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCursorRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFirstVisibleRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFontSize As Integer = 12
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHighlightActiveLine As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLastVisibleRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLineCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMode As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMode
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMode = value
			  
			  dim strExec() as String
			  if mMode <> "" Then
			    strExec.Append( "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
			    "editor.session.setMode({path:'ace/mode/" + Lowercase( mMode ) + "',v: Date.now()});" )
			  end if
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		Mode As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private ModeString As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReadOnly As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelEnd As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelStart As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowGutter As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowIndentGuides As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowInvisibles As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowPrintMargin As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSoftTabs As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSplitMode As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSplitView As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSyntaxChecking As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTheme As String = """Monokai"""
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWordWrap As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mReadOnly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReadOnly = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".setReadOnly(" + Lowercase( Str( mReadOnly ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		ReadOnly As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelEnd
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelEnd = value
			  
			  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
			  "var posStart = editor.session.doc.indexToPosition(" + Str( mSelStart, "#" ) + ");" + _
			  "var posEnd = editor.session.doc.indexToPosition(" + Str( mSelEnd, "#" ) + ");" + _
			  "var newRange = new window.gwSyntaxEditorRange(posStart.row, posStart.column, posEnd.row, posEnd.column);" + _
			  "editor.getSelection().setSelectionRange(newRange);"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		SelEnd As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelStart
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelStart = value
			  mSelEnd = value
			  
			  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";" + _
			  "var posStart = editor.session.doc.indexToPosition(" + Str( mSelStart, "#" ) + ");" + _
			  "var posEnd = editor.session.doc.indexToPosition(" + Str( mSelEnd, "#" ) + ");" + _
			  "editor.moveCursorTo(" + Str( mSelStart, "#" ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		SelStart As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		SelText As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowGutter
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowGutter = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".renderer.setShowGutter(" + Lowercase( Str( mShowGutter ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		ShowGutter As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowIndentGuides
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowIndentGuides = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".setDisplayIndentGuides(" + Lowercase( Str( mShowIndentGuides ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		ShowIndentGuides As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowInvisibles
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowInvisibles = value
			  dim strScript as String = "var editor = window.gwSyntaxEditor_" + me.ControlID + ";editor.setShowInvisibles(" + Lowercase( Str( value ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		ShowInvisibles As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowPrintMargin
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowPrintMargin = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".setShowPrintMargin(" + Lowercase( Str( mShowPrintMargin ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		ShowPrintMargin As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSoftTabs
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSoftTabs = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".session.setUseSoftTabs(" + Lowercase( Str( mSoftTabs ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		SoftTabs As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSyntaxChecking
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSyntaxChecking = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".getSession().setUseWorker(" + Lowercase( Str( mSyntaxChecking ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		SyntaxChecking As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mText = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".session.setValue('" + EscapeString( mText ) + "');"
			  
			  lastText = mText
			  
			  Buffer( strScript )
			  
			  me.SelStart = 0
			  me.SelEnd = 0
			  
			  ResetUndoStack()
			End Set
		#tag EndSetter
		Text As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTheme
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTheme = value
			  
			  dim strExec() as String
			  strExec.Append( "window.gwSyntaxEditor_" + me.ControlID + ".setTheme('ace/theme/" + Lowercase( ReplaceAll( mTheme, " ", "_" ) ) + "');" )
			  
			  Buffer( Join( strExec, "" ) )
			End Set
		#tag EndSetter
		Theme As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mWordWrap
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mWordWrap = value
			  
			  dim strScript as String = "window.gwSyntaxEditor_" + me.ControlID + ".session.setUseWrapMode(" + Lowercase( Str( mWordWrap ) ) + ");"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		WordWrap As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = AnnotationError, Type = String, Dynamic = False, Default = \"error", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AnnotationInfo, Type = String, Dynamic = False, Default = \"info", Scope = Public
	#tag EndConstant

	#tag Constant, Name = AnnotationWarning, Type = String, Dynamic = False, Default = \"warning", Scope = Public
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.syntaxeditor", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAqvSURBVHhe7d1tjFxVGQfw89zZ3e7caasFIxpIRW3Ed3yDoKZg1ZWmdndmqTHxgzF+aWIaujuzlYgQSoNgoezOkhqN8RMJfmrTnd1S+k5poZaX1giaQD8IIYiGECSF7sw23TnH58w+q/sy98w9s/dtxuf3Ze5zd6Gz8/zPvefcvTMrGGOMMcYYY4wxxhhjjDHGGGOMMcYYY4wxxhhjjDHGGGOMMdZSgB5ZHW6usF0JsUko9WdwxJMqJY5X9hbfpC+3BQ6AB7c/v14oOEjl/yh1SqmOH1Umdv2T9rQ0hx7ZQlKsp635AG4WTvUxqloeB8ADHvpvpc1FQODJoE1wAOpY0TdwHQB8msrFJByhrZbHAahjOuV4jn5NCXmYNlseB6AeBYYAqDcqE6NnqWh5HICFNm/uBOV9/se5QduMfo0DsED6LXc9zvK8J3nS4QC0MxCmw78QruAARKZ749bV7oZtH6EyGuAdADz8H3tnYtf7VJrd8pPuTDb/BaoSK7EBcLOFO5yOjtdFl/wXvpBDtDtUmU2Dn8OHNTPVYrj+97X8S/fnb3JXXXleAbzkZvMl2p1IiQyAbj6+2g9SiZNyeDjdl99KZWiq01D/6h+Baqrh4V83X0g4gJurazsAsm6uMFHbTqDEBWBh82eBA4+kc4WfURkKMFz9E0r9fXL/wy9RVdds8wHEFbRrVm9SQ5CoAHg1fxY26LfpbH4TlcHauNkFgB6qFgMwjv7u24Y+5tH8WYkMQWIC0Kj5c/yUHgOVdjLG2b8C8/rfqap1hubPSlwIEhEAi+bjQHT20Gag8BRjOPyLaqVy2RgA/O/P4jdOU2mSqBDEHgCb5uMLfEe5NPwoFUHzXv7p0X9o9yUq65ocG/4bfuNGHRbaZZKYEMQaAKvmK/WLcqm4i6pAudmBL+HDtTNVPdLX8q88PnJYgNyAz1XSLpNEhCC2ANiNfHFnebzo93ubYP7tX2ra/9W/cmn0CJ4O9HJSzewxij0EsQTAbuTLu8qlkZ1UhUKZrv4p8fLFx0deodKXybGRozghNIZqjlhDEHkALEf+3eXx0QdoOxRXrL99JS4v11G5CDbS9+ifqxYCKb2XlfPFFoJIA2A38sU9OPLvpyo0lXSneaSCaioA2uTE6DFQ4rtUNhJLCCILgOXIvxcnVPfRdrik8r78q0SlPFZsOgDa5PjI8SSHIJIA2DRfKbUDR/4OKkMHAr5Hm4vNjH4/kzmjWghAfofKRiINQegBsGq+UPdVxov3Uhm6dN/g1/C5XUPlIkoEd/Pn5NjokyDVt6lsJLIQhBoAy5F/f6VUvIfKSIBjXv7JKhyizUBMThRP2IUgH3oIQguA3ciHB3Dk301llEzLvxcv7R9+jcrA1EKgwHPVMR+EHoJQAmA58ndWSsN3URmZFbktV+LD2pmqjiXM/huZHB9+Sgr1LSobCDcEgQfArvmwE0f+nVRGSqou4+HfCTEA2lSpeFI6cAuVDYQXgkADYN/84Viar0nDzZ94VLqgJ21UhmZq3/ApKeMNQWABaKXm14DyXP7h0jDU0T/X1ASGQMmbqWwg+BAEEoBWa76+dQubbLjb2N/Nn0GZGh99WprmI/MEG4IlB6DlRr7m9dZvojpFoMs/P6ZKI8/EEYIlBaAlm4+MV/+UeiGuTwGJIwRNB6BVm5/p3XoVPu+vU7mIivD8X08tBBHOCZoKQKs2X1MdKePyr9HNn1GozQkiWh1YB6CVm6/hczJd/Xtbj0AqY1VbHURwscgqANj8W1u5+TVKmY4AsY/+ufTFIqvLxtn876jwze4IAKal03xKVJ+gzcTozg6uBQB9CbiusK/+NUNfNsZX8xyVZiBW0ZZvVgGo3ZKt1GkqjRxwTnVnC9+gMhEavfUbOqYTFwA8tJ/FZ/ZVKo1Ait/Tpm/Wc4BlXV3f9x8CcTrdm7+JytiBEKbl3+mLe3e/TVUi2DQfj84b9W8aqfLNOgDv7nnwgk0IIAVn0v1DN1AZm/QP8lfji2R4HvEu/xaybX55bFi/I9madQA06xAo9bybG/L3w4QEps2Hf+UEe/PHUkTVfK2pAGi2IcCX+KzbP/gVKqJn+OAn9GZlbPgF2o4VzuSfj6r5WtMB0KxDoJxzbl/hy1RFSgnv5Z9SyZj94zL7OfNpao4Amq8tKQCadQgccc7NbtPvxYtMpi+/Dpd/H6ByEZWA5R82/wzOUm+k0iyg5mtLDoBmGQKcjFfP0hsyIyFNb/1GbqYa6/kfz/mn8VXxt1oKsPlaIAHQrEIAkMJ/+jm3P3897QmZ980fSogT//7j7veojByO/GfwBfF3vSTg5muBBUCzDEGXkHAms2nbF2lPKJZlB64FAZ7zDlyhxHb4d3OFUzjyv0mlWQjN1wINgGYXApFW09U/ZXq3hhYCp8Fbv/EbYjn848h/Ch/8/e4/pOZrgQdAszwSZGSq4+mwPlTRePVPqNfKY8UXqYhMOpc/gU/M3697Q2y+FkoANJsQYJNWShAnM/1Dn6ddQfI+Aqjor/6ls4XjeEry92vekJuvhRYAzS4EsEoqeSLTvzWwEGT6Cz36CEPlIlF/8jeO/GMAwt9bwyJovhZqADTLEHxIytTxTG/tI1uXTJqv/qnKB1dEdv5PZ/NH8Ofz9w7hiJqvhR4AzSoEAB+WjnN0ed/QZ2lX8ww3fyh96/ejO6aoDBWO/EPGD6GcK8Lma5EEQLMLgfioBHV4KSFY1ptfgy+69+lERTP7x3P+QRz55pXIrIibr0UWAM0mBHg+uEaCPLg8N/AZ2mPFMXzwk5aC8P/uTzpX0B8da3wPwn/F0Hwt0gBodiGA1VI4B5bfVvD+C14ewDFe/Tt/sfTIy1SGAs/5+3F1s4FKs5iar0UeAM0qBAI+LqviwIq+n19HOxrbvh1/Lu8jAKhwZ/9uLj+Bp5+NVJrF2HwtlgBolqeDT1RhGkMw4CsE7l/e04fdZTNVHRDezR9utlDCf6CXSrOYm6/FFgDN8nTwyaqTelxf26c9Joarf+JS+frloRwB3FxhH4Y1S6VZApqvxRoAze50INY4kGr4OUJ4jjdMANVhsWOHn8/ytdKdG/whPvTPVA0kpPla7AHQbEKAE6sLtFmXnjDi+ddz0qhCOv8DOP6uKSSo+VoiAqD5CQGO7CfKpZE8lXVVpfHqn5ApFcr5vzI2MoHh+jWV9SWs+VpiAqCZQqCEOlrpfKOPShPT8u+vl/aNvkpl4CrjI7/0DEECm68lKgBa3RAocbLSgZOrPXvMf4xh/e3LTH/2Ff8/oV/9qxuChDZfS1wANB2CcqfowVH/EL6YvypfdbFH7C1W6Mue0l2d+s2rnn/2FX/YSH77NxMC2ILP/Q9KyhuS2nwN51Ttw83mf4OjbQuVC6j3y6XiSioYSeQRoGnm6/+RjP5W0zYBoLuJPP/saxx3/7SC9jkCSGlc/imnI7KbP1pJ2wRAmj75S6hzlbGH/kEFm6M9AtAzlAEwrP8juvmjFbVFANIZ89U/XE7y+d9DexwBlPJ85w+O/nf0x65RyRZoiwA4Sp7EUf4ulfMl4HP/kszzqlkruXz+2VfTn1pbUo56XQkxDUJcjbs79dcA1I8vv/LsW3qb/R9J5/I3NnMvIWOMMcYYY4wxxhhjjDHGGGOMMcYYY4wxxhhjLUaI/wCEC1Kdc6cmiwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEZSURBVDhPYxgBgCugMBXKxAqYoDQccPjkyUGZEPCfoQ5EcfoVmID5aIAZSoMBV0CBGyMj00ZmVbO1f26d/MgRUBDGyMDEy6pp/omBkWkLm4b5ud83TtyFKgcDuAtAmhn+M85lYPyf/GPLpEcgMUYGRldGhv+nvm2YsIvp//+m/wxMq8HqkADYAGTNIMVgGQiw+vv372oQ4+vG/l5shoANYPzPqPufkeHTvz//boBFgQDmZ5hrQOA/4//LEBajPISGGgAxnWEeIwvzdnggMjL5MjIw7ASzgQBkK8h2kCu+beifDRVGhAGGIYz/zYF27gDJgVwD0wxSB9aAC4BCHkRz+Rc+BgsAAchAQukBBYBsBGrog3KHN2BgAACf8GxHV0mELQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Modes, Flags = &h0
		ABAP
		  ABC
		  ActionScript
		  ADA
		  ApacheConf
		  AppleScript
		  ASCIIDoc
		  AssemblyX86
		  AutoHotKey
		  BatchFile
		  Bro
		  C_CPP
		  C9Search
		  Cirru
		  Clojure
		  Cobol
		  CoffeeScript
		  ColdFusion
		  CSharp
		  CSS
		  Curly
		  D
		  Dart
		  Diff
		  Django
		  DockerFile
		  Dot
		  Drools
		  Eiffel
		  EJS
		  Elixir
		  Elm
		  ERLang
		  Forth
		  Fortran
		  FTL
		  GCode
		  Gherkin
		  GitIgnore
		  GLSL
		  Gobstones
		  GOLang
		  Groovy
		  HAML
		  Handlebars
		  Haskell
		  HaskellCabal
		  Haxe
		  HJSON
		  HTML
		  HTMLElixir
		  HTMLRuby
		  INI
		  IO
		  Jack
		  Jade
		  Java
		  JavaScript
		  JSON
		  JSONIQ
		  JSP
		  JSX
		  Julia
		  Kotlin
		  LaTeX
		  Lean
		  LESS
		  Liquid
		  LISP
		  Live_Script
		  LiveScript
		  LogIQL
		  LSL
		  LUA
		  LUAPage
		  Lucene
		  MakeFile
		  Markdown
		  Mask
		  MatLab
		  Maze
		  Mel
		  MIPS_Assembler
		  MIPSAssembler
		  MushCode
		  MySQL
		  Nix
		  NSIS
		  ObjectiveC
		  OCAML
		  Pascal
		  Perl
		  PGSQL
		  PHP
		  PlainText
		  PowerShell
		  Praat
		  Prolog
		  Properties
		  Protobuf
		  Python
		  R
		  Razor
		  RDoc
		  RHTML
		  RST
		  Ruby
		  Rust
		  SASS
		  SCAD
		  Scala
		  Scheme
		  SCSS
		  SH
		  SJS
		  Smarty
		  Snippets
		  SoyTemplate
		  Space
		  SQL
		  SQLServer
		  Stylus
		  SVG
		  Swift
		  Swig
		  TCL
		  Tex
		  Text
		  Textile
		  TOML
		  TSX
		  Twig
		  TypeScript
		  Vala
		  VBScript
		  Velocity
		  Verilog
		  VHDL
		  Wollok
		  XML
		  Xojo
		  XQuery
		YAML
	#tag EndEnum

	#tag Enum, Name = Themes, Flags = &h0
		Ambiance
		  Chaos
		  Chrome
		  Clouds
		  CloudsMidnight
		  Cobalt
		  CrimsonEditor
		  Dawn
		  Dreamweaver
		  Eclipse
		  GitHub
		  IdleFingers
		  iPlastic
		  Katzenmilch
		  KRTheme
		  Kuroir
		  Merbivore
		  MerbivoreSoft
		  MonoIndustrial
		  Monokai
		  PastelOnDark
		  SolarizedDark
		  SolarizedLight
		  SQLServer
		  Terminal
		  TextMate
		  Tomorrow
		  TomorrowNight
		  TomorrowNightBlue
		  TomorrowNightBright
		  TomorrowNightEighties
		  Twilight
		  VibrantInk
		XCode
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="CanRedo"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CanUndo"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CodeFolding"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CursorColumn"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CursorRow"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FirstVisibleRow"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FontSize"
			Visible=true
			Group="Text"
			InitialValue="12"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HighlightActiveLine"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LastVisibleRow"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LineCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Visible=true
			Group="Behavior"
			InitialValue="ABAP"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SelEnd"
			Group="Text"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SelStart"
			Group="Text"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SelText"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowGutter"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowIndentGuides"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowInvisibles"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowPrintMargin"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SoftTabs"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SyntaxChecking"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Visible=true
			Group="Text"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Theme"
			Visible=true
			Group="Behavior"
			InitialValue="Chrome"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WordWrap"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
