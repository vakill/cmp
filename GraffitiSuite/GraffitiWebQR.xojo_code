#tag Class
Protected Class GraffitiWebQR
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "toDataURL"
		      if Parameters.Ubound < 0 then Return True
		      if Parameters(0).StringValue.CountFields( "," ) <= 1 then Return True
		      Image = Picture.FromData( DecodeBase64( Parameters(0).StringValue.NthField( ",", 2 ) ) )
		      ImageAvailable()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  DoResize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "'>" )
		  'source.Append("<canvas width=""" + Str( me.Width, "#" ) + """ height=""" + Str( me.Height, "#" ) + """ id=""" + me.ControlID + "_qrcode"">")
		  source.append("</canvas>")
		  source.append("</div>")
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub DoResize()
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + " > canvas').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebQR -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwqrcode.min.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebQR -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + " > canvas').remove();" )
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').qrcode('" + EscapeString( QRValue ) + "');" )
		    
		    DoResize()
		    
		    me.ExecuteJavaScript( "Xojo.triggerServerEvent('" + me.ControlID + "', 'toDataURL', [window.GSjQuery('#" + me.ControlID + " > canvas')[0].toDataURL()]);" )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event ImageAvailable()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://jeromeetienne.github.io/jquery-qrcode/
		
		MIT License
		
	#tag EndNote


	#tag Property, Flags = &h0
		Image As Picture
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return misShown
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  misShown = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private isShown As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private misShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  if mValue = "" then mValue = "No Text"
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		QRValue As String
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.qrcode", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANlSURBVHhe7d2/axNhHMfx5zlFTIogLgqOooMOKg5O4iSior2ii6DgXyC9FkEHESfBSlIEB50UheogvaI4uAluYrWKm4OroG5Jqm3v8YrfJdwlhfYu99Tv+wUPeb70SXoJH3J88+SHAQAAAAAAAAAAAAAAAAD8L6xclmLo7Og+mfrhT/Kj9eLud6kyfDve1vPJLzItTakBqIdjTqZecMbd6cTNK1J2GQqjC87Yx1L6wboD7enmnFSlCOQSShEA5QiAcgRAOQKgHAFQrsI20F1P25xXUhQnse+MtbnBXlMbaJNDMiuMNXaPc3ZKyqwBtIGVBcAad7EVN59IWZj6cLRURgDacaPwx6o+Eu03zn6UMovXAVA2AqAcAVCOAChHAJTzsguoh9E158wRKTM6M82TMs2oogtI/+ez9I5ukbKbtZ86ceOqVF3oAnpIH8xt1toTucOY47LMG86a3bnHmg7j3A5Z5iVOAcoRAOUIgHIEQDkCoJy3AUg7gc+5w9rS3ym7Cr9yjzUdxgYLssZL7AYKdgOhEgFQjgAoRwCUIwDKEQDlKmsDq8CHQ7N4BlCOAChHAJQjAMoRAOUIgHLltoHDY5dk6oXEmq/zceOtlF1q56KddsEek9IL7ZnGQ5kCAAAAQEFKbQNrw9EtmXrBBWZufrr5VMoutTPjh41NQimL9K0z07wv8y5Dpy9vT4INo1JmBMnSZL/vNi4C28GirO1g59yjNAC5r4fwrmBUjgAoRwCUIwDKEQDlCEDJbGC2Lv8UTd4IXLJLllWmsjZwvX04tBK0gSgbAVCOAChHAJQjAMoRAOW8DEAtjCbSdu53ryHLBmv5J2N6jLS9/CCr1h1/nwGs3ZQ7jNn4b8FgtacnZ3sNWbIucQpQjgAoRwCUIwDKEQDlvNwNrI+MnzIu2StlRjtuTsg0I20TB/+TMWGUe5tr1V5s3TMvH7SlLAXbwaKK7wr2AacA5QiAcgRAOQKgHAFQrrIuwDk3lf7xvZSFSRN9e9BdwOZw7LxMMwKT/GzHk6+lzOh33fm4MSXT0lQWgCqUFYBaGM1aYw9K2YUPh8JrBEA5AqAcAVCOAChHAJQrd4fr6I1K3sDZ05ubizLL1+94+113pfu52uuudLwAAAAAAAAAAAAAAAAAAAAAAAAAAH2M+Qs4N6t/G4qeFgAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAExSURBVDhP7ZK9SsRQEIXPuVlEBEFr18ZGwRfYxUIs7Bf8ew23ECx9All8CJG1EatY2qWwFbayURsLERsF1xnn3gyu1zyAjR+EmTlJJpNzB38OPSZmev1jTxMqehrIdSUWXIJAq/eLwdBLBI81ih3PLGdHA5YU3HIlaQHseJXIG4D39lRJxSOolYuGlgrM5lrNrwY24lhGonrtZSJqhJ57mdFoEIrWGRlO6nHxotTbqEHDUdTSdD/ITJzu7e96WjOWKrTCigBzqZngOcpvl4ObdN8oPCamlrslyVeCbWrY1gIPFg9N65vBGzbZGgPmP0bVlb/SNDEeXTyqzDDFEGTb5u268k3DAxV5ouDOy4R8jg8Eshev+AGXE1kDW5jViYmTnYg+2G9tFsrFuFgu/5MAvgDKO3BkOJ2gvQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="QRValue"
			Visible=true
			Group="Behavior"
			InitialValue="No text"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Image"
			Group="Behavior"
			Type="Picture"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
