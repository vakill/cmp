#tag Class
Protected Class GraffitiWebDatePickerSingle
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  if not IsNull( Session ) then
		    Session.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_widgetCalendar').detach();" )
		  end if
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "changeMonth"
		      if Parameters.Ubound >= 0 then
		        dim d as date = StringToDate( Parameters(0).StringValue )
		        if not IsNull( d ) then MonthChange( d.Month )
		      end if
		    case "changeYear"
		      if Parameters.Ubound >= 0 then
		        dim d as date = StringToDate( Parameters(0).StringValue )
		        if not IsNull( d ) then YearChange( d.Year )
		      end if
		    case "changeDate"
		      if Ticks - ticksClear <= 50 then Return False
		      if Parameters.Ubound >= 0 then
		        if lastValue = Parameters(0).StringValue then Return False
		        lastValue = Parameters(0).StringValue
		        ticksValue = Ticks
		        dim d as date = StringToDate( Parameters(0).StringValue )
		        if not IsNull( d ) then
		          if IsNull( mValue ) or d.ShortDate <> mValue.ShortDate then
		            mValue = d
		            ValueChange()
		          end if
		        end if
		      end if
		    case "textChange"
		      if not ForceParse then
		        if Parameters.Ubound < 1 then
		          mValue = nil
		          PickerCleared()
		        else
		          dim theText as String = Parameters(0)
		          if theText <> lastText then TextChanged( theText )
		        end if
		      else
		        if Parameters.Ubound = 0 then
		          mValue = nil
		          PickerCleared()
		        elseif Parameters.Ubound = 1 then
		          if Ticks - ticksClear <= 50 then Return False
		          if lastValue = Parameters(1).StringValue then Return False
		          lastValue = Parameters(1).StringValue
		          ticksValue = Ticks
		          dim d as date = StringToDate( Parameters(1).StringValue )
		          if not IsNull( d ) then
		            if IsNull( mValue ) or d.ShortDate <> mValue.ShortDate then
		              mValue = d
		              ValueChange()
		            end if
		          end if
		        end if
		      end if
		    case "onfocus"
		      GotFocus()
		    case "onblur"
		      LostFocus()
		    case "mouseenter"
		      MouseEnter()
		    case "mouseexit"
		      MouseExit()
		    case "pickerHide"
		      PickerHidden()
		    case "pickerShow"
		      PickerShown()
		    case "pickerClear"
		      'if Ticks - ticksClear <= 750 then Return False
		      mValue = nil
		      PickerCleared()
		    end select
		  Catch
		    
		  End Try
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  Select Case Name
		  case "enabled"
		    Buffer( "window.GSjQuery('#" + me.ControlID + "').prop('disabled'," + Lowercase( Str( not value.BooleanValue ) ) + ");" )
		    Return True
		  case "style"
		    dim sVal as WebStyle = Value
		    if not IsNull( sVal ) then
		      if sVal.Name <> "_DefaultStyle" then
		        Buffer( "window.GSjQuery('#" + me.ControlID + "').addClass('" + sVal.Name + "')" + if(OldStyle <> nil, ".removeClass('" + OldStyle.Name + "')", "") + ";" )
		        OldStyle = sVal
		      end if
		    end if
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  me.AvailableLanguages = Array( "en","ar","az","bg","bs","ca","cs","cy","da","de","el","en-GB","es","et","eu","fa","fi","fo","fr","fr-CH","gl","he","hr","hu","hy","id","is","it","ja","ka","kh","kk","kr","lt","lv","mk","ms","nb","nl","nl-BE","no","pl","pt-BR","pt","ro","rs","rs-latin","ru","sk","sl","sq","sr","sr-latin","sv","sw","th","tr","uk","vi","zh-CN","zh-TW")
		  
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<input type='text' id='" + me.ControlID + "' class='form-control' style='position: absolute;box-sizing:border-box;display:none;'>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ClearValue()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('clearDates');" )
		  'strExec.Append( "var theDP = window.GSjQuery('#" + me.ControlID + "');" )
		  'strExec.Append( "theDP.val('');" )
		  'strExec.Append( "theDP.removeData();" )
		  ticksClear = ticks
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DatesToString(theDates() as Date) As String
		  dim strOut as String = "["
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( theDates )
		  for intCycle = 0 to intMax
		    dim currDate as Date = theDates(intCycle)
		    if not IsNull( currDate ) then
		      strOut = strOut + "'" + currDate.ShortDate + "'"
		      if intCycle < intMax then strOut = strOut + ","
		    end if
		  next
		  
		  strOut = strOut + "]"
		  
		  Return strOut
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DateToDate(theDate as Date) As String
		  dim strRet() as String
		  
		  dim dWork as Date = theDate
		  
		  if not IsNull( theDate ) then
		    strRet.Append( "new Date(" )
		    strRet.Append( Str( theDate.Year, "####" ) + "," )
		    strRet.Append( Str( theDate.Month - 1, "00" ) + "," )
		    strRet.Append( Str( theDate.Day, "00" ) )
		    strRet.Append( ")" )
		  end if
		  
		  Return Join( strRet, " " )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DateToObject(theDate as Date) As String
		  dim strRet() as String
		  
		  if not IsNull( theDate ) then
		    strRet.Append( "{" )
		    strRet.Append( "year: " + Str( theDate.Year, "####" ) + "," )
		    strRet.Append( "month: " + Str( theDate.Month, "00" ) + "," )
		    strRet.Append( "day: " + Str( theDate.Day, "00" ) )
		    strRet.Append( "}" )
		  end if
		  
		  Return Join( strRet, " " )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DisableDate(theDate as Date)
		  if IsNull( theDate ) then Return
		  
		  DatesDisabled.Append( theDate )
		  UpdateDatesDisabled()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EnableDate(theDate as Date)
		  if IsNull( theDate ) then Return
		  
		  for intCycle as Integer = UBound( DatesDisabled ) DownTo 0
		    if DatesDisabled(intCycle).TotalSeconds = theDate.TotalSeconds then
		      DatesDisabled.Remove( intCycle )
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HidePicker()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('hide');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebDatePicker -->" )
		  
		  strOut.Append( AddjQuery( CurrentSession ) )
		  strOut.Append( AddjQueryUI( CurrentSession ) )
		  strOut.Append( AddBootstrap4( CurrentSession ) )
		  strOut.Append( AddDatepicker( CurrentSession ) )
		  
		  if not Session.GraffitiSuiteInstances.HasKey( "bootstrapDP-noConflict" ) then
		    strOut.Append( "<script type='text/javascript'>" )
		    strOut.Append( "if (typeof window.GSjQuery.fn.datepicker_bs == 'undefined') {" )
		    strOut.Append( "var dp_noconflict = window.GSjQuery.fn.datepicker.noConflict();" )
		    strOut.Append( "window.GSjQuery.fn.datepicker_bs = dp_noconflict;" )
		    strOut.Append( "}" )
		    strOut.Append( "</script>" )
		    Session.GraffitiSuiteInstances.Value( "bootstrapDP-noConflict" ) = True
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebDatePicker -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDayEnabled(theValue as Boolean, ParamArray theDays as Integer)
		  for each theDay as Integer in theDays
		    if theDay = 0 then
		      mDay0Enabled = theValue
		    elseif theDay = 1 then
		      mDay1Enabled = theValue
		    elseif theDay = 2 then
		      mDay2Enabled = theValue
		    elseif theDay = 3 then
		      mDay3Enabled = theValue
		    elseif theDay = 4 then
		      mDay4Enabled = theValue
		    elseif theDay = 5 then
		      mDay5Enabled = theValue
		    elseif theDay = 6 then
		      mDay6Enabled = theValue
		    end if
		  next
		  
		  UpdateDaysOfWeekDisabled
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetDayHighlighted(theValue as Boolean, ParamArray theDays as Integer)
		  for each theDay as Integer in theDays
		    if theDay = 0 then
		      mDay0Highlighted = theValue
		    elseif theDay = 1 then
		      mDay1Highlighted = theValue
		    elseif theDay = 2 then
		      mDay2Highlighted = theValue
		    elseif theDay = 3 then
		      mDay3Highlighted = theValue
		    elseif theDay = 4 then
		      mDay4Highlighted = theValue
		    elseif theDay = 5 then
		      mDay5Highlighted = theValue
		    elseif theDay = 6 then
		      mDay6Highlighted = theValue
		    end if
		  next
		  
		  UpdateDaysOfWeekHighlighted()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFocus()
		  Buffer( "window.GSjQuery('#" + me.ControlID + "').focus();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetValue(newValue as String)
		  dim d as new date
		  if ParseDate_GS( newValue, d ) then me.Value = d
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowPicker()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('show');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function StringToDate(theString as String) As Date
		  if CountFields( theString, "/" ) < 3 then Return nil
		  
		  dim strMonth as String = NthField( theString, "/", 1 )
		  dim strDay as String = NthField( theString, "/", 2 )
		  dim strYear as String = NthField( theString, "/", 3)
		  
		  if not IsNumeric( strMonth ) or not IsNumeric( strDay ) or not IsNumeric( strYear ) then Return nil
		  
		  return new date( strYear.Val, strMonth.Val, strDay.Val )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateDatesDisabled()
		  dim strOut() as String
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( DatesDisabled )
		  
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('setDatesDisabled', [" )
		  for intCycle = 0 to intMax
		    strOut.Append( DateToObject( DatesDisabled(intCycle) ) )
		    if intCycle < intMax then strOut.Append( "," )
		  next
		  strOut.Append( "]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateDaysOfWeekDisabled()
		  dim arrDays() as String
		  if not mDay0Enabled then arrDays.Append( "0" )
		  if not mDay1Enabled then arrDays.Append( "1" )
		  if not mDay2Enabled then arrDays.Append( "2" )
		  if not mDay3Enabled then arrDays.Append( "3" )
		  if not mDay4Enabled then arrDays.Append( "4" )
		  if not mDay5Enabled then arrDays.Append( "5" )
		  if not mDay6Enabled then arrDays.Append( "6" )
		  
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('setDaysOfWeekDisabled', [" )
		  strOut.Append( Join( arrDays, "," ) )
		  strOut.Append( "]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateDaysOfWeekHighlighted()
		  dim arrDays() as String
		  if not mDay0Highlighted then arrDays.Append( "0" )
		  if not mDay1Highlighted then arrDays.Append( "1" )
		  if not mDay2Highlighted then arrDays.Append( "2" )
		  if not mDay3Highlighted then arrDays.Append( "3" )
		  if not mDay4Highlighted then arrDays.Append( "4" )
		  if not mDay5Highlighted then arrDays.Append( "5" )
		  if not mDay6Highlighted then arrDays.Append( "6" )
		  
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('setDaysOfWeekHighlighted', [" )
		  strOut.Append( Join( arrDays, "," ) )
		  strOut.Append( "]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    if not FirstLoad then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('remove');" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs({" )
		    if not IsNull( DefaultViewDate ) then strExec.Append( "defaultViewDate: " + DateToObject( mDefaultViewDate ) + "," )
		    if not IsNull( StartDate ) then strExec.Append( "startDate: '" + mStartDate.ShortDate + "'," )
		    if not IsNull( EndDate ) then strExec.Append( "endDate: '" + mEndDate.ShortDate + "'," )
		    strExec.Append( "keyboardNavigation: " + if(mKeyboardNavigation, "true", "false") + "," )
		    strExec.Append( "todayHighlight: " + if(mTodayHighlight, "true", "false") + "," )
		    strExec.Append( "calendarWeeks: " + if(mCalendarWeeks, "true", "false") + "," )
		    if mTodayButton then strExec.Append( "todayBtn: 'linked'," )
		    if mClearButton then strExec.Append( "clearBtn: 'enabled'," )
		    strExec.Append( "language: '" + mLanguage + "'," )
		    strExec.Append( "weekStart: " + Str( mWeekStart, "#" ) + "," )
		    if mOrientation <> "auto" then strExec.Append( "orientation: '" + mOrientation + "'," )
		    strExec.Append( "minViewMode: " + Str( mMinView, "#" ) + "," )
		    strExec.Append( "enableOnReadyonly: false," )
		    strExec.Append( "format: '" + EscapeString( mFormat ) + "'," )
		    strExec.Append( "DisableKeyboard: " + if(mDisableKeyboard, "true", "false") + "," )
		    strExec.Append( "immediateUpdates: true," )
		    strExec.Append( "forceParse: " + If( mForceParse, "true", "false" ) + "," )
		    strExec.Append( "toggleActive: " + if(mToggleSelect, "true", "false" ) + "," )
		    strExec.Append( "showOnFocus: " + if(mShowOnFocus, "true", "false" ) + "," )
		    strExec.Append( "autoclose: " + if( mAutoClose, "true", "false" ) + "," )
		    strExec.Append( "datesDisabled: " + DatesToString( DatesDisabled ) + "" )
		    strExec.Append( "});" )
		    
		    if FirstLoad then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').focus( function (e) {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onfocus');" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').blur( function (e) {" )
		      strExec.Append( "var theText = window.GSjQuery('#" + me.ControlID + "').val();" )
		      'strExec.Append( "if(theText.length == 0) {" )
		      'strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pickerClear');" )
		      'strExec.Append( "return;" )
		      'strExec.Append( "}" )
		      strExec.Append( "var theDate = window.GSjQuery('#" + me.ControlID + "').datepicker_bs('getFormattedDate','mm/dd/yyyy');" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onblur', [theText]);" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'textChange', [theText, theDate]);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').mouseenter( function (e) {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseenter');" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').mouseleave( function (e) {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseexit');" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs().on('show', function () {" )
		      
		      strExec.Append( "var maxZ = Math.max.apply(null,window.GSjQuery.map(window.GSjQuery('*'), function(e,n){" )
		      strExec.Append( "if(window.GSjQuery(e).css('position')=='absolute')" )
		      strExec.Append( "return parseInt(window.GSjQuery(e).css('z-index'))||1 ;" )
		      strExec.Append( "})" )
		      strExec.Append( ");" )
		      strExec.Append( "var thePopup = window.GSjQuery('.datepicker-dropdown');" )
		      strExec.Append( "thePopup.css('z-index', maxZ + 1);" )
		      
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pickerShow');" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs().on('hide', function () {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pickerHide');" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs().on('clearDate', function () {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pickerClear');" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs().on('changeDate', function (ev) {" )
		      strExec.Append( "var theDate = window.GSjQuery('#" + me.ControlID + "').datepicker_bs('getFormattedDate','mm/dd/yyyy');" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'changeDate', [theDate]);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs().on('changeMonth', function (ev) {" )
		      strExec.Append( "var theDate = ev.date;" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'changeMonth', [(theDate.getMonth() + 1) + '/' + theDate.getDate() + '/' + theDate.getFullYear()]);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs().on('changeYear', function (ev) {" )
		      strExec.Append( "var theDate = ev.date;" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'changeYear', [(theDate.getMonth() + 1) + '/' + theDate.getDate() + '/' + theDate.getFullYear()]);" )
		      strExec.Append( "});" )
		    end if
		    
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		    
		    if IsNull( mValue ) then
		      Value = nil
		    else
		      Value = mValue
		    end if
		    
		    UpdateDaysOfWeekDisabled()
		    UpdateDaysOfWeekHighlighted()
		    UpdateDatesDisabled()
		    
		    if FirstLoad Then
		      RunBuffer()
		      FirstLoad = False
		      UpdateOptions()
		    end if
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MonthChange(theMonth as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PickerCleared()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PickerHidden()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event PickerShown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TextChanged(newText as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event YearChange(theYear as Integer)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/eternicode/bootstrap-datepicker
		
		Source Javascript: Apache License
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAutoClose
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAutoClose = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AutoClose As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		AvailableLanguages() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCalendarWeeks
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCalendarWeeks = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		CalendarWeeks As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mClearButton
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mClearButton = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ClearButton As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private DatesDisabled() As Date
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay0Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay0Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day0Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay0Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay0Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day0Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay1Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay1Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day1Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay1Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay1Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day1Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay2Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay2Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day2Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay2Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay2Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day2Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay3Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay3Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day3Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay3Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay3Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day3Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay4Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay4Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day4Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay4Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay4Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day4Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay5Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay5Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day5Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay5Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay5Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day5Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay6Enabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay6Enabled = value
			  
			  UpdateDaysOfWeekDisabled()
			End Set
		#tag EndSetter
		Day6Enabled As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDay6Highlighted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDay6Highlighted = value
			  
			  UpdateDaysOfWeekHighlighted()
			End Set
		#tag EndSetter
		Day6Highlighted As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDefaultViewDate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDefaultViewDate = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		DefaultViewDate As Date
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDisableKeyboard
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDisableKeyboard = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		DisableKeyboard As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mEndDate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEndDate = value
			  
			  dim strOut() as String
			  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('setEndDate', " + DateToDate( mEndDate ) + ");" )
			  
			  Buffer( strOut )
			End Set
		#tag EndSetter
		EndDate As Date
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		FirstLoad As Boolean = True
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mForceParse
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mForceParse = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ForceParse As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mFormat
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFormat = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Format As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mKeyboardNavigation
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mKeyboardNavigation = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		KeyboardNavigation As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLanguage
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLanguage = value
			  if mLanguage = "en" then mLanguage = ""
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Language As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private lastText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastValue As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAutoClose As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCalendarWeeks As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mClearButton As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay0Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay0Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay1Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay1Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay2Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay2Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay3Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay3Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay4Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay4Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay5Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay5Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay6Enabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDay6Highlighted As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDefaultViewDate As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDisableKeyboard As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEndDate As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mForceParse As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFormat As String = "mm/dd/yyyy"
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMinView
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMinView = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		MinView As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mKeyboardNavigation As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLanguage As String = "en"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMinView As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOrientation As String = "top left"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReadOnly As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mShowOnFocus As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStartDate As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTodayButton As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTodayHighlight As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToggleSelect As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Date
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWeekStart As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mOrientation
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mOrientation = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Orientation As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mReadOnly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReadOnly = value
			  
			  'Buffer( "window.GSjQuery('#" + me.ControlID + "').prop('disabled'," + Lowercase( Str( mReadOnly ) ) + ");" )
			  Buffer( "window.GSjQuery('#" + me.ControlID + "').prop('readonly'," + Lowercase( Str( mReadOnly ) ) + ");" )
			End Set
		#tag EndSetter
		ReadOnly As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mShowOnFocus
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mShowOnFocus = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ShowOnFocus As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStartDate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStartDate = value
			  
			  dim strOut() as String
			  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('setStartDate', " + DateToDate( mStartDate) + ");" )
			  
			  Buffer( strOut )
			End Set
		#tag EndSetter
		StartDate As Date
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private ticksClear As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ticksValue As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTodayButton
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTodayButton = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TodayButton As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTodayHighlight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTodayHighlight = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TodayHighlight As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mToggleSelect
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mToggleSelect = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ToggleSelect As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  dim strOut() as String
			  if IsNull( mValue ) then
			    strOut.Append( "var theDP = window.GSjQuery('#" + me.ControlID + "');" )
			    strOut.Append( "theDP.val('');" )
			    'strOut.Append( "theDP.removeData();" )
			  else
			    strOut.Append( "window.GSjQuery('#" + me.ControlID + "').datepicker_bs('update',  " + if(IsNull(mValue), "''", DateToDate( mValue )) + ");" )
			  end if
			  
			  Buffer( strOut )
			End Set
		#tag EndSetter
		Value As Date
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mWeekStart
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mWeekStart = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		WeekStart As Integer
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.datepickersingle", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY5SURBVHhe7d1fiFR1FAfw37k74+yMKwaViimGVG5BoCWBJD4UUZTujFZQmEkhvVi5M1v2JBVE9Ed3BCuIHrRQEyPaXRB6KR8irOgpo1QoMgiMIAjcmWVn557OtZPttjO76957546e7weG+zu/We/eub/v/fObmUUHAAAAAAAAAAAAAABXNNJlW+nq6bul7o11O5da4Hx/jnY751FNNvn3Dt+dOj+05wftTVxnobTWES+hulugXRcQuWHneee86ug35z/d94d2t5W2CkCup/gEe1SUjbpVu5pi506Sz+XKUHm/drXWfc9kspnULhnlbbK9C7W3KWb3ucf8yvBQ+bh2tYW2CED2geJ1Lu3eJ0d3a9eMsePPXM1trR4r/6ZdscvlS/fKEf+e7L6l2jVjEoQ3q4P9O7VMXOIBCAafUu5LOZKWadelYz7LY+7OVoSgc2PxEY/pQy1nRUJwUEKwRctEebpMToo+CDX4geDfB+uJWa7Qd7vnh/89cm/wWDZffEnLRCUagOzG0nbZGXdpGUqwnmB9WsaD/T45Z6a1CoWIXpxTKN2kZWISDQD5rqTNSES9vvE6N/Utk1F7VMtIpJyLN7AzkFgAcht7b5OjabmWDckN3jlZ/PrvQ+vmZH0X1hsDmXHcr80m5Mo+bluDBzP/JcupTLPO+CV2Ezi3p3cze95BLSeRvbmnOtD/nJYXZXtKu8lzfVpOQsybhwfLh7WMTK5QfEvW3vCIlaH/s05uzehA/xntukhmDPtkLz+t5SSV9Mg899E757VsucTOAHXPaz53lrv6RoMfqA5Jvzyv5SR1okXajFjz9cpRdLjR4Aco5cl0sbnMSCam7Z2ZxALgMTe9mWKiEW02NNXznuOcNqPFwSW7MSauaHMSYn9Umw2lPJfVZiISvQmMg1w6/nvrOErEsVwu2Ut2DK64AIiMLiPFTJ3avKIkGACK50iN6TXJ4R9LsJKWWADkOn55HVF0ZQYg8utattCbdz6tJ49WyjAvllNnWn5LWqZnsqSUnEuD5ZTBk+v4aZkFdGs5SbZQOiUbvkLLxLHjN6oD5Re0nKBrU6nb992PWk6Bx+SoqMm6RmV/1eT1BTePdVn5Wbn7OO6Rd3T4kz3f//Oz0YnsDDB3w7MLZa48SM4bkMHfJl2rJV+Lidy18mKukkGfK32Z6QbfLjk4yGWJaD45uibYd/JYKvtrrdS7mPlkLl/cqz8cmUgGI7OheAN7HV/LBvdoF8SBaEcuXzqqVSQiCUCHR4dk48J9ogczQ+7hbKH4ulahhQ5ArtD7lGzUHVpCC8glYWcmv+N6LUOJ4AzgrdcGtFDKpR7UZijhA8BulbaghXzHMssKL3wAyC3RFrSQzKzmaTOUSG4C4fKFAMRtys8Ck4cAGNeuAZjwFzYNTPd8izX/skjd87u02ZZCfxaQK5SC78JFjh3v9jr4QG3cFyrSnM747G+VeXDDbwsliunxsY7aV1pd0DHmzXdErxLRPdoVHXaDlcH+glaz1rYBgGlEFADcAxiHABiHABiHABiHABiHABiHABiHABgX7xtBzNsqFe+IVnCJsjn/bSLaquVEl8M7geR4y/BAuelfAMPUsvnigbgDgEuAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcQiAcRH8x5HFqqymU8uJ2P2sLRiP3HJthfFxZaD/IW3PWvgA5ItnHNGNWkKLsHP7qwP9T2o5a6EvAezohDahhYjpuDZDCR0AOYUc1ia0CvPZysquQ1qF0qHLWaudPvFTesWaRZKE1doFMZODbnvt3de+0zKU0AEISAiOSQhWyZZ1axfEhJlfrgyW92kZWiQBCEgIjkgIfGmukyBgehkxGfgzxO756lB5r3ZFIvQs4P/mFbZfPebPWUfEN8vsYJF2w2wwMXv0C/v1b0cG936hvQAAAAAAAAAAAAAAAAAz4dzfjER8lyFIUI0AAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = MinViewDays, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MinViewMonth, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MinViewYears, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEBSURBVDhPY6A64AoocANhKBcMQHyOgIIwKBcFMENpMAAp/M/A2M/AwOjBpmFx5/eNE3c5/QpMGBkYlzEyMGkBxQR+3zxxHKocDBihNAOnf1EjkKPBwPj/KVjgP6M0mA2iGRhkgewTQLbFfwaG3d839tWD1QAB3AVsmpb7gcZpAxVcBXLZGRkZY4HmW4LEgPwnQBqIGUMZGRnsmTXMr/+5cQKkjoEJRCADoHNTQBjKhQBGBksgUQjlYQdc/kXHoEy8AKQOOUAxXEAqGDUAOSEFFF79z/C/EcrFCYBRXA9S92PDhFUQPhQADZjN+J8RlGjwgv+M/z9939DvAeUOOGBgAABR70NmEg0vhAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = PositionBottomLeft, Type = String, Dynamic = False, Default = \"top left", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionBottomRight, Type = String, Dynamic = False, Default = \"top right", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopLeft, Type = String, Dynamic = False, Default = \"bottom left", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PositionTopRight, Type = String, Dynamic = False, Default = \"bottom right", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ViewDecade, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ViewMonth, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ViewYear, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AutoClose"
			Visible=true
			Group="Calendar"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CalendarWeeks"
			Visible=true
			Group="Calendar"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ClearButton"
			Visible=true
			Group="Calendar"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day0Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day0Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day1Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day1Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day2Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day2Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day3Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day3Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day4Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day4Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day5Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day5Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day6Enabled"
			Visible=true
			Group="Calendar"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Day6Highlighted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FirstLoad"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ForceParse"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Format"
			Visible=true
			Group="Calendar"
			InitialValue="DD, MM dd, yyyy"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="KeyboardNavigation"
			Visible=true
			Group="Calendar"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Language"
			Visible=true
			Group="Calendar"
			InitialValue="en"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinView"
			Visible=true
			Group="Calendar"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Orientation"
			Visible=true
			Group="Behavior"
			InitialValue="auto"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowOnFocus"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TodayButton"
			Visible=true
			Group="Calendar"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TodayHighlight"
			Visible=true
			Group="Calendar"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ToggleSelect"
			Visible=true
			Group="Calendar"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WeekStart"
			Visible=true
			Group="Calendar"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisableKeyboard"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
