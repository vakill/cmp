#tag Class
Protected Class GraffitiWebLikert
Inherits GraffitiControlWrapper
Implements GraffitiUpdateInterface
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "valuechange"
		      if Parameters.Ubound < 1 then Return True
		      dim theQuestion as GraffitiWebLikertQuestion = Question( Parameters(0).StringValue )
		      if IsNull( theQuestion ) then Return True
		      theQuestion.Value = Parameters(1).IntegerValue
		      ValueChange( theQuestion )
		    case "mousein"
		      if Parameters.Ubound < 0 then Return True
		      dim theQuestion as GraffitiWebLikertQuestion = Question( Parameters(0).StringValue )
		      if not IsNull( theQuestion ) then MouseEnter( theQuestion )
		    case "mouseleave"
		      if Parameters.Ubound < 0 then Return True
		      dim theQuestion as GraffitiWebLikertQuestion = Question( Parameters(0).StringValue )
		      if not IsNull( theQuestion ) then MouseLeave( theQuestion )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "style"
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  DoResize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<style>" + ReplaceAll( cssLikert, "#controlid#", me.ControlID ) + "</style>" )
		  source.Append( "<div id='" + me.ControlID + "' style='overflow-y:auto;display:none;' class='" + if(not IsNull( me.Style ), if( me.Style.Name <> "_defaultStyle", me.Style.Name, "" ), "" ) + "'>" )
		  source.Append( "<form action=''>" )
		  source.Append( "</form>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Add(Question as GraffitiWebLikertQuestion)
		  if IsNull( Question ) then Return
		  if Question.Count <= 0 then Return
		  
		  Question.mObserver = me
		  
		  Questions.Append( Question )
		  
		  dim questionHTML as String = ReplaceAll( htmlStatement, "#QUESTIONID#", Question.ID )
		  questionHTML = ReplaceAll( questionHTML, "#QUESTION#", Question.Caption )
		  
		  dim strExec() as String
		  strExec.Append( "var myForm = GSjQuery('#" + me.ControlID + " form');" )
		  strExec.Append( "var newQuestion = GSjQuery('" + EscapeString( questionHTML ) + "').appendTo(myForm);" )
		  strExec.Append( "var thisBlock = GSjQuery('#" + Question.ID + "_container');" )
		  strExec.Append( "var thisLabel = GSjQuery('label#" + Question.ID + "');" )
		  strExec.Append( "thisBlock.addClass('" + backgroundStyleToString( Question.BackgroundStyle ) + "');" )
		  strExec.Append( "thisBlock.addClass('" + TextStyleToString( Question.TextStyle ) + "');" )
		  strExec.Append( "thisBlock.addClass('" + borderStyleToString( Question.BorderStyle ) + "');" )
		  strExec.Append( "thisBlock.addClass('" + borderTypeToString( Question.BorderType ) + "');" )
		  strExec.Append( "thisLabel.addClass('" + AlignmentToString( Question.CaptionAlignment ) + "');" )
		  strExec.Append( "var questionUL = GSjQuery('#" + Question.ID + "_likert');" )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Question.Count - 1
		  
		  dim currentHTML as String
		  for intCycle = 0 to intMax
		    currentHTML = ReplaceAll( htmlChoice, "#VALUE#", Str( intCycle ) )
		    currentHTML = ReplaceAll( currentHTML, "#CAPTION#", Question.Choice(intCycle) )
		    currentHTML = ReplaceAll( currentHTML, "#QUESTIONID#", Question.ID )
		    strExec.Append( "questionUL.append('" + EscapeString( currentHTML ) + "');" )
		    strExec.Append( "var thisChoice = GSjQuery('#" + Question.ID + "_" + Str( intCycle ) + "');" )
		    
		    if intCycle = Question.Value then
		      strExec.Append( "thisChoice.prop('checked', true);" )
		    end if
		  next
		  
		  strExec.Append( "questionUL.find('li').css('width', '" + Str( (100 / Question.Count) - 0.8 ) + "%');" )
		  
		  strExec.Append( "GSjQuery('#" + me.ControlID + " input[name=" + chr(34) + Question.ID + chr(34) + "]:radio').change(function () {")
		  strExec.Append( "var fieldName = window.GSjQuery(this).prop('name');" )
		  strExec.Append( "var fieldValue = this.value;" )
		  strExec.Append( "Xojo.triggerServerEvent('" + Me.ControlID + "','valueChange',[fieldName,fieldValue]);")
		  strExec.Append( "});" )
		  
		  strExec.Append( "newQuestion.mouseenter(function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','mousein',['" + Question.ID + "']);" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "newQuestion.mouseleave(function() {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','mouseleave',['" + Question.ID + "']);" )
		  strExec.Append( "});" )
		  
		  
		  if isShown and LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  else
		    Buffer( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function alignmentToString(theAlignment as Alignments) As String
		  select case theAlignment
		  case Alignments.Center
		    Return "text-center"
		  case Alignments.Left
		    return "text-left"
		  case Alignments.Right
		    Return "text-right"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allAlignments() As String()
		  return Array( "text-center", "text-left", "text-right" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allBGStyles() As String()
		  return Array( "bg-primary", "bg-secondary", "bg-success", "bg-danger", "bg-warning", "bg-info", "bg-light", "bg-dark", "bg-white" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allBorderStyles() As String()
		  Return Array( "border-primary", "border-secondary", "border-success", "border-danger", "border-warning", _
		  "border-info", "border-light", "border-dark", "border-white" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allBorderTypes() As String()
		  Return Array( "border", "border-top", "border-right", "border-bottom", "border-left", _
		  "border-0", "border-top-0", "border-right-0", "border-bottom-0", "border-left-0" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function allTextStyles() As String()
		  return Array( "text-danger", "text-dark", "text-info", "text-light", "text-muted", "text-primary", "text-secondary", _
		  "text-success", "text-warning", "text-white" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function backgroundStyleToString(theStyle as BGStyles) As String
		  select case theStyle
		  case BGStyles.Danger
		    Return "bg-danger"
		  case BGStyles.Dark
		    Return "bg-dark"
		  case BGStyles.Info
		    Return "bg-info"
		  case BGStyles.Light
		    Return "bg-light"
		  case BGStyles.Primary
		    Return "bg-primary"
		  case BGStyles.Secondary
		    Return "bg-secondary"
		  case BGStyles.Success
		    Return "bg-success"
		  case BGStyles.Warning
		    Return "bg-warning"
		  case BGStyles.White
		    Return "bg-white"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function borderStyleToString(theStyle as BorderStyles) As String
		  Select case theStyle
		  case BorderStyles.Danger
		    Return "border-danger"
		  case BorderStyles.Dark
		    Return "border-dark"
		  case BorderStyles.Info
		    Return "border-info"
		  case BorderStyles.Light
		    Return "border-light"
		  case BorderStyles.Primary
		    Return "border-primary"
		  case BorderStyles.Secondary
		    Return "border-secondary"
		  case BorderStyles.Success
		    Return "border-success"
		  case BorderStyles.Warning
		    Return "border-warning"
		  case BorderStyles.White
		    Return "border-white"
		  End Select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function borderTypeToString(theType as BorderTypes) As String
		  select case theType
		  case BorderTypes.All
		    Return "border"
		  case BorderTypes.Bottom
		    Return "border-bottom"
		  case BorderTypes.Left
		    Return "border-left"
		  case BorderTypes.NoBottom
		    Return "border-bottom-0"
		  case BorderTypes.NoLeft
		    Return "border-left-0"
		  case BorderTypes.None
		    Return "border-0"
		  case BorderTypes.NoRight
		    Return "border-right-0"
		  case BorderTypes.NoTop
		    Return "border-top-0"
		  case BorderTypes.Right
		    Return "border-right"
		  case BorderTypes.Top
		    Return "border-top"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Count() As Integer
		  Return Questions.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoResize()
		  ' if isShown and LibrariesLoaded Then
		  ' me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  ' else
		  ' Buffer( "window.GSjQuery('#" + me.ControlID + "').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  ' end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebLikert -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebLikert -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Insert(index as Integer, Question as GraffitiWebLikertQuestion)
		  if IsNull( Question ) then Return
		  if Question.Count <= 0 then Return
		  
		  Question.mObserver = me
		  
		  if index < 0 then index = 0
		  if index > Questions.Ubound then Add( Question )
		  
		  Questions.Insert( index, Question )
		  dim prevQuestion as GraffitiWebLikertQuestion = Questions(index)
		  
		  dim strExec() as String
		  strExec.Append( "var beforeElement = GSjQuery('#" + me.ControlID + " div#" + prevQuestion.ID + "');" )
		  
		  dim questionHTML as String = ReplaceAll( htmlStatement, "#QUESTIONID#", Question.ID )
		  questionHTML = ReplaceAll( questionHTML, "#QUESTION#", Question.Caption )
		  
		  strExec.Append( "var myForm = GSjQuery('#" + me.ControlID + " form');" )
		  strExec.Append( "var newQuestion = GSjQuery('" + EscapeString( questionHTML ) + "').insertBefore(beforeElement);" )
		  strExec.Append( "var thisBlock = GSjQuery('#" + Question.ID + "_container');" )
		  strExec.Append( "thisBlock.addClass('" + backgroundStyleToString( Question.BackgroundStyle ) + "');" )
		  strExec.Append( "thisBlock.addClass('" + TextStyleToString( Question.TextStyle ) + "');" )
		  strExec.Append( "thisBlock.addClass('" + borderStyleToString( Question.BorderStyle ) + "');" )
		  strExec.Append( "thisBlock.addClass('" + borderTypeToString( Question.BorderType ) + "');" )
		  strExec.Append( "var questionUL = GSjQuery('#" + Question.ID + "_likert');" )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Question.Count - 1
		  
		  dim currentHTML as String
		  for intCycle = 0 to intMax
		    currentHTML = ReplaceAll( htmlChoice, "#VALUE#", Str( intCycle ) )
		    currentHTML = ReplaceAll( currentHTML, "#CAPTION#", Question.Choice(intCycle) )
		    currentHTML = ReplaceAll( currentHTML, "#QUESTIONID#", Question.ID )
		    strExec.Append( "questionUL.append('" + EscapeString( currentHTML ) + "');" )
		    strExec.Append( "var thisChoice = GSjQuery('#" + Question.ID + "_" + Str( intCycle ) + "');" )
		    
		    if intCycle = Question.Value then
		      strExec.Append( "thisChoice.prop('checked', true);" )
		    end if
		  next
		  
		  strExec.Append( "questionUL.find('li').css('width', '" + Str( (100 / Question.Count) - 0.8 ) + "%');" )
		  
		  strExec.Append( "GSjQuery('#" + me.ControlID + " input[name=" + chr(34) + Question.ID + chr(34) + "]:radio').change(function () {")
		  strExec.Append( "var fieldName = window.GSjQuery(this).prop('name');" )
		  strExec.Append( "var fieldValue = this.value;" )
		  strExec.Append( "Xojo.triggerServerEvent('" + Me.ControlID + "','valueChange',[fieldName,fieldValue]);")
		  strExec.Append( "});" )
		  
		  if isShown and LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  else
		    Buffer( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Question(index as Integer) As GraffitiWebLikertQuestion
		  if index >= 0 and index <= Questions.Ubound then Return Questions(index)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Question(qID as String) As GraffitiWebLikertQuestion
		  for each q as GraffitiWebLikertQuestion in Questions
		    if q.id = qid then Return q
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Remove(Question as GraffitiWebLikertQuestion)
		  if IsNull( Question ) then Return
		  
		  dim strExec() as String
		  strExec.Append( "GSjQuery('#" + me.ControlID + " div#" + Question.ID + "_container').remove();" )
		  Buffer( strExec )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Questions.Ubound
		  for intCycle = intMax DownTo 0
		    if Questions(intCycle) = Question then
		      Questions.Remove( intCycle )
		      Return
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  ReDim Questions(-1)
		  
		  dim strExec() as String
		  strExec.Append( "GSjQuery('#" + me.ControlID + "').empty();" )
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollTo(Question as GraffitiWebLikertQuestion)
		  if IsNull( Question ) then Return
		  
		  dim strExec() as String
		  strExec.Append( "var theControl = GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "var theQuestion = GSjQuery('#" + Question.ID + "_container');" )
		  strExec.Append( "theControl.animate({" )
		  strExec.Append( "scrollTop: theQuestion.offset().top - theControl.offset().top + theControl.scrollTop()" )
		  strExec.Append( "}, 500);" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TextStyleToString(theStyle as TextStyles) As String
		  select case theStyle
		  case TextStyles.Danger
		    return "text-danger"
		  case TextStyles.Dark
		    return "text-dark"
		  case TextStyles.Info
		    return "text-info"
		  case TextStyles.Light
		    return "text-light"
		  case TextStyles.Muted
		    return "text-muted"
		  case TextStyles.Primary
		    return "text-primary"
		  case TextStyles.Secondary
		    return "text-secondary"
		  case TextStyles.Success
		    return "text-success"
		  case TextStyles.Warning
		    return "text-warning"
		  case TextStyles.White
		    return "text-white"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    RunBuffer()
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateProperty(item as Variant, identifier as String)
		  // Part of the GraffitiUpdateInterface interface.
		  
		  dim theQuestion as GraffitiWebLikertQuestion = item
		  if IsNull( theQuestion ) then Return
		  
		  dim strExec() as String
		  strExec.Append( "var thisBlock = GSjQuery('#" + theQuestion.ID + "_container');" )
		  select case identifier
		  case "caption"
		    strExec.Append( "GSjQuery('label#" + theQuestion.ID + "').html('" + EscapeString( theQuestion.Caption ) + "');" )
		  case "bgstyle"
		    strExec.Append( "thisBlock.removeClass('" + Join( allBGStyles, " ") + "');" )
		    strExec.Append( "thisBlock.addClass('" + backgroundStyleToString( theQuestion.BackgroundStyle ) + "');" )
		  case "borderstyle"
		    strExec.Append( "thisBlock.removeClass('" + Join( allBorderStyles, " ") + "');" )
		    strExec.Append( "thisBlock.addClass('" + borderStyleToString( theQuestion.BorderStyle ) + "');" )
		  case "bordertype"
		    strExec.Append( "thisBlock.removeClass('" + Join( allBorderTypes, " ") + "');" )
		    strExec.Append( "thisBlock.addClass('" + borderTypeToString( theQuestion.BorderType ) + "');" )
		  case "textstyle"
		    strExec.Append( "thisBlock.removeClass('" + Join( allTextStyles, " " ) + "');" )
		    strExec.Append( "thisBlock.addClass('" + TextStyleToString( theQuestion.TextStyle ) + "');" )
		  case "value"
		    strExec.Append( "GSjQuery('#" + theQuestion.ID + "_" + Str( theQuestion.Value ) + "').prop('checked', true);" )
		  case "captionalign"
		    strExec.Append( "GSjQuery('label#" + theQuestion.ID + "').removeClass('" + Join( allAlignments ) + "');" )
		    strExec.Append( "GSjQuery('label#" + theQuestion.ID + "').addClass('" + AlignmentToString( theQuestion.CaptionAlignment ) + "');" )
		  end select
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdatePropertyMultiple(identifier as String, item as Variant, modifier as Variant)
		  // Part of the GraffitiUpdateInterface interface.
		  
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event MouseEnter(Question as GraffitiWebLikertQuestion)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseLeave(Question as GraffitiWebLikertQuestion)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange(Question as GraffitiWebLikertQuestion)
	#tag EndHook


	#tag Note, Name = License
		Original Work
		
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBackground
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBackground = value
			  
			  Buffer( "GSjQuery('#" + me.ControlID + "').removeClass('" + Join( allBGStyles, " " ) + "').addClass('" + backgroundStyleToString( mBackground ) + "');" )
			End Set
		#tag EndSetter
		Background As BGStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBorderStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBorderStyle = value
			  
			  Buffer( "GSjQuery('#" + me.ControlID + "').removeClass('" + Join( allBorderStyles, " " ) + "').addClass('" + borderStyleToString( mBorderStyle ) + "');" )
			End Set
		#tag EndSetter
		BorderStyle As BorderStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBorderType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBorderType = value
			  
			  Buffer( "GSjQuery('#" + me.ControlID + "').removeClass('" + Join( allBorderTypes, " " ) + "').addClass('" + borderTypeToString( mBorderType ) + "');" )
			End Set
		#tag EndSetter
		BorderType As BorderTypes
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mBackground As BGStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderStyle As BorderStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderType As BorderTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTextStyle As TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Questions() As GraffitiWebLikertQuestion
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTextStyle = value
			  
			  Buffer( "GSjQuery('#" + me.ControlID + "').removeClass('" + Join( allTextStyles, " " ) + "').addClass('" + textStyleToString( mTextStyle ) + "');" )
			End Set
		#tag EndSetter
		TextStyle As TextStyles
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = cssLikert, Type = String, Dynamic = False, Default = \"##controlid# h1.likert-header {\r  padding:0 4.25%;\r  margin:0;\r}\r##controlid# form .statement {\r  display:block;\r  font-size: 14px;\r  font-weight: bold;\r  padding: 10px 7.75% 0 4.25%;\r  margin-bottom:0px;\r}\r##controlid# form .likert {\r  list-style:none;\r  width:100%;\r  margin:0;\r  padding:0 0 5px;\r  display:block;\r}\r##controlid# form .likert:last-of-type {border-bottom:0;}\r\r##controlid# form .likert li {\r  display:inline-block;\r  width:19%;\r  text-align:center;\r  vertical-align: top;\r}\r##controlid# form .likert li input[type\x3Dradio] {\r  display:block;\r  position:relative;\r  top:0;\r  left:50%;\r  margin-left:-6px;\r}\r##controlid# form .likert li label {width:100%;}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = htmlChoice, Type = String, Dynamic = False, Default = \"      <li>\r        <input id\x3D\"#QUESTIONID#_#VALUE#\" type\x3D\"radio\" name\x3D\"#QUESTIONID#\" value\x3D\"#VALUE#\">\r        <label>#CAPTION#</label>\r      </li>", Scope = Private
	#tag EndConstant

	#tag Constant, Name = htmlStatement, Type = String, Dynamic = False, Default = \"<div id\x3D\"#QUESTIONID#_container\">\r<label id\x3D\'#QUESTIONID#\' class\x3D\'statement pb-3\'>#QUESTION#</label>\r<ul id\x3D\'#QUESTIONID#_likert\' class\x3D\"likert\">\r</ul>\r</div>", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.likert", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA+ZSURBVHhe7Z1/kBzHVcf7zeze3eyefliCJGWn7MIOtkISYakQsQNBcShHtixp9866uAQGwo/IsbGt3T0p8R8kiiAFhmh3T3JIghIIIbET+xTdniRjF2BsCVxSYiQZG8VyIIHEkBCMY+l0++Nud/vx+vSuSkK6u+nZmdnZS3+q5On39rw70+/br3t6e3qFwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBjmC8DHHy8GHrMX1A8tnrTii2w7VqlUx8bEwT0VfvXHinkrgO701msskCuFFCsE4EqBcCUALqKX6B8kz/3VBdQRcQwEnEHAMwLFCaqdE4hwvBbHE2Jvscp/N6+YNwJIrN36JuxqbqRLSlHwVgCIpfySL6AQLwKKQ9KG4dq+/GF2dzydLYCB7b2JibEBtGAjXcha9gYP4rdRWMMksr2VUv4YezuSjhRAb39uWbOJWQrAB+kS2noN1G0ct4TIl0eLj7Cro+goAST7Bt8uETN00r/NrsiAKI5bKAvl/UMPs6sj6AgBJG/PvE02rG3U1n+DXZFlSgiWKJRHCh0hhMgLwEnnttBJFqnYUdmKlPAVkM1s+cDuH7InkkS2UrvXZ99CLakIAOvY1XFQNnjVEpiJ8vggkgJw0pkP0f04tXroYVdHQ7eQX+iJx7OvD//xGXZFhsgJIJHKfpoG9nezOW+gbPASWLipMlL8Z3ZFgugI4ObBZCIpv0KntJ498w7KBGOAJILR4l+zq+3YfGwrXenctV1xfJz6+/ewa15Cra2bstuvxpe98wf1U0cjMYHU9gzQk8q8G8AaphN5I7uCA/G7COJHdNmn6fNOI0o6Wg61zMVkL6bjIjq+hWolzv9HYFCX8PvV0cJ2NttGWwXQ3Z+52m5ah+ksrmCXfyBO0vuOUOkYZZbnoVp/fvzJh1499+LsJG/fuhybjRVCWNeTKG6mSnobv+Qr9N6Zaqmwi8220D4B3HJfd6I7roL/8+xpHWrS9H7DEmGk1rVgRAzvmORXWsLpG1wlpOynyuqnFH4tu31BAmyqjeS/ymbotE0AiVRuH316H5s+gEWUsZ3V/Z/8PjsCIZHO/Ral760A4q3sahm6O3hveV/xaTZDpS0CoOA/RJ98L5utgeJT0oadtX3577InFJxU9i4Q8GG6jqvZ5RnqCl61mvKm8oGhk+wKjdAFkOjL/rpA+CKbnqFKO2mh2FIeLTzFrvBZtz2RiJ0tUOmuc44WQDxMt4er2QqNUG8DnQ3bLhcg91PLcdjlDWr11esXpOp/9uB32NMevnWoXj915GDs2neepC7hRmpPasWRNwCuil93o6y/fCTUxSahZgBK/Y/RJw6w6RH4QKWUbzmD+M2C9ANLGzj5CAnhfezyhBTNd9VKu46wGTgWHwMnkc5sbi34+IoU+J4oBl9xtvTga3Rfv4a6pi+wyxOWsNQ3n6ERShfQ0z94FSAcoKK3CRbEI7LZXFvbv+sF9kSWxqkjo7FlN9jUzXnsz+HNsbfeEG+cOvr37AiUUDIAjXCzdEics/SgW64fNG28s3Zw9/fYFXmqpeLH6MQ/y6Y2IOHDvRvv+0k2AyVwATi3Za+gAc4WNrWhE/y1iX1D7R3seYBG9HeTfFXW0wdEvFmPq0YTOMFngDh6vxAUH2zrbV6LVHqtTXQRnr70odF5ZtFtD1zGZmAEKoDk+vvfCGhl2NQCEf+iMlr4PJudyZfzZWmht+wHwpmMTwSeBQIVgLRjWboQ/YEmiu9UuyY8dxuBs3lzvHfD4M+wNSu1fUPP0gV9lE0tAEV26YZtC9gMhGC7AERvq3hBbBHDnx5nKzqs3h5LpHKfSfxP7yS17JOJdO4/k6nsr/CrM1IpFT9BGU1/VA/QW4XG+9kKhMAEkEhl1wLAm9h0Dd1Hf6lSKhxkMzpszDrOZWNPkjg/xB7FFQjwMF3rR9ieEbTxY1zUAgX4+IXZxQQmAKqYjVzUwrKtnVyMDus2J5w6HKR7+19mz4UAPDiXCKa6AsT9bLoGQNy2IP27vj7neD4BCYDCj6g966cGfuWv7YzWZM/Ng0kn1nuQAvFe9lwaFyKgPn2Ii1o0sSvNRd8JRAA96eyA6r/YdI0NMlqt/061UBWp5Yub2DM7c4igvL/4NIn8CTZdQ80psG4gEAFQqryVi65Bgc+Ml3a9xGb7UU8ej08FS2+h6hwiIDF9jYuuAYFrxcBAINP2wXQBCCu45J5z6/cigbr1SkyO/Q0V333Oo8ksIojFvVwngDNxhX6dusD/r4PVWr+eeI0t18hG4yrd+X5nQyZLdxq3U4pcSKo7Vo2Je1rdyWPJLfctrHXH/5Zqxoe1itaaSmmnEtIFOKnsAf1H3uRdldLQHjZ8w/cM4DgxL0r9B+3gp3J/CZZVoMbxC6Tid1C38wGnASfUMwb8J9pcNvCRRdXu+NP+BF8NauWl74RA/C+X3OMlq7rA/y5AipVc0gCf54IrKPhfolH5RZNMJITrYiie6E1v0V6wuSi1ZXGtPnmY3tfD+c/Im/l4ARYKtYxMDxAdIgBPJwquBeCkcw9TkO5k82JAXC2F9aSOCNSXLpNgPUtZZDm7fAIv+RxgebT4Ig16z7Lpjo7JACi0Z/8ESFcCSKSzX6VWPufUK73hlSSCv3MjgoUbs0smYxNHKfiu5vbdohatVk8vnPk7ABTPcckdILoSA1v163YOfBcADW60F0ZWYv815xOzFPxhevc72HQBXN5E65nZRKBm2Bp1+Aads68Pe1Bwv203xEZxaEeDPRcD4p+45BqQDd9nBH0XAClfSwCIeEYMDzfZvCRTD5EI/allCuwbmsJ69lIi6F2X+4kmdh2jQFzDLn+g4FtNsW78YOEUey4JZbLTXHRNs25HXwCAU5sxumauiqCWv5v+yPNMGKX2y0gEXz9fBGq5lYyJ50khV7HLH1wGX4ECtAUAQnZABtDsAmarCLWghC77PjY9QyJYINE+pkSg3rPZiL9Ibn8fSNUIvsJSmU8TGvwu4aJv+J8BNLuA2TIA2tbPcrF1QDg0MHxBWvZJ+kx/H0XXDL6CukrtDEAX0RFjAK3bGwRcyMWLkA3pukLdATEaF/hbiR6Cr5CWmPG6ZwTwdS75RgAZQC+1UWtczMWLULODJKjo7sDpMfgKkHqZUoGoNrfwF/8zAIKWAOjvZ62Iai9spoo+xGZ0aCH4itmEPxOWsF7jom/4nwEAx7joCsoYs1fEl/PlygK4LVIiaDH4Chr8amcAIaETMoDQG90CWHPO2EVJBD4E/xz4Di64RnY3o58BKKLaJ4nCvp6LMxMFEfgWfAJgFZdcU00ujL4AEMQJLrpGCpxbAIp2isDH4KttcHVvRVHgC+KLO7TXWcyF7wKwJWoLgFTjTgCKdojAz5ZPgK3f+qmO9OvVBb4LIAZS+0QBxPvU9/Fszk2YIvA5+AoLUX83VA+Z1Q2+C+DM6K7TNBL8FpuuqQvrdi66IwwRBBB8tWSO/qt3rQSiPM5FXwlgEEh4UCt6qJRARRBE8Imenrjaa7CLTdfUuiY7IwMw2o90A8CtPevuv5JN9wQhgoCCr6AK1xc64j8G9axkIAKI12t7uaiFZce2cVEPP0UQYPCddFYtNtUWgAQIbMl8IAI48/hnXqeK3Meme0Dcq37wkS09/BBBgMFXAApveyU0O0wAChrZe8oCtpDesoCiFREEHPzEhtwKqpRNbLqH0v/Egfy/s+U7gQmg/IbxvVSpXh7SuKsnnftFLuvjRQQBB38KEJ42iQgy/SsCE4DYs6eO4O2rXDqp1vbK0xFBCMF30rm7SQCelrUFmf4VwQmAsCXoPwBxjp9zUtk/4rI33IgghODztK83QSP+aZDpXxGoAMb3579J9/d/zqYWdFv4QCKVW8OmN2YTQRhpn1A/fUcHNfmjTdNGrw3INSTOYFGbKan9dNjUQq2AsazG6vLI7n9hl2cS6VyRxHgrjcTj9M5fjwt5z9SsZYDQZ6rNIr3tJE6tvzJa9GdL/VkIXAAK6gM/Tx/k6fd+KWgvdGFzddDB8hsnlfkDAOv32NSmaclrwtggM9AuYBpbxPJc1IaEs3xSWI+oTZrYFXlo/JJpJfjE7rB2Rw1FAOOlP3kJELeyqY2aJk7UxSE1oGJXZHFSuT+k8/V8F4MCv1mJv5JjM3BC6QKmocp5AkDcwqYH8PvSsjbV9uVD/VEFtzjp7OdAwO+w6RG5plIaumhTiaAIJQNMY9siS0Gc+YHJOYHLLYmHnHQmUruIJtffv5zS/lOtBp8GvZ8MM/iKUDOAglrJvVRRD7HZAliSCPfXRguvsKMtJFPZQQRoeXczSv0nqqWin5tTuCL0n45tnDr6jdiyG68h5bX42BcsI/n+ZnzZDWX1nuwMjWR/9qbYdTd+lgYo97DLOyjqKOD9jZePhC7m0DPANDQeeIrGA7Nvvuie71ErHKqO7ByiS6I7x+BQk1P0ATTKb2UscyEI2F8dKQY65TsTbROAekpXWvZhGjH7tjkDIv43dS+P0b+R8mj+GXa3jFqoArbdT+faT6a3reNmgFr+3dVS3vOvi7RK2wSgSPRlVgoJhymNJtnlI/gKNa0RCeIoNvG5iQPFf+MX5ubOwWTyrFiFIFchwi0+Zqr/z8crpcIOLreFtgpA4aQy60FYI3QmgY5HKG3/kFLEc3TJr9FFn0EhT1P5dRCYmNrTAKd/QRyuo4D791j6TKD4VGW00PLeB63SdgEo1Pf/lsBH6XQuZ9c8Bz5aKeU/wUZbiYQAFF39237alo1Hqf8OZDu06ICbK6Xi59hoO5ERgEJt01rtjj/q5wg7MiDW0II7qiMF7d8MCJJICWCaRCqrtoAN5WfTQuJZGmdkqyN5vb0BQyCSAlCQCNbSoUhC8HcPv5ChW9Md1dHix9mMHKHPBLql/vLRf60vX7MnLifVnj76D1O2HTwmBdxRGy3+FTsiSWQzwPk4G7J9aokYna0vu3gHCooqCixQq29lPUBodIQApnH6cgOAmKHTfhe7ogMHPh4XhbG9Rd+3cgmKjhLANFMZwVJP2cAvsat9cOC7Gt35M48/6Ps2bkHTkQKYxukbXEUBSNNIi7oIMefO4L6Bokk1NwJSlMpyfEQc3FPhVzqOjhbA+UyJQWIfRWc9jRfezm7foNH8q/S+h0BKCnqlo4N+PvNGABewMev0TOBKsGEFRW6lAEsdf4oC6GZrtompDRlBHKeWfkJA84REcXxidNd/8OvzivkpgJlYvT2WXDK+BLCxtCHspWr3bQutcRVwKeOv1ZyzP4rkbxYbDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMMyKEP8H1NGj/IFuZTUAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAFvSURBVDhP1VK7SkNBEJ3ZpDARkx8wCpbWWqSy00Y0IuoHSFpJfPR+gCTBMmLANmmuIviAFDba2CrW4hekyE0R9jizbm7uVbAVD1xmdx5nZs5d+nOwtwlk1ioLhnnJEufA9mUQNNo+9AMJgonVvRlOp5pMvOJdXwA+iLHbDxr33hPBeDsqvhHOolTUYe2iHQ5nGTjUOMh0sqXKskuOISIw6VRFOs+DbLkf1PfDq8bz4Pr03QIPADaEqEcwxz49QkQgLbZAuNN9s6VqWV2uozFdMnwB4pYsXFR9XL7HmIB5WgR5zZSqZ3Jpir3VscWXk+ijiqlpMDSndoSIAEQ9+aaY0NGzCumKQe0wqJcNcd6nJjCegPAknbft0L4xWVmHZGdq9y9rOy5KtKm+7780IpDONe2of0JJwqCWHxWLJhLTiXDukmNIvIPJ9eoBmE/cCiC3s/QuqD7xaeJIEChUZWZzJMeC3sEQbdD67TX+axB9At3fkp9FDZ86AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Alignments, Flags = &h0
		Left
		  Center
		Right
	#tag EndEnum

	#tag Enum, Name = BGStyles, Flags = &h0
		Light
		  White
		  Dark
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum

	#tag Enum, Name = BorderStyles, Flags = &h0
		Dark
		  Light
		  White
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum

	#tag Enum, Name = BorderTypes, Flags = &h0
		None
		  All
		  Top
		  Right
		  Bottom
		  Left
		  NoTop
		  NoRight
		  NoBottom
		NoLeft
	#tag EndEnum

	#tag Enum, Name = ButtonStyles, Type = Integer, Flags = &h21
		Primary
		  Secondary
		  Success
		  Danger
		  Warning
		  Info
		  Light
		  Dark
		Link
	#tag EndEnum

	#tag Enum, Name = Corners, Flags = &h0
		None
		  Rounded
		  Top
		  Right
		  Bottom
		  Left
		Circle
	#tag EndEnum

	#tag Enum, Name = TextStyles, Flags = &h0
		Dark
		  Light
		  White
		  Muted
		  Primary
		  Secondary
		  Info
		  Success
		  Warning
		Danger
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Background"
			Visible=true
			Group="Appearance"
			Type="BGStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Light"
				"1 - White"
				"2 - Dark"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderStyle"
			Visible=true
			Group="Appearance"
			Type="BorderStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderType"
			Visible=true
			Group="Appearance"
			Type="BorderTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - None"
				"1 - All"
				"2 - Top"
				"3 - Right"
				"4 - Bottom"
				"5 - Left"
				"6 - NoTop"
				"7 - NoRight"
				"8 - NoBottom"
				"9 - NoLeft"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextStyle"
			Visible=true
			Group="Appearance"
			Type="TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
