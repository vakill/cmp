#tag Class
Protected Class GraffitiWebLikertQuestion
	#tag Method, Flags = &h0
		Function Choice(choiceValue as Integer) As String
		  if choiceValue < 0 or choiceValue > Choices.Ubound then Return "Undefined"
		  
		  Return Choices(choiceValue)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(QuestionCaption as String, AnswerChoices() as String, DefaultValue as Integer = -1)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  Caption = QuestionCaption
		  Value = DefaultValue
		  Choices = AnswerChoices
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Count() As Integer
		  Return Choices.Ubound + 1
		End Function
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBackgroundStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBackgroundStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "bgstyle" )
			End Set
		#tag EndSetter
		BackgroundStyle As GraffitiWebLikert.BGStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBorderStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBorderStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "borderstyle" )
			End Set
		#tag EndSetter
		BorderStyle As GraffitiWebLikert.BorderStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mBorderType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBorderType = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "bordertype" )
			End Set
		#tag EndSetter
		BorderType As GraffitiWebLikert.BorderTypes
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaption = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "caption" )
			End Set
		#tag EndSetter
		Caption As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCaptionAlignment
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaptionAlignment = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "captionalign" )
			End Set
		#tag EndSetter
		CaptionAlignment As GraffitiWebLikert.Alignments
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Choices() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		ID As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mBackgroundStyle As GraffitiWebLikert.BGStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderStyle As GraffitiWebLikert.BorderStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderType As GraffitiWebLikert.BorderTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaptionAlignment As GraffitiWebLikert.Alignments
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		mObserver As GraffitiUpdateInterface
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTextStyle As GraffitiWebLikert.TextStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTextStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "textstyle" )
			End Set
		#tag EndSetter
		TextStyle As GraffitiWebLikert.TextStyles
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  if mValue < -1 then mValue = -1
			  if mValue > Choices.Ubound then mValue = Choices.Ubound
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "value" )
			End Set
		#tag EndSetter
		Value As Integer
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Caption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackgroundStyle"
			Group="Behavior"
			Type="GraffitiWebLikert.BGStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Light"
				"1 - White"
				"2 - Dark"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderStyle"
			Group="Behavior"
			Type="GraffitiWebLikert.BorderStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Primary"
				"4 - Secondary"
				"5 - Info"
				"6 - Success"
				"7 - Warning"
				"8 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BorderType"
			Group="Behavior"
			Type="GraffitiWebLikert.BorderTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - None"
				"1 - All"
				"2 - Top"
				"3 - Right"
				"4 - Bottom"
				"5 - Left"
				"6 - NoTop"
				"7 - NoRight"
				"8 - NoBottom"
				"9 - NoLeft"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextStyle"
			Group="Behavior"
			Type="GraffitiWebLikert.TextStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - Light"
				"2 - White"
				"3 - Muted"
				"4 - Primary"
				"5 - Secondary"
				"6 - Info"
				"7 - Success"
				"8 - Warning"
				"9 - Danger"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="CaptionAlignment"
			Group="Behavior"
			Type="GraffitiWebLikert.Alignments"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
