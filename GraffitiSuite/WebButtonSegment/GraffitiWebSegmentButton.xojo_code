#tag Class
Protected Class GraffitiWebSegmentButton
	#tag Method, Flags = &h0
		Sub Constructor(theID as String, theCaption as String, theValue as Boolean = False)
		  ID = theID
		  mCaption = theCaption
		  mValue = theValue
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(theID as String, theCaption as String, theStyle as WebStyle, theValue as Boolean = False)
		  ID = theID
		  mCaption = theCaption
		  mStyle = theStyle
		  mValue = theValue
		End Sub
	#tag EndMethod


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaption = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "caption" )
			End Set
		#tag EndSetter
		Caption As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mEnabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEnabled = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "enabled" )
			End Set
		#tag EndSetter
		Enabled As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		ID As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnabled As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		mObserver As GraffitiUpdateInterface
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "removeStyle" )
			  
			  mStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "addStyle" )
			End Set
		#tag EndSetter
		Style As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "value" )
			End Set
		#tag EndSetter
		Value As Boolean
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="mCaption"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mEnabled"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mValue"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
