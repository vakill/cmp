#tag Class
Protected Class GraffitiWebButtonSegment
Inherits GraffitiControlWrapper
Implements GraffitiUpdateInterface
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "buttonClick"
		      
		      if not me.Enabled then Return false
		      
		      if Parameters.Ubound >= 0 then
		        
		        dim theButton as GraffitiWebSegmentButton = ButtonFromID( Parameters(0).StringValue )
		        dim oldState as Boolean = theButton.Value
		        
		        if IsNull( theButton ) then Return True
		        
		        if not mToggle and theButton.Enabled then
		          DeselectAll()
		        end if
		        
		        if theButton.Enabled then
		          if mToggle then
		            theButton.Value = not oldState
		          else
		            SelectButton( theButton )
		          end if
		          SelectionChange( theButton )
		        end if
		      end if
		      
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    ToggleEnabled()
		    Return True
		  case "style"
		    dim strExec() as String
		    dim sVal as WebStyle = value
		    
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
		    if not IsNull( value ) then
		      if not IsNull( oldStyle ) then strExec.Append( "theButton.removeClass('" + oldStyle.Name + "');" )
		      
		      if not sVal.isdefaultstyle and sVal.Name <> "_Style" and sVal.Name <> "_DefaultStyle" then
		        strExec.Append( "theButton.addClass('" + sval.Name + "');" )
		      end if
		    end if
		    
		    oldStyle = value
		    
		    Buffer( strExec )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  
		  source.Append( "<div class='btn-group' id='" + me.ControlID + "' style='display:none;'></div>" )
		  
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddButton(newButton as GraffitiWebSegmentButton)
		  Buttons.Append( newButton )
		  
		  newButton.mObserver = me
		  
		  dim strExec() as String
		  strExec.Append( "var theContainer = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "var newButton = window.GSjQuery(" + chr(34) + "<button class='btn' style='height:100%;text-overflow:ellipsis;line-height:unset;' data-buttonid='" + EscapeString( newButton.ID ) + "'>" + ParseForIcon( newButton.Caption ) + "</button>" + chr(34) + ");" )
		  strExec.Append( "newButton.appendTo(theContainer);" )
		  
		  if not IsNull( mUnselectedStyle ) and mUnselectedStyle.Name <> "_Style" and mUnselectedStyle.Name <> "_defaultStyle" then
		    strExec.Append( "newButton.addClass('" + mUnselectedStyle.Name + "');" )
		  end if
		  
		  dim btnStyle as WebStyle = newButton.Style
		  if not IsNull( btnStyle ) and btnStyle.Name <> "_Style" and btnStyle.Name <> "_defaultStyle" then
		    strExec.Append( "newButton.addClass('" + btnStyle.Name + "');" )
		  else
		    strExec.Append( "newButton.addClass('btn-default');" )
		  end if
		  
		  dim strNewClass as String
		  Select case mSegmentHeight
		  case SizeLarge
		    strNewClass = "btn-lg"
		  case SizeSmall
		    strNewClass = "btn-sm"
		  case else
		    strNewClass = ""
		  End Select
		  
		  if strNewClass <> "" then strExec.Append( "newButton.addClass('" + strNewClass + "');" )
		  
		  strExec.Append( "newButton.on('click', function (e) {" )
		  strExec.Append( "var theButton = window.GSjQuery(this);" )
		  strExec.Append( "var theIndex = theButton.data('buttonid');" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'buttonClick', [theIndex]);" )
		  strExec.Append( "});" )
		  
		  Buffer( strExec )
		  
		  if newButton.Value then
		    SelectButton( newButton.ID )
		  end if
		  
		  if not me.Enabled then newButton.Enabled = True
		  'DisableButton( newButton, False )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Button(Index as Integer) As GraffitiWebSegmentButton
		  if Index >= 0 and Index <= buttons.Ubound then Return buttons(Index)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Button(ID as String) As GraffitiWebSegmentButton
		  dim intButton as Integer = ButtonIndexFromID( ID )
		  if intButton >= 0 and intButton <= Buttons.Ubound then
		    Return Buttons(intButton)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ButtonFromCaption(buttonCaption as String) As GraffitiWebSegmentButton
		  dim b as GraffitiWebSegmentButton
		  for each b in Buttons
		    if b.Caption = buttonCaption then Return b
		  next
		  
		  Return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ButtonFromID(ButtonID as String) As GraffitiWebSegmentButton
		  for intCycle as Integer = 0 to Buttons.Ubound
		    dim theButton as GraffitiWebSegmentButton = Buttons(intCycle)
		    if theButton.ID = ButtonID then Return theButton
		  next
		  
		  Return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ButtonIndexFromID(ButtonID as String) As Integer
		  for intCycle as Integer = 0 to Buttons.Ubound
		    dim theButton as GraffitiWebSegmentButton = Buttons(intCycle)
		    if theButton.ID = ButtonID then Return intCycle
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Count() As Integer
		  Return buttons.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAll()
		  for intCycle as Integer = 0 to Buttons.Ubound
		    dim currButton as GraffitiWebSegmentButton = Buttons(intCycle)
		    if not IsNull( currButton ) then currButton.Value = False
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DeselectButton(theButton as GraffitiWebSegmentButton)
		  if not IsNull( theButton ) then
		    dim strExec() as String
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( theButton.ID ) + chr(34) + "]');" )
		    
		    if not IsNull( SelectedStyle ) and SelectedStyle.Name <> "_style" and SelectedStyle.Name <> "_defaultStyle" then
		      strExec.Append( "theButton.removeClass('" + SelectedStyle.Name + "');" )
		    else
		      strExec.Append( "theButton.removeClass('active');" )
		    end if
		    
		    if not IsNull( theButton.Style ) and theButton.Style.Name <> "_style" and theButton.Style.Name <> "_defaultStyle" then
		      strExec.Append( "theButton.addClass('" + theButton.Style.Name + "');" )
		    else
		      if not IsNull( mUnselectedStyle ) and mUnselectedStyle.Name <> "_style" and mUnselectedStyle.Name <> "_defaultStyle" then
		        strExec.Append( "theButton.addClass('" + mUnselectedStyle.Name + "');" )
		      else
		        strExec.Append( "theButton.addClass('btn-default');" )
		      end if
		    end if
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DeselectButton(ButtonID as String)
		  if ButtonID <> "" then
		    dim theButton as GraffitiWebSegmentButton = ButtonFromID( ButtonID )
		    
		    if not IsNull( theButton ) then
		      DeselectButton( theButton )
		    end if
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DisableButton(theButton as GraffitiWebSegmentButton)
		  if not IsNull( theButton ) then
		    
		    dim strExec() as String
		    
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( theButton.ID ) + chr(34) + "]');" )
		    strExec.Append( "if (!theButton.hasClass('disabled')) {" )
		    strExec.Append( "theButton.addClass('disabled');" )
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DisableButton(ButtonID as String)
		  DisableButton( Button( ButtonID ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub EnableButton(theButton as GraffitiWebSegmentButton)
		  if not IsNull( theButton ) then
		    
		    dim strExec() as String
		    
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( theButton.ID ) + chr(34) + "]');" )
		    strExec.Append( "theButton.removeClass('disabled');" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub EnableButton(ButtonID as String)
		  EnableButton( Button( ButtonID ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebSegment -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ParseForIcon(Caption as String) As String
		  dim strCapt() as String
		  
		  dim intStart as Integer = 0
		  dim intEnd as Integer = 0
		  
		  intStart = Caption.InStr( "<" )
		  
		  if intStart > 1 then strCapt.Append( EscapeString( Caption.Left( intStart - 1 ) ) )
		  
		  while intStart > 0
		    intEnd = Caption.InStr( intStart, ">" )
		    dim strTag as String = Caption.Mid( intStart + 1, (intEnd - intStart) - 1 )
		    if strTag <> "br" and strTag <> "br /" then
		      strCapt.Append( "<i class='" + EscapeString( strTag ) + "'></i>" )
		    else
		      strCapt.Append( "<br />" )
		    end if
		    intStart = Caption.InStr( intEnd, "<" )
		  wend
		  
		  strCapt.Append( Caption.Mid( intEnd + 1, Caption.Len ) )
		  
		  Return Join( strCapt, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveButton(intIndex as Integer)
		  if intIndex >= 0 and intIndex <= Buttons.Ubound then
		    dim theButton as GraffitiWebSegmentButton = Buttons(intIndex)
		    if not IsNull( theButton ) then RemoveButton( theButton.ID )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveButton(ButtonID as String)
		  dim theButtonID as Integer = ButtonIndexFromID( ButtonID )
		  
		  if theButtonID >= 0 and theButtonID <= Buttons.Ubound then
		    dim strExec() as String
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( ButtonID ) + chr(34) + "]').detach();" )
		    
		    Buttons.remove( theButtonID )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectAll()
		  for intCycle as Integer = 0 to Buttons.Ubound
		    dim currButton as GraffitiWebSegmentButton = Buttons(intCycle)
		    if not IsNull( currButton ) then currButton.Value = True
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SelectButton(theButton as GraffitiWebSegmentButton)
		  if not IsNull( theButton ) then
		    
		    mValue = theButton
		    
		    dim strExec() as String
		    
		    if not IsNull( mSelectedStyle ) and mSelectedStyle.Name <> "_style" and mSelectedStyle.Name <> "_defaultStyle" then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('button').removeClass('" + mSelectedStyle.Name + "');" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( theButton.ID ) + chr(34) + "]')" )
		    
		    if not IsNull( mUnselectedStyle ) and mUnselectedStyle.Name <> "_style" and mUnselectedStyle.Name <> "_defaultStyle" then strExec.Append( ".removeClass('" + mUnselectedStyle.Name + "')" )
		    if not IsNull( theButton.Style ) and theButton.Style.Name <> "_style" and theButton.Style.Name <> "_defaultStyle" then strExec.Append( ".removeClass('" + theButton.Style.Name + "')" )
		    
		    if not IsNull( mSelectedStyle ) and mSelectedStyle.Name <> "_style" and mSelectedStyle.Name <> "_defaultStyle" then
		      strExec.Append( ".removeClass('btn-default').removeClass('active').addClass('" + mSelectedStyle.Name + "');" )
		    else
		      strExec.Append( ".addClass('active').addClass('btn-default');" )
		    end if
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SelectButton(ButtonID as String)
		  if ButtonID <> "" then
		    dim theButton as GraffitiWebSegmentButton = ButtonFromID( ButtonID )
		    
		    if not IsNull( theButton ) then SelectButton( theButton )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ToggleButton(theButton as GraffitiWebSegmentButton)
		  if not IsNull( theButton ) then
		    
		    if not theButton.Value then
		      SelectButton( theButton )
		    else
		      DeselectButton( theButton )
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ToggleEnabled()
		  dim strExec() as String
		  
		  for intCycle as Integer = 0 to Buttons.Ubound
		    dim theButton as GraffitiWebSegmentButton = Buttons(intCycle)
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( theButton.ID ) + chr(34) + "]')" )
		    strExec.Append( "." + if( me.Enabled, "remove", "add" ) + "Class('disabled');" )
		  next
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    RunBuffer()
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateProperty(item as Variant, identifier as String)
		  // Part of the GraffitiUpdateInterface interface.
		  
		  dim theButton as GraffitiWebSegmentButton = item
		  
		  dim strExec() as String
		  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + EscapeString( theButton.ID ) + chr(34) + "]');" )
		  
		  select case identifier
		  case "caption"
		    strExec.Append( "theButton.html('" + ParseForIcon( theButton.Caption ) + "');" )
		  case "disabled"
		    if theButton.Enabled then
		      EnableButton( theButton )
		    else
		      DisableButton( theButton )
		    end if
		  case "removeStyle"
		    if not IsNull( theButton.Style ) then strExec.Append( "theButton.removeClass('" + theButton.Style.Name + "');" )
		  case "addStyle"
		    if not IsNull( theButton.Style ) then strExec.Append( "theButton.addClass('" + theButton.Style.Name + "');" )
		  case "value"
		    if theButton.Value then
		      SelectButton( theButton )
		    else
		      DeselectButton( theButton )
		    end if
		  end select
		  
		  if strExec.Ubound > 0 then Buffer( strExec )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdatePropertyMultiple(identifier as String, item as Variant, modifier as Variant)
		  // Part of the GraffitiUpdateInterface interface.
		  
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionChange(theButton as GraffitiWebSegmentButton)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Property, Flags = &h21
		Private Buttons() As GraffitiWebSegmentButton
	#tag EndProperty

	#tag Property, Flags = &h21
		Private firstLoad As Boolean = True
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFullWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFullWidth = value
			  
			  dim strExec() as String
			  strExec.Append( "var theControl = window.GSjQuery('#" + me.ControlID + "');" )
			  strExec.Append( "theControl." + if( mFullWidth, "add", "remove" ) + "Class('d-flex');" )
			  strExec.Append( "theControl.find('.btn')." + if( mFullWidth, "add", "remove" ) + "Class('w-100');" )
			  Buffer( strExec )
			End Set
		#tag EndSetter
		FullWidth As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mFullWidth As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSegmentHeight As Integer = 26
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectedStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTextStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToggle As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mUnselectedStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As GraffitiWebSegmentButton
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldSelectedStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSegmentHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSegmentHeight = value
			  
			  dim strNewClass as String
			  Select case mSegmentHeight
			  case SizeLarge
			    strNewClass = "btn-lg"
			  case SizeSmall
			    strNewClass = "btn-sm"
			  case else
			    strNewClass = ""
			  End Select
			  
			  dim strExec() as String
			  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('.btn')" )
			  strExec.Append( ".removeClass('btn-lg')" )
			  strExec.Append( ".removeClass('btn-sm')" )
			  strExec.Append( ".removeClass('btn-xs')" )
			  if strNewClass <> "" then
			    strExec.Append( ".addClass('" + strNewClass + "');" )
			  Else
			    strExec.Append( ";" )
			  end if
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		SegmentSize As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelectedStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelectedStyle = value
			  
			  dim strExec() as String
			  dim sVal as WebStyle = value
			  
			  if not IsNull( value ) then
			    if not IsNull( oldSelectedStyle ) then
			      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('.btn').removeClass('" + oldSelectedStyle.Name + "');" )
			    else
			      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('.btn').removeClass('active');" )
			    end if
			    
			    if not sVal.isdefaultstyle and sVal.Name <> "_Style" and sVal.Name <> "_DefaultStyle" and not IsNull( mValue ) then
			      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + mValue.ID + chr(34) + "]').addClass('" + sval.Name + "');" )
			    end if
			  end if
			  
			  oldSelectedStyle = value
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		SelectedStyle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mToggle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mToggle = value
			End Set
		#tag EndSetter
		Toggle As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mUnselectedStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  if not IsNull( mUnselectedStyle ) then strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('button').removeClass('" + mUnselectedStyle.Name + "');" )
			  
			  mUnselectedStyle = value
			  
			  if not IsNull( mUnselectedStyle ) and not mUnselectedStyle.isdefaultstyle and mUnselectedStyle.Name <> "_Style" and mUnselectedStyle.Name <> "_DefaultStyle" and not IsNull( mUnselectedStyle ) then
			    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('button').addClass('" + mUnselectedStyle.Name + "');" )
			    if not IsNull( mValue ) then
			      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('[data-buttonid=" + chr(34) + mValue.ID + chr(34) + "]')" + _
			      ".removeClass('" + mUnselectedStyle.Name + "')" )
			      if not IsNull( mUnselectedStyle ) and not mUnselectedStyle.isdefaultstyle and mUnselectedStyle.Name <> "_Style" and mUnselectedStyle.Name <> "_DefaultStyle" and not IsNull( mUnselectedStyle ) then
			        strExec.Append( ".addClass('" + mSelectedStyle.Name + "');" )
			      else
			        strExec.Append( ".addClass('active');" )
			      end if
			    end if
			  end if
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		UnselectedStyle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  SelectButton( mValue )
			End Set
		#tag EndSetter
		Value As GraffitiWebSegmentButton
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webbuttonsegment", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANWSURBVHhe7drPa1RXFMDxex5GMoM/cFX8B1QQF8ZQ0G7cFn9NFiJ00YI7u1BnSsGdK0EQZ9SN4EJXuhE00fZfKEUTXRSllOLWpYiQ0Zjk9NzMQUUmM5O8e6Xyvp+QvHvPvLx3f5x3Z2BuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwJclfsxq66FT2xY2jF+04qSITPSiI1BdsBY+DSqP52fap2Ok98JIpH6sdc3+f9KuMxFENnp8KNXwxP7Oblx8e+7179dfeXgk9UbrvB0OWkMnbHC39KLDadDn1uTZDSFceDPd/sfD2WVPgNpU63hYDldFwnYPrYtNyrMgerI73XnkoVXVGs1vrWs3rXO7PbQuds+XoQhnuvfbdz20qs1Hf925JEu3bET3e2h9VJftvs3ug841j2RV+DGL+OSnmPzIrrHbJrTj1YHieWUnP1ppt7V/pR9DLMpip/TkRyKF/VytT50dfaUsIWsCxGU/xeR/JAfGG62WV/rqvS4HvFpabL+/fa2q3vjlJ3tr+96rSajKZS9mlTUBbPgmvZCMPdn7vNhXoZr8nmbgNVWXSq82nxNNP3b9ZE0Ae3pyLGMDE0DX8iFzRMM+uEoodngxHQmbNh1u7fJaNplXAPzfkQAVRwJUHAlQcSRAxZEAFUcCVBwJUHEkQMWRABWXNwHi9/npDbtm+nsO64eERS8l9V40y3U/lTcBRJ54KR0NT73Un2r6e8ZNKQOo6t9eTEaDvnr3sPOvV7PJvAKEWS+lU+icl/or4m6exFQee6mvQkLyBLCx+9NLWYkfc5Fao/VXis0ZkT1pv3VnOke8uqraseZDETns1VLiTqTuTHtPLPYi/Vk/b1s/f/BqSTq/VOied/euvPBANrk/BNqg6Un7/cPr6xYnPxRjp7w6mJ23cn5p1m6J7R88+dH42NjPdtIdr5Zhy76c+BKTH+VeAT6IO3XiZo01fl+/sPKebst69/7a98jVppqnw7Lss17uterIm0LNnE3m3NvpdtvrI6sfbf6oEr6zgd1rfR15U6jEPY9heW7+m/lL4caN9x4GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADA1ymE/wAD4NYVgbLoPQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACNSURBVDhPYxgFgwAwQmkw4Awo+ggU4Pv/n6EJxGdkZKj7z8DwieHfP2dGJqbTYLH//0u+buzvBbFBgAlKgwFIM5hm/M8LwjCx/0wMSiA2CPxnZJCGMsGAGUqDAYu6JRNQ47l/DP9PAG1+AtT85P9/xt0/NvZPZdWw4AcqOQE0YsfvGyfuQnSMgmEBGBgA0MwnM4Q1jVQAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = SizeDefault, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SizeLarge, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SizeSmall, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SegmentSize"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Large"
				"2 - Small"
				"3 - ExtraSmall"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Toggle"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FullWidth"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
