#tag Class
Protected Class GraffitiWebEmailValidator
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "empty"
		      NoSuggestion()
		    case "suggestion"
		      if UBound( Parameters ) < 0 then Return True
		      dim strFull as String = NthField( Parameters(0), ",", 3 )
		      strFull = NthField( strFull, chr(34), 4 )
		      UpdateSuggestion( strFull )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  //
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  if Domains = "" then Domains = DefaultDomains()
		  if TLDs = "" then TLDs = DefaultTLDs()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AttachField(AttachTo as WebTextField)
		  dim strExec() as String = Array( "var domains = ['" + ReplaceAll( ReplaceAll( Domains, ",", "','" ), " ", "" ) + "'];", _
		  "var topLevelDomains = ['" + ReplaceAll( ReplaceAll( TLDs, ",", "','" ), " ", "" ) + "'];", _
		  "window.GSjQuery('#" + AttachTo.ControlID + "_inner').on('blur change', function () {", _
		  "window.GSjQuery(this).mailcheck({", _
		  "domains: domains,", _
		  "topLevelDomains: topLevelDomains,", _
		  "suggested: function (element, suggestion) {", _
		  "Xojo.triggerServerEvent('" + self.ControlID + "', 'suggestion', [JSON.stringify(suggestion)]);", _
		  "},"  + _
		  "empty: function (element) {", _
		  "Xojo.triggerServerEvent('" + self.ControlID + "', 'empty','');", _
		  "}", _
		  "});", _
		  "});" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DefaultDomains() As string
		  return "yahoo.com,google.com,hotmail.com,gmail.com,me.com," + _
		  "aol.com,mac.com,live.com,comcast.net,googlemail.com,msn.com," + _
		  "hotmail.co.uk,yahoo.co.uk,facebook.com,verizon.net,sbcglobal.net," + _
		  "att.net,gmx.com,mail.com,gmx.de"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DefaultTLDs() As string
		  return "AERO, ARPA, ASIA, BIZ,COM,CN, CO.UK, COOP,DE," + _
		  "EDU,EU,FM,GOV,INFO,IS,IT,JOBS,JP,MIL,MOBI," + _
		  "MUSEUM,NAME,NET,ORG,POST,PRO,TEL,TRAVEL" + _
		  "UK,US,WS"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Empty()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebEmailValidator -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwmailcheck.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebEmailValidator -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Suggestion()
		  
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event NoSuggestion()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event UpdateSuggestion(SuggestedEmail as String)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		The MIT License (MIT)
		Copyright © 2012 Received Inc, http://kicksend.com
		
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the “Software”), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in
		all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
		THE SOFTWARE.
		
	#tag EndNote


	#tag Property, Flags = &h0
		Domains As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  RunBuffer()
			  
			  mLibrariesLoaded = value
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		TLDs As String
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webemailfield", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kInitializer, Type = String, Dynamic = False, Default = \"RSCustom.graffitisuite.webemailfield.<<var>>_change \x3D function(ev) {\r      var html \x3D RSCustom.graffitisuite.webemailfield.<<var>>.suggestion();\r      RS.triggerServerEvent(\"<<var>>\"\x2C\"suggestion\"\x2C[html]);\r};", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABReSURBVHhe7Z0JeBzFlcfrdWskzci2AjgE+MwRCBgvWZYri00gWXM6AXtGYEPChhjCtYH40IgEkoV4neUKxpIMMRCOXVizQNbGmrEMIZCwGxJsY44sbGIQBEgMeCEL2MbSjI6Zevtv6cmx0fQxo+7RyOnf943mvZrWTHfVq1evqquqVUhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISMiuAsn7XxZfXFA1drf363uNSD3ljb6qvMp29lBWHTcmqxYu1HLUXwS7rAHUTZ9/mDLpSFZ0ECt1IC70IKX404pVvSKqk8MKkWHFf8Bxb5KiNzVkg/QGU+XWb0st/UCO2WXYZQxgzMw5n9R91dOYeCouaiqSDhj4xD+Y1ct4exYGtFaRuTrbdtPbA5+MXka9AURnNDYQ0VdxJbMkqXww/xp/V1MVr+56uPV3A4mji1FpALEvX7EXR/RciOcTqb0HUkcaTimmOzLp5p9JwqhgVBlAzZnzDzTzNBdnjcJH0VcirNYT8a1dqZb7JaWiGR0GcMYlsVjVmGshNQ4kVD4IPB8l0tdk2lpfkKSKxJT3iiUWT54fMSMp2OqpkjQqQM06GH8vrTp08pjcK+uekOSKo2I9QM30xs8YBt0KRz9NknwD33lqno1Ok/OdmrmGlRkzTB1jreqUQXspzRNw1AQcN4GZj0SQWS//WiL8PDzCZdlUy3pJqBgq0gBqGxq/QlrdUUrGow+/GZe1mlg9qfO5J7tX37JRPioZa0yBTXUMs/F5/EIc57WnfFQUMIL52VTzElErgoozgFgieTPemga0omjXSj3YnWp+UPTAqGtInsKa48i+2cjBMZLsCXiUf8mmWy4UdcSpHANYsMCIvbhtBaSGgQRvoMbfm1d0Q2+q+VVJKh9fnF0brd/jQkV8IbzCkZLqAW7PbP7wbPXL+7olYcSoCAMYN7Nx974crcDJWCN4HuF2JnV9tq1lnSSMKLF403lK6QUIMA6SJGdYrWWj6uyRHk0ccQOoic87wCBjJSnPNehp+NHrM+mWR0WvKNBruQa5+gNRHWFWL9RWR07cvPyHWyWp7IyoAdTH532ij4yf4zSOliR7WGVx3KWZ9OJlklKx1MaTxyFjb0Mv4m8kyRYYwePZdPNpopadER0HMCYdtxo1H5G1MwicXiKlp6PWPy5JFU2uY+1buY41P45MfPxTqGLHSHJBYCQHVU2cfHCuY91KSSorI+YBYvHGh3H1Z4rqAK/I5MbNVqsXZiRhOBCanP1NNndDxu+uDbU7+v7vqHzk7e7oG++o5cvzcpxveG0S4AmuhSe4RtSyMSIGEE00Xoea/z1RbUGEf3021fKPopZErKFxGmt4GVKfJ2a8U7V8VIhN8DYvGOhOdh0x7iG/JodE402XEfFSUW1hpRPZVGta1LJQdgOIxpMJ1L42UR3Q52dSrfeJUhRjEvMm5dk8jxSfhwKfIMlFwtuY6SGt8g/0pJf8lySWTCwx/xI0ej8WtSAw+HeVrjo6u2rRJkkKnLIaQLRhzgRc4POuI2msLs6km+8WzTM1iSsOMllfi6v6iiT5gp17jsYb70UGniMqIBahHxjgRV3plgdEtQa5rsLbDQOaLe2ZVPMMkQMH3q58kI40uxU+MntRKYUfSzR+1+T8q34XvgU81tVoy9ei6fpbSRqA1On4sPbPLxXd8aWJTpcj+0HB3mhdn6h2TLe8pMiBUzYDiM5omolMcZ61wyqNmvYd0TwxJvGdSSicNSiN61EIwV0PqcmIW56xYgpLrU4kD4E+vv8zG+AdNoi4Hev64OodmxTEC1eLGDjl8wAGXLMDqBkv11RHZovqidrE/LM159Yjp6dIUuDgPP9jbOKqPSLMjt27fph/I9JOmNq4HJ/1iloAOhrNy2WiBEpZDACu8weoLRNFHQqrvDJodjEjYtZ3Gsr4CQq/qJsxwwXXMTbPPc9oD4M8qtosOBmkc9XiDQgWrClttqCp/LaIgRJ4ENg/2qfMTfilqCQNgUl9K9vW7NpNGgTtPVwk/bOoxcP8GhP9Ee8bkdMRJOyDk9gHbf0kOcIVFOBHyLxxog6F+e1MumVf0QoSizfCe9HnRB0K09eDHvkM3ABQWN/HzywUdQjod/82m275a1FdQaZdiUy7UdQi4BQrY1k2tdh2xK1/zqGmkyB+F+f86YHUUuFVmVRLXJSCRGckZyBqcej38xp8h+tI6XAI1gBmLaiO9m7bhJq1h6QMQSt1rtd7+DCmi3HKd4rqCQRcN3Euv7TYiSGxRBO8DJfuZZT6J0T9toY/SDSRfASF8GVRh0Ban9K1qvXnovpOoDFArHfbXKfCR/V/ymvh1511xeFwibeL6goK/hd4HZtNtVxZyqygTGoxgla+RNSiYa0KBoBDYH2HSAXRBu0wzuA/gRoAE58nog3ounmEc8go8nbzqj+mSLWcPNw5eHC/d8GI5ohaHGaVp9nA2XRrO3oW9scyFTVBplgCM4DaRPJ4RMyHi1qINq+LKNDut6LwvXX1WF1QTEDpBozoRwj4to/meQFxzbvFTPRAv9/WC1geNDZj/k4DSn4SmAHgi78qYkEQhbsNifYTa5h/FHJhnqiOoOafDaO6V1TfMFXecQxjCERFrQUwcuR4b4QNCmxkMDADQK05V8QC8DPZtsXPiuKMpgUiOYKa8jXU/OWi+kpnasnLuB7vM5CcXHoBOlc3v48329FBeNKTRfSdQAygLp48CZHtJ0QdAmqqJ5caTTSdiZL1cGOEb+1qa/53UQIBbvonIrpD5C0A3BFWPxWpEAf030gLgEAMQBOfKGJBzGzOU+RPzO6jYczPIlhzHFXzg3xOrRHRFZ332APYEcN07uqxsfONKJ8IxACI7Wf3Wt2zzsdu/T9RbbG8CHzfZFHtIeObIgVKT3vL763RPVFtwfW939O++E1RPZNpW/QCgkf7oXCush8xHAa+G4A19AsXaBuxE9OTIjqCLuTFItrCrP8V/fXnRS0H/yvv9nBxAeBOEFn7DRSGdBHrDrzjuwH0KHIc1mWDXQ3AWheIK3YdACE2bhWxLKDn4n6zyijdAEjx0yIWwvG+Qqn4bgCGMpwWRmS8LOQwDY+TRVc1F9/WDgMEtq4GwJwv+Zy0oj+IWIjRYQBw3QeKOAS0cb8X0RF8h2u3R+vy1n4LnH9MRFu0oUs3Sta2MYZ1G7r+9Kt2E9U3AggC7ZdGEdFrItoze0Gta7+X+bXuVYufEq1s4Pztb/8CVmpL78pb3K/RBjbVOyIWJGdkfPcCvhsA3KTtSSJCfl1EW6Jbtp6CN8e7lKxo2LN0SwEewHm5epEDQB+nx6x37GXkDNpdRN/wvwlQ9plksPEnEe0x6FiRbDFc5tQFBazSeY4ADcP9WyxfaE0T6xlQhmKoqrEi+ob/HsDaiNEGrXSniLbg/51uIPWjuarsBlAbT+6LNsBpg0kY+DC6gAKakS4Rh2AY7NgElUIAQaD9rh6GQa4GABNw7kYq9VI5F04Mgp7JwSLaQsP1AICYbZfAaXaYglYiQcQA9h6ADUcDkCjXcYdP9JXfELGsaK0dh7dhmZ3WTSPRSobJ3gPAO1Z+E4Aauk3EIRicd3ShvZT5jIi2sFbWnbOyQ+SyeQXxsN2/YDuDCsZhm7elEoAHsB/PZjIcp3AbhuEcZVsQud5H8Ju6hqY98cPHiVoYLuEG0MdZsMBAF9h2sQk+/EhE3/DfA7D9cKlW7GgA2sOuYKgFZfcAWqm/F9EeKrwIpBjqXup0XGmEZqjyDYCIbU8S1m0/QRSQBwMwNJfdAIi1qwGQDz0AZeQ/KVJBDK18j398NwBUUdvRLES4jm08a+3FA9gGSUFQF2/6O5y58xY2zN1d6Zb/Ea1kdN48QcSCdO2d8X0ntACCQPvRPgSIjgYAD7FFRFu8eAk/YdLu8xFpeCOAg8DTWKOghWHk65139onmGwEEgWQ/3Evs2JdmRa5P5GD2fzDEjtrEvCk4afcJmX4EgIDJ/h4IKk8g28wGYQC2d/zw2djYjKTtxAY21Ici2lPyjh/FQ8r8vohuDNsAYonkGeS01pDoGZF8xf8YQPc5DoawSWhTC1Ol8q4eAJnkPk3MB6KJpn/Ab3nbqLrKhwBQOc9/JK0CufvpuwF0td/yHuIA2xqBQNB2QCUSqXUf4mUYwKxZgW5vNzAjiX8oqgucyzw8vIkp0XjjWciZL4g6FFbrg5r84r8HAMTqP0UcCvOJ6uhLIqLthLU/ALN6UdTCwC/X9U1wXHQyLGYtqDZNut/RHe+ID+0/kXJczMKsHxLRdwIxAHgA+7t1RHWx/epsCxCZ4TplTCt1voi+E+vdZj3qxfWW9J+hYd2YisaTKHyyHSBD8Pdedq/Mj0T1nUAMIMYRywBw7oVBLbdfNcTuBoBg8qQBt+kvCMRW4MuLe/oYqXdFKhprCToM3nFbHGTiTUF0/wYJxAA+WLVoG07cdvEHCvA0azMGUXfC5PxaEd24LTrj2/uIPCys3cphUNY2tEUbFYy5+OHZaXNqYvHGB/DfjvsPIA9/151qbhY1EAIxAAu0oY6rf0xtFNwNbNuqJR24cNelY0S0J1F+Rd30uZ+SpJKwtmTL9akX8X32gzBOkNpLJE9Ymz/FaiIbcAHucYzmwLeORTkFB1zqH/G234BWAK2OKhTdWrttG6Sc5shvxwoa2dSXd69s9XT8INY2b1VKWaN8w9qNC8bakU01HypqQSwjZdO4QDFdhIL39jwBxUszqZZviRIYgRpAtKHpBnT7rN0xbeAVuMiCbS5qyqOolV8S1RUEnsuY+a7udOuvJGkoMxujsT41FYV2Fr77G5I6bPB99ximbqk2at7u7c7WwrGOz5s0npgORwZbS9wc9woaAqt0Jt1cls0igzUAtNFk5B2nOqPLeHJXuvkXom4nFp93hCKz6C4WjOBPuKhfM9FmqFtIcTVqHtw0743ad/zAUd5Awc7H3wRiFtvBqwBoz0Teaghi5/JCBDqgkutYsy1y6OQ9UMy23SrU3Am5jnX/Jup2+jqeebfq0ClWYRa1OwZqdh3+TML/HYXXcf2/TeowpNk3RQVgpb+ZTbUsjUycshu+0/dH1xUCzdl92XTzOWqDtY1geQgsCBwkn1e3iFgQZO6J0YZkwWYAbevtyJSyP4IVvzk3m2rt37aF+syHYA0eJrMOi41a6XNQ+IGNb9gR+BND8q+u+7Bq4hRrE0bbe+qoqZ+rOuT45ZbHkKTt5DrWtllP1IChuE4X94GNrOgbKIjt29T3vbams2rSlAzOMRgvwHx7JmMkco+2/LeklJXAPYBFFfV8D7XK6UbPAcrIDWkGBsmmW74Gl7xY1EBAU3R3RpufLbSRZP/DHpk9bWrhFev3EAAdkUm3XKaeWFzWSS47UpZnBvW+8my2etKxW1DXp0vSEBBoHWh5CtT41ZK0E7lX1j0OT/AuauIktBt+LpF6B+734u5U642qY43tBs59HeseRjywO060iGHij8NvWj0GNoxzu9ual/W9su49+WDEQH6Wj2ii8TFrFFDUwrC6El2gm0QrSDSenIMm5QqIRQV2O8H8Otz9Erj7olYZW9vT51VfEud5PJolx/6/5fWQwb9FLj+mtPpZuZeze6GsBlB31vzDOGf8Br9a8G7gIOjKzYTbf1hUW6z5epr0iTAqa9GG2566VmT9HP48idfq7lSz/W4cHhkYiu7bB93VvZnyUYPoI83UyYbe1PNB/Sb1y4Uj/mRQN8pqABaxePIi/OpdohbGekYg61mZVa2PSIo7sxaMqendMt5gGk/9gzBqfJ5UzsipLUzG5uzWMc+jQHJydIhQdgOwiMYb74H7dB2J6++OFemiQ4pjRAyg/25YbcRywV6eurEEkfJ80UYNYxPJQ/LMOG/qy1S/lSzXyF6xjIwBgNozm/Y3NFt747muBwTtEc5/fWt6ieu08UrAepQN4pK78ZLFnNyeSbWU7UlgxVCWbmAhci+v3RqZNPkpBGQzkVFue+9MzBPNiBwyeX2uY13Zl4YXQzTRdJ2h6BZcU40kAZoYmTh5HLqSFffo2xHzAIPAE3yB8noVYgJPCz6sfrQ29PU9K1tHZJm4HdZ1GHlehBy13dETvZvL0bu5TdSKYMQNwCI6Y/4xMIAVimh/SXKFFd9UzfqGkW4W+u94Uq4J556UJFtgvB9lU81lXdnkRkUYgIU1FdswaAV5eRqXgBq1FRdwA4JEj1O4/aP2jLn7GaY5z0vB78DGTKrZs5GXg4oxAIsxZyTH6yq+B6dVbMC0kYkeMHL0YFf7zS9JWiDE4snTFPEFOMfiH+VS4iNxg6SiDGAQr49cLwgzupf8U63Ur7rTrVZXE563dKxta3oj2amKzWnwTtYwdtHDz2xtH8f60uyq1uckqWKoSAOwiCXmfUkpYylOcTiPb+tB5j9Niq0HPmwwlLEhR71vVeWps6s+0qnuX9xlbUw5pqt3bF+ucxypmj3RP9kXvzsBGfNXKLljEZt8Vr6rNFjdgVpflh3NS6FiDaCfWbPMaN++1+Ekr5QUv7G8QzB5wGo9GerqrrbmJySlIqlsAxCsFcVMfB1qo+dJoiMFeiebDVbXdaVbAp2/4BejwgAGiTU0nc6arWcRnipJlYM1bYzUzZlc5yK1+k7bvf4qjVFlAIPUzWicyob18OXgnqblFXRFX0GMsSyi+LbRMlS9I6PSAAaRZ/2ei0DvXDQPnh/87AMZ/OZKIr0sk2qtuOHdYhjVBrAj/ZNDFJ+K2niCKnL+vxcQLVpT2h5ROp/K1rzTVql394pllzGAnZg2p6au2jyBiY7GayKMYiIzHYrYweNcQmvuHr1udR+VVs8Zhn7Wj21gK5Fd0wBssEYa+6r1ODNn1Fs3nzTl60mZvVqrLsM0Os286tq219Y3glyOHRISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEhISEuI/Sv0/3i/i95ZMZq0AAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAF7SURBVDhP1VIxS0NBDM53LWKFirNaZxHcBFsXN3EQ6eIsCF3lCbqquLd9oygUZ6H46mLVP9BBXFQobqLuUrEVqi/mrvFpK7iK35Jcku+7XHL054DaLiQWvSkDzDJoJCSuvQb+oaZ+oEugf2F1DPHYHhiDBK7ZGBMlwTQvlTvNoLjvCr8hpvaTfGKYysy8C2DSJUI+gMGtCHl945l2u167dHGFUUsmHvOknVMGXwn5SG9+VN8ToWVpZ1PLI0QCklwK3959YrMtQn4rKOZeKsW8zKEqT7puHfsX8uL7gaw3pwyHSEAKG84BZSzR+QrpoO4cmUtINOR8xVcHhFGKmzQxP2jAwQ6QEd5YX4QmhPDkEopoC4nsWpWJSyCzL5W+JYGwIiUZewY4KW2mm5XCjFIcog5AXBDCFlOYk2unDRs71LLE1+U8zIznXrJFJNAM/DNZYckw8mLPbXFn73wn6ZQh7syoB10fycL+QsBsiJuyZ1lrwz7tt9/4r0H0ATwWkxzmMaQRAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Domains"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TLDs"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
