#tag Class
Protected Class GraffitiWebGridRow
	#tag Method, Flags = &h0
		Function Cell(Column as GraffitiWebGridColumn) As Variant
		  if IsNull( Column ) then Return nil
		  
		  if cells.HasKey( Column.ID ) then Return Cells.Value( Column.ID )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Cell(Column as GraffitiWebGridColumn, Assigns Value as Variant)
		  if IsNull( Column ) then Return
		  
		  Cells.Value( Column.ID ) = Value
		  
		  if not IsNull( mObserver ) then mObserver.UpdatePropertyMultiple( "cellchange", me, Column )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(rowCells as Dictionary, isEnabled as Boolean = True, rowTag as Variant = nil)
		  mID = GraffitiControlWrapper.GenerateUUID
		  Cells = RowCells
		  if IsNull( Cells ) then Cells = new Dictionary
		  Enabled = isEnabled
		  Tag = rowTag
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Cells As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		Children() As GraffitiWebGridRow
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mColSpanField
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mColSpanField = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "colspan" )
			End Set
		#tag EndSetter
		ColSpanField As GraffitiWebGridColumn
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mColSpanLength
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mColSpanLength = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "colspan" )
			End Set
		#tag EndSetter
		ColSpanLength As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mEnabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEnabled = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "enabled" )
			End Set
		#tag EndSetter
		Enabled As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Expandable As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mExpanded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mExpanded = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "expanded" )
			End Set
		#tag EndSetter
		Expanded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		ID As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Indent As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mColSpanField As GraffitiWebGridColumn
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mColSpanLength As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnabled As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mExpanded As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		mObserver As GraffitiUpdateInterface
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelected As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h0
		Parent As GraffitiWebGridRow
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSelected
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelected = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "selected" )
			End Set
		#tag EndSetter
		Selected As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "style" )
			End Set
		#tag EndSetter
		Style As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Expandable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Indent"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Selected"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ColSpanLength"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Expanded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
