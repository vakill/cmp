#tag Class
Protected Class GraffitiWebGridColumn
	#tag Method, Flags = &h0
		Sub Constructor(colID as String, colName as String)
		  me.ID = colID
		  me.Title = colName
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		DecimalPlaces As Integer = 4
	#tag EndProperty

	#tag Property, Flags = &h0
		Editor As GraffitiWebGrid.EditTypes
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFocusable
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFocusable = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "focusable" )
			End Set
		#tag EndSetter
		Focusable As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Formatter As GraffitiWebGrid.FormatTypes
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mHeaderStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeaderStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "headerstyle" )
			End Set
		#tag EndSetter
		HeaderStyle As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		ID As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mMaxWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMaxWidth = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "maxWidth" )
			End Set
		#tag EndSetter
		MaxWidth As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mFocusable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeaderStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mMinWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMinWidth = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "minWidth" )
			End Set
		#tag EndSetter
		MinWidth As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mMaxWidth As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMinWidth As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		mObserver As GraffitiUpdateInterface
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mResizeable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSortable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTitle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mToolTip As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisible As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mWidth As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mResizeable
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mResizeable = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "resizeable" )
			End Set
		#tag EndSetter
		Resizeable As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSelectable
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelectable = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "selectable" )
			End Set
		#tag EndSetter
		Selectable As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSortable
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSortable = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "sortable" )
			End Set
		#tag EndSetter
		Sortable As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "style" )
			End Set
		#tag EndSetter
		Style As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTitle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTitle = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "title" )
			End Set
		#tag EndSetter
		Title As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mToolTip
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mToolTip = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "tootltip" )
			End Set
		#tag EndSetter
		ToolTip As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mVisible
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mVisible = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "visible" )
			End Set
		#tag EndSetter
		Visible As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mWidth
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mWidth = value
			  
			  if not IsNull( mObserver ) then mObserver.UpdateProperty( me, "width" )
			End Set
		#tag EndSetter
		Width As Integer
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Resizeable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Focusable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Selectable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Sortable"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinWidth"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxWidth"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ToolTip"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Editor"
			Group="Behavior"
			Type="GraffitiWebGrid.EditTypes"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Formatter"
			Group="Behavior"
			Type="GraffitiWebGrid.FormatTypes"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
