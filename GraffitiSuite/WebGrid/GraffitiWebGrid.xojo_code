#tag Class
Protected Class GraffitiWebGrid
Inherits GraffitiControlWrapper
Implements GraffitiUpdateInterface
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  // 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "resized"
		      'UpdateStyles()
		    case "toggleClick"
		      if Parameters.Ubound >= 1 then
		        dim theRow as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		        theRow.Expanded = Parameters(1).BooleanValue
		      end if
		    case "onContextMenu"
		      if Parameters.Ubound >= 3 then
		        dim X as Integer = Parameters(2)
		        dim Y as Integer = Parameters(3)
		        dim theCell as GraffitiWebGridColumn = Column( Parameters(1).IntegerValue )
		        dim theRow as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		        if not IsNull( theCell ) and not IsNull( theRow ) then
		          ContextClick( theRow, theCell, X, Y )
		        end if
		      end if
		    case "onColumnsResized"
		      dim j as new JSONItem( Parameters(0) )
		      mLockUpdate = True
		      
		      dim currColumn as GraffitiWebGridColumn
		      dim currJ as JSONItem
		      dim theCol, theWidth as String
		      
		      for i as Integer = 0 to j.Count - 1
		        currJ = j.Child(i)
		        theCol = currJ.Lookup( "column", "" )
		        theWidth = currJ.Lookup( "width", "0" )
		        
		        if theCol.Len > 0 then
		          currColumn = Column( theCol )
		          if IsNull( currColumn ) then Continue
		          
		          if theWidth.Val <> currColumn.Width then
		            currColumn.Width = theWidth.Val
		            ColumnResized( currColumn )
		          end if
		        end if
		      Next
		      LockUpdate = False
		    case "onViewportChanged"
		      mRenderedTopRow = Parameters(0).IntegerValue
		      mRenderedBottomRow = Parameters(1)
		      StyleRowEven = mStyleRowEven
		      StyleRowOdd = mStyleRowOdd
		    case "onDblClick"
		      dim Row as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      dim Column as GraffitiWebGridColumn = Column( Parameters(1).StringValue )
		      mSelectedCell = ColumnIndex( Parameters(1).StringValue )
		      CellDoubleClick( row, Column )
		    case "onActiveCellChange"
		      dim Row as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      dim Column as GraffitiWebGridColumn = Column( Parameters(1).StringValue )
		      mListIndex = RowIndex( Row )
		      mSelectedCell = ColumnIndex( Parameters(1).StringValue )
		      
		      DeleteSelRows()
		      SelectedRows.Append( Row )
		      
		      SelectionChanged()
		    case "onClick"
		      dim Row as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      dim Column as GraffitiWebGridColumn = Column( Parameters(1).StringValue )
		      mListIndex = Parameters(0)
		      mSelectedCell = ColumnIndex( Parameters(1).StringValue )
		      CellClick( Row, Column, Parameters(2).IntegerValue, Parameters(3).IntegerValue )
		    case "onKeyPress"
		      if Parameters.Ubound < 5 then Return True
		      dim Row as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      dim Column as GraffitiWebGridColumn = Column( Parameters(1).StringValue )
		      KeyPress( Row, Column, Parameters(2).IntegerValue, Parameters(3).BooleanValue, Parameters(4).BooleanValue, Parameters(5).BooleanValue )
		    case "onSelectedRowsChanged"
		      DeleteSelRows()
		      AddSelRows( Parameters )
		      SelectionChanged()
		      'UpdateStyles()
		    case "onUnlockUpdate"
		      ApplyCellStyles()
		    case "onRenderCompleted"
		      'UpdateStyles()
		      HideLoader()
		    case "onSort"
		      dim Column as GraffitiWebGridColumn = Column( Parameters(0).StringValue )
		      Sorted( Column, (Parameters(1) = "true") )
		      'UpdateStyles()
		    case "sortData"
		      ReDim SortBuffer(-1)
		      if Parameters.Ubound >= 0 then
		        for intCycle as Integer = 0 to Parameters.Ubound
		          SortBuffer.Append( GetRowByID( Parameters(intCycle).StringValue ) )
		        next
		      end if
		      UpdateRows()
		    case "onScroll"
		      mRenderedTopRow = Parameters(2).IntegerValue + 3
		      mRenderedBottomRow = Parameters(3).IntegerValue
		      
		      dim newScrollX as Integer = Parameters(0).IntegerValue
		      dim newScrollY as Integer = Parameters(1).IntegerValue
		      if newScrollX <> lastScrollX or newScrollY <> lastScrollY then
		        lastScrollX = newScrollX
		        lastScrollY = newScrollY
		        ScrollPositionChanged( Parameters(0).IntegerValue, Parameters(1).IntegerValue )
		      end if
		    case "onCellChange"
		      dim strCell as String = Parameters(1).StringValue
		      
		      dim j as new JSONItem( Parameters(2).StringValue )
		      
		      dim theRow as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      UpdateRowValues( theRow, j, strCell )
		    case "onBeforeEditCell"
		      mEditing = True
		      
		      if not IsNull( StyleTextEditor ) then
		        Buffer( "window.GSjQuery('#" + me.ControlID + "').find('input.editor-text').addClass('" + StyleTextEditor.Name + "');" )
		      end if
		      
		      dim Row as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      dim Column as GraffitiWebGridColumn = Column( Parameters(1).StringValue )
		      EditBegin( Row, Column )
		    case "onBeforeCellEditorDestroy"
		      mEditing = False
		      
		      dim Row as GraffitiWebGridRow = Row( Parameters(0).StringValue )
		      dim Column as GraffitiWebGridColumn = Column( Parameters(1).StringValue )
		      EditEnd( Row, Column )
		    case "onColumnsReordered"
		      if Parameters.Ubound >= 0 then
		        dim arrNewOrder() as String = Split( Parameters(0), "," )
		        ReorderColumns( arrNewOrder )
		      end if
		    case "rowDrag"
		      if Parameters.Ubound >= 1 then
		        ReorderRows( Parameters(0).StringValue, Parameters(1).IntegerValue )
		      end if
		    case "onHeaderContext"
		      break
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "Style"
		    Dim st as WebStyle = WebStyle(value)
		    dim strOut() as String
		    
		    strOut.Append( "var thisControl = window.GSjQuery('#" + me.ControlID + "');" )
		    If not self.style.IsDefaultStyle or self.style.Name = "_Style" then strOut.Append( "thisControl.removeClass('" + self.Style.Name + "');" )
		    if not st.isdefaultstyle then
		      strOut.Append( "thisControl.addClass('" + st.Name + "');" )
		    else
		      strOut.Append( "thisControl.addClass('bg-white border border-dark');" )
		    end if
		    
		    Buffer( strOut )
		    
		    Return True
		    
		  case "left"
		    dim strExec() as String
		    strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		    strExec.Append( "theElement.style.left='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "}" )
		    Buffer( strExec )
		    
		    if isShown then UpdateDisplay()
		    Return True
		  case "top"
		    dim strExec() as String
		    strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		    strExec.Append( "theElement.style.top='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "}" )
		    Buffer( strExec )
		    
		    if isShown then UpdateDisplay()
		    Return True
		  case "width"
		    dim strExec() as String
		    strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		    strExec.Append( "theElement.style.width='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.maxWidth='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.minWidth='" + Str( Value.IntegerValue ) + "px';" )
		    strExec.Append( "}" )
		    Buffer( strExec )
		    
		    if isShown then UpdateDisplay()
		    Return True
		  case "height"
		    dim strExec() as String
		    strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		    strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		    strExec.Append( "theElement.style.height='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.maxHeight='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.minHeight='" + Str( Value.IntegerValue ) + "px';" )
		    strExec.Append( "}" )
		    Buffer( strExec )
		    
		    if isShown then UpdateDisplay()
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  UpdateOptions( False )
		  Open()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  if isShown then
		    UpdateDisplay()
		  end if
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  me.ExecuteJavaScript( "window.grid" + me.ControlID + " = [];" )
		  source.Append( "<style>#" + me.ControlID + " .gwGrid_HiddenColumn {display:none !important;}</style>" )
		  source.Append( "<style>#" + me.ControlID + " .bg-primary a{color:#fff} #" + me.ControlID + " .border-dark-alt{border-color:#bdbdbd!important}</style>" )
		  source.Append( "<div id='" + me.ControlID + "' style='overflow: visible;display:none;'></div>" )
		  source.Append( "<div id='" + me.ControlID + "_loader' class='mdl-spinner mdl-js-spinner is-active' style='top:50%;left:50%;'></div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateDisplay()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddCellStyle(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn, newStyle as WebStyle)
		  if IsNull( newStyle ) or IsNull( Row ) or IsNull( Column ) then Return
		  
		  dim rowIndex as Integer = RowIndex( Row )
		  dim columnIndex as Integer = ColumnIndex( Column )
		  
		  if rowIndex < 0 or columnIndex < 0 then Return
		  
		  dim strID as String = Str( rowIndex ) + "_" + Str( columnIndex )
		  CSSIdentifiers.Append( strID )
		  
		  dim strExec() as String
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  dim theHash as String = "{" + Str( rowIndex ) + ": {'" + Column.ID + "': '" + newStyle.Name + "'}}"
		  cellCSS.Append( new Dictionary( "Row" : rowIndex, "Col": Column.ID, "Style" : newStyle, "RowObj": Row ) )
		  strExec.Append( "var cellStyles = grid.getCellCssStyles( '" + strID + "');" )
		  strExec.Append( "if (typeof cellStyles === 'undefined') {" )
		  strExec.Append( "grid.addCellCssStyles('" + strID + "'," + theHash + ");" )
		  strExec.Append( "}" )
		  
		  if mLockUpdate then
		    CellStyles.Append( Join( strExec, "" ) )
		  else
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddChild(ParentRow as GraffitiWebGridRow, ChildRow as GraffitiWebGridRow)
		  if IsNull( ParentRow ) or IsNull( ChildRow ) then Return
		  
		  ChildRow.Parent = ParentRow
		  ParentRow.Children.Append( ChildRow )
		  
		  if ParentRow.Expanded then
		    dim newIndex as Integer = RowIndex( ParentRow ) + ParentRow.Children.Ubound + 1
		    InsertRow( ChildRow, newIndex )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AddClass(theClassID as String, addStyle as String)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('." + theClassID + "').addClass('" + addStyle + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AddClass(theClassID as String, addStyle as WebStyle)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('." + theClassID + "').addClass('" + addStyle.Name + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddColumn(newHeader as GraffitiWebGridColumn)
		  if not IsNull( newHeader ) then
		    newHeader.mObserver = me
		    Headers.Append( newHeader )
		    dim strExec() as String
		    strExec.Append( HeadersToString )
		    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		    strExec.Append( "grid.setColumns(columns);" )
		    
		    Buffer( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddRow(newRow as GraffitiWebGridRow)
		  if not IsNull( newRow ) then
		    newRow.mObserver = me
		    Rows.Append( newRow )
		    
		    dim strExec() as String
		    strExec.Append( "var dd = window.grid" + me.controlID + ".getData();" )
		    strExec.Append( "dd.splice( dd.length, 0, " + RowToString( newRow, Rows.Ubound ) + ");" )
		    
		    strExec.Append( jsItemMetaData )
		    
		    strExec.Append( "window.grid" + me.controlID + ".setData(dd, false);" )
		    if not LockUpdate then
		      strExec.Append( "window.grid" + me.controlID + ".updateRowCount();" )
		      strExec.Append( "window.grid" + me.controlID + ".render();" )
		    end if
		    
		    Buffer( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AddSelRows(Parameters() as Variant)
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Parameters )
		  
		  dim currRow as GraffitiWebGridRow
		  
		  for intCycle = 0 to intMax
		    currRow = Row( Parameters(intCycle).IntegerValue )
		    SelectedRows.Append( currRow )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ApplyCellStyles()
		  if CellStyles.Ubound >= 0 then
		    dim strStyles as String = Join( CellStyles, "" )
		    Buffer( strStyles )
		    ReDim CellStyles(-1)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ColSpan(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn, NumberOfColumns as Integer = -1)
		  if not IsNull( Row ) and not IsNull( Column ) then
		    
		    dim rowIndex as Integer = RowIndex( Row )
		    
		    dim strExec() as String
		    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		    
		    strExec.Append( "var theItem = grid.getDataItem(" + Str( rowIndex ) + ");" )
		    strExec.Append( "if (typeof theItem !== 'undefined') {" )
		    strExec.Append( "theItem._colSpan_field = '" + Column.ID + "';" )
		    strExec.Append( "theItem._colSpan_len = " + if( NumberOfColumns > 0, Str( NumberOfColumns ), "*" ) + ";" )
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		    
		    UpdateDisplay( False )
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Column(atIndex as Integer) As GraffitiWebGridColumn
		  if atIndex >= 0 and atIndex <= Headers.Ubound then Return Headers(atIndex)
		  
		  Return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Column(ID as String) As GraffitiWebGridColumn
		  dim intMax as Integer = ColumnCount - 1
		  for intCycle as Integer = 0 to intMax
		    dim currHeader as GraffitiWebGridColumn = Headers(intCycle)
		    if currHeader.ID = ID then Return currHeader
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ColumnCount() As Integer
		  Return UBound( Headers ) + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ColumnIndex(Column as GraffitiWebGridColumn) As Integer
		  Return Headers.IndexOf( Column )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ColumnIndex(ID as String) As Integer
		  dim intMax as Integer = ColumnCount - 1
		  for intCycle as Integer = 0 to intMax
		    dim currHeader as GraffitiWebGridColumn = me.Headers(intCycle)
		    if ID = ReplaceAll( currHeader.ID, " ", "_" ) then
		      Return intCycle
		    end if
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub CopyArray(fromArr() as GraffitiWebGridRow, toArr() as GraffitiWebGridRow)
		  ReDim toArr(-1)
		  
		  dim intMax as Integer = fromArr.Ubound
		  for intCycle as Integer = 0 to intMax
		    toArr.Append( fromArr(intCycle) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Debug()
		  dim strExec as String = "window.grid" + me.ControlID + ".debug();"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeleteAllRows()
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Rows )
		  for intCycle = intMax DownTo 0
		    Rows.Remove( intCycle )
		  next
		  
		  DeselectAllRows()
		  
		  dim strExec() as string
		  strExec.Append( "var dd = [];" )
		  strExec.Append( "window.grid" + me.controlID + ".setData(dd, false);" )
		  strExec.Append( "window.grid" + me.controlID + ".updateRowCount();" )
		  strExec.Append( "window.grid" + me.controlID + ".render();" )
		  strExec.Append( "window.grid" + me.ControlID + ".setSortColumns([]);" )
		  
		  Buffer( strExec )
		  
		  ReDim CellStyles(-1)
		  ReDim CellCSS(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DeleteSelRows()
		  ReDim SelectedRows(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAllRows()
		  dim strExec() as String
		  strExec.Append( "window.grid" + me.ControlID + ".resetActiveCell();" )
		  'strExec.Append( "window.grid" + me.ControlID + ".setSelectedRows([]);" )
		  Buffer( strExec )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Rows.Ubound
		  for intCycle = 0 to intMax
		    if Rows(intCycle).Selected then Rows(intCycle).Selected = False
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoLoad()
		  'UpdateOptions( False )
		  '
		  'dim oldSelType as SelectionTypes = SelectionType
		  'if SelectionType = SelectionTypes.Row then
		  'me.SelectionType = SelectionTypes.Cell
		  'else
		  'me.SelectionType = SelectionTypes.Row
		  'end if
		  '
		  'UpdateOptions()
		  '
		  'me.SelectionType = oldSelType
		  '
		  'UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditActiveCell()
		  if me.Editable then
		    dim strExec as String = "window.grid" + me.controlID + ".editActiveCell();"
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditCancel()
		  Buffer( "window.grid" + me.ControlID + ".getEditorController().cancelCurrentEdit();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditCell(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn)
		  if me.Editable then
		    if IsNull( Row ) or IsNull( Column ) then Return
		    
		    EditCell( RowIndex( Row ), ColumnIndex( Column ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditCell(Row as Integer, Column as Integer)
		  if me.Editable then
		    dim theColumn as GraffitiWebGridColumn = Column( Column )
		    if IsNull( theColumn ) then Return
		    
		    dim strEditor as String = GetEditorType( theColumn )
		    
		    if strEditor.Len = 0 then Return
		    
		    dim strExec() as String
		    strExec.Append( "window.grid" + me.ControlID + ".setActiveCell(" + Str( Row ) + "," + Str( Column ) + ");" )
		    strExec.Append( "window.grid" + me.controlID + ".editActiveCell(" + strEditor + ");" )
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub EditCommit()
		  Buffer( "window.grid" + me.ControlID + ".getEditorController().commitCurrentEdit();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EditorToString(editor as EditTypes) As String
		  select case editor
		  case EditTypes.Checkbox
		    Return "editor: Slick.Editors.Checkbox,"
		  case EditTypes.Date
		    Return "editor: Slick.Editors.Date,"
		  case EditTypes.Integer
		    Return "editor: Slick.Editors.Integer,"
		  case EditTypes.LongText
		    Return "editor: Slick.Editors.LongText,"
		  case EditTypes.Password
		    Return "editor: Slick.Editors.Password,"
		  case EditTypes.Percent
		    Return "editor: Slick.Editors.PercentComplete,"
		  case EditTypes.Text
		    Return "editor: Slick.Editors.Text,"
		  case EditTypes.YesNoSelect
		    Return "editor: Slick.Editors.YesNoSelect,"
		  case EditTypes.Currency
		    Return "editor: Slick.Editors.Currency,"
		  case EditTypes.Double
		    Return "editor: Slick.Editors.Double,"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FooterToString() As String
		  if not hasLoaded then Return ""
		  
		  dim strFooter() as String
		  
		  strFooter.Append( "if( typeof window.grid" + me.ControlID + ".onFooterRowCellRendered !== 'undefined' ) {" )
		  strFooter.Append( "window.grid" + me.ControlID + ".onFooterRowCellRendered.unsubscribe();" )
		  strFooter.Append( "}" )
		  if not IsNull( FooterRow ) then
		    strFooter.Append( "window.grid" + me.ControlID + ".onFooterRowCellRendered.subscribe(function (e,args) {" )
		    strFooter.Append( "window.GSjQuery(args.node).empty();" )
		    for i as Integer = 0 to FooterRow.Count - 1
		      dim strKey as string = FooterRow.Key(i)
		      dim strVal as String = FooterRow.Value( strKey )
		      strFooter.Append( if(i = 0, "if", "else if" ) + "(args.column.id == '" + EscapeString( strKey ) + "') {" )
		      strFooter.Append( "window.GSjQuery(args.node).html('" + EscapeString( strVal ) + "');" )
		      strFooter.Append( "}" )
		    next
		    strFooter.Append( "});" )
		  end if
		  
		  Return Join( strFooter, "" )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FormatterToString(formatter as FormatTypes) As String
		  select case formatter
		  case FormatTypes.Checkbox
		    return "formatter: Slick.Formatters.Checkmark," + _
		    "sorter: window.GSgridSorterString,"
		  case FormatTypes.Color
		    Return "formatter: Slick.Formatters.Color,"
		  case FormatTypes.Currency
		    Return "formatter: Slick.Formatters.Currency," + _
		    "sorter: window.GSgridSorterInteger,"
		  case FormatTypes.Date
		    Return "formatter: Slick.Formatters.Date," + _
		    "sorter: window.GSgridSorterDate,"
		  case FormatTypes.FontAwesome
		    Return "formatter: Slick.Formatters.FontAwesome,"
		  case FormatTypes.HTML
		    Return "formatter: Slick.Formatters.HTML," + _
		    "sorter: window.GSgridSorterString,"
		  case FormatTypes.LongText
		    Return "formatter: Slick.Formatters.LongText," + _
		    "sorter: window.GSgridSorterString,"
		  case FormatTypes.Password
		    Return "formatter: Slick.Formatters.Password,"
		  case FormatTypes.Percent
		    Return "formatter: Slick.Formatters.PercentComplete," + _
		    "sorter: window.GSgridSorterInteger,"
		  case FormatTypes.Picture
		    Return "formatter: Slick.Formatters.Picture,"
		  case FormatTypes.ProgressBar
		    Return "formatter: Slick.Formatters.PercentCompleteBar," + _
		    "sorter: window.GSgridSorterInteger,"
		  case FormatTypes.Text
		    return "sorter: window.GSgridSorterString,"
		  case FormatTypes.Tree
		    Return "formatter: Slick.Formatters.Tree," + _
		    "sorter: window.GSgridSorterString,"
		  case FormatTypes.YesNo
		    Return "formatter: Slick.Formatters.YesNo,"
		  case FormatTypes.Integer
		    Return "formatter: Slick.Formatters.Integer," + _
		    "sorter: window.GSgridSorterInteger,"
		  case FormatTypes.Double
		    Return "formatter: Slick.Formatters.Double," + _
		    "sorter: window.GSgridSorterInteger,"
		  end select
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetEditorType(theColumn as GraffitiWebGridColumn) As String
		  if IsNull( theColumn ) then return ""
		  
		  Select case theColumn.Editor
		  case EditTypes.Checkbox
		    Return "Slick.Editors.Checkbox"
		  case EditTypes.Date
		    Return "Slick.Editors.Date"
		  case EditTypes.Integer
		    Return "Slick.Editors.Integer"
		  case EditTypes.LongText
		    Return "Slick.Editors.LongText"
		  case EditTypes.Password
		    Return "Slick.Editors.Password"
		  case EditTypes.Percent
		    Return "Slick.Editors.PercentComplete"
		  case EditTypes.Text
		    Return "Slick.Editors.Text"
		  case EditTypes.YesNoSelect
		    Return "Slick.Editors.YesNoSelect"
		  end select
		  
		  Return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetRowByID(theID as String) As GraffitiWebGridRow
		  dim intMax as Integer = Rows.Ubound
		  dim currRow as GraffitiWebGridRow
		  for intCycle as Integer = 0 to intMax
		    currRow = Rows(intCycle)
		    if currRow.ID = theID then Return currRow
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GetRowCSSStyles(Row as Dictionary) As Dictionary()
		  dim arrRet() as Dictionary
		  
		  dim intMax as Integer = cellCSS.Ubound
		  for intCycle as Integer = 0 to intMax
		    dim currCSS as Dictionary = cellCSS(intCycle)
		    
		    if currCSS.Value( "RowObj" ) = Row then
		      arrRet.Append( currCSS )
		    end if
		  next
		  
		  Return arrRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function HeadersToString() As String
		  dim strBuffer() as String = Array( "var columns = [" )
		  dim currHeader as GraffitiWebGridColumn
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Headers )
		  for intCycle = 0 to intMax
		    currHeader = Headers(intCycle)
		    if IsNull( currHeader ) then Continue
		    
		    strBuffer.Append( "{id:'" + ReplaceAll( currHeader.ID, " ", "_" ) + "'," )
		    strBuffer.Append( "name: '" + EscapeString( currHeader.Title ) + "'," )
		    strBuffer.Append( "field: '" + ReplaceAll( currHeader.ID, " ", "_" ) + "'," )
		    strBuffer.Append( "focusable: " + Str( currHeader.Focusable ).Lowercase + "," )
		    if currHeader.MaxWidth > 0 then strBuffer.Append( "maxWidth: " + Str( currHeader.MaxWidth, "#" ) + "," )
		    if currHeader.Width > 0 then strBuffer.Append( "width: " + Str( currHeader.Width, "#" ) + "," )
		    strBuffer.Append( "minWidth: " + Str( currHeader.MinWidth, "#" ) + "," )
		    strBuffer.Append( "resizable: " + Str( currHeader.Resizeable ).Lowercase + "," )
		    strBuffer.Append( "selectable: " + Str( currHeader.Selectable ).Lowercase + "," )
		    strBuffer.Append( "sortable: " + Str( currHeader.Sortable ).Lowercase + "," )
		    if mEnableRowReorder then strBuffer.Append( "behavior: 'selectAndMove'," )
		    if currHeader.Formatter = FormatTypes.Double or currHeader.Editor = EditTypes.Double then strBuffer.Append( "_decimalPlaces: " + Str( currHeader.DecimalPlaces ) + "," )
		    
		    strBuffer.Append( "headerCssClass: '" )
		    if not IsNull( StyleHeader ) then
		      if not StyleHeader.isdefaultstyle then
		        strBuffer.Append( StyleHeader.Name )
		      end if
		    end if
		    if not IsNull( currHeader.HeaderStyle ) then
		      if not currHeader.HeaderStyle.isdefaultstyle then
		        strBuffer.Append( " " + currHeader.HeaderStyle.Name )
		      end if
		    end if
		    strBuffer.Append( "'," )
		    
		    if not IsNull( currHeader.Style ) then
		      if not currHeader.Style.isdefaultstyle then
		        strBuffer.Append( "cssClass: '" + currHeader.Style.Name + "'," )
		      end if
		    end if
		    
		    strBuffer.Append( "toolTip: '" + Str( currHeader.ToolTip ).Lowercase + "'," )
		    strBuffer.Append( EditorToString( currHeader.Editor ) )
		    strBuffer.Append( FormatterToString( currHeader.Formatter ) )
		    
		    strBuffer.Append( "}" )
		    
		    if intCycle < intMax then strBuffer.Append( "," )
		  next
		  
		  strBuffer.Append( "];" )
		  
		  Return Join( strBuffer )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub HeaderStyle(theHeader as GraffitiWebGridColumn, Assigns theStyle as WebStyle)
		  if not IsNull( theHeader ) then
		    dim oldStyle as WebStyle = theStyle
		    theHeader.HeaderStyle = theStyle
		    
		    dim strExec() as String
		    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		    
		    strExec.Append( "var col = grid.getColumns()[grid.getColumnIndex('" + theHeader.ID + "')];" )
		    strExec.Append( "var theHeaderStyle = col.headerCssClass;" )
		    if not IsNull( theStyle ) then
		      strExec.Append( "theHeaderStyle += ' " + EscapeString( theStyle.Name ) + "';" )
		    end if
		    if not IsNull( oldStyle ) then
		      strExec.Append( "theHeaderStyle = theHeaderStyle.replace(' " + EscapeString( oldStyle.Name ) + "', '');" )
		    end if
		    strExec.Append( "col.headerCssClass = theHeaderStyle;" )
		    strExec.Append( "grid.invalidate();" )
		    
		    Buffer( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub HideColumn(theColumn as GraffitiWebGridColumn)
		  if IsNull( theColumn ) then Return
		  
		  dim atIndex as Integer = ColumnIndex( theColumn )
		  'if IsNull( prevColWidths ) then prevColWidths = new Dictionary
		  
		  'prevColWidths.Value( theColumn.ID ) = theColumn.Width
		  
		  'UpdateColumnWidth( atIndex, 10 )
		  dim strExec() as String
		  'strExec.Append( HeadersToString )
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  strExec.Append( "var cols = grid.getColumns();" )
		  strExec.Append( "cols[" + Str( atIndex ) + "].width = 0;" )
		  strExec.Append( "cols[" + Str( atIndex ) + "].maxWidth = 0;" )
		  strExec.Append( "cols[" + Str( atIndex ) + "].cssClass = 'gwGrid_HiddenColumn';" )
		  strExec.Append( "cols[" + Str( atIndex ) + "].headerCssClass = 'gwGrid_HiddenColumn';" )
		  strExec.Append( "grid.setColumns(cols);" )
		  
		  if LockUpdate then
		    LockUpdateBuffer.Append( Join( strExec, "" ) )
		  else
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub HideColumn(atIndex as Integer, updateProp as Boolean = False)
		  if atIndex < 0 or atIndex > Headers.Ubound then Return
		  
		  dim theColumn as GraffitiWebGridColumn = Column( atIndex )
		  
		  HideColumn( theColumn )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub HideColumn(ColumnField as String)
		  HideColumn( Column( ColumnField ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub HideLoader()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_loader').css('display','none');" )
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebGrid -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddMoment( CurrentSession ) )
		    strOut.Append( AddjQueryUI( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		    strOut.Append( AddDatepicker( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "jQuery" ).Child( "jquery.insertclass.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "gridcore.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "cellrangesel.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "cellselmodel.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "colpick.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "editors.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "editor_base.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "formatters.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "grideventdrag.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "grid.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "rangedeco.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "rowselmodel.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "autotooltip.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "slick.dataview.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "slick.rowmovemanager.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "material" ).Child( "material.min.js" ), "graffitisuite" ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "material" ).Child( "material.min.css" ), "graffitisuite" ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "WebGrid" ).Child( "slick.grid.css" ), JavascriptNamespace ) )
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InsertChild(ParentRow as GraffitiWebGridRow, ChildRow as GraffitiWebGridRow, atIndex as Integer)
		  if IsNull( ParentRow ) or IsNull( ChildRow ) then Return
		  
		  if atIndex < 0 then atIndex = 0
		  if atIndex > ParentRow.Children.Ubound then atIndex = ParentRow.Children.Ubound
		  
		  ParentRow.Children.Insert( atIndex, ChildRow )
		  
		  if ParentRow.Expanded then
		    dim newIndex as Integer = RowIndex( ParentRow ) + (atIndex + 1)
		    InsertRow( ChildRow, newIndex )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InsertRow(Row as GraffitiWebGridRow, intIndex as Integer)
		  if intIndex < 0 then intIndex = 0
		  
		  if not IsNull( Row ) then
		    row.mObserver = me
		    
		    dim strExec() as String
		    
		    if intIndex > rows.Ubound then
		      me.Rows.Append( Row )
		      
		      strExec.Append( "var dd = window.grid" + me.controlID + ".getData();" )
		      strExec.Append( "dd.splice( dd.length, 0, " + RowToString( Row, intIndex ) + ");" )
		      
		      strExec.Append( jsItemMetaData )
		      
		      strExec.Append( "window.grid" + me.controlID + ".setData(dd, false);" )
		    else
		      me.Rows.Insert( intIndex, Row )
		      
		      strExec.Append( "var dd = window.grid" + me.controlID + ".getData();" )
		      strExec.Append( "dd.splice( " + Str( intIndex ) + ", 0, " + RowToString( intIndex ) + ");" )
		      
		      strExec.Append( jsItemMetaData )
		      
		      strExec.Append( "window.grid" + me.controlID + ".setData(dd, false);" )
		    end if
		    
		    if not LockUpdate then
		      strExec.Append( "window.grid" + me.controlID + ".updateRowCount();" )
		      strExec.Append( "window.grid" + me.controlID + ".render();" )
		    end if
		    
		    Buffer( strExec )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NextCell()
		  dim strExec as String = "window.grid" + me.controlID + ".navigateNext();"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NextRow()
		  dim strExec as String = "window.grid" + me.controlID + ".navigateDown();"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function OptionsToString() As String
		  dim strOptions() as String = Array( "var options = {" + _
		  "autoEdit:" + Str( mAutoEdit ).Lowercase + "," + _
		  "editable:" + Str( mEditable ).Lowercase + "," + _
		  "enableCellNavigation:true," + _
		  "enableColumnReorder: " + Str( mEnableColumnReorder ).Lowercase + "," + _
		  "enableRowReorder: " + Str( mEnableRowReorder ).Lowercase + "," + _
		  "forceFitColumns:" + Str( mForceFitColumns ).Lowercase + "," + _
		  "topPanelHeight:0," + _
		  "multiSelect:" + Str( mMultiSelectRows ).Lowercase + "," + _
		  "rowHeight:" + Str( mRowHeight, "#" ) + "," + _
		  "headerHeight:0," + _
		  "showHeaderRow: false," + _
		  "showFooterRow: false," + _
		  "page:1," )
		  
		  if not IsNull( mStyleRowEven ) then
		    strOptions.Append( "evenRowClass:'" + mStyleRowEven.Name + "'," )
		  else
		    strOptions.Append( "evenRowClass:'bg-white text-dark border-top border-bottom border-dark-alt'," )
		  end if
		  
		  if not IsNull( mStyleRowOdd ) then
		    strOptions.Append( "oddRowClass:'" + mStyleRowOdd.Name + "'," )
		  else
		    strOptions.Append( "oddRowClass:'bg-light text-dark border-top border-bottom border-dark-alt'," )
		  end if
		  
		  if not IsNull( mStyleCellBorder ) then
		    strOptions.Append( "cellBorderClass:'" + mStyleCellBorder.Name + "'," )
		  else
		    strOptions.Append( "cellBorderClass:'border-left-0 border-top-0 border-bottom-0 border-right'," )
		  end if
		  
		  if not IsNull( mStyleHeader ) then
		    strOptions.Append( "headerClass:'" + mStyleHeader.Name + "'," )
		  else
		    strOptions.Append( "headerClass:'bg-dark text-white border-left-0 border-top-0 border-bottom-0 border-right'," )
		  end if
		  
		  if not IsNull( mStyleTextEditor ) then
		    strOptions.Append( "editorClass:'" + mStyleTextEditor.Name + "'," )
		  else
		    strOptions.Append( "editorClass:'bg-light border-0'," )
		  end if
		  
		  if not IsNull( mStyleRowSelected ) then
		    if not mStyleRowSelected.isdefaultstyle then
		      strOptions.Append( "cellHighlightCssClass:'" + mStyleRowSelected.Name + "'," )
		      strOptions.Append( "selectedCellCssClass:'" + mStyleRowSelected.Name + "'," )
		    end if
		  else
		    strOptions.Append( "cellHighlightCssClass:'bg-primary text-white'," )
		    strOptions.Append( "selectedCellCssClass:'bg-primary text-white'," )
		  end if
		  
		  strOptions.Append( "multiColumnSort:true" )
		  strOptions.Append( "};" )
		  
		  Return Join( strOptions )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PreviousCell()
		  dim strExec as String = "window.grid" + me.controlID + ".navigatePrev();"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PreviousRow()
		  dim strExec as String = "window.grid" + me.controlID + ".navigateUp();"
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RemoveAllCellStyles()
		  dim intMax as Integer = CSSIdentifiers.Ubound
		  dim strExec() as String
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  
		  for intCycle as Integer = intMax DownTo 0
		    dim theKey as String = CSSIdentifiers(intCycle)
		    strExec.Append( "grid.removeCellCssStyles('" + theKey + "');" )
		  next
		  
		  Buffer( strExec )
		  UpdateDisplay( True )
		  'UpdateStyles()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveCellStyle(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn, theStyle as WebStyle)
		  if IsNull( Row ) or IsNull( Column ) or IsNull( theStyle ) then Return
		  
		  dim rowIndex as Integer = RowIndex( Row )
		  dim columnIndex as Integer = ColumnIndex( Column )
		  
		  dim intMax as Integer = cellCSS.Ubound
		  for intCycle as Integer = intMax DownTo 0
		    dim currD as Dictionary = cellCSS(intCycle)
		    if currD.Value( "Row" ) = rowIndex and currD.Value( "Col" ) = Column.ID and currD.Value( "Style" ) = theStyle then
		      cellCSS.Remove( intCycle )
		      
		      dim strExec() as String
		      strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		      
		      strExec.Append( "var cellStyles = grid.getCellCssStyles( '" + Str( rowIndex ) + "_" + Str( columnIndex ) + "');" )
		      strExec.Append( "if (typeof cellStyles !== 'undefined') {" )
		      strExec.Append( "grid.removeCellCssStyles('" + Str( rowIndex ) + "_" + Str( columnIndex ) + "');" )
		      strExec.Append( "}" )
		      
		      if mLockUpdate then
		        CellStyles.Append( Join( strExec, "" ) )
		      else
		        Buffer( strExec )
		      end if
		    end if
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveChild(ParentRow as GraffitiWebGridRow, ChildRow as GraffitiWebGridRow)
		  dim childIndex as Integer = ParentRow.Children.IndexOf( ChildRow )
		  if childIndex >= 0 and childIndex <= ParentRow.Children.Ubound then
		    ParentRow.Children.Remove( childIndex )
		    
		    if ParentRow.Expanded then
		      childIndex = RowIndex( ChildRow )
		      RemoveRow( childIndex )
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RemoveClass(theClassID as String, remStyle as String)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('." + theClassID + "').removeClass('" + remStyle + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RemoveClass(theClassID as String, remStyle as WebStyle)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('." + theClassID + "').removeClass('" + remStyle.Name + "');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveColumn(Column as GraffitiWebGridColumn)
		  if not IsNull( Column ) then
		    dim intHeader as Integer = ColumnIndex( Column )
		    RemoveColumn( intHeader )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveColumn(columnIndex as Integer)
		  if columnIndex >= 0 and columnIndex <= Headers.Ubound then
		    Headers.Remove( columnIndex )
		    
		    if not LockUpdate then
		      dim strExec() as String
		      strExec.Append( HeadersToString )
		      strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		      strExec.Append( "grid.setColumns(columns);" )
		      
		      Buffer( strExec )
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveRow(Row as GraffitiWebGridRow)
		  dim rowIndex as Integer = RowIndex( Row )
		  if rowIndex >= 0 and rowIndex <= rows.Ubound then RemoveRow( rowIndex )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveRow(intIndex as Integer)
		  if intIndex >= 0 and intIndex <= UBound( Rows ) then
		    Rows.Remove( intIndex )
		    dim strExec as String = "var dd = window.grid" + me.controlID + ".getData();" + _
		    "dd.splice( " + Str( intIndex, "#" ) + ", 1 );" + _
		    "window.grid" + me.controlID + ".setData(dd, false);"
		    
		    if not LockUpdate then
		      strExec = strExec + "window.grid" + me.controlID + ".updateRowCount();" + _
		      "window.grid" + me.controlID + ".render();"
		    end if
		    
		    Buffer( strExec )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RemoveRowStyle(Row as Integer, theStyle as WebStyle)
		  #Pragma Unused theStyle
		  'dim blnLock as Boolean = not mLockUpdate
		  'if not LockUpdate then LockUpdate = True
		  '
		  'dim intCycle as Integer
		  'dim intMax as Integer = Headers.Ubound
		  'for intCycle = 0 to intMax
		  'RemoveCellStyle( Row, intCycle, theStyle )
		  'next
		  '
		  'if blnLock then LockUpdate = False
		  
		  SetRowStyle( Row, Nil )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ReorderColumns(newOrder() as String)
		  dim arrReordered() as GraffitiWebGridColumn
		  
		  dim intCycle as Integer
		  dim intMax as Integer = newOrder.Ubound
		  
		  dim currColumn as GraffitiWebGridColumn
		  dim currField as String
		  for intCycle = 0 to intMax
		    currField = newOrder(intCycle)
		    currColumn = Column( currField )
		    if not IsNull( currColumn ) then
		      arrReordered.Append( currColumn )
		    end if
		  next
		  
		  Headers = arrReordered
		  
		  ColumnsReordered()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ReorderRows(rowID as String, newIndex as Integer)
		  dim theRow as GraffitiWebGridRow = Row( rowID )
		  dim currIndex as Integer = RowIndex( theRow )
		  
		  if IsNull( theRow ) then Return
		  
		  rows.Remove( currIndex )
		  
		  if currIndex < newIndex then newIndex = newIndex - 1
		  rows.Insert( newIndex, theRow )
		  
		  RowReordered( theRow, currIndex, newIndex )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ResetColumnWidth(theCol as Integer)
		  if theCol >= 0 and theCol < me.ColumnCount then
		    dim strExec() as String
		    'strExec.Append( jsSorters )
		    'strExec.Append( HeadersToString )
		    strExec.Append( "var columns = window.grid" + me.controlID + ".getColumns();" )
		    strExec.Append( "columns[" + Str( theCol, "#" ) + "].width = '';" )
		    strExec.Append( "window.grid" + me.ControlID + ".setColumns(columns);" )
		    
		    'UpdateColumnWidth( me.GetHeader( theCol ).id, 0, True )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ResetGrid()
		  ReDim Headers(-1)
		  ReDim Rows(-1)
		  
		  UpdateOptions( False )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Row(RowIndex as Integer) As GraffitiWebGridRow
		  if RowIndex >= 0 and RowIndex <= UBound( Rows ) then
		    Return Rows(RowIndex)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Row(RowID as String) As GraffitiWebGridRow
		  for each r as GraffitiWebGridRow in Rows
		    if r.ID = RowID then Return r
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RowCount() As Integer
		  Return UBound( Rows ) + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RowEnabled(Row as GraffitiWebGridRow, Assigns State as Boolean)
		  if IsNull( Row ) then Return
		  
		  dim rowIndex as Integer = RowIndex( Row )
		  
		  dim strExec() as String
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  
		  strExec.Append( "var theItem = grid.getDataItem(" + Str( rowIndex ) + ");" )
		  strExec.Append( "if (typeof theItem !== 'undefined') {" )
		  strExec.Append( "theItem.disabled = " + Str( not State ).Lowercase + ";" )
		  strExec.Append( "}" )
		  
		  Buffer( strExec )
		  
		  UpdateDisplay( False )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RowIndex(Row as GraffitiWebGridRow) As Integer
		  Return Rows.IndexOf( Row )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RowsToString() As String
		  dim strOut() as String
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Rows.Ubound
		  for intCycle = 0 to intMax
		    strOut.Append( "data[" + Str( intCycle, "#" ) + "] = " + RowToString( intCycle ) + ";" )
		  next
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RowToString(theRow as GraffitiWebGridRow, rowIndex as Integer) As String
		  if IsNull( theRow ) then Return ""
		  
		  dim currDict as Dictionary = theRow.Cells
		  if IsNull( currDict ) then Return ""
		  
		  dim intPair, intPairMax as Integer
		  dim strKey, strValue as String
		  
		  intPairMax = currDict.Count - 1
		  
		  dim strOut() as String
		  strOut.Append( "{" )
		  strOut.Append( "id: '" + theRow.ID + "'," )
		  strOut.Append( "originalIndex: " + Str( rowIndex ) + "," )
		  if not IsNull( theRow.Parent ) then strOut.Append( "_parent: '" + theRow.Parent.ID + "'," )
		  
		  if not IsNull( theRow.ColSpanField ) then
		    strOut.Append( "_colSpan_field: '" + theRow.ColSpanField.ID + "'," )
		    strOut.Append( "_colSpan_len: " + Str( theRow.ColSpanLength + 1 ) + "," )
		  end if
		  
		  strOut.Append( "_expandable: " + Str( theRow.Expandable ).Lowercase + "," )
		  strOut.Append( "_indent: " + Str( theRow.Indent ) + "," )
		  
		  if not IsNull( theRow.Style ) then strOut.Append( "_rowCss: 'customRowStyle " + theRow.Style.Name + "'," )
		  
		  for intPair = 0 to intPairMax
		    strKey = currDict.Key( intPair )
		    
		    if strKey = "RowTag" then Continue
		    
		    dim v as Variant = currDict.Value( strKey )
		    strOut.Append( """" + ReplaceAll( strKey, " ", "_" ) + """: " )
		    select case v.Type
		    case Variant.TypeBoolean
		      strOut.Append( v.StringValue.Lowercase )
		    case Variant.TypeInteger, Variant.TypeInt32, Variant.TypeInt64
		      strOut.Append( Str( v.IntegerValue, "#" ) )
		    case Variant.TypeDouble, Variant.TypeCurrency
		      dim theCol as GraffitiWebGridColumn = Column( strKey )
		      if IsNull( theCol ) then Continue
		      if theCol.Formatter = FormatTypes.Currency then
		        strOut.Append( chr(34) + Str( v.DoubleValue, "#.00" ) + chr(34) )
		      else
		        dim arrDec() as String
		        for intCycle as Integer = 0 to theCol.DecimalPlaces
		          arrDec.Append( "" )
		        next
		        strOut.Append( chr(34) + Str( v.DoubleValue, "#." + Join( arrDec, "0" ) ) + chr(34) )
		      end if
		    case Variant.TypeString, Variant.TypeText
		      strOut.Append( Chr( 34 ) + EscapeString( v.StringValue ) + Chr( 34 ) )
		    case Variant.TypeDate
		      dim theDate as Date = v
		      strOut.Append( "new Date(" + Str( theDate.Year )+ ", " + Str( theDate.Month - 1, "00" ) + ", " + Str( theDate.Day, "00" ) + ")" )
		    case Variant.TypeNil
		      strOut.Append( chr( 34 ) + chr( 34 ) )
		    case Variant.TypeSingle
		      strOut.Append( Str( v.SingleValue, "#" ) )
		    case Variant.TypeColor
		      strOut.Append( "'rgba(" + Str( v.ColorValue.Red, "#" ) + "," + Str( v.ColorValue.Green, "#" ) + "," + Str( v.ColorValue.Blue, "#" ) + "," + Str( 1 - v.ColorValue.Alpha, "0.00" ) + ")'" )
		    case Variant.TypeObject
		      if v isa Picture then
		        dim strPic as String = "data:image/png;base64," + ReplaceLineEndings( EncodeBase64( Picture( v.ObjectValue ).GetData( Picture.FormatPNG ) ), "" )
		        strOut.Append( "'" + strPic + "'" )
		      elseif v isa WebPicture then
		        strOut.Append( "'" + WebPicture( v ).URL + "'" )
		      end if
		    end select
		    
		    if intPair < intPairMax then strOut.Append( "," )
		  next
		  
		  strOut.Append( "}" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RowToString(CurrentRow as Integer) As String
		  Return RowToString( Row( CurrentRow ), CurrentRow )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollTo(PositionX as Integer, PositionY as Integer)
		  dim strExec() as String
		  strExec.Append( "var display = window.GSjQuery('#" + me.ControlID + " .slick-viewport');" )
		  strExec.Append( "display.scrollLeft(" + Str( PositionX ) + ").scrollTop(" + Str( PositionY ) + " );" )
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollToCell(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn)
		  if IsNull( Row ) or IsNull( Column ) then Return
		  
		  ScrollToCell( RowIndex( Row ), ColumnIndex( Column ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollToCell(Row as Integer, Column as Integer)
		  if Row >= 0 and Row < me.RowCount then
		    
		    Buffer( "window.grid" + me.controlID + ".scrollCellIntoView(" + Str( Row, "#" ) + "," + Str( Column, "#" ) + ");" )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollToRow(Row as GraffitiWebGridRow)
		  if not IsNull( Row ) then ScrollToRow( RowIndex( Row ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ScrollToRow(Row as Integer)
		  if Row >= 0 and Row < me.RowCount then
		    
		    dim strExec as String = "window.grid" + me.controlID + ".scrollRowIntoView(" + Str( Row, "#" ) + ",false);"
		    Buffer( strExec )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SelectRow(Row as GraffitiWebGridRow)
		  if IsNull( Row ) then Return
		  
		  
		  if MultiSelectRows then
		    
		    if row.Selected then
		      SelectedRows.Append( Row )
		    else
		      dim selIndex as Integer = SelectedRows.IndexOf( Row )
		      if selIndex >= 0 and selIndex <= SelectedRows.Ubound Then
		        SelectedRows.Remove( selIndex )
		      end if
		    end if
		    
		    Dim strData as String
		    
		    Dim intCycle as Integer
		    dim intMax as Integer = SelectedRows.Ubound
		    For intCycle = 0 to intMax
		      strData = strData + Str( RowIndex( SelectedRows(intCycle) ), "#" )
		      if intCycle < intMax Then strData = strData + ","
		    Next
		    
		    strData = "[" + strData + "]"
		    
		    UpdateDisplay()
		    dim strExec as String = "window.grid" + me.controlID + ".setSelectedRows(" + strData + ");"
		    Buffer( strExec )
		  Else
		    ReDim SelectedRows(-1)
		    if row.Selected then SelectedRows.Append( Row )
		    SetActiveCell( RowIndex( Row ), 0 )
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SelectRows(SelRows() as Integer)
		  if UBound( SelRows ) >= 0 then
		    if MultiSelectRows then
		      Dim strData as String
		      
		      Dim intCycle as Integer
		      dim intMax as Integer = UBound( SelRows )
		      For intCycle = 0 to intMax
		        strData = strData + Str( SelRows(intCycle), "#" )
		        if intCycle < intMax Then strData = strData + ","
		      Next
		      
		      strData = "[" + strData + "]"
		      
		      UpdateDisplay()
		      dim strExec as String = "window.grid" + me.controlID + ".setSelectedRows(" + strData + ");"
		      Buffer( strExec )
		    Else
		      SetActiveCell( SelRows(0), 0 )
		    End If
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetActiveCell(Row as Integer, Cell as Integer = 0, ForceEdit as Boolean = False)
		  if Row >= 0 and row <= rows.Ubound and cell >= 0 and cell <= Headers.Ubound then
		    
		    dim strExec as String
		    if not ForceEdit then
		      strExec = "window.grid" + me.controlID + ".setActiveCell(" + Str( Row, "#" ) + "," + Str( Cell, "#" ) + ");"
		    else
		      strExec = "window.grid" + me.controlID + ".gotoCell(" + Str( Row, "#" ) + "," + Str( Cell, "#" ) + ",true);"
		    end if
		    
		    Buffer( strExec )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetChild(Row as Integer, newParent as Integer)
		  dim strExec as String = "window.grid" + me.controlID + ".invalidateRow(" + Str( Row, "#" ) + ");" + _
		  "var dd = window.grid" + me.controlID + ".getData();" + _
		  "dd[" + Str( Row, "#" ) + "]['parent'] = {id:" + Str( newParent ) + "};" + _
		  "dd[" + Str( Row, "#" ) + "]['indent'] = 1;"
		  
		  if not LockUpdate then
		    strExec = strExec + "window.grid" + me.controlID + ".updateRowCount();" + _
		    "window.grid" + me.controlID + ".render();"
		  end if
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetColumnProperty(ColumnID as String, PropertyName as String, PropertyValue as String)
		  if ColumnID.Len = 0 or PropertyName.Len = 0 or PropertyValue.Len = 0 then Return
		  
		  dim strExec() as String
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  strExec.Append( "var col = grid" + me.ControlID + ".getColumns()[grid.getColumnIndex('" + ColumnID + "')];" )
		  strExec.Append( "col." + PropertyName + " = " + PropertyValue + ";" )
		  
		  if not mLockUpdate then
		    strExec.Append( "grid.invalidate();" )
		    Buffer( strExec )
		  else
		    LockUpdateBuffer.Append( Join( strExec, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetRowExpanded(Row as GraffitiWebGridRow)
		  if not IsNull( Row ) then
		    
		    dim rowIndex as Integer = RowIndex( Row )
		    
		    dim strExec() as String
		    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		    
		    strExec.Append( "var theItem = grid.getDataItem(" + Str( rowIndex ) + ");" )
		    strExec.Append( "if (typeof theItem !== 'undefined') {" )
		    strExec.Append( "theItem._collapsed = " + Str( Row.Expanded ).Lowercase + ";" )
		    strExec.Append( "}" )
		    
		    Buffer( strExec )
		    
		    if Row.Expanded then
		      tree_AddChildren( Row )
		    else
		      tree_RemoveChildren( Row )
		    end if
		    
		    UpdateDisplay( False )
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetRowStyle(Row as Integer, newStyle as WebStyle)
		  if row >= 0 and row <= UBound( Rows ) then
		    
		    dim strExec as String = "window.grid" + me.controlID + ".invalidateRow(" + Str( Row, "#" ) + ");" + _
		    "var dd = window.grid" + me.controlID + ".getData();" + _
		    "dd[" + Str( Row, "#" ) + "]['_rowCss'] = '" + If( not IsNull( newStyle ), "customRowStyle " + newStyle.Name, "" ) + "';"
		    
		    if not LockUpdate then
		      strExec = strExec + "window.grid" + me.controlID + ".updateRowCount();" + _
		      "window.grid" + me.controlID + ".render();"
		    end if
		    
		    Buffer( strExec )
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ShowColumn(theColumn as GraffitiWebGridColumn)
		  if IsNull( theColumn ) then Return
		  
		  dim strExec() as String
		  strExec.Append( HeadersToString )
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  strExec.Append( "grid.setColumns(columns);" )
		  
		  if LockUpdate then
		    LockUpdateBuffer.Append( Join( strExec, "" ) )
		  else
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ShowColumn(atIndex as Integer)
		  dim theColumn as GraffitiWebGridColumn = Column( atIndex )
		  
		  ShowColumn( theColumn )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ShowColumn(ColumnField as String)
		  dim intIndex as Integer = ColumnIndex( ColumnField )
		  
		  ShowColumn( intIndex )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ShowLoader()
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_loader').css('display','block');" )
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Sort(SortDirection as SortDirections, Column as GraffitiWebGridColumn)
		  if IsNull( Column ) then Return
		  
		  dim arrSortBool() as String
		  dim arrSortInteger() as Integer
		  dim arrSortDouble() as Double
		  dim arrSortString() as String
		  dim arrSortDate() as Integer
		  dim arrSortSingle() as Single
		  dim arrSortColor() as Double
		  
		  dim arrIDs() as String
		  
		  dim ColType as Integer
		  
		  dim intCycle as Integer
		  dim intMax as Integer = Rows.Ubound()
		  
		  dim currRow as GraffitiWebGridRow
		  dim currDict as Dictionary
		  
		  for intCycle = 0 to intMax
		    currRow = Row( intCycle )
		    currDict = currRow.Cells
		    
		    arrIDs.Append( currRow.ID )
		    
		    dim v as Variant = currDict.Value( column.ID )
		    ColType = v.Type
		    
		    select case v.Type
		    case Variant.TypeBoolean
		      arrSortBool.Append( v )
		    case Variant.TypeInteger, Variant.TypeInt32, Variant.TypeInt64
		      arrSortInteger.Append( v )
		    case Variant.TypeDouble, Variant.TypeCurrency
		      arrSortDouble.Append( v )
		    case Variant.TypeString, Variant.TypeText
		      arrSortString.Append( v )
		    case Variant.TypeDate
		      arrSortDate.Append( v.DateValue.TotalSeconds )
		    case Variant.TypeSingle
		      arrSortSingle.Append( v )
		    case Variant.TypeColor
		      dim c as Color = v.ColorValue
		      dim l as Double = (0.2126 * c.Red) + (0.7152 * c.Green) + (0.0722 * c.Blue)
		      arrSortColor.Append( l )
		    end select
		  next
		  
		  dim newRowOrder() as GraffitiWebGridRow
		  
		  select case ColType
		  case Variant.TypeBoolean
		    arrSortBool.SortWith( arrIDs )
		  case Variant.TypeInteger, Variant.TypeInt32, Variant.TypeInt64
		    arrSortInteger.SortWith( arrIDs )
		  case Variant.TypeDouble, variant.TypeCurrency
		    arrSortDouble.SortWith( arrIDs )
		  case Variant.TypeString, variant.TypeText
		    arrSortString.SortWith( arrIDs )
		  case Variant.TypeDate
		    arrSortDate.SortWith( arrIDs )
		  case Variant.TypeSingle
		    arrSortSingle.SortWith( arrIDs )
		  case Variant.TypeColor
		    arrSortColor.SortWith( arrIDs )
		  case else
		    'newRowOrder = CustomSort( SortDirection, Column )
		  end select
		  
		  if newRowOrder.Ubound = -1 then
		    
		  end if
		  
		  Rows = newRowOrder
		  UpdateOptions( False )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function toCSV() As String
		  dim intCol as Integer
		  dim intColMax as Integer = Headers.Ubound
		  
		  dim headerFields() as String
		  dim strHeaders() as String
		  
		  dim strRows() as String
		  
		  for intCol = 0 to intColMax
		    dim thisCol as GraffitiWebGridColumn = Headers(intCol)
		    strHeaders.Append( thisCol.Title )
		    headerFields.Append( thisCol.Title )
		  next
		  
		  dim intRow as Integer
		  dim intRowMax as Integer = Rows.Ubound
		  for intRow = 0 to intRowMax
		    dim dRow as Dictionary = Rows(intRow).Cells
		    dim thisRow() as String
		    
		    intColMax = headerFields.Ubound
		    for intCol = 0 to intColMax
		      dim thisValue as Variant = dRow.Value( headerFields(intCol) )
		      
		      dim strValue as String
		      select case thisValue.Type
		      case Variant.TypeBoolean
		        strValue = thisValue.StringValue.Lowercase
		      case Variant.TypeInteger, Variant.TypeInt32, Variant.TypeInt64
		        strValue = Str( thisValue.IntegerValue, "#" )
		      case Variant.TypeDouble, Variant.TypeCurrency
		        strValue = Str( thisValue.DoubleValue, "#.00" )
		      case Variant.TypeString, Variant.TypeText
		        strValue = thisValue.StringValue
		      case Variant.TypeDate
		        dim theDate as Date = thisValue
		        strValue = theDate.ShortDate + " " + theDate.ShortTime
		      case Variant.TypeNil
		        strValue = ""
		      case Variant.TypeSingle
		        strValue = Str( thisValue.SingleValue, "#" )
		      case Variant.TypeColor
		        strValue = ColorToHex( thisValue.ColorValue )
		      case Variant.TypeObject
		        if thisValue isa Picture then
		          dim strPic as String = "data:image/png;base64," + ReplaceLineEndings( EncodeBase64( Picture( thisValue.ObjectValue ).GetData( Picture.FormatPNG ) ), "" )
		          strValue = strPic
		        elseif thisValue isa WebPicture then
		          thisValue = WebPicture( thisValue ).URL
		        end if
		      end select
		      
		      thisRow.Append( Chr( 34 ) + strValue + Chr( 34 ) )
		    next
		    
		    strRows.Append( Join( thisRow, "," ) )
		  next
		  
		  Return Join( strHeaders, "," ) + EndOfLine.UNIX + Join( strRows, "," )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub tree_AddChildren(Row as GraffitiWebGridRow)
		  dim rowIndex as Integer = me.RowIndex( Row )
		  
		  dim currRow as GraffitiWebGridRow
		  for intCycle as Integer = 0 to Row.Children.Ubound
		    currRow = row.Children(intCycle)
		    currRow.Indent = row.Indent + 1
		    me.InsertRow( currRow, rowIndex + intCycle + 1 )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub tree_RemoveChildren(Row as GraffitiWebGridRow)
		  for intCycle as Integer = 0 to Row.Children.Ubound
		    dim rowIndex as Integer = me.RowIndex( Row.Children(intCycle) )
		    
		    me.RemoveRow( rowIndex )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateCell(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn)
		  if IsNull( Row ) or IsNull( Column ) then Return
		  
		  dim rowIndex as Integer = RowIndex( Row )
		  dim colIndex as Integer = ColumnIndex( Column )
		  
		  dim newValue as Variant = row.Cell( Column )
		  
		  dim strExec as String = "window.grid" + me.controlID + ".invalidateRow(" + Str( rowIndex, "#" ) + ");" + _
		  "var dd = window.grid" + me.controlID + ".getData();" + _
		  "dd[" + Str( rowIndex, "#" ) + "][window.grid" + me.ControlID + ".getColumns()[" + Str( colIndex, "#" ) + "].field] = "
		  
		  select case NewValue.Type
		  case Variant.TypeBoolean
		    strExec = strExec + Lowercase( Str( NewValue.BooleanValue ) ) + ";"
		    Row.Cells.Value( Column.ID ) = NewValue.BooleanValue
		  case Variant.TypeString
		    strExec = strExec + "'" + EscapeString( NewValue.StringValue ) + "';"
		    Row.Cells.Value( Column.ID ) = NewValue.StringValue
		  case Variant.TypeInteger
		    strExec = strExec + "" + Str( NewValue.IntegerValue, "#" ) + ";"
		    Row.Cells.Value( Column.ID ) = NewValue.IntegerValue
		  case Variant.TypeColor
		    dim strHex as string = "#" + Right( NewValue.StringValue, 6 )
		    strExec = strExec + "'" + strHex + "';"
		    Row.Cells.Value( Column.ID ) = NewValue.ColorValue
		  case Variant.TypeDate
		    strExec = strExec + "'" + NewValue.DateValue.Month.ToText + "/" + NewValue.DateValue.Day.ToText + "/" + NewValue.DateValue.Year.ToText + "‘;"
		    Row.Cells.Value( Column.ID ) = NewValue.DateValue
		  case Variant.TypeSingle
		    strExec = strExec + Str( NewValue.SingleValue, "0" ) + ";"
		    Row.Cells.Value( Column.ID ) = NewValue.SingleValue
		  case Variant.TypeCurrency
		    strExec = strExec + "'" + Str( NewValue.CurrencyValue, "#.##" ) + "';"
		    Row.Cells.Value( Column.ID ) = NewValue.CurrencyValue
		  case Variant.TypeDouble
		    strExec = strExec + "'" + Str( NewValue.DoubleValue, "#.##" ) + "';"
		    Row.Cells.Value( Column.ID ) = NewValue.DoubleValue
		  end select
		  
		  if not LockUpdate then
		    strExec = strExec + "window.grid" + me.controlID + ".updateRowCount();" + _
		    "window.grid" + me.controlID + ".render();"
		  end if
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateColumnWidth(theCol as Integer, theWidth as Integer, theMaxWidth as Integer = -1)
		  if theCol >= 0 and theCol < me.ColumnCount then
		    dim strExec() as String
		    strExec.Append( jsSorters )
		    strExec.Append( HeadersToString )
		    strExec.Append( "var cols = window.grid" + me.controlID + ".getColumns();" )
		    strExec.Append( "cols[" + Str( theCol, "#" ) + "].width = " + Str( theWidth, "#" ) + ";" )
		    if theMaxWidth >= 0 then
		      strExec.Append( "cols[" + Str( theCol, "#" ) + "].maxWidth = " + Str( theWidth, "#" ) + ";" )
		    end if
		    strExec.Append( "window.grid" + me.ControlID + ".setColumns(cols);" )
		    
		    'UpdateColumnWidth( Column( theCol ).id, theWidth, True )
		    
		    if LockUpdate then
		      LockUpdateBuffer.Append( Join( strExec, "" ) )
		    else
		      Buffer( strExec )
		      'UpdateDisplay()
		    end if
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateColumnWidth(theColID as String, theWidth as Integer)
		  for intCycle as Integer = 0 to me.ColumnCount - 1
		    dim currHeader as GraffitiWebGridColumn = me.Headers(intCycle)
		    if currHeader.ID = theColID then
		      UpdateColumnWidth( intCycle, theWidth )
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateColumnWidth(theCol as String, theWidth as Integer, Internal as Boolean)
		  #Pragma Unused Internal
		  
		  dim intMax as Integer = UBound( me.Headers )
		  for intCycle as Integer = 0 to intMax
		    dim currCol as GraffitiWebGridColumn = me.Headers(intCycle)
		    if currCol.ID = theCol then 
		      if currCol.Width <> theWidth then
		        currCol.Width = theWidth
		        ColumnResized( currCol )
		      end if
		      exit
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateDisplay(InvalidateAllRows as Boolean = False)
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').width('" + Str( me.Width ) + "px').height('" + Str( me.Height ) + "px');" )
		  'strExec.Append( "window.GSjQuery('#" + me.ControlID + "').top('" + Str( me.Top ) + "px').left('" + Str( me.Left ) + "px');" )
		  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		  strExec.Append( "if( typeof( grid ) != 'undefined' ) {" )
		  strExec.Append( "grid" + me.ControlID + ".resizeCanvas();" )
		  if InvalidateAllRows then
		    strExec.Append( "grid.resizeCanvas();" )
		    strExec.Append( "grid.invalidateAllRows();" )
		    strExec.Append( "grid.render();" )
		  end if
		  strExec.Append( "grid.invalidate();" )
		  strExec.Append( "}" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateHeader(theHeader as GraffitiWebGridColumn)
		  if not IsNull( theHeader ) then
		    dim strExec() as String
		    strExec.Append( "window.grid" + me.ControlID + ".updateColumnHeader('" + theHeader.ID + "', '" + EscapeString( theHeader.Title ) + "', '" + EscapeString( theHeader.ToolTip ) + "');" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions(UpdateOnly as Boolean = True)
		  if hasLoaded then
		    dim strOptions as String = OptionsToString()
		    
		    dim strSelType as String
		    if mSelectionType = SelectionTypes.Cell then
		      strSelType = "window.grid" + me.controlID + ".setSelectionModel(new Slick.CellSelectionModel());"
		    Else
		      strSelType = "window.grid" + me.controlID + ".setSelectionModel(new Slick.RowSelectionModel());"
		    end if
		    
		    if UpdateOnly and hasLoaded and not mLockUpdate Then
		      me.ExecuteJavaScript( strOptions + "window.grid" + me.controlID + ".setOptions( options );" + _
		      strSelType + _
		      "window.grid" + me.ControlID + ".setColumns(window.grid" + me.ControlID + ".getColumns());" + _
		      "window.grid" + me.ControlID + ".invalidate();" + _
		      "window.grid" + me.ControlID + ".render();" )
		      me.ExecuteJavaScript( FooterToString() )
		      Return
		    elseif UpdateOnly and hasLoaded then
		      Return
		    end if
		    
		    dim strExec() as String
		    
		    strExec.Append( "var theSpinner = window.GSjQuery('#" + me.ControlID + "_loader');" )
		    strExec.Append( "componentHandler.upgradeElement(theSpinner[0]);" )
		    
		    strExec.Append( HeadersToString() )
		    
		    strExec.Append( "var data = [];" )
		    strExec.Append( "try {" )
		    strExec.Append( "data = window.grid" + me.ControlID + ".getData();" )
		    strExec.Append( "} catch (e) {" )
		    strExec.Append( "}" )
		    
		    strExec.Append( strOptions )
		    
		    strExec.Append( "window.grid" + me.ControlID + " = new Slick.Grid('#" + me.ControlID + "', data, columns, options);" )
		    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		    strExec.Append( strSelType )
		    
		    strExec.Append( ReplaceAll( jsToggleClick, "#CONTROLID#", me.ControlID ) )
		    
		    '// Row drag reordering
		    strExec.Append( jsDragReorder.ReplaceAll( "#CONTROLID#", me.ControlID ) )
		    
		    strExec.Append( jsSorters )
		    strExec.Append( ReplaceAll( jsSortersSub, "#CONTROLID#", me.ControlID ) )
		    
		    strExec.Append( "var renderedRange = grid.getRenderedRange();" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onViewportChanged', [renderedRange.top,renderedRange.bottom]);" )
		    
		    strExec.Append( "grid.onContextMenu.subscribe(function(e){" )
		    strExec.Append( "e.preventDefault();" )
		    strExec.Append( "var theItem = grid.getCellFromEvent(e);" )
		    strExec.Append( "var row = grid.getDataItem(theItem.row);" )
		    strExec.Append( "var cell = theItem.cell;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onContextMenu', [row.id,cell,e.pageX,e.pageY]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( FooterToString() )
		    
		    strExec.Append( "grid.onActiveCellChanged.subscribe(function(e,args){" )
		    strExec.Append( "try {" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var cell = args.cell;" )
		    strExec.Append( "var theItem = grid.getDataItem(args.row);" )
		    strExec.Append( "var theCell = grid.getColumns()[cell].field;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onActiveCellChange', [theItem.id,theCell]);" )
		    strExec.Append( "} catch (e) {}" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onSelectedRowsChanged.subscribe(function(e,args){" )
		    strExec.Append( "try {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onSelectedRowsChanged', args[""rows""]);" )
		    strExec.Append( "} catch (e) { }" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onViewportChanged.subscribe(function(e,args){" )
		    strExec.Append( "var renderedRange = grid.getRenderedRange();" )
		    strExec.Append( "var currOpts = args.grid.getOptions();" )
		    strExec.Append( "if (currOpts.lastRenderedTop !== renderedRange.top || currOpts.lastRenderedBottom !== renderedRange.bottom) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onViewportChanged', [renderedRange.top,renderedRange.bottom]);" )
		    strExec.Append( "currOpts.lastRenderedTop = renderedRange.top;" )
		    strExec.Append( "currOpts.lastRenderedBottom = renderedRange.bottom;" )
		    strExec.Append( "}" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onScroll.subscribe(function(e,args){" )
		    strExec.Append( "var newScrollX = args['scrollLeft'];" )
		    strExec.Append( "var newScrollY = args['scrollTop'];" )
		    strExec.Append( "var currOpts = args.grid.getOptions();" )
		    strExec.Append( "if (currOpts.lastScrollX !== newScrollX || currOpts.lastScrollY !== newScrollY) {" )
		    strExec.Append( "if (currOpts.scrollTimer) { clearTimeout(currOpts.scrollTimer); }" )
		    strExec.Append( "var renderedRange = grid.getRenderedRange();" )
		    strExec.Append( "currOpts.lastScrollX = newScrollX;" )
		    strExec.Append( "currOpts.lastScrollY = newScrollY;" )
		    strExec.Append( "currOpts.scrollTimer = setTimeout(function() {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onScroll', [newScrollX,newScrollY,renderedRange.top,renderedRange.bottom]);" )
		    strExec.Append( "}, 100);" )
		    strExec.Append( "}" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onKeyDown.subscribe(function(e,args){" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var currOpts = grid.getOptions();" )
		    strExec.Append( "var theItem = grid.getDataItem(args.row);" )
		    strExec.Append( "var theCell = grid.getColumns()[args.cell].field;" )
		    strExec.Append( "currOpts.lastKeyCode = e.keyCode;" )
		    strExec.Append( "currOpts.lastKeyRow = theItem.id;" )
		    strExec.Append( "currOpts.lastKeyCell = theCell;" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "GSjQuery('#" + me.ControlID + "').keyup(function(e) {" )
		    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
		    strExec.Append( "var currOpts = grid.getOptions();" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onKeyPress', [currOpts.lastKeyRow, currOpts.lastKeyCell, currOpts.lastKeyCode, e.altKey, e.ctrlKey, e.shiftKey]);" )
		    strExec.Append( "currOpts.lastKeyCode = -1;" )
		    strExec.Append( "currOpts.lastKeyRow = '';" )
		    strExec.Append( "currOpts.lastKeyCell = '';" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onClick.subscribe(function(e,args){" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var cell = grid.getCellFromEvent(e);" )
		    strExec.Append( "var theItem = grid.getDataItem(cell.row);" )
		    strExec.Append( "var theCell = grid.getColumns()[cell.cell].field;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onClick', [theItem.id,theCell, e.clientX, e.clientY]);" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onClick.subscribe(function (e, args) {" )
		    strExec.Append( "if (window.GSjQuery(e.target).is(':checkbox') && options['editable'])" )
		    strExec.Append( "{" )
		    strExec.Append( "var column = args.grid.getColumns()[args.cell];" )
		    strExec.Append( "if (column['editable'] == false || column['autoEdit'] == false)" )
		    strExec.Append( "return;" )
		    strExec.Append( "data[args.row][column.field] = !data[args.row][column.field];" )
		    'strExec.Append( "window.GSjQuery(e.target).blur();" )
		    strExec.Append( "}" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onDblClick.subscribe(function(e,args){" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var cell = grid.getCellFromEvent(e);" )
		    strExec.Append( "var theItem = grid.getDataItem(cell.row);" )
		    strExec.Append( "var theCell = grid.getColumns()[cell.cell].field;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onDblClick', [theItem.id,theCell]);" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onRenderCompleted.subscribe(function renderComplete" + me.ControlID + "(e,args){" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onRenderCompleted', []);" )
		    strExec.Append( "grid.onRenderCompleted.unsubscribe(renderComplete" + me.ControlID + ");" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onRenderCompleted.subscribe(function(e, args) {" )
		    strExec.Append( "var currOpts = args.grid.getOptions();" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .slick-row.even').each(function(){" )
		    strExec.Append( "if(!GSjQuery(this).hasClass('customRowStyle')) {" )
		    strExec.Append( "GSjQuery(this).addClass(currOpts.evenRowClass);" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .slick-row.odd').each(function(){" )
		    strExec.Append( "if(!GSjQuery(this).hasClass('customRowStyle')) {" )
		    strExec.Append( "GSjQuery(this).addClass(currOpts.oddRowClass);" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .slick-cell').addClass(currOpts.cellBorderClass);" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .slick-header-column').addClass(currOpts.headerClass);" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " .slick-header-columns').addClass(currOpts.headerClass);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onColumnsResized.subscribe(function(e,args){" )
		    strExec.Append( "var columns = args.grid.getColumns();" )
		    strExec.Append( "var columnsWidth = [];" )
		    strExec.Append( "for (i=0;i<columns.length;i++) {" )
		    strExec.Append( "columnsWidth.push({'column':columns[i].id,'width':columns[i].width});" )
		    strExec.Append( "}" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onColumnsResized', [JSON.stringify(columnsWidth)]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onColumnsReordered.subscribe(function(e,args) {" )
		    strExec.Append( "var orderedCols = [];" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var columns = args.grid.getColumns();" )
		    strExec.Append( "for (var i =0; i < columns.length; i++) {" )
		    strExec.Append( "orderedCols[i] = columns[i].id;" )
		    strExec.Append( "}" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onColumnsReordered', [orderedCols]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onBeforeEditCell.subscribe(function(e,args) {" )
		    strExec.Append( "if (args.item.disabled == true) {" )
		    strExec.Append( "return false;" )
		    strExec.Append( "}" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var theCell = grid.getColumns()[args.cell].field;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onBeforeEditCell', [args.item.id, theCell]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onBeforeCellEditorDestroy.subscribe(function(e,args) {" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var currentlyActive = grid.getActiveCell();" )
		    strExec.Append( "var theItem = grid.getDataItem(currentlyActive.row);" )
		    strExec.Append( "var theCell = grid.getColumns()[currentlyActive.cell];" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onBeforeCellEditorDestroy', [theItem.id, theCell.field]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "grid.onCellChange.subscribe(function(e,args){" )
		    strExec.Append( "var grid = args.grid;" )
		    strExec.Append( "var cell = args.cell;" )
		    strExec.Append( "var theField = grid.getColumns()[cell];" )
		    strExec.Append( "var theItem = grid.getDataItem(args.row);" )
		    strExec.Append( "var theCell = grid.getColumns()[cell].field;" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onCellChange', [theItem.id,theCell, JSON.stringify(args[""item""])]);" )
		    strExec.Append( "return false;" )
		    strExec.Append( "});" )
		    
		    'strExec.Append( "window.GSjQuery('#" + me.ControlID + " .slick-viewport').on('blur', 'input.editor-text, input.editor-percentcomplete, i', function (e) {" )
		    'strExec.Append( "setTimeout( function() {" )
		    'strExec.Append( "Slick.GlobalEditorLock.commitCurrentEdit();" )
		    'strExec.Append( "}, 0);" )
		    'strExec.Append( "});" )
		    
		    strExec.Append( "grid.registerPlugin(new Slick.AutoTooltips());" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		    
		    'UpdateStyles()
		    
		    HeaderRowVisible = mHeaderRowVisible
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateProperty(item as Variant, identifier as String)
		  // Part of the GraffitiUpdateInterface interface.
		  
		  if item isa GraffitiWebGridRow then
		    UpdateProperty_Row( item, identifier )
		  elseif item isa GraffitiWebGridColumn then
		    UpdateProperty_Column( item, identifier )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdatePropertyMultiple(identifier as String, item as Variant, modifier as Variant)
		  if item isa GraffitiWebGridRow then
		    UpdateProperty_Row( item, identifier, modifier )
		  elseif item isa GraffitiWebGridColumn then
		    UpdateProperty_Column( item, identifier )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateProperty_Column(Column as GraffitiWebGridColumn, identifier as String)
		  select case identifier
		  case "visible"
		    if Column.Visible then
		      ShowColumn( Column )
		    else
		      HideColumn( Column )
		    end if
		  case "resizeable"
		    SetColumnProperty( Column.ID, "resizable", Str( Column.Resizeable ).Lowercase )
		  case "focusable"
		    SetColumnProperty( column.ID, "focusable", Str( Column.Focusable ).Lowercase )
		  case "selectable"
		    SetColumnProperty( Column.ID, "selectable", Str( Column.Selectable ).Lowercase )
		  case "sortable"
		    SetColumnProperty( Column.ID, "sortable", Str( Column.Sortable ).Lowercase )
		  case "title"
		    SetColumnProperty( Column.ID, "name", "'" + EscapeString( Column.Title ) + "'" )
		  case "width"
		    'UpdateColumnWidth( ColumnIndex( Column ), Column.Width, Column.MaxWidth )
		    SetColumnProperty( Column.ID, "width", Str( Column.Width ) )
		  case "maxWidth"
		    SetColumnProperty( Column.ID, "maxWidth", Str( Column.MaxWidth ) )
		  case "minWidth"
		    SetColumnProperty( Column.ID, "minWidth", Str( Column.MinWidth ) )
		  case "tooltip"
		    SetColumnProperty( Column.ID, "tooltip", "'" + EscapeString( Column.ToolTip ) + "'" )
		  case "headerstyle"
		    dim headerClasses() as String
		    if not IsNull( StyleHeader ) then
		      if not StyleHeader.isdefaultstyle then headerClasses.Append( StyleHeader.Name )
		    end if
		    if not IsNull( Column.HeaderStyle ) then
		      if not Column.HeaderStyle.isdefaultstyle then headerClasses.Append( Column.HeaderStyle.Name )
		    end if
		    SetColumnProperty( Column.ID, "headerCssClass", "'" + Join( headerClasses, " " ) + "'" )
		  case "style"
		    if not IsNull( Column.Style ) then
		      if not Column.Style.isdefaultstyle then SetColumnProperty( Column.ID, "cssClass", Column.Style.Name )
		    end if
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateProperty_Row(Row as GraffitiWebGridRow, identifier as String, Column as GraffitiWebGridColumn = Nil)
		  select case identifier
		  case "style"
		    SetRowStyle( RowIndex( Row ), row.Style )
		  case "enabled"
		    RowEnabled( Row ) = row.Enabled
		  case "selected"
		    SelectRow( Row )
		  case "colspan"
		    ColSpan( Row, row.ColSpanField, row.ColSpanLength )
		  case "expanded"
		    SetRowExpanded( Row )
		  case "cellchange"
		    UpdateCell( Row, Column )
		  end select
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateRows()
		  CopyArray( SortBuffer, Rows )
		  
		  RemoveAllCellStyles()
		  
		  dim intMax as Integer = Rows.Ubound
		  for intCycle as Integer = 0 to intMax
		    dim Row as GraffitiWebGridRow = Rows(intCycle)
		    dim theRow as Dictionary = Row.Cells
		    dim theStyles() as Dictionary = GetRowCSSStyles( theRow )
		    
		    dim intStyleMax as Integer = theStyles.Ubound
		    for intStyle as Integer = 0 to intStyleMax
		      dim currDic as Dictionary = theStyles(intStyle)
		      dim theStyle as WebStyle = currDic.Value( "Style" )
		      
		      dim theCol as GraffitiWebGridColumn = Column( currDic.Value( "Col" ).StringValue )
		      
		      AddCellStyle( Row, theCol, theStyle )
		    next
		  next
		  
		  'Buffer( "grid.invalidateAllRows();" )
		  'UpdateStyles()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateRowValues(theRow as GraffitiWebGridRow, j as JSONItem, strCell as String)
		  if IsNull( theRow ) or IsNull( j ) then Return
		  
		  dim currHeader as GraffitiWebGridColumn = Column( strCell )
		  dim newValue as String = j.Value( strCell ).StringValue
		  
		  select case currHeader.Editor
		  case EditTypes.Checkbox, EditTypes.YesNoSelect
		    theRow.Cells.Value( currHeader.ID ) = CBool( newValue )
		  case EditTypes.Date
		    dim strDate as String = NthField( newValue, "T", 1 )
		    dim intYear as Integer = NthField( strDate, "-", 1 ).Val
		    dim intMonth as Integer = NthField( strDate, "-", 2 ).Val
		    dim intDay as Integer = NthField( strDate, "-", 3 ).Val
		    theRow.Cells.Value( currHeader.ID ) = new Date( intYear, intMonth, intDay )
		  case EditTypes.Integer, EditTypes.Percent
		    dim intValue as Integer = newValue.Val
		    theRow.Cells.Value( currHeader.ID ) = intValue
		  case EditTypes.Currency
		    dim intValue as Integer = newValue.Val * 100
		    dim dblValue as Double = intValue / 100
		    theRow.cells.Value( currHeader.ID ) = dblValue
		  case else
		    theRow.Cells.Value( currHeader.ID ) = newValue
		  end select
		  
		  CellChange( theRow, currHeader, theRow.Cell( currHeader ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateStyles()
		  'StyleCellBorder = StyleCellBorder
		  'StyleRowEven = StyleRowEven
		  'StyleRowOdd = StyleRowOdd
		  'StyleRowSelected = StyleRowSelected
		  'StyleHeader = StyleHeader
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event CellChange(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn, NewValue as Variant)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event CellClick(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn, pageX as Integer, pageY as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event CellDoubleClick(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ColumnResized(Column as GraffitiWebGridColumn)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ColumnsReordered()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ContextClick(Row as GraffitiWebGridRow, Cell as GraffitiWebGridColumn, X as Integer, Y as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event EditBegin(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event EditEnd(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event KeyPress(Row as GraffitiWebGridRow, Column as GraffitiWebGridColumn, KeyCode as Integer, AltKey as Boolean, CtrlKey as Boolean, ShiftKey as Boolean)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event RowCollapsed(Row as GraffitiWebGridRow)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event RowExpanded(Row as GraffitiWebGridRow)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event RowReordered(Row as GraffitiWebGridRow, oldIndex as Integer, newIndex as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ScrollPositionChanged(X as Integer, Y as Integer)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionChanged()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Sorted(ByColumn as GraffitiWebGridColumn, Ascending as Boolean)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/mleibman/SlickGrid/wiki
		
		Licensed under MIT
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAutoEdit
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAutoEdit = value
			  UpdateOptions()
			  'UpdateStyles()
			End Set
		#tag EndSetter
		AutoEdit As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private cellCSS() As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private CellStyleKey As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private CellStyles() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private CSSIdentifiers() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mEditable
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEditable = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Editable As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mEditing
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  'mEditing = value
			End Set
		#tag EndSetter
		Editing As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mEnableColumnReorder
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEnableColumnReorder = value
			  
			  UpdateOptions( False )
			End Set
		#tag EndSetter
		EnableColumnReorder As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mEnableRowReorder
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEnableRowReorder = value
			  
			  UpdateOptions( False )
			End Set
		#tag EndSetter
		EnableRowReorder As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mFooterRow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFooterRow = value
			  
			  Buffer( FooterToString() )
			End Set
		#tag EndSetter
		Private FooterRow As Dictionary
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mForceFitColumns
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mForceFitColumns = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		ForceFitColumns As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHeaderRowHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeaderRowHeight = value
			  
			  if hasLoaded and mHeaderRowVisible then
			    dim strExec() as String
			    strExec.Append( "var jqGrid = window.GSjQuery('#" + me.ControlID + "');" )
			    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
			    
			    strExec.Append( "if( typeof(jqGrid) !== 'undefined' ) {" )
			    strExec.Append( "jqGrid.find('.slick-header').css('height','" + Str( mHeaderRowHeight ) + "px');" )
			    strExec.Append( "grid.resizeCanvas();" )
			    strExec.Append( "}" )
			    Buffer( strExec )
			  end if
			End Set
		#tag EndSetter
		HeaderRowHeight As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHeaderRowVisible
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHeaderRowVisible = value
			  
			  if hasLoaded then
			    dim strExec() as String
			    strExec.Append( "var jqGrid = window.GSjQuery('#" + me.ControlID + "');" )
			    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
			    strExec.Append( "if( typeof(jqGrid) !== 'undefined' ) {" )
			    if not mHeaderRowVisible then
			      strExec.Append( "jqGrid.find('.slick-header').css('height','0px');" )
			    else
			      strExec.Append( "jqGrid.find('.slick-header').css('height','" + Str( mHeaderRowHeight ) + "px');" )
			    end if
			    
			    strExec.Append( "grid.resizeCanvas();" )
			    strExec.Append( "}" )
			    Buffer( strExec )
			  end if
			End Set
		#tag EndSetter
		HeaderRowVisible As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Headers() As GraffitiWebGridColumn
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile() As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastScrollX As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastScrollY As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions( False )
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mListIndex
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		ListIndex As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLockUpdate = value
			  
			  if mLockUpdate = False then
			    dim strExec() as String
			    strExec.Append( HeadersToString )
			    strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
			    strExec.Append( "grid.setColumns(columns);" )
			    
			    strExec.Append( "window.grid" + me.controlID + ".updateRowCount();" )
			    strExec.Append( "window.grid" + me.controlID + ".render();" )
			    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'onUnlockUpdate');" )
			    strExec.Append( Join( CellStyles, "" ) )
			    strExec.Append( Join( LockUpdateBuffer, "" ) )
			    Buffer( strExec )
			    ReDim CellStyles(-1)
			  else
			    redim CellStyles(-1)
			  end if
			End Set
		#tag EndSetter
		LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private LockUpdateBuffer() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAutoEdit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEditable As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEditing As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnableColumnReorder As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnableRowReorder As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFooterRow As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mForceFitColumns As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeaderRowHeight As Integer = 25
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHeaderRowVisible As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mListIndex As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mmHeaderRowVisible As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMultiSelectRows As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRenderedBottomRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRenderedTopRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRowHeight As Integer = 25
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectedCell As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectionType As SelectionTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleCellBorder As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleRowEven As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleRowOdd As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleRowSelected As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleTextEditor As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMultiSelectRows
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMultiSelectRows = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		MultiSelectRows As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private prevColWidths As Dictionary
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRenderedBottomRow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		RenderedBottomRow As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRenderedTopRow
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		RenderedTopRow As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRowHeight
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRowHeight = value
			  
			  dim strExec() as String
			  strExec.Append( "var grid = window.grid" + me.ControlID + ";" )
			  strExec.Append( "grid.setOptions( { rowHeight: '" + Str( value ) + "' } );" )
			  strExec.Append( "var cols = window.grid" + me.controlID + ".getColumns();" )
			  strExec.Append( "window.grid" + me.ControlID + ".setColumns(cols);" )
			  Buffer( strExec )
			  
			End Set
		#tag EndSetter
		RowHeight As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Rows() As GraffitiWebGridRow
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mSelectedCell
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  //mSelectedCell = value
			End Set
		#tag EndSetter
		SelectedCell As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		SelectedRows() As GraffitiWebGridRow
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelectionType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSelectionType = value
			  
			  dim strSelType as String
			  if SelectionType = SelectionTypes.Cell then
			    strSelType = "window.grid" + me.controlID + ".setSelectionModel(new Slick.CellSelectionModel());"
			  Else
			    strSelType = "window.grid" + me.controlID + ".setSelectionModel(new Slick.RowSelectionModel());"
			  end if
			  Buffer( strSelType )
			End Set
		#tag EndSetter
		SelectionType As SelectionTypes
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private SortBuffer() As GraffitiWebGridRow
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleCellBorder
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'if not IsNull( mStyleCellBorder ) then
			  'If not mStyleCellBorder.IsDefaultStyle then RemoveClass( "slick-cell", mStyleCellBorder )
			  'end if
			  
			  mStyleCellBorder = value
			  
			  'if not IsNull( mStyleCellBorder ) then
			  'If not mStyleCellBorder.IsDefaultStyle then AddClass( "slick-cell", mStyleCellBorder )
			  'end if
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleCellBorder As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'if not IsNull( mStyleHeader ) then
			  'If not mStyleHeader.IsDefaultStyle then
			  'RemoveClass( "slick-header-column", mStyleHeader )
			  'RemoveClass( "slick-header-columns", "slick-header-columns-default" )
			  'end if
			  'end if
			  
			  StyleHeaderOld = mStyleHeader
			  
			  mStyleHeader = value
			  
			  'if not IsNull( mStyleHeader ) then
			  'If not mStyleHeader.IsDefaultStyle then
			  'AddClass( "slick-header-column", mStyleHeader )
			  'RemoveClass( "slick-header-columns", "slick-header-columns-default" )
			  'AddClass( "slick-header-columns", mStyleHeader )
			  'end if
			  'else
			  'AddClass( "slick-header-columns", "slick-header-columns-default" )
			  'end if
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleHeader As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private StyleHeaderOld As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleRowEven
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'if not IsNull( mStyleRowEven ) then
			  'If not mStyleRowEven.IsDefaultStyle then RemoveClass( "slick-row.even", mStyleRowEven )
			  'end if
			  '
			  mStyleRowEven = value
			  '
			  'if not IsNull( mStyleRowEven ) then
			  'If not mStyleRowEven.IsDefaultStyle then AddClass( "slick-row.even", mStyleRowEven )
			  'end if
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleRowEven As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleRowOdd
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'if not IsNull( mStyleRowOdd ) then
			  'If not mStyleRowOdd.IsDefaultStyle then RemoveClass( "slick-row.odd", mStyleRowOdd )
			  'end if
			  
			  mStyleRowOdd = value
			  
			  'if not IsNull( mStyleRowOdd ) then
			  'If not mStyleRowOdd.IsDefaultStyle then AddClass( "slick-row.odd", mStyleRowOdd )
			  'end if
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleRowOdd As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleRowSelected
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleRowSelected = value
			  
			  if not IsNull( mStyleRowSelected ) then
			    If not mStyleRowSelected.IsDefaultStyle then
			      dim strExec() as String
			      strExec.Append( "window.grid" + me.controlID + ".setOptions( { selectedCellCssClass: '" + value.Name + "' } );" )
			      Buffer( strExec )
			    end if
			  end if
			End Set
		#tag EndSetter
		StyleRowSelected As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mStyleTextEditor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleTextEditor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		StyleTextEditor As WebStyle
	#tag EndComputedProperty


	#tag Constant, Name = cssColPick, Type = String, Dynamic = False, Default = \".slick-columnpicker {\r  border: 1px solid #718BB7;\r  background: #f0f0f0;\r  padding: 6px;\r  -moz-box-shadow: 2px 2px 2px silver;\r  -webkit-box-shadow: 2px 2px 2px silver;\r  box-shadow: 2px 2px 2px silver;\r  min-width: 100px;\r  cursor: default;\r}\r\r.slick-columnpicker li {\r  list-style: none;\r  margin: 0;\r  padding: 0;\r  background: none;\r}\r\r.slick-columnpicker input {\r  margin: 4px;\r}\r\r.slick-columnpicker li a {\r  display: block;\r  padding: 4px;\r  font-weight: bold;\r}\r\r.slick-columnpicker li a:hover {\r  background: white;\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.slickgrid", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsDragReorder, Type = String, Dynamic = False, Default = \"  var moveRowsPlugin \x3D new Slick.RowMoveManager({\r    cancelEditOnDrag: true\r  });\r\r  moveRowsPlugin.onBeforeMoveRows.subscribe(function (e\x2C data) {\r    for (var i \x3D 0; i < data.rows.length; i++) {\r      // no point in moving before or after itself\r      if (data.rows[i] \x3D\x3D data.insertBefore || data.rows[i] \x3D\x3D data.insertBefore - 1) {\r        e.stopPropagation();\r        return false;\r      }\r\r      // Honor option\r      var opts \x3D grid.getOptions();\r      if (opts.enableRowReorder \x3D\x3D false) {\r        e.stopPropagation();\r        return false;\r      }\r    }\r    return true;\r  });\r\r  moveRowsPlugin.onMoveRows.subscribe(function (e\x2C args) {\r    var data \x3D grid.getData();\r    var extractedRows \x3D []\x2C left\x2C right;\r    var rows \x3D args.rows;\r    var insertBefore \x3D args.insertBefore;\r    left \x3D data.slice(0\x2C insertBefore);\r    right \x3D data.slice(insertBefore\x2C data.length);\r\r    rows.sort(function(a\x2Cb) { return a-b; });\r\r    for (var i \x3D 0; i < rows.length; i++) {\r      extractedRows.push(data[rows[i]]);\r    }\r\r    rows.reverse();\r\r    for (var i \x3D 0; i < rows.length; i++) {\r      var row \x3D rows[i];\r      if (row < insertBefore) {\r        left.splice(row\x2C 1);\r      } else {\r        right.splice(row - insertBefore\x2C 1);\r      }\r    }\r\r    data \x3D left.concat(extractedRows.concat(right));\r\r    var selectedRows \x3D [];\r    for (var i \x3D 0; i < rows.length; i++)\r      selectedRows.push(left.length + i);\r\r    grid.resetActiveCell();\r    grid.setData(data);\r    grid.setSelectedRows(selectedRows);\r    grid.render();\r    var rowsMoved \x3D [];\r    rows.forEach(function(item) {\r      rowsMoved.push( grid.getDataItem(item).id );\r    });\r    Xojo.triggerServerEvent(\'#CONTROLID#\'\x2C \'rowDrag\'\x2C [rowsMoved\x2C insertBefore]);\r  });\r\r  grid.registerPlugin(moveRowsPlugin);", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsItemMetaData, Type = String, Dynamic = False, Default = \"dd.getItemMetadata \x3D function(row) {\r    var thisRow \x3D this[row];\r    var thisSpan \x3D thisRow._colSpan_field;\r    var thisSpanLen \x3D thisRow._colSpan_len;\r    if (thisRow._colSpan_field && thisRow._colSpan_len) {\r        return {\r            \'columns\': {\r                [thisSpan]: {\r                    \'colspan\': thisSpanLen\r                }\r            }\r        };\r    }\r};", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsSorters, Type = String, Dynamic = False, Default = \"if (!window.GSgridSorterDate) {\r    window.GSgridSorterDate \x3D function(a\x2C b) {\r        var date_a \x3D new Date(a[sortcol]);\r        var date_b \x3D new Date(b[sortcol]);\r        var diff \x3D date_a.getTime() - date_b.getTime();\r        return sortdir * (diff \x3D\x3D\x3D 0 \? 0 : (date_a > date_b \? 1 : -1));\r    }\r\r    window.GSgridSorterString \x3D function(a\x2C b) {\r        var x \x3D a[sortcol]\x2C\r            y \x3D b[sortcol];\r        return sortdir * (x \x3D\x3D\x3D y \? 0 : (x > y \? 1 : -1));\r    }\r\r    window.GSgridSorterInteger \x3D function(a\x2C b) {\r        var x \x3D (isNaN(a[sortcol]) || a[sortcol] \x3D\x3D\x3D \'\' || a[sortcol] \x3D\x3D\x3D null) \? -99e+10 : parseFloat(a[sortcol]);\r        var y \x3D (isNaN(b[sortcol]) || b[sortcol] \x3D\x3D\x3D \'\' || b[sortcol] \x3D\x3D\x3D null) \? -99e+10 : parseFloat(b[sortcol]);\r        return sortdir * (x \x3D\x3D\x3D y \? 0 : (x > y \? 1 : -1));\r    }\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsSortersSub, Type = String, Dynamic = False, Default = \"window.grid#CONTROLID#.onSort.subscribe(function(e\x2C args) {\r    var cols \x3D args.sortCols;\r    var dataView \x3D args.grid.getData();\r    dataView.sort(function(dataRow1\x2C dataRow2) {\r        for (var i \x3D 0\x2C l \x3D cols.length; i < l; i++) {\r            sortdir \x3D cols[i].sortAsc \? 1 : -1;\r            sortcol \x3D cols[i].sortCol.field;\r            var result \x3D cols[i].sortCol.sorter(dataRow1\x2C dataRow2);\r            if (result !\x3D 0) {\r                return result;\r            }\r        }\r        return 0;\r    });\r    args.grid.invalidateAllRows();\r    args.grid.render();\r\r  var sortedData \x3D [];\r  var newData \x3D dataView;\r  for (i \x3D 0; i < newData.length; i++) {\r  \tsortedData[i] \x3D newData[i].id;\r  }\r  Xojo.triggerServerEvent(\'#CONTROLID#\'\x2C \'sortData\'\x2C sortedData);\r  Xojo.triggerServerEvent(\'#CONTROLID#\'\x2C \'onSort\'\x2C [args[\"sortCols\"][0][\"sortCol\"][\"id\"]\x2Cargs[\"sortCols\"][0][\"sortAsc\"]]);\r  \r});\r", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsSorterSub_07102018, Type = String, Dynamic = False, Default = \"function depthFirstTreeSort(arr\x2C cmp) {\r    // Returns an object\x2C where each key is a node number\x2C and its value\r    // is an array of child nodes.\r    function makeTree(arr) {\r        var tree \x3D {};\r        for (var i \x3D 0; i < arr.length; i++) {\r            if (!tree[arr[i]._parent]) tree[arr[i]._parent] \x3D [];\r            tree[arr[i]._parent].push(arr[i]);\r        }\r        return tree;\r    }\r\r    // For each node in the tree\x2C starting at the given id and proceeding\r    // depth-first (pre-order)\x2C sort the child nodes based on cmp\x2C and\r    // call the callback with each child node.\r    function depthFirstTraversal(tree\x2C id\x2C cmp\x2C callback) {\r        var children \x3D tree[id];\r        if (children) {\r            children.sort(cmp);\r            for (var i \x3D 0; i < children.length; i++) {\r                callback(children[i]);\r                depthFirstTraversal(tree\x2C children[i].id\x2C cmp\x2C callback);\r            }\r        }\r    }\r\r    // Overwrite arr with the reordered result\r    var i \x3D 0;\r    depthFirstTraversal(makeTree(arr)\x2C 0\x2C cmp\x2C function(node) {\r        arr[i++] \x3D node;\r    });\r}\r\rfunction nameCmp(a\x2C b) { return a.name.localeCompare(b.name); }\rdepthFirstTreeSort(data\x2C nameCmp);  // ascending sort\r//depthFirstTreeSort(data\x2C function(a\x2C b) { return nameCmp(b\x2C a); });  // descending sort\r\r$(\'#results\').html(JSON.stringify(data\x2Cnull\x2C2));", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsToggleClick, Type = String, Dynamic = False, Default = \"grid.onClick.subscribe(function (e\x2C args) {\r    if (GSjQuery(e.target).hasClass(\"toggle\")) {\r\r      var item \x3D grid.getDataItem(args.row);\r      if (item) {\r        if (!item._collapsed) {\r          item._collapsed \x3D true;\r        } else {\r          item._collapsed \x3D false;\r        }\r\r        Xojo.triggerServerEvent(\'#CONTROLID#\'\x2C \'toggleClick\'\x2C [item.id\x2C item._collapsed]);\r      }\r      e.stopImmediatePropagation();\r    }\r  });", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAV8SURBVHhe7d3La1xVHAfw87vTlHnUFgRfqBvxtXFTW4tgoWgEsTZzC61F7MaFitHSeSiC/4B2MQ+pRnQhgoiPFDKTtripVuimai2IqxQXUmgQXVkzM8E09+eZ5LcwufdOnJM7N5N7vh9I7/mdycwk53ybnHNnJqMAAAAAAAAAAAAAAAAAAAAAAAAgKUiOkcodLN+6QAs7pIQIjPDIX62pyh9SRiayAGTyxXF9a2PENKqPKemGKLFaZOJz+jjdadYmpHdd1h2AdL6wl5RTJ1I7pQtiwKwus/IK8836Bekysq4AZN3yS/pL+VBK2BD0crtR+UiKvhkHIOMWHyFF30sJG4gV7+k0aj9I2RdHjiZOyBE2nvFcGAUg7Zaf1f/790kJG6w7F905kbIvRgEg9p6WJgwJ0zkxC4BSj0kThoTpnJiuAW6XIwwPozkxCwBRTlowLAznxPQnACQEAmA5BMByCIDlEADLIQCWQwAshwBYDgGwHAJgOQTAckbPCMq6JZZmIH3hjGL+VUqIAtG9erIekCpQu1Htez4jDwAr/qTTqL0gJUQo45Y+1hMWOrYmARjArwDntDQgcnRGGpEZQAAWF6UBkYt+bLEItBwCYDkEwHIIgOUGsA303E6j3pTSJ5MvniFFT0m5RN9Yy3HUeGuq+pl0+eTGSs97xO/r626TrsTRW+ivO83aASl9Mm4hT8ppSOkzJNvAcFm3+CIR7dexS/33g0ht91i9Lp8WyCP1hr7ujtXXTdKH/v6e6Y6RfMuxiPdXAKubpRUkI8dgpNLSSrbeYxQ5rAEshwBYLuYAUPiZLL16lFawtS5PjB5jNACxBsBL0aRe6f4u5Sr8lTTCTMoxsbpj0x0jKWMR+zZw26Fjtyze2LpXymXkzXamahelCpV2jz9KassdUiZOass/F+ZOnfxTSp9BbANjDwCY2/TnAWD4IACWQwAshwBYDgGwXOy7gOzBwk7lOY9LuYRIXWs1qp9LGSrtlp5zWN0pZfI43rftqfplqXw2/TYw65Yf1p9xScpVvIl2o/6qFD7ZfOmk/mpfkzLBaFe7UflJihU2/zaQvVFp+bBynpBmICb1pDSTrccYDQLWAJZDACyHAFgu5gA4s9LwIcVz0gy01uXJET5GgxD/NtAtVfVhxRMfmdVVh723W9P1c9LlkxsrjHrkvKW3jHdLV/IwT7ebtbJUPng00HJ4NBAihwBYDgGwHAJgOQTAcgiA5eI/D5Avvse08g2niFWLFL3Zala+ky6fXL68j8l7hxP84lA9DufbzeoxKX02/XmAnFs8qifwUylXu6S/gd3S9tH3+aM+7FqukotIHQ17lfSmPw/APZ7MoRN1kzQDrXV5UrDHd0kzFlgDWA4BsBwCYLl41wCkzkvTp7sClmYgvboJ3SEkSa8xGoTYt4GZsfIeIn5IyiWeUn/PN6tfShkqnS8d0YlN7GKQmX7pTFdC35EdDwdbDg8HQ+QQAMshAJZDACxnFABmdV2aPvoGQ//SJayXEzq2veakF6NdQCZfnCGi+6VcQW8Prut/viBSeOOICBF7Yx45R/SEbZeulZivtJu1nm8pE8RsG5gvTiiiV6SEYcD8gQ7AuFT/m9kawHHOSguGheGcGAWgPVXRd8ahJyQgbtxYnpP+Ge8CvJGtx/Udx/oyJgjCs8tzYcY4APOTJ66Sp47qBd816YK46bHvzkF3LqSnbyk5GlmYufhb6r7dp4ice/Ry8kHphjiwanqLNw51Tr/7s/QYMdoFBMnkCwd0EPbr1eiovtnb9C0n9smbG4LVnB7T7t9Z/obZO9tp1iPZZkcWAJ/Dh9f10wVWmZzE+zECAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBSSv0LyG/IEqwMfwAAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAADYSURBVDhPYxhwwAilGTgCCsKYGBgtoFy84B/D/xM/NkxYBWLDDeAMKPoI5PBBuXjBfwaGT9839PGD2ExgESAgVjMIIKtFckHh1f8M/xuhXLyAkYGx/vuGfm0QG+GC/4wfgZwPzP8ZZUGY6T9TAVYMVANSC9WGcAGXf9Gxf3//RDAyMYmB+P+ZGJTAEujgz78TTMwsK75t7LMCceEuAAGQZpBGEAbFCDYMswAGUAwgB6AY8P/fv1eM/xjugTAorrFhkBqocjBAiQXkwMEH/jP+54fFwihgYAAAVfVY6L0LxW8AAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = treeFilter, Type = String, Dynamic = False, Default = \"function treeFilter(item) {\r    if (item.parent !\x3D null) {\r        var parent \x3D data[item.parent.id];\r        while (parent) {\r            if (parent._collapsed) {\r                return false;\r            }\r            parent \x3D data[parent.parent \? parent.parent : null];\r        }\r    }\r    return true;\r}", Scope = Private
	#tag EndConstant


	#tag Enum, Name = EditTypes, Type = Integer, Flags = &h0
		None
		  Text
		  LongText
		  Integer
		  YesNoSelect
		  Checkbox
		  Percent
		  Date
		  Password
		  Currency
		Double
	#tag EndEnum

	#tag Enum, Name = FormatTypes, Type = Integer, Flags = &h0
		Text
		  LongText
		  Currency
		  YesNo
		  Checkbox
		  Percent
		  ProgressBar
		  FontAwesome
		  Date
		  Color
		  Picture
		  HTML
		  Password
		  Tree
		  Integer
		Double
	#tag EndEnum

	#tag Enum, Name = SelectionTypes, Type = Integer, Flags = &h0
		Row
		Cell
	#tag EndEnum

	#tag Enum, Name = SortDirections, Type = Integer, Flags = &h21
		Ascending
		  Descending
		None
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoEdit"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Editable"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EnableColumnReorder"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ForceFitColumns"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderRowHeight"
			Visible=true
			Group="Behavior"
			InitialValue="25"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderRowVisible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockUpdate"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MultiSelectRows"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RenderedBottomRow"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RenderedTopRow"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RowHeight"
			Visible=true
			Group="Behavior"
			InitialValue="25"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SelectionType"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="SelectionTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - Row"
				"1 - Cell"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EnableRowReorder"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SelectedCell"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Editing"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
