#tag Class
Protected Class GraffitiWebCalculator
Inherits GraffitiControlWrapper
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "numPress"
		      DigitPressed( Parameters(0).StringValue )
		    case "calcValue"
		      Calculated( Parameters(1).DoubleValue )
		    case "negate"
		      ValueNegated( Parameters(0).DoubleValue )
		    case "backspace"
		      Backspaced( Parameters(0).DoubleValue )
		    case "clear"
		      DisplayCleared()
		    case "opPress"
		      if Parameters(0) = "÷" then Parameters(0) = "/"
		      OperatorPressed( Parameters(0).StringValue, Parameters(1).DoubleValue )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim strOut() as String
		  strOut.Append( "<div id='" + me.ControlID + "' style='width: 198; height: 229;' tabindex='" + Str( me.ZIndex, "#" ) + "'>" )
		  strOut.Append( calcHTML )
		  strOut.Append( "</div>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCalc -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "calculator" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "knockout-min.js" ), "graffitisuite" ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwcalc.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwcalc.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCalc -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  Buffer( "ko.applyBindings(new Calculator('" + me.ControlID + "'), document.getElementById('" + me.ControlID + "'));" )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Backspaced(theDisplayContent as Double)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Calculated(theResult as Double)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event DigitPressed(theDigit as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event DisplayCleared()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event OperatorPressed(theOperator as String, theValue as Double)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueNegated(theNewValue as Double)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		https://github.com/Ideaviate/html5-css3-js-calculator
		
		Copyright (C) 2012 Ideaviate AB
		
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty


	#tag Constant, Name = calcHTML, Type = String, Dynamic = False, Default = \"<div class\x3D\"gswCalculator\">\n            <div class\x3D\"u4 display\">\n                <div class\x3D\"display-inner\"><div class\x3D\"display-text\" data-bind\x3D\"text: display\"></div></div>\n            </div>\n            <div id\x3D\"calculator-button-c\" class\x3D\"u1 button button-red\" data-bind\x3D\"click: clear\">c</div>\n            <div id\x3D\"calculator-button-backspace\" class\x3D\"u1 button button-backspace button-gray\" data-bind\x3D\"click: backspace\">&#x21e4;</div>\n            <div id\x3D\"calculator-button-negate\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: negate\">&#xb1;</div>\n            <div id\x3D\"calculator-button-\xC3\xB7\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: operator\">\xC3\xB7</div>\n            <div id\x3D\"calculator-button-7\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">7</div>\n            <div id\x3D\"calculator-button-8\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">8</div>\n            <div id\x3D\"calculator-button-9\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">9</div>\n            <div id\x3D\"calculator-button-x\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: operator\">x</div>\n            <div id\x3D\"calculator-button-4\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">4</div>\n            <div id\x3D\"calculator-button-5\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">5</div>\n            <div id\x3D\"calculator-button-6\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">6</div>\n            <div id\x3D\"calculator-button--\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: operator\">-</div>\n            <div id\x3D\"calculator-button-1\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">1</div>\n            <div id\x3D\"calculator-button-2\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">2</div>\n            <div id\x3D\"calculator-button-3\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">3</div>\n            <div id\x3D\"calculator-button-+\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: operator\">+</div>\n            <div id\x3D\"calculator-button-0\" class\x3D\"u2 button button-gray\" data-bind\x3D\"click: number\">0</div>\n            <div id\x3D\"calculator-button-.\" class\x3D\"u1 button button-gray\" data-bind\x3D\"click: number\">.</div>\n            <div id\x3D\"calculator-button-\x3D\" class\x3D\"u1 button button-blue\" data-bind\x3D\"click: operator\">\x3D</div>            \n        </div>", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webcalculator", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAARFSURBVHhe7d3Na5xVFMfxeyaJ0fFlIVoFu9CNiG5Eu+hfIIJvrZCde1elpqEiuFexmoHWLvoXuKi0qSK4dOULbYVSqoKCC90o6KJIXmaS53ie6QniC/rc6dwhnPv9LHJfMplknvnNufcJzyQJAAAAAAAAAAAAAAAAABCFeBva4nMrD+nCaN6H/2t47uR33g0vbAAWXzj6YC/1TtkjPChJ7vHpTlTT9SR6USStrJ8fXPHpkEIGoP/8K8+knrxvD+9On5qYJj2ysTZ4z4fhxAvA0tJcf7j/6yTysM/cJN3e7u08GnVZ6HkbRn+0/9XpPfktmZ/bmX/dB+GEC4DV7APemx4pcJ97RLgAqMhj3p0aWyenfp97RbwKgCwEoHIEoHLhAiCqVfx2c1riVQARqlqGeAdLWdZyBKwALAE5qACVC1gBCECOgBWATWCOcAdLI4a6oHgHi01gloBLABUgR8AKQAByBKwAbAJzUAEqF7ACsAnMQQWoXLiDZS9/ApAh4BLAJjBHvIPFEpAlYAVgE5iDClC5mbxa7n76yF2bi/NvWbd9g8WTXLb1N6qNilxLmi41jb6x9dHge/9MccUDcNuhlRdF9aR9pwd8Cv9F00h7aXnj/OppnymqaABuP7yyr2maqyKyz6fQkTZycOPDd7/0YTFFS7E9+W/y5E9GpHnHu0WVXYslPeE95BKZybErGgBJ8rh3ka9/x7PHHvF+MezGK0cAKkcAKkcAKkcAKkcAKkcAKkcAKkcAKkcAKlc2AKpD72ECw1vmdrxbTNkASLrkPWRSTb8Oz50o/veJiwZAU/rMu8gkkj71blFFA7CxNjhuIbjqQ3Sl6fdG07KPiiq+CdS087J9LH5lSxz6g0rz0uaF1R99oqiZXULdP7R8PKkc+MdFIu1l3O2VvO37+setFT/Rnq2BbTj/Mrd7Gxm3Npf+nLvRtnM2KkW1aT/at7DWfsKUmvbSnd2+2g85nmvH9iza0NrxbcatNbvjxm5iN7e+2ufGbbpmX3t5fWv0dvrk1Jb1Z2JmAZgVC9rIHlbn/w/U1fraarhj1Sq+BMxc+4pCZ/EC0JZTdBawAozXY3QUsAK0mzJ0RQWoXLgA2EklFSBDvApw41wcHQUMABUgR8AAsAfIEXATyFlAjngBaH/fjs4CVgD2ADkCVgACkCPiHoAlIEO4AAgVIEvACsAmMEfAPQCngTmoAJULWAHYA+QIWAE4C8hBBagce4DKhQuAchaQJVwARPUr706N1ZSp3+deES4AjUiJdyRf9jaccAHYXFtdTZo+9+FNsw3FzwsL+poPw4m3CTTa02NWtr/x4cRsP/mL3c/R6x8MfvOpcOa8DWX72y9+2r7/qTMLt27dm9o3jGq6z04PO4fdnvQrdvuP52Xx8PqFExd9OqSQb3j8F5KWlrpXu7Nni/9pFgAAAAAAAAAAAAAAAAAAAAAAAAAAJpLSH7cNIW1XFhTuAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACiSURBVDhPYxhwwAilMQBXQIEbUNqD8T/D068b+3uhwhiACUpjAYweQFz4n5GxByqAFeAxgDhAsQEoYcAZUPQRKMAH5WIFjP//lyCHCYoLCGkGgf+MDNJQJhgwQ2kwYFG3ZGJk/H8OyDwBUgk0URYi878fLAbG/3f8vnHiLlgYCPBEY2EfULoQxP62oQ+nOtrFwv//jJ/BNAPDJ7DAKMABGBgAdZIl0BuicNgAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="229"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="189"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
