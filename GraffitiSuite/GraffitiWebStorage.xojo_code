#tag Class
Protected Class GraffitiWebStorage
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case name
		    case "LibrariesLoaded"
		      if UBound( Parameters ) >= 0 then
		        mSupported = Parameters(0).BooleanValue
		      end if
		      
		      LibrariesLoaded = True
		      
		      LoadAll()
		    case "DataReceived"
		      if UBound( Parameters ) >= 1 then
		        if Parameters(1).StringValue = "null" then Parameters(1) = ""
		        ValueReceived( Parameters(0).StringValue, Parameters(1).StringValue )
		      end if
		      
		    case "storageFull"
		      if UBound( Parameters ) >= 1 then
		        StorageFull( Parameters(0).StringValue, Parameters(1).BooleanValue )
		      end if
		      
		    case "loaded"
		      DataLoaded()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  //
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Values = new Dictionary
		  
		  dim source() as String
		  source.Append( Self.ControlID + "_constructor = function(n) {")
		  source.Append( "var lsSupport = (typeof(Storage) == 'undefined' ? false : true);" )
		  source.Append("Xojo.triggerServerEvent('" + Me.ControlID + "','LibrariesLoaded',[lsSupport]);")
		  source.Append("};")
		  
		  source.Append( Self.ControlID + "_constructor();" )
		  
		  me.ExecuteJavaScript( Join( source, "" ) )
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  //
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  //
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function Count() As Integer
		  Return values.Count
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Key(keyIndex as Integer) As String
		  if keyIndex < Values.Count then Return Values.Key( keyIndex )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LoadAll()
		  if Supported then
		    
		    dim strExec() as String
		    strExec.Append( "for (var intCycle = 0; intCycle < " + if( StorageType = StorageTypes.Session, "sessionStorage", "localStorage" ) + ".length; intCycle ++ ) {" )
		    strExec.Append( "var currKey = " + if( StorageType = StorageTypes.Session, "sessionStorage", "localStorage" ) + ".key(intCycle);" )
		    strExec.Append( "var currValue = " + if( StorageType = StorageTypes.Session, "sessionStorage", "localStorage" ) + ".getItem(currKey);" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','DataReceived',[currKey,currValue]);" )
		    strExec.Append( "}" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','Loaded');" )
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Remove(Key as String)
		  if Supported then
		    dim strExec as String = if( StorageType = StorageTypes.Session, "sessionStorage", "localStorage" ) + ".removeItem('" + EscapeString( Key ) + "');"
		    
		    Buffer( strExec )
		    
		    if not IsNull( Values ) then
		      if Values.HasKey( Key ) then Values.Remove( Key )
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  
		  if Supported then 
		    dim strExec as String = if( StorageType = StorageTypes.Session, "sessionStorage", "localStorage" ) + ".clear();"
		    
		    Buffer( strExec )
		    
		    values = new Dictionary
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Value(Key as String, DefaultValue as String = "") As String
		  if not IsNull( Values ) then
		    
		    if Values.HasKey( Key ) then
		      Return Values.Value( Key )
		    else
		      Value( Key ) = DefaultValue
		    end if
		    
		  end if
		  
		  Return DefaultValue
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Value(Key as String, Assigns Value as String)
		  if mSupported then 
		    dim strExec() as String
		    strExec.Append( "try {" )
		    strExec.Append( if( StorageType = StorageTypes.Session, "sessionStorage", "localStorage" ) + ".setItem('" + EscapeString( Key ) + "', '" + EscapeString( Value ) + "');" )
		    strExec.Append( "} catch(e) {" )
		    strExec.Append( "if(e == QUOTA_EXCEEDED_ERR) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','storageFull',['" + EscapeString( Key ) + "']);" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    
		    Buffer( Join( strExec, "" ) )
		  end if
		  
		  Values.Value( Key ) = Value
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ValueReceived(Key as String, Value as String, IsSession as Boolean = False)
		  if IsNull( Bindings ) then Bindings = new Dictionary
		  
		  Values.Value( Key ) = Value
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event DataLoaded()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event StorageFull(Key as String, isSession as Boolean)
	#tag EndHook


	#tag Property, Flags = &h1
		Protected Bindings As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h1
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  Shown()
			  
			  if value and Supported then
			    RunBuffer()
			  end if
			End Set
		#tag EndSetter
		Protected LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSupported As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		StorageType As StorageTypes
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSupported
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		Supported As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Values As Dictionary
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.storage", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAudSURBVHhe7Z19jBxlHcef3+zeXXfuzmJrECkgVSvVKKjVvihKItUSXrp7TU/xBRUFQsS2t3stwbecVSMBe7fXEqI2IqJpjFC43SvUAqKBaIu8EwytsSTF0KTlpbUtO3O93Z2fv7n7FW9v95m7vZ2Znd15Pn90ft9p77ozz3ee5/e8zLNCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFAqFQqFQKBTNAvCxYehM3DTXKhrR3I4th/nUGK2r1i7gsCFoyYvjdA2vUojjZ+pDwxhA70pegBYMAIjP8imBiK/TBRylq5hPlxLl0w0Flf4uQNxuZNN38ClfaQgDtHelPmehuJs+7Gl8qvlAMTSrI/+NI9tuO85nfEHjY3Dp7uuwLPxdUxe+DYguMxe9m5VvBN4A+uiJmwHgDJZNDQhYEUskf87SFwLdBMyKp87WQPyHZWignOD8XDb9AktPCXQNAAK/zWGosIS4mEPPCXYTAOIajkIF9XQ+xaHnBNYAeiL1TWoT38EyVCBCB4eeE1gDUB9/A4chBFs58JxAGkCPp26kzH8hy/AB8CRHnhM4A+grey6jtv8WluHEgqc48pxAGaB11YYFqMFvWYYVNE3xJ449J1AGiFqFW8Oa+J0ChdgpHu7PsfScQBmAst9PcxheEHZy5AuBGgnU48knKAH6BMuqQYG/BoR/sKw7KOC9AHgTy2lRxJb5J7O3HGDpOYEyQCyRuos+0NdYVgl+z8ikb2YRGPREag8dlo6rKUB80simF7PyhaD1As7kY1UgiueDWPg2iHguh1My1v77TLAMgDjD6h8zHASK9njq4qpmMqGyAWKrk/PskVGWrhIYA+iJ3kV0s2azrApq97McBgoEjHM4NYgvm5n0E6wqcYceT+3uuDzl6gBZYAxAVeUXOawKSvxeNIYHnmUZNBJ8nA7S6r9YgPaxAMQyK4oP6V09HxvTLhAYA4DAL3BYFUDtP4eBQk8kv0uf7myWUwPy7l9EwIc4JOh3WvB9FjUTiF4Adf8upRvwAMuqIfNcRV2u/7IMAPgR+lQ/YTEdDOOCzk6xcaPFuoRYPLUVQFzLchywFhlDg8+wmjEBMUAqQ59k+u1l83GvkRlYzXEZlAC+Qod54+oU7nR7694EtF+x9vyQF76NtPqfFU99kg6TCt9m5gNmE6m7ASwtcgOH4WVUkxoAAC/jsBTKmjmqiboaINaVXEpdv+tYhhR8zNi56RCLSlzKxxIQ4CiHNVE/A1yypo36cD9lFV5QXv3b0+MggBLKcujxf5HDmqiPAbq7I7FZLffTxfm2+jWoUBYu7/5ZxYpP/xguLRqpiwH0/Fnb6cKXswwt1Izvc1z/j5LqX4jDI8P9j7GsCd+7gbFEcjE9+TVP2VoWfmlktDAkOg8V+FRdmXvyXN2E4u+r6tEgDhjZdC+rEmbH152Wh0jFdh7RutPMDroyN9CgBoAbjUy/r69QTQc9nvoh3dEfs5wSQLE8lx14hGUJ7fHklynR28ayBKo5VpvZ9L0sa8L3JkADrdbFDnmjpWMzx4ECAS/icEqoE3dEVvg2liT7p5+0zBZ53lAtvhsgN9T/GoczA0VG3LNxlFVg0Lt63lVlUutciACV23+717A9bbKsGd8NoHdt+CiHMwI0EcypX6xuNBMlc/827SvXL6e2+e0sS3GYNJoJvhsArcL0lkdJyEVHAmkAKplqpn5FrKVFWpCoWZVH/wjUGtwA5Oya+v6deZjRsjEv0RO9P6DqfwXLKUGBDx6955ZjLMtBrNz+o3hi5L7+l1m5gr+9gNXJmJ6HE/S/RvhM9SC+RH8GZg0AZeoL6CZ+mOW0oGRxnTmU3sKyBHsvJPoHz7GczI+MzMBGjl3BVwPM6kpeqSH8gWVoKRZxwckd6f0sS+CFJD9jWQKZbbE51O/qe4O+NgFgia9wGFqo+n9WVvhjoJC1/wfcLnwb3wzQsSq1EAAuZxleHCZ/YitvOJOyfMnmEJaryd8pfDNA0cKrOQw1KORZPECbw+SPe4M/E/HFAO1dvadTlryOZXhBcXAkO7CbVTkgHf0zjOHBxjWAhZa920fbuAovjm/+XNQXpR6OZPXP2M+5sgJoMp4bILay5+P09K9nGXakBojNOWGvjJZsDTPzFdNT4bkBALTbOAw3KEbN1k55DWBJnn4CXB79m4inBtDjvYPUrtU09Ns0AO50msSiWlI2+vdobqjf3lXcEzyuAXAZBwqnpd+JdcvIAWexnIxnT7+NpwZAgZ45t9HAqLwgASPS7p+X1b+N5zmAwgZ3m9vTB1mUI1n7jyj2UvX/T5ae4K0BABrqWzy8Q/4Ut12RfB+1/5XXSDisGXALzwwwNvQrxHksw43DKJ4mHfyx/86b4d+JeGaAIuLnOQw7+x33L9AkS78R38gNDf6FpWd4ZgBAkL7tGiocJn/mXLLmbdKFJD5U/zaeGKA1kXo/HdSef+NIC9KItUqrf/Ro8mcynhggKvB2DkMNZfHHjezAgyzLAIfRP3200LgGID7Ix1ADU83hy7p/Quw6ssufbw9z3QB6ovc6urLALdysE1IDjG0hJ6Dy0m+HvMFtXDcAZa+h/J6fSkQgLy1IFPLq3wL5hhFu46oBYome6wHEBSxDDRXwIycyt7/BsgyUv/nzzMnMJnvlsy+4Z4C+Po26flVtjNzMOC39svdFchgk8+3pt3HNAPrzxwcFwLtZhp6oJi9IjETl3T8fq38bVwwQ60p1U+K3hmXooSz+hTfvG9jHshzp0i98ZSSzyd5d3DdqN8Ci61qEJX7JSjEG7OCgDL17/RlUU17IsgTHNYMeUbMB9LM7r6bEbw5LhY1VGOaonNGiNPsfWzXkMzUbAK1izduVNhn7zeHNTjugVG7/EUfN043GM4DZNipv60IIIv6Rw3K6uyNU/VeuAezJn61b86x8w5UkUPF/LIjcyWEZsdFz7Kdf9n6E70+/Tc0GaC/Epv2VKE0P4n1OgzjSbV8JzDeoASzLUqt+TgFaxde630LW/RPi7+YDDmsGPcSFJkB7JwfhBvEXRqb/aVZl2PsiUxVQeem3j5M/k6nZAADWtLdGaVbsd/6NbNp5Ekyy6+c4/o7+TaRmAyCKae+N12ygEIep8G81M+mpv8MHJdk/in8b2U2yLWE8p+YtYvR4qkC/Rbbnzwhd4HN0k15n3bAAZXB0LSYVukk37UXQ4KncUP+0Fm22rep5T8TSKieHiJup9uhh5Tu1GyCRNOnXzGL5FtQfflizilfldmw5zKdCSyyeWgMgKm4KRWn0CiMz+BAL36k9CURRuYBBDKvCZ6Rv/uCxeha+Te0GAMm+9aiWhdnMXbmhU7b0m6rfuiV/p6g9CRSi4obH1GReKbr7JBsehIecKEgHf6hZaHwDxNrz9pbmJ8fVRGC+nj++iUVooeTR3h6nIhGHN4b9ouYk0EZPpOzt29eOq0mguKetteVax61RmxGq/fTRE3fRHb6Sz5Rgrxmk7mPdvzXFFQPYu4BZFu6Vrgug7hMlQsOWgKyGxb2Ira+aB4+9Jp72f/bLU/r6tPZnjn0ANbicHv3r6YzTPMm3jMzAbziuG64YwEbvSl5NiV91F4SiSJ9g5vsGNyj09B8yjx6ZLx69a4RP1Q3Xbn5+3+PPRRcui5GjKi53qgi40AtpQEDgd/IP/ko6b+Anrj59hX17/hw9b9k8agoW8SnFJBDFVjObDsz3Jbpe/Rb+tWdHdOHSCPV9QztHIAd3UOEHasNsT9rfwr7H/9qycMkBRFhCtUEnnw43KLYY2fTXWQUGzxIwygmen33ehVsLAjup0VvMp0MHIh5CAdeY2YFAjom41gtwIrY6OU/kYR0Afob+yyV8urlB/BuCts3M9Af6nQlfDFDCV3vb23NiPlrWOXSDymYRGxoLC0IUD0esyEtv3j/Q8FPgCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFAqFQqFQKBoFIf4HFYCI5ZRlc7QAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAErSURBVDhP3ZG9SgNBEMfnv5WJmCfQJwh2PoNYmYOQCHaCWl/ME1grGixVsFabIBYK9oLaqrX6BimixXnj7GZ2k83aC/5gbj5ud3Y+6M+B6ohKY2cX4Dl1J3kd9g9P1HYkCWYbnS4D++omDPsH0R2jOsCEJhMNSirXJoWJb0Veqlm+rEcdUbbKar4EYx6dw3TvtMKgunwvwKgxl3ufV70nG48qgMG61fLSaYmy54XAZ/JSDcSX8nvBX7bELTC1rAJh05A59yKRY9uWOwJ+ttoTEsxkeZuAeXUTwHQj82lJJQ8acoQEhk1Tzd8B30n/9ek1jlsAtdVKGJXPb2J+jCJjQgK7ounVeeGiWJQXVmSIUfmWsMZq1tmSCW2omyDDG3Dxvf11ffSuof8B0Q9R4nmjdGM/SwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag Enum, Name = StorageTypes, Type = Integer, Flags = &h0
		Local
		Session
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Supported"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StorageType"
			Group="Behavior"
			Type="StorageTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - Local"
				"1 - Session"
			#tag EndEnumValues
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
