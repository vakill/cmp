#tag Class
Protected Class GraffitiWebAdSense
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		      
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		  
		  select case Name
		  case "Style"
		    UpdateOptions()
		    Return True
		  case "Enabled"
		    if LibrariesLoaded and isShown then
		      
		      dim strExec() as String
		      strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
		      strExec.Append( "theField.prop('disabled', " + Str( not Value.BooleanValue ).Lowercase + ");" )
		      Buffer( strExec )
		      
		      Return True
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// Do not implement
		  UpdateOptions()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' class='" + if(not IsNull( me.Style ), " " + me.Style.Name, "" ) + "' style='overflow:visible;'>" )
		  source.Append( GetAdSenseHTML() )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function GetAdSenseHTML() As String
		  dim strOut() as String
		  strOut.Append( "<ins class=""adsbygoogle""" )
		  if mAutoFormat then
		    strOut.Append( "style=""display:block""" )
		  else
		    strOut.Append( "style=""display:inline-block;width:" + Str( me.Width ) + "px;height:" + Str( me.Height ) + "px""" )
		  end if
		  strOut.Append( "data-ad-client=""" + mAdClient + """" )
		  strOut.Append( "data-ad-slot=""" + mAdSlot + """" )
		  if mAutoFormat then strOut.Append( "data-ad-format=""auto""" )
		  if DebugBuild or mTestMode then
		    strOut.Append( "data-adtest=""on""" )
		  end if
		  strOut.Append( ">" )
		  strOut.Append( "</ins>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebAdSense -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    
		    if not IsLibraryRegistered( Session, JavascriptNamespace, "adsense" ) then
		      strOut.Append( "<script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>" )
		      RegisterLibrary( Session, JavascriptNamespace, "adsense" )
		    end if
		    
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebAdSense -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    
		    dim strExec() as String
		    strExec.Append( "var myElement = window.GSjQuery('#" + me.ControlID + "');" )
		    strExec.Append( "myElement.empty().html('" + EscapeString( GetAdSenseHTML() ) + "');" )
		    strExec.Append( "(adsbygoogle = window.adsbygoogle || []).push({});" )
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAdClient
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAdClient = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AdClient As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAdSlot
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAdSlot = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AdSlot As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mAutoFormat
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAutoFormat = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AutoFormat As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAdClient As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAdSlot As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mAutoFormat As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTestMode As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTestMode
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTestMode = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		TestMode As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.adsense", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAYe0lEQVR4nO2df4hc13XHP2dZtsvMshghhFFU4QYhRAgmFcakqXFTxU1S18lO2jQkTdzQ5odju25iTY0xwRhjhJu6s8Y1beK0Tdq0aVw3dWdbN3WN47iuozrCVY0xxhjjqqoRQhhVCM2yLMt++8d9b+69772ZfTM7v1baLyw7c+c78+5799xzzzn33HtNiG1cupgadwW2MV5sC8Aljm0BuMSxLQCXOLYF4BLHtgBc4pgedwWGjWqtXgF2AjuAOUmzZsyAzQDToBnJMNOqZKvAmhmrwApwDnQO7Fyr2bgwxtsYGuxiigNUa/UpYLekK81sP/DToF1gu0A7JebNmJVsxkwzwAwwI0HS6KsSq2a2CiwDZ3FCcBbsDPC/wKvAy61m49SYbnOg2NICUK3VZ4BZYB9wCPQBiXcCl5lRcZ/ZNAgwACRhZoBQcutmJJ935a2DLQMXzDgPvAn6D7DnJB0zs7VWs7E6olsfGLacACSNvkdiH+hjZnYDaDfYVNpYruHiRixq2EGUAes4YXgC+KHEa2b2VqvZWBn2sxgEtowAVGv1OeAg8CFJ7zWzq0DzbvwG3yhhg9H+zPPyGCBvOdEGR4F/lXR0eWlxrf+7Hj4mXgCqtfo0cEjic2ZcBewFTQc9sN0w/r8Tgmxj+QZMvzc03irwFuhZiUfM7Hir2ZhIQZhIAXDGnHaBXS1xmxnXJo3exW2N1LIriRouLfNCEg4VQ+KtAyuSjoI9ALywvNQ43+djGQomTgCqtfqcxA3AjaBrzWwubVwl1lg4DseGXJYH4XDgyuIxfXQ8zgHfAX0P7PikGIwTIwDVWn0qcd9uBmqSdnk1mzO82nA9rxOvs0CMgyexZsbrQBP0QKu5eK7HxzRwjF0AXMMzb8YtwBeAPQQBqrxqDa18PwYX8UJu+Huu3PfOMfBWzOwYcA9wdJzaYKwCUK3VZyWuAd0FHMqOryHc80vrWpaXPnCLBMWVW0ZQRs8zs9PAw8C3Ws3G6eI7Gi7GJgDV2uEK8CWwm4B9kqbShxOrVm9UpQ1bnhd/HiLbWGPkrUj6vhkPtpqLxwu/NESMRQCqtfouSUeAT4DN+14CoKSBkwrm3LpeeAp6nLV54WcTwlsDOw66y8yebTUb65t4vD1hpAKQ+vTAEVxQZwrih9IN/fGKgjbeSJsw3lnQHRKPLi8tLm94owPAqKeDPyrxoMRVoI7XlvwfXQR0Y15soHleVogmhrcDeMCML1Zr9R0db3yAGMl0cDJL96vA/Wbsc6VhT7BMVC3sIZvjxXECx0u/O4k8yXaAbgVeBp7p+FAHhKFrgMpCfQ64RdJDkmv80BACIjXZyZ3rj2eY5S1x/9nk8cx4G6xhxvOMAEPVANVafQrpi8DdZnZZpp1yrlMazMm7cv3x8h4CmZ6nSeOdAo4sLzW+wYgw7CGgZsZdYJcB7bBpaADlp249z6M/nnvo2WuSfBfSYWMyeLwFdgT4i24PdNAYihdQWTg8ZWYfBj0I7E8uRVby4/HcI2vtb5aX+Rax0EwE7zRwJ/DYqPMIBi4AicF3HfAg8K60PO//gheEjcbz/nkEmT+eG/3GuplPJunEK/t7vfIknTSzL7eajSZjwBCGAO0Bu5d248fBGSCnFkMLvlsUrVdeei0z1nDj6wngLeACsGxmF4CWxJSZTZtRBeYldppxObBXYjfYdHYeIjTcQlXeC8/MXjeze4EnOtzI0DFQAajW6tOS3WXG1aFL5oMd4c1D2IC+R+Ss4n55F4DngX8HHccldZ4DzgOrwBpotdV0GTuJ5poGZs1UAavgcgt3AwfBfhk4KGkmuUI70hdeOzVOvYbqyDsJfFXSE+PMGhrYEJBM7HzJjHuBed8o3mpP3iWq0WfVFGmJ8AGW4ZnZmqRTZrwO9k/AoxJngfXlpc2FVhObZtppN66X7FOgA8Bl6fARN3Rq+Xu7J04wtRPAp8x0rNVcHFnYtwgDEYDKQn3KjOtAD0vs943me6YfD8Pxz7tG2aBIPE525oHWgZNgT4K+i0u2GGoYtVo7fJnEDWb8CtgHcWsOckZoKrTBkLcu8bIZd7WajSeHWceyGMgQYKadEreb2b608Zyqyxo9WcMotoyLNUZnHnAO7G9Afw861moujmTxRpLI8dfVWv0p4FqgLnE1QWDNa4PITnkFuFPSs6OoZxlsWgMkY+dXgK/Rk0CljVocFAmHjCxPYsVMx8DuAF4Zdo/vhiSTabcZnwFuBdtTQFsHvQb2WeClSUoQ3bQAVBbqB8z4IbAb4khXNlpXnCUT92w/RRpb0N544k3g26BvtpqLZzZV+QGjWqu/H3Q/cNCtPgKJdTNeBN3aai6+OO46ZrGpuYDKQn3OjC+DdrkSN+Y71yu19tV+T0bYXHkqHOn3Y/cp5SXfPSpxE/CHk9b4AK1m49mkfo+bsZI0/lGJ2yVGnuxRBpu0AXQIuEFiOgxxhjN0ETsyklLL2Rt6WQvfl2vNjB9Iunt5afHlzdV5uDCzV4A7cLGGdwO3AS8vL43X2u+EvoeAykJ9N+gR4AYII3GdQ55F/nrwaafvLQPfAu5rNRsT1+s7IUl+qbSak7UOIIu+NYAL9nCtex2GVi1jzUNq/XeK2xfxkrIVSd8ws/tazcbYU6h7QWLoTXTjw+ZsgJvMbB5IIl/tsGsQnImFIuV5fz7UPrmxfw34jpndv9UafyuhLwGo1urXAB8Me3U8tntk7QFnJHpOGDEMeGtmPAq6r9VsvN1PHbdRDj0PAcmOG7cBU2nDZRuweEo2bxwWTQoltsBzwL2t5uJbvdZvG72hZw0g6SDo6rCnu5kuJSo9fA1xAEeR2g+zZXyZnQZ9tdVsvNFr3bbRO3oSgGqtPm1mh4Dd+bnu8HU2h7+IV+Tza8WMB8AmLmBysaKnIUBiL+gXcHvrtJEGasKFGmH4NjX64mBQNtrHmpk1gUcnKVR6saPXIeAA2HvivLtU7acUURT6zfMgFBDQSeDrF8vmS1sFpTVAtVafQfpFM3b4Wa4ouyXi55MiPS+bzpWEkB8DXtjEvWyjD/QyezdnZh+HfEQvHv99eSdeXljsbdADk7JpwqWEHoYAuwLYm7yO0p58PF/t1zGPDC9nHP5Zq7l4tufab2PT6MUGuDblZ3uyn9HLJn/6/+nMYMoPeG+A/q6/6m9jsyglAJWF+pSkD/iSOHrn5/wVaAW1eelQkPIC7bEO/CPYa4O4mW30jlI2gBmXg70rduu6p3L5HD5vLIaaIsEZiR8vL40vo+dSR8khQFcCc5Bv/HQeHwqNu0x5LkZ8woyXeqvyNgaJsjbAfkmV0LpP/7xqzyd9el76PuWB2xVDLwEnB3In2+gLJd1A+xkzzfqxP0zYDIeAeNvW7FxP5v0KsLQd9RsvNhQAN/unXY6bzdSN0759A2d5RDwAc1uyjz1Prlo7PC0xD52SVrMJL6PhhWH1MFm2JG+9bA5FGQ2wA2xnXFSc8pVHV94pYALm+u0A8PU4eSXOTAqXuY2Kl6ulpUOsn3fpzONUZaF+0/LSxkJQRgB2Jn9AagTG3oBX+xBOAmWqFg0PwPFR7obVBfNmuiYbtvYxjDAlPR/eHhbPodPy9/xGU5l5l7eAK2BjA7uMETgPXJa+iUO7oXUfViBv9WdXCwH/VeLaI0KskuPXNiYeUQO793G6XWeezZrZbkqgjAaYlZgJVVVW4sKKp+sA0qBP0TLp5P3rZSo4fOTtFIfuw9yoePEayTSWoq480AzFK5RyKCMAM2Zu/j82RsIjV/xCkLBi2fl/Xwa483gmALHB2imRxWNUvNBADL+cFaQ8T2LWjHd0umKIDYeAZD38bDbl21ckr8byn/sp4CAEPGEp01mhHjcvm1wTTsHHApHlgU1LZAz3YmwoAGbp8WrF0bzUsPNaIHsj7d9p80AruE0aJgjelfXBq6JuOzpenGGVbfjOPDOmQLPJwt2uKGMEzgAz2VTu7NiUVjAViDhRlIgncQGnBcaOeOzNzmoWddvR8kLbyz2/VKN247mOK208xPe1LiDrg3rhCHfJsAwvnA7ubhCNEnFVytZr+Lx4RA0N6Wz0tZgnd0Dm7EZXLiMAq+6v2814965z41rImyt57RHAC2g2TT1v14yOF7t5jhcPAerKM2N6UBpgFbTmL0pU4XTeP7yR9CbiG4t4s85VmSR4bdVdQY2Ol186lzX4OvOAdTNtOMyW1QDR5oXeoEtfeys16xp6uyDiTYFbVzip6BSSHRUvtrHSBs7um9SZB7bm/rqjlABIocVeHIjIqiSvGbKuS/v/SLZD3xiRe9pGGHWL2CPixTZWuqoqbGhtwNM6boFtV5QRgJVk5o54HM+OR7EUes3gt4ULeWD7S1x7BOhsu4yzLDuchoGe0MDuzLNVl3PRHSUigToP1p5V8oZGOAZ1cnNSjyDeGzDBz2587VEh1QLZsTgbnh0dLzbu4g7nJ4U68taBC2U2oCwRCbS32XDatpxqy9zEwTKBimEjnKRyNk34aWp4aSy8bB1TThyNLeLZmqRSofYSkUDOgt6O07u8ug+lseDbCa/Qj90N5cKVw0RssxSP02E8Y5S84BtBkKcMjzUz+78uxDY2FIBWs7EsccaMtaykxpGrWP3HxomvYCA8M6CDZSo5XHgboCjrJsceAy+dYU35DkWC1eatUnKyrZQKNuO/wVa8hHkrtLNAKvMXfOLezkosVGuHR3JuUScUW+bZCNt4eLEnFX6WT76NeVqlZLJt2Yf/Om63rrnQgMmqzzBtKZbUvIfgolT2HtxyszdL1mPgMHd655+kvdDHMbxVnR32Bsx7lxnXQLrVnjfyYp8/NsDd73Xi2YpEqd1VymYFvwy6AOwKVVbc8G1uJBhZPzX6VeMK4D2MUQBazcabwK3jun61Vr8PeF/6Pm1Eb1tB2ujZYFInHrBiVk4AylrhpyVezVqc3gZILx7HBlJePmbQfr8L+Plk36FLDpWFw7PAlSQbbsSx/Wx4N1vWmSfxZtn9k0sJgEvetB8mPw6ZsG/o0viG97x2Vb2bkt7oFPBR4ECZelx8sAPAFe518cFY4IePuAN25plxrGwNSvvhZnouCS/m/NGsD5uqqtj6L+ZJeifo18vW42JBEgN5D+5YGoqDaB7ueSp435kn8Z9l69HL/gAnwE7GwZwwshVWrng3sJjXzg+YAj4/qqNSJwjzwM/hjqWJnpcPozv4j+Kd1op5WjOj9H7KpQVA0gXg+0VGXTwLlTVMvK8bTw+HRo3tBN1RrdUnbIp4eJA4YKbr3Wtv5AWM9iu/DqNoQUiWZ8egXBQQehCA5aXFVeBHuBOuwxtJL08YVMlrhZAXu41J+SeA95atz1aHmW6U2ONeh+neaWPHnlXKyyK/I4uOuphNOfQUi5d4TeKlUO2EfmgeWfcwjrplOHuBm6u1eqkFDVsZ1drhK8B+I+8+++3zNwggBsNAVLwK9hPYeBYwRU8CYMZJM/6NJKM3jQQmYw9FbmD42vMKQ5rToBrok8lW6xclnMtrXyVYbRUiDAaliL2tTtFBAL0Ker2Xk8h6EoBWs7Em6Rncws4CSz80SHKqKXrt7YHQjrBZ3DlAEzBHMHg4G0efBGqxkezdurhnx7ZVnH2V461LvOiM9fLoeTrWzI5LOhZWvEhdhZ5ANmgUGjI+mtX+kcuBI9VafV+vdZt0SBwAbgZ2FkRFg2eUGx7T7wevLcs7D/xkeam3Ayp6FoBWs7FsZg+T5PWHFe6U8uSDRkWuYezTJsXvB91zMdkDlYXD82bcgfP9geLnFQbUUpVf8HyCWdh2R3sLeK7XevWVkNFqNp4HPZX23qyVGkcG85NCKXxuW1SKpGnJPgncXa3Vx54zsFlUFurzybj/GdyyLaCTVZ/+t0jlJ9/Iadtg4u3J5aXFnhfcbiIjxx4BzmeDGECk3kO1nw12pDfoDZ+obBr4TeDOaq1eaDBtBVRrh+fM9NtmfCktyzZiHDOJ/8fPLGtot8O/a8Bf9lO/zaRkHQOe8+N3vnKhGnOVjSOBETOYCAkeSEXiFuDeaq2+axN1HQuc0Wc13CliyfE68XDZPcOnk1bNfed54NV+6ti3ACS7ev+5GafiyoUI1ZgvCyc24hyB0F1sC0UFdAvwp9Va/cp+6ztqVGv1edBXgEdIDtWEUDvmgzvFwpB1myH2qjhvZg/0u9vKZpMyn5b0BNhamKveCambs/ECiexN2zRu1vDr1Vr9uspCfcM1b+NEYrzeKXEvUMl3jGIUP5fi7wbcp0B9H7CxKQFoNRsXwB4y40wc5SvmZ5MhQ/cvFox4P4Hg994HPGLG71VrhydySKjW6vtBXwN+18xmY62W9YLyXlHK69SRfLEAvQ0sgfW92dagDo8+DNzPCA+PBh1zbpWN9fBoaD+DOeAQ0ADtTbRWGx0yd3IIed20pdyxtI9KunV5abHvY/U2LQAAlYX6LjP9Fdh1kj9NLL3RTsEiyN5wbzyJc0ByfLwdW15qjOT4+BDOTdU1YDcCHwYGlN0UCknYCdoP6ATwkVaz8cpmrjIQAXA9QNcBD4Pt79SQ3tIPkyVjgzAr8SV46xInzfQk8F3geKu5OHSNUFk4fLm5A7Q+ArwfF8HM1X1Dc6cDb4PvrgN3tZqNP+ilzkUYiAAAVGv1WUm3mNk9JC5PVtWVVW8hyvJwCyFPAa8B/yzpUTM7C1rvZXKkE5IJqmlJ+83s08AHgT3ADjK21GYaviSeAT7bajY2fa7iwAQA0m1X7Y/N+Dww1SmmXQYDeIgXJD1nZj8GHQc7A5zDxcyTTS+01mq69XOVhfoUMGWmGbCKRAV3TM5luLy9XwKulbjCjKnsdYvtlnz9yvK63PFJyb6wvNR4qgx7IwxUACCd6+Zvwa72N5sfw7I9O3zf6XWvPB+i1hrYKYk3zTiFW/C6jFvr0Eq+MiPxU2aaA9sB7JTYY8Zeifmw7tkGdWXZgM1geL4cgBUz/RHYfc4D2zyGMO9uJ4G7JR4CO5CfvEjfx70gFpTB8LxQ2DSw18yfeRQgHR6m8nWM6140HBW9HyQvM5P6LNhDg2p8GIIGAGcUSrrezBpAoVEY+rMbuHx981KfOhaaPCaZlwjAuqSXgI8tLy0O9HyFoWTeJGHJJyoL9VkzHgHtKHIHsz0uhd8Re3O82HPoPMZOMi8JrL1qZrfLbQI9UAx1fb6ZHgeOmLkNJuLGz08chZ/5yOJmeJ2GiC3FO40Lsr2wvLR5byaLoQpA4n59E3QPcNqVpquKQzWdfBKkkaXzBpvhpdcLeXFwZXJ5iTG4AnbEjMeHdajmUGyALJJQ6ccl7jfTO2N1XhTxKkLvPK9W898tjkBOFO+0GQ+1mo3f73CjA8FIsm8Tm+CxysLhdbD7JB2AdO8gn+AYj+nhrqP98YosalceDxmTxgOdMLMjwKMMGSNNvzZ3PPwFM46ADnpLPhzH07JwCXp/vODKbV5clqvh2HlJz78NeLrVbJRe4NEvRjIEZFGt1S8H7gM+AWmQBbKunUPaq6F3XohobA1cr8ngSayZ2Uugm1vNxb7n93vFWHbpajUbp0G344TgDbB135BG2Ev8zJ8yn23MKzq6Jt8bJ4K3bMbjEjeDjfQktbFogBTJkXTXSHanmQ6Bt+LDnuIaN+455XhZo7GofOy8M8DXgMcGMbnTK8YqAOAmYcyYB35H0ufA9piltkmR5Z99mN15Xig6/cZ4eJIumNmzwBHcCWpjOUBj7AKQInEVr5S42YwabvuYNrpN/HTiFX/u3bBuGBZP0jrYm2Y8LOk7m8nmGQQmRgBSuGxarpd0o5ldS3JoddiLskfUxsh6A7HGyDfYaHhJ+VmgKel7y0uLT3d/EqPB2LdqzaLVbJwHHjOzz0l8Gpf8sEp71q7MSVyOlyJeqAqxFT503jpoReIHwMeAuhnPdKv5KDFxGiCLaq0+LXGdGb8FXIXbRyAXv+imivsZPgbAW5V0wozngW9L9sLy0uQdlD3xApCiWqvPSRw040O49PCDtFPPIK/6k9JcAxUZkQPlnQO9CHYU+BfQi2nW0SRiywhAimqtPiNpj5ntk/RrZna9pN3AVD7y5oWhUyrWIHguiMPrwNOgfwBOgJ0al2XfC7acAIRINpWaBfYBh0AfANsHzEuqALNmNp1tOIf8StuNeBKroBUzO4/LLXwD+JGkJ83sJLC2FRo9xJYWgCyS9PQ9YO/GbT75DonLzdwJ6JLmzZgFm1FyIqq5gzEB1iStmdtrf8XMLgDp31ngTBKn/x/QK2CvtZqNCTn+tn9cVAKQRbV2eAqsgkvd3gHMyZ2rOwOakZgxs5lkvUJyyJLWcPPwadLoBeCcxNnlpa3Vu8vgohaAbWyMiYsDbGO02BaASxzbAnCJY1sALnFsC8Aljm0BuMTx/x2xAJxpLLeuAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAACN0lEQVQ4jZWUMWhTURiFv/N4lJKUUkqHIKVIkVLEsRTp5ODQrW8rODrUxTj04eAgTiIOr1IQUXAQik6FvqJTJ6fOEiRDcHAoCqWEUHyhhJDj8F5smqaD/3S597/nP+f8/70yZlSUo7hkuyJpEgiANnCcpUlzVL6GgUprm6GkZWAdWLJdAUKgKVG3tSdxkKXJ6ZVApbXNcYkNUNVmXiK4WNeAToBPwJMsTdr9k3BAypjQI+AFEEh0gB/gI5uepBnQok0A/i2pO1giPF96BVQFAtsd4I2kHVBDcsdmTvKqpAD0IUuTziVp5SieAF4B94Ee8BZ4mqVJa4R/YZYmZ8NmhwC2r0taKWjXJL0bBgFo7291ge7w/j8giWlgNl+rkXtzHuUoTmxPFrl9MQAdYDtLk0bhkcZsT+Sy1BpB/Z6kyjALm67EHtAo2uuOpD9Fu6fKUTw+dKcOrtmugWtAyzZFZ7sDHtGUOLJ1U2IBuAF8H6hdBYWFrBnguaTbwAn4FPLRR9JP4FCiB9wCHpSjeKoPk6Vb9SxNauA6MAcsFEffQL9gYLLL0eYdWzvArOQO8NrWR4lGYWoFuAt+ZmsOfCrpMfA+S5PewEByKHkb9DI3n4cSq8BRbqqnbS0CU7lEfQV2szTpXWCUs4rHgQ2bqsS87UCFMbYBIbllcyCpmqXJcf/upddfjuIQWAavg5aAazahRBPcAD7b+tLev/idXALqR2ktLkmuAJOgADizfdze3zoZlX8l0P/GXwLn/2wil20JAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="280"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="336"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoFormat"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AdClient"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AdSlot"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TestMode"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
