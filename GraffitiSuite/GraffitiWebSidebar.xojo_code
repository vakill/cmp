#tag Class
Protected Class GraffitiWebSidebar
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "sidebarOpen"
		      Buffer( "window.GSjQuery('#" + me.ControlID + "_bar').css('width','" + Str( Container.Width ) + "px');" )
		      Buffer( "window.GSjQuery('#" + me.Container.ControlID + "').resize();" )
		      isOpen = True
		      BarOpen()
		    case "sidebarClose"
		      isOpen = False
		      BarClose()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "style"
		    dim sVal as WebStyle = Value
		    if not IsNull( sVal ) then
		      if sVal.Name <> "_DefaultStyle" and sVal.Name <> "_Style" then
		        dim strExec() as String
		        
		        strExec.Append( "var theSidebar = window.GSjQuery('#" + me.ControlID + "_bar');" )
		        strExec.Append( "if (theSidebar.hasClass('defaultSidebar') === true) { theSidebar.removeClass('defaultSidebar'); }" )
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + "_bar').addClass('" + sVal.Name + "')" + if(OldStyle <> nil, ".removeClass('" + OldStyle.Name + "')", "") + ";" )
		        Buffer( strExec )
		        OldStyle = sVal
		      end if
		    end if
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  //
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim strOut() as string
		  strOut.Append( "<div id='" + me.ControlID + "_bar' class='sidr defaultSidebar'>" )
		  strOut.Append( "</div>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  Return "var theSidebar = window.GSjQuery('#" + me.ControlID + "_bar').detach();" + _
		  "var theBody = window.GSjQuery('body');" + _
		  "theSidebar.appendTo( theBody );" + _
		  "window.GSjQuery(document).ready(function() {Xojo.triggerServerEvent('" + Me.ControlID + "','LibrariesLoaded');});"
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub CloseBar()
		  Buffer( "window.GSjQuery.sidr('close', '" + me.ControlID + "_bar');" )
		  isOpen = False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebSidebar -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "sidebar" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwsidebar.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwsidebar.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebSidebar -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Initialize()
		  if IsNull( Container ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "var theContent = window.GSjQuery('#" + Container.ControlID + "').detach().appendTo(window.GSjQuery('#" + me.ControlID + "_bar'));" )
		  strOut.Append( "theContent.css('position', 'relative').css('left', '0').css('top', '0').css('bottom', 0).css('right',0).css('width','100%').css('height','100%');" )
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "').sidr({" + _
		  "name: '" + me.ControlID + "_bar'," + _
		  "displace: false," + _
		  "side: '" + if( DisplaySide = 0, "left", "right" ) + "'," + _
		  "speed: " + Str( AnimationSpeed, "#" ) + "," + _
		  "onOpenEnd: function() {" + _
		  "Xojo.triggerServerEvent('" + me.ControlID + "', 'sidebarOpen',[]);" + _
		  "}," + _
		  "onCloseEnd: function(name) {" + _
		  "Xojo.triggerServerEvent('" + me.ControlID + "', 'sidebarClose',[]);" + _
		  "}" + _
		  "});" )
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "_bar').css('width','" + Str( Container.Width ) + "px');" )
		  
		  Buffer( Join( strOut, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenBar()
		  Buffer( "window.GSjQuery.sidr('open', '" + me.ControlID + "_bar');" )
		  Buffer( "window.GSjQuery('#" + me.ControlID + "_bar').css('width','" + Str( Container.Width ) + "px');" )
		  isOpen = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ToggleBar()
		  Buffer( "window.GSjQuery.sidr('toggle', '" + me.ControlID + "_bar', function() {Xojo.triggerServerEvent('" + me.ControlID + "', 'toggle')});" )
		  isOpen = not isOpen
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event BarClose()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event BarOpen()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		https://github.com/artberri/sidr
		
		Released under the MIT License http://opensource.org/licenses/MIT
		
		Copyright © 2013 Alberto Varela (http://www.albertovarelasanchez.com)
		
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationSpeed
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationSpeed = value
			  
			  dim strOut as String = "window.GSjQuery('#" + me.ControlID + "').sidr({speed: " + Str( AnimationSpeed, "#" ) + "});"
			  
			  if isShown and LibrariesLoaded then
			    me.ExecuteJavaScript( strOut )
			  else
			    Buffer( strOut )
			  end if
			  
			End Set
		#tag EndSetter
		AnimationSpeed As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mContainer
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if isShown and LibrariesLoaded then Return
			  
			  mContainer = value
			End Set
		#tag EndSetter
		Container As WebContainer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDisplaySide
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDisplaySide = value
			  
			  dim strOut as String = "window.GSjQuery('#" + me.ControlID + "').sidr({side: '" + if( value = 0, "left", "right" ) + "'});"
			  
			  if isShown and LibrariesLoaded then
			    me.ExecuteJavaScript( strOut )
			  else
			    Buffer( strOut )
			  end if
			  
			End Set
		#tag EndSetter
		DisplaySide As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		isOpen As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  Initialize()
			  RunBuffer()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAnimationSpeed As Integer = 200
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mContainer As WebContainer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDisplaySide As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldStyle As WebStyle
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.websidebar", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAALoSURBVHhe7d0/a1NhFMfx59xak9Z/IOqgg2AHXUzBTQUHpw5ia0Ffg4hiSaVDi+iiiyZ16iB1dHFp2oKCBQdRcHF10CG6uShCbUKr9x6f4bwAac4F4/P9DMk5Z0hC7i9PhvvcJAAAAAAAAAAAAAAAAAAAAAAAAAAA+o3Yvavq5PRRKYp78eFr8QmGbIxtUNW2ZPKss9RctJEr9wDsvlA/kQ/oOxHZZyM40CD3u63GnLVuMrt3k+8ICxx8fxJ0ds/Fm8etdeMegLhmnbIKzvIsO2elG/cA8OkvkYb9VrnxXwHQVwhA4ghA4ghA4ghA4ghA4twDoKrfrIQ3yb5a5cY9ABLkjZVwlufhtZVuSlgBNq/Fm0/Wwo3Obq422ta4KeVsYHyxMjxen1GRmqhWbYhtiO9hOxN5vrHUeGUjAAAAAAAAAAAAAPgLJZ0ODmF4vH47iNZUA6eDexAP0BcpZHljtblmI1f+F4eO3TiYVwbXRMKojeBAC6l3Vxrz1rpx3xFUVAefcPD9SabNyuTUMWvd+G8JC3raSjgbyAfOWummjE2hB6yEu+KwFW7cA4D+QgASRwASRwASRwASRwAS5x4ADeGHlfAm4btVbtwDIEE/WAlnWZG9tdKN/1dAIdfjOvDbOvh59HOl4f7hKuVsYGXi1kgWirm4GtTiVwI/FdsLDW0J0uosl/NTsQAAAAAAAAAAAAD+V6VdHFqdmLoiQUbjU1RshG3Rz3mQl1ut5kcbuPIPwOU7O4e21l+IhPM2Qe80HqqrnVbjsfVu3HcEDf1aX+DguxMN+mDXpelD1rvx3xOoYcxKOIpL9d4iD//+P4fGV3rEKjgTKUasdOMfAPQVApA4ApA4ApA4ApA4ApA4/wBo6FoFZ1rCe+sfAOHi0LJI1gcXh4rojJVwpEEXO63Ge2vdlHI2sDpeP5MFfRgf/mRsuTi0FxLaqvq0uzx/1yYAAAAAAAAAAAAAAAAAAAAAAAAAACBZIfwBpzegNsTQNbcAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABnSURBVDhPYxj6gBFKM3AFFPZBmXjB/3//l33fNOEMlItsQNF/KBMv+MfwL/zHhgmroFxkAwrc/jEwCEC5OAGyZhBggtJkA2p6gbxApBjAXcAZULiD8T8jH5SLE/z//y+PqtE4ChgYAErAMwpQHKR1AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = SideLeft, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SideRight, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AnimationSpeed"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplaySide"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="isOpen"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
