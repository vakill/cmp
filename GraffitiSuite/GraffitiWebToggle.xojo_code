#tag Class
Protected Class GraffitiWebToggle
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "valuechange"
		      if mReadOnly then Return True
		      
		      dim newValue as Boolean
		      if Parameters(0) = "true" then
		        newValue = true
		      else
		        newValue = False
		      end if
		      
		      if newValue <> mValue then
		        mValue = newValue
		        ValueChange()
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    dim strExec() as String
		    strExec.Append( "var theToggle = window.GSjQuery('#" + me.ControlID + "_toggle').toggles()." + _
		    if( Value.BooleanValue, "removeClass", "addClass" ) + _
		    "('disabled');" )
		    
		    Buffer( strExec )
		    
		    Return True
		  case "width"
		    UpdateOptions()
		  case "height"
		    UpdateOptions()
		  case "visible"
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  'if LibrariesLoaded and isShown then
		  'me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_toggle').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  'end if
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  'source.Append( "<style>" )
		  
		  dim strClass as String
		  
		  Select case mVisualStyle
		  case Styles.Dark
		    'source.Append( kStyle_DarkCSS )
		    strClass = strClass + "toggle-dark"
		  case Styles.iPhone
		    'source.Append( kStyle_iPhoneCSS )
		    strClass = strClass + "toggle-iphone"
		  case Styles.Light
		    'source.Append( kStyle_LightCSS )
		    strClass = strClass + "toggle-light"
		  case Styles.Modern
		    'source.Append( kStyle_ModernCSS )
		    strClass = strClass + "toggle-modern"
		  case Styles.SoftFace
		    'source.Append( kStyle_SoftCSS )
		    strClass = strClass + "toggle-soft"
		  End Select
		  
		  'source.Append( "</style>" )
		  
		  source.Append( "<div id='" + me.ControlID + "' class='" + strClass + "' style='padding: 2px 2px 2px 2px;'>" )
		  source.Append( "<div class='toggle' id='" + Me.controlid + "_toggle' style='width:100%;height:100%;'>" )
		  source.append( "</div>" )
		  source.append( "</div>" )
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  Shown()
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebTimePicker -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "toggles" ).Child( "toggles.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "toggles" ).Child( "toggles-full.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebTimePicker -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub MakeInvisible()
		  if LibrariesLoaded then me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').css('visibility','hidden');" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub MakeVisible()
		  if LibrariesLoaded then me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').css('visibility','visible');" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    
		    dim strExec() as String
		    dim strOpts as String = "{"
		    
		    strOpts = strOpts + "on: " + Str( mValue ).Lowercase + "," + _
		    "click: " + If( mReadOnly, "false", "true" ) + "," + _
		    "drag: " + If( mReadOnly, "false", "true" ) + "," + _
		    "text: { on: '" + EscapeString( TrueText ) + "',off: '" + EscapeString( FalseText ) + "'}," + _
		    "width: " + Str( me.Width - 4, "#" ) + "," + _
		    "height: " + Str( me.Height - 4, "#" ) + "," + _
		    "animate: " + Str( AnimationTime, "#" ) + _
		    "}"
		    
		    'strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').val(" + Str( mValue ).Lowercase + ");" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_toggle').toggles(" + strOpts + ");" )
		    
		    strExec.Append( "window.eventTimer_" + me.ControlID + " = 0;" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_toggle').off('toggle').on('toggle', function (e, active) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'valuechange', [active]);" )
		    strExec.Append( "});" )
		    
		    'strExec.Append( "window.GSjQuery('#" + me.ControlID + "').css('visibility','" + if( me.Visible, "visible", "hidden" ) + "');" )
		    
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		    
		    RunBuffer()
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		GitHub README
		https://github.com/simontabor/jquery-toggles/blob/master/README.md
		
		Project Home
		http://simontabor.com/labs/toggles/
		
		MIT License
		Developed by Simon Tabor
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationTime
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationTime = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		AnimationTime As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFalseStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mFalseStyle ) then Buffer( "GSjQuery('#" + me.ControlID + " .toggle-off').removeClass('" + mFalseStyle.Name + "');" )
			  
			  mFalseStyle = value
			  
			  if not IsNull( mFalseStyle ) then Buffer( "GSjQuery('#" + me.ControlID + " .toggle-off').addClass('" + mFalseStyle.Name + "');" )
			  
			End Set
		#tag EndSetter
		FalseStyle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mFalseText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFalseText = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		FalseText As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isInit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAnimationTime As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFalseStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFalseText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReadOnly As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTrueStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTrueText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisible As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mVisualStyle As Styles
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mReadOnly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReadOnly = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		ReadOnly As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mTrueStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( mTrueStyle ) then Buffer( "GSjQuery('#" + me.ControlID + " .toggle-on').removeClass('" + mTrueStyle.Name + "');" )
			  
			  mTrueStyle = value
			  
			  if not IsNull( mTrueStyle ) then Buffer( "GSjQuery('#" + me.ControlID + " .toggle-on').addClass('" + mTrueStyle.Name + "');" )
			End Set
		#tag EndSetter
		TrueStyle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTrueText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTrueText = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		TrueText As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  if isShown and LibrariesLoaded then
			    Buffer( "window.GSjQuery('#" + me.ControlID + "_toggle').data('toggles').toggle(" + Str( mValue ).Lowercase + ");" )
			  end if
			  'UpdateOptions()
			End Set
		#tag EndSetter
		Value As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mVisible
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mVisible = value
			  
			  if value then
			    MakeVisible()
			  Else
			    MakeInvisible()
			  end if
			End Set
		#tag EndSetter
		Visible As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mVisualStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec as String = "window.GSjQuery('#" + me.ControlID + "').removeClass( 'toggle-dark toggle-iphone toggle-light toggle-modern toggle-soft' )"
			  
			  mVisualStyle = value
			  
			  dim strClass as String
			  
			  Select case mVisualStyle
			  case Styles.Dark
			    strClass = "toggle-dark"
			  case Styles.iPhone
			    strClass = "toggle-iphone"
			  case Styles.Light
			    strClass = "toggle-light"
			  case Styles.Modern
			    strClass = "toggle-modern"
			  case Styles.SoftFace
			    strClass = "toggle-soft"
			  End Select
			  
			  strExec = strExec + ".addClass( '" + strClass + "' );"
			  
			  if isShown then me.ExecuteJavaScript( strExec )
			End Set
		#tag EndSetter
		VisualStyle As Styles
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.toggle", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kInitializer, Type = String, Dynamic = False, Default = \"RSCustom.graffitisuite.webemailfield.<<var>>_change \x3D function(ev) {\r      var html \x3D RSCustom.graffitisuite.webemailfield.<<var>>.suggestion();\r      RS.triggerServerEvent(\"<<var>>\"\x2C\"suggestion\"\x2C[html]);\r};", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kStyle_DarkCSS, Type = String, Dynamic = False, Default = \".toggle-dark .toggle-slide {\r  border-radius: 5px;\r  box-shadow: 0 0 0 1px #242529\x2C 0 1px 0 1px #666;\r}\r.toggle-dark .toggle-on\x2C\r.toggle-dark .toggle-off\x2C\r.toggle-dark .toggle-blob {\r  color: rgba(255\x2C 255\x2C 255\x2C 0.7);\r  font-size: 11px;\r}\r.toggle-dark .toggle-on\x2C\r.toggle-dark .toggle-select .toggle-inner .active {\r  background: -webkit-linear-gradient(#1A70BE\x2C #31A2E1);\r  background: linear-gradient(#1A70BE\x2C #31A2E1);\r}\r.toggle-dark .toggle-off\x2C\r.toggle-dark .toggle-select .toggle-on {\r  background: -webkit-linear-gradient(#242529\x2C #34363B);\r  background: linear-gradient(#242529\x2C #34363B);\r}\r.toggle-dark .toggle-blob {\r  border-radius: 4px;\r  background: -webkit-linear-gradient(#CFCFCF\x2C whiteSmoke);\r  background: linear-gradient(#CFCFCF\x2C whiteSmoke);\r  box-shadow: inset 0 0 0 1px #888\x2C inset 0 0 0 2px white;\r}\r.toggle-dark .toggle-blob:hover {\r  background: -webkit-linear-gradient(#c0c0c0\x2C #dadada);\r  background: linear-gradient(#c0c0c0\x2C #dadada);\r  box-shadow: inset 0 0 0 1px #888\x2Cinset 0 0 0 2px #ddd;\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kStyle_iPhoneCSS, Type = String, Dynamic = False, Default = \".toggle-iphone .toggle-slide {\r  border-radius: 9999px;\r  box-shadow: 0 0 0 1px #999;\r}\r.toggle-iphone .toggle-on\x2C\r.toggle-iphone .toggle-off {\r  color: white;\r  font-size: 18px;\r  font-weight: bold;\r  text-shadow: 0 0 8px rgba(0\x2C 0\x2C 0\x2C 0.5);\r}\r.toggle-iphone .toggle-on {\r  border-radius: 9999px 0 0 9999px;\r  background: #037bda;\r  box-shadow: inset 2px 2px 5px rgba(0\x2C 0\x2C 0\x2C 0.4);\r}\r.toggle-iphone .toggle-on:after {\r  background: -webkit-linear-gradient(#1189f1\x2C #3797ef);\r  background: linear-gradient(#1189f1\x2C #3797ef);\r  height: 50%;\r  content: \'\';\r  margin-top: -19%;\r  display: block;\r  border-radius: 9999px;\r  margin-left: 10%;\r}\r.toggle-iphone .toggle-off {\r  box-shadow: inset -2px 2px 5px rgba(0\x2C 0\x2C 0\x2C 0.4);\r  border-radius: 0 9999px 9999px 0;\r  color: #828282;\r  background: #ECECEC;\r  text-shadow: 0 0 1px white;\r}\r.toggle-iphone .toggle-off:after {\r  background: -webkit-linear-gradient(#fafafa\x2C #fdfdfd);\r  background: linear-gradient(#fafafa\x2C #fdfdfd);\r  height: 50%;\r  content: \'\';\r  margin-top: -19%;\r  display: block;\r  margin-right: 10%;\r  border-radius: 9999px;\r}\r.toggle-iphone .toggle-blob {\r  border-radius: 50px;\r  background: -webkit-linear-gradient(#d1d1d1\x2C #fafafa);\r  background: linear-gradient(#d1d1d1\x2C #fafafa);\r  box-shadow: inset 0 0 0 1px rgba(0\x2C 0\x2C 0\x2C 0.6)\x2C inset 0 0 0 2px white\x2C 0 0 3px rgba(0\x2C 0\x2C 0\x2C 0.6);\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kStyle_LightCSS, Type = String, Dynamic = False, Default = \".toggle-light .toggle-slide {\r  border-radius: 9999px;\r  box-shadow: 0 0 0 1px #999;\r}\r.toggle-light .toggle-on\x2C\r.toggle-light .toggle-off {\r  font-size: 11px;\r  font-weight: 500;\r}\r.toggle-light .toggle-on\x2C\r.toggle-light .toggle-select .toggle-inner .active {\r  background: #45a31f;\r  box-shadow: inset 2px 2px 6px rgba(0\x2C 0\x2C 0\x2C 0.2);\r  text-shadow: 1px 1px rgba(0\x2C 0\x2C 0\x2C 0.2);\r  color: rgba(255\x2C 255\x2C 255\x2C 0.8);\r}\r.toggle-light .toggle-off\x2C\r.toggle-light .toggle-select .toggle-on {\r  color: rgba(0\x2C 0\x2C 0\x2C 0.6);\r  text-shadow: 0 1px rgba(255\x2C 255\x2C 255\x2C 0.2);\r  background: -webkit-linear-gradient(#cfcfcf\x2C #f5f5f5);\r  background: linear-gradient(#cfcfcf\x2C #f5f5f5);\r}\r.toggle-light .toggle-blob {\r  border-radius: 50px;\r  background: -webkit-linear-gradient(#f5f5f5\x2C #cfcfcf);\r  background: linear-gradient(#f5f5f5\x2C #cfcfcf);\r  box-shadow: 1px 1px 2px #888;\r}\r.toggle-light .toggle-blob:hover {\r  background: -webkit-linear-gradient(#e4e4e4\x2C #f9f9f9);\r  background: linear-gradient(#e4e4e4\x2C #f9f9f9);\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kStyle_ModernCSS, Type = String, Dynamic = False, Default = \".toggle-modern .toggle-slide {\r  border-radius: 4px;\r  text-shadow: 0 1px 1px rgba(0\x2C 0\x2C 0\x2C 0.25)\x2C 0 0 1px rgba(0\x2C 0\x2C 0\x2C 0.2);\r  background: -webkit-linear-gradient(#c0c5c9\x2C #a1a9af);\r  background: linear-gradient(#c0c5c9\x2C #a1a9af);\r  box-shadow: inset 0 2px 1px rgba(0\x2C 0\x2C 0\x2C 0.2)\x2C inset 0 0 0 1px rgba(0\x2C 0\x2C 0\x2C 0.15)\x2C 0 1px 0 rgba(255\x2C 255\x2C 255\x2C 0.15);\r}\r.toggle-modern .toggle-on\x2C\r.toggle-modern .toggle-off {\r  -webkit-transition: all 0.1s ease-out;\r          transition: all 0.1s ease-out;\r  color: white;\r  text-shadow: 1px 1px rgba(0\x2C 0\x2C 0\x2C 0.1);\r  font-size: 11px;\r  box-shadow: inset 0 2px 0 rgba(255\x2C 255\x2C 255\x2C 0.2)\x2C inset 0 0 0 1px rgba(0\x2C 0\x2C 0\x2C 0.2)\x2C inset 0 -1px 1px rgba(0\x2C 0\x2C 0\x2C 0.1)\x2C 0 1px 1px rgba(0\x2C 0\x2C 0\x2C 0.2);\r}\r.toggle-modern .toggle-select .toggle-off\x2C\r.toggle-modern .toggle-select .toggle-on {\r  background: none;\r}\r.toggle-modern .toggle-off\x2C\r.toggle-modern .toggle-off.active {\r  background: -webkit-linear-gradient(#737e8d\x2C #3f454e);\r  background: linear-gradient(#737e8d\x2C #3f454e);\r}\r.toggle-modern .toggle-on\x2C\r.toggle-modern .toggle-on.active {\r  background: -webkit-linear-gradient(#4894cd\x2C #2852a6);\r  background: linear-gradient(#4894cd\x2C #2852a6);\r}\r.toggle-modern .toggle-blob {\r  background: -webkit-linear-gradient(#c0c6c9\x2C #81898f);\r  background: linear-gradient(#c0c6c9\x2C #81898f);\r  box-shadow: inset 0 2px 0 rgba(255\x2C 255\x2C 255\x2C 0.2)\x2C inset 0 0 0 1px rgba(0\x2C 0\x2C 0\x2C 0.2)\x2C inset 0 -1px 1px rgba(0\x2C 0\x2C 0\x2C 0.1)\x2C 1px 1px 2px rgba(0\x2C 0\x2C 0\x2C 0.2);\r  border-radius: 3px;\r}\r.toggle-modern .toggle-blob:hover {\r  background-image: -webkit-linear-gradient(#a1a9af\x2C #a1a9af);\r  background-image: linear-gradient(#a1a9af\x2C #a1a9af);\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kStyle_SoftCSS, Type = String, Dynamic = False, Default = \".toggle-soft .toggle-slide {\r  border-radius: 5px;\r  box-shadow: 0 0 0 1px #999;\r}\r.toggle-soft .toggle-on\x2C\r.toggle-soft .toggle-off {\r  color: rgba(0\x2C 0\x2C 0\x2C 0.7);\r  font-size: 11px;\r  text-shadow: 1px 1px white;\r}\r.toggle-soft .toggle-on\x2C\r.toggle-soft .toggle-select .toggle-inner .active {\r  background: -webkit-linear-gradient(#d2ff52\x2C #91e842);\r  background: linear-gradient(#d2ff52\x2C #91e842);\r}\r.toggle-soft .toggle-off\x2C\r.toggle-soft .toggle-select .toggle-on {\r  background: -webkit-linear-gradient(#cfcfcf\x2C #f5f5f5);\r  background: linear-gradient(#cfcfcf\x2C #f5f5f5);\r}\r.toggle-soft .toggle-blob {\r  border-radius: 4px;\r  background: -webkit-linear-gradient(#cfcfcf\x2C #f5f5f5);\r  background: linear-gradient(#cfcfcf\x2C #f5f5f5);\r  box-shadow: inset 0 0 0 1px #bbb\x2C inset 0 0 0 2px white;\r}\r.toggle-soft .toggle-blob:hover {\r  background: -webkit-linear-gradient(#e4e4e4\x2C #f9f9f9);\r  background: linear-gradient(#e4e4e4\x2C #f9f9f9);\r  box-shadow: inset 0 0 0 1px #ddd\x2Cinset 0 0 0 2px #ddd;\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAuGSURBVHhe7d1/bBv1FQDw9852EjtpYVXLfrHBVkpb/ikTiDZQYFBAlYDEKRStHV0RYhsagtYJZbQbY2FSMgaNU5CYNASMClqNAolDtaHxc/xKOzRRBnRJ12k/yhgbPwo0sZPGvrd3yWNi9d3Zjp0f9+V9pNbf92I79t27733PufsalFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFJKBQ3K7ZSb0bBufhZDCxFpIQEuBKBjkKCWEOqQsFbuFggEkEWEQQIa4GCA23/l9l6yQ3sjVuSNQ90/fU/uOuWmrABi8ZZTbKCzLYClvHDORMDZ8iPj8fvdwwv+eX7/LwyF8TF4OJmRH026SS2AuuXXzslFw6t4i17N4eKx7KfeMBfEdgtx22BXxxOSmzSTUgAzL03MGsnCRt7Kr5eUckV/IIL2TCr5iCQm3IQXQCye2Mg3/A9njGVUIUT0hMWFMNiTfEZSE2bCCiDamPguP/lGQDxOUqpk9CsMhdoGH7n9j5KouIoXQE1Dy1lo2Z3c3X9NUqpcRD9Pp5Lfk6iiKloAscaWNYC0VULj8eHeB3youo9v93H0JqeGEXGIf8ADfKohy6rm2zm8TObxBjGPf/7F0QeOA48NfmtZuGawa/N/JFURFSuAWGPzTfxst0hoLN4/H0Sg2wnxyUx38veSLkr1xS1fCVm0jAviPF70KzgVGftJ0fYT0ppMV3KXxGWrSAHw/v4ervwrJTQQHeAt8N4w2dsP9Wzpl2R5zm+pjdXaqwlwFa+EcyRbGJHN918zmEpuk0xZyiqA2MrrP0cjuQe4e1smKcNQmhdRW3rRjHZobbUlWXHRpuaVvNvYxCvjZEkVRvCjdKrjJxKNW3kFEE/8jp/iLAlNk7SGRtoHHr/zHYknXDTecjWS/QM+cjpWUv4INnAR3C7RuIy7AKLx5vv5wd+S0CDUHbLDNx7qua0yXX3JCGMNPJ6ysFUSvnhMsILHBF0SlmxcBRCNJ27hbv8mCY3Bo/lbM90dN0o4pWobE874YCuvoZCk3BEMgEVL013JVyVTkpILIBZvdgZ794xFBrHpynRP8j6JpoVow7rFYIWcnna+pFzxAPXVTNXQUthx14CkimbJbVFqG5udwZ5ZK5/ozzbBGdNt5TsyPVt2h6HqDO6Zfi0pV4iwKHa4Zlyfv/h3L0eILKh/kvuMz0gYeM4HOaEQnpfu7nhFUtPO4b4XMtm+3m2R+fXn8LI/XtL5EBbyfQZH+ntfkkxRiu4BYo3NN/Av+aqEhrCvGHi0o0+CaY2PQddwyR6Q0BUX9EZo2FDSH92KKoBZy6+dSej8Rc8gBN/PdHemJJr2hlIdB5As36Mu3hXMimKupPVUVAFkaiLOhxRHSxh4RHQvHz//TMLAGExtfpZf/FUSuuIi2FizoqXov8AWHAM4n19bFuyQMPiI9mVSyfMlCpyR/l2vROYvmcNr+jRJ5bMplu3ftVMiXwV7AMsis7p+wHZpBBZRuI3/z0qYBxG/E7ukuag/x/sWQF1Dy0ncpXxbwuAj6OWu/5cSBVam57a3iJCLwEe2uDGbbwHk0HZO3jQGgR34rf9jmeGRNh71/1vCfAgrnZNwJfLkWwAIuEqagccDv52ZVOdjEgbf43cOA5JvL+CcgS1NT54FEGtKLDfpuJ+AAjfqLyTTlbyDb/45FuVDgoI9uHcPYENgR8pH4q7ytaFU5/MSmsbnFHJcXHPRdV+WwJXPLgCXSiPwEKAiZ89MR7aFvtcQWKHwudJ05VoAzid/vNS8jzMDBkO2Ofv+Iww9uvk5IviXhHkIqfQCGI5GjLlsiwd/bw8+0vmGhKZ6Sm7z8DjA94wt1wKwgRZKM/AQ4QVpGot3cZ4FwAvgOFh+bbVEeVwLAG3n8mxjvCa3BsvtkYaruqqqudLM4zUINKYAbNu5aMNs6VzG9z3mEEougC/IbeBZgFN0cuck2vmLNA92/i5RHgT7GGnmcS0APm6uk2bg2WF8X5pGI4SD0sxDiJ7r070HQDKmAKpz2Q+laTQk9HyfPEgsrQD8HhA0H5589EfSNBuS5/vkQ+ESewAeV8ht4M3a/b4xxeyHyLub50NhzzmI3McABIPSDLxMNDpTmoajo6SRxwb0vF7AYxfg/YCgsSBnzLmMfhDRs9D5SKi0AmDvym3g2WA7EzOYbeVDzrmdJ4wF+QjJ8wjBfReAtFeagYc2nChNY9WNvOj7HjEH+6WZx2MXAMYUAB8D+15XZ4IchH0/uY1CqLQCANugHoDI1PkL/geBLpCmC3rrvZ7bDkmQx70HCIVelmbwIc6tXrHesEvajkC+Z2/5XivoWgDOTFQENGFz0002K2ddKE3jRBvWn8pdgGeBE+DT0nTlvgtg3K0Y83d0HtMYc3ZzHsRLpOUqbOfGVwBE/pUTKAj1sYbirpQJGi5uzwIggjcKzWrmWQDOhMVEVNFJCacSWZSQpjFq44nLuQfw/JyDi6PgybCeBeDgJ9guzcBDwDU18XX1EhrBHp2E21sOcuUVAKFZp1NbYE2LCaAqIdrUfA0X9UkS5uHe+zfDqS1/k9CTbwGMToVKBl0aDtgwesVT0N18s4W2/9ZvWVTU/IG+BTCKwJgLKkfZGPheILbn0I28T/OeeJroocGuzqIG8QUniBjp7307sqD+89w8dSwTcAjHR+bX2/y+npNMoNQ2tTgXetw/Fnmw4Jsjfbu8rxz+hMI9ALOzWf9r0YMG4ZbaeHPgPhuojl8/17bpAQndOd8tUMKkkUUVwNDOO/5BUGBCgoAhoK3ReCI4l7+tfCgUIvsBRHB6Yw9EdokzoBRVAI5MJNNOBAadYYth/rfV+UIrSUxrsZHeB7nnWiKhK14/bc5sYhIWpfiJIve+fDi8sP6A3ydPQcPvZbadwyXWgtOezfXv/kDS0060IXE3IhaamPvFTCp5ubSLVtJModm+3td5QOhMTuR7xWmg8KCQF+43quaf/joPDP8i2Wkh2nTDsZEFi7v59fludLzlv8Njg4tz+3aV3EOXVACOkb7e58ML6o/jrceYz9YRsI7/uzx84pKD2f5duyU9pWobW74OkNvJr22RpDwh0GVDPclxvW5ej+MTa2x+lh99toQmuSudnbEBdrZO2anxvGyv4mV7t4S+CGB9prtji4QlG3cB1DQ2f8kCegmK/XaLAOEjhHfBttoyPZuTkpoUsaaWC4HsTbxaTpdUIXeluzuukfa4jLsAHE43RWg/yE9jzMWkn8SF0M+HXW18XD2hX4UXbUosQUJe8XDxWKYIRNvTqWTZ0/iVVQCO6sZ1x4cw5CygM8cyJuKeDvCe9FEztsH9rUOSLFssvv4CIlzNg7y1kioKEbXyiP/HEpal7AL4WDSeuI8HLFdIaCaiId7nbnO+MzCE+FTpX+JIWNuwYZmNuWW80i/lhOe5/O4oiwRrK/WVcY6KFYAjFk/8kJ+y7K8yCwouhj1ItI8Q+nkk/ibYOGQjDnN3nrMsu4a37hpAmkOA8/h+J/J46RR+WM3Yo0vDW/2fwLLWZro2V/SE3YoWgKMm3ryKF8Yd3BvMlpQqF0Gquiqy9uCOWyt+qXvFC2DURTfHoqGPNvEAyhnRTszv+FQYHXu080i/qKnfx2NCV44cKm7iGrhaUqoIfPTRhza0p3sm9ujDMSlbZ6wpsQhs7g0QL5OUcuF8pMv/t/EIv1NSE25Su2fnIga00PlCxNX8iz8r6U89Hkw+w6P7bekIPQgPJz0nc5gIU7Z/jsZbVvDI+Hx+++dyz2D8Fbz/h8jmI4eneaD8lGVB91R+c9mUFcAnVV143bxIVfg0PtSZy1vDCfyi5vJmMZtfXR0fStVyHKxpXhCy/F4G+HUPEOIAvxfnz+j7eaXvR6T+dHjm07Cj9bDcWymllFJKKaWUUkoppZRSSimllFJKKaWUUkoppZRSSimllFKqSAD/BYCOjN4r8cxXAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEoSURBVDhP7ZHPKkRxFMfPuXfIpMbK1k5hthR5AJSwtlGTspNhZcmWmsnSZB6AzZBCeQDJlqyNJ6DGyNz7db4/v9zr4gGUz+L8/Z1zzz1H/j7qtSM/tzaqgS569xuAPiPqHLRP9x58KGmQXyjXVHTZuwnAI0Tr0PhWERRVULKq7VajUmPaNeidL29AdZd2Ggju0Ilm0l/smV0d0Fx4JjGWXk6qNwGDsUrJZTNYg610MaEfQOoS6Ap918BGH6HO0m5UD735hUjRVGiRtmsAkSfqLFyqN3+iSfExAeScOstvF7GJbZG4pB06MTh2bf80ZYl++gk60TU03vd2f3VBjwvsHp7ct3evrePKpntBQZgMwtwOVKYtWPDhT3gRm7RgFUd2wnUf/kdE3gF5yF1wf3VQYgAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Styles, Type = Integer, Flags = &h0
		Dark
		  iPhone
		  Light
		  Modern
		SoftFace
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AnimationTime"
			Visible=true
			Group="Behavior"
			InitialValue="250"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FalseText"
			Visible=true
			Group="Behavior"
			InitialValue="OFF"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TrueText"
			Visible=true
			Group="Behavior"
			InitialValue="ON"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VisualStyle"
			Visible=true
			Group="Behavior"
			Type="Styles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Dark"
				"1 - iPhone"
				"2 - Light"
				"3 - Modern"
				"4 - SoftFace"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
