#tag Class
Protected Class GraffitiWebSignature
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "SignatureChanged"
		      if Parameters.Ubound >= 0 then
		        SignatureData = Parameters(0)
		        SignatureChanged
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  if LibrariesLoaded then
		    'me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_canvas').attr('width','" + Str( me.Width, "#" ) + "');")
		    'me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_canvas').attr('height','" + Str( me.Height, "#" ) + "');" )
		  end if
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id=""" + me.ControlID + """ style=""padding: 2px 2px 2px 2px;background:#fff;"">" )
		  source.Append( "<canvas id=""" + me.ControlID + "_canvas"" width=""100%"" height=""100%"">" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Clear()
		  Buffer( "window.gwsigpad_" + me.controlid + ".clear();" )
		  mSignatureData = ""
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearBackgroundImage()
		  BGPicture = ""
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColorToString(c as Color) As String
		  Return "rgba(" + _
		  Str( c.Red, "###" ) + "," + _
		  Str( c.Green, "###" ) + "," + _
		  Str( c.Blue, "###" ) + "," + _
		  Str( 1 - (c.Alpha / 255) ) + ")"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebSignature -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwsig.min.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebSignature -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Load(ImageData as String)
		  dim strData as String = ImageData
		  if InStr( strData, "," ) = 0 then
		    strData = "data:image/png;base64," + strData
		  end if
		  
		  Buffer( "window.gwsigpad_" + me.controlid + ".fromDataURL(""" + strData + """);" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBackground(theImage as Picture)
		  SetBackgroundSRC( "data:image/png;base64," + ReplaceLineEndings( EncodeBase64( theImage.GetData( Picture.FormatPNG ) ), "" ) )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBackground(base64Image as String)
		  SetBackgroundSRC( base64Image )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBackground(theImage as WebPicture)
		  if not IsNull( theImage ) then
		    SetBackgroundSRC( "data:image/png;base64," + ReplaceLineEndings( EncodeBase64( theImage.Data() ), "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetBackgroundSRC(theSRC as String)
		  BGPicture = theSRC
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    strExec.Append( "var sigDiv = window.GSjQuery('#" + me.ControlID + "');" )
		    if BGPicture.LenB = 0 then
		      strExec.Append( "sigDiv.css('background-color', '" + ColorToString( mBGColor ) + "');" )
		    else
		      strExec.Append( "sigDiv.css('background-image', 'url(" + BGPicture + ")');" )
		    end if
		    
		    strExec.Append( "var sigcanvas = document.querySelector('#" + me.ControlID + "_canvas');" )
		    strExec.Append( "window.gwsigpad_" + me.controlid + " = new SignaturePad(sigcanvas, {" )
		    strExec.Append( "backgroundColor: '" + ColorToString( mBGColor ) + "'," )
		    strExec.Append( "penColor: '" + ColorToString( mPenColor ) + "'," )
		    strExec.Append( "onEnd: function () {" )
		    strExec.Append( "var sigData = window.gwsigpad_" + me.controlid + ".toDataURL();" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'SignatureChanged', [sigData]);" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery(window).bind('resize', function () {" )
		    strExec.Append( "var sigData = window.gwsigpad_" + me.controlid + ".toDataURL();" )
		    strExec.Append( "var ratio =  window.devicePixelRatio || 1;" )
		    strExec.Append( "var canvas = window.GSjQuery('#" + me.ControlID + "_canvas')[0];" )
		    strExec.Append( "if (typeof canvas !== 'undefined') {" )
		    strExec.Append( "canvas.width = canvas.offsetWidth * ratio;" )
		    strExec.Append( "canvas.height = canvas.offsetHeight * ratio;" )
		    strExec.Append( "}" )
		    strExec.Append( "var sigData = window.gwsigpad_" + me.controlid + ".fromDataURL(sigData);" )
		    strExec.Append( "}).trigger('resize');" )
		    
		    Buffer( strExec )
		    
		    RunBuffer()
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SignatureChanged()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/szimek/signature_pad
		
		Licensed under MIT
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBGColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBGColor = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		BGColor As Color
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private BGPicture As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mBGColor As Color = &c000000FF
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mBorderColor As Color
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPenColor As Color = &c000000
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSignatureData As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPenColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPenColor = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		PenColor As Color
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSignatureData
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSignatureData = value
			End Set
		#tag EndSetter
		SignatureData As String
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.signature", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAj6SURBVHhe7d19jBx1GQfw5ze7x12XN7UQxTd8QUCNLzFKqgnRKCaF3nFbaEVjfInGGGMq3FKwQgyJMYiUmz1KjKYxJPyhgCC3e+UE00KFVkqxWsGXRG2MMfhuipHc7r3tPD6/mWeLQ+9mZ29nfvOb3eeTkJ3n6bV33PfZmd/syywIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCiPxTfCt6tG5zZSt4uAmUeheV5wLCX5TCZwDwoUZt6q7gq+wjA9CrrTedUlp6/nu0tSVorASfaKH38YX67X/ihjVkAHpw5qYdL10aWniQfo3v41aUP6vW8tjcnl20V7CHDMAadRl+APFZcGC0MV19mjuZkwFYgzWF30ZrA4DWaKN++y+5kykZgC754RcXfkyLvfdwaw3wr+Cp0caMe5QbmZEB6IIOf3Fo4VEF6p3cWjNE+JtyCnQ42PkLbmVCBiAmP/zi4gGl4K3c6hkC/p1OHceaM1NHuGWcDEAMwT1/8RD9si7gVmIQ4B90OBlrTk/+jFtGyQB0EBzzF4/Qb+oN3EocIv6L/v3RZq36FLeMkQGIcMaWiZctLwOt1tVruJUaOhz8GzxntDkzeZhbRjh8K1awtAS/MRG+RgvLs0B5s+s2T2zglhEyABEcxE/wphFKqfV0ejg7Ut7+Xm6lTg4BHZx6+TWXoOPs5dIIWhg+R6eJo/N19wlupUYGIIaMhuA/9N/YfM09yK1UyCGAnTZaOas0PvF9LkPmZqb2Kc/7MJdG0D3zJQphdl154iJupUIGgOjwW0X4NR2EP1Yav+ZubodkMgQKzgBUd8KnbhrhVuIGfgDa4dM97uV+QzkfLZUrqw8BwiVcGqEfeSw99/y1XCZuoAfgpPBfQEMwcQ9vh8zV3UdMDwEo/CRvJW5gByAifKauKo1X7uUixPgQKHX+cHnbG7lK1EAOQOfwmYKP0BD8gKsQ00NQ9Iqv5c1EDdwA+OEX8Fcdw29TsNWGIcACHOfNRA3UAAThwzNKqVdwKx49BOXKfVyFGBkCxMXG2XO/5SpRAzMAL4QP53CrW1toCO7n7ZD0h0DVYffuJS4SNRADoMP3ivh0D+G3XVkan/ghb4ekOQTooMubiev7AQjC95/SfSW3ohxDT20YmV86ExFXfqmWUlfQmsDYEKCHVzenq09ymbi+HgA//IIOH14VdCIda7XwUv18/PGH7/gvJf1P7p9MwRV0OHiAq5BkhwC/2pyp7uIiFX07ABz+UQordvgLe6rHdDEyXrmKDhcb/T9Z3eZSeWKat0OSGAJU8LVGrfp1LlNDZ0P9JwgfKXz1am5FOSl8R8GKjwKuCKHeqLtlrkJOHa98iILcx2VsdPi5pVmvfoXLVPXdAPCCj47fsV7J01v4bUkOAaLbqFdTe+z/xfpqAHjB93PajPOoWTLhn4AztMse5yIk/hDgHfRvfIkLI/pmAPieT+GrDMI/YU+j5l7O2yEdh0DBdxrT7he4MqYvBoCP+UfomH8ut6KkFb6Pjt8P0vF7jMuQ1YYAEXY36+7nuTQq9wPAu339porXBZ1Ix5aXli9bnN31B10kHX5bN0OAgN9t1qqf49K4XA+AjeG3IcBss+aOchnSHgIalDtpUD7L7UzkdgD4mP8U/S+8nltRjIbfRkPwIxqCTVyG6Nf6ZfFOoBfL5QDkIfw2upc/RPfyy7i0Tu4GIFjwwWH6yeO8Vy/T8NtokUdD4Fo5BLkaAA7/Sfqp47w8yorw22gIHqYhuJRLa+TmuYAgfMxl+D6l32don1wMQHDMh0N0nh8vfMeu8GkxOEmLwe1cWsX6Q8CJ8AHOCzqRgvAfsCl8j8KfsjJ8zeo9AO/29Rskcxo+3mZz+Jq1A8Dh/5R2+2/iVhRLw69ex6W1rBwAHX4rCP98bkWR8Htg3RrAD78IB+kHi3NBJvvCR9hJp3vXc2k9q/YAQfh4QMI3x5oB4N0+ha8u5FYUCT8hVgxAED48rlROwwe8NY/ha5kPAN/zKXx4M7ei2Bl+rfplLnMn00UgH/Mfo93+W7gVRcJPQWZ7gNM2bjubVvs/iR9+QcJPQSYD4Ic/PLSfdj9xLrzM4e+0KHz4Zj+ErxkfgCD84qP62jfcimJp+O4OLnPP6BrAD39k6BH6pm/jVhQJ3wBje4Dch4/qln4LXzOyB+Dw99E3ezu3Inmeev/8zOTjetuO8M29V8+01PcAfMyPHb6mCoU/6lsJP32p7gGC8If20oLvHdyKB/H3COpZ+nsf5E4mEOEbzbp7A5d9KbUBCB7kgb30DXr+gKUsDEL4WiqHgNPLO9a3ihgrfFTwGG9aY1DC1xIfgNPLX1y/DAsUfpyPVsMbmtPuB+gXnvqVMOIapPC1At8mpnDBxQdV8AnakRTi9ka9eqveXv7dof3FCzecQkNzsf+HGaF1x80U/o1cDoTE9wC0cOsYPipVmatXJ7n0NWvVG/W9j0vjEPDmZm1yoMLXjD0Q1KbDb05PVrkM0btefdrFpTH+PZ8GkMuBYnQAosJv0+fc+pk2LlPnH/MH8J7fZnYA/E/O7kw/00Zfu5PL1Azagm8lRgeg4LWGeLMjCuZ62hPcxmXiJPyA2TWAQ0vELtCe4DpKKvHr5AYP70r4mvFFYLeCa+Zh5LqhG/3+2H63rB8ArVGrVuhmKqjWzn9KV8IPSfy5gFK5grx5MsRveaAOcBVScOD43LQb+eGMpfFrp0Dh1Vx2pR9fzJEEswMQgXbNd9G989NcrqpUnthFP/Y2LmOR8FeXi0PA//MvpYr4bS47kvCj5W4AYOO2YVAq1id4Bi/dlvCj5GsAKPx1w8WjtNXxghH98rr9tOVnAPzwhw4rpTq+hUzCjy8fA0Dhl0aGDsZ5aZl+CFnCj8/+AdDhDxf309a7g8bq/PBz+i7drFhzGtgrCX9t8ncWsAL9pJGEvza5HwA//JxckMlGuR4ACb93uR2A4AqcEn6vcjkAtMq0+vKreZLHAbD2wst5lMZp4Gd4s3sencwpheCgB56DSnlBTf2W41APcb7u3stfLYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCF8AP8DudmSEguToGEAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAD7SURBVDhPYxhwwAyl8QKugAI3ZhWz339unfwIFYIDJiiNE3D7FxYDle1kZGHezuGTJwcVhgO8BnAEFIT9Z2A0A7EZGRi1QIaAJZAATgNANjP9Z+wFc/4zrALTDIwy6K7AagBI8z9GhqT/jAxXgVaHgcT+M/yfw8jwL/THlkmPwIqggBFKwwFMM5D5GOhsd5DYfwaGTwz//jl/3zThDIiPDFBcgEszyGZsmkEAbgBI839Gxh7G/4wf0TV/2zBhF1gRFgA3AKi44P+/f6b/Gf/zQ/kENYMAwgBGRj4GJsZ0YGA1EqsZBOAGMP5nuAp0vjbTf6YCYjUPC8DAAAAvfl5JaenSnAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="BGColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PenColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SignatureData"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
