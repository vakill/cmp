#tag Class
Protected Class GraffitiWebSlideshow
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "slideclick"
		      if Parameters.Ubound >= 1 then
		        dim strURL as String = Parameters(0).StringValue
		        dim theTime as Integer = Parameters(1).integerValue
		        if theTime = instanceTime then 
		          dim theSlide as Integer = GetSlideByURL( strURL )
		          SlideClicked( theSlide, strURL )
		        end if
		      end if
		    case "afterchange"
		      if Parameters.Ubound >= 1 then
		        dim strURL as String = Parameters(0).StringValue
		        dim theTime as Integer = Parameters(1).integerValue
		        if theTime = instanceTime then 
		          if strURL <> lastChange then
		            mCurrentSlide = GetSlideByURL( strURL )
		            lastChange = strURL
		            SlideChanged()
		          end if
		        end if
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id=""" + me.ControlID + """ style=""overflow: visible;"" class=""gwslider_container"">" )
		  source.Append( "<ul class=""gwslider"" id=""" + me.ControlID + "_slider""></ul>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddSlide(theSlide as Variant, theCaption as String = "")
		  if IsNull( theSlide ) then Return
		  
		  dim theURL as String
		  select case theSlide.Type
		  case Variant.TypeString, Variant.TypeText
		    theURL = theSlide
		    if theURL = "" then Return
		  case else
		    if theSlide isa WebPicture then
		      dim wp as WebPicture = WebPicture( theSlide )
		      
		      if IsNull( wp ) then Return
		      
		      theURL = wp.URL
		      myWebPics.Append( wp )
		    elseif theSlide isa Picture then
		      dim p as Picture = Picture( theSlide )
		      
		      if IsNull( p ) then Return
		      
		      theURL = "data:image/png;base64," + ReplaceLineEndings( EncodeBase64( p.GetData( Picture.FormatPNG ) ), "" )
		    elseif theSlide isa WebFile then
		      dim wf as WebFile = WebFile( theSlide )
		      
		      if IsNull( wf ) then Return
		      
		      theURL = wf.URL
		      myWebFiles.Append( wf )
		    elseif theSlide isa FolderItem then
		      dim wf as WebFile = WebFile.Open( FolderItem( theSlide ) )
		      
		      if IsNull( wf ) then Return
		      
		      theURL = wf.URL
		      myWebFiles.Append( wf )
		    end if
		  end select
		  
		  SlideImages.Append( theURL )
		  Captions.Append( theCaption )
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSlideByCaption(theCaption as String) As Integer
		  dim intMax as Integer = Captions.Ubound
		  for intCycle as Integer = 0 to intMax
		    if Captions(intCycle) = theCaption then Return intCycle
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSlideByURL(theURL as String) As Integer
		  dim intMax as Integer = SlideImages.Ubound
		  for intCycle as Integer = 0 to intMax
		    if SlideImages(intCycle) = theURL then Return intCycle
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSlideCaption(theIndex as Integer) As String
		  if theIndex >= 0 and theIndex <= Captions.Ubound then
		    Return Captions(theIndex)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSlideURL(theIndex as Integer) As String
		  if theIndex >= 0 and theIndex <= SlideImages.Ubound then
		    Return SlideImages(theIndex)
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebSlideshow -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "responsiveslides" ).Child( "responsiveslides.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "responsiveslides" ).Child( "responsiveslides.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "responsiveslides" ).Child( "gwslideshow.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebSlideshow -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  ReDim SlideImages(-1)
		  Redim Captions(-1)
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveSlide(theSlide as Integer)
		  if theSlide >= 0 and theSlide <= SlideImages.Ubound then
		    SlideImages.Remove( theSlide )
		    Captions.Remove( theSlide )
		    
		    UpdateOptions()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if isShown then
		    ReDim UUIDs(-1)
		    dim strExec() as String
		    
		    dim strMyName as String = me.ControlID + "_slider"
		    
		    if AlreadySetup then
		      dim newHTML as String = "<ul class=""gwslider"" id=""" + me.ControlID + "_slider""></ul>"
		      'me.ExecuteJavaScript( "window.GSjQuery('#" + strMyName + "').unbind().removeData();" )
		      me.ExecuteJavaScript( "window.GSjQuery('#" +me.ControlID + "').empty().html('" + newHTML + "');" )
		    end if
		    
		    dim strHTML as String
		    
		    dim intCycle as Integer
		    dim intMax as Integer = UBound( SlideImages )
		    for intCycle = 0 to intMax
		      dim thisUUID as String = "gws_" + GenerateUUID()
		      UUIDs.Append( thisUUID )
		      
		      strHTML = strHTML + "<li id=\'" + thisUUID + "\'>" + _
		      "<img src=""" + SlideImages(intCycle) + """ style=""width:100%;height:100%;background-size:cover;"" alt="""" />"
		      if UBound( Captions ) >= intCycle then
		        if Captions(intCycle) <> "" then
		          strHTML = strHTML + "<p class=""caption"">" + EscapeString( Captions(intCycle) ) + "</p>"
		        end if
		      end if
		      strHTML = strHTML + "</li>"
		    next
		    
		    strExec.Append( "window.GSjQuery('#" + strMyName + "').html('" + strHTML + "');" )
		    
		    dim strClass as String
		    Select case NavigationStyle
		    case NavStyleCentered
		      strClass = "centered-btns"
		    case NavStyleTransparent
		      strClass = "transparent-btns"
		    case NavStyleLarge
		      strClass = "large-btns"
		    End Select
		    
		    if strClass = "" then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').removeClass().addClass('gwslider_container');" )
		    else
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').addClass('" + strClass + "');" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + strMyName + "').responsiveSlides({" )
		    strExec.Append( "auto: true," )
		    strExec.Append( "pager: false," )
		    strExec.Append( "speed: " + Str( mAnimationSpeed, "#" ) + "," )
		    strExec.Append( "timeout: " + Str( mSlideTimeout, "#" ) + "," )
		    strExec.Append( "pause: " + Str( PauseOnHover ).Lowercase + "," )
		    if NavigationStyle > 0 then strExec.Append( "nav: true," )
		    strExec.Append( "namespace: '" + me.ControlID + "'," )
		    strExec.Append( "after: function () {" )
		    instanceTime = Ticks
		    strExec.Append( "var instanceTime = " + Str( instanceTime, "#" ) + ";" )
		    strExec.Append( "var theSlide = this.find('img').attr('src');" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'afterchange', [theSlide, instanceTime]);" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + " li').click(function() {" )
		    strExec.Append( "var instanceTime = " + Str( instanceTime, "#" ) + ";" )
		    strExec.Append( "var theSlide = GSjQuery(this).find('img').attr('src');" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'slideclick', [theSlide, instanceTime]);" )
		    strExec.Append( "});" )
		    
		    me.ExecuteJavaScript( join( strExec, "" ) )
		    
		    AlreadySetup = True
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SlideChanged()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SlideClicked(slideIndex as Integer, slideURL as String)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/viljamis/ResponsiveSlides.js
		
		MIT License
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private AlreadySetup As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAnimationSpeed
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAnimationSpeed = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		AnimationSpeed As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private Captions() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCurrentSlide
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		CurrentSlide As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private instanceTime As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private lastChange As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAnimationSpeed As Integer = 300
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCurrentSlide As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mNavigationStyle As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPauseOnHover As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSlideTimeout As Integer = 5000
	#tag EndProperty

	#tag Property, Flags = &h21
		Private myWebFiles() As WebFile
	#tag EndProperty

	#tag Property, Flags = &h21
		Private myWebPics() As WebPicture
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mNavigationStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mNavigationStyle = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		NavigationStyle As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPauseOnHover
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPauseOnHover = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		PauseOnHover As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private SlideImages() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSlideTimeout
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSlideTimeout = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		SlideTimeout As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private UUIDs() As String
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.slideshow", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAicSURBVHhe7d1dbBzVFQfwe2Zm195dO4n5DEls2kZtQKJ94IGatkhVFUibONl1ZIrUVuoL6gMoH7sG0iIklKpSVRFsGqkviAqJipdaIjZp6AegfqAqLioIYldKVBAVfqCNeACUeGOPdw7nrs9WgO3dmd25szO75yc5e+/dmfFG5793PvbDSgghhBBCCCGEEEIIIYQQQgghhBBCCCFEpwC+bUludPw6F9zN3BWGWctq6cq2K++pJ590eahpTQcgky/eR2sfAITddGvzsIgQKnxDIfzerqiJS7+beJ+HAwkcgN780TtAWU8AqFt5SLQb4mVEOFZ+fuJXPOJboGdutjD+IwB4jop/Aw+JOABIU032pnYND7gXZv/Ao774DkCmULyNposZ7oo4Ahh2dg1/uHJhdpZHGrL41o9f8K2IMZqhJ9MHH/widxvyFYDewvh3QcE3uStizq5UHuFmQ74CAOjt5aZIAkDf9fJ1FpDNF9+iuWUndz8D30FUJy0PX+QBYZjn2Fl6Uo5T+e7hoTVoV/Dly6cen+fuhvwG4BJtMcfdTwP84eKpyWe4JyLSkz/yORvsd7i7BlTUXZdPTzR8Uvo7CNyo+AQRP+SmiNDSzC//w811ebaX5WZdQc4CRAeSAHQ5CUCXkwB0OX9nAYUScnMNVF6hPP1EU5eIewoP7LTRu4POJIboFwzSg1lQCO9WwHplafrE27yY2EAYdWnLDJDLF7+XKRTftJX3FkXwacrhcVBwr77VfT2u79fL8SrCkEgD0Df2k2szhdIZBHiWCv4VHl6Xvl8vp5fX6/GwCFlkAcjsLw577tIsTfOBLivr5fV6en0eEiGKJAADdx/bTL/paarmF3goGL0erV/djghVJAFYcpefAoCbuNsUvb7eDndFSIwHgI5UR6h8Y9xtEYxl8kf3c0eEwHgAENUhboYD4H5uiRCYDcDIo1kAdRf3QkFnB3v0drkrWmQ0ADn70m3cDFXO+uir3GyrzfkjW/oKR27mbiIZDQCiN8TNUOmrhtxsG118F+x5D+25vpFSSwe47WQ0AB4oIxdwTG3Xr837fjzggvUvam6nfZLtOepculD60uq9yWI0AGDBu9wMlant+rFprHiVm1qap0exjYe0lIM4lz542Pe7cePC7EGgwgVuhMzUduurFt+FzxZ/FUDa8Zy59L5khcBoAMqnJmdRYVOfWduI3p7eLncj01+4/2pd/AafiupxHOfNJIXA8AxAUP2RW+EIe3s+0EHeNSuYblT8VaAyTsp+IykhMB4A24afcTMUYW+vEV38iqOo+LCVh3yALM0ErychBMYDcOm5ifOI6qfcbYnejt4ed43rGzt0bbX4Sl3PQ/6B6rNT9mtxD4H5XQApz0w8SlP3b7nbHFRT1e1ERH/pRcV1mis+AwX9tuP8M84hiCQA2uIH/d9HxGe5G4hej9aP7N1Buvie583RtH8dDzWNjhs22Snn1biGILIAqL8eXynPTP6Ayvkw9ZZWBxui5fDh6nq0Po8Zldt/+HrPQ73Pb7n4NTSLbKHdwT/iGILoAsAWpyd/ji7upNO5E/TMXveza3q8ej8tp5fnYeOqxbdtfbQf+pVG2h0M0O7gbNxCQOFszNS7grXs3ge2Yg8O0oYG6dEswBIsLL5w4r98d2Syd9PjcCtzVKhreMgIfR2j4la+tnzm5L95qGlh1KXtAYiD7OjRG9ADvc+/moeMohnuYmWl8o1WQ5DYt4XHyWrxrciKr+njCzowfCUOu4OuDkDmwIPbqPh6nx9Z8Wv06aXt2H9rdwi6NgCZfcXtylrRxb+KhyJHM8FWJ2X/pZ0h6MoAZMao+CmlD/gGeKiNYJvjOH9uVwi6LgCZ0UM71ArMx6P4DNR2mglebkcIEhGAbL4YypdUZUYf2qEwRc98tYWHYgQGnZTzUtQhiH0AqsUHOJPJl17joab05kuDynP1tf0YFv//hhzHfjHKEMQ6ALXi6zYdrN3abAh6Rw4PUeH15d34f7QM4MYoQxDbAHyy+DXNhKB35NgQOA6d56tNPBR/EYYglgFYr/g1QULQe3D8RnCW9bSfnOLXUAhsxznJPWNiF4B6xa/xEwL9PXqWfklXQT8PJQ8gZdesWAXAT/Fr6oWg+iWKyp6jpZJbfA3pf2lYbAIQpPg164WgZ//4521lzdO5dR8PJViXzADNFL/mkyGoFt/25mhww282TRQwX5+2B6CV4tfUQkDF1x/a6Izia+jv5fpWtDUAYRS/RoeA/u2sj41DBx8DhFn8ztWhxwBSfH/oLLDzAiDFD6DTrgNkC0e+I8UPooNmgGy+tEcp+wXuCj8QjdcnkgDkRkt30glNoD9oKAh0wGlg7sDR3YjqT9wVQST9IDA3Ov4ttCwpfvOSHQBEvJ1ujP8nOlYn7AJECzr9UrBooBteDBL1dNkbQsSnYTe9IUSsI/mXgsHmhogpowFA5TncFDFlehcgM0DMyS6gy/k6yGj2q0gyheJj9Cv2cFcEhhfL05O7ubOGfEdQl5PvCBItkwB0OQlAl5MAdDl/AUBV5tYaFlgd8Bm85NFfa8vNdQFaV7hZl88ZAC9yYw3PUyU6Gh3hroiA/vM1aDmPcHcDlf9xoy6fp4HFKVq0/t//RfS4JUwDqP/EpRl7Mb3Qr6amKjyyIX8zgKdOc2tj+kHJTzQ/jZ32U3zNVwAWn598BpU6x10Rcwj4ODcb8nkMQFBF9udaRPMQ1WPl6clXuduQ7xdrVi6cPZ/adXuZjhru5CERMzRL/6Y8M3Efd30J9Gqde+Hs39M3Db9NzW/TTj+1OirioPrMD1h8LfDLte752XPpm7/+FP3KXtrZ7KAZIXlfwdY5XKrDGU/BvVdmJn7NY4G0/J6z3OjhW1TF2upZtgQhIqBwmf59bzHVN6emjlNbCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQ3U2pjwFWDJPiLGMmgwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAD6SURBVDhPYxhwwAgiOAIKwpgYGC3AIkSA///+L/u+acIZEJsJRDAyMNaDaGIBIyPTJCgTArj8i46BaM6AwtkgDBbEA2DqQQDsAhAAeQPokhQQBrGhwgwcPnlyXAGFfVAuBoAbwPDn34n/DP93gjCIDRICaWZkYd4OdHQh0NaVYHVoAG7Ajy2THn3f0O/x/8/fNBAbphnoIi2wAkaGMGyGIFwABDBNIIUommEAaAiy90AAEQbINgIVYmjGARAuYGGyIFYTMgAnJGDUXQX63RNkCFgUHwAGMBMzy4pvG/usQFyYAbMZ/zNqg9hEAcb/J75t6C+C8oY2YGAAAM8xTKqliJFmAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavStyleCentered, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NavStyleLarge, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NavStyleNone, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NavStyleTransparent, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AnimationSpeed"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CurrentSlide"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="NavigationStyle"
			Visible=true
			Group="Behavior"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - NavStyleNone"
				"1 - NavStyleCentered"
				"2 - NavStyleTransparent"
				"3 - NavStyleLarge"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="PauseOnHover"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SlideTimeout"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
