#tag Class
Protected Class GraffitiWebAccordionNavItem
	#tag Method, Flags = &h0
		Sub Constructor(ItemText as String, ItemNotify as Integer = -1)
		  mUUID = GraffitiControlWrapper.GenerateUUID
		  Text = ItemText
		  Tag = Lowercase( Text )
		  NotificationCount = ItemNotify
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ItemText as String, ItemNotify as Integer, ItemActive as Boolean)
		  mUUID = GraffitiControlWrapper.GenerateUUID
		  Text = ItemText
		  Tag = Lowercase( Text )
		  Expanded = ItemActive
		  NotificationCount = ItemNotify
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ItemText as String, ItemNotify as Integer, ItemChildren() as GraffitiWebAccordionNavItem)
		  mUUID = GraffitiControlWrapper.GenerateUUID
		  Text = ItemText
		  Tag = Lowercase( Text )
		  Children = ItemChildren
		  NotificationCount = ItemNotify
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(ItemTag as Variant, ItemText as String, NotifyCount as Integer = -1)
		  mUUID = GraffitiControlWrapper.GenerateUUID
		  Tag = ItemTag
		  me.Text = ItemText
		  NotificationCount = NotifyCount
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Children() As GraffitiWebAccordionNavItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Expanded As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Icon As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mUUID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		NotificationCount As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty

	#tag Property, Flags = &h0
		Text As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mUUID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		UUID As String
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Expanded"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="NotificationCount"
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mUUID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
