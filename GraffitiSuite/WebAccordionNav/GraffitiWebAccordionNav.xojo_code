#tag Class
Protected Class GraffitiWebAccordionNav
Inherits GraffitiControlWrapper
	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "expand"
		      if Parameters.Ubound >= 0 then
		        if not IsNull( expandedItem ) then
		          expandedItem.Expanded = False
		          HeaderCollapsed( expandedItem )
		        end if
		        
		        dim theItem as GraffitiWebAccordionNavItem = Item_FindByUUID( Parameters(0) )
		        if not IsNull( theItem ) then HeaderExpanded( theItem )
		        if not IsNull( theItem ) then theItem.Expanded = True
		        expandedItem = theItem
		      end if
		      UpdateStyles()
		    case "itemclicked"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebAccordionNavItem = Item_FindByUUID( Parameters(0) )
		        if not IsNull( theItem ) then SetItemClicked( theItem )
		        if not IsNull( theItem ) then ItemClicked( theItem )
		      end if
		    case "itemDblClicked"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebAccordionNavItem = Item_FindByUUID( Parameters(0) )
		        if not IsNull( theItem ) then ItemDoubleClicked( theItem )
		      end if
		    case "collapse"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebAccordionNavItem = Item_FindByUUID( Parameters(0) )
		        if not IsNull( theItem ) then HeaderCollapsed( theItem )
		        if not IsNull( theItem ) then theItem.Expanded = False
		      end if
		      UpdateStyles()
		    case "itemmousedown"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebAccordionNavItem = Item_FindByUUID( Parameters(0) )
		        if not IsNull( theItem ) then ItemMouseDown( theItem )
		      end if
		    case "headermousedown"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebAccordionNavItem = Item_FindByUUID( Parameters(0) )
		        if not IsNull( theItem ) then HeaderMouseDown( theItem )
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id=""" + me.ControlID + """ style=""overflow-x: hidden; overflow-y: auto;display:none;"">" )
		  source.Append( "<div id=""" + me.ControlID + "_wrapper"" class=""accnav"">" )
		  source.Append( "<ul class=""gwanmenu"">" )
		  source.Append( "</ul>" )
		  source.Append( "</div>" )
		  source.Append( "</div>" )
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddChildToItem(TheParent as GraffitiWebAccordionNavItem, TheChild as GraffitiWebAccordionNavItem)
		  if not IsNull( TheParent ) and not IsNull( TheChild ) then
		    AddItem( TheChild, TheParent )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddItem(newItem as GraffitiWebAccordionNavItem, ParentItem as GraffitiWebAccordionNavItem = Nil)
		  if not IsNull( newItem ) then
		    
		    if not IsNull( ParentItem ) then
		      ParentItem.Children.Append( newItem )
		      
		      dim strExec() as String
		      strExec.Append( "var theItem = window.GSjQuery('" + EscapeString( ItemToString( newItem ) ) + "');" )
		      strExec.Append( "window.GSjQuery('." + ParentItem.UUID + "').append(theItem);" )
		      
		      strExec.Append( "theItem.find('a').mousedown(function(e) {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'itemmousedown', [window.GSjQuery(e.target).data('uuid')]);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "theItem.find('a').single_double_click(function () {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'itemClicked', [window.GSjQuery(this).data('uuid')]);" )
		      strExec.Append( "}, function () {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'itemDblClicked', [window.GSjQuery(this).data('uuid')]);" )
		      strExec.Append( "});" )
		      
		      Buffer( strExec )
		    else
		      Items.Append( newItem )
		      dim strItem() as String
		      strItem.Append( "<li data-uuid='" + newItem.UUID + "'>" )
		      strItem.Append( "<a class='gwanmenu_panel accNavDefaultStyle" + if( newItem.Expanded, " active", "" ) + "' href=""javascript:void(0)"" data-uuid='" + newItem.UUID + "'>" )
		      if newItem.Icon <> "" then strItem.Append( "<i class='fa " + newItem.Icon + "' style='display: inline;margin-right:5px;'></i>" )
		      strItem.Append( EscapeString( newItem.Text ) )
		      if newItem.NotificationCount >= 0 then strItem.Append( "<span class='accnavbadge' " + if( newItem.NotificationCount >= 0, "style=" + chr(34) + "display: block;" + chr(34), "style=" + chr(34) + "display:none;" + chr(34) ) + ">" + Str( newItem.NotificationCount, "#" ) + "</span>" )
		      strItem.Append( "</a><ul id='" + newItem.UUID + "_panel' class='" + newItem.UUID + "' style='display: " + if( newItem.Expanded, "block", "none" ) + ";'>" )
		      
		      dim strExec() as String
		      strExec.Append( "var theItem = window.GSjQuery('" + EscapeString( Join( strItem, "" ) ) + "');" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_wrapper >ul.gwanmenu').append(theItem);" )
		      strExec.Append( "theItem.find('a').mousedown(function(e) {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'headermousedown', [window.GSjQuery(e.target).data('uuid')]);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "theItem.find('a').click(function(e) {" )
		      strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_wrapper').find('[data-uuid=\'" + newItem.UUID + "\']').find('a');" )
		      strExec.Append( "var isExpanded = window.GSjQuery('.gwanmenu_panel[data-uuid=" + newItem.UUID + "]').hasClass('active');" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_wrapper').find('a.gwanmenu_panel').removeClass('active');" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_wrapper ul li ul').slideUp(400);" )
		      strExec.Append( "if (isExpanded == false) {" )
		      strExec.Append( "theItem.addClass('active');" )
		      strExec.Append( "window.GSjQuery('." + newItem.UUID + "').slideDown(400);" )
		      strExec.Append( "window.GSjQuery('#" + newItem.UUID + "_panel').scrollintoview();" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'expand', [window.GSjQuery(e.target).data('uuid')]);" )
		      strExec.Append( "} else {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'collapse', [window.GSjQuery(e.target).data('uuid')]);" )
		      strExec.Append( "}" )
		      strExec.Append( "});" )
		      
		      Buffer( strExec )
		      
		      dim arrHold() as GraffitiWebAccordionNavItem = CloneChildren( newItem.Children )
		      ReDim newItem.Children(-1)
		      
		      for intCycle as Integer = 0 to arrHold.Ubound
		        AddItem( arrHold(intCycle), newItem )
		      next
		    end if
		    
		    'UpdateOptions()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function CloneChildren(theChildren() as GraffitiWebAccordionNavItem) As GraffitiWebAccordionNavItem()
		  dim n() as GraffitiWebAccordionNavItem
		  for intCycle as Integer = 0 to theChildren.Ubound
		    n.Append( theChildren(intCycle) )
		  next
		  Return n
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Collapse(theItem as GraffitiWebAccordionNavItem)
		  if not IsNull( theItem ) then
		    dim strExec() as String
		    strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_wrapper').find('[data-uuid=\'" + theItem.UUID + "\']').find('a');" )
		    strExec.Append( "theItem.removeClass('active');" )
		    strExec.Append( "window.GSjQuery('." + theItem.UUID + "').slideUp(400);" )
		    
		    theItem.Expanded = False
		    
		    Buffer( strExec )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CollapseAll()
		  for intCycle as Integer = 0 to Items.Ubound
		    Collapse( Items(intCycle) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function EscapeString(InputString as String) As String
		  dim strHold as String = ReplaceAll( InputString, chr( 34 ), "\" + chr( 34 ) )
		  strHold = ReplaceAll( InputString, "'", "\'" )
		  strHold = ReplaceLineEndings( strHold, "\r\n" )
		  
		  Return strHold
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Expand(theItem as GraffitiWebAccordionNavItem, CollapseOthers as Boolean = True)
		  if not IsNull( theItem ) then
		    dim strExec() as String
		    strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_wrapper').find('[data-uuid=\'" + theItem.UUID + "\']').find('a');" )
		    if CollapseOthers then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_wrapper').find('a.gwanmenu_panel').removeClass('active');" )
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_wrapper ul li ul').slideUp(400);" )
		    end if
		    strExec.Append( "theItem.addClass('active');" )
		    strExec.Append( "window.GSjQuery('." + theItem.UUID + "').slideDown(400);" )
		    
		    theItem.Expanded = True
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExpandAll()
		  for intCycle as Integer = 0 to Items.Ubound
		    Expand( Items(intCycle) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ExpandedItems() As Integer
		  dim intRet as Integer = 0
		  for intCycle as Integer = 0 to UBound( Items )
		    if Items(intCycle).Expanded then intRet = intRet + 1
		  next
		  
		  Return intRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(SearchFor as String, IsText as Boolean = True) As GraffitiWebAccordionNavItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebAccordionNavItem
		  dim retItem as GraffitiWebAccordionNavItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if IsText then
		      if currItem.Text = SearchFor then Return currItem
		    else
		      if currItem.Tag = SearchFor then Return currItem
		    end if
		    
		    if IsNull( currItem.Children ) then Continue
		    if UBound( currItem.Children ) >= 0 then
		      if IsText then
		        retItem = Item_Find( SearchFor, currItem.Children )
		      else
		        retItem = Item_FindByTag( SearchFor, currItem.Children )
		      end if
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetParent(ofItem as GraffitiWebAccordionNavItem) As GraffitiWebAccordionNavItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebAccordionNavItem
		  dim retItem as GraffitiWebAccordionNavItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    
		    dim theChild as GraffitiWebAccordionNavItem = Item_Find( ofItem.Text, currItem.Children )
		    
		    if not IsNull( theChild ) then Return currItem
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  strOut.Append( "<!-- BEGIN GraffitiWebAccordionNav -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "jQuery" ).Child( "jquery.single_double_click.js" ), "graffitisuite" ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "jQuery" ).Child( "jquery.scrollintoview.min.js" ), "graffitisuite" ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "accordion" ).Child( "gwacnav.css" ), JavascriptNamespace ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebAccordionNav -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function IsTopLevel(theItem as GraffitiWebAccordionNavItem) As Boolean
		  for intCycle as Integer = 0 to items.Ubound
		    if Items(intCycle).UUID = theItem.UUID then Return True
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Item(itemIndex as Integer) As GraffitiWebAccordionNavItem
		  if itemIndex >= 0 and itemIndex <= Items.Ubound then Return Items(itemIndex)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ItemCount() As Integer
		  Return Items.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemToString(newItem as GraffitiWebAccordionNavItem) As String
		  if not IsNull( newItem ) then
		    dim strChild() as String
		    strChild.Append( "<li data-uuid='" + newItem.UUID + "'>" )
		    strChild.Append( "<a href='javascript:void(0)' data-uuid='" + newItem.UUID + "' class='accNavDefaultStyle'>" )
		    if newItem.Icon <> "" then strChild.Append( "<i class='accicon " + newItem.Icon + "' style='display: inline;margin-right:5px;'></i>" )
		    strChild.Append( "<span class='caption'>" + EscapeString( newItem.Text ) + "</span>" )
		    strChild.Append( "<span class='accnavbadge' " + if( newItem.NotificationCount >= 0, "style=" + chr(34) + "display: block;" + chr(34), "style=" + chr(34) + "display:none;" + chr(34) ) + ">" + Str( newItem.NotificationCount, "#" ) + "</span>" )
		    strChild.Append( "</a></li>" )
		    
		    Return Join( strChild, "" )
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_Find(ItemText as String, ChildItems() as GraffitiWebAccordionNavItem) As GraffitiWebAccordionNavItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( ChildItems )
		  
		  dim currItem as GraffitiWebAccordionNavItem
		  dim retItem as GraffitiWebAccordionNavItem
		  
		  for intCycle = 0 to intMax
		    currItem = ChildItems(intCycle)
		    if currItem.Text = ItemText then Return currItem
		    
		    if UBound( currItem.Children ) >= 0 then
		      retItem = Item_Find( ItemText, currItem.Children )
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_FindByTag(ItemTag as Variant, ChildItems() as GraffitiWebAccordionNavItem) As GraffitiWebAccordionNavItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( ChildItems )
		  
		  dim currItem as GraffitiWebAccordionNavItem
		  dim retItem as GraffitiWebAccordionNavItem
		  
		  for intCycle = 0 to intMax
		    currItem = ChildItems(intCycle)
		    if currItem.Tag = ItemTag then Return currItem
		    
		    if UBound( currItem.Children ) >= 0 then
		      retItem = Item_Find( ItemTag, currItem.Children )
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_FindByUUID(theUUID as String) As GraffitiWebAccordionNavItem
		  for intPanel as Integer = 0 to Items.Ubound
		    dim currPanel as GraffitiWebAccordionNavItem = Items(intPanel)
		    if currPanel.UUID = theUUID then Return currPanel
		    
		    for intChild as Integer = 0 to currPanel.Children.Ubound
		      dim currChild as GraffitiWebAccordionNavItem = currPanel.Children(intChild)
		      if currChild.UUID = theUUID then Return currChild
		    next
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Item_Remove(theItem as GraffitiWebAccordionNavItem)
		  if not IsNull( theItem ) then
		    dim strExec() as String
		    
		    strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_wrapper').find('li[data-uuid=\'" + theItem.UUID + "\']');" )
		    strExec.Append( "theItem.remove();" )
		    
		    for intPanel as Integer = Items.Ubound DownTo 0
		      dim currPanel as GraffitiWebAccordionNavItem = Items(intPanel)
		      if currPanel.UUID = theItem.UUID then
		        Items.Remove( intPanel )
		        Continue
		      end if
		      
		      for intChild as Integer = currPanel.Children.Ubound DownTo 0
		        dim currChild as GraffitiWebAccordionNavItem = currPanel.Children(intChild)
		        if currChild.UUID = theItem.UUID then currPanel.Children.Remove( intChild )
		      next
		    next
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll()
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  for intCycle = intMax DownTo 0
		    Item_Remove( items(intCycle) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAll(fromItem as GraffitiWebAccordionNavItem)
		  if IsNull( fromItem ) then Return
		  
		  dim intMax as Integer = fromItem.Children.Ubound
		  for intCycle as Integer = intMax DownTo 0
		    Item_Remove( fromItem.Children(intCycle) )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(theItem as GraffitiWebAccordionNavItem)
		  if not IsNull( theItem ) then Item_Remove( theItem )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemText as String)
		  dim theItem as GraffitiWebAccordionNavItem = Item_Find( ItemText, Items )
		  if not IsNull( theItem ) then Item_Remove( theItem )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SetItemClicked(theItem as GraffitiWebAccordionNavItem)
		  if IsNull( theItem ) then Return
		  if IsNull( StyleItemClicked ) then Return
		  
		  dim strOut() as String
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + " .gwanmenu').find('li>ul>li>a').each(function (index) {" )
		  strOut.Append( "window.GSjQuery(this).removeClass('" + StyleItemClicked.Name + "');" )
		  strOut.Append( "});" )
		  strOut.Append( "window.GSjQuery('a[data-uuid=" + chr(34) + theItem.UUID + chr(34) + "]').addClass('" + StyleItemClicked.Name + "');" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateHeaderStyles()
		  if IsNull( mStyleHeader ) or IsNull( mStyleHeaderInactive ) Then Return
		  
		  dim strExec() as String
		  dim currItem as GraffitiWebAccordionNavItem
		  for intCycle as Integer = 0 to UBound( Items )
		    currItem = Items(intCycle)
		    
		    if IsNull( currItem ) then Continue
		    
		    strExec.Append( "var thisLink = window.GSjQuery('.gwanmenu_panel[data-uuid=\'" + currItem.UUID + "\']');" )
		    strExec.Append( "var thisPanel = window.GSjQuery('#" + currItem.UUID + "_panel');" )
		    strExec.Append( "var isOpen;" )
		    strExec.Append( "if (thisPanel.css('display') == 'none') { isOpen = false; } else { isOpen = true; }" )
		    
		    strExec.Append( "if (thisLink.hasClass('accNavDefaultStyle')) {" )
		    strExec.Append( "thisLink.removeClass('accNavDefaultStyle');" )
		    strExec.Append( "}" )
		    
		    if not IsNull( oldHeaderActive ) and oldHeaderActive <> mStyleHeader then
		      strExec.Append( "if (thisLink.hasClass('" + oldHeaderActive.Name + "')) {" )
		      strExec.Append( "thisLink.removeClass('" + oldHeaderActive.Name + "');" )
		      strExec.Append( "}" )
		    end if
		    if not IsNull( oldHeaderInactive ) and oldHeaderInactive <> mStyleHeaderInactive then
		      strExec.Append( "if (thisLink.hasClass('" + oldHeaderInactive.Name + "')) {" )
		      strExec.Append( "thisLink.removeClass('" + oldHeaderInactive.Name + "');" )
		      strExec.Append( "}" )
		    end if
		    oldHeaderActive = mStyleHeader
		    oldHeaderInactive = mStyleHeaderInactive
		    
		    strExec.Append( "if(isOpen){" )
		    
		    strExec.Append( "if(thisLink.hasClass('" + mStyleHeaderInactive.Name + "')){" )
		    strExec.Append( "thisLink.removeClass('" + mStyleHeaderInactive.Name + "');" )
		    strExec.Append( "}" )
		    
		    strExec.Append( "thisLink.addClass('" + mStyleHeader.Name + "');" )
		    strExec.Append( "}else{" )
		    
		    strExec.Append( "if(thisLink.hasClass('" + mStyleHeader.Name + "')){" )
		    strExec.Append( "thisLink.removeClass('" + mStyleHeader.Name + "'); " )
		    strExec.Append( "}" )
		    
		    strExec.Append( "thisLink.addClass('" + mStyleHeaderInactive.Name + "');" )
		    strExec.Append( "}" )
		  next
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateIcon(theItem as GraffitiWebAccordionNavItem, newIcon as String)
		  dim strExec() as String
		  strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_wrapper').find('li[data-uuid=\'" + theItem.UUID + "\']');" )
		  strExec.Append( "theItem.find('i.accicon').removeClass('" + EscapeString( theItem.Icon ) + "').addClass('" + EscapeString( newIcon ) + "');" )
		  
		  theItem.Icon = newIcon
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateItemStyles()
		  if IsNull( StyleItems ) then Return
		  
		  dim strExec() as String
		  for intCycle as Integer = 0 to Items.Ubound
		    dim currItem as GraffitiWebAccordionNavItem = Items(intCycle)
		    strExec.Append( "window.GSjQuery('#" + currItem.UUID + "_panel').find('a').each(function(index) {" )
		    strExec.Append( "if (window.GSjQuery(this).hasClass('accNavDefaultStyle')) {window.GSjQuery(this).removeClass('accNavDefaultStyle');}" )
		    if not IsNull( oldItemStyle ) and oldItemStyle <> StyleItems then
		      strExec.Append( "if (window.GSjQuery(this).hasClass('" + oldItemStyle.Name + "')) {window.GSjQuery(this).removeClass('" + oldItemStyle.Name + "');}" )
		    end if
		    oldItemStyle = StyleItems
		    strExec.Append( "window.GSjQuery(this).addClass('" + StyleItems.Name + "');" )
		    strExec.Append( "});" )
		  next
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateNotification(theItem as GraffitiWebAccordionNavItem, newCount as Integer)
		  dim strExec() as String
		  
		  strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "').find('[data-uuid=" + chr(34) + theItem.UUID + chr(34) + "]').parent();" )
		  strExec.Append( "var theSpan = theItem.find('span.accnavbadge');" )
		  strExec.Append( "theSpan.html('" + Str( newCount, "#" ) + "');" )
		  
		  if newCount >= 0 then
		    strExec.Append( "theSpan.css('display','block');" )
		  else
		    strExec.Append( "theSpan.css('display','none');" )
		  end if
		  
		  if isShown and LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		  else
		    Buffer( Join( strExec, "" ) )
		  end if
		  
		  theItem.NotificationCount = newCount
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    'dim strHTML as String = "<ul class=""gwanmenu"">" + ItemsToString() + "</ul>"
		    
		    'me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_wrapper').html('" + strHTML + "');" )
		    '
		    'me.ExecuteJavaScript( "window.GSjQuery(function () {" + _
		    '"var gwanmenu_ul = window.GSjQuery('.gwanmenu > li > ul')," + _
		    '"gwanmenu_a = window.GSjQuery('.gwanmenu > li > a');" + _
		    '"var gwanmenu_item = window.GSjQuery('.gwanmenu > li > ul >li > a');" + _
		    '"gwanmenu_ul.hide();" + _
		    '"gwanmenu_item.click(function (e) {" + _
		    '"e.preventDefault();" + _
		    '"Xojo.triggerServerEvent('" + self.ControlID + "', 'itemclicked', [window.GSjQuery(this).attr('data-tag')]);" + _
		    '"});" + _
		    '"gwanmenu_a.click(function (e) {" + _
		    '"e.preventDefault();" + _
		    '"if (!window.GSjQuery(this).hasClass('active')) {" + _
		    '"gwanmenu_a.removeClass('active');" + _
		    '"gwanmenu_ul.filter(':visible').slideUp('normal');" + _
		    '"window.GSjQuery(this).addClass('active').next().stop(true, true).slideDown('normal');" + _
		    '"Xojo.triggerServerEvent('" + self.ControlID + "', 'expanded', [window.GSjQuery(this).attr('data-tag')]);" + _
		    '"} else {" + _
		    '"window.GSjQuery(this).removeClass('active');" + _
		    '"window.GSjQuery(this).next().stop(true, true).slideUp('normal');" + _
		    '"Xojo.triggerServerEvent('" + self.ControlID + "', 'collapsed', [window.GSjQuery(this).attr('data-tag')]);" + _
		    '"}" + _
		    '"});" + _
		    '"});" )
		    
		    UpdateStyles()
		    
		    RunBuffer()
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateStyles()
		  UpdateHeaderStyles()
		  UpdateItemStyles()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateText(theItem as GraffitiWebAccordionNavItem, newText as String)
		  dim strExec() as String
		  strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_wrapper').find('li[data-uuid=\'" + theItem.UUID + "\']');" )
		  strExec.Append( "theItem.find('.caption').html('" + EscapeString( newText ) + "');" )
		  
		  theItem.Text = newText
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event HeaderCollapsed(CollapsedItem as GraffitiWebAccordionNavItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event HeaderExpanded(ExpandedItem as GraffitiWebAccordionNavItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event HeaderMouseDown(theItem as GraffitiWebAccordionNavItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemClicked(ClickedItem as GraffitiWebAccordionNavItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemDoubleClicked(ClickedItem as GraffitiWebAccordionNavItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ItemMouseDown(theItem as GraffitiWebAccordionNavItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://codepen.io/KevinCarpenter/pen/lFmIc
		
		Copyright (c) 2013 by Kevin Carpenter (http://codepen.io/KevinCarpenter/pen/lFmIc)
		
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
		
	#tag EndNote


	#tag Property, Flags = &h0
		expandedItem As GraffitiWebAccordionNavItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Items() As GraffitiWebAccordionNavItem
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleHeader As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleHeaderInactive As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleItemClicked As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleItems As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldHeaderActive As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldHeaderInactive As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldItemStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleHeader
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleHeader = value
			  
			  UpdateStyles()
			End Set
		#tag EndSetter
		StyleHeaderActive As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleHeaderInactive
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleHeaderInactive = value
			  
			  UpdateStyles()
			End Set
		#tag EndSetter
		StyleHeaderInactive As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleItemClicked
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleItemClicked = value
			  
			  UpdateStyles()
			End Set
		#tag EndSetter
		StyleItemClicked As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleItems
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mStyleItems = value
			  
			  UpdateStyles()
			End Set
		#tag EndSetter
		StyleItems As WebStyle
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.accordionnav", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jqAccordion, Type = String, Dynamic = False, Default = \"window.GSjQuery(function () {\n\n    var gwanmenu_ul \x3D window.GSjQuery(\'.gwanmenu > li > ul\')\x2C\n        gwanmenu_a \x3D window.GSjQuery(\'.gwanmenu > li > a\');\n\n    gwanmenu_ul.hide();\n\n    gwanmenu_a.click(function (e) {\n        e.preventDefault();\n        if (!window.GSjQuery(this).hasClass(\'active\')) {\n            gwanmenu_a.removeClass(\'active\');\n            gwanmenu_ul.filter(\':visible\').slideUp(\'normal\');\n            window.GSjQuery(this).addClass(\'active\').next().stop(true\x2C true).slideDown(\'normal\');\n        } else {\n            window.GSjQuery(this).removeClass(\'active\');\n            window.GSjQuery(this).next().stop(true\x2C true).slideUp(\'normal\');\n        }\n    });\n\n});", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACx\rjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPDSURBVHhe7d1BaFx1EMfxmdfdmk3QgyCCoOBB\rtpc20IOYQz2oIAg1sZqTelIQBEu3Ej00WIJ4apNUwZ70qJfa0kQQvIi1h0a8uIrQ4MUeRfBgTTfJ\rbt74onPw1rxZuu/R//dzyH/+AxsC89uFt++fXQEAAAAAAAAAAAAAAAAA3C3U17Cx2XcfyXd69/gW\rI5RJc3vz0uIN34aEAzAx03klN/1QVe73FipgIn9muRzfWF36zFulhALQmnn7mIpd9C1qwMxe6q0s\rl55J5ms5Zu95hbpQnfeqlFAAimf/QS9RE2p2yMtSYq8AqrHH4c4JzoRBJo4AJI4AJI4AJI4AJI4A\rJC4agN98RX2EZhIKgIl97iVqIjqTfb6WMri+9k3jwNTDKnLYW6iQiXzau7x8wrelDHU7ePyFzqTk\r2aHiT+B2cCV0Sxva3bh49idvAAAAAMBtDHUZOHH0+IO2L5sUyfZ7CyOVb+tO3t348qPfvVFaOADj\rM5354uHv+xbVmr91eekDr0sJvRPYmu68qapnfYvqPdVoP/HHYH3tB9/vWfRm0Fu+oj5CMwkFoHj2\rH/ASNRGdCecBEkcAEkcAEkcAEkcAEkcAEkcAEhcKgImte4maMJHQTGKvALmc9wp1YfKxV6XETgWv\rr33fbE/1ReVpb6FKJqd6K0uLvitlqNvB8vzcveM6OGKajXkHI6SWb96yxlVZPXPTWwAAAABwG0Nd\rBo5NnziiqpNqymVgBUxky0y7m6uL33mrtHAAWjMnPyke/JpvUaH//j186XXflhI8FXxyQVU6vkXF\riifi4UZ7KhusX/vWW3sWuhegaq96iZqIziR4O1gf9QK1EZsJ5wESRwASRwASRwASRwASRwASFw2A\r+Yr6CM0kFAAz+9lL1IRJbCaxAGgW+jQK3DlFAEb3CSGD69d+abandr+x8hlR4fOBqmTyd/Hzjd7K\r8ui+OPL/9h+be6zZ7xOCCvSbze3tS2d+9S0AAAAA7MFQl4Gt6c6LqvqkiUx4CyNUDG/D8vxqb/Xc\rF94qLRaA06ezVvevr1T0We+gQib2dW/yvudkYSH31p6F3goe7948z/DrY3cWuzPxbSnBu4F21AvU\rRmwmwQDoQ16gNmIzCQYAdwsCkDgCkDgCkDgCkDgCkLhoADZ9RX2EZhILgAlfV143wZPaoQDkau94\riZrIVea8LCV4KnjtRtZ+/EqmelBMHxCN/R4MyaQvaj/uWP7y1sq5K94tZehTwf+anSUAVbhwYccr\rAAAAAAAAAAAAAAAAAAAAAAAAAEAqRP4BxEXdbT4xoAkAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAB8SURBVDhPYxhwwAilGTj9ixoZGf/zQrl4wf//jJ+/b+yrB7HhBnAFFP2HMokC3zb0gfXCDeAIKAiDMokCPzZMWAWiES7wLzoGZRIFvm3sswLRTGAeBYCKXqA0ECmORs6Awh2M/xn5oFy84D/j/0/fN/R7gNgUe2EUMDAAABjHMAwmilUpAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
