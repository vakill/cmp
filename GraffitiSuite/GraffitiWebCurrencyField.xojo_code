#tag Class
Protected Class GraffitiWebCurrencyField
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "textchange"
		      if Parameters.Ubound >= 1 then
		        mText = Parameters(0)
		        mValue = Parameters(1)
		      else
		        mText = ""
		        mValue = 0
		      end if
		      TextChanged()
		    case "onfocus"
		      GotFocus()
		    case "onfocuslost"
		      LostFocus()
		    case "mouseenter"
		      MouseEnter()
		    case "mouseleave"
		      MouseExit()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    DoEnabled()
		    
		    Return True
		  case "style"
		    dim strExec() as String
		    dim sVal as WebStyle = value
		    
		    strExec.Append( "var theControl = window.GSjQuery('#" + me.ControlID + "_inner');" )
		    if not IsNull( value ) then
		      if not IsNull( oldStyle ) then strExec.Append( "theControl.removeClass('" + oldStyle.Name + "');" )
		      
		      if not IsNull( sVal ) then
		        if not sVal.isdefaultstyle and sVal.Name <> "_Style" and sVal.Name <> "_DefaultStyle" then
		          strExec.Append( "theControl.addClass('" + sval.Name + "');" )
		        end if
		      end if
		    end if
		    
		    oldStyle = value
		    
		    Buffer( strExec )
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id=""" + me.ControlID + """ style=""overflow: visible;display:none;"">" )
		  source.Append( "<input style=""width: 100%; height: 100%;padding: 0;"" class=""inputDate textcontrol"" id=""" + me.ControlID + "_inner"" value=""" + me.Text + """>" )
		  source.append("</div>")
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions( True )
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Clear()
		  mText = ""
		  mValue = 0
		  Buffer( "window.gsCurr_" + me.ControlID + ".clear();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoEnabled()
		  dim strScript as String
		  
		  if me.Enabled = False Then
		    strScript = "window.GSjQuery('#" + me.ControlID + "_inner').attr('disabled', 'disabled');"
		  Else
		    strScript = "window.GSjQuery('#" + me.ControlID + "_inner').removeAttr('disabled');"
		  end if
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoVisible()
		  dim strScript as String
		  
		  if me.Visible = False Then
		    strScript = "window.GSjQuery('#" + me.ControlID + "_inner').css('display', 'none');"
		  Else
		    strScript = "window.GSjQuery('#" + me.ControlID + "_inner').css('display', 'inline');"
		  end if
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCurrencyField -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "autoNumeric.min.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCurrencyField -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RoundingToString() As String
		  select case RoundingStyle
		  case RoundingMethods.HalfUpSymmetric
		    Return "S"
		  case RoundingMethods.HalfUpAsymmetric
		    Return "A"
		  case RoundingMethods.HalfDownSymmetric
		    Return "s"
		  case RoundingMethods.HalfDownAsymmetric
		    Return "a"
		  case RoundingMethods.HalfEven
		    Return "B"
		  case RoundingMethods.AwayFromZero
		    Return "U"
		  case RoundingMethods.TowardZero
		    Return "D"
		  case RoundingMethods.ToCeiling
		    Return "C"
		  case RoundingMethods.ToFloor
		    Return "F"
		  case RoundingMethods.Nearest
		    Return "N05"
		  case RoundingMethods.Up
		    Return "U05"
		  case RoundingMethods.Down
		    Return "d05"
		  end select
		  
		  Return "S"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SymbolPlacementToString() As String
		  if mSymbolPlacement = SymbolLocations.Suffix then Return "s"
		  Return "p"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions(isInit as Boolean = False)
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    strExec.Append( "var opts = {currencySymbol: '" + EscapeString( mCurrencySymbol ) + "', " + _
		    "minimumValue: '" + Str( mMinimumValue, "-0.00" ) + "', " + _
		    "maximumValue: '" + Str( mMaximumValue, "-0.00" ) + "', " + _
		    "negativeBracketsTypeOnBlur: '" + EscapeString( mBracketNegatives ) + "', " +_
		    "emptyInputBehavior: 'always', " + _
		    "allowDecimalPadding: " + Str( mPadDecimal ).Lowercase + ", " + _
		    "roundingMethod: '" + RoundingToString() + "', " + _
		    "decimalPlaces: '" + Str( DecimalPlaces ) + "', " + _
		    "currencySymbolPlacement: '" + SymbolPlacementToString + "', " + _
		    "decimalCharacter: '" + DecimalChar + "', " + _
		    "digitalGroupSpacing: '" + Str( GroupCount ) + "', " + _
		    "leadingZero: 'allow'," + _
		    "digitGroupSeparator: '" + ThousandsSep + "'};" )
		    
		    if not isInit then
		      strExec.Append( "window.gsCurr_" + me.ControlID + ".update(opts);" )
		    else
		      strExec.Append( "window.gsCurr_" + me.ControlID + " = new AutoNumeric('#" + me.ControlID + "_inner', opts);" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').change(function() {" )
		      strExec.Append( "var unformatted = window.gsCurr_" + me.ControlID + ".getNumericString();" )
		      strExec.Append( "var formatted = window.gsCurr_" + me.ControlID + ".getFormatted();" )
		      strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'textchange', [formatted, unformatted]);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').focus(function() {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onfocus', []);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').focusout(function() {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'onfocuslost', []);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').mouseenter(function() {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'mouseenter', []);" )
		      strExec.Append( "});" )
		      
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').mouseleave(function() {" )
		      strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'mouseleave', []);" )
		      strExec.Append( "});" )
		    end if
		    
		    Buffer( strExec )
		    
		    DoEnabled()
		    DoVisible()
		    
		    RunBuffer()
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TextChanged()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://www.decorplanit.com/plugin/
		
		MIT License
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mBracketNegatives
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mBracketNegatives = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		BracketNegatives As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCurrencySymbol
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCurrencySymbol = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		CurrencySymbol As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDecimalChar
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if mDecimalChar <> value then
			    if value = mThousandsSep then
			      mThousandsSep = mDecimalChar
			    end if
			    
			    mDecimalChar = value
			    UpdateOptions()
			  end if
			End Set
		#tag EndSetter
		DecimalChar As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDecimalPlaces
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDecimalPlaces = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		DecimalPlaces As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mGroupCount
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mGroupCount = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		GroupCount As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions( True )
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMaximumValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMaximumValue = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		MaximumValue As Double
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mBracketNegatives As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCurrencySymbol As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDecimalChar As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDecimalPlaces As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDisplayEmpty As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mGroupCount As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMinimumValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMinimumValue = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		MinimumValue As Double
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMaximumValue As Double
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMinimumValue As Double
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPadDecimal As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReadOnly As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRoundingStyle As RoundingMethods
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSymbolPlacement As SymbolLocations
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mText As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mThousandsSep As String = """"""","""""""
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Double
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPadDecimal
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPadDecimal = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		PadDecimal As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mReadOnly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReadOnly = value
			  
			  if mReadOnly then
			    Buffer( "GSjQuery('#" + me.ControlID + "_inner').prop('readonly', true);" )
			  else
			    Buffer( "GSjQuery('#" + me.ControlID + "_inner').removeProp('readonly');" )
			  end if
			End Set
		#tag EndSetter
		ReadOnly As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRoundingStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRoundingStyle = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		RoundingStyle As RoundingMethods
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSymbolPlacement
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSymbolPlacement = value
			  UpdateOptions()
			End Set
		#tag EndSetter
		SymbolPlacement As SymbolLocations
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-only
			  
			  'mText = ReplaceAll( ReplaceAll( ReplaceAll( value, ThousandsSep, "" ), DecimalChar, "." ), CurrencySymbol, "" )
			  '
			  'if IsNumeric( mText ) Then
			  'Buffer( "window.gsCurr_" + me.ControlID + ".set(" + mText + ");" )
			  'Else
			  'Buffer( "window.gsCurr_" + me.ControlID + ".set('" + mText + "');" )
			  'end if
			End Set
		#tag EndSetter
		Text As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mThousandsSep
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if mThousandsSep <> value then
			    if mDecimalChar = value then
			      mDecimalChar = mThousandsSep
			    end if
			    
			    mThousandsSep = value
			    UpdateOptions()
			  end if
			End Set
		#tag EndSetter
		ThousandsSep As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  dim formatString as String = "#0"
			  if mDecimalPlaces > 0 then
			    formatString = formatString + "."
			    for intCycle as Integer = 1 to mDecimalPlaces
			      formatString = formatString + "0"
			    next
			  end if
			  
			  Buffer( "window.gsCurr_" + me.ControlID + ".set(" + str( mValue, formatString ) + ");" )
			  
			  mText = Str( mValue, formatString )
			End Set
		#tag EndSetter
		Value As Double
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.currencyfield", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAApzSURBVHhe7d0PkJRlHQfw5/fu7t3tHjf+q5lyMM2IuyENUmkOojJNKZDbPYySSf41DDjpzN3uhhhW5FSW4e0uOulQCuhkYSK3e6AxBDJqwaXkDDOmd1MZZkIaZiW3e3e7+/z6PdzvDOGA3b295Xn2fT4z8D7Pb3XY3ff7Pu+fff8Iy7Isy7Isy7Isy7Isy7Isy7KqGvDUFQIt4YXoEWdz90QSUHrkEwNbEq9wpeq5JgCBUOQZmnx6qHcKKLI5T+5jg1vu+RNXqprD0+q2bJmP/j79zFdA+LzSN4F7Vc8dASgarShcwgZgBCikDYC7eWwAXA2lazaObQBczgbA5WwAXM4GwOVsAFzOFbs7gYtmzBTofJW7hcj6GqeNr21sbvBO+sxgrud3/+V61anK3Z1Aa3iyQDELBVxDH/BzXC4ZothP39ROANyZ7oxv53JVqKoA+IPh6+kjLQUQX+BS2aHAwwKhEwA608mOX3PZWFURgEAouoxmzQpqVvhHHPwD/bUunYz/bKhvHqMDUN8avUQi3kUfYhaXzgzE34KA7/alYru4YgxjAxAIRW6nyfeHenqg1cNGkRXfyjwRf51L2jMyAIFgOCEA2rirFdpgPCSEXJ5JJbZySWvG7Qb6g5GNtAG2nLvaoQ3QBnp/831NzelsT/ceLmvLqADQsL+ZvuD53NUcXONtan4r19P9HBe0ZMyRQFry76QJ7eaZBOZyQ1tGBCAQal9ES/43uWsM2jPYxE1tab8RWDsnPMHjAfPO0EV8Pp2Kf5J72tJ+BHAcoQ7wGAcBf8BNrWk9AoxriU6SDv6Ru6OGiNtoC32fOrYPIn9Aerxv1+ecbF+t43UG+89F9E4QDl5B/+UVNHxfzf9b0dS/k0nF53BXa1oHwB+KrKc3uIS7pcoKFKuEIx9JdyZoH70w58xbeVZmYPCLDsAN9C0FuVwQWvqnZTrj3dzVmt4BCIb/TUvsWdwtASbzeScysLXjr1woSd3c6IVOHm+jb+smLp0UCvFgJhlbyl3taRuA+mD0SlqSdnO3FIl0Mhbmdlnwz8y0NwJf4dIJ8nm4eLSBqyRtNwKlkDO5WTRax+8o98xX0p3x/elk/AaUch51TzjejwLuNGnmK9oGgIb+wq7lG4FHiPu5OSYyXYnNHjEwmWb5o1xSQ/8bmdw4I7b8j6XtKiAQDB+gFFzI3YLRjHiS1sGzuTvm/K3RsJDYBA52mni2kM4ByFMAih+hUHwnnYp9j3vWaWi5CvC3rDi/pJmvALzBLasAWgYA5YCXm8WT+H5uWQXQMgD92+75G01odV48dEQjN60CaBkA9hpPiwKITdy0CqBvABBLCgBtA0ytC0W/zD3rNLQNAI3/B7hZNEfI9XWhtmnctU5B3wA4Yhs3SwD1Dnp2+FsiLVywTkLbAPR7B0YRAAJiHO1Ipvyh8C1csUag70mhLz0/6G2aNhWEmMiVkoCAWb7G5st8jdN7s717/8Fli2l9VnDNxOZa2qgr6rf4EQE0UhKWexunTfBNaj6U6+n+O7/ielqfD6AEQuF99DYv525ZoMBdQsj7Msm1W7jkWtoHoDbYdqUHPKM5L+Ck1KlhCOJhD8DP+zo73uSyq2gfACUQjNxK7/Qu7o6Vx1HKTeqnXu67ghEBUAKhSJwm7UO9saN+16cv5ZcUhkcoDLT6qW7GBEAJBMO/oA26Cl4ahnuEFA+ku+IbuFB1jAqAQiOB2nBrHepVCOKrNDIkMql4gitVw7gAKBSCn9Dk60O9SsLXUEBHJhlbywXjGRkAhULwNVoy76dVQg2XKgfxLzQirKERYR1XjGVsAJTa0Dc+4hFyDTUru0p4F+4Biav7uhI7uWAc424Qcax8z563sz17H/U2Nr9IUZ4IAj7AL1UIXEAj0ELfxGbM9nY/zUWjGD0CHM/fGrkZpIjQp7qYS5WDYlM6FTPk5hX/V1UBGBYIRhbTJ1O3kWkeqlSGuiAlU9N/vXjsviNc0l5VBmBYfTByNW2sLadPqa7kqQzEZ9I1A7NNCUFVB2BYzdwVH/XI3AJAWEKfeDyXx44KQSr+We5pzRUBOJa63QwKZxF98FHfQ/g0yn5x6lhwXQCG1QfDl0oQNwqEBQDig1wuKwB5dV9n4inuasm1AThWXSgyn76IW+nPFC6Vy+M0CnyJ21qyAThGIBSl1YNsAwGf4NKoeaSn6Z2uNb3c1Y7RB4LKLduzd3+up3udr3H6K7RoXEqlc4deKR0KfDPbu1c9t1hLdgQ4BX8o8iP6glZytySI+GImFVdh0pINwGmoR84LBx7ibkkkig/1p2KlXek0xnS+NlAL6a74wzSM/5i7pUH8FLe0YwNQgEwyvlLd+4+7RQNwJnNTOzYABXJQxLhZNBB4Nje1YwNQoL6u+G4ayw9yt0jSBqAq4Im3hisMIDe0Y1wAGkI3n8fNiqO56OdmcQD+yS3tGBOAhpYVjYFQ+PW8qD2s9s+5XFkg3set4qA4zC3tGBOAPOQeoDlwvmqrgzMUgm2B1vYx+RFnJIFQ9PJSTzlDkHYEGI1AMHIvDaMzuHsUhWC2kPAcvVbyLWWLgrKYZw+/B6Cj7e1jtQ8AzeClNLdHvskDwHh6bbs/1H63WLS6jqtlpy5QpX+r1N/2+9NTGn7Dbe1ofSjY3xqdCoiFPXULhbrm/9vpVGzjUKE8xl0Xacp7xa/oiyrxeD5uTSfj2t6qRt8RYNkyn5CS1vsFUqd6gdgQCIafDQTbF3B1VOpbo1flPbi79Jmvcil2cFNL2o4AtJH3EL25hdwtHuKr9OnWOdJJHenqeImrBakLRi5wQNxGzVFffoZZHK/zo2S1DEBdKBKhoamDu+XwZwrETkTcheAcRAmHBrKDB8X2ewfUi+e1rGhIe2QTjTiXAYDaqCzPlUaIsXQqHuWelrQLwNHHs0gs+R6BxUAUbwGIcdSsHaqUl+5Lv6LdNgDk8Dpujjma+eqo4pjMfLX0m/AUce0CkKnB9bThZPot3/fpPvQP028vYHM8A+CZRYuQlmfQFASktk83P56Wu4HpzjUvyFx+Bu1DGXfFLW1o3pTuTLzAXe1puxs47MzdDaQEKG5Pp2LqKefG0P608GzP3ifV9f+0e6YexKztiRUoxS2ZrljJZw2dKUZcF5Dr7X45e0nzT30Sz6FBayqXtUF7EzfSkv8gd42i/SrgeIGW9tnCgVX01qdz6YxBFLvBwbB6oCSXjGNcAIYdfRYAiDZa+q7iUoXhqnQy/kPuGMvYAAxTJ2rQoriYZshiGovVUb0xo44cCsANHiHXH0mufZnLRjM+AO+at7rGP/ifmSCca2lWXUthGNVzBo7zLP3ZmE7G1g91q0f1BOA4tXOiH3Y8+Y9TIKagwBkg4PP8UgHw92r97jjwVN+/GnaLp+/I8QtVp2oD8B40OgSy7xz95a8QKGQok0ykuFvVtDwSeKbRqJHnZtWzARgR2ABY7mAD4HI2AC5nA+ByNgAuZwPgcjYALueOADx2xyAKUfjNGiFv+kmpBXPHoWBSG2y7yBGeJQDqpJKTQEBwZEr3+/talmVZlmVZlmVZlmVZlmVZlmVZVgGE+B+WHxom61VuIQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAFVSURBVDhPY6AUMEJpDMDhkyfHwMJkwfDn34kfWyY9ggpjACYojQmAmpkYmFaCDcEDmKE0CuAKKEwFOq6CkYFRFMjVZtOwVPl948ROiCwqwHABR0BBGFDzLMb/DHz/Gf7PgYgyFnL6FZhA2KgAwwAmBkawk/8x/i/+vqE/FYi1///7Z/p904QzYAVoAMMLbOqWOgyMjG5AJh+rmsWNPzdPPANhiCwmwBoLnAGFs4H+TwFz/jMc/8f4b8KPDRNWgfloAGsg/rlxYjOLmvlWoPFAHzE6AokYZg3z60Dxq1AlcIA1GkFpAORnUBgwMvwvAYnBwgYdYLiA27+wmJGJaTYw6piYNS1kgb60A3rHCBgra3/fPHEcqgwOMFzwl/H/YxD9n5GxB5SQwGEBDIevG/t7wQqIBZz+RY1cAUX/QTRUCCvAmZQZGf8dBdnM8P/fZqjQoAQMDAAtFmO0ApOGEQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = RoundingMethods, Type = Integer, Flags = &h0
		HalfUpSymmetric
		  HalfUpAsymmetric
		  HalfDownSymmetric
		  HalfDownAsymmetric
		  HalfEven
		  AwayFromZero
		  TowardZero
		  ToCeiling
		  ToFloor
		  Nearest
		  Up
		Down
	#tag EndEnum

	#tag Enum, Name = SymbolLocations, Type = Integer, Flags = &h0
		Prefix
		Suffix
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BracketNegatives"
			Visible=true
			Group="Behavior"
			InitialValue="(,)"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CurrencySymbol"
			Visible=true
			Group="Behavior"
			InitialValue="$"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="DecimalChar"
			Visible=true
			Group="Behavior"
			InitialValue="."
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DecimalPlaces"
			Visible=true
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GroupCount"
			Visible=true
			Group="Behavior"
			InitialValue="3"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaximumValue"
			Visible=true
			Group="Behavior"
			InitialValue="9999999999.99"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimumValue"
			Visible=true
			Group="Behavior"
			InitialValue="-9999999999.99"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PadDecimal"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RoundingStyle"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="RoundingMethods"
			EditorType="Enum"
			#tag EnumValues
				"0 - HalfUpSymmetric"
				"1 - HalfUpAsymmetric"
				"2 - HalfDownSymmetric"
				"3 - HalfDownAsymmetric"
				"4 - HalfEven"
				"5 - AwayFromZero"
				"6 - TowardZero"
				"7 - ToCeiling"
				"8 - ToFloor"
				"9 - Nearest"
				"10 - Up"
				"11 - Down"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="SymbolPlacement"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="SymbolLocations"
			EditorType="Enum"
			#tag EnumValues
				"0 - Prefix"
				"1 - Suffix"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThousandsSep"
			Visible=true
			Group="Behavior"
			InitialValue=","
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
