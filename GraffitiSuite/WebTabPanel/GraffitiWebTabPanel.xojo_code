#tag Class
Protected Class GraffitiWebTabPanel
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "tabchanged"
		      TabChanged( Parameters(0) )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  end if
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id=""" + me.ControlID + """ style=""overflow:auto;background-color:rgba(0, 0, 0, 0.0);"">" )
		  source.Append( ItemsToString() )
		  source.Append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddItem(newItem as GraffitiWebTabPanelItem)
		  if not IsNull( newItem ) then
		    dim strScript as String
		    Items.Append( newItem )
		    
		    if not IsNull( newItem.Child ) then
		      dim strID as String = newItem.Child.ControlID
		      strScript = "window.GSjQuery('" + strID + "').addClass('accordionWebCC');"
		    end if
		    
		    
		    me.ExecuteJavaScript( strScript )
		    
		    UpdateOptions()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(ItemText as String) As GraffitiWebTabPanelItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebTabPanelItem
		  dim retItem as GraffitiWebTabPanelItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Caption = ItemText then Return currItem
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebTabPanel -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "tabpanel" ).Child( "gwtabpanel.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "tabpanel" ).Child( "gwtabpanel.css" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebTabPanel -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemsToString() As String
		  dim strTabs as String = "<div id=""" + me.ControlID + "_panel"" style=""height:calc(100% - 59px);""><ul class=""resp-tabs-list"">"
		  dim strPanels as String = "<div class=""resp-tabs-container"">"
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebTabPanelItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    
		    if IsNull( currItem ) then Continue
		    if IsNull( currItem.Child ) then Continue
		    
		    strTabs = strTabs + "<li data-index=""" + Str( intCycle, "#" ) + """>" + currItem.Caption + "</li>"
		    strPanels = strPanels + "<div data-index=""" + Str( intCycle, "#" ) + """ style=""overflow:auto;""></div>"
		    
		  next
		  
		  strTabs = strTabs + "</ul>"
		  strPanels = strPanels + "</div></div>"
		  
		  Return strTabs + strPanels
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_Find(ItemText as String, ChildItems() as GraffitiWebAccordionNavItem) As GraffitiWebAccordionNavItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( ChildItems )
		  
		  dim currItem as GraffitiWebAccordionNavItem
		  dim retItem as GraffitiWebAccordionNavItem
		  
		  for intCycle = 0 to intMax
		    currItem = ChildItems(intCycle)
		    if currItem.Text = ItemText then Return currItem
		    if UBound( currItem.Children ) >= 0 then
		      retItem = Item_Find( ItemText, currItem.Children )
		      if not IsNull( retItem ) then Return retItem
		    end if
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Item_Remove(ItemText as String, ChildItems() as GraffitiWebAccordionNavItem) As Integer
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( ChildItems )
		  
		  dim currItem as GraffitiWebAccordionNavItem
		  dim retItem as GraffitiWebAccordionNavItem
		  
		  for intCycle = 0 to intMax
		    currItem = ChildItems(intCycle)
		    if currItem.Text = ItemText then Return 2
		    if UBound( currItem.Children ) >= 0 then
		      retItem = Item_Find( ItemText, currItem.Children )
		      if not IsNull( retItem ) then
		        ChildItems.Remove( intCycle )
		        Return 1
		      end if
		    end if
		  next
		  
		  Return 0
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemIndex as Integer)
		  if ItemIndex >= 0 and ItemIndex <= UBound( Items ) then
		    Items.Remove( ItemIndex )
		    UpdateOptions()
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemText as String)
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebTabPanelItem
		  dim intRet as Integer
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Caption = ItemText then
		      Items.Remove( intCycle )
		      UpdateOptions()
		      Return
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SelectItem(ItemIndex as Integer)
		  if ItemIndex < 0 or ItemIndex > Items.Ubound then Return
		  
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_panel > div.resp-tabs-container').find('div.resp-tab-content').css('display','none').removeClass('resp-tab-content-active');" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_panel > ul.resp-tabs-list').find('li').removeClass('resp-tab-active');" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_panel > div.resp-tabs-container').find('div.resp-tab-content[data-index=" + Str( ItemIndex ) + "]').css('display','block').addClass('resp-tab-content-active');" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "_panel > ul.resp-tabs-list').find('li[data-index=" + Str( ItemIndex ) + "]').addClass('resp-tab-active');" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim currItem as WebContainer
		    for intCycle as Integer = 0 to UBound( Items )
		      currItem = Items(intCycle).Child
		      me.ExecuteJavaScript( "window.GSjQuery('#" + currItem.ControlID + "').detach().appendTo('body');" )
		    next
		    
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').empty();" )
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "').html('" + ItemsToString() + "');" )
		    
		    for intCycle as Integer = 0 to UBound( Items )
		      currItem = Items(intCycle).Child
		      me.ExecuteJavaScript( "var sibArt = window.GSjQuery('.resp-tabs-container').children('div').eq(" + Str( intCycle, "#" ) + ");" + _
		      "window.GSjQuery('#" + currItem.ControlID + "').appendTo(sibArt);" + _
		      "window.GSjQuery('#" + currItem.ControlID + "').width('100%');" + _
		      "window.GSjQuery('#" + currItem.ControlID + "').css('position','relative').css('top','0px').css('left','0px');" )
		    next
		    
		    me.ExecuteJavaScript( "window.GSjQuery('#" + me.ControlID + "_panel').easyResponsiveTabs({" + _
		    "type: 'default', " + _
		    "width: 'auto', " + _
		    "fit: true," + _
		    "closed: 'accordion'," + _
		    "activate: function(event) {" + _
		    "var $tab = window.GSjQuery(this);" + _
		    "Xojo.triggerServerEvent('" + self.ControlID + "', 'tabchanged', [window.GSjQuery(this).attr('data-index'),$tab.text()]);" + _
		    "$name.text($tab.text());" + _
		    "}" + _
		    "});" )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TabChanged(SelectedTab as Integer)
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://github.com/samsono/Easy-Responsive-Tabs-to-Accordion
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Items() As GraffitiWebTabPanelItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.tabpanel", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIgAAACICAMAAAALZFNgAAAAmVBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHJ/5Z0oAAAAMnRSTlMAAQIDCxARFBUWGB4iIyUtLi82OTpERUlKVVZkZnR1j5GXmJqbqre8vsDByM7r7ff7/aKx3dsAAAEnSURBVHja7dvJbsIwFEBRhzAUQiClzHNLoWUG///HwQocCItE4Gehe5denYXtSNGzUkREREQO5VV7o8nUSpNRr+o9YPiDvbbafuAnOWpbbb1teO/oaJFat46mFqoRd5SPUpDDRwyy0mL9m45PLVhkQH4lIT9XR+4oCTlcL7a8Fi1/gVSM1fUiY5vMkMoFEhirhawfqiAzJAACBAgQIECAAAECBIgIpJjqj9H8dZB0FYAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIEyBtCnjJYm6514mCtM6PGzgxfq5kk5NvYOpEkpG5u4qWc4y92mgQfcZTi5/pLCtK4vWG6Mo72/V1X29lnJD19UsofuvEY7JwX9se2nseN+6GniIiIiBzqBADfTk5BnujzAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAFVBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHICqkQOAAAABnRSTlMAAhweUrK189/aAAAAL0lEQVQIW2NgwACiaWAQyKBmbGzslpaWxGAGFDVLS0tGZQABiQyQgUCQhLACAwAA05UQTlSdYbUAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
