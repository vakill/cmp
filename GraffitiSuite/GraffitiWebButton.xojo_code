#tag Class
Protected Class GraffitiWebButton
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "mousein"
		      MouseEnter
		    case "mouseout"
		      MouseExit
		    case "action"
		      Action
		    case "focus"
		      GotFocus
		    case "blur"
		      LostFocus
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    'buffer( "window.GSjQuery('#" + me.ControlID + "')." + if( Value, "addClass", "removeClass" ) + "('mdl-button--disabled');" )
		    Buffer( "window.GSjQuery('#" + me.ControlID + "')." + if( Value, "prop('disabled',false)", "prop('disabled',true)" ) + ";" )
		    Return True
		  case "style"
		    dim strExec() as String
		    dim sVal as WebStyle = value
		    
		    strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
		    if not IsNull( value ) then
		      if not IsNull( oldStyle ) then strExec.Append( "theButton.removeClass('" + oldStyle.Name + "');" )
		      
		      if not sVal.isdefaultstyle and sVal.Name <> "_Style" and sVal.Name <> "_DefaultStyle" then
		        strExec.Append( "theButton.addClass('" + sval.Name + "');" )
		      end if
		    end if
		    
		    oldStyle = value
		    
		    Buffer( strExec )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  source.Append( "<button style='text-transform:unset;line-height:unset;'" )
		  
		  if FaceStyle <> FaceStyles.Default then
		    source.Append( " class='mdl-button mdl-js-button" )
		    
		    select case FaceStyle
		    case FaceStyles.Circle
		      source.Append( " mdl-button--fab" )
		    case FaceStyles.Raised
		      source.Append( " mdl-button--raised" )
		    case FaceStyles.Icon
		      source.Append( " mdl-button--icon" )
		    end select
		    
		    if Colored then
		      source.Append( " mdl-button--colored" )
		    elseif Accented then
		      source.Append( " mdl-button--accent" )
		    end if
		    
		    if OverrideBGColor <> "" then source.Append( " mdl-color--" + OverrideBGColor )
		    if OverrideTextColor <> "" then source.Append( " mdl-color-text--" + OverrideTextColor )
		    
		    if Ripple then source.Append( " mdl-js-ripple-effect" )
		    
		    source.Append( "'" )
		  end if
		  
		  source.Append( " id='" + me.ControlID + "' style='display:none;margin-top:0px;margin-bottom:0px;min-width:36px;min-height:36px;width:" + Str( me.Width ) + "px;height:" + Str( me.Height ) + "px;'><span id='" + me.controlID + "_caption'>" + ParseForIcon() + "</span></button>" )
		  
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebButton -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "material" ).Child( "material.min.js" ), "graffitisuite" ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "material" ).Child( "material.min.css" ), "graffitisuite" ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebButton -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ParseForIcon() As String
		  dim strCapt() as String
		  
		  dim intStart as Integer = 0
		  dim intEnd as Integer = 0
		  
		  intStart = Caption.InStr( "<" )
		  
		  if intStart > 1 then strCapt.Append( EscapeString( Caption.Left( intStart - 1 ) ) )
		  
		  while intStart > 0
		    intEnd = Caption.InStr( intStart, ">" )
		    dim strTag as String = Caption.Mid( intStart + 1, (intEnd - intStart) - 1 )
		    if strTag <> "br" and strTag <> "br /" then
		      strCapt.Append( "<i class='" + EscapeString( strTag ) + "'></i>" )
		    else
		      strCapt.Append( "<br />" )
		    end if
		    intStart = Caption.InStr( intEnd, "<" )
		  wend
		  
		  strCapt.Append( Caption.Mid( intEnd + 1, Caption.Len ) )
		  
		  Return Join( strCapt, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function UpdateClasses() As String
		  dim strOut() as String
		  strOut.Append( "mdl-button mdl-js-button" )
		  
		  select case mFaceStyle
		  case FaceStyles.Circle
		    strOut.Append( " mdl-button--fab" )
		  case FaceStyles.Raised
		    strOut.Append( " mdl-button--raised" )
		  case FaceStyles.Icon
		    strOut.Append( " mdl-button--icon" )
		  end select
		  
		  if mColored then
		    strOut.Append( " mdl-button--colored" )
		  elseif mAccented then
		    strOut.Append( " mdl-button--accent" )
		  end if
		  
		  if mOverrideBGColor <> "" and mColored then strOut.Append( " mdl-color--" + mOverrideBGColor )
		  if mOverrideTextColor <> "" and mColored then strOut.Append( " mdl-color-text--" + mOverrideTextColor )
		  
		  if mRipple then strOut.Append( " mdl-js-ripple-effect" )
		  
		  Return Join( strOut )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if not LibrariesLoaded then Return
		  
		  dim strExec() as String
		  
		  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
		  if FaceStyle <> FaceStyles.Default then
		    strExec.Append( "componentHandler.upgradeElement(theButton[0]);" )
		  end if
		  
		  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
		  strExec.Append( "theButton.on('mouseenter', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mousein');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "theButton.on('mouseleave', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'mouseout');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "theButton.on('click', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'action');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "theButton.on('focus', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'focus');" )
		  strExec.Append( "});" )
		  
		  strExec.Append( "theButton.on('blur', function () {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'blur');" )
		  strExec.Append( "});" )
		  
		  me.ExecuteJavaScript( Join( strExec, "" ) )
		  
		  RunBuffer()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Action()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseEnter()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event MouseExit()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAccented
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAccented = value
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
			  
			  if value and FaceStyle <> FaceStyles.Default then
			    strExec.Append( "theButton.removeClass('mdl-button--colored')" )
			    strExec.Append( ".addClass('mdl-button--accent');" )
			  else
			    strExec.Append( "theButton.removeClass('mdl-button--accent');" )
			  end if
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Accented As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mCaption
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mCaption = value
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theButtonCaption = window.GSjQuery('#" + me.ControlID + "_caption');" )
			  strExec.Append( "theButtonCaption.empty().html(" + chr(34) + ParseForIcon() + chr(34) + ");" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Caption As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mColored
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mColored = value
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
			  
			  strExec.Append( "theButton.removeClass();" )
			  strExec.Append( "theButton.addClass('" + UpdateClasses + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Colored As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mFaceStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mFaceStyle = value
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
			  strExec.Append( "theButton.removeClass('mdl-button--fab').removeClass('mdl-button--raised').removeClass('mdl-button--icon');" )
			  
			  dim strNew as String
			  select case mFaceStyle
			  case FaceStyles.Circle
			    strNew = "mdl-button--fab"
			  case FaceStyles.Raised
			    strNew = "mdl-button--raised"
			  case FaceStyles.Icon
			    strNew = "mdl-button--icon"
			  case FaceStyles.Default
			    strExec.Append( "theButton.removeClass('mdl-button--colored').removeClass('mdl-button--accent');" )
			  end select
			  
			  strExec.Append( "theButton.addClass('" + strNew + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		FaceStyle As FaceStyles
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAccented As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mCaption As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mColored As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mFaceStyle As FaceStyles
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOverrideBGColor As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mOverrideTextColor As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRipple As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mTextStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldTextStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mOverrideBGColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mOverrideBGColor = value
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
			  
			  strExec.Append( "theButton.removeClass();" )
			  strExec.Append( "theButton.addClass('" + UpdateClasses + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		OverrideBGColor As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mOverrideTextColor
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mOverrideTextColor = value
			  
			  dim strExec() as String
			  
			  strExec.Append( "var theButton = window.GSjQuery('#" + me.ControlID + "');" )
			  
			  strExec.Append( "theButton.removeClass();" )
			  strExec.Append( "theButton.addClass('" + UpdateClasses + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		OverrideTextColor As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mRipple
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRipple = value
			End Set
		#tag EndSetter
		Ripple As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mTextStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mTextStyle = value
			  
			  dim strExec() as String
			  strExec.Append( "var theCaption = window.GSjQuery('#" + me.ControlID + "_caption');" )
			  if not IsNull( value ) then
			    if not IsNull( oldTextStyle ) then strExec.Append( "theCaption.removeClass('" + oldTextStyle.Name + "');" )
			    
			    if not value.isdefaultstyle then strExec.Append( "theCaption.addClass('" + value.Name + "');" )
			  end if
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		TextStyle As WebStyle
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webbutton", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAWhSURBVHhe7d1dbBRVFAfwe2e3tN0KiCghjYI+mGgkwcRPNMaPRPAD3FYE4cnE7xBBukRRY0JQJChpC2iiopH4AtEGuy2aktCYEBIKkZr4YFITH8AEgo1WsXa3tLtzPNs9JurOri97Z7d7/r+Hzjl3Hnbnzn/mzrQPNQAAAAAAAAAAAAAAAAAAAAAAAFArrGyrztxHXpo5Gr04X9ppqfFi7JcLX+34TdqqVBUBuGR54nLyzDLfmqXG0m081GyNnZnfO93RmCFzjqd6kGf7iJ/J9I9/uecn2VlxFQ1AffzFqz0Tec1a84wM6UD0hbHe9lSyfVBGKqZiAWiMJ7bziX9VWpWIaG96xpJ1pmt1VoZCF34A7t4Sjc0ZPcDVY/kB7ei4n8murdSyEGoAGlvXX2n96OfG2iUyBDlEZ3zrrx1P7h6QkdB4sg2DtVS3Hyc/gLULPfIONKzavEBGQhNaAGItif28uSvfQYFcCCYnDuSWSBkJRShLQCzetpkPcIe0pZHp4ltiH3+x07wdltFpKeuZy3iGH+VX2ku5jfN2Tn5PcUTmo3RPx7PSOuc8ALMffmXORN3Eaf6gWTIUjKiDKNqe7t3J78w16IktDbELo5u42pYfKI58/5Z0765T0joVka0z3g03b+Pk3yttMJ+eTPV2vp354fiojNSe745mJocGjtVdf/tJMnYZXxBNsqcAvx7Pmxw68Zm0ToXwDOCV/CUPvws/zyd/n7Q1L9XdeZjvu09LW4RtqV+x6RppnHIagKbWjfeVuvXzyf8k3dP5obRqpLs7esnQO9IG8jx/qZROOQ2AT17Jg/B86pBSncbxzFv8wDcibQFr7TIpnXK7BJBZLFWQY2OHdn0vtTojh9/9gycoKW0AulEKpxw/A1CzFAUsUVpKvYhK/KnYFp27cnIbAGuKHgRZG8pTbjXjJaBbyiD1uT+TS+2M0wDw61/RAyDj/yqlWlHy/m8OpncAoPohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAMohAJU0Q7YVhAAohwAohwAohwAohwAohwAohwAo5zQARHReygLW2HlSqpXJlJ6DSCRbdP7KxWkArDVnpSxAxlwlpVqeNQulLEQ0dqFn9+/SOeN6CSgaAGvMQ1Iq5j8oRZCic1dObgNAdFKqAPamhnjiDmnUaVi+YQFfBWukLUDGhvKv5N0+A3iRI1IG4ltgQkp1bCT6+tQiWYRnqV9Kp5wGIN3d/g2v9T9LG2RlrKWNJ0KXWLztKT71Jf+lrslmS1485eL6GYDvcvSelEXYNxtbE1ulqXmN8bZ1fOF/LG0gvmj2jR3aU+rCKZuit6DyIRtrSZzhjyr51M8H3U+Gto0nO4/KUE1pat20iMjP3fYfl6Gislm69uKhzh+ldSqEAEyl/jlr7QfSlkbEywb1WetxaMxwfnC68udy/ueTNQ9YY++RwZKIzM50T8fL0joXSgByeK3fwx+3XloIRMlUsrNVmlA4fwb4Gx/YBt4czHdQgO98qdmz1koXmtACkJOa92fuABGC/yIzkDX+avPp1nEZCU1oS8A/YTn4l4NTF8bevZPShyoi21BNDp3oi163ZNiSWcwRnCXDupBJG0tv8NL4ghkc9GU0dBUJQE5maODU5Jqlu+vOT2T5FXCRtaZJdtU4GuOT/771vJWpZEefDFZMRZaAIA0rNt5pPXs/l7fy12rmbTOH4oqpndMUv9KN8M9zPMtn+XXwW8+z/WPd7V/L7qpQNQEItGpVxe5QZdHVlZUKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgPIz5C6debxrvMae5AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACZSURBVDhPYxhwwAil4YDbv7D4PyODNJSLAv7/+7/s+6YJZ6BcMGCC0mDA5V907B8jgwaUiwEYGRnXc/oVmEC5mIAzoPAqlIkVcAUU9oEwlAsGKC5g/M/4EcokGqAYQA4Yfgb8l+XwyZODcjDAfwYGXsb/DE+hXDBASUjgRMTAUAAUfgwVgoP/jP/5QbH0bWOfFVRoFIABAwMArt4ipvigHPIAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag Enum, Name = FaceStyles, Type = Integer, Flags = &h0
		Default
		  Flat
		  Circle
		  Raised
		Icon
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Accented"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Caption"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Colored"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FaceStyle"
			Visible=true
			Group="Behavior"
			Type="FaceStyles"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Flat"
				"2 - Circle"
				"3 - Raised"
				"4 - Icon"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OverrideBGColor"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OverrideTextColor"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Ripple"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
