#tag Class
Protected Class GraffitiWebRadioButton
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "valueChange"
		      if Parameters.Ubound >= 0 then
		        mValue = Parameters(0).BooleanValue
		        ValueChange
		      end if
		    case "valueChangeDiscrete"
		      if Parameters.Ubound >= 0 then
		        mValue = Parameters(0).BooleanValue
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "enabled"
		    Buffer( "window.GSjQuery('#" + me.ControlID + "_inner')." + if( Value, "prop('disabled',false)", "prop('disabled',true)" ) + ";" )
		    Return True
		  case "style"
		    dim newStyle as WebStyle = WebStyle( Value )
		    dim strExec as String = "var theFace = window.GSjQuery('#" + me.ControlID + "').find('label')" '.find('.labelauty-checked,.labelauty-unchecked')"
		    if not IsNull( oldStyle ) then
		      if not oldStyle.IsDefaultStyle then
		        strExec = strExec + ".removeClass('" + oldStyle.Name + "')"
		      end if
		    end if
		    
		    if not IsNull( newStyle ) then
		      if not newStyle.IsDefaultStyle then
		        strExec = strExec + ".addClass('" + newStyle.Name + "')"
		      end if
		    end if
		    
		    oldStyle = newStyle
		    Buffer( strExec + ";" )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  source.Append( "<div id='" + me.ControlID + "' style='display:none;min-height:36px;width:" + Str( me.Width ) + "px;height:" + Str( me.Height ) + "px;left:" + Str( me.Left ) + "px;top:" + Str( me.Top ) + "px'>" )
		  source.Append( "<input type='radio' id='" + me.ControlID + "_inner' value='" + me.ControlID + "'" + if( me.mValue, " checked", "" ) + "></input>" )
		  source.Append( "</div>" )
		  
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  UpdateOptions
		  
		  Shown
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebRadioButton -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwoption" ).Child( "gwoption.js" ), "graffitisuite.weboption" ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwoption" ).Child( "gwoption.css" ), "graffitisuite.weboption" ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebRadioButton -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if isShown and hasLoaded and ControlAvailableInBrowser then
		    dim strExec() as String
		    strExec.Append( "var theCheck = window.GSjQuery('#" + me.ControlID + "_inner');" )
		    strExec.Append( "theCheck.labelauty({" )
		    if not me.Style.IsDefaultStyle then
		      strExec.Append( "class: '" + me.Style.Name + "'," )
		    end if
		    if HasLabel then
		      strExec.Append( "checked_label: '" + EscapeString( LabelTrue ) + "'," )
		      strExec.Append( "unchecked_label: '" + EscapeString( LabelFalse ) + "'" )
		    else
		      strExec.Append( "label: false" )
		    end if
		    strExec.Append( "});" )
		    strExec.Append( "var theLabel = window.GSjQuery('#" + me.ControlID + "_inner + label').css('width','100%').css('box-sizing','border-box');" )
		    strExec.Append( "theLabel.addClass('defaultOptionStyle');" )
		    
		    strExec.Append( "theCheck.click(function (e, data) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'valueChange', [this.checked]);" )
		    strExec.Append( "});" )
		    
		    RunBuffer()
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    Style = Style
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = Sources
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		http://lab.ejci.net/favico.js/
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private ExecutionBuffer() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mGroup
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mGroup = value
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + "_inner').attr('name','" + if( mGroup <> "", EscapeString( mGroup ), me.Name ) + "');" )
			End Set
		#tag EndSetter
		Group As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHasLabel
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHasLabel = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		HasLabel As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelFalse
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelFalse = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelFalse As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelTrue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelTrue = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelTrue As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mGroup As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHasLabel As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelFalse As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelTrue As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRadioID As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  dim strExec() as String
			  strExec.Append( "var theRadio = window.GSjQuery('#" + me.ControlID + "_inner');" )
			  strExec.Append( "var allRadios = window.GSjQuery('input[name=\" + chr(34) + EscapeString( me.Group ) + "\" + chr(34) + "]');" )
			  
			  if mValue then
			    strExec.Append( "allRadios.each(function() {" )
			    strExec.Append( "var theControl = window.GSjQuery(this);" )
			    strExec.Append( "var theControlID = theControl.attr('id').replace('_inner','');" )
			    strExec.Append( "if (theControlID != '" + me.ControlID + "') {" )
			    strExec.Append( "theControl.prop('checked', false);" )
			    strExec.Append( "var theValue = theControl.prop('checked');" )
			    strExec.Append( "Xojo.triggerServerEvent(theControlID, 'valueChangeDiscrete', [theValue]);" )
			    strExec.Append( "}" )
			    strExec.Append( "});" )
			  end if
			  
			  strExec.Append( "theRadio.prop('checked'," + Lowercase( Str( value ) ) + ");" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Value As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.weboption", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAB7FBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHI3BhY4AAAAo3RSTlMAAQIDBAUGBwgJCgsNDxAREhMUFRYXGBkaGxweHyAhIiMkJSYnKCkqLC0uLzAyMzg5Ojs9Pj9AQUJDRUZHSktMTU5PUVJUVVZXWFlbXF1eX2JjZGZnaGlrbG1vcHFzdHV3eHl8fn+Ag4WIiYuMjo+RkpWXmpudnqCjpaaqq62vsLK0wMHDxcfIyszOz9HT1dfZ3N7g4uTm6Onr7e/x8/X3+fv9AWCDlgAAB15JREFUGBm9wYtDU+cdBuD3OyQQEiSGNGqlOKoUdUBbW6cToW7dQJxVa70AAm0t1guTOhW1k9ltykUZ2AI2F0iA8v6jK7Wa38n3nVzOCX0eeKD8gWBNTTDgV/htWbX7u4fH59f52vr8+HD3vloLm6+q5dIkHU30t1RhE8XOzrCgmU9j2BThi3EW6ccLYZSZev8JS/LkPYXy8fWkWLJktw/lUXFyha5kTlTAO9W+TNeWj8Crhv/Rk2f18MJ3jZ59XQHX9iRYBvG34Y4aZJn0K7hQPcE8EuNDR9uaG7aFw9samtuODo0nmMfjAEq2a4kOVu91Ri1orGjn/VU6SL2FErXTbPF0HZypujMvaHYYJemlSebymyho51cZmnyGEozQIHHMQlGsPydpcA3FUneoWzisUDR1ZJG6WwpFUQ+oWetSKInq+YmaMYVi3KHmQRAlC31LzS0U4e/MldgHV36fZK5rKKiPub6rhEtV/2Gucyigg7nOwoMLzHUYeTUyR3oPPHknwxxvIY/gMu3iYXgUSdAuFYAjNUm774PwLPQD7R4rOPmcdhN+lEHlNO364aCJdhMVKIuKKdq9DSNfgjbP/SiTyh9oE6+AyXXaxIMom1CCNl/D4He0SYdRRpEMbeqhUbO02Y0CfNHWrr7h0dHhvq7WqA8FvEObZ9B00OYs8lG7zk/RZup8g0I+F2hzBDl8aUrfIY/a/hQNUn1bkMd/KS1XwO4UpUQlHEXv0NHtOjiqSlI6ARvfCqW9cOK/wbyu++FkP6WMD9IJSvfg5MM0C1j+AE6+pdQNQaUorFXDzBplEUYsmIV+opBUyDpA6S8wC82yKDNBmB2n9B6yJijMKxhFkixSPAwjtUjhMV7bSukPMNqZYdHS22F0hFIYr/RSiCuYRFZYgnQYJipJ4QJeSVD4CCahJEsSD8LkTxR+xK+2U8hYMLDmWKIZCwZWhkIML52j8CVMRlmyEZh8ReFTvDRLYQcMDtKFD2Cwk8Iz/CJAYREGlRm6sOyHwQsKVdjQRuEUDEboyjUYnKHQgg2DFCLQvUGX6qCro9CPDdPMWoXBXbp0Gzq1yqwJ/MyiMAZdLV3bAt19ChaAMIUO6C7RtT7oOinUAmihUAeNWqJrKQVNlMI+AD0ULGga6UEDNBaFbgBXmRWH7iI9OA9dglnDAB4x6yF0T+nBFHTjzBoHsMCsAWh89MQHzRCz5gGsM6sdmhg9iUJzlFnrgEWhFZo2etIKTRsFBT+FJmiO05MuaJop+BGgUA9NPz3pg6aBQgAhCjFortCTYWi2UQiihkIYmlF6MgpNmEINaiiEoRmlJ6PQhCnUIEQhBs0VejIMzTYKQQQo1EPTT0/6oGmgEICfQhM0x+lJFzTNFPywKLRC00ZPWqFpo6CAdWa1QxOjJ1FojjJrHcACswag8dETHzRDzJoH8IhZD6F7Rg+moBtn1jiAq8yKQ9dLD85Dl2DWMIAeChY0jfRgFzQWhW4ALRTqoFFLdC2loIlS2A9gK4UO6AboWh90nRRqAVgU7kIXpmu10N2jYOFn08xagcE9unQHOrXKrElsGKIQgS5Gl6LQ1VG4hA3vUvgEBjfpyg0YnKbQgg3VFBZgUJmhC2k/DBYpVOEXcxR2wOAQXfgQBm9SmMFL5yh8AZN/sGQ3YXKZwlm8tJ1CxoKB9ZwlmrVgYGUoxPCrBIVOmNSkWJJkCCbHKMTxSh+FuIJJdIUlyERgohIULuKVCKWDMKpfZdFWdsLoj5TCeG2SwryCUTTFIiUjMFKLFJ4g6wClj2FW85xFmQvBrIvS+8hSSxTWqmFm3WIRRi2YBdcopBSEk5TuwsmhDAvIHISTB5R6IPlXKTXDSeVN5jVSCSf7KK34YHOaUtwPR7ExOrr7BhxVJin9DXa+DKVx5BEeWKLB0qVa5PFvSssVyNFJmzPIRzX2PqXN04uNCvl8Rpt25FJztGlEAb5Y2/H+K6OjV/qPt8V8KGAPbWYUNI20Wd6CMgqnadMAgxu0eVGNsgnGaXMNJr4kbWb9KBP/97RJ+GDUTLvHFsqiYoJ2e+DgMu0e+1EG/gnaDcKJmqbdbDU8Cz6n3YSCo1Cadi+2wKNwnHZL1chjN3MsN8KT3Wnm2IW8jjHXaXhwlrnaUcAgc/2rEi5VfsdcvSjoG+aKN8OVvQnmGkFhaoyasWqUrPo+NbdRDPVPatY+ViiJ+usaNQ8UiqLGqJs/qFA0dWieutsKRVLf0CDeaaEo1kcJGoygBIM0yXyxAwXt+DJDkz6U5BjNFj6JII/IqUWadaBEu9N0sHK3o86CxqrrGFulg6VGlCw0zTziDwfaW5vqY+FwrL6ptX3gYZx5TFbDBXWZZfK5gjt7kyyDRBNc892gZ9d98KJxjp7M7oJHqjND19IdCt75zqzSlZVTPpSH/+QSS5Y64UP5qAOTLMnEAYUyi/QlWKRE71Zsiu3n5ljQ7Lnt2ETV7w5N09HUYFsAm8/a2tJz9dHCOl9bX3h0taclbOG3ZfkDoZqaUMBvwb3/A+hI10PF5NuPAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAY1BMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHJIeqNBAAAAIHRSTlMAAQIFBgcJChkaLzAxTk9YWVtdgoyOvMDBw8XOz/H19xexTLsAAACOSURBVBhXZY/ZDsIwDATHHO6RkraklDPp/v9X8lBASPWbZ7SWFwCsDqE2vmNDkaTSf9DhmmMFVczzHsBuL1+Nv2YDxuzQptSC5x6snGCSpAliMRo5QZKklko1nSCtIIHCFmwitkS4/B3lnB1CSgE8D8Du/vw9djWA4yNHBz/l22E1u/MiSWX869t0XbOub4opDFxA74zQAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Group"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasLabel"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelFalse"
			Visible=true
			Group="Behavior"
			InitialValue="No!"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelTrue"
			Visible=true
			Group="Behavior"
			InitialValue="Yes!"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
