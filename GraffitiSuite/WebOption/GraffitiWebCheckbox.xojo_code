#tag Class
Protected Class GraffitiWebCheckbox
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "valueChange"
		      if Parameters.Ubound >= 0 then
		        mValue = Parameters(0).BooleanValue
		        ValueChange
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case name
		  case "enabled"
		    Buffer( "window.GSjQuery('#" + me.ControlID + "_inner')." + if( Value, "prop('disabled',false)", "prop('disabled',true)" ) + ";" )
		    
		    Return True
		  case "style"
		    dim newStyle as WebStyle = WebStyle( Value )
		    dim strExec as String = "var theFace = window.GSjQuery('#" + me.ControlID + "').find('label')" '.find('.labelauty-checked,.labelauty-unchecked')"
		    if not IsNull( oldStyle ) then
		      if not oldStyle.IsDefaultStyle then
		        strExec = strExec + ".removeClass('" + oldStyle.Name + "')"
		      end if
		    end if
		    
		    if not IsNull( newStyle ) then
		      if not newStyle.IsDefaultStyle then
		        strExec = strExec + ".addClass('" + newStyle.Name + "')"
		      end if
		    end if
		    
		    oldStyle = newStyle
		    Buffer( strExec + ";" )
		    
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  source.Append( "<div id='" + me.ControlID + "' style='display:none;min-height:36px;width:" + Str( me.Width ) + "px;height:" + Str( me.Height ) + "px;left:" + Str( me.Left ) + "px;top:" + Str( me.Top ) + "px'>" )
		  source.Append( "<input type='checkbox' id='" + me.ControlID + "_inner' />" )
		  source.Append( "</div>" )
		  
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  isShown = True
		  
		  UpdateOptions
		  
		  Shown
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCheckbox -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "gwoption" ).Child( "gwoption.js" ), "graffitisuite.weboption" ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "gwoption" ).Child( "gwoption.css" ), "graffitisuite.weboption" ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCheckbox -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if isShown and hasLoaded and ControlAvailableInBrowser then
		    dim strExec() as String
		    strExec.Append( "var theCheck = window.GSjQuery('#" + me.ControlID + "_inner');" )
		    strExec.Append( "theCheck.labelauty({" )
		    
		    if HasLabel then
		      strExec.Append( "checked_label: '" + EscapeString( LabelTrue ) + "'," )
		      strExec.Append( "unchecked_label: '" + EscapeString( LabelFalse ) + "'" )
		    else
		      strExec.Append( "label: false" )
		    end if
		    strExec.Append( "});" )
		    strExec.Append( "var theLabel = window.GSjQuery('#" + me.ControlID + " > label').css('width','100%').css('box-sizing','border-box');" )
		    
		    strExec.Append( "theCheck.change(function () {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'valueChange', [this.checked]);" )
		    strExec.Append( "});" )
		    
		    RunBuffer()
		    
		    Buffer( strExec )
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChange()
	#tag EndHook


	#tag Note, Name = Sources
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		http://lab.ejci.net/favico.js/
		
		MIT License
	#tag EndNote


	#tag Property, Flags = &h21
		Private ExecutionBuffer() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHasLabel
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHasLabel = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		HasLabel As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelFalse
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelFalse = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelFalse As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLabelTrue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabelTrue = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		LabelTrue As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mHasLabel As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelFalse As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelTrue As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRadioID As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private oldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  Buffer( "window.GSjQuery('#" + me.ControlID + "_inner')." + if( not Value, "removeAttr('checked')", "prop('checked',true)" ) + ";" )
			End Set
		#tag EndSetter
		Value As Boolean
	#tag EndComputedProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webcheckbox", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAi0SURBVHhe7d1/bFtXFQfwc57tOr+6NtlgKAIJgUTLH9UQKjRdx0In0XSt0yRTpsHWrpMQIJCqLtnKOvijAiQYMP9o+W8SICjd0Cj1j9ob0DEhhEpAQjAGowGGhFA7oaq0TeJnJ7Hv4d74hLJ0TZP4vvjZPp8/6ntOWq3z+frd9xrnGYQQQgghhBBCCCGEEEIIIYQQQgghhBBCNArkR1+4dc+htZPB6Xdw2XCC5Ey5ycQbXPpCzQLQERm9jRzoUwg7AGmLbnUj4NrKVxsYkdLP+nkC+Kv+/31Jj+CMm4r+jr+66lY9AOGBg+92IPAFRPgUt5qeDsOroPDLhUz0JLdWzaoGoHVg9Kt68E9yKRYgohcdRYfypxN/5pbnVicAvUeCbZ2Tz+nVcKUhbkQfDa4g4l43Gc1xy1MBfvRM69CBd64Jz74AiH3cEovQr8gW/fBgcGPPpdK5sd9Wut5x+NEriBR6Vv+6lWuxRPoE8VvtQ6MPcekZT7eAtsFRc9j/eKUSK4GE2/Pp6C+4tM6zI0DbwMgT+kGGXyVC+i4vPeHJOcC63Yc7ywH1Y314CXNLrNz64Iaeq6XxsTGurfLkCDATKn5RD/8WLkWV9KXzIThyxJNZebQFOPKPPFZhd9sfpjw5IbQegPahR++RV78XqJ8XVlkPgCJnBy+FXREvtgH7WwDBHbwSNiG0tv1xYhNX1nhwDkDdvBC2kbL+3NoPAIIEwCvk+D8ACHgbL4V91p9bD7YAUU8kAE1OAmAdnQWiP+mFZ9/AsUkCYAkRXSQFA24qvs1Nxze5qdh2BeU7Cej3/Ft8SQJggR7+JX32GylkYhluzSmmjv4aVDBCBK9wy3ckAFXSr/DLoPTwU/G3fPdOIfPNCwTQr0NitgXfkQBUQQ/1KhFGCqfji36rtpiO/YsCTkSH5TVu+YYEYIX0q3qCECN6uGe5tajiqeg/lUP9+s+Nc8sXJAArQTBFCvuLqdivuLMk06cS/yjPlsx39f5e6dSeBGD53DKU+4uZ6C+5XpaZ3LG/BVRZbwcwwa2akgAsTxFR9U+nj1Z1jT+ZOTquTyA+z2VNSQCWimAGEXfnk4mXuVMVx3GSvKwpCcCSUAkBduWTUSvDN7Awo3eB2pMA3Azp0z1F9+bTsZ9zxwoVDg3wsqYkADeBFOjLZxIvcWlFePDx9xLSN7isKQnAIhBhRz7ztPXhO6ByCNjJrZqSANyQ6ssnY2e4sOLa8GEDt2rO7wEoEtF39HH4A0phr66PVdpec/rcVOJnXFjhx+Eb/g0AweuBUvm+Qjr+STcZf8X8w4ubih3UB+ZH+Hd4g2Cnm3q6KYZv+DMAevhOGSKT2aMvcud/3FT0e6BoP5d2Id3rpmM/5coKPw/f8F8AePhT2dg57lzHzcS/D6Qe5tKS8i59pPkJF1b4ffiGvwKwhOHPc9OJ4/osfS+XVcJdbur6o0016mH4hn8CsIzhz9Nn6SeQqLofmiTarbeVphy+4Y8ArGD48/Lp+LMK4EEul0epiJuOv8CVFfU0fMMXAVBY3reS4c8rpmLPKaRPcLk0iBE3k7B6J656G75R8wAQwLfn3jxZpWIy/kMF+ACXN9Nv+zZs9Th8wwdHANXBi6oVU9HnFahFQ0Ck9ripWJZLK+p1+EbNA4DgXOKlFcVU4vkbbQdEuKeQTpzm0op6Hr7hgyMA7VszOPo+Lqww28HCqwPzQxuFdFSGv4APAoBrgwAn2oceezs3rDBXB6jDZdYEanDhD21UqxGGb/ggAHM2E1GuY/jJt3FtRT4V/0Fodk1XIZVIc8uKRhm+4ZcAGJvVbDHXsfOA1RBczT11mZdWNNLwDT8FwFybf0iFQ1nzYRLc8ZVGG77hrwAYCB9WQciuHTx8K3d8oRGHb/gvABVbyjCTvWV4pIvrmmrU4Rt+DYDRU5rFnLnvMNc1URl+uSGHb/g5AGY76JkNTusQfLYmIbg2fGzI4Rv+DoCBuHU21JJdN3BwPXdWRTMM3/B/AObgnbMYyHbe/8Q6bniqWYZv1EkA5mybnpnJdT10wNMbUTfT8I16CoDZDrYVp4K5rp3ehKDZhm/UVwAMxLuKLaGs+ZhZ7ljRjMM36i8AFR8pOKUs3P85K+8laNbhG/UaAA3vbpsJ52DvY+3cWJFmHr5RxwHQUIdgknLwsZWFoNmHb9R3AAyE3tZ2lYXIp9u4syQy/Ir6D4Cmh/jR1mBHFoZHWrm1KBn+NQ0RAAMBtrfOYg72HzGfvXtDMvw3a5gAGIg6BFcmctC7/y1DIMO/XkMFwEDEe1o7u7ItA6Pv4tac1j2PbpbhX08fOe1qGxz1xd2v9F/iCgId149deuhTuvWZylfqGNFhNx3/OldWNNwRYJ5O9nr96wE9fPP28PofvkcaNgBiaSQATc56AMzey0thmRfPrRdHgPP8KCzTVzjWn1v7ASAJgGdQXeCVNdYDgAi++1iUBkFuAP/Ca2vsHwGUsnprVVFhfnYSTsYLXFpjPQBueN0ZcxcGLoUtiFbvZTTP/hHgR1+aIcDjXAk7pp1y6RSvrbIfAE2h8xVeCgv04f+p/Olj/+bSqgA/WlU+d/ZycENPSF+23M0tsUJEcLEQgmF4bazELas8CYBRGh97Obhx610I8B5uiZVAfLh0KvYqV9Z5sgXM0/uWuZWrbz4jr94g0eOFVNSTvX+epwEw+xYh7SMAT/avBpfIp+NRXnvG0wAYhWR8LKjKvX7/GHU/0S+ag24qNsKlpzw7B/h/M+O/uVTa9METoXJwIyC8n9tiIaLXCZxHCqnYql1GW39H0M20DI70IuAh/R/eza2mpy/zLoLjfK2QjMa5tWpWPQDz2oZG7gCCfv1XiOhyS6XbPPTl3Rv6LC+nj/e5wu1TOXjmmVn+0qqqWQDeZOeBcDi0phtD5W5Ugdu523D0WX0eHTwfCKgLEyfj/+G2EEIIIYQQQgghhBBCCCGEEEIIIYQQQgghRJUA/guA/AbkibOPUAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAD4SURBVDhP3ZI7DkFREIbnXBRIbECh0Sj0lwUQFZV1eG3DI3pRo7oo2ACNlgWIBQjxKA5jzsl4HO9O4ktu5p/J/f87mVz4OYKrxp/OF1FAkFsDRLEGPPZ23dqER5pLgC9dGFEX4/YpCLBCKaP7fn3OI7C4qqi3ZgV9LQBuy+ZWcw14AQLOjlKGSLR5ZPAYgDBmpc0oD6nble8xAsgw3DqVuEAs3ZrpPi3aP8uvGdxtIGK+TC6xcarlb8wKI0AdCcHqqJBvzIqHG5xDvJn84JNZcQ1AXLDSIQJEklsDMixZalxcwROxp4AiTFIFPX3oR2runGqD9P8AcAKUyGN+M5QGnAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HasLabel"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelFalse"
			Visible=true
			Group="Behavior"
			InitialValue="No!"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LabelTrue"
			Visible=true
			Group="Behavior"
			InitialValue="Yes!"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
