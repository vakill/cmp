#tag Class
Protected Class GraffitiWebQRReader
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "qrData"
		      if Parameters.Ubound >= 0 then
		        DataAvailable( Parameters(0).StringValue )
		      end if
		    case "error"
		      if Parameters.Ubound >= 0 then Error( Parameters(0).StringValue )
		    case "videoError"
		      if Parameters.Ubound >= 0 then Error( Parameters(0).StringValue )
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim source() as String
		  source.Append( "<div id='" + me.ControlID + "' style='width:" + Str( me.Width ) + "px;height:" + Str( me.Height ) + "px;'>" )
		  source.Append( "</div>" )
		  Return Join( source, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  me.Style = me.Style
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCamera -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "codescanner" ).Child( "jsqrcode-combined.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "codescanner" ).Child( "html5-qrcode.min.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCamera -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Play()
		  dim strExec() as String
		  strExec.Append( "GSjQuery('#" + me.ControlID + "').html5_qrcode(" )
		  strExec.Append( "function(data) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'qrData', [data]);" )
		  strExec.Append( "}," )
		  strExec.Append( "function(error) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'error', [error]);" )
		  strExec.Append( "}," )
		  strExec.Append( "function(videoError) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.controlID + "', 'error', [videoError]);" )
		  strExec.Append( "});" )
		  
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Stop()
		  dim strExec() as String
		  strExec.Append( "try {" )
		  strExec.Append( "GSjQuery('#" + me.ControlID + "').html5_qrcode_stop();" )
		  strExec.Append( "GSjQuery('#" + me.ControlID + "').empty();" )
		  strExec.Append( "} catch(e) {" )
		  strExec.Append( "console.log(e);" )
		  strExec.Append( "}" )
		  Buffer( strExec )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event DataAvailable(theData as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Error(errorReason as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Licensed under MIT
		https://github.com/dwa012/html5-qrcode
		
		Copyright © 2013 Daniel Ward
		
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webbarcodereader", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANlSURBVHhe7d2/axNhHMfx5zlFTIogLgqOooMOKg5O4iSior2ii6DgXyC9FkEHESfBSlIEB50UheogvaI4uAluYrWKm4OroG5Jqm3v8YrfJdwlhfYu99Tv+wUPeb70SXoJH3J88+SHAQAAAAAAAAAAAAAAAAD8L6xclmLo7Og+mfrhT/Kj9eLud6kyfDve1vPJLzItTakBqIdjTqZecMbd6cTNK1J2GQqjC87Yx1L6wboD7enmnFSlCOQSShEA5QiAcgRAOQKgHAFQrsI20F1P25xXUhQnse+MtbnBXlMbaJNDMiuMNXaPc3ZKyqwBtIGVBcAad7EVN59IWZj6cLRURgDacaPwx6o+Eu03zn6UMovXAVA2AqAcAVCOAChHAJTzsguoh9E158wRKTM6M82TMs2oogtI/+ez9I5ukbKbtZ86ceOqVF3oAnpIH8xt1toTucOY47LMG86a3bnHmg7j3A5Z5iVOAcoRAOUIgHIEQDkCoJy3AUg7gc+5w9rS3ym7Cr9yjzUdxgYLssZL7AYKdgOhEgFQjgAoRwCUIwDKEQDlKmsDq8CHQ7N4BlCOAChHAJQjAMoRAOUIgHLltoHDY5dk6oXEmq/zceOtlF1q56KddsEek9IL7ZnGQ5kCAAAAQEFKbQNrw9EtmXrBBWZufrr5VMoutTPjh41NQimL9K0z07wv8y5Dpy9vT4INo1JmBMnSZL/vNi4C28GirO1g59yjNAC5r4fwrmBUjgAoRwCUIwDKEQDlCEDJbGC2Lv8UTd4IXLJLllWmsjZwvX04tBK0gSgbAVCOAChHAJQjAMoRAOW8DEAtjCbSdu53ryHLBmv5J2N6jLS9/CCr1h1/nwGs3ZQ7jNn4b8FgtacnZ3sNWbIucQpQjgAoRwCUIwDKEQDlvNwNrI+MnzIu2StlRjtuTsg0I20TB/+TMWGUe5tr1V5s3TMvH7SlLAXbwaKK7wr2AacA5QiAcgRAOQKgHAFQrrIuwDk3lf7xvZSFSRN9e9BdwOZw7LxMMwKT/GzHk6+lzOh33fm4MSXT0lQWgCqUFYBaGM1aYw9K2YUPh8JrBEA5AqAcAVCOAChHAJQrd4fr6I1K3sDZ05ubizLL1+94+113pfu52uuudLwAAAAAAAAAAAAAAAAAAAAAAAAAAH2M+Qs4N6t/G4qeFgAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAExSURBVDhP7ZK9SsRQEIXPuVlEBEFr18ZGwRfYxUIs7Bf8ew23ECx9All8CJG1EatY2qWwFbayURsLERsF1xnn3gyu1zyAjR+EmTlJJpNzB38OPSZmev1jTxMqehrIdSUWXIJAq/eLwdBLBI81ih3PLGdHA5YU3HIlaQHseJXIG4D39lRJxSOolYuGlgrM5lrNrwY24lhGonrtZSJqhJ57mdFoEIrWGRlO6nHxotTbqEHDUdTSdD/ITJzu7e96WjOWKrTCigBzqZngOcpvl4ObdN8oPCamlrslyVeCbWrY1gIPFg9N65vBGzbZGgPmP0bVlb/SNDEeXTyqzDDFEGTb5u268k3DAxV5ouDOy4R8jg8Eshev+AGXE1kDW5jViYmTnYg+2G9tFsrFuFgu/5MAvgDKO3BkOJ2gvQAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
