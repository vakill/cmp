#tag Class
Protected Class GraffitiControlWrapper
Inherits WebControlWrapper
	#tag Event
		Sub FrameworkPropertyChanged(Name as String, Value as Variant)
		  if FrameworkPropertyChanged( Name, Value ) then Return
		  
		  dim strExec() as string
		  strExec.Append( "/* Loading " + me.Name + " which is a " + me.JavascriptNamespace + " */" )
		  strExec.Append( "var theElement = document.getElementById('" + me.ControlID + "');" )
		  strExec.Append( "if (typeof(theElement) !== 'undefined' && theElement !== null) {" )
		  
		  select case Name
		  case "cursor"
		    strExec.Append( "theElement.style.cursor = '" + CursorToString( Value.IntegerValue ) + "';" )
		  case "left"
		    strExec.Append( "theElement.style.left='" + Str( value.IntegerValue ) + "px';" )
		  case "top"
		    strExec.Append( "theElement.style.top='" + Str( value.IntegerValue ) + "px';" )
		  case "width"
		    strExec.Append( "theElement.style.width='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.maxWidth='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.minWidth='" + Str( Value.IntegerValue ) + "px';" )
		  case "height"
		    strExec.Append( "theElement.style.height='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.maxHeight='" + Str( value.IntegerValue ) + "px';" )
		    strExec.Append( "theElement.style.minHeight='" + Str( Value.IntegerValue ) + "px';" )
		  case "Visible"
		    strExec.Append( "theElement.style.display = '" + if( Value, "block", "none" ) + "';" )
		    strExec.Append( "theElement.style.visibility = 'visible';" )
		  case else
		    return
		  end select
		  
		  strExec.Append( "}" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  '// Do not implement, or you break everything.  Seriously.  Why on Earth did Xojo even expose this for the WebSDK?
		End Sub
	#tag EndEvent

	#tag Event
		Sub Shown()
		  if hasLoaded then
		    Shown()
		  else
		    hasLoaded = True
		    Open()
		    Shown()
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Shared Function AddBootstrap(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "datepcss_fixes" ) then
		    strOut.Append( "<style>.datepicker table tr td, .datepicker table tr th {padding: 0 !important;}</style>" )
		    RegisterLibrary( CurrentSession, "graffitisuite", "datepcss_fixes" )
		  end if
		  
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "bootstrap.js" ) then
		    try
		      
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        strOut.Append( "<script src='" + strOffload + "/bootstrap.min.js'></script>" )
		        strOut.Append( "<link rel='stylesheet' href='" + strOffload + "/bootstrap.xojocompat.css'>" )
		        strOut.Append( "<link rel='stylesheet' href='" + strOffload + "/bootstrap-theme.min.css'>" )
		      else
		        dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "bootstrap.min.js" )
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		        
		        theFile = theFile.Parent.Child( "bootstrap.xojocompat.css" )
		        dim cssFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<link rel='stylesheet' href='" + cssFile.URL + "'>" )
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = cssFile
		        
		        theFile = theFile.Parent.Child( "bootstrap-theme.min.css" )
		        dim cssFile2 as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<link rel='stylesheet' href='" + cssFile2.URL + "'>" )
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = cssFile2
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "bootstrap.js" )
		      
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddBootstrap4(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "datepcss_fixes" ) then
		    strOut.Append( "<style>.datepicker table tr td, .datepicker table tr th {padding: 0 !important;}</style>" )
		    RegisterLibrary( CurrentSession, "graffitisuite", "datepcss_fixes" )
		  end if
		  
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "bootstrap4.js" ) then
		    try
		      
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        strOut.Append( "<script src='" + strOffload + "/bootstrap4/bootstrap.bundle.min.js'></script>" )
		        strOut.Append( "<link rel='stylesheet' href='" + strOffload + "/bootstrap4/bootstrap.min.css'>" )
		      else
		        dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).child( "bootstrap4" ).Child( "bootstrap.bundle.min.js" )
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		        
		        theFile = theFile.Parent.Child( "popper.min.js" )
		        dim jsFile2 as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile2.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile2
		        
		        theFile = theFile.Parent.Child( "bootstrap.min.css" )
		        dim cssFile2 as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<link rel='stylesheet' href='" + cssFile2.URL + "'>" )
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = cssFile2
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "bootstrap4.js" )
		      
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddCSSFile(CurrentSession as WebSession, theFile as FolderItem, theNamespace as String) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, theNamespace, theFile.Name ) then
		    try
		      dim jsFile as WebFile = WebFile.Open( theFile, False )
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        dim strPath as String = GraffitiControlWrapper.GetFilePath( theFile )
		        strOut.Append( "<link rel='stylesheet' href='" + strOffload + strPath + "'>" )
		      else
		        strOut.Append( "<link rel='stylesheet' href='" + jsFile.URL + "'>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		      end if
		      
		      RegisterLibrary( CurrentSession, theNamespace, theFile.Name )
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddDatepicker(CurrentSession as WebSession) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "datepicker" ) then
		    try
		      dim theFilePath as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "datepicker" )
		      strOut.Append( AddJSFile( CurrentSession, theFilePath.Child( "bootstrap-datepicker.js" ), "graffitisuite" ) )
		      strOut.Append( AddCSSFile( CurrentSession, theFilePath.Child( "bootstrap-datepicker.css" ), "graffitisuite" ) )
		      
		      if not IsLibraryRegistered( CurrentSession, "graffitisuite", "datepickerlangs" ) then
		        dim strLangs() as String = Array( "ar","az","bg","bs","ca","cs","cy","da","de","el","en-GB","es","et","eu","fa","fi","fo","fr","fr-CH","gl","he","hr","hu","hy","id","is","it","ja","ka","kh","kk","kr","lt","lv","mk","ms","nb","nl","nl-BE","no","pl","pt-BR","pt","ro","rs","rs-latin","ru","sk","sl","sq","sr","sr-latin","sv","sw","th","tr","uk","vi","zh-CN","zh-TW")
		        for i as Integer = 0 to UBound( strLangs )
		          dim currLang as String = "bootstrap-datepicker." + strLangs(i) + ".min.js"
		          dim fLib as FolderItem = theFilePath.Child( "langs" ).Child( currLang )
		          strOut.Append( AddJSFile( CurrentSession, fLib, JavascriptNamespace + "." + strLangs(i) ) )
		        next
		        RegisterLibrary( CurrentSession, "graffitisuite", "datepickerlangs" )
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "datepicker" )
		      
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddFontAwesome4(CurrentSession as WebSession) As String
		  dim strOut() as string
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "fontawesome" ) then
		    
		    if IsNull( session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		    
		    dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		    if strOffload.LenB > 0 then
		      strOut.Append( "<link rel='stylesheet' href='" + strOffload + "/fontawesome4/css/remote/font-awesome.min.css'>" )
		      RegisterLibrary( CurrentSession, "graffitisuite", "fontawesome" )
		    else
		      dim fLib2 as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).child( "fontawesome4" ).Child( "css" ).Child( "local" ).Child( "font-awesome.min.css" )
		      if not IsNull( fLib2 ) and fLib2.Exists Then
		        dim jsFile as WebFile = WebFile.Open( fLib2 )
		        dim theData as String = jsFile.Data
		        
		        dim faFonts() as Dictionary
		        faFonts.Append( new Dictionary( "tag" : "OTF", "lib" : "fontawesomeOTF", "file" : "FontAwesome.otf" ) )
		        faFonts.Append( new Dictionary( "tag" : "EOT", "lib" : "fontawesomeEOT", "file" : "fontawesome-webfont.eot" ) )
		        faFonts.Append( new Dictionary( "tag" : "SVG", "lib" : "fontawesomeSVG", "file" : "fontawesome-webfont.svg" ) )
		        faFonts.Append( new Dictionary( "tag" : "TTF", "lib" : "fontawesomeTTF", "file" : "fontawesome-webfont.ttf" ) )
		        faFonts.Append( new Dictionary( "tag" : "WOFF", "lib" : "fontawesomeWOFF", "file" : "fontawesome-webfont.woff" ) )
		        faFonts.Append( new Dictionary( "tag" : "WOFF2", "lib" : "fontawesomeWOFF2", "file" : "fontawesome-webfont.woff2" ) )
		        
		        for intCycle as Integer = 0 to faFonts.Ubound
		          dim currDic as Dictionary = faFonts(intCycle)
		          dim fFont as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "fontawesome4" ).child( "fonts" ).Child( currDic.Value( "file" ) )
		          if not IsNull( fFont ) and fFont.Exists then
		            dim wfFont as WebFile = WebFile.Open( fFont )
		            Session.GraffitiSuiteInstances.Value( currDic.Value( "lib" ) ) = wfFont
		            theData = ReplaceAll( theData, "%" + currDic.Value( "tag" ) + "%", wfFont.URL )
		          end if
		        next
		        
		        strOut.Append( "<style>" + theData + "</style>" )
		        Session.GraffitiSuiteInstances.Value( "fontawesome" ) = jsFile
		        RegisterLibrary( CurrentSession, "graffitisuite", "fontawesome" )
		      end if
		    end if
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddFontAwesome5(CurrentSession as WebSession) As String
		  dim strOut() as string
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "fontawesome" ) then
		    
		    if IsNull( session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		    
		    dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		    if strOffload.LenB > 0 then
		      strOut.Append( "<link rel='stylesheet' href='" + strOffload + "/fontawesome5/fontawesome-all.min.css'>" )
		      RegisterLibrary( CurrentSession, "graffitisuite", "fontawesome" )
		    else
		      dim fLib2 as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).child( "fontawesome5" ).Child( "fontawesome-all.min.css" )
		      if not IsNull( fLib2 ) and fLib2.Exists Then
		        dim jsFile as WebFile = WebFile.Open( fLib2 )
		        dim theData as String = jsFile.Data
		        
		        dim arrFiles() as String = Array( "fa-brands-400.eot", _
		        "fa-brands-400.svg", _
		        "fa-brands-400.ttf", _
		        "fa-brands-400.woff", _
		        "fa-brands-400.woff2", _
		        "fa-regular-400.eot", _
		        "fa-regular-400.svg", _
		        "fa-regular-400.ttf", _
		        "fa-regular-400.woff", _
		        "fa-regular-400.woff3", _
		        "fa-solid-900.eot", _
		        "fa-solid-900.svg", _
		        "fa-solid-900.ttf", _
		        "fa-solid-900.woff", _
		        "fa-solid-900.woff2" )
		        
		        dim currFilename as String
		        for intCycle as Integer = 0 to arrFiles.Ubound
		          currFilename = arrFiles(intCycle)
		          dim fFont as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "fontawesome5" ).child( "webfonts" ).Child( currFilename )
		          if not IsNull( fFont ) and fFont.Exists then
		            dim wfFont as WebFile = WebFile.Open( fFont )
		            Session.GraffitiSuiteInstances.Value( currFilename ) = wfFont
		            theData = ReplaceAll( theData, "webfonts/" + currFilename, wfFont.URL )
		          end if
		        next
		        
		        strOut.Append( "<style>" + theData + "</style>" )
		        Session.GraffitiSuiteInstances.Value( "fontawesome" ) = jsFile
		        RegisterLibrary( CurrentSession, "graffitisuite", "fontawesome" )
		      end if
		    end if
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddjQuery(CurrentSession as WebSession) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "jquery.min.js" ) then
		    try
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        strOut.Append( "<script src='" + strOffload + "/jQuery/jquery.min.js'></script>" )
		      else
		        dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "jQuery" ).Child( "jquery.min.js" )
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "jquery.min.js" )
		      
		      strOut.Append( "<script>if( typeof( window.GSjQuery ) == 'undefined') {if( typeof($) !== 'undefined') {window.GSjQuery = $.noConflict();}}</script>" )
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddjQueryUI(CurrentSession as WebSession) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "jquery-ui.js" ) then
		    try
		      
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        strOut.Append( "<script src='" + strOffload + "/jQuery/jquery-ui.js'></script>" )
		      else
		        dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "jQuery" ).Child( "jquery-ui.js" )
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "jquery-ui.js" )
		      
		      dim touchFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "jQuery" ).Child( "jquery.ui.touch-punch.min.js" )
		      strOut.Append( AddJSFile( CurrentSession, touchFile, touchFile.Name ) )
		      
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddJSFile(CurrentSession as WebSession, theFile as FolderItem, theNamespace as String) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, theNamespace, theFile.Name ) then
		    try
		      if IsNull( theFile ) then break
		      
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        dim strPath as String = GraffitiControlWrapper.GetFilePath( theFile )
		        strOut.Append( "<script src='" + strOffload + strPath + "'></script>" )
		      else
		        
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        
		        '// ===== ATTENTION =====
		        '// If you see a Breakpoint here, you likely need to update
		        '// your scripts directory.  There may be files or folders missing.
		        if IsNull( jsFile ) then break
		        
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		      end if
		      
		      RegisterLibrary( CurrentSession, theNamespace, theFile.Name )
		      
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddMoment(CurrentSession as WebSession) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "moment.js" ) then
		    try
		      
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        strOut.Append( "<script src='" + strOffload + "/moment/moment-with-locales.js'></script>" )
		      else
		        dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "moment" ).Child( "moment-with-locales.js" )
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "moment.js" )
		      
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function AddPromisePolyfill(CurrentSession as WebSession) As String
		  dim strOut() as String
		  if not IsLibraryRegistered( CurrentSession, "graffitisuite", "promise.polyfill.min.js" ) then
		    try
		      dim strOffload as String = GraffitiControlWrapper.GetOffloadURL()
		      if strOffload.LenB > 0 then
		        strOut.Append( "<script src='" + strOffload + "/promise.polyfill.min.js'></script>" )
		      else
		        dim theFile as FolderItem = app.ExecutableFile.Parent.Child( "scripts" ).Child( "promise.polyfill.min.js" )
		        dim jsFile as WebFile = WebFile.Open( theFile, False )
		        strOut.Append( "<script src='" + jsFile.URL + "'></script>" )
		        if IsNull( Session.GraffitiSuiteInstances ) then Session.GraffitiSuiteInstances = new Dictionary
		        Session.GraffitiSuiteInstances.Value( theFile.Name ) = jsFile
		      end if
		      
		      RegisterLibrary( CurrentSession, "graffitisuite", "promise.polyfill.min.js" )
		    catch
		      
		    end try
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Buffer(toBuffer() as String)
		  Buffer( Join( toBuffer, "" ) )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Buffer(toBuffer as String)
		  if hasLoaded then
		    me.ExecuteJavaScript( toBuffer )
		  else
		    ExecutionBuffer.Append( toBuffer )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function CBool(strValue as String) As Boolean
		  if strValue = "true" or strValue = "t" or strValue = "y" or strValue = "1" then Return True
		  
		  Return false
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ClearBuffer()
		  redim ExecutionBuffer(-1)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function ColorFromHex(theHex as String) As Color
		  theHex = theHex.ReplaceAll( "#", "" )
		  
		  dim colorRed, colorGreen, colorBlue, alphaValue as Integer
		  if theHex.Len = 3 then
		    '// RGB
		    colorRed = Val( "&H" + theHex.Mid( 1, 1 ) + theHex.Mid( 1, 1 ) )
		    colorGreen = Val( "&H" + theHex.Mid( 2, 1 ) + theHex.Mid( 2, 1 ) )
		    colorBlue = Val( "&H" + theHex.Mid( 3, 1 ) + theHex.Mid( 3, 1 ) )
		    alphaValue = 0
		  elseif theHex.Len = 6 then
		    '// RRGGBB
		    colorRed = Val( "&H" + theHex.Mid( 1, 2 ) )
		    colorGreen = Val( "&H" + theHex.Mid( 3, 2 ) )
		    colorBlue = Val( "&H" + theHex.Mid( 5, 2 ) )
		    alphaValue = 0
		  ElseIf theHex.Len = 8 then
		    '// RRGGBBAA
		    colorRed = Val( "&H" + theHex.Mid( 1, 2 ) )
		    colorGreen = Val( "&H" + theHex.Mid( 3, 2 ) )
		    colorBlue = Val( "&H" + theHex.Mid( 5, 2 ) )
		    alphaValue = Val( "&H" + theHex.Mid( 7, 2 ) )
		  end if
		  
		  Return RGB( colorRed, colorGreen, colorBlue, alphaValue )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function ColorToHex(c as Color) As String
		  dim r as string = Hex( c.Red )
		  dim g as String = Hex( c.Green )
		  dim b as String = Hex( c.Blue )
		  
		  if r.Len = 1 then r = "0" + r
		  if g.Len = 1 then g = "0" + g
		  if b.Len = 1 then b = "0" + b
		  
		  Return "#" + r + g + b
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function ColorToRGBAString(c as Color) As String
		  Return "rgba(" + Str( c.Red, "#" ) + "," + _
		  Str( c.Green, "#" ) + "," + _
		  Str( c.Blue, "#" ) + "," + _
		  Str( 1 - (c.Alpha / 255), "0.00" ) + _
		  ")"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function CursorToString(intCursor as Integer) As String
		  dim strCursor() as String = Array( "auto", "default", "pointer", "text", "wait", _
		  "help", "move", "n-resize", "s-resize", "e-resize", "w-resize", "ne-resize", _
		  "nw-resize", "se-resize", "sw-resize", "ew-resize", "ns-resize", "progress", _
		  "no-drop", "not-allowed", "vertical-text", "crosshair" )
		  
		  Return strCursor( intCursor )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function DebugRemote() As Boolean
		  dim sessionAttributes() as Introspection.AttributeInfo = Introspection.GetType(Session).GetAttributes
		  for each a as Introspection.AttributeInfo in sessionAttributes
		    if a.name = "DebugRemote" then Return CBool( a.Value )
		  next
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function DeployRemote() As Boolean
		  dim sessionAttributes() as Introspection.AttributeInfo = Introspection.GetType(Session).GetAttributes
		  for each a as Introspection.AttributeInfo in sessionAttributes
		    if a.name = "DeployRemote" then Return CBool( a.Value )
		  next
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EscapeString(InputString as String) As String
		  dim strHold as String = ReplaceAll( InputString, "\", "\\" )
		  strHold = ReplaceAll( strHold, chr( 34 ), "\" + chr( 34 ) )
		  strHold = ReplaceAll( strHold, "'", "\'" )
		  strHold = ReplaceLineEndings( strHold, "\r\n" )
		  strHold = ReplaceAll( strHold, chr( 10 ), "\r" )
		  strHold = ReplaceAll( strHold, chr( 13 ), "\n" )
		  
		  Return strHold
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function GenerateUUID() As String
		  dim strRet as String
		  
		  dim strInput as String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		  dim intInputLen as Integer = strInput.Len
		  
		  Dim r as New Random
		  for intCycle as integer = 1 To 25
		    strRet = strRet + strInput.Mid( r.InRange( 1, intInputLen ), 1 )
		  Next
		  return strRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function GetFilePath(theFile as FolderItem) As String
		  if IsNull( theFile ) then Return ""
		  
		  dim fTemp as new Xojo.IO.FolderItem( theFile.NativePath.ToText )
		  if IsNull( fTemp ) then Return ""
		  
		  dim strPath as String
		  
		  while fTemp.Name <> "scripts"
		    strPath = "/" + fTemp.Name + strPath
		    fTemp = fTemp.Parent
		  wend
		  
		  Return strPath
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function GetOffloadURL() As String
		  #if DebugBuild then
		    if not DebugRemote then Return ""
		  #else
		    if not DeployRemote then Return ""
		  #endif
		  
		  dim sessionAttributes() as Introspection.AttributeInfo = Introspection.GetType(Session).GetAttributes
		  for each a as Introspection.AttributeInfo in sessionAttributes
		    if Session.Secure then
		      if a.name = "CDNSecure" then Return a.Value
		    else
		      if a.Name = "CDNUnsecure" then Return a.Value
		    end if
		  next
		  
		  Return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ParseDate_GS(theValue as String, byRef ParseTo as Date) As Boolean
		  if not IsNull( ParseTo ) then
		    if CountFields( theValue, "/" ) >= 3 then
		      '// mm/dd/yyyy
		      ParseTo.Month = NthField( theValue, "/", 1 ).Val
		      ParseTo.Day = NthField( theValue, "/", 2 ).Val
		      ParseTo.Year = NthField( theValue, "/", 3 ).Val
		      Return True
		    ElseIf CountFields( theValue, "-" ) >= 3 then
		      dim intYear as Integer = NthField( theValue, "-", 1 ).Val
		      if intYear > 32 then
		        '// yyyy-mm-dd
		        '// ISO 8601
		        ParseTo.Year = NthField( theValue, "-", 1 ).Val
		        ParseTo.Month = NthField( theValue, "-", 2 ).Val
		        ParseTo.Day = NthField( theValue, "-", 3 ).Val
		        Return True
		      else
		        '// dd-mm-yyyy
		        ParseTo.Day = NthField( theValue, "-", 1 ).Val
		        ParseTo.Month = NthField( theValue, "-", 2 ).Val
		        ParseTo.Year = NthField( theValue, "-", 3 ).Val
		        Return True
		      end if
		    ElseIf CountFields( theValue, "." ) >= 3 then
		      '// dd.mm.yyyy
		      ParseTo.Day = NthField( theValue, ".", 1 ).Val
		      ParseTo.Month = NthField( theValue, ".", 2 ).Val
		      ParseTo.Year = NthField( theValue, ".", 3 ).Val
		      Return True
		    end if
		  end if
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub RunBuffer()
		  ExecuteJavaScript( Join( ExecutionBuffer, "" ) )
		  ClearBuffer()
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LoadComplete()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Property, Flags = &h21
		Private ExecutionBuffer() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h1
		#tag Getter
			Get
			  return mhasLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mhasLoaded = value
			End Set
		#tag EndSetter
		Protected hasLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mhasLoaded As Boolean = False
	#tag EndProperty


	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
