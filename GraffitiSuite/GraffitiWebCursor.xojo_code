#tag Class
Protected Class GraffitiWebCursor
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub DoBind(Caller as Timer)
		  t.Mode = 0
		  t = nil
		  
		  if LibrariesLoaded Then
		    if not IsNull( me ) and BindingCache.Ubound >= 0 then
		      me.ExecuteJavaScript( Join( BindingCache, "" ) )
		      
		      redim BindingCache(-1)
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebCursor -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "jquery.awesome-cursor.js" ), JavascriptNamespace ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebCursor -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetBodyCursor(iconName as String, iconColor as Color = &c000000, iconSize as Integer = 14, hotspotX as Integer = 0, hotspotY as Integer = 0, iconFlip as String = FlipNone, iconRotate as Integer = 0, iconOutline as Color = &c00000000)
		  dim strBind() as String
		  
		  strBind.Append( "window.GSjQuery('body').awesomeCursor('" + iconName + "', {" )
		  strBind.Append( "color: 'rgba(" + Str( iconColor.Red, "#" ) + "," + Str( iconColor.Green, "#" ) + "," + Str( iconColor.Blue, "#" ) + "," + Str( 1 - (iconColor.Alpha / 255), "0.0#" ) + ")'," )
		  strBind.Append( "size: " + Str( iconSize, "#" ) + "," )
		  strBind.Append( "hotspot: [" + Str( hotspotX, "#" ) + "," + Str( hotspotY, "#" ) + "]," )
		  strBind.Append( "flip: '" + iconFlip + "'," )
		  strBind.Append( "rotate: " + Str( iconRotate, "#" ) + "," )
		  strBind.Append( "outline: 'rgba(" + Str( iconOutline.Red, "#" ) + "," + Str( iconOutline.Green, "#" ) + "," + Str( iconOutline.Blue, "#" ) + "," + Str( 1 - (iconOutline.Alpha / 255), "0.##" ) + ")'" )
		  strBind.Append( "});" )
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strBind, "" ) )
		  else
		    BindingCache.Append( Join( strBind, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetCursor(BindTo as WebControl, iconName as String, iconColor as Color = &c000000, iconSize as Integer = 14, hotspotX as Integer = 0, hotspotY as Integer = 0, iconFlip as String = FlipNone, iconRotate as Integer = 0, iconOutline as Color = &c00000000)
		  dim strBind() as String
		  
		  dim strTheName as String = BindTo.ControlID
		  if BindTo isa WebTextField or BindTo isa WebButton or BindTo isa WebTextArea then
		    strTheName = strTheName + "_inner"
		  end if
		  
		  strBind.Append( "window.GSjQuery('#" + strTheName + "').awesomeCursor('" + iconName + "', {" )
		  strBind.Append( "color: 'rgba(" + Str( iconColor.Red, "#" ) + "," + Str( iconColor.Green, "#" ) + "," + Str( iconColor.Blue, "#" ) + "," + Str( 1 - (iconColor.Alpha / 255), "0.0#" ) + ")'," )
		  strBind.Append( "size: " + Str( iconSize, "#" ) + "," )
		  strBind.Append( "hotspot: [" + Str( hotspotX, "#" ) + "," + Str( hotspotY, "#" ) + "]," )
		  strBind.Append( "flip: '" + iconFlip + "'," )
		  strBind.Append( "rotate: " + Str( iconRotate, "#" ) + "," )
		  strBind.Append( "outline: 'rgba(" + Str( iconOutline.Red, "#" ) + "," + Str( iconOutline.Green, "#" ) + "," + Str( iconOutline.Blue, "#" ) + "," + Str( 1 - (iconOutline.Alpha / 255), "0.0#" ) + ")'," )
		  strBind.Append( "});" )
		  
		  if LibrariesLoaded then
		    me.ExecuteJavaScript( Join( strBind, "" ) )
		  else
		    BindingCache.Append( Join( strBind, "" ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded Then
		    SetCursor( me, "pencil" )
		    t = new Timer
		    t.Mode = 2
		    t.Period = 2000
		    AddHandler t.Action, AddressOf DoBind
		    
		    me.ExecuteJavaScript( Join( BindingCache, "" ) )
		    
		    redim BindingCache(-1)
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://jwarby.github.io/jquery-awesome-cursor/
		
		Licensed under MIT.
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private BindingCache() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private BoundControls() As WebControl
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			  
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private t As Timer
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = FlipBoth, Type = String, Dynamic = False, Default = \"both", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FlipHorizontal, Type = String, Dynamic = False, Default = \"horizontal", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FlipNone, Type = String, Dynamic = False, Default = \"", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FlipVertical, Type = String, Dynamic = False, Default = \"vertical", Scope = Public
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"False", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.cursor", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jqCurs, Type = String, Dynamic = False, Default = \"/*! jquery-awesome-cursor - v0.0.5 - 2014-12-12\n* https://jwarby.github.io/jquery-awesome-cursor\n* Copyright (c) 2014 James Warwood; Licensed MIT */\n;(function(global\x2C factory) {\n  if (typeof define \x3D\x3D\x3D \'function\' && define.amd) {\n    define([\'jquery\']\x2C factory);\n  } else if (typeof exports \x3D\x3D\x3D \'object\') {\n    factory(require(\'jquery\'));\n  } else {\n    factory(global.jQuery);\n  }\n})(this\x2C function($) {\n  \'use strict\';\n\n  /**\n   * Parse the user-supplied hotspot string.  Hotspot values as strings are used\n   * to set the cursor based on a human-readable value.\n   *\n   * ## Examples\n   *\n   * - `hotspot: \'center\'`: the hotspot is in the center of the cursor\n   * - `hotspot: \'center left\'`: the hotspot is centered vertically\x2C and fixed\n   *                             to the left of the cursor horizontally\n   * - `hotspot: \'top right\'`: the hotspot is at the top right\n   * - `hotspot: \'center top\'`: the hotspot is centered horizontally\x2C and fixed\n   *                            to the top of the cursor vertically\n   *\n   * @param {String} hotspot  The string descriptor for the hotspot location\n   * @param {Number} size     The size of the cursor\n   *\n   * @return {[Number]} an array with two elements\x2C the x and y offsets for the\n   *                    hotspot\n   *\n   * @throws {Error} if `hotspot` is not a string\x2C or `cursorSize` is not a\n   *                 number\n   */\n  var parseHotspotString \x3D function(hotspot\x2C cursorSize) {\n    var xOffset \x3D 0\x2C\n      yOffset \x3D 0;\n\n    if (typeof hotspot !\x3D\x3D \'string\') {\n      $.error(\'Hotspot value is not a string and could not be parsed\');\n    }\n\n    if (typeof cursorSize !\x3D\x3D \'number\') {\n      $.error(\'Cursor size must be a number\');\n    }\n\n    hotspot.split(\' \').forEach(function(part) {\n      switch (part) {\n        case \'center\':\n          xOffset \x3D cursorSize / 2;\n          yOffset \x3D cursorSize / 2;\n          break;\n        case \'top\':\n          yOffset \x3D 0;\n          break;\n        case \'bottom\':\n\n          /* Browsers will default to 0 0 if yOffset is the very last pixel\x2C\n           * hence - 1\n           */\n          yOffset \x3D cursorSize - 1;\n          break;\n        case \'left\':\n          xOffset \x3D 0;\n          break;\n        case \'right\':\n          xOffset \x3D cursorSize - 1;\n          break;\n      }\n    });\n\n    return [xOffset\x2C yOffset];\n  };\n\n  /**\n   * Returns a new canvas with the same contents as `canvas`\x2C flipped\n   * accordingly.\n   *\n   * @param {Canvas} canvas     The canvas to flip\n   * @param {String} direction  The direction flip the canvas in.  Can be one\n   *                            of:\n   *                              - \'horizontal\'\n   *                              - \'vertical\'\n   *                              - \'both\'\n   *\n   * @return {Canvas} a new canvas with the flipped contents of the input canvas\n   */\n  function flipCanvas(canvas\x2C direction) {\n    if ($.inArray(direction\x2C [\'horizontal\'\x2C \'vertical\'\x2C \'both\']) \x3D\x3D\x3D -1) {\n      $.error(\'Flip value must be one of horizontal\x2C vertical or both\');\n    }\n\n    var flippedCanvas \x3D $(\'<canvas />\')[0]\x2C\n      flippedContext;\n\n    flippedCanvas.width \x3D canvas.width;\n    flippedCanvas.height \x3D canvas.height;\n\n    flippedContext \x3D flippedCanvas.getContext(\'2d\');\n\n    if (direction \x3D\x3D\x3D \'horizontal\' || direction \x3D\x3D\x3D \'both\') {\n      flippedContext.translate(canvas.width\x2C 0);\n      flippedContext.scale(-1\x2C 1);\n    }\n\n    if (direction \x3D\x3D\x3D \'vertical\' || direction \x3D\x3D\x3D \'both\') {\n      flippedContext.translate(0\x2C canvas.height);\n      flippedContext.scale(1\x2C -1);\n    }\n\n    flippedContext.drawImage(canvas\x2C 0\x2C 0\x2C canvas.width\x2C canvas.height);\n\n    return flippedCanvas;\n  }\n\n  $.fn.extend({\n    awesomeCursor: function(iconName\x2C options) {\n      options \x3D $.extend({}\x2C $.fn.awesomeCursor.defaults\x2C options);\n\n      if (typeof iconName !\x3D\x3D \'string\' || !iconName) {\n        $.error(\'First parameter must be the icon name\x2C e.g. \\\'pencil\\\'\');\n      }\n\n      options.size \x3D typeof options.size \x3D\x3D\x3D \'string\' \?\n          parseInt(options.size\x2C 10) : options.size;\n\n      if (typeof options.hotspot \x3D\x3D\x3D \'string\') {\n        options.hotspot \x3D parseHotspotString(options.hotspot\x2C options.size);\n      }\n\n      // Clamp hotspot coordinates between 0 and size - 1\n      options.hotspot \x3D $.map(options.hotspot\x2C function(coordinate) {\n        return Math.min(options.size - 1\x2C Math.max(0\x2C coordinate));\n      });\n\n      var srcElement \x3D $(\'<i />\'\x2C {\n          class: \'fa fa-\' + iconName\x2C\n          style: \'position: absolute; left: -9999px; top: -9999px;\'\n        })\x2C\n        canvas \x3D $(\'<canvas />\')[0]\x2C\n        canvasSize \x3D options.size\x2C\n        hotspotOffset\x2C unicode\x2C dataURL\x2C context;\n\n      // Render element to the DOM\x2C otherwise `getComputedStyle` will not work\n      $(\'body\').append(srcElement);\n      \n      var t \x3D new Date().getTime(); while (new Date().getTime() < t + 2000);\n      \n      // Get the unicode value of the icon\n      unicode \x3D window.getComputedStyle(srcElement[0]\x2C \':before\')\n          .getPropertyValue(\'content\');\n\n      // Remove the source element from the DOM\n      srcElement.remove();\n\n      // Increase the size of the canvas to account for the cursor\'s outline\n      if (options.outline) {\n        canvasSize +\x3D 2;\n      }\n\n      if (options.rotate) {\n\n        // @TODO: move this into it\'s own function\n        canvasSize \x3D Math.ceil(Math.sqrt(\n          Math.pow(canvasSize\x2C 2) + Math.pow(canvasSize\x2C 2)\n        ));\n\n        hotspotOffset \x3D (canvasSize - options.size) / 2;\n        canvas.width \x3D canvasSize;\n        canvas.height \x3D canvasSize;\n\n        context \x3D canvas.getContext(\'2d\');\n        context.translate(canvas.width / 2\x2C canvas.height / 2);\n\n        // Canvas API works in radians\x2C not degrees\x2C hence `* Math.PI / 180`\n        context.rotate(options.rotate * Math.PI / 180);\n        context.translate(-canvas.width / 2\x2C -canvas.height / 2);\n\n        // Translate hotspot offset\n        options.hotspot[0] +\x3D options.hotspot[0] !\x3D\x3D canvas.width / 2 \?\n            hotspotOffset : 0;\n\n        options.hotspot[1] +\x3D options.hotspot[1] !\x3D\x3D canvas.height / 2 \?\n            hotspotOffset : 0;\n      } else {\n\n        canvas.height \x3D canvasSize;\n        canvas.width \x3D canvasSize;\n\n        context \x3D canvas.getContext(\'2d\');\n      }\n\n      // Draw the cursor to the canvas\n      context.fillStyle \x3D options.color;\n      context.font \x3D options.size + \'px FontAwesome\';\n      context.textAlign \x3D \'center\';\n      context.textBaseline \x3D \'middle\';\n      context.fillText(unicode\x2C canvasSize / 2\x2C canvasSize / 2);\n\n      // Check for outline option\n      if (options.outline) {\n        context.lineWidth \x3D 0.5;\n        context.strokeStyle \x3D options.outline;\n        context.strokeText(unicode\x2C canvasSize / 2\x2C canvasSize / 2);\n      }\n\n      // Check flip option\n      if (options.flip) {\n        canvas \x3D flipCanvas(canvas\x2C options.flip);\n      }\n\n      dataURL \x3D canvas.toDataURL(\'image/png\');\n\n      $(this).css(\'cursor\'\x2C [\n        \'url(\' + dataURL + \')\'\x2C\n        options.hotspot[0]\x2C\n        options.hotspot[1]\x2C\n        \'\x2C\'\x2C\n        \'auto\'\n      ].join(\' \'));\n\n      // Maintain chaining\n      return this;\n    }\n  });\n\n  // Expose the defaults so that users can override them if they want to\n  $.fn.awesomeCursor.defaults \x3D {\n    color: \'#000000\'\x2C\n    size: 18\x2C\n    hotspot: [0\x2C 0]\x2C\n    flip: \'\'\x2C\n    rotate: 0\x2C\n    outline: null\n  };\n});", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAauSURBVHhe7d1LbFRVHMfx878NQ2fwASg+eBo3mogCioqKkQVEEoROxUdcuDA+FibQTqfER2IiCxMMdlo3aiwLja5QcaZxFho3algYo5igLNy4MGo0JkgCnYLt/XvOnf8UaKf0ce/c3nPu77OZc05JH5zv3Lm3M20VAAAAAAAAAAAAAAA4jeQ2VtmdhU2KeD2R9+twpfS5LMM8iD2AXL7wqf6weZkaRz2fnjs91HdC5hAjT25jkc0XDk7YfOM+3+Pqop3dt8gcYhRrAPpw87gMJ7qBPUIE8yDWAHQCq2QwGdGaIILdiCBOMQcwDRPBKCKIU7ICMBoRdBbXygq0UPICMEwEvo8IYpDMAAyi1cz66gARtFRyA6irR9BRuFXmELGkB2CsZqLPEEFr2BCAoSNQOBK0gC0BaLQKEUTPogAMiWDn3ttkAUKyLABDR+C16Qh6EUEELAxAI1rJ3hgiiICdARhBBH510W5EEIa9ARikVvKoX811FtbJCsyS3QEYOgLlEyKYI/sDMEitQARz40YAhomAEcFsuRNAXT2Cjq71ModpuBaAsUKRhwhmyMUANFpej6AXEUzD0QAME8FYNberZ4MsQBMOB2DoCDz1GSKYmuMBBJabJ5AQQXNpCEARqesRQXOpCMAYj6Cz+3ZZAi01ARhBBMF3DBFBQ6oCMIjoOkRwXuoCMIIIgm8b70t9BKkMwCBlIhit5vLFO2QplVIbgBFEoDjVEaQ6AIOUujbNEaQ+ACPNESAA0Yggu6t7oyylAgK4gIlAXyKkKgIEMIG+RLwmTREggCbGI3ho752y5CwEMIUggra2araz6HQECOASdATLlK9PDB2OAAFMg0g5HQECmIEgAtYR5At3yZIzEMAM6UvEZfrGuQgQwCyQoqv1jVMRIIBZci0CBDAH4xHs6rq7vmIvBDBHQQTk6QiKVkeAAEIgoqsU+VZHgABCqkdgnkW0MwIEEAEiVY/A/CkcyyCAiAQReFTNdtoVAQKIkI5gqflNJTZFgAAi1oigPd91jywlGgJoARMBKc+KCBBAi5CiJfUIehMdAQJooXoEY4mOAAG0WD0Cv9re0XOvLCUKAogBKbVEnxckMgIEEBMdwWL9n/3B0u17rpClREAAcSJ1Yy3T9rTMEgEBxI3oZhklAgKIG/ExGSUCAogRKz5RKw+8I9NEQAAxYVY/EPGTMk0MfXIan1y+h2WYTMSlMR59S2bhjGV8WqB84raxzDnvzKnqgZPylkRBABc7O1wutcs4FfAQcLGFizp7tsk4FRDABPqx+hUZpgICmOz+yx55yfwUUCoggCb80bMvyNB5CKC5otw6DwFMoT1ffEyGTkMAUyDmVJwMOhOAr3gLK3VcpqERqbVp+FP1TgTge/7mkXL/V3rbXpWlSOioCjJ0lvUBsE+bRo4MHDXjWrnviL6QPxe8IQJE3lNq+56FMnWS5QHQxtpQ37cyCbCiAzKMRDaz4BkZOsniAGjjcLnve5mMq62/fL8MI0EevyxDJ1kaQPPND+zfrx+6VUVmEaDlLj8/YGEAl9j8BlaRHgWY+XkZOseyAGaw+drwUOkYs/pZphGgfPvDxTUycYpFAcxs8xuI6KAMI0G+n6hX80bFkgBmt/mG/vfv62P3GZmGRky9MnRKwgPg0bls/gXeldvwSGVzHd2Je01fWEkOYER/eptCbL5amMlEejKoPx/nHgaSGQCr0/outznM5hsnP3r9lH5nZZmGR+oB238t3ESJC4CV+lefwW0Ju/kNPnNJhtEgduookKgAWPE/pGhrVJtvjFQGvtHv90eZhkaknr1yx4tLZGq9uAP4XW4n0ff8v0h526Pc/PMoupNB7b/MOWeOAvEGwPyxjC5S33za0ZrNN88Slt4OHloiwszOPEEUawDDlf4Cs/pCpgFzeG7l5o9jf1BGoenP96ZsvrtDplaL+yGAa5XSg+T728w3VljR7lq5f0PLN99YQG/KKBI6gidkaLVYfzRsvuU6eg7rr/hRmYbEfwyX+1fIxFqJugpoNWIvspNBZvpJhlZLVQBnht74Uu/cdzINiSO9spgvqQog4EVwSUhqX63S/4nMrJaqc4CGbEfP32T+FNws6KuXP/XdZZB9dWikUvpNlq2XzgDyhdf0WfxMX+t3VF+rHhqulN6TuVNSGYB5dY835v+iiDKyNIm+x3/IbTQ4cqTva1lyUioDMLL5ni79xQ/INBAc5okP6TP8QZcO85eS2gCMXGdhHflqq69oMRMdHyn3HZY3AQAAAAAAAAAAAAAAANhOqf8B9BjpDibEGGYAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAACsSURBVDhPxZIxDsIwDEW/M4Z70W7txgG4AJRrIMoNmGEqY7kA92EoGyZ2MzU0TREST0q+HclPiRT8FFtulr5Mxvj0mHZRbHe+SWIgAJhoP0cSCIQ5ko8CIVUyKhBSJFGBMCWZFAgxSfwJwMPttSwmqUPIp2LLys3IILcEyvQQr6xrjre+DglvwLg8mzp3ee97s9YcYSDgurseVloSn/SEkGv/Dbaozqkf6l8Ab7VyNORPUOm+AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="36"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
