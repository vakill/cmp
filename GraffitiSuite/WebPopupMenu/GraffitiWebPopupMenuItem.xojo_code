#tag Class
Protected Class GraffitiWebPopupMenuItem
	#tag Method, Flags = &h0
		Sub Constructor(ItemText as String, ItemSelected as Boolean = False, ItemDisabled as Boolean = False, ItemTag as Variant = Nil, ItemIcon as String = "")
		  Text = ItemText
		  Selected = ItemSelected
		  Disabled = ItemDisabled
		  Tag = ItemTag
		  Icon = ItemIcon
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Disabled As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Icon As String
	#tag EndProperty

	#tag Property, Flags = &h0
		IconColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		IconPosition As IconPositions
	#tag EndProperty

	#tag Property, Flags = &h0
		ID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Selected As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		Style As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h0
		Tag As Variant
	#tag EndProperty

	#tag Property, Flags = &h0
		Text As String
	#tag EndProperty


	#tag Enum, Name = IconPositions, Type = Integer, Flags = &h0
		Left
		Right
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Disabled"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Icon"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IconPosition"
			Group="Behavior"
			Type="IconPositions"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Selected"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
