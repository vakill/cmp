#tag Class
Protected Class GraffitiWebPopupMenu
Inherits GraffitiControlWrapper
	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  // 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "itemRemoved"
		      if Parameters.Ubound >= 0 then
		        dim theItem as GraffitiWebPopupMenuItem = GetItemByID( Parameters(0).IntegerValue )
		        if not IsNull( theItem ) then
		          theItem.Selected = False
		          SelectionRemoved( theItem )
		        end if
		      end if
		    case "itemAdded"
		      
		      if Parameters.Ubound >= 1 then
		        if not mMultipleSelect then
		          '// Single Select
		          DeselectAll( True )
		          
		          dim intID as Integer = Parameters(0).IntegerValue
		          
		          if IsNumeric( Parameters(0).StringValue ) and Parameters(0).StringValue <> Parameters(1).StringValue then
		            dim theItem as GraffitiWebPopupMenuItem = GetItemByID( intID )
		            if not IsNull( theItem ) then
		              theItem.Selected = True
		              mSelectedItem = theItem
		              SelectionChanged( theItem )
		            end if
		          else
		            AddCustomSelection( Parameters(1).StringValue )
		          end if
		        else
		          '// Multiple Select
		          dim strText as String = Parameters(1).StringValue
		          
		          dim blnAdd as Boolean = False
		          if IsNumeric( Parameters(0).StringValue ) and Parameters(0).StringValue <> Parameters(1).StringValue then
		            dim intID as Integer = Parameters(0).IntegerValue
		            dim currItem as GraffitiWebPopupMenuItem = GetItemByID( intID )
		            if IsNull( currItem ) then
		              blnAdd = True
		            else
		              currItem.Selected = True
		              SelectionAdded( currItem )
		            end if
		          else
		            AddCustomSelection( strText )
		          end if
		          
		        end if
		        
		        UpdateOptions()
		        
		      end if
		      
		    case "dblClick"
		      DoubleClick()
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  select case Name
		  case "height"
		    dim intNewHeight as Integer = value.Int32Value - 1
		    Buffer( "window.GSjQuery('#" + me.ControlID + " a.select2-choice').css('height','" + Str( intNewHeight ) + "px').css('min-height','" + Str( intNewHeight ) + "px').css('max-height','" + Str( intNewHeight ) + "px');" )
		    Buffer( "window.GSjQuery('#" + me.ControlID + " ul.select2-choices').css('height','" + Str( intNewHeight ) + "px').css('min-height','" + Str( intNewHeight ) + "px').css('max-height','" + Str( intNewHeight ) + "px');" )
		    Return True
		  case "style"
		    if not IsNull( value ) then
		      if not WebStyle( value ).IsDefaultStyle then
		        dim strExec() as String
		        strExec.Append( "var theContainer = window.GSjQuery('#" + me.ControlID + "').find('.select2-container');" )
		        if not IsNull( OldStyle ) then
		          if not OldStyle.IsDefaultStyle then strExec.Append( "theContainer.removeClass('" + OldStyle.Name + "');" )
		        end if
		        strExec.Append( "theContainer.addClass('" + WebStyle( value ).Name + "');" )
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + " .select2-choices, #" + me.ControlID + " .select2-choice').css('box-sizing', 'border-box');" )
		        OldStyle = value
		        Buffer( strExec )
		      end if
		    end if
		    Return True
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		  
		  me.ClosePopup()
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  DoResize()
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<style>li.select2-result {clear:both;}</style>" )
		  
		  source.Append( "<div id='" + me.ControlID + "' style='min-height:30px;overflow:hidden;box-sizing:border-box;display:none;'>" )
		  source.Append( "<input id=""" + me.ControlID + "_inner"" data-placeholder=""" + EscapeString( PlaceholderText ) + """ style='width: 100%;height: 100%;' />" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  RunBuffer()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub AddCustomSelection(theItemText as String)
		  dim newItem as new GraffitiWebPopupMenuItem( theItemText, True, False )
		  newItem.ID = allItemCount
		  Items.Append( newItem )
		  allItemCount = allItemCount + 1
		  mSelectedItem = newItem
		  SelectionAdded( newItem )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddItem(NewItem as GraffitiWebPopupMenuItem)
		  NewItem.ID = allItemCount
		  allItemCount = allItemCount + 1
		  
		  Items.Append( NewItem )
		  
		  if mAllowAdd = false and mMultipleSelect = false and not isLoaded then Return
		  
		  dim strExec() as String
		  
		  strExec.Append( "var thejQuery = window.GSjQuery;" )
		  strExec.Append( "var theSelect = thejQuery('#" + me.ControlID + "_inner');" )
		  
		  'if not AllowAdd then
		  'strExec.Append( "theSelect" )
		  'strExec.Append( ".append(window.GSjQuery('<option></option>')" )
		  'strExec.Append( ".attr('value','" + Str( NewItem.ID, "#" ) + "')" )
		  'if NewItem.Selected then strExec.Append( ".attr('selected','')" )
		  'if NewItem.Disabled then strExec.Append( ".attr('disabled','')" )
		  'strExec.Append( ".text('" + EscapeString( NewItem.Text ) + "'));" )
		  'Else
		  UpdateOptions()
		  Return
		  strExec.Append( "if (window.GSPM_" + me.ControlID + "_data == null) {" )
		  strExec.Append( "window.GSPM_" + me.ControlID + "_data = [];" )
		  strExec.Append( "}" )
		  strExec.Append( "var theData = window.GSPM_" + me.ControlID + "_data;" )
		  strExec.Append( "theData.push( " + ItemToString( NewItem ) + " );" )
		  'end if
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddItem(ItemText as String)
		  dim newItem as new GraffitiWebPopupMenuItem( ItemText )
		  me.AddItem( newItem )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClosePopup()
		  if isShown and LibrariesLoaded then
		    dim strScript as String = "var thisSelect = window.GSjQuery('#" + me.ControlID + "_inner');" + _
		    "if( typeof( thisSelect != 'undefined' ) ) {" + _
		    "thisSelect.select2('close');" + _
		    "};"
		    
		    Buffer( strScript )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DeselectAll(DoLockUpdate as Boolean = False)
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  for intCycle = 0 to intMax
		    Items(intCycle).Selected = False
		    if not DoLockUpdate then Buffer( "window.GSjQuery('#" + me.ControlID + "_inner').select2('val', '');" )
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub DoResize()
		  Return
		  dim strExec() as String
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('.select2-container').width(" + Str( me.Width, "#" ) + ").height(" + Str( me.Height, "#" ) + ");" )
		  
		  Buffer( strExec )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(ItemIndex as Integer) As GraffitiWebPopupMenuItem
		  if ItemIndex >= 0 and ItemIndex <= UBound( Items ) then Return Items(ItemIndex)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItem(strText as String) As GraffitiWebPopupMenuItem
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebPopupMenuItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Text = strText then Return currItem
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FindItemByTag(theTag as Variant) As GraffitiWebPopupMenuItem
		  dim intMax as Integer
		  for intCycle as integer = 0 to Items.Ubound
		    dim currItem as GraffitiWebPopupMenuItem = Items(intCycle)
		    if IsNull( currItem ) then Continue
		    
		    if currItem.Tag = theTag then Return currItem
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetItemByID(theID as Integer) As GraffitiWebPopupMenuItem
		  for intCycle as Integer = 0 to Items.Ubound
		    dim currItem as GraffitiWebPopupMenuItem = Items(intCycle)
		    if currItem.ID = theID then
		      Return currItem
		    end if
		  next
		  
		  Return nil
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetItemIndex(theItem as GraffitiWebPopupMenuItem) As Integer
		  dim intMax as Integer = Items.Ubound
		  for intCycle as Integer = 0 to intMax
		    if Items(intCycle) = theItem then Return intCycle
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetItemIndexByTag(theTag as Variant) As Integer
		  dim intMax as Integer
		  for intCycle as integer = 0 to Items.Ubound
		    dim currItem as GraffitiWebPopupMenuItem = Items(intCycle)
		    if IsNull( currItem ) then Continue
		    
		    if currItem.Tag = theTag then Return intCycle
		  next
		  
		  Return -1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSelectedItems() As GraffitiWebPopupMenuItem()
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebPopupMenuItem
		  
		  dim arrRet() as GraffitiWebPopupMenuItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Selected then arrRet.Append( currItem )
		  next
		  
		  Return arrRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSelectedItemsIndex() As Integer()
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebPopupMenuItem
		  
		  dim arrRet() as Integer
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Selected then arrRet.Append( intCycle )
		  next
		  
		  Return arrRet
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebPopupMenu -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "select2" ).Child( "select2.js" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "select2" ).Child( "select2.css" ), JavascriptNamespace ) )
		    strOut.Append( AddCSSFile( CurrentSession, fScripts.Child( "select2" ).Child( "select2-skins.css" ), JavascriptNamespace ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ItemCount() As Integer
		  Return Items.Ubound + 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemsToString() As String
		  dim strOut() as String
		  dim intMax as Integer = Items.Ubound
		  dim currItem as GraffitiWebPopupMenuItem
		  
		  strOut.Append( "[" )
		  for intCycle as Integer = 0 to intMax
		    strOut.Append( ItemToString( Items(intCycle) ) )
		    if intCycle < intMax then strOut.Append( "," )
		  next
		  strOut.Append( "]" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ItemToString(theItem as GraffitiWebPopupMenuItem) As String
		  return "{id:" + Str( theItem.ID, "#" ) + ", " + _
		  "value:" + Str( theItem.ID, "#" ) + ", " + _
		  "text: '" + EscapeString( theItem.Text ) + "', " + _
		  "disabled: " + Lowercase( Str( theItem.Disabled ) ) + ", " + _
		  "selected: " + Lowercase( Str( theItem.Selected ) ) + ", " + _
		  if( not IsNull( theItem.Style ), "itemStyle: '" + theItem.Style.Name + "', ", "itemStyle: '', " ) + _
		  "icon: '" + theItem.Icon + "'," + _
		  "iconPosition: '" + if( theItem.IconPosition = GraffitiWebPopupMenuItem.IconPositions.Left, "left", "right" ) + "'," + _
		  "iconColor: '" + ColorToRGBAString( theItem.IconColor ) + "'}"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenPopup()
		  if isShown and LibrariesLoaded then
		    dim strScript as String = "var thisSelect = window.GSjQuery('#" + me.ControlID + "_inner');" + _
		    "if( typeof( thisSelect != 'undefined' ) ) {" + _
		    "thisSelect.select2('open');" + _
		    "};"
		    
		    Buffer( strScript )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function OptionToString(theItem as GraffitiWebPopupMenuItem) As String
		  if IsNull( theItem ) then Return ""
		  
		  dim strOut() as String
		  if theItem.Icon <> "" then
		    dim strIconPos as String = if( theItem.IconPosition = GraffitiWebPopupMenuItem.IconPositions.Left, "left", "right" )
		    
		    strOut.Append( "<i style='color: " + ColorToRGBAString( theItem.IconColor ) + ";" + _
		    "float: " + strIconPos + ";" + _
		    "padding-right:5px;" + _
		    "class='fa " + theItem.Icon + ";'></i>" )
		  end if
		  
		  if not IsNull( theItem.Style ) then strOut.Append( "<span class='" + theItem.Style.Name + "'>" )
		  strOut.Append( EscapeString( theItem.Text ) )
		  if not IsNull( theItem.Style ) then strOut.Append( "</span>" )
		  
		  Return EscapeString( Join( strOut, "" ) )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveAllItems()
		  Redim Items(-1)
		  allItemCount = 0
		  
		  UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemIndex as Integer)
		  if ItemIndex >= 0 and ItemIndex <= UBound( Items ) then
		    dim theItem as GraffitiWebPopupMenuItem = Items(ItemIndex)
		    Items.Remove( ItemIndex )
		    
		    dim strExec() as String
		    if not AllowAdd then
		      strExec.Append( "var theItem = window.GSjQuery('#" + me.ControlID + "_inner option[value=" + chr(34) + Str( theItem.ID, "#" ) + chr(34) + "]');" )
		      if MultipleSelect then
		        dim strSel() as String
		        dim intMax as Integer = UBound( Items )
		        for intCycle as Integer = 0 to intMax
		          dim currItem as GraffitiWebPopupMenuItem = Items(intCycle)
		          if currItem.Selected then strSel.Append( Str( currItem.ID, "#" ) )
		        next
		        strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2('val', ['" + Join( strSel, "','" ) + "']);" )
		      end if
		      strExec.Append( "theItem.remove();" )
		      UpdateSelection()
		    else
		      UpdateSelection()
		      'UpdateOptions()
		      'Return
		      
		    end if
		    
		    Buffer( strExec )
		    
		    if AllowAdd then
		      UpdateOptions()
		    else
		      UpdateDisplay()
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RemoveItem(ItemText as String)
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( Items )
		  
		  dim currItem as GraffitiWebPopupMenuItem
		  
		  for intCycle = 0 to intMax
		    currItem = Items(intCycle)
		    if currItem.Text = ItemText then
		      RemoveItem( intCycle )
		    end if
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function RowsToString() As String
		  dim strRows() as String
		  
		  if AllowAdd and not MultipleSelect then
		    strRows.Append( "window.GSPM_" + me.ControlID + "_data = [" )
		    dim intCycle as Integer
		    dim intMax as Integer = UBound( Items )
		    
		    dim currItem as GraffitiWebPopupMenuItem
		    
		    for intCycle = 0 to intMax
		      
		      currItem = Items(intCycle)
		      strRows.Append( ItemToString( currItem ) + if(intCycle < intMax, ",", "" ) )
		      'strRows.Append( "{id:" + Str( intCycle, "#" ) + ", value:" + Str( intCycle, "#" ) + ", text: '" + currItem.Text + "', disabled: " + Lowercase( Str( currItem.Disabled ) ) + ", selected: " + Lowercase( Str( currItem.Selected ) + "}" + if( intCycle < intMax, ",", "" ) ) )
		      
		      if currItem.Selected Then SelectedItem = currItem
		      
		    next
		    
		    strRows.Append( "];" )
		  else
		    dim intCycle as Integer
		    dim intMax as Integer = UBound( Items )
		    
		    dim currItem as GraffitiWebPopupMenuItem
		    strRows.Append(  "window.GSjQuery('#" + me.ControlID + "_inner')" + _
		    ".empty()" + _
		    ".append(window.GSjQuery('<option></option>')" + _
		    ".attr('value','')" + _
		    ".text(''));" )
		    
		    for intCycle = 0 to intMax
		      currItem = Items(intCycle)
		      strRows.Append( "window.GSjQuery('#" + me.ControlID + "_inner')" + _
		      ".append(window.GSjQuery('<option></option>')" + _
		      ".attr('value','" + Str( intCycle, "#" ) + "')" )
		      if currItem.Selected then strRows.Append( ".attr('selected','')" )
		      if currItem.Disabled then strRows.Append( ".attr('disabled','')" )
		      strRows.Append( ".html('" + OptionToString( currItem ) + "'));" )
		      
		      if not MultipleSelect Then
		        if currItem.Selected Then SelectedItem = currItem
		      end if
		      
		    next
		  end if
		  
		  Return Join( strRows, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetItemSelect(theItem as GraffitiWebPopupMenuItem, ItemState as Boolean)
		  if not IsNull( theItem ) then
		    theItem.Selected = ItemState
		    
		    if mMultipleSelect then
		      UpdateSelection()
		    else
		      DeselectAll( True )
		      
		      dim strExec() as String
		      'if mAllowAdd then
		      'strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2('val','" + Str( theItem.ID ) + "');" )
		      'else
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2('data',{" + _
		      "id:'" + Str( theItem.ID, "#" ) + "'" + _
		      ",text:'" + EscapeString( theItem.Text ) + "'" + _
		      "});" )
		      'end if
		      
		      mSelectedItem = theItem
		      theItem.Selected = True
		      
		      Buffer( strExec )
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetItemSelect(ItemIndex as Integer, ItemSelected as Boolean)
		  if ItemIndex < 0 or ItemIndex > items.Ubound then Return
		  
		  dim theItem as GraffitiWebPopupMenuItem = FindItem( ItemIndex )
		  SetItemSelect( theItem, ItemSelected )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetItemSelect(ItemText as String, ItemState as Boolean)
		  dim theItem as GraffitiWebPopupMenuItem = FindItem( ItemText )
		  SetItemSelect( theItem, ItemState)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetItemState(ItemIndex as Integer, ItemState as Boolean)
		  if ItemIndex >= 0 and ItemIndex <= UBound( Items ) then
		    Items(ItemIndex).Disabled = not ItemState
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetText(theText as String)
		  dim strScript as String
		  if (not MultipleSelect And AllowAdd) then
		    '// Input
		    strScript = "window.GSjQuery('#" + me.ControlID + " > .select2-container > .select2-choice > .select2-chosen').text('" + EscapeString( theText ) + "');"
		  ElseIf MultipleSelect Then
		    '// Select
		  Else
		    '// Option
		  end if
		  
		  Buffer( strScript )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SkinFromString(theSkin as String) As Skins
		  Select Case theSkin
		  case "blue"
		    Return Skins.Blue
		  case "bluegreen"
		    Return Skins.BlueGreen
		  case "darkgrey"
		    Return Skins.DarkGrey
		  case "error"
		    Return Skins.Error
		  case "grey"
		    Return Skins.Grey
		  case "lightblue"
		    Return Skins.LightBlue
		  case "lime"
		    Return Skins.Lime
		  case "orange"
		    return Skins.Orange
		  case "pomelo"
		    Return Skins.Pomelo
		  case "success"
		    Return Skins.Success
		  case "warning"
		    Return Skins.Warning
		  End Select
		  
		  return Skins.None
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SkinToString(theSkin as Skins) As String
		  Select Case theSkin
		  case Skins.Blue
		    Return "blue"
		  case Skins.BlueGreen
		    Return "bluegreen"
		  case Skins.DarkGrey
		    Return "darkgrey"
		  case Skins.Error
		    Return "error"
		  case Skins.Grey
		    Return "grey"
		  case Skins.LightBlue
		    Return "lightblue"  
		  case Skins.Lime
		    Return "lime"
		  case Skins.Orange
		    Return "orange"
		  case Skins.Pomelo
		    Return "pomelo"
		  case Skins.Success
		    Return "success"
		  case Skins.Warning
		    Return "warning"
		  End Select
		  Return ""
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TrueParent() As WebControl
		  dim currParent as WebControl = me
		  dim isDone as Boolean
		  while not isDone
		    if not IsNull( me.Page ) then Return me.Page
		    if not IsNull( me.Parent ) then
		      currParent = currParent.Parent
		      if currParent isa WebDialog then
		        Return currParent
		      elseif currParent isa WebPage then
		        Return currParent
		      end if
		    end if
		  wend
		  
		  Return currParent
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateDisplay()
		  '// Method within JS to force an update.  We don't need this, though.
		  Buffer( "window.GSjQuery('" + me.ControlID + "_inner').select2('updateResults');" )
		  
		  'UpdateOptions()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    
		    if mLockUpdate then Return
		    
		    dim strExec() as String
		    
		    dim strInner as String
		    dim isInput as Boolean = False
		    if mAllowAdd then
		      strInner = "<input id=""" + me.ControlID + "_inner"" type=""text"" style=\'width: 100%;height: 100%;\' />"
		      isInput = True
		    else
		      strInner = "<select id=""" + me.ControlID + "_inner""" + if( MultipleSelect, " multiple", "" ) + " style=\'width: 100%;height: 100%;\'></select>"
		    end if
		    
		    strExec.Append( "var oldDiv = window.GSjQuery('#s2id_" + me.ControlID + "_inner');" )
		    strExec.Append( "if(typeof oldDiv !== 'undefined') {" )
		    strExec.Append( "oldDiv.remove();" )
		    strExec.Append( "}" )
		    
		    strExec.Append( "var theOld = window.GSjQuery('#" + me.ControlID + "_inner');" )
		    strExec.Append( "if(typeof theOld === 'undefined') {" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "').append('" + strInner + "');" )
		    strExec.Append( "}else{" )
		    strExec.Append( "theOld.replaceWith(window.GSjQuery('" + strInner + "'), true);" )
		    strExec.Append( "}" )
		    
		    'if mAllowAdd = false and mMultipleSelect = false then strExec.Append( RowsToString() )
		    if not mAllowAdd then strExec.Append( RowsToString() )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2({" )
		    if mAllowAdd then strExec.Append( "data: " + itemsToString() + "," )
		    'strExec.Append( "data: function() {var currData = window.GSPM_" + me.ControlID + "_data;return {results:currData};}," )
		    if mAllowAdd then strExec.Append( ReplaceAll( jsSearchCrit, "#CONTROLID#", me.ControlID ) )
		    if mAllowAdd then strExec.Append( "initSelection: function (element,callback) {var data = [];window.GSjQuery(element.val().split(',')).each(function () {data.push({id: this, text: this});});callback(data);}," )
		    if mMultipleSelect and isInput then strExec.Append( "multiple: true," )
		    strExec.Append( "minimumResultsForSearch: " + Str( SearchThreshold, "-#" ) + "," )
		    strExec.Append( "maximumSelectionSize: " + Str( MaxSelectedItems ).Lowercase + "," )
		    strExec.Append( "placeholder: '" + EscapeString( PlaceholderText ) + "'," )
		    strExec.Append( "minimumInputLength: " + Str( MinimumInputLength ) + "," )
		    
		    strExec.Append( "formatResult: function(option) {" )
		    strExec.Append( "var elOpt = option.element;" )
		    strExec.Append( "var strRet = '';" )
		    strExec.Append( "if(typeof option.icon == 'string') {" )
		    strExec.Append( "strRet += '<i style=""color:' + option.iconColor + ';" + _
		    "float:' + option.iconPosition + ';" + _
		    "padding:5px;"" " + _
		    "class=""' + option.icon + '""></i>';" )
		    strExec.Append( "}" )
		    strExec.Append( "strRet += '<span style=""display:block;"" class=""' + option.itemStyle + '"">' + option.text + '</span>';" )
		    strExec.Append( "return strRet;" )
		    strExec.Append( "}," )
		    
		    dim skinContainer as String = SkinToString( mContainerSkin )
		    dim skinDropdown as String = SkinToString( mDropdownSkin )
		    strExec.Append( "containerCssClass: 'tpx-select2-container" + if( skinContainer <> "", " select2-" + skinContainer, "" ) + "'," )
		    strExec.Append( "dropdownCssClass: 'tpx-select2-drop " + me.ControlID + "_dropdown" + if( skinDropdown <> "", " select2-" + skinDropdown, "" ) + "'" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').change(function(e){" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'itemAdded',[e.added.id,e.added.text]);" )
		    strExec.Append( "});" )
		    
		    Enabled = mEnabled
		    ReadOnly = mReadOnly
		    
		    dim arrSel() as Integer = GetSelectedItemsIndex()
		    
		    If AllowAdd and UBound( arrSel ) >= 0 then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').val('" + Str( arrSel(0), "#" ) + "').trigger('change');" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').on('select2-open', function() {" )
		    strExec.Append( "setTimeout(function() {" )
		    strExec.Append( "var maxZ = Math.max.apply(null,window.GSjQuery.map(window.GSjQuery('*'), function(e,n){" )
		    strExec.Append( "if(window.GSjQuery(e).css('position')=='absolute')" )
		    strExec.Append( "return parseInt(window.GSjQuery(e).css('z-index'))||1 ;" )
		    strExec.Append( "})" )
		    strExec.Append( ");" )
		    strExec.Append( "window.GSjQuery('.select2-drop').css('z-index', maxZ + 100);" )
		    strExec.Append( "if( window.GSjQuery('#" + me.ControlID + "_inner').parents('.palette').length > 0 ) {" )
		    strExec.Append( "window.GSjQuery('.select2-drop-mask').css('z-index', maxZ + 99);" )
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').parents('.palette .modal').css('z-index', maxZ - 1);" )
		    strExec.Append( "}" )
		    strExec.Append( "}, 100);" )
		    strExec.Append( "});" )
		    
		    if SearchThreshold < 0 then
		      strExec.Append( "var searchEl = window.GSjQuery('.select2-search-hidden');" )
		      strExec.Append( "if (searchEl) searchEl.remove();" )
		    end if
		    
		    if MultipleSelect then
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "').find('.select2-choices').css('overflow', 'auto').css('max-height', '" + Str( me.Height, "#" ) + "px');" )
		    end if
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').on('select2-removed', function(e) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'itemRemoved', [e.val]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.GSjQuery('#s2id_" + me.controlID + "_inner').click( function(e) {" )
		    strExec.Append( "var d = new Date();" )
		    strExec.Append( "var ticks = d.getTime();" )
		    strExec.Append( "if (window.lastClick_" + me.ControlID + " > 0) {" )
		    strExec.Append( "if (ticks - window.lastClick_" + me.ControlID + " < 500) {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + self.ControlID + "', 'dblClick', [ticks]);" )
		    strExec.Append( "window.lastClick_" + me.ControlID + " = 0;" )
		    strExec.Append( "}" )
		    strExec.Append( "} else {" )
		    strExec.Append( "window.lastClick_" + me.ControlID + " = ticks;" )
		    strExec.Append( "}" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "window.lastClick_" + me.ControlID + " = 0;" )
		    
		    me.ExecuteJavaScript( Join( strExec, EndOfLine ) )
		    
		    RunBuffer()
		    
		    'if not (mAllowAdd and not mMultipleSelect) then
		    UpdateSelection()
		    
		    me.Height = me.Height
		    
		    EventsAdded = True
		    
		    DoResize()
		  end if
		  
		  isLoaded = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateSelection()
		  dim arrSel() as GraffitiWebPopupMenuItem = GetSelectedItems()
		  
		  dim strExec() as String
		  
		  if mMultipleSelect then
		    dim strSel() as String
		    
		    dim intMax as Integer = arrSel.Ubound
		    for intCycle as Integer = 0 to intMax
		      strSel.Append( ItemToString( arrSel(intCycle) ) )
		      if intCycle < intMax then strSel.Append( "," )
		    next
		    
		    strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2('data',[" + Join( strSel, "" ) + "]);" )
		  else
		    if arrSel.Ubound >= 0 then
		      dim theItem as GraffitiWebPopupMenuItem = arrSel(0)
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2('data',{" + _
		      "id:'" + Str( theItem.ID, "#" ) + "'" + _
		      ",text:'" + EscapeString( theItem.Text ) + "'" + _
		      "});" )
		    else
		      strExec.Append( "window.GSjQuery('#" + me.ControlID + "_inner').select2('val','');" )
		    end if
		  end if
		  
		  Buffer( strExec )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateZIndex()
		  dim strExec() as String
		  
		  strExec.Append( "var myPopMenu_" + me.ControlID + " = window.GSjQuery('#" + me.ControlID + "_inner').zindex();" )
		  strExec.Append( "window.GSjQuery('.select2-drop').css('z-index', myPopMenu_" + me.ControlID + " + 1);" )
		  
		  Buffer( Join( strExec, "" ) )
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event DoubleClick()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionAdded(theItem as GraffitiWebPopupMenuItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionChanged(theItem as GraffitiWebPopupMenuItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionCleared()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event SelectionRemoved(theItem as GraffitiWebPopupMenuItem)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TextChange()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		http://ivaynberg.github.io/select2/select2-latest.html
		
		Apache Software Foundation License Version 2.0 and GPL Version 2.0
		
	#tag EndNote


	#tag Property, Flags = &h21
		Private allItemCount As Integer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mAllowAdd
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mAllowAdd = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		AllowAdd As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mContainerSkin
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldValue as String = SkinToString( mContainerSkin )
			  dim theValue as String = SkinToString( value )
			  
			  dim strExec() as String
			  strExec.Append( "var theContainer = window.GSjQuery('#" + me.ControlID + " > div.select2-container');" )
			  if oldValue <> "" then strExec.Append( "theContainer.removeClass('select2-" + oldValue + "');" )
			  mContainerSkin = value
			  if theValue <> "" then strExec.Append( "theContainer.addClass('select2-" + theValue + "');" )
			  Buffer( strExec )
			End Set
		#tag EndSetter
		ContainerSkin As Skins
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDropdownSkin
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim oldValue as String = SkinToString( mContainerSkin )
			  dim theValue as String = SkinToString( value )
			  
			  dim strExec() as String
			  strExec.Append( "var theDiv = window.GSjQuery('." + me.ControlID + "_dropdown');" )
			  if oldValue <> "" then strExec.Append( "theDiv.removeClass('select2-" + oldValue + "');" )
			  mDropdownSkin = value
			  if theValue <> "" then strExec.Append( "theDiv.addClass('select2-" + theValue + "');" )
			  Buffer( strExec )
			End Set
		#tag EndSetter
		DropdownSkin As Skins
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mEnabled
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEnabled = value
			  
			  dim strScript as String = "var thisSelect = window.GSjQuery('#" + me.ControlID + "_inner');" + _
			  "if( typeof( thisSelect != 'undefined' ) ) {" + _
			  "thisSelect.select2('enable', " + Lowercase( Str( value ) ) + ");" + _
			  "};"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		Enabled As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private EventsAdded As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FromFramework As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isLoaded As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Items() As GraffitiWebPopupMenuItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mLockUpdate
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLockUpdate = value
			  
			  if not mLockUpdate then UpdateOptions()
			End Set
		#tag EndSetter
		LockUpdate As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mAllowAdd As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMaxSelectedItems
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMaxSelectedItems = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		MaxSelectedItems As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mContainerSkin As Skins
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mDropdownSkin As Skins
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEnabled As Boolean = True
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMinimumInputLength
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMinimumInputLength = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		MinimumInputLength As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLockUpdate As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMaxSelectedItems As Integer = 5
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMinimumInputLength As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mMultipleSelect As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPlaceholderText As String = "Please select an option"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReadOnly As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSearchThreshold As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSelectedItem As GraffitiWebPopupMenuItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mStyleTag As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mMultipleSelect
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mMultipleSelect = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		MultipleSelect As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private OldStyle As WebStyle
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPlaceholderText
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPlaceholderText = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		PlaceholderText As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mReadOnly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReadOnly = value
			  
			  dim strScript as String = "var thisSelect = window.GSjQuery('#" + me.ControlID + "_inner');" + _
			  "if( typeof( thisSelect != 'undefined' ) ) {" + _
			  "thisSelect.select2('readonly', " + Lowercase( Str( value ) ) + ");" + _
			  "};"
			  
			  Buffer( strScript )
			End Set
		#tag EndSetter
		ReadOnly As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSearchThreshold
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mSearchThreshold = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		SearchThreshold As Integer
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSelectedItem
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Do not implement
			End Set
		#tag EndSetter
		SelectedItem As GraffitiWebPopupMenuItem
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mStyleTag
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not IsNull( value ) then
			    if not WebStyle( value ).IsDefaultStyle then
			      dim strExec() as String
			      strExec.Append( "var theItems = window.GSjQuery('#" + me.ControlID + "').find('li.select2-search-choice');" )
			      if not IsNull( mStyleTag ) then
			        if not mStyleTag.IsDefaultStyle then strExec.Append( "theItems.removeClass('" + mStyleTag.Name + "');" )
			      end if
			      mStyleTag = value
			      strExec.Append( "theItems.addClass('" + mStyleTag.Name + "');" )
			      Buffer( strExec )
			    end if
			  end if
			End Set
		#tag EndSetter
		StyleTag As WebStyle
	#tag EndComputedProperty


	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.popupmenu", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jqReplace, Type = String, Dynamic = False, Default = \"window.GSjQuery.extend({\r    replaceTag: function (currentElem\x2C newTagObj\x2C keepProps) {\r        var $currentElem \x3D window.GSjQuery(currentElem);\r        var i\x2C $newTag \x3D window.GSjQuery(newTagObj).clone();\r        var theParent \x3D $currentElem.parent();\r        if (keepProps) { //{{{\r            newTag \x3D $newTag[0];\r            newTag.className \x3D currentElem.className;\r            window.GSjQuery.extend(newTag.classList\x2C currentElem.classList);\r            window.GSjQuery.extend(newTag.attributes\x2C currentElem.attributes);\r            var events \x3D window.GSjQuery._data(currentElem\x2C \'events\');\r            $currentElem.remove();\rif(typeof(events) !\x3D\x3D \'undefined\') {\r            window.GSjQuery.each(events\x2C function (i\x2C event) {\r                window.GSjQuery.each(event\x2C function (j\x2C h) {\r                    $newTag.bind(i\x2Ch.handler);\r                });\r            });\r}\r        } //}}}\r        $newTag.appendTo(theParent);\r        return this;\r    }\r});\r\rwindow.GSjQuery.fn.extend({\r    replaceTag: function (newTagObj\x2C keepProps) {\r        return this.each(function() {\r            jQuery.replaceTag(this\x2C newTagObj\x2C keepProps);\r        });\r    }\r});\r", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsSearchCrit, Type = String, Dynamic = False, Default = \"createSearchChoice: function (term\x2C data) {\r    return {\r        id: term\x2C\r        text: term\r    };\r}\x2C\r", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACABAMAAAAxEHz4AAAAJFBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHK3Oh71AAAAC3RSTlMAAwQMITZna6Wq+62EtAQAAACnSURBVGje7ZaxDcMwDARdmWtpDc/CMhO4TK8JXCbLpbTIJuIbLhLclSoOB5iivCwAAADwb7xnORAgQIDgi+A5y87yvY/aV9jy6aM4B5YOX606SB4Pe3kSLQeUR9lTQFlgKaB+mTwG1AUWA4Tr7CFAEFgIUBaKjwGKwMYAaaX5ECAJbAjQlqqfAZrAzgBxrXu/+C6sjVX8w+8CvzgIECC4UwAAAACQ+ACKh9nKzCq0nwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAJFBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHK3Oh71AAAAC3RSTlMAAQIEBg5AUViYoKsfU4sAAAArSURBVAiZY2AgAuwGgQRkRigIGCgpQUQSZ2+HSkk1QKUYselyAQEFvFYBAMZqH4NFlyESAAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant


	#tag Enum, Name = Skins, Type = Integer, Flags = &h0
		None
		  Error
		  Success
		  Warning
		  Grey
		  DarkGrey
		  LightBlue
		  Blue
		  Lime
		  Pomelo
		  Orange
		BlueGreen
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="AllowAdd"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ContainerSkin"
			Visible=true
			Group="Behavior"
			Type="Skins"
			EditorType="Enum"
			#tag EnumValues
				"0 - None"
				"1 - Error"
				"2 - Success"
				"3 - Warning"
				"4 - Grey"
				"5 - DarkGrey"
				"6 - LightBlue"
				"7 - Blue"
				"8 - Lime"
				"9 - Pomelo"
				"10 - Orange"
				"11 - BlueGreen"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="DropdownSkin"
			Visible=true
			Group="Behavior"
			Type="Skins"
			EditorType="Enum"
			#tag EnumValues
				"0 - None"
				"1 - Error"
				"2 - Success"
				"3 - Warning"
				"4 - Grey"
				"5 - DarkGrey"
				"6 - LightBlue"
				"7 - Blue"
				"8 - Lime"
				"9 - Pomelo"
				"10 - Orange"
				"11 - BlueGreen"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="25"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MaxSelectedItems"
			Visible=true
			Group="Behavior"
			InitialValue="5"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimumInputLength"
			Visible=true
			Group="Behavior"
			InitialValue="3"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MultipleSelect"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PlaceholderText"
			Visible=true
			Group="Behavior"
			InitialValue="Select an Option"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SearchThreshold"
			Visible=true
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockUpdate"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
