#tag Class
Protected Class GraffitiWebSpeak
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  select case Name
		  case "functionSupport"
		    if Parameters.Ubound >= 0 then
		      mIsSupported = Parameters(0).BooleanValue
		      
		    end if
		  case "voicesLoadStart"
		    ReDim Voices(-1)
		  case "supportedVoice"
		    if not VoiceInit then Return True
		    if Parameters.Ubound >= 0 then
		      dim theVoice as new GraffitiWebSpeakVoice( Parameters(0).StringValue )
		      if Parameters.Ubound >= 2 then
		        theVoice.Language = Parameters(1).StringValue
		        theVoice.Default = Parameters(2).BooleanValue
		      elseif Parameters.Ubound = 1 then
		        theVoice.Default = Parameters(1).BooleanValue
		      end if
		      Voices.Append( theVoice )
		    end if
		  case "voicesLoaded"
		    if VoiceInit then VoicesLoaded()
		    VoiceInit = False
		  case "voiceComplete"
		    mSpeaking = False
		    if Parameters.Ubound >= 0 then SpeakingComplete( Parameters(0).StringValue )
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  '//
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  dim strExec() as String
		  strExec.Append( "var functionSupport = ('speechSynthesis' in window);" )
		  
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'functionSupport',[functionSupport]);" )
		  strExec.Append( "if (functionSupport) {" )
		  if Session.Browser = WebSession.BrowserType.Chrome then strExec.Append( "window.speechSynthesis.onvoiceschanged = function(e) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'voiceLoadStart');" )
		  strExec.Append( "window.GSVoices = speechSynthesis.getVoices();" )
		  strExec.Append( "for(i = 0; i < window.GSVoices.length; i++) {" )
		  strExec.Append( "var currVoice = window.GSVoices[i];" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'supportedVoice', [currVoice.name,currVoice.lang,currVoice.default]);" )
		  strExec.Append( "}" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'voicesLoaded' );" )
		  if Session.Browser = WebSession.BrowserType.Chrome then strExec.Append( "};" )
		  strExec.Append( "}" )
		  
		  Return Join( strExec, "" )
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  'dim strExec() as String
		  ''strExec.Append( "var functionSupport = ('speechSynthesis' in window);" )
		  ''
		  ''strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'functionSupport',[functionSupport]);" )
		  ''
		  ''strExec.Append( "window.speechSynthesis.onvoiceschanged = function(e) {" )
		  ''strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'voiceLoadStart');" )
		  ''strExec.Append( "window.GSVoices = speechSynthesis.getVoices();" )
		  ''strExec.Append( "for(i = 0; i < window.GSVoices.length; i++) {" )
		  ''strExec.Append( "var currVoice = window.GSVoices[i];" )
		  ''strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'supportedVoice', [currVoice.name,currVoice.lang,currVoice.default]);" )
		  ''strExec.Append( "}" )
		  ''strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'voicesLoaded' );" )
		  ''strExec.Append( "};" )
		  '
		  'Buffer( strExec )
		  
		  RunBuffer()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub Cancel()
		  mSpeaking = False
		  Buffer( "window.speechSynthesis.cancel();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Pause()
		  mSpeaking = False
		  Buffer( "window.speechSynthesis.pause();" )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Resume()
		  Buffer( "window.speechSynthesis.resume();" )
		  mSpeaking = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Speak(Message as String, theVoice as GraffitiWebSpeakVoice = nil, Volume as Double = 1.0,  Rate as Double = 1, Pitch as Double = 1)
		  if not IsSupported then Return
		  if Message.LenB = 0 then Return
		  
		  dim strExec() as String
		  strExec.Append( "var voiceMessage = new SpeechSynthesisUtterance('" + EscapeString( Message ) + "');" )
		  
		  if not IsNull( theVoice ) then
		    strExec.Append( "for(i = 0; i < window.GSVoices.length; i++) {" )
		    strExec.Append( "if(window.GSVoices[i].name === '" + theVoice.Name + "') {" )
		    strExec.Append( "voiceMessage.voice = window.GSVoices[i];" )
		    strExec.Append( "}" )
		    strExec.Append( "}" )
		    strExec.Append( "voiceMessage.lang = '" + theVoice.Language + "';" )
		  end if
		  
		  strExec.Append( "voiceMessage.volume = parseFloat(" + Str( Volume ) + ");" )
		  strExec.Append( "voiceMessage.rate = parseFloat(" + Str( Rate ) + ");" )
		  strExec.Append( "voiceMessage.pitch = parseFloat(" + Str( Pitch ) + ");" )
		  
		  strExec.Append( "voiceMessage.onend = function(e) {" )
		  strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'voiceComplete', [e.currentTarget.text]);" )
		  strExec.Append( "};" )
		  
		  strExec.Append( "window.speechSynthesis.speak(voiceMessage);" )
		  
		  Buffer( strExec )
		  mSpeaking = True
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event SpeakingComplete(theText as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event VoicesLoaded()
	#tag EndHook


	#tag Note, Name = License
		Implementation completed by Anthony G. Cyphers, taken from documentation available at:
		https://developer.mozilla.org/en-US/docs/Web/API/Detecting_device_orientation
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIsSupported
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		IsSupported As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mIsSupported As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mSpeaking As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mSpeaking
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '//
			End Set
		#tag EndSetter
		Speaking As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private VoiceInit As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		Voices() As GraffitiWebSpeakVoice
	#tag EndProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.webspeak", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA74SURBVHhe7Z0LlFtFGcfnu8m2e7N9WMADAlVBPeWhiEceBRQEEWiBTbYPQCjqORxaHi1ssqW8hFJsqRWSbCkUAVF5im7tJq0t5VVAwK4ocJBXK6i8jghYamE32W429/Ob5KMtzZ1JtrnJJnvmd056v2+SJtn7/We+mblzJ8JgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAz1DPDRsJM0BiNjLRBxMr9Oj/cQ8fEA+hdtXHH9x7kX1DhGAGUQmDh7DxzmvEon8TNclINEsBlALEo1vPMz0dGR5eKaxOKjYSeg4M/fMfgSABhN/14XyIx9yw5FLubimsQIoAwo+LLZ17EnvaY9EIq8ZjeHW7ispjACKI9+Phbjy2DBcjsUvp79msHHR8NO4B833k/N/SnsFgUEHNkw7ogT6fFcZsO6/3DxoDI0O4HT2prsblxKVjOg8CPg046FF2xZ3v7P/Au8g5r3uXS4Ju+VDgqclU7Eb2J30BhyArBPDu8l/LCKeuE75ue3UqNHjhN3zutl3zOaJrceiP3WTDqb53FRqURTidhstgeFIZUCNMGXjG7Y0tebWb/uSfY9I/Nq1wfUpK/y73/IHYCWTcOAQ/mpYlBKGD8ms6FrDftVZ8gIoEjw86DYSIHqYM9z+tc/8xEFc5XvgMMfoc/6AuX8ffkpNQDjqU+wG32vB7ikqgwJAZQU/Dyv04m+n+2K0f9q19v967vuathvfJYifBwXqwFxGLUEe0jxcEnVqPth4ACCX3VSifh8B52jyXwjX6IB4Dw7GLmVvapR1wLIB1+srsXgf0Jvsv1JdHxHoRCPcJES+jum28HwBexWhbodBeSC30DBF3AQFxUHRTKVjIXYGzAjmtsOyArneLTADw48nV4R/TM/VRI0ZPw5HWbkPTWO5Xyrd3n70+xWlLpsAewpMvjwwICCXyYUvMscC18GCxbTSYuChV2BYDjZFGz7Dr+kKDTkk8PE9rynBrLW7WLqBSPYrSh1J4Bc8Ptl8MXXuKjiNLW0yo7cwry3HQDNCPgYNdur+TVFIRGEEfE2dl2hVLB/oK/xdnYrSl0JIBf8jFhTzeBLHEffkweACYjWo4HmyJVcpCWdjM8gEdzLrjsgzqAW5lL2KkbdCGBr8AG+ykVVgz6ziU09lphPrcFKuUiES5SQCKYJxHXsqrhqxJTLP8t2RagLAeSDDw8ORvAliLCWzaLIi0PUhP/Fbm49hIuUgGVNpzfvY7cQEp7T33sVexWh5gXAOZ+CLw7koqqTTkZXIoqSczKlqN2FZf2hmAh6OqMvkQqms6sAZgWCsw9mx3NqWgD54IuH6IQOWvA/IZ2MTQeE2SSED7lIS6kiSCWid9KhyARQtmKtQM0KYFvw4QAuGnR6ktFoeiR8nkQwnxJD0cUgUgRyIUhg6uw9uMgdcObRv+qrlACTAi1tJ7PnKTUpAM75D9dS8LdyT7SHWoOrssK3H3nL8oU6YCxmnDvYcSXV2f4uCepadl1Bx4mw6Sk1dzFoa/BpLMxFXrLBq4tB2fV/2pRZv67Dv/8RFtX0Y7jYFXr+K/5xRzb2b1j3KBcVkFnf9VTDuPFnUm3flYs+BXUu9xk2bvzyzIau97nIE2qqBbBbZu1NQ71HKhT8ipDujM2lCF/CrhIAvKwx1HYau+4ALGLLFUfA2Wx6Rs0IwG6Zs7dw/FTzQTatdUWqM3YDNdHns6sEBF7NpiupROyXNCx8i10X0HMBUOtUPk2TwseiA/MQ8SDK2zYV0QHz741Un9mQpfyRnnzugBngxSDZeUt13FDy4s2m5shZaIl72FWAc1KJuHJ1cCAYmUNnR9kSOMI5vTfR/jt2y6bsFiAQaj2Bgi8nSr5NtXc0fflh9GigGPtzD6B+Ru4B9Fk5MQxO8AeAHQwvDIQivSLjvGsHI6/kgjKtrehsYM+K2L0o8NfsuoNw9Ygps5Sze6lhvUvpRSl2C6B65Wkr4EEKsBawMSQIhMI3kpAvI3O49HP9EaqRdjf+O9Ac/oEs0+EXfbNRiPfYLQTEiGx/Qxt7hXQs7aZhprKjKmcaxTFzG9ktm/IFgIM/SeMVwye17kuneBa7n4KarVHCgjsDwTbt5dyPEzdvpGGBOsAEoJjGpjsWaBeJNu7afRibZVO+AEDInD8ksLJW8fUFgBdTSljMnis9nZQKUNMXALEXpRZlX8ROZx5k0xXLcQ5ns2w8SAFDB0vgP9jUA+IiuyV8EXuugOXIW8aVUGpRtgIfrlnyEfUllHMG9L+NACpBTzL+Ig3DfsOuFuqMLQ6E2r7JbgGpzvbn6NCZ91yZLI75oTKX0/s/waYLWEMpYIiRSsbPpBO8hF09iHPYcgUdvJtNVxrH7Kq8UITg/I1NF2Ds8ODFX2SnLIwAXKBxumzezyl65Q/EaZQKxrNXQDptPcSmK4CoFICT9WkEIIQP/J7MlhoBKJCzcpbTfwDl4oe5yBXtuPzhaA8i6haTKAWwZWX0X/TZm9gtANAZw2ZZGAFo6Fl543vpRPwECsR6LnJBPz0LAl5h0wXQLvQgcSk/17HACKBqoPgxWy7AyMbQ7CPYKQBBqAUAYje2XEEhNrNZAInDCKBapA8eRb159fSsD/FINgsAx1Fe3KE+gHbBJwAqBUA9TCOAqjFvnkPhUNZkREd5ncABS5nHKcLWqCnhXdgrgDqhSgEgmBag2rzLxwLQggCbBfh9+pEE9oMmDYA6BQg0AqgyyuYaKEuwWUA2gwXbyG0P9mbUPX2BI9ksAFEtjoFgBFAqKDQ3e2gu3/osbU3tXrPkAzYLoE7gaDYLABDq1DIAjABKIBCMnEjVcS92CwBH/JXNQhCV/4+Gl/9l053chpNK/sfHsjACKA25E5g7KDI9w9/5I3suoHplM4Ky9ktolDCKzQLQUU8SDQQjgCLYwcjdVPvV43whfqvbDxhBKAVAzfjLbLoDsA9bBdAAwgigkshm3w6GX9RdtpVQR0179RAQ5BYxKp7lYwFNp160Ox006SNrUkClCARbr6DIFr0Tmcbpa1PJ+Gp2C2hqbj2e3ke5YIbEpRSA8Pu1i1MsR3iy6aURwA4EQpGYgNLWOdLJu45NVxyAqWy64vOhUgDoqPc9IuFt7FnZrk8fJWIEsB0jJkXkPQnhvKeHcvu1PcmYctWOXF9ILYjyzl8K4mMfLYtrJolQ2e+gVmVAexPpMALYjqzjjGNTi9zdI3dHkAZfVi8k6uFrFovIeypgAjsFgINGAJXAyorX2VRCNfe23O4eGgLB8ERKIzPZLYBGDh+lNo9SCoA6oBN0fQdhYRdbZVO2AOiP0a5cqSc4r/4+7xVCf2trOhnTb/M2d65Fr4uy5wqNHO4WT8zT3F6urv2S4Q3Da6cFQIDC3bPqmJQfz6ZaLjdtyINynh+XWJbYP52IaZeDSwIvfHwD5X7t/Y3ZrH6rOAScwmYBKPDxTR2LPLkOIClbAL2d0ftJ0WfTV3ubi+qbZfE01fIfUaLdE7N4RCoZC8g1gt3LY5pVQXl4Vy9t7qf+w4ItK+PKVBMItU0HAcoNJeg57ULTgeLpfXojmy8Z5yDmbqkqmWF0UpwsaQgPJz3KnTQrd+9gmTuF6pC/CSR3A2HXHRTvpIaN/JLomKfcGMoOhZ+jIH+D3U9Btf/j9KZRu+jTx8Co3MneCZqCke9S8/cQfa3KdE4rJIDcPYMWbEsbSpwZqUS7cpNIO9h2KgCuYNeNpalE7EK2PaGmRgFyXA0IJ1Br4HBRzZPb3LmE4FPH8Fe64EuoGdTeU0iVw9PmX1JTApDkRCDqQwR2qG0BdfhuZlcJdSpfSG8aqd0OjvoP55AC1FvNoHgm3Rn3bPj3CTUnAMk2EYia/NVNO9R6HgXsDaqxV3CRHsDp2rx90izqN4HcKUwJfVbREcjOUJMCkORFIE6sJRHIK4SBUKQLhHWLAPgCF2txHPx+OhF/hl1X7OH+ayjCuoUjj/Yk4/ex6yk1KwDJVhGUsCdfpZHNPQVJ3rdf8p25KGBy74q4dlcyO9hKHb/chhRKHHTms+k5NS0ACXcMTxpMEcjfBCi5uWeoC9OcTkS1w0K7+ZI9KQTaXUKp83jXluTix9n1nJoXgGSbCEQmX1JdHJGdyGYJ4LPUZB+eTrav5AI10H8rgPgce65kM/0Vq/2SuhCAJC8CMYGqxKCIoCRQ3JRKxA8plvMlgVA4Rk2/9mdnafSwsG/Vja+xWxHqRgAS7hNIEai3WK8IoL34QjX+FXDEtFQy5rq/0I7YwchP6D2LTRk/nE7GBpR2doa6EoCERTCRzlDVRJBOxuUVQrcLOK9TIGdQjT9QbhHHZVqo5l9Ozb7mZtNc8N9Hn3UuuxWlpqaCB0Ju2ljgahqODeOi4pQ5FdwYjJxuAcqfgLPpvZ4nYcgfqC6ZQHP4amHpx/sSBBFMd8Z0U8KeUbcCkOSvHQj5a5ulXYCq4MWgYtihyC/oZJ/DrhoUV9N3pBRRHeouBWwPdwzlPvpb8iW1h9wV1A6G5Y9elBB8XF7N4EvqWgCSnAgA5DDN85+FL5fGlvAZTn/D8/T9vsdFalB0ppLxyexVjboXgKSnM7oWwDmZalBNiEDu4GWHwndZCPKmEeUU71ZQdFDNn8ReVRkSApD0dLavpXRAowP1rdrUaXyJzYoRCEWu9Anf61Dq3v7o3E/B1/+OQAWp606gG03N4WMRYBX9ZTuuqpVzyfv1JWJ/Z98zxky9dPSWvsyFNDqYWWxmb3sQxT001vd09++BMmRagE/oWRF/DNE5mmr71vlzOVFDZ/uUSgSfxvXn9mb63ibBLRhg8BcOdvAlQ64F2J7RJ58/Jmv7/d3L1JswlENTMHwmtTYlTQBtBfFNYVkXpjqjcvg66AxpAVSaQDDyFJ3Bo9gtCqWI+4Zl0jM3r7rFk1u7vWDIpYBqQqlFuYfPp8FedMTMdCJ2Vi0FX2IEUA4ARUcVJJIbrH4Ym14RK7p2cDAwKaAM7Ja2QwHR/dIv4i2Oz1rUuzz6JpfUJEYAZdLU0nYcOs5P6VQeTGfzferdP+KD7KLuxOJX+SUGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwVQIj/A4gDC/uVqPC/AAAAAElFTkSuQmCC", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAEoSURBVDhP3ZG9SgNBEMdndiPIifgC2usTKEmtbQ7E+BrxfIFgG/WOYCdiJwoWUSxUrDWFYCEIaSy8PMMp6O3fudzkU0Hr/GB3ZudrZ3dogvH86pqqNFPe2vbKwf2wrQerHCFLAPOuI7dJX65lrd1IGbEB77k0LX1cNd40lIzKPr1kPXZxTKsGZl1ui4wt1NXcpV/A8wNkazxZEk6ZsC/qAhjPRCjmnpwfHYzj2EUgDojRAkxJXh0P/8WfBbT1FxDNMqMjXcwlzehW3YMCAO3IHoJwo6YcdkdivxatCIen3Djg1ynIyM7EU8mmwI5emU1DCtUIpiZPOU6a4aGGklU5wmf74bywuDIvyXfvl9Hj1NJyLP8QSrsnyUV4oGH/Z9qvVrKlx8mC6BvZ6GtO/SjW8AAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsSupported"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Speaking"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
