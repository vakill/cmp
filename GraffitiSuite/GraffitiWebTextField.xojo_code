#tag Class
Protected Class GraffitiWebTextField
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "focus"
		      GotFocus()
		    case "blur"
		      LostFocus()
		      if Parameters.Ubound >= 0 then
		        if mValue <> Parameters(0).StringValue then
		          mValue = Parameters(0).StringValue
		          ValueChanged()
		        end if
		      else
		        mValue = ""
		        ValueChanged()
		      end if
		    case "labelClick"
		      LabelClick()
		    end select
		  Catch
		    
		  End Try
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '//
		  
		  select case Name
		  case "Style"
		    UpdateOptions()
		    Return True
		  case "Enabled"
		    if LibrariesLoaded and isShown then
		      
		      dim strExec() as String
		      strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
		      strExec.Append( "theField.prop('disabled', " + Str( not Value.BooleanValue ).Lowercase + ");" )
		      Buffer( strExec )
		      
		      Return True
		    end if
		  end select
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Open()
		  
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '// Do not implement
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  Styles(0).Value("visibility") = "visible"
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  Dim source() As String
		  
		  source.Append( "<div id='" + me.ControlID + "' class='" + if(not IsNull( me.Style ), " " + me.Style.Name, "" ) + "' style='overflow:visible;display:none;'>" )
		  source.Append( "<div class='input-group' style='width:100%;height:100%;box-sizing:border-box;'>" )
		  source.Append( "<div class='input-group-prepend'>" )
		  source.Append( "<span class='input-group-text'>" + EscapeString( mLabel ) + "</span>" )
		  source.Append( "</div>" )
		  source.Append( "<input type='" + GetFieldType() + "' class='form-control' placeholder='" + EscapeString( mPlaceholder ) + "' style='box-sizing:border-box;height:100%;line-height:1em;' value='" + EscapeString( mValue ) + "'>" )
		  source.Append( "</div>" )
		  source.append( "</div>" )
		  
		  Return Join(source,"")
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  'UpdateOptions()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function GetFieldType() As String
		  select case Type
		  case FieldTypes.Color
		    Return "color"
		  case FieldTypes.Date
		    Return "date"
		  case FieldTypes.DateTime
		    Return "datetime"
		  case FieldTypes.DateTimeLocal
		    Return "datetime-local"
		  case FieldTypes.Email
		    Return "email"
		  case FieldTypes.Number
		    Return "number"
		  case FieldTypes.Password
		    Return "password"
		  case FieldTypes.Telephone
		    Return "tel"
		  case FieldTypes.Time
		    Return "time"
		  case FieldTypes.URL
		    Return "url"
		  case FieldTypes.Week
		    Return "week"
		  case else
		    Return "text"
		  end select
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebPagination -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddBootstrap4( CurrentSession ) )
		    strOut.Append( AddFontAwesome5( CurrentSession ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebPagination -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateOptions()
		  if LibrariesLoaded then
		    dim strExec() as String
		    
		    strExec.Append( "var myLabel = window.GSjQuery('#" + me.ControlID + " span.input-group-addon');" )
		    
		    strExec.Append( "myLabel.on('click touchend', function() { " )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'labelClick');" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "var myField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
		    
		    strExec.Append( "myField.on('blur', function() {" )
		    strExec.Append( "var newValue = myField.val();" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'blur', [newValue]);" )
		    strExec.Append( "});" )
		    
		    strExec.Append( "myField.on('focus', function() {" )
		    strExec.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'focus');" )
		    strExec.Append( "});" )
		    
		    me.ExecuteJavaScript( Join( strExec, "" ) )
		    
		    RunBuffer()
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event GotFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LabelClick()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event LostFocus()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Open()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event ValueChanged()
	#tag EndHook


	#tag Note, Name = License
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		Project Home
		https://getbootstrap.com/docs/4.0/components/forms/
		
		MIT License
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mFieldStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  
			  if not IsNull( mFieldStyle ) then strExec.Append( "theField.removeClass('" + EscapeString( mFieldStyle.Name ) + "');" )
			  
			  mFieldStyle = value
			  
			  if not IsNull( mFieldStyle ) then
			    if mFieldStyle.Name <> "_Style" then strExec.Append( "theField.addClass('" + mFieldStyle.Name + "');" )
			  end if
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		FieldStyle As WebStyle
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private jsFile As WebFile
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mLabel
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLabel = value
			  
			  dim strExec() as String
			  strExec.Append( "var theLabel = window.GSjQuery('#" + me.ControlID + " span.input-group-text');" )
			  strExec.Append( "theLabel.html('" + EscapeString( mLabel ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Label As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mLabelStyle
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  dim strExec() as String
			  strExec.Append( "var theLabel = window.GSjQuery('#" + me.ControlID + " span.input-group-text');" )
			  
			  if not IsNull( mLabelStyle ) then strExec.Append( "theLabel.removeClass('" + EscapeString( mLabelStyle.Name ) + "');" )
			  
			  mLabelStyle = value
			  
			  if not IsNull( mLabelStyle ) then
			    if mLabelStyle.Name <> "_Style" then strExec.Append( "theLabel.addClass('" + mLabelStyle.Name + "');" )
			  end if
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		LabelStyle As WebStyle
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  if not value Then Return
			  
			  mLibrariesLoaded = value
			  
			  UpdateOptions()
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mFieldStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabel As String = "Untitled"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLabelStyle As WebStyle
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPlaceholder As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mReadOnly As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mRequired As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mState As States
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mType As FieldTypes
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mValue As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mPlaceholder
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPlaceholder = value
			  
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  strExec.Append( "theField.attr('placeholder', '" + EscapeString( mPlaceholder ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Placeholder As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mReadOnly
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mReadOnly = value
			  
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  strExec.Append( "theField.prop('readonly', " + Str( Value ).Lowercase + ");" )
			  Buffer( strExec )
			  
			End Set
		#tag EndSetter
		ReadOnly As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mRequired
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mRequired = value
			  
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  strExec.Append( "theField.prop('required', " + Str( Value ).Lowercase + ");" )
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Required As Boolean
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mState
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mState = value
			  
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  strExec.Append( "theField.removeClass('is-valid').removeClass('is-invalid');" )
			  select case mState
			  case states.Valid
			    strExec.Append( "theField.addClass('is-valid');" )
			  case states.Invalid
			    strExec.Append( "theField.addClass('is-invalid');" )
			  end select
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		State As States
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mType = value
			  
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  strExec.Append( "theField.prop('type','" + EscapeString( GetFieldType() ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Type As FieldTypes
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mValue = value
			  
			  dim strExec() as String
			  strExec.Append( "var theField = window.GSjQuery('#" + me.ControlID + " input.form-control');" )
			  strExec.Append( "theField.val('" + EscapeString( mValue ) + "');" )
			  
			  Buffer( strExec )
			End Set
		#tag EndSetter
		Value As String
	#tag EndComputedProperty


	#tag Constant, Name = APIVersion, Type = Double, Dynamic = False, Default = \"3", Scope = Private
	#tag EndConstant

	#tag Constant, Name = IncludeInTabOrder, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.textfield", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAMkklEQVR4nO2dUYgk13WGv78ZhqZ7WYZhsoiNEI4RwiRC2MIoQtkXm+DABns64Ie8BIE2MSEPBk9DwAThB2GUPPQ4hDzENsQWIXkw2PSY4FWEHpJltSiQrIVw3kIijDHLMizDsj0MzTB/Hm7V1K3q6tWud6a7Urd/aLq76nbXrbr/Ofecc+89V8askC46y67ACsvFigCJY0WAxLEiQOJYESBxrC27AlX0B8MOGBAAk/HoZLk1ejKE+wlo4r2oSW5gb3unJ2knOnQCHAOHwJHtI0mHtg8k3Qfvg/Yn49GDZdS3Pxhu2myBN0EXJTZsuhI9211QVyoJ2QfA25Px6HgZ9a1DwzSAeuA3bJB0etQ2kpCE7SPgPvDA1oHk/f5g+DOb/5S4CdwBjs9a2vqDnQ6oCzxrc0XiM8BzEpugi8AF8EWJNRtARLeQ38P3gXcJpG4EGkWA8MCKB1c0fFxGXaBr+1IgBEh8ISszBW4De/3B8Kbt24d7u4dPUqfe9s6mpJdBnwN/CfSsRCevW/6e1S6ro4m7Magr2ww0igAB4cHlWsAuH8+fX3EuHMiIsG7zssRLtu9I3OgPdr4NujUZj6aPU4ve9s4FSVexrtn+LLD5CNcnlv74mETjGh8aSYBcioIGCJ/LD25WMxSSFaRPHYnLoD+0/WWJd/qDnTdB739c/9sfDLu2vyDpG+BPA51f4fpUpd6nTG4WCRrlBrpij+b9fna2dLxSMjpfVcdaA67avGXz573tnQvzrt/b3tkCRpK+C7wYiPTE16+8z7v6ctAoAsw+nPBQg/QEKQqv7Kxj0ui0rKtMAiR9UuKbwHd728NPVs/3BzsvSvoh8GfApbO+flaHGZIvG40iQBnOHnBsTOUawadloNoQxYMuGstxd/JlyW/1todPQ/DTe9vDT4P+AbhyntfPbYEmoWk2wJHNdwrDSRDq2AXWJbo2XdAlm6dAW+BOroILycwNRkWfTxtgDbgi8VZ/MLwG3pD0lu3fLFvquSFXtehnuoCp5Dugu+B9ianNEfhI0tTmJLJr3rOb4wJCwwJBdegPhh3b66Ev9zqwlvndG8BTNs+BvyjpBZsLWRnKxla1fzYEX/yfJW3ZfgXoVM7P/IekE2Bq+56k94HrwEdZQOoA/CD8r6a2jyWdeTzirNF4Ajwq+oPhMzZXJb5o+/OSuoX0xyrapyq6HKiZPR+7njYH4B9Lug5+ZzLevbe4uzs/tIYAOfqD4RZwFfia7eczLyDyyQuXrepm1pUhBJdugL8FurGssPN5oXUEyNEf7GzaekPitRCXpyLZZcS+emQLHIDftPU3h3ujo0XWf1FoLQEA+oPhBvB14KvZIM1DUDb0gAPgL4C/n4zb2fjQcgIA9AfDHvBX4D8FreUaIKj6OOoYjofvOrJ5XeKvmzRydx5oPQEA+oPhJ4Drtj9VHK3zErIz4gZoezIeHSyifstEgwNBZ4fJePQR8CbouAgvOxuggbKPz33QGyk0PiRCgAy3JP573slivgG3gZ8tsF5LRUIE8C+AW3FYOI7bZ28nkm4Cd5dUyYUjGQJMxrtHwL+Dprm050O6RVfAEfBe06N3Z4lkCJDhF2TTseaM2B2Df7nQGi0ZqRHgbj4YEw8UBZhwTsmof0iOAN4nzDQGyiHgbMTwBNyKGP+jomnDweeNaTxsnCMeBn7cuYP/35EUAWxFc/bKaOKEzUUgqS4gDv/GKGb+pIekCBCGe2fn7CUq/EBiBKiGfaEs+SlqgcRsgOosoOau2FkUktIAAWUpj8PCKSIpDZAbgfHyLog1wRIrtyQkRYDQ0JAv8qicTbIbSKoLyOf71yFF6YckNUB5rV7qSEoDFCuIZmMBKyMwGZQTUJweTVQjJEaA8pKvunX/qSGpLmC+lNcnokgBSRFgvoCn1/A5kiJAQJ7oIXx7WEKHFJAgAXJV76THAHIkZQRW0s3Vfk4NSREgxsoFDEiMAD6dFlZu9PppYikgKRsgHwmcnQJWNziUBpIiwDyk2viQXBcQUO3zU7YBktQAdYtDU0WrEkT0todr4IuEVHJ12cGeAv8bsFFn9NneB/1WnDGkkjFserjXrrwBLesC/FKW9PFynMg5QgdC+ri6EUFgC/y/eZLK3FiM0srdBH5vATeyMLSMAHSAHtCbTQGXq/s4gyhZGYhy/faqGcMidM/7BhaNVtkAdcbcbD9fFwCqdwOrRGkj2qYBTlGd8ZujuhAkniJWV7acHLp9aJUGiEf58u/V9zqpLqeKKWuIcg6B9qFlGkBRf+7oe96fx+WKhi2vFirv9nH6i+h/2oRWaYAif381N39ZkitG32mCiKrrGO9YUpRpF1pGgIDyVjPFsRjz9iN62H+2Ea3qAiTdAn5j/pYtXLb5KWGvgVOboDAA2bf59fxczf+fX+WXhFZFAj8O/cHwMvBftjfqjEFJ+5Px6NeWU7vloFUa4FEwawtAG/v2R0UrbYB5yEO8RbbQYPilPCiUFAGqy8KhbAekiKQIUEXqySEgMQLU5QdMVfJzJEWA+oGgtJEUAeaN7qVMhqQIEIeHU08PlyMpAgSUl4bPGyFMBUkFgvIRvepgT6qND4lpgHhEb3a4N007ICkC5HmCi5m+ikiRphZIigB56BfifYLyTSSXWa/lISkC1OUKzrHqAhJCdfAnZSMwKS8gR6rSXoekNMCq3WeRFAFWmEVSBKhbM5A6kiLAvC4gZS4kRYB8mfdDNo9ODkkRIGB2vUBbV/08CpJyA8tzAstr/lJ1DRPUADmKPYJCN5CmBkiUAHWNvdIASaBw/xwNBqU7GpiUDRBQ3R5u/obSKSA5DVBIejlTKKQZHEpQA1Aj8enuGJIcAYpGrkp7mnMDk+wCCvevfDxFDZAcAYpZQfnK4PxzmkiNAOt1oeBYE/QHO0l1i4kRwFvg6J5DBLAwCt2x2VpS5ZaCxAigS6A1KHcFkQZYk3RpSZVbChIjgJ+WWCukPqAYDdQacHlJlVsKkiFAfzDsgn4bvB6kvmoECpsu8Dv9wTCZ55LMjQJPg18JDc+cxSDuAFdsJ9MNpESAV4Bn430Ewu5h+VDw6R4CL4KeX04VF48k8gT2tnc+IXEd9KnZs54xCCVu2N4+3Ntt1e4gdWi9BugPhj2Joc2zxdEg8Xn/H+cKyELEL0l6rT8Ytj4m0GoC9AfDDeB10B9LyhqzyA9YjvzGy8bpAq/bfCUYj+1Fa7uA/mC4afMG+DVJWSOGxi8SRUMRGaydE3AAfBP428l4dHT+tV48WkeA/mC4ZfuqpK/ZPC+5FPjJt44tPACVYgL5seg3U5sbEt8C35iMdx8s7m7OH60hQH8wfAa4Cvy+7d+V1C0avdg8ojgG1U0l4q1lA0lKOYUPgB8D122/c7i3e2/xd3n2aDwBsqDMmu11Qqh23eaixAZwCXgO2AZesLkgsR7793FeoMpmEMfAT4Atm5clOnGZggCObYUTW1OJe8At4F+Aj8B3QfeBB+Bj0BQ4Bo4n49HJuT6gJ0SjCNAf7FwAjSqH12yvZ/14/roEPBUGd5QZsvl9zM75n4N/Ba4BG+Dv2XqhaOx5v529hu0pcAd0JyPGFDjKXlMgJsB74H+ajHePP65yi0LD3Bx1ga/UL9qobvQU5/yrGnTMlA/lhMQx8D7w6mQ8+nl/MOzYelXyP2Zxgk49gVz6nGuJTCM9I/mZskE5C5t1ST8gaIdGoFFuYL5TZz2K/D5FX52r6NPhXGYne6r0W+BHZI0PMBmPTg73Rh+AXgVuLeD6jUKjCBAkCuqlL38V/nvx4GO1Hz/sUoN8BH4dfG0yHv1P9dqT8eg/bP+BpL8D7Z/99etmIy8fjSJAQG3jRW5b0RAFitW+5d8Jm2PgbfAfSfrLh7lxh3u7+8AQ+BPwB0T99xNcn4I4zdMETbMB5n4uu3CuWvTZ++kPTsB3gBuSvg2+NRnvTh+lBpPx6BAY97aH70pcBa6BPiux+RjXr7mvuJtoDhpGAIilKze0wucigFNe0lX6zdTmNnhP0k3gdtagj43DvdED4Af9wfBd8Mugz0l8iTCi2Km/fu4ZFPfycEN1+WggAaoPKJYuIXEEZD43B8A+6EPwT21ugu6Azsz/noxH94Cf9LZ33gZ9g0CAK6DPEGIQG6AN4ALoIrBWtxlVU6ecN4wAPgS+nn/LHtqx7SNJh9n7EXAPfD+86+6vKuWPg8O93RPgEPgwe9EfDDeBLfBm1vgb4C7QtemG2EU8CZUPaZALCA0LBMFp5A8ILtoy63IWaPr9NI4AKywWDXQDV1gkVgRIHCsCJI4VARLHigCJ4/8AmndA5Jnqe0EAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABJ0lEQVQ4jZWTsUrDUBSGvz84SNJBioOTiKOzaAfBR2gmcfMZ2uADdJbkDXST4pa+gItbhw7iJA7iJCIOYkIQ8TgkQ2jS2/rf6XI//nMO/7kyjLqCMOoCW2CvWZp8BGHkVfcN0EuWxl91fo2GrAecm6nw+9FYsA0cAzkwAmZ1WvMdlF0M1820D3YN3IMGEs9ZGv806zmOHw5u/HB44mK85gg1cyMHChfjNFhFCw2CMOpK6gCbQRj5i7iWFKCMzk7NdCSxCzwA05U7yNL4F3QFdgtcmNmsjXOOkKVxAXwDeT5JmvEtMyglaNmTlQ0kPJCTcaXQAdsDO6z+Q3uR+VX2+1EPOJPYAd6ATvX0ZGaX+SR5rPONGCXewe6AMWhaGRwAvqTPpR38V39d23fXVFI0bAAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant


	#tag Enum, Name = FieldTypes, Type = Integer, Flags = &h0
		Text
		  Color
		  Date
		  DateTime
		  DateTimeLocal
		  Email
		  Number
		  Password
		  Telephone
		  Time
		  URL
		Week
	#tag EndEnum

	#tag Enum, Name = States, Type = Integer, Flags = &h0
		Unvalidated
		  Valid
		Invalid
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="400"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="300"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Label"
			Visible=true
			Group="Behavior"
			InitialValue="Untitled"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Placeholder"
			Visible=true
			Group="Behavior"
			InitialValue="Type Here"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Value"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			Type="FieldTypes"
			EditorType="Enum"
			#tag EnumValues
				"0 - Text"
				"1 - Color"
				"2 - Date"
				"3 - DateTime"
				"4 - DateTimeLocal"
				"5 - Email"
				"6 - Number"
				"7 - Password"
				"8 - Telephone"
				"9 - Time"
				"10 - URL"
				"11 - Week"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Required"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="State"
			Group="Behavior"
			Type="States"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unvalidated"
				"1 - Valid"
				"2 - Invalid"
			#tag EndEnumValues
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
