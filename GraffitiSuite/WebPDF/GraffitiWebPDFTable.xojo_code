#tag Class
Protected Class GraffitiWebPDFTable
	#tag Method, Flags = &h0
		Sub AddCellStyle(Row as Integer, Column as Integer, theStyle as GraffitiWebPDFCellStyle)
		  dim d as new Dictionary
		  d.Value( "row" ) = Row
		  d.Value( "col" ) = Column
		  d.Value( "style" ) = theStyle
		  
		  cellStyles.Append( d )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddColumn(Title as String, Width as Integer = -1)
		  dim p as Pair = title : width
		  Columns.Append( p )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddRow(ParamArray Values as String)
		  dim d as new Dictionary
		  
		  dim intCurr as Integer = 0
		  for each v as String in Values
		    d.Value( intCurr ) = v
		    intCurr = intCurr + 1
		  next
		  
		  Rows.Append( d )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Clone() As GraffitiWebPDFTable
		  dim t as new GraffitiWebPDFTable
		  t.AvoidPageSplit = AvoidPageSplit
		  t.CellAltBorder = CellAltBorder
		  t.CellAltBorderColor = CellAltBorderColor
		  t.CellAltColor = CellAltColor
		  t.CellAltTextColor = CellAltTextColor
		  t.CellAltTextFont = CellAltTextFont
		  t.CellAltTextFontStyle = CellAltTextFontStyle
		  t.CellAltTextSize = CellAltTextSize
		  t.CellBorder = CellBorder
		  t.CellBorderColor = CellBorderColor
		  t.CellColor = CellColor
		  t.CellPadding = CellPadding
		  t.CellTextColor = CellTextColor
		  t.CellTextFont = CellTextFont
		  t.CellTextFontStyle = CellTextFontStyle
		  t.CellTextSize = CellTextSize
		  t.Width = Width
		  t.HeaderBorder = HeaderBorder
		  t.HeaderBorderColor = HeaderBorderColor
		  t.HeaderColor = HeaderColor
		  t.HeaderTextFont = HeaderTextFont
		  t.HeaderTextFontStyle = HeaderTextFontStyle
		  t.HeaderTextSize = HeaderTextSize
		  t.LineHeight = LineHeight
		  t.OverflowStyle = OverflowStyle
		  t.StartX = StartX
		  t.StartY = StartY
		  
		  t.Columns = Columns
		  t.Rows = Rows
		  t.cellStyles = cellStyles
		  
		  Return t
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(X as Integer = 0, Y as Integer = 0, TableWidth as Integer = 0, TableLineHeight as Integer = 20, PageSplit as Boolean = true)
		  mID = GraffitiControlWrapper.GenerateUUID()
		  StartX = X
		  StartY = Y
		  LineHeight = TableLineHeight
		  Width = TableWidth
		  AvoidPageSplit = not PageSplit
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		AvoidPageSplit As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltBorder As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltBorderColor As Color = &cFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltColor As Color = &cDCDCDC
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltTextColor As Color = &c000000
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltTextFont As String = "helvetica"
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltTextFontStyle As String = "normal"
	#tag EndProperty

	#tag Property, Flags = &h0
		CellAltTextSize As Double = 12
	#tag EndProperty

	#tag Property, Flags = &h0
		CellBorder As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		CellBorderColor As Color = &cEEEEEE
	#tag EndProperty

	#tag Property, Flags = &h0
		CellColor As Color = &cFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h0
		CellPadding As Integer = 3
	#tag EndProperty

	#tag Property, Flags = &h0
		cellStyles() As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		CellTextColor As Color = &c000000
	#tag EndProperty

	#tag Property, Flags = &h0
		CellTextFont As String = "helvetica"
	#tag EndProperty

	#tag Property, Flags = &h0
		CellTextFontStyle As String = "normal"
	#tag EndProperty

	#tag Property, Flags = &h0
		CellTextSize As Double = 12
	#tag EndProperty

	#tag Property, Flags = &h0
		Columns() As Pair
	#tag EndProperty

	#tag Property, Flags = &h0
		EndY As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderBorder As Double = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderBorderColor As Color = &c34495E
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderColor As Color = &c34495E
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderTextColor As Color = &cFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderTextFont As String = "helvetica"
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderTextFontStyle As String = "bold"
	#tag EndProperty

	#tag Property, Flags = &h0
		HeaderTextSize As Double = 14
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Return mID
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  '// Read-Only
			  dim e as new RuntimeException
			  e.Message = CurrentMethodName + " is read-only."
			  Raise e
			End Set
		#tag EndSetter
		ID As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		LineHeight As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		MarginBottom As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		MarginLeft As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		MarginRight As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h0
		MarginTop As Integer = -1
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		OverflowStyle As String = "ellipsize"
	#tag EndProperty

	#tag Property, Flags = &h0
		Rows() As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		StartX As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		StartY As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		Width As Integer = 0
	#tag EndProperty


	#tag Constant, Name = OverflowEllipsize, Type = String, Dynamic = False, Default = \"ellipsize", Scope = Public
	#tag EndConstant

	#tag Constant, Name = OverflowLineBreak, Type = String, Dynamic = False, Default = \"linebreak", Scope = Public
	#tag EndConstant

	#tag Constant, Name = OverflowNone, Type = String, Dynamic = False, Default = \"false", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AvoidPageSplit"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltBorder"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltBorderColor"
			Group="Behavior"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltColor"
			Group="Behavior"
			InitialValue="&cDCDCDC"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltTextColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltTextFont"
			Group="Behavior"
			InitialValue="helvetica"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltTextFontStyle"
			Group="Behavior"
			InitialValue="normal"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellAltTextSize"
			Group="Behavior"
			InitialValue="12"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellBorder"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellBorderColor"
			Group="Behavior"
			InitialValue="&cEEEEEE"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellColor"
			Group="Behavior"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellPadding"
			Group="Behavior"
			InitialValue="3"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellTextColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellTextFont"
			Group="Behavior"
			InitialValue="helvetica"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellTextFontStyle"
			Group="Behavior"
			InitialValue="normal"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CellTextSize"
			Group="Behavior"
			InitialValue="12"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EndY"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderBorder"
			Group="Behavior"
			InitialValue="0"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderBorderColor"
			Group="Behavior"
			InitialValue="&c34495E"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderColor"
			Group="Behavior"
			InitialValue="&c34495E"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderTextColor"
			Group="Behavior"
			InitialValue="&cFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderTextFont"
			Group="Behavior"
			InitialValue="helvetica"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderTextFontStyle"
			Group="Behavior"
			InitialValue="bold"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderTextSize"
			Group="Behavior"
			InitialValue="14"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LineHeight"
			Group="Behavior"
			InitialValue="20"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MarginBottom"
			Group="Behavior"
			InitialValue="40"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MarginLeft"
			Group="Behavior"
			InitialValue="20"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MarginRight"
			Group="Behavior"
			InitialValue="20"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MarginTop"
			Group="Behavior"
			InitialValue="50"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OverflowStyle"
			Group="Behavior"
			InitialValue="ellipsize"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StartX"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StartY"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
