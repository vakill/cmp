#tag Class
Protected Class GraffitiWebPDFCellStyle
	#tag Method, Flags = &h0
		Sub Constructor()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(TextFontName as String, TextFontSize as Integer, TextFontColor as Color)
		  Font = TextFontName
		  FontSize = TextFontSize
		  TextColor = TextFontColor
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		FillColor As Color = &cFFFFFF
	#tag EndProperty

	#tag Property, Flags = &h0
		Font As String = "Helvetica"
	#tag EndProperty

	#tag Property, Flags = &h0
		FontSize As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		FontStyle As FontStyles
	#tag EndProperty

	#tag Property, Flags = &h0
		HasFillColor As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		HorizatalAlign As Alignments
	#tag EndProperty

	#tag Property, Flags = &h0
		LineColor As Color
	#tag EndProperty

	#tag Property, Flags = &h0
		LineWidth As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		Overflow As OverflowStyles
	#tag EndProperty

	#tag Property, Flags = &h0
		Padding As Integer = 5
	#tag EndProperty

	#tag Property, Flags = &h0
		TextColor As Color = &c000000
	#tag EndProperty

	#tag Property, Flags = &h0
		VerticalAlign As Alignments
	#tag EndProperty


	#tag Enum, Name = Alignments, Type = Integer, Flags = &h0
		LeftTop
		  CenterMiddle
		RightBottom
	#tag EndEnum

	#tag Enum, Name = FontStyles, Type = Integer, Flags = &h0
		Normal
		  Bold
		  Italic
		BoldItalic
	#tag EndEnum

	#tag Enum, Name = OverflowStyles, Type = Integer, Flags = &h0
		Visible
		  Hidden
		  Ellipsize
		LineBreak
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
