#tag Class
Protected Class GraffitiWebPDF
Inherits GraffitiControlWrapper
	#tag Event
		Sub Close()
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub ContextualMenuAction(Item As WebMenuItem)
		  '// 
		End Sub
	#tag EndEvent

	#tag Event
		Function ExecuteEvent(Name as String, Parameters() as Variant) As Boolean
		  try
		    select case Name
		    case "LibrariesLoaded"
		      LibrariesLoaded = True
		    case "pdfOutput"
		      if Parameters.Ubound >= 0 then PDFOutput( Parameters(0) )
		    case "fontList"
		      AvailableFonts = new JSONItem( Parameters(0) )
		    case "HTMLAdded"
		      Buffer( "window.GSjQuery('#" + me.ControlID + "_hidden').css('display', 'none');" )
		    case "tableEndY"
		      if Parameters.Ubound >= 1 then
		        dim theTable as GraffitiWebPDFTable = GetTable( Parameters(0).StringValue )
		        if not IsNull( theTable ) then
		          theTable.EndY = Parameters(1).IntegerValue
		          TableUpdated( theTable )
		        end if
		      end if
		    end select
		  Catch
		    
		  End Try
		  
		  Return True
		End Function
	#tag EndEvent

	#tag Event
		Function FrameworkPropertyChanged(Name as String, Value as Variant) As Boolean
		  '// 
		End Function
	#tag EndEvent

	#tag Event
		Sub Hidden()
		  isShown = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub LoadComplete()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  LibrariesLoaded = True
		End Sub
	#tag EndEvent

	#tag Event
		Sub Resized()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub ScaleFactorChanged()
		  '//
		End Sub
	#tag EndEvent

	#tag Event
		Sub SetupCSS(ByRef Styles() as WebControlCSS)
		  
		End Sub
	#tag EndEvent

	#tag Event
		Function SetupHTML() As String
		  dim strOut() as string
		  strOut.Append( "<div id='" + me.ControlID + "_hidden' style='margin:auto;display:none;z-index:10000;background-color:#ccc;border-radius:20px;'>Rendering...<br />" )
		  strOut.Append( "<div id='" + me.ControlID + "_display' style='background-color:#fff;'></div>" )
		  strOut.Append( "</div>" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndEvent

	#tag Event
		Function SetupJavascriptFramework() As String
		  
		End Function
	#tag EndEvent

	#tag Event
		Sub Shown()
		  Shown()
		  
		  isShown = True
		  
		  RunBuffer()
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AddBezier(StartX as double, StartY as Double, X1 as Double, Y1 as Double, X2 as Double, Y2 as Double, X3 as Double, Y3 as Double)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".lines([[" )
		  strOut.Append( Str( X1 - StartX, "#" ) + "," )
		  strOut.Append( Str( Y1 - StartY, "#" ) + "," )
		  strOut.Append( Str( X2 - StartX, "#" ) + "," )
		  strOut.Append( Str( Y2 - StartY, "#" ) + "," )
		  strOut.Append( Str( X3 - StartX, "#" ) + "," )
		  strOut.Append( Str( Y3 - StartY, "#" ) + "]]," )
		  strOut.Append( Str( startX, "#" ) + "," + Str( startY, "#" ) + "," )
		  strOut.Append( "[1,1]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddCircle(X as Double, Y as Double, Radius as Double, hasFill as Boolean = False, hasStroke as Boolean = False)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".circle(" )
		  strOut.Append( Str( X, "#" ) + "," )
		  strOut.Append( Str( Y, "#" ) + "," )
		  strOut.Append( Str( Radius, "#" ) + "," )
		  strOut.Append( "'" + if( hasFill, "F", "" ) + if( hasStroke, "D", "" ) + "'" )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddEllipse(X as Double, Y as Double, RadiusX as Double, RadiusY as Double, hasFill as Boolean = False, hasStroke as Boolean = False)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".ellipse(" )
		  strOut.Append( Str( X, "#" ) + "," )
		  strOut.Append( Str( Y, "#" ) + "," )
		  strOut.Append( Str( RadiusX, "#" ) + "," )
		  strOut.Append( Str( RadiusY, "#" ) + "," )
		  strOut.Append( "'" + if( hasFill, "F", "" ) + if( hasStroke, "D", "" ) + "'" )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddHTML(HTMLContent as String, W as Integer = 1200)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "_hidden').css('display','block').css('width','" + Str( W, "#" ) + "px');" )
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "_display').html('" + EscapeString( HTMLContent ) + "');" )
		  strOut.Append( "var theNode = document.getElementById('" + me.ControlID + "_display');" )
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".addHTML(theNode, " )
		  strOut.Append( "{'margin': 1," )
		  strOut.Append( "'pagesplit': true}," )
		  strOut.Append( "function() {" )
		  strOut.Append( "window.GSjQuery('#" + me.ControlID + "_hidden').css('display','none');" )
		  strOut.Append( "});" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddImage(PictureData as String, X as Double, Y as Double, W as Double = - 1, H as Double = - 1)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".addImage(" )
		  strOut.Append( "'" + PictureData + "'," )
		  strOut.Append( Str( X, "#" ) + "," )
		  strOut.Append( Str( Y, "#" ) )
		  if W >= 0 and H >= 0 then
		    strOut.Append( ", " + Str( W, "#" ) + "," )
		    strOut.Append( Str( H, "#" ) )
		  end if
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddLine(X1 as Double, Y1 as Double, X2 as Double, Y2 as Double, hasFill as Boolean = False, hasStroke as Boolean = False)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".line(" )
		  strOut.Append( Str( X1, "#" ) + "," )
		  strOut.Append( Str( Y1, "#" ) + "," )
		  strOut.Append( Str( X2, "#" ) + "," )
		  strOut.Append( Str( Y2, "#" ) + "," )
		  strOut.Append( "'" + if( hasFill, "F", "" ) + if( hasStroke, "D", "" ) + "'" )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddPage(W as Double = 210, H as Double = 297)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".addPage(" )
		  strOut.Append( Str( W, "#" ) + "," )
		  strOut.Append( Str( H, "#" ) )
		  strOut.Append( ");" )
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pageCount', [window.gwPDF_" + me.ControlID + ".internal.getNumberOfPages()]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddRect(X as Double, Y as Double, W as Double, H as Double, hasFill as Boolean = False, hasStroke as Boolean = False)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".rect(" )
		  strOut.Append( Str( X, "#" ) + "," )
		  strOut.Append( Str( Y, "#" ) + "," )
		  strOut.Append( Str( W, "#" ) + "," )
		  strOut.Append( Str( H, "#" ) + "," )
		  strOut.Append( "'" + if( hasFill, "F", "" ) + if( hasStroke, "D", "" ) + "'" )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddRoundedRect(X as Double, Y as Double, W as Double, H as Double, RadiusX as Double, RadiusY as Double, hasFill as Boolean = False, hasStroke as Boolean = False)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".roundedRect(" )
		  strOut.Append( Str( X, "#" ) + "," )
		  strOut.Append( Str( Y, "#" ) + "," )
		  strOut.Append( Str( W, "#" ) + "," )
		  strOut.Append( Str( H, "#" ) + "," )
		  strOut.Append( Str( RadiusX, "#" ) + "," )
		  strOut.Append( Str( RadiusY, "#" ) + "," )
		  strOut.Append( "'" + if( hasFill, "F", "" ) + if( hasStroke, "D", "" ) + "'" )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddTable(Data as GraffitiWebPDFTable)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "var doc = window.gwPDF_" + me.ControlID + ";" )
		  
		  dim UUIDs() as String
		  for intCycle as Integer = 0 to data.Columns.Ubound
		    UUIDs.Append( GenerateUUID )
		  next
		  
		  Tables.Append( Data )
		  
		  dim strOpts as String = "{" + _
		  "styles: {" + _
		  "overflow: 'ellipsize'" + _
		  "}," + _
		  "startY: " + Str( Data.StartY, "#" ) + "," + _
		  "startX: " + Str( Data.StartX, "#" ) + ","
		  if Data.Width > 0 then
		    strOpts = strOpts + "tableWidth: " + Str( Data.Width ) + ","
		  else
		    strOpts = strOpts + "tableWidth: 'auto',"
		  end if
		  strOpts = strOpts + "lineHeight: " + Str( Data.LineHeight, "#" ) + "," + _
		  "avoidPageSplit: " + if( Data.AvoidPageSplit, "true", "false" ) + "," + _
		  "margins: {"
		  if Data.MarginRight >= 0 then strOpts = strOpts + "right: " + Str( Data.MarginRight, "#" ) + ","
		  strOpts = strOpts + "left: " + Str( Data.MarginLeft + Data.StartX, "#" ) + ","
		  if Data.MarginTop >= 0 then strOpts = strOpts + "top: " + Str( Data.MarginTop, "#" ) + ","
		  if Data.MarginBottom >= 0 then strOpts = strOpts + "bottom: " + Str( Data.MarginBottom, "#" )
		  strOpts = strOpts + "}," + _
		  "overflow: '" + Data.OverflowStyle + "'," + _
		  "padding: " + Str( Data.CellPadding, "#" ) + "," + _
		  "renderHeaderCell: function (x, y, width, height, key, value, settings) {" + _
		  "doc.setFillColor(" + Str( Data.HeaderColor.Red, "#" ) + "," + Str( Data.HeaderColor.Green, "#" ) + "," + Str( Data.HeaderColor.Blue, "#" ) + ");" + _
		  "doc.setDrawColor(" + Str( Data.HeaderBorderColor.Red, "#" ) + "," + Str( Data.HeaderBorderColor.Green, "#" ) + "," + Str( Data.HeaderBorderColor.Blue, "#" ) + ");" + _
		  "doc.setTextColor(" + Str( Data.HeaderTextColor.Red, "#" ) + "," + Str( Data.HeaderTextColor.Green, "#" ) + "," + Str( Data.HeaderTextColor.Blue, "#" ) + ");" + _
		  "doc.setFont('" + Data.HeaderTextFont + "');" + _
		  "doc.setFontStyle('" + Data.HeaderTextFontStyle + "');" + _
		  "doc.setFontSize('" + Str( Data.HeaderTextSize, "#.####" ) + "');" + _
		  "doc.rect(x, y, width, height, 'F" + if( data.HeaderBorder > 0, "D", "" ) + "');" + _
		  "y += settings.lineHeight / 2 + doc.internal.getLineHeight() / 2 - 2.5;" + _
		  "doc.text('' + value, x + settings.padding, y);" + _
		  "}," + _
		  "renderCell: function (x, y, width, height, key, value, row, settings) {" + _
		  "if(row % 2 === 0) {" + _
		  "doc.setFillColor(" + Str( Data.CellColor.Red, "#" ) + "," + Str( Data.CellColor.Green, "#" ) + "," + Str( Data.CellColor.Blue, "#" ) + ");" + _
		  "doc.setDrawColor(" + Str( Data.CellBorderColor.Red, "#" ) + "," + Str( Data.CellBorderColor.Green, "#" ) + "," + Str( Data.CellBorderColor.Blue, "#" ) + ");" + _
		  "doc.setTextColor(" + Str( Data.CellTextColor.Red, "#" ) + "," + Str( Data.CellTextColor.Green, "#" ) + "," + Str( Data.CellTextColor.Blue, "#" ) + ");" + _
		  "doc.setFont('" + Data.CellTextFont + "');" + _
		  "doc.setFontStyle('" + Data.CellTextFontStyle + "');" + _
		  "doc.setFontSize('" + Str( Data.CellTextSize, "#.####" ) + "');" + _
		  "} else {" + _
		  "doc.setFillColor(" + Str( Data.CellAltColor.Red, "#" ) + "," + Str( Data.CellAltColor.Green, "#" ) + "," + Str( Data.CellAltColor.Blue, "#" ) + ");" + _
		  "doc.setDrawColor(" + Str( Data.CellAltBorderColor.Red, "#" ) + "," + Str( Data.CellAltBorderColor.Green, "#" ) + "," + Str( Data.CellAltBorderColor.Blue, "#" ) + ");" + _
		  "doc.setTextColor(" + Str( Data.CellAltTextColor.Red, "#" ) + "," + Str( Data.CellAltTextColor.Green, "#" ) + "," + Str( Data.CellAltTextColor.Blue, "#" ) + ");" + _
		  "doc.setFont('" + Data.CellAltTextFont + "');" + _
		  "doc.setFontStyle('" + Data.CellAltTextFontStyle + "');" + _
		  "doc.setFontSize('" + Str( Data.CellAltTextSize, "#.####" ) + "');" + _
		  "}" + _
		  "doc.rect(x, y, width, height, 'F" + if( data.CellBorder > 0, "D", "" ) + "');" + _
		  "y += settings.lineHeight / 2 + doc.internal.getLineHeight() / 2 - 2.5;" + _
		  "doc.text('' + value, x + settings.padding, y);" + _
		  "}," + _
		  ColumnStylesToString( Data, UUIDs ) + If( Data.cellStyles.Ubound >= 0, ",", "" ) + _
		  TableCellStyler( Data ) + _
		  "}"
		  
		  dim strColumns as String = ColumnsToString( Data, UUIDs )
		  dim strData as String = DataToString( Data, UUIDs )
		  
		  strOut.Append( strColumns )
		  strOut.Append( strData )
		  strOut.Append( "doc.autoTable(newColumns, newData, " + strOpts + ");" )
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "','tableEndY',['" + Data.ID + "',doc.autoTableEndPosY()]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddText(theContent as String, X as Integer, Y as Integer, BlockWidth as Double = -1)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "var theText = '" + EscapeString( theContent ) + "';" )
		  strOut.Append( "var splitText = window.gwPDF_" + me.ControlID + ".splitTextToSize(theText, " )
		  if BlockWidth < 0 then
		    strOut.Append( "window.gwPDF_" + me.ControlID + ".internal.pageSize.width - " + Str( X, "#.#######" ) )
		  else
		    strOut.Append( Str( BlockWidth, "#.#######" ) )
		  end if
		  strOut.Append( ");" )
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".text(" )
		  strOut.Append( "splitText," )
		  strOut.Append( Str( X, "#" ) + "," )
		  strOut.Append( Str( Y, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub AddTriangle(X1 as Double, Y1 as Double, X2 as Double, Y2 as Double, X3 as Double, Y3 as Double, hasFill as Boolean = False, hasStroke as Boolean = False)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".triangle(" )
		  strOut.Append( Str( X1, "#" ) + "," )
		  strOut.Append( Str( Y1, "#" ) + "," )
		  strOut.Append( Str( X2, "#" ) + "," )
		  strOut.Append( Str( Y2, "#" ) + "," )
		  strOut.Append( Str( X3, "#" ) + "," )
		  strOut.Append( Str( Y3, "#" ) + "," )
		  strOut.Append( "'" + if( hasFill, "F", "" ) + if( hasStroke, "D", "" ) + "'" )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColorToPDFColor(c as Color) As String
		  if c.Alpha = 255 then return "false"
		  
		  Return "[" + Str( c.Red ) + ", " + Str( c.Green ) + ", " + str( c.Blue ) + "]"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColumnsToString(theData as GraffitiWebPDFTable, UUIDs() as String) As String
		  dim strOut() as String
		  strOut.Append( "var newColumns = [" )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( theData.Columns )
		  
		  for intCycle = 0 to intMax
		    dim currPair as Pair = theData.Columns(intCycle)
		    strOut.Append( "{key: '" + Lowercase( EscapeString( currPair.Left ) ) + "'," )
		    strOut.Append( "title: '" + EscapeString( currPair.Left ) + "'," )
		    strOut.Append( "dataKey: '" + UUIDs( intCycle ) + "'" )
		    if currPair.Right >= 0 then strOut.Append( ", width: " + currPair.Right )
		    strOut.Append( "}" )
		    if intCycle < intMax then strOut.Append( "," )
		  next
		  
		  strOut.Append( "];" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function ColumnStylesToString(theData as GraffitiWebPDFTable, UUIDs() as String) As String
		  dim strOut() as String
		  strOut.Append( "columnStyles: {" )
		  
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( theData.Columns )
		  
		  for intCycle = 0 to intMax
		    dim currPair as Pair = theData.Columns(intCycle)
		    if currPair.Right >= 0 then
		      if strOut.Ubound > 0 then strOut.Append( "," )
		      strOut.Append( "'" + UUIDs(intCycle ) + "':{columnWidth: " + currPair.Right + "}" )
		    end
		  next
		  
		  strOut.Append( "}" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  // Note that this may need modifications if there are multiple constructor choices.
		  // Possible constructor calls:
		  // Constructor() -- From WebControl
		  // Constructor() -- From WebObject
		  Super.Constructor
		  
		  LibrariesLoaded = True
		  hasLoaded = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function DataToString(theData as GraffitiWebPDFTable, UUIDs() as String) As String
		  dim strOut() as String
		  strOut.Append( "var newData = [" )
		  dim intCycle as Integer
		  dim intMax as Integer = UBound( theData.Rows )
		  for intCycle = 0 to intMax
		    dim currRow as Dictionary = theData.Rows(intCycle)
		    
		    if IsNull( currRow ) then Continue
		    
		    strOut.Append( "{" )
		    
		    dim currCycle as Integer
		    dim currMax as Integer = currRow.Values.Ubound
		    for currCycle = 0 to currMax
		      'dim currCol as String = Lowercase( EscapeString( theData.Columns( currCycle ).Left ) )
		      dim currCol as String = UUIDs(currCycle)
		      dim currKey as String = currRow.Key( currCycle )
		      strOut.Append( "'" + currCol + "':'" + EscapeString( currRow.Value( currRow.Key( currCycle ) ).StringValue ) + "'" )
		      if currCycle < currMax Then strOut.Append( "," )
		    next
		    strOut.Append( "}" )
		    if intCycle < intMax then strOut.Append( "," )
		  next
		  
		  strOut.Append( "];" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Download(Filename as String)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "var theDoc = window.gwPDF_" + me.ControlID + ";" )
		  strOut.Append( "if (typeof(theDoc) !== 'undefined') {" )
		  strOut.Append( "theDoc.save('" + EscapeString( Filename ) + "');" )
		  'strOut.Append( "var docOut = window.gwPDF_" + me.ControlID + ".output('dataurlstring');" )
		  'strOut.Append( "var link = document.createElement('a');" )
		  'strOut.Append( "link.download = '" + EscapeString( Filename ) + "';" )
		  'strOut.Append( "link.id = '" + me.ControlID + "_downloadlink';" )
		  'strOut.Append( "link.href = docOut;" )
		  'strOut.Append( "var myContainer = window.GSjQuery('#" + me.ControlID + "');" )
		  'strOut.Append( "window.GSjQuery(link).appendTo(myContainer);" )
		  'strOut.Append( "setTimeout( function() {" )
		  'strOut.Append( "var link = window.GSjQuery('#" + me.ControlID + "_downloadlink');" )
		  'strOut.Append( "link.click();" )
		  'strOut.Append( "link.detach();" )
		  'strOut.Append( "}, 1000);" )
		  strOut.Append( "}" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FromHTML(theHTML as String,DocumentOrientation as String = "portrait", DocumentUnits as String = "pt", DocumentFormat as String = "letter", Margin as Double = 0)
		  dim strOut() as String
		  
		  strOut.Append( "var theOpts = {" )
		  strOut.Append( "margin: " + Str( Margin, "#0.00" ) + "," )
		  strOut.Append( "image: {type: 'jpeg', quality: 1.0}," )
		  strOut.Append( "jsPDF: {" )
		  strOut.Append( "orientation: '" + DocumentOrientation + "'," )
		  strOut.Append( "unit: '" + DocumentUnits + "'," )
		  strOut.Append( "format: '" + DocumentFormat + "'" )
		  strOut.Append( "}," )
		  strOut.Append( "onComplete: function(pdf) {" )
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pdfOutput', [pdf.output('datauristring')]);" )
		  strOut.Append( "}" )
		  strOut.Append( "};" )
		  
		  strOut.Append( "html2pdf('" + EscapeString( theHTML ) + "', theOpts);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetTable(tableID as String) As GraffitiWebPDFTable
		  for each theTable as GraffitiWebPDFTable in Tables
		    if theTable.ID = tableID then Return theTable
		  next
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Shared Function HTMLHeader(CurrentSession as WebSession) As String
		  dim strOut() as String
		  
		  strOut.Append( "<!-- BEGIN GraffitiWebPDF -->" )
		  
		  dim fScripts as FolderItem = app.ExecutableFile.Parent.Child( "scripts" )
		  if not IsNull( fScripts ) then
		    strOut.Append( AddjQuery( CurrentSession ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "pdf" ).Child( "jspdf.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "pdf" ).Child( "jspdf.plugin.autotable.min.js" ), JavascriptNamespace ) )
		    'strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "pdf" ).Child( "jspdfh2c.min.js" ), JavascriptNamespace ) )
		    strOut.Append( AddJSFile( CurrentSession, fScripts.Child( "pdf" ).Child( "html2pdf.js" ), JavascriptNamespace ) )
		  end if
		  
		  strOut.Append( "<!-- END GraffitiWebPDF -->" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function LoadInViewer(theViewer as WebHTMLViewer, theContent as String) As WebFile
		  dim strOut() as String
		  
		  dim j as WebFile
		  Dim theURL as String = theContent
		  if Session.Browser = WebSession.BrowserType.InternetExplorer then
		    dim f as FolderItem = SpecialFolder.Temporary.Child( "report.pdf" )
		    dim t as TextOutputStream = TextOutputStream.Create( f )
		    t.Write( DecodeBase64( NthField( theContent, ",", 2 ) ) )
		    t.Close
		    
		    j = WebFile.Open( f )
		    theURL = j.URL
		    
		    strOut.Append( "var theViewer = window.GSjQuery('#" + theViewer.ControlID + "');" )
		    strOut.Append( "var theObject = window.GSjQuery('#" + theViewer.ControlID + " object');" )
		    strOut.Append( "var theFrame = window.GSjQuery('#" + theViewer.ControlID + " iframe');" )
		    
		    strOut.Append( "if(typeof theObject != 'undefined' || theObject.length > 0) {" )
		    strOut.Append( "theFrame.detach().appendTo(theViewer);" )
		    strOut.Append( "theObject.detach();" )
		    strOut.Append( "}" )
		    
		    strOut.Append( "theObject = window.GSjQuery('<object></object>');" )
		    strOut.Append( "theObject.attr('data', '" + j.URL + "')" + _
		    ".attr('type', 'application/pdf')" + _
		    ".css('width','100%').css('height','100%');" )
		    strOut.Append( "theObject.appendTo(theViewer);" )
		    strOut.Append( "theFrame.detach().appendTo(theObject);" )
		    if Session.RenderingEngine <> WebSession.EngineType.EdgeHTML then
		      'strOut.Append( "theFrame.attr('src', '" + theURL + "');" )
		    end if
		  else
		    strOut.Append( "window.GSjQuery('#" + theViewer.ControlID + " iframe').attr('src', '" + theURL + "');" )
		  end if
		  
		  Buffer( strOut )
		  
		  if not IsNull( j ) then Return j
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewDocument(DocumentOrientation as String = "portrait", DocumentUnits as String = "mm", DocumentWidth as Integer = 210, DocumentHeight as Integer = 297)
		  dim strOut() as String
		  
		  ReDim Tables(-1)
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + " = new jsPDF(" )
		  strOut.Append( "'" + DocumentOrientation + "'," )
		  strOut.Append( "'" + DocumentUnits + "'," )
		  strOut.Append( "[" + Str( DocumentWidth ) + "," + Str( DocumentHeight ) + "]" )
		  strOut.Append( ");" )
		  
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'fontList', [JSON.stringify(window.gwPDF_" + me.ControlID + ".getFontList())]);" )
		  
		  Buffer( strOut )
		  
		  documentCreated = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub NewDocument(DocumentOrientation as String = "portrait", DocumentUnits as String = "mm", DocumentFormat as String = "A4")
		  dim strOut() as String
		  
		  ReDim Tables(-1)
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + " = new jsPDF(" )
		  strOut.Append( "'" + DocumentOrientation + "'," )
		  strOut.Append( "'" + DocumentUnits + "'," )
		  strOut.Append( "'" + DocumentFormat + "'" )
		  strOut.Append( ");" )
		  
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'fontList', [JSON.stringify(window.gwPDF_" + me.ControlID + ".getFontList())]);" )
		  
		  Buffer( strOut )
		  
		  documentCreated = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Output()
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "var theDoc = window.gwPDF_" + me.ControlID + ";" )
		  strOut.Append( "if (typeof(theDoc) !== 'undefined') {" )
		  strOut.Append( "var docOut = theDoc.output('dataurlstring');" )
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pdfOutput', [docOut]);" )
		  strOut.Append( "}" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Save()
		  if not documentCreated then return
		  
		  dim strOut() as String
		  strOut.Append( "var theDoc = window.gwPDF_" + me.ControlID + ";" )
		  strOut.Append( "if (typeof(theDoc) !== 'undefined') {" )
		  strOut.Append( "theDoc.output('dataurlnewwindow');" )
		  strOut.Append( "}" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFillColor(theColor as Color)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFillColor(" )
		  strOut.Append( Str( theColor.Red, "#" ) + "," )
		  strOut.Append( Str( theColor.Green, "#" ) + "," )
		  strOut.Append( Str( theColor.Blue, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFillColor(Gray as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFillColor(" )
		  strOut.Append( Str( Gray, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFillColor(Red as Integer, Green as Integer, Blue as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFillColor(" )
		  strOut.Append( Str( Red, "#" ) + "," )
		  strOut.Append( Str( Green, "#" ) + "," )
		  strOut.Append( Str( Blue, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFillColor(Cyan as Integer, Magenta as Integer, Yellow as Integer, Key as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFillColor(" )
		  strOut.Append( Str( Cyan, "#" ) + "," )
		  strOut.Append( Str( Magenta, "#" ) + "," )
		  strOut.Append( Str( Yellow, "#" ) + "," )
		  strOut.Append( Str( Key, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFont(Face as String, Style as String)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFont(" )
		  strOut.Append( "'" + Face + "'," )
		  strOut.Append( "'" + Style + "');" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFontSize(newSize as Double)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFontSize(" + Str( newSize, "#.0#######" ) + ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetFontStyle(Style as String)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setFontStyle('" + Style + "');" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetLineCap(Style as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setLineCap(" + Str( Style, "#" ) + ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetLineJoin(Style as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setLineJoin(" + Str( Style, "#" ) + ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetLineWidth(W as Double)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setLineWidth(" + Str( W, "#.##########" ) + ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetPage(PageNumber as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setPage(" + Str( PageNumber, "#" ) + ");" )
		  strOut.Append( "Xojo.triggerServerEvent('" + me.ControlID + "', 'pageCount', [window.gwPDF_" + me.ControlID + ".internal.getNumberOfPages()]);" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetProperties(Title as String, Subject as String = "", Author as String = "", Keywords as String = "", Creator as String = "GraffitiWebPDF")
		  if not documentCreated then return
		  
		  dim strArray as String = "{title:'" + EscapeString( Title ) + "'," + _
		  "subject:'" + EscapeString( Subject ) + "'," + _
		  "author:'" + EscapeString( Author ) + "'," + _
		  "keywords:'" + EscapeString( Keywords ) + "'," + _
		  "creator:'" + EscapeString( Creator ) + "'}"
		  
		  dim strOut() as String
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setProperties(" + strArray + ");" )
		  
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetStrokeColor(theColor as Color)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setDrawColor(" )
		  strOut.Append( Str( theColor.Red, "#" ) + "," )
		  strOut.Append( Str( theColor.Green, "#" ) + "," )
		  strOut.Append( Str( theColor.Blue, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetStrokeColor(Gray as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setDrawColor(" )
		  strOut.Append( Str( Gray, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetStrokeColor(Red as Integer, Green as Integer, Blue as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setDrawColor(" )
		  strOut.Append( Str( Red, "#" ) + "," )
		  strOut.Append( Str( Green, "#" ) + "," )
		  strOut.Append( Str( Blue, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetStrokeColor(Cyan as Integer, Magenta as Integer, Yellow as Integer, Key as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setDrawColor(" )
		  strOut.Append( Str( Cyan, "#" ) + "," )
		  strOut.Append( Str( Magenta, "#" ) + "," )
		  strOut.Append( Str( Yellow, "#" ) + "," )
		  strOut.Append( Str( Key, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetTextColor(theColor as Color)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setTextColor(" )
		  strOut.Append( Str( theColor.Red, "#" ) + "," )
		  strOut.Append( Str( theColor.Green, "#" ) + "," )
		  strOut.Append( Str( theColor.Blue, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetTextColor(Red as Integer, Green as Integer, Blue as Integer)
		  if not documentCreated then return
		  
		  dim strOut() as String
		  
		  strOut.Append( "window.gwPDF_" + me.ControlID + ".setTextColor(" )
		  strOut.Append( Str( Red, "#" ) + "," )
		  strOut.Append( Str( Green, "#" ) + "," )
		  strOut.Append( Str( Blue, "#" ) )
		  strOut.Append( ");" )
		  
		  Buffer( strOut )
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function TableCellStyler(theTable as GraffitiWebPDFTable) As String
		  dim intMax as Integer = theTable.cellStyles.Ubound
		  
		  if intMax < 0 then Return ""
		  
		  dim strOut() as String
		  
		  strOut.Append( "createdCell: function(cell, opts) {" )
		  
		  dim currStyle as Dictionary
		  dim theStyle as GraffitiWebPDFCellStyle
		  
		  dim intCycle as Integer
		  for intCycle = 0 to intMax
		    currStyle = theTable.cellStyles(intCycle)
		    
		    dim strStyle as String = ReplaceAll( jsTableCellStyle, "#ROW#", Str( currStyle.Value( "row" ).IntegerValue ) )
		    strStyle = ReplaceAll( strStyle, "#COL#", Str( currStyle.Value( "col" ).IntegerValue ) )
		    
		    theStyle = currStyle.Value( "style" )
		    strStyle = ReplaceAll( strStyle, "#PADDING#", Str( theStyle.Padding ) )
		    strStyle = ReplaceAll( strStyle, "#FONTSIZE#", Str( theStyle.FontSize ) )
		    strStyle = ReplaceAll( strStyle, "#FONT#", theStyle.Font )
		    strStyle = ReplaceAll( strStyle, "#LINECOLOR#", ColorToPDFColor( theStyle.LineColor ) )
		    strStyle = ReplaceAll( strStyle, "#LINEWIDTH#", Str( theStyle.LineWidth ) )
		    
		    dim strFontStyle as String = "normal"
		    select case theStyle.FontStyle
		    case GraffitiWebPDFCellStyle.FontStyles.Bold
		      strFontStyle = "bold"
		    case GraffitiWebPDFCellStyle.FontStyles.Italic
		      strFontStyle = "italic"
		    case GraffitiWebPDFCellStyle.FontStyles.BoldItalic
		      strFontStyle = "bolditalic"
		    end select
		    strStyle = ReplaceAll( strStyle, "#FONTSTYLE#", strFontStyle )
		    
		    dim strOverflow as String = "ellipsize"
		    select case theStyle.Overflow
		    case GraffitiWebPDFCellStyle.OverflowStyles.Hidden
		      strOverflow = "hidden"
		    case GraffitiWebPDFCellStyle.OverflowStyles.LineBreak
		      strOverflow = "linebreak"
		    case GraffitiWebPDFCellStyle.OverflowStyles.Visible
		      strOverflow = "visible"
		    end select
		    strStyle = ReplaceAll( strStyle, "#OVERFLOW#", strOverflow )
		    
		    strStyle = ReplaceAll( strStyle, "#FILLCOLOR#", ColorToPDFColor( theStyle.FillColor ) )
		    strStyle = ReplaceAll( strStyle, "#TEXTCOLOR#", ColorToPDFColor( theStyle.TextColor ) )
		    
		    dim strHalign as String = "left"
		    select case theStyle.HorizatalAlign
		    case GraffitiWebPDFCellStyle.Alignments.CenterMiddle
		      strOverflow = "center"
		    case GraffitiWebPDFCellStyle.Alignments.RightBottom
		      strOverflow = "right"
		    end select
		    strStyle = ReplaceAll( strStyle, "#HALIGN#", strHAlign )
		    
		    dim strValign as String = "middle"
		    select case theStyle.VerticalAlign
		    case GraffitiWebPDFCellStyle.Alignments.LeftTop
		      strOverflow = "top"
		    case GraffitiWebPDFCellStyle.Alignments.RightBottom
		      strOverflow = "bottom"
		    end select
		    strStyle = ReplaceAll( strStyle, "#VALIGN#", strVAlign )
		    
		    strOut.Append( strStyle )
		  next
		  
		  strOut.Append( "}" )
		  
		  Return Join( strOut, "" )
		End Function
	#tag EndMethod


	#tag Hook, Flags = &h0
		Event PDFOutput(theContent as String)
	#tag EndHook

	#tag Hook, Flags = &h0
		Event Shown()
	#tag EndHook

	#tag Hook, Flags = &h0
		Event TableUpdated(theTable as GraffitiWebPDFTable)
	#tag EndHook


	#tag Note, Name = Format Sizes
		
		GraffitiWebPDF's format sizes, by name, in pt:
		
		Constant                    Width                    Height
		FormatA0                    2383.94                  3370.39
		FormatA1                    1683.78                  2383.94
		FormatA2                    1190.55                  1683.78
		FormatA3                    841.89                   1190.55
		FormatA4                    595.28                   841.89
		FormatA5                    419.53                   595.28
		FormatA6                    297.64                   419.53
		FormatA7                    209.76                   297.64
		FormatA8                    147.40                   209.76
		FormatA9                    104.88                   147.40
		FormatA10                   73.70                    104.88
		FormatB0                    2834.65                  4008.19
		FormatB1                    2004.09                  2834.65
		FormatB2                    1417.32                  2004.09
		FormatB3                    1000.63                  1417.32
		FormatB4                    708.66                   1000.63
		FormatB5                    498.90                   708.66
		FormatB6                    354.33                   498.90
		FormatB7                    249.45                   354.33
		FormatB8                    175.75                   249.45
		FormatB9                    124.72                   175.75
		FormatB10                   87.87                    124.72
		FormatC0                    2599.37                  3676.54
		FormatC1                    1836.85                  2599.37
		FormatC2                    1298.27                  1836.85
		FormatC3                    918.43                   1298.27
		FormatC4                    649.13                   918.43
		FormatC5                    459.21                   649.13
		FormatC6                    323.15                   459.21
		FormatC7                    229.61                   323.15
		FormatC8                    161.57                   229.61
		FormatC9                    113.39                   161.57
		FormatC10                   79.37                    113.39
		Formatdl                    311.81                   623.62
		Formatletter                612                      792
		Formatgovernment-letter     576                      756
		Formatlegal                 612                      1008
		Formatjunior-legal          576                      360
		Formatledger                1224                     792
		Formattabloid               792                      1224
		Formatcredit-card           153                      243
	#tag EndNote

	#tag Note, Name = Sources
		This license applies to the JavaScript library(ies), Stylesheets, or images that are the property
		of the originating author, and does not apply to the Xojo wrapper created by GraffitiSuite Solutions.
		These resources are a separate entity from the Xojo code, and are licensed differently.
		To view the Source Code EULA for the Xojo code provided by GraffitiSuite Solutions, please visit
		https://graffitisuite.com/legal/source-eula/
		
		=====================
		
		https://mrrio.github.io/jsPDF/
		
		Released under the MIT License http://opensource.org/licenses/MIT
		
		Copyright (c) 2010-2012 James Hall, https://github.com/MrRio/jsPDF
		Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
		The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	#tag EndNote


	#tag Property, Flags = &h0
		AvailableFonts As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private documentCreated As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		Private isShown As Boolean = False
	#tag EndProperty

	#tag ComputedProperty, Flags = &h21
		#tag Getter
			Get
			  return mLibrariesLoaded
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mLibrariesLoaded = value
			  
			  RunBuffer
			End Set
		#tag EndSetter
		Private LibrariesLoaded As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mLibrariesLoaded As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Tables() As GraffitiWebPDFTable
	#tag EndProperty


	#tag Constant, Name = FormatA0, Type = String, Dynamic = False, Default = \"a0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA1, Type = String, Dynamic = False, Default = \"a1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA10, Type = String, Dynamic = False, Default = \"a10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA2, Type = String, Dynamic = False, Default = \"a2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA3, Type = String, Dynamic = False, Default = \"a3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA4, Type = String, Dynamic = False, Default = \"a4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA5, Type = String, Dynamic = False, Default = \"a5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA6, Type = String, Dynamic = False, Default = \"a6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA7, Type = String, Dynamic = False, Default = \"a7", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA8, Type = String, Dynamic = False, Default = \"a8", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatA9, Type = String, Dynamic = False, Default = \"a9", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB0, Type = String, Dynamic = False, Default = \"b0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB1, Type = String, Dynamic = False, Default = \"b1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB10, Type = String, Dynamic = False, Default = \"b10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB2, Type = String, Dynamic = False, Default = \"b2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB3, Type = String, Dynamic = False, Default = \"b3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB4, Type = String, Dynamic = False, Default = \"b4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB5, Type = String, Dynamic = False, Default = \"b5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB6, Type = String, Dynamic = False, Default = \"b6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB7, Type = String, Dynamic = False, Default = \"b7", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB8, Type = String, Dynamic = False, Default = \"b8", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatB9, Type = String, Dynamic = False, Default = \"b9", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC0, Type = String, Dynamic = False, Default = \"c0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC1, Type = String, Dynamic = False, Default = \"c1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC10, Type = String, Dynamic = False, Default = \"c10", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC2, Type = String, Dynamic = False, Default = \"c2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC3, Type = String, Dynamic = False, Default = \"c3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC4, Type = String, Dynamic = False, Default = \"c4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC5, Type = String, Dynamic = False, Default = \"c5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC6, Type = String, Dynamic = False, Default = \"c6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC7, Type = String, Dynamic = False, Default = \"c7", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC8, Type = String, Dynamic = False, Default = \"c8", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatC9, Type = String, Dynamic = False, Default = \"c9", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatCreditCard, Type = String, Dynamic = False, Default = \"credit-card", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatDL, Type = String, Dynamic = False, Default = \"dl", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatGovernmentLetter, Type = String, Dynamic = False, Default = \"government-letter", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatJuniorLegal, Type = String, Dynamic = False, Default = \"junior-legal", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatLedger, Type = String, Dynamic = False, Default = \"ledger", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatLegal, Type = String, Dynamic = False, Default = \"legal", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatLetter, Type = String, Dynamic = False, Default = \"letter", Scope = Public
	#tag EndConstant

	#tag Constant, Name = FormatTabloid, Type = String, Dynamic = False, Default = \"tabloid", Scope = Public
	#tag EndConstant

	#tag Constant, Name = JavascriptNamespace, Type = String, Dynamic = False, Default = \"graffitisuite.pdf", Scope = Private
	#tag EndConstant

	#tag Constant, Name = jsTableCellStyle, Type = String, Dynamic = False, Default = \"if( opts.row.index \x3D\x3D #ROW# && opts.column.index \x3D\x3D #COL#) {\r    cell.styles.cellPadding \x3D #PADDING#;\r    cell.styles.fontSize \x3D #FONTSIZE#;\r    cell.styles.font \x3D \'#FONT#\';\r    cell.styles.lineColor \x3D #LINECOLOR#;\r    cell.styles.lineWidth \x3D #LINEWIDTH#;\r    cell.styles.fontStyle \x3D \'#FONTSTYLE#\';\r    cell.styles.overflow \x3D \'#OVERFLOW#\';\r    cell.styles.fillColor \x3D #FILLCOLOR#;\r    cell.styles.textColor \x3D #TEXTCOLOR#;\r    cell.styles.halign \x3D \'#HALIGN#\';\r    cell.styles.valign \x3D \'#VALIGN#\';\r}", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LayoutEditorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAABHVBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHJrzx8KAAAAXnRSTlMAAQIDBAcICQoLDhESExQVFxgZGhwdHyAiIyYnKCsxMzQ2OzxAQUVGSUpQUlRdX2FicHV8f4KDiYuPkZKXmJuipqivsrS1t7m8vsDDx8jOz9HT19ne4uTm8fX3+fv9Ml3t7QAAAmpJREFUeNrt2mtbUkEQB/BZLno4ne6pXczQNDPTtKsRpGEXKcsriKnz/T+Gpgi04uxBzuzwYv4v2QPzg+fsnmd2AYiTdC5MMFkD3SSVLxxgwtmYi2LXn6ghS5YGY5XPrCBX6vdj1A+2kTFP3d9/E1nj/A3KvPWx7rgP8sidD2R9U2UHIDkbh/nr4xwFWPQA2KAAqx4ASK3Kuz4AWQLgoz6GClCAAhSgAAVAeKlHAQpQQPPVBau/j0Zmvre/a8wav/FgttI2vHxxh2CsO8Bkh0XizhrdXQz9ajXiF0ej3gFg3tHtjfnEDAB47+ivlrkB5icNSO8xA+Ceo8Oc5AZAhQaYQ27AC0eP/Zkb8NABeJU4IDpNs6O9aQHM2Xjz8mkLkIsaGb8qwLoqsgCNz7LuwhageMWlWAEK6COAYxq6AL1PQ8dC5AKwL8XeARVhwF2UBZg1YcBbFAWYNygKuPUDJQBnbcW1kedfO546uAADPTcmjmMPX8+C/gUMLJ1GDmDnpTTgtTRgRxgQoTCgJAx4grKA239lAeOHKAhIPfqGmBzgd/E8nQHl4v/58ueofbjaHKjpRqUCFKAABShAAQpQQHeAug9AjgCs+wCkCUDJQ/0DIABTHgAFCjDoAZCnAPCRvX4tRQKCI27ABJCAxrEbX1bAAWjssXBlO+MEsAo2A3ADYJrtPihnIA4AAp65UM1bu5uXAk7Wg6nSeqLPhd3VxeHWP6rdAOYoQAEKUIAC+gvwOGQKAch66cAIgJEGwJY0YF4acF0aAAVpQLAvDIBRaQA8kwbA6L4wAIKCMOBkNs5vyQL+rcrZ0P/D6BgSkcK0c4JAnwAAAABJRU5ErkJggg\x3D\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = LineStyleBevel, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LineStyleRound, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LineStyleSquare, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant

	#tag Constant, Name = NavigatorIcon, Type = String, Dynamic = False, Default = \"iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAhFBMVEUAAAAKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHIKUHI56cByAAAAK3RSTlMAAQMGBwkODxASFhgaHB0eHyAnKjQ7PD0+QEFJTlBkaHmChpSir8HR19n5dFRkKwAAAH9JREFUGFdlzUkCgjAUBNEyIoiIgShfcADEObn//VwQEbWWb9ENQFqITwOQu08aoBqBEJU/IJbqFMaXfZg8lmHXQx0ka5MEjdrePDDZGZnXSrUeZmdrFtNGHe89dBvnDvHqqa9+9O9lBAViyUaQIhay6l0OYvlOOztU9iJDEfACkYwY5bLS6xAAAAAASUVORK5CYII\x3D", Scope = Private
	#tag EndConstant

	#tag Constant, Name = OrientationLandscape, Type = String, Dynamic = False, Default = \"landscape", Scope = Public
	#tag EndConstant

	#tag Constant, Name = OrientationPortrait, Type = String, Dynamic = False, Default = \"portrait", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ShowInTrayArea, Type = Boolean, Dynamic = False, Default = \"True", Scope = Private
	#tag EndConstant

	#tag Constant, Name = SideLeft, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = SideRight, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = UnitCentimeters, Type = String, Dynamic = False, Default = \"cm", Scope = Public
	#tag EndConstant

	#tag Constant, Name = UnitInches, Type = String, Dynamic = False, Default = \"in", Scope = Public
	#tag EndConstant

	#tag Constant, Name = UnitMillimeters, Type = String, Dynamic = False, Default = \"mm", Scope = Public
	#tag EndConstant

	#tag Constant, Name = UnitPoints, Type = String, Dynamic = False, Default = \"pt", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
