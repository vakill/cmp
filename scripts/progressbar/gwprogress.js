/*!
GraffitiWebProgressBar v1.0
(c) 2014 CyphersTECH Consulting
Proptietary license available at
http://www.graffitisuite.com/legal/source-code-eula/
*/

(function ($) {

    $.gwProgressBar = function (element, options) {

        var defaults = {
            value: 0,
            progressColor: '#186de2',
            animationTime: 1000,
            customText: '',
            percentSign: '%',
            showTip: true,
            tooltipColor: '#4C4C4C',
            tooltipTextColor: '#fff',
            backgroundColor: '#444'
        };

        var plugin = this;

        plugin.settings = {};

        var noAnim = true;

        var $element = $(element);

        plugin.init = function () {
            plugin.settings = $.extend({}, defaults, options);
            $element.addClass('gwprogressbar');
            $element.attr('data-perc', plugin.settings.value);
            $element.append('<div class=\'gwpbinner\' style=\'background-color:' + plugin.settings.progressColor + ';\'><span></span></div>');
            $element.append('<div class=\'gwpblabel\'><span></span><div class=\'gwpbperc\'>' + plugin.settings.value + ' ' + plugin.settings.percentSign + '</div></div>');
            this.updateOption('value', plugin.settings.value);
            this.updateOption('progressColor', plugin.settings.progressColor);
            this.updateOption('backgroundColor', plugin.settings.backgroundColor);
            this.updateOption('showTip', plugin.settings.showTip);
            this.updateOption('tooltipColor', plugin.settings.tooltipColor);
            this.updateOption('tooltipTextColor', plugin.settings.tooltipTextColor);
            noAnim = false;
        };

        plugin.updateOption = function (theOption, newValue) {
            if (theOption == 'tooltipTextColor') {
                plugin.settings.tooltipTextColor = newValue;
                $element.find('.gwpblabel').css('color', plugin.settings.tooltipTextColor);
            } else if (theOption == 'tooltipColor') {
                plugin.settings.tooltipColor = newValue;
                $element.find('.gwpblabel').css('background', plugin.settings.tooltipColor);
                var oldStyle = $('#' + $element.attr('id') + '_tooltip');
                if (typeof(oldStyle) != 'undefined') {
                    oldStyle.remove();
                }
                $('<style id="' + $element.attr('id') + '_tooltip">.gwpblabel > span::before{background:' + plugin.settings.tooltipColor + '}</style>').appendTo('head');
            } else if (theOption == 'progressColor') {
                plugin.settings.progressColor = newValue;
                var theRGB = hexToRGB(newValue);
                $element.find('.gwpbinner').css('background', plugin.settings.progressColor).css('-webkit-box-shadow', '0px 0px 12px 0px rgba(' + theRGB.r + ', ' + theRGB.g + ', ' + theRGB.b + ', 1), inset 0px 1px 0px 0px rgba(255, 255, 255, 0.45), inset 1px 0px 0px 0px rgba(255, 255, 255, 0.25), inset -1px 0px 0px 0px rgba(255, 255, 255, 0.25)').css('box-shadow', '0px 0px 12px 0px rgba(' + theRGB.r + ', ' + theRGB.g + ', ' + theRGB.b + ', 1), inset 0px 1px 0px 0px rgba(255, 255, 255, 0.45), inset 1px 0px 0px 0px rgba(255, 255, 255, 0.25), inset -1px 0px 0px 0px rgba(255, 255, 255, 0.25)');
            } else if (theOption == 'value') {
                plugin.settings.value = newValue;
                var barPerc = (($element.width() / 100) * plugin.settings.value) - 4;
                if (noAnim === false) {
                    $element.find('.gwpbinner').animate({
                        width: barPerc
                    }, plugin.settings.animationTime);
                } else {
                    $element.find('.gwpbinner').css('width', barPerc);
                }
                var intWidth = $element.find('.gwpbperc').width();
                $element.find('.gwpblabel').css('min-width', intWidth);
                $element.find('.gwpblabel span').css('min-width', intWidth + 50);
                if (plugin.settings.customText.length === 0) {
                    $element.find('.gwpbperc').text(plugin.settings.value + plugin.settings.percentSign);
                } else {
                    $element.find('.gwpbperc').text(plugin.settings.customText);
                }
                intWidth = $element.find('.gwpbperc').width();
                $element.find('.gwpblabel span').css('left', (intWidth / 2) - 1);
                var labelPos = barPerc - (intWidth / 2);
                if (noAnim === false) {
                    $element.find('.gwpblabel').stop().animate({
                        left: labelPos - 5
                    }, plugin.settings.animationTime);
                } else {
                    $element.find('.gwpblabel').css('left', labelPos - 5);
                }
            } else if (theOption == 'animationTime') {
                plugin.settings.animationTime = newValue;
                noAnim = true;
                plugin.updateOption('value', plugin.settings.value);
                noAnim = false;
            } else if (theOption == 'customText') {
                plugin.settings.customText = newValue;
                noAnim = true;
                plugin.updateOption('value', plugin.settings.value);
                noAnim = false;
            } else if (theOption == 'showTip') {
                plugin.settings.showTip = newValue;
                if (newValue === true) {
                    $element.find('.gwpblabel').css('visibility', 'visible');
                } else {
                    $element.find('.gwpblabel').css('visibility', 'hidden');
                }
                noAnim = true;
                plugin.updateOption('value', plugin.settings.value);
                noAnim = false;
            } else if (theOption == 'backgroundColor') {
                plugin.settings.backgroundColor = newValue;
                $element.css('background', newValue);
            }
        };

        var hexToRGB = function (hex) {
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        };

        plugin.init();

    };

    $.fn.gwProgressBar = function (options) {

        return this.each(function () {
            if (undefined === $(this).data('pluginName')) {
                var plugin = new $.gwProgressBar(this, options);
                $(this).data('gwProgressBar', plugin);
            }
        });
    };

})(jQuery);