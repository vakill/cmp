/*! jQuery insertClass
(c) 2018 GraffitiSuite Solutions
https://graffitisuite.com/legal/source-eula/ */

jQuery.fn.extend({
  insertClass: function(atIndex, className) {
    return this.each(function() {
      var classes = this.getAttribute('class').split(" ");
      classes.splice(atIndex, 0, className);
      this.setAttribute('class', classes.join(" "));
    });
  }
});