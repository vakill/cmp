/***
 * Base editor - From which all of our slickgrid editors are derived
 * @module Editors
 * @namespace Slick
 */

(function ($) {
    function BaseEditor() {}

    /**
     * init
     * Append the text field to the editor container and run setup if defined
     */
    BaseEditor.prototype.init = function () {
        if (this.args) {
            this.container  = this.args.container;
            this.column     = this.args.column;
            this.item       = this.args.item;
            this.grid       = this.args.grid;
        }

        this.$input = $('<input type="text" />');

        if (_.isFunction(this.setup)){
            this.setup();
        }

        this.$input.appendTo(this.container);

        this.focus();
    };

    /**
     * destroy
     * Remove the text field from the editor container
     */
    BaseEditor.prototype.destroy = function () {
        this.$input.empty();
        this.$input.remove();
    };

    /**
     * focus
     * Give focus to, and select the text of the editor field
     */
    BaseEditor.prototype.focus = function () {
        var scope = this;
        this.$input.focus();

        _.defer( function () {
            scope.$input.select();
        });
    };

    /**
     * serializeValue
     * Get the plain text contents of the editor field
     */
    BaseEditor.prototype.serializeValue = function () {
        return this.$input.val();
    };

    /**
     * applyValue
     * Apply the value of the editor field to the dataset in dataView
     */
    BaseEditor.prototype.applyValue = function (item, state) {
        item[this.column.field] = state;
    };

    /**
     * loadValue
     * Load the value of a cell in the dataset into the editor field
     */
    BaseEditor.prototype.loadValue = function (item) {
        var defaultValue = '';
        if (!_.isUndefined(item[this.column.field])) {
            defaultValue = item[this.column.field];
        }
        this.$input.val(defaultValue);
        this.$input.defaultValue = defaultValue;
        this.$input.select();
    };

    /**
     * isValueChanged
     * @return {boolean} - Has the data in the editor field changed when compared with the dataview
     */
    BaseEditor.prototype.isValueChanged = function () {
        return this.item !== this.$input.val();
    };

    /**
     * validate
     * @return {object}
     *      valid: is the input valid against the regex for that field
     *      msg: Failed validation message
     */
    BaseEditor.prototype.validate = function () {
        var validation = { valid: true };

        if (this.column.validator) {
            if (_.isNull(this.$input.val().match(new RegExp(this.column.validator)))) {
                validation.valid = false;
                validation.msg = "Failed validation: " + this.column.validatorMessage;
            }
        }

        return validation;
    };

    /** Create the Slick.Editors object if it does not exist */
    if (!window.Slick.Editors) 
        window.Slick.Editors = {};

    /** Extend the Slick.Editors */
    $.extend(true, window.Slick.Editors, {
        "Base": BaseEditor
    });

})(jQuery);