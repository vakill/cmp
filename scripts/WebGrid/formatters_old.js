/***
 * Contains basic SlickGrid formatters.
 *
 * NOTE:  These are merely examples.  You will most likely need to implement something more
 *        robust/extensible/localizable/etc. for your use!
 *
 * @module Formatters
 * @namespace Slick
 */

(function ($) {
  // register namespace
  $.extend(true, window, {
    "Slick": {
      "Formatters": {
        "PercentComplete": PercentCompleteFormatter,
        "PercentCompleteBar": PercentCompleteBarFormatter,
        "YesNo": YesNoFormatter,
        "Checkmark": CheckmarkFormatter,
        "FontAwesome": FAFormatter,
        "Currency": CurrencyFormatter,
        "Color": ColorFormatter,
        "Picture": PictureFormatter,
        "HTML": HTMLFormatter
      }
    }
  });

  function PictureFormatter(row,cell,value,columnDef,dataContext) {
    if (value==null || value === "") {
      return ""
    } else {
      return "<img style='background-image:url(" + value + ");background-repeat:no-repeat;background-size:100% 100%;width:100%;height:100%'>&nbsp;</span>"
    }
  }

  function ColorFormatter(row,cell,value,columnDef,dataContext) {
    if (value==null || value === "") {
      return ""
    } else {
      return "<span style='background-color:" + value + ";width:100%;height:100%'>&nbsp;</span>"
    }
  }

  function HTMLFormatter(row, cell, value, columnDef, dataContext) {
    return value;
  }

  function PercentCompleteFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "-";
    } else if (value < 50) {
      return "<span style='color:red;font-weight:bold;'>" + value + "%</span>";
    } else {
      return "<span style='color:green'>" + value + "%</span>";
    }
  }

  function FAFormatter(row, cell, value, columnDef, dataContent) {
    if (value == null || value === "") {
      return ""
    }
    return "<i class='" + value.toString() + "' style='font-family: fontawesome;font-style:normal;'></i>";
  }

  function PercentCompleteBarFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "";
    }

    var color;

    if (value < 30) {
      color = "red";
    } else if (value < 70) {
      color = "silver";
    } else {
      color = "green";
    }

    return "<span class='percent-complete-bar' style='background:" + color + ";width:" + value + "%'></span>";
  }

  function YesNoFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "Yes" : "No";
  }

  function CheckmarkFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "<input type='checkbox' name='' value='" + value + "' checked disabled />" : "<input type='checkbox' name='' value='" + value + "' disabled />";
  }

  function CurrencyFormatter(row, cell, value, columnDef, dataContext) {
    return parseFloat(value).toFixed(2);
  }
})(jQuery);
