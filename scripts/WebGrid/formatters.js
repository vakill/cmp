/***
 * Contains basic SlickGrid formatters.
 *
 * NOTE:  These are merely examples.  You will most likely need to implement something more
 *        robust/extensible/localizable/etc. for your use!
 *
 * @module Formatters
 * @namespace Slick
 */

(function ($) {
  // register namespace
  $.extend(true, window, {
    "Slick": {
      "Formatters": {
        "PercentComplete": PercentCompleteFormatter,
        "PercentCompleteBar": PercentCompleteBarFormatter,
        "YesNo": YesNoFormatter,
        "Checkmark": CheckmarkFormatter,
        "FontAwesome": FAFormatter,
        "Currency": CurrencyFormatter,
        "Double": DoubleFormatter,
        "Color": ColorFormatter,
        "Picture": PictureFormatter,
        "Date": DateFormatter,
        "HTML": HTMLFormatter,
        "LongText": LongTextFormatter,
        "Password": PasswordFormatter,
        "Tree": TreeFormatter
      }
    }
  });

  function TreeFormatter(row, cell, value, columnDef, dataContext) {
	  var spacer = "<span style='display:inline-block;height:1px;width:" + (10 * dataContext["_indent"]) + "px'></span>";
	  if (dataContext._expandable) {
      	if (dataContext._collapsed) {
	    	return spacer + "<span class='toggle fas fa-caret-down fa-fw'></span>&nbsp;" + value;
	  	} else {
	    	return spacer + "<span class='toggle fas fa-caret-right fa-fw'></span>&nbsp;" + value;
	  	}
	  }
	  return spacer + "<span class='toggle fas fa-fw'></span>&nbsp;" + value;
  }

  function PasswordFormatter(row, cell, value, columnDef, dataContext) {
    if (value==null || value === "") {
      return "";
    } else {
      var placeHolder = new Array(value.length + 1).join( '&#183;' );
      return "<span style='font-size:200%;'>" + placeHolder + "</span>";
    }
  }

   function LongTextFormatter(row, cell, value, columnDef, dataContext) {
    if (value==null || value === "") {
      return "";
    } else {
      return "<div style='display:block;width:100%;height:100%;word-wrap:normal;white-space:initial;overflow:auto;'>" + value + "</div>";
    }
  }

  function DateFormatter(row, cell, value, columnDef, dataContext) {
    var browserLocale = window.navigator.userLanguage || window.navigator.language;
    moment.locale(browserLocale);
    var theLocale = moment.localeData();
    var shortFormat = theLocale.longDateFormat('L');
    var theMoment = new moment(value);
    return theMoment.format(shortFormat);
  }

  function PictureFormatter(row,cell,value,columnDef,dataContext) {
    if (value==null || value === "") {
      return ""
    } else {
      return "<div style='display:block;background-image:url(" + value + ");background-repeat:no-repeat;background-size:100% 100%;width:100%;height:100%'>&nbsp;</div>"
    }
  }

  function ColorFormatter(row,cell,value,columnDef,dataContext) {
    if (value==null || value === "") {
      return ""
    } else {
      return "<span style='background-color:" + value + ";width:100%;height:100%;display:block'>&nbsp;</span>"
    }
  }

  function HTMLFormatter(row, cell, value, columnDef, dataContext) {
    return value;
  }

  function PercentCompleteFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "-";
    } else if (value < 50) {
      return "<span style='color:red;font-weight:bold;'>" + value + "%</span>";
    } else {
      return "<span style='color:green'>" + value + "%</span>";
    }
  }

  function FAFormatter(row, cell, value, columnDef, dataContent) {
    if (value == null || value === "") {
      return ""
    }
    return "<i class='" + value.toString() + "'></i>";
  }

  function PercentCompleteBarFormatter(row, cell, value, columnDef, dataContext) {
    if (value == null || value === "") {
      return "";
    }

    var color;

    if (value < 30) {
      color = "red";
    } else if (value < 70) {
      color = "silver";
    } else {
      color = "green";
    }

    return "<span class='percent-complete-bar' style='background:" + color + ";width:" + value + "%'></span>";
  }

  function YesNoFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "Yes" : "No";
  }

  function CheckmarkFormatter(row, cell, value, columnDef, dataContext) {
    return value ? "<i class='fa fa-fw fa-check-square'></i>" : "<i class='fa fa-fw fa-square'></i>";
  }

  function CurrencyFormatter(row, cell, value, columnDef, dataContext) {
    var browserLocale = window.navigator.userLanguage || window.navigator.language;
    var currencyVal = parseFloat(value).toLocaleString(browserLocale, {minimumFractionDigits: columnDef['_decimalPlaces'], maximumFractionDigits: columnDef['_decimalPlaces'], useGrouping: false});
    return currencyVal;
  }

  function DoubleFormatter(row, cell, value, columnDef, dataContext) {
    var browserLocale = window.navigator.userLanguage || window.navigator.language;
    var doubleVal = parseFloat(value);
    doubleVal = doubleVal.toLocaleString(browserLocale, {minimumFractionDigits: columnDef['_decimalPlaces'], maximumFractionDigits: columnDef['_decimalPlaces'], useGrouping: false});
    return doubleVal;
  }

  function IntegerFormatter(row, cell, value, columnDef, dataContext) {
    return parseInt(value);
  }
})(jQuery);
