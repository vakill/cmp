/***
 * DatePicker - A cell editor with bootstrap datepicker driven calendar
 * @module Editors
 * @namespace Slick
 */
 
(function ($) {
    function DatePicker (args) {
        var scope = this;
        this.args = args;
 
        this.setup = function () {
            this.$input.datepicker({
                    format: "mm/dd/yyyy",
                    todayHighlight: true,
                    forceParse: false
                })
                .on('changeDate', function () {
                    scope.$input.focus();
                });
        };
 
        this.destroy = function () {
            this.$input.datepicker('hide');
            this.$input.datepicker('destroy');
            this.$input.empty();
            this.$input.remove();
        };
 
        this.init();
    }
 
    DatePicker.prototype = Slick.Editors.Base.prototype;
 
    /** Extend the Slick.Editors */
    $.extend(true, window.Slick.Editors, {
        "Date": DatePicker
    });
 
})(jQuery);