/***
 * Contains basic SlickGrid editors.
 * @module Editors
 * @namespace Slick
 */

(function ($) {
  // register namespace
  $.extend(true, window, {
    "Slick": {
      "Editors": {
        "Text": TextEditor,
        "Integer": IntegerEditor,
        "YesNoSelect": YesNoSelectEditor,
        "Checkbox": CheckboxEditor,
        "PercentComplete": PercentCompleteEditor,
        "LongText": LongTextEditor,
        "Date": DateEditor,
        "Password": PasswordEditor,
        "Double": DoubleEditor,
        "Currency": CurrencyEditor
      }
    }
  });

  function TextEditor(args) {
    var $input;
    var defaultValue;
    var scope = this;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-text' />")
          .appendTo(args.container)
          .bind("keydown.nav", function (e) {
            if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
              e.stopImmediatePropagation();
            }
          })
          .focus()
          .select();
    };

    this.destroy = function () {
      $input.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.getValue = function () {
      return $input.val();
    };

    this.setValue = function (val) {
      $input.val(val);
    };

    this.loadValue = function (item) {
      defaultValue = item[args.column.field] || "";
      $input.val(defaultValue);
      $input[0].defaultValue = defaultValue;
      $input.select();
    };

    this.serializeValue = function () {
      return $input.val();
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = state;
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };

    this.validate = function () {
      if (args.column.validator) {
        var validationResults = args.column.validator($input.val());
        if (!validationResults.valid) {
          return validationResults;
        }
      }

      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  function PasswordEditor(args) {
    var $input;
    var defaultValue;
    var scope = this;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-text' />")
          .appendTo(args.container)
          .bind("keydown.nav", function (e) {
            if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
              e.stopImmediatePropagation();
            }
          })
          .focus()
          .select();
    };

    this.destroy = function () {
      $input.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.getValue = function () {
      return $input.val();
    };

    this.setValue = function (val) {
      $input.val(val);
    };

    this.loadValue = function (item) {
      defaultValue = item[args.column.field] || "";
      $input.val(defaultValue);
      $input[0].defaultValue = defaultValue;
      $input.select();
    };

    this.serializeValue = function () {
      return $input.val();
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = state;
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };

    this.validate = function () {
      if (args.column.validator) {
        var validationResults = args.column.validator($input.val());
        if (!validationResults.valid) {
          return validationResults;
        }
      }

      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  function IntegerEditor(args) {
    var $input;
    var defaultValue;
    var scope = this;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-text' />");

      $input.bind("keydown.nav", function (e) {
        if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
          e.stopImmediatePropagation();
        }
      });

      $input.appendTo(args.container);
      $input.focus().select();
    };

    this.destroy = function () {
      $input.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.loadValue = function (item) {
      defaultValue = item[args.column.field];
      $input.val(defaultValue);
      $input[0].defaultValue = defaultValue;
      $input.select();
    };

    this.serializeValue = function () {
      return parseInt($input.val(), 10) || 0;
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = parseInt(state);
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };

    this.validate = function () {
      if (isNaN($input.val())) {
        return {
          valid: false,
          msg: "Please enter a valid integer"
        };
      }

      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  function CurrencyEditor(args) {
    var $input;
    var defaultValue;
    var scope = this;
    var floatNum = 1000.1;
    var browserLocale = window.navigator.userLanguage || window.navigator.language;
    var decimalSep = Intl.NumberFormat(browserLocale).formatToParts(floatNum).find(part => part.type === 'decimal').value;
    var thousandsSep = Intl.NumberFormat(browserLocale).formatToParts(floatNum).find(part => part.type === 'group').value;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-text' />");

      $input.bind("keydown.nav", function (e) {
        if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
          e.stopImmediatePropagation();
        }
      });

      $input.appendTo(args.container);
      $input.focus().select();
    };

    this.destroy = function () {
      $input.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.loadValue = function (item) {
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      defaultValue = parseFloat(item[args.column.field]).toLocaleString(browserLocale, {minimumFractionDigits: 2, maximumFractionDigits: 2, useGrouping: false});
	  $input.val(defaultValue);
      $input[0].defaultValue = defaultValue;
      $input.select();
    };

    this.serializeValue = function () {
      var theVal = $input.val().replace(thousandsSep, "").replace(decimalSep, ".");
      return parseFloat(theVal).toFixed(2) || 0;
    };

    this.applyValue = function (item, state) {
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      var theVal = parseFloat(state).toLocaleString(browserLocale, {minimumFractionDigits: 2, maximumFractionDigits: 2, useGrouping: false}).replace(decimalSep, ".");
      item[args.column.field] = theVal;
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };

    this.validate = function () {
      var theVal = $input.val().replace(thousandsSep, "").replace(decimalSep, ".");
      if (isNaN(theVal)) {
        return {
          valid: false,
          msg: "Please enter a valid currency value"
        };
      }

      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  function YesNoSelectEditor(args) {
    var $select;
    var defaultValue;
    var scope = this;

    this.init = function () {
      $select = $("<SELECT tabIndex='0' class='editor-yesno'><OPTION value='yes'>Yes</OPTION><OPTION value='no'>No</OPTION></SELECT>");
      $select.appendTo(args.container);
      $select.focus();
    };

    this.destroy = function () {
      $select.remove();
    };

    this.focus = function () {
      $select.focus();
    };

    this.loadValue = function (item) {
      $select.val((defaultValue = item[args.column.field]) ? "yes" : "no");
      $select.select();
    };

    this.serializeValue = function () {
      return ($select.val() == "yes");
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = state;
    };

    this.isValueChanged = function () {
      return ($select.val() != defaultValue);
    };

    this.validate = function () {
      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  function CheckboxEditor(args) {
    var $select;
    var defaultValue;
    var scope = this;

    this.init = function () {
      $select = $("<i class='fa fa-check-square'></i>");
      $select.on('mousedown', function(){
        if ($select.hasClass('fa-check-square')) {
          $select.removeClass('fa-check-square').addClass('fa-square');
        } else {
          $select.removeClass('fa-square').addClass('fa-check-square');
        }
      })
      $select.appendTo(args.container);
    };

    this.destroy = function () {
      $select.remove();
    };

    this.focus = function () {
      
    };

    this.loadValue = function (item) {
      defaultValue = !!item[args.column.field];
      if (defaultValue) {
        $select.addClass('fa-check-square').removeClass('fa-square');
      } else {
        $select.addClass('fa-square').removeClass('fa-check-square');
      }
    };

    this.serializeValue = function () {
      return $select.hasClass('fa-check-square');
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = state;
    };

    this.isValueChanged = function () {
      return (this.serializeValue() !== defaultValue);
    };

    this.validate = function () {
      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  function PercentCompleteEditor(args) {
    var $input, $picker;
    var defaultValue;
    var scope = this;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-percentcomplete' />");
      $input.width($(args.container).innerWidth() - 25);
      $input.appendTo(args.container);

      $picker = $("<div class='editor-percentcomplete-picker' />").appendTo(args.container);
      $picker.append("<div class='editor-percentcomplete-helper'><div class='editor-percentcomplete-wrapper'><div class='editor-percentcomplete-slider' /></div>");

      $input.focus().select();

      $picker.find(".editor-percentcomplete-slider").slider({
        orientation: "vertical",
        range: "min",
        value: defaultValue,
        slide: function (event, ui) {
          $input.val(ui.value)
        }
      });

      $picker.find(".editor-percentcomplete-buttons button").bind("click", function (e) {
        $input.val($(this).attr("val"));
        $picker.find(".editor-percentcomplete-slider").slider("value", $(this).attr("val"));
      })
    };

    this.destroy = function () {
      $input.remove();
      $picker.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.loadValue = function (item) {
      $input.val(defaultValue = item[args.column.field]);
      $input.select();
    };

    this.serializeValue = function () {
      return parseInt($input.val(), 10) || 0;
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = state;
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ((parseInt($input.val(), 10) || 0) != defaultValue);
    };

    this.validate = function () {
      if (isNaN(parseInt($input.val(), 10))) {
        return {
          valid: false,
          msg: "Please enter a valid positive number"
        };
      }

      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

  /*
   * An example of a "detached" editor.
   * The UI is added onto document BODY and .position(), .show() and .hide() are implemented.
   * KeyDown events are also handled to provide handling for Tab, Shift-Tab, Esc and Ctrl-Enter.
   */
  function LongTextEditor(args) {
    var $input, $wrapper;
    var defaultValue;
    var scope = this;

    this.init = function () {
      var $container = $("body");

      $wrapper = $("<DIV style='z-index:10000;position:absolute;background:white;padding:5px;border:3px solid gray; -moz-border-radius:10px; border-radius:10px;'/>")
          .appendTo($container);

      $input = $("<TEXTAREA hidefocus rows=5 style='backround:white;width:250px;height:80px;border:0;outline:0'>")
          .appendTo($wrapper);

      $("<DIV style='text-align:right'><BUTTON>Save</BUTTON><BUTTON>Cancel</BUTTON></DIV>")
          .appendTo($wrapper);

      $wrapper.find("button:first").bind("click", this.save);
      $wrapper.find("button:last").bind("click", this.cancel);
      $input.bind("keydown", this.handleKeyDown);

      scope.position(args.position);
      $input.focus().select();
    };

    this.handleKeyDown = function (e) {
      if (e.which == $.ui.keyCode.ENTER && e.ctrlKey) {
        scope.save();
      } else if (e.which == $.ui.keyCode.ESCAPE) {
        e.preventDefault();
        scope.cancel();
      } else if (e.which == $.ui.keyCode.TAB && e.shiftKey) {
        e.preventDefault();
        args.grid.navigatePrev();
      } else if (e.which == $.ui.keyCode.TAB) {
        e.preventDefault();
        args.grid.navigateNext();
      }
    };

    this.save = function () {
      args.commitChanges();
    };

    this.cancel = function () {
      $input.val(defaultValue);
      args.cancelChanges();
    };

    this.hide = function () {
      $wrapper.hide();
    };

    this.show = function () {
	  var maxZ = Math.max.apply(null, window.GSjQuery.map(window.GSjQuery('*'), function(e, n) {
      if (window.GSjQuery(e).css('position') == 'absolute')
	       return parseInt(window.GSjQuery(e).css('z-index')) || 1;
	  }));
      $wrapper.show().css("z-index", maxZ + 100);
    };

    this.position = function (position) {
      $wrapper
          .css("top", position.top - 5)
          .css("left", position.left - 5)
    };

    this.destroy = function () {
      $wrapper.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.loadValue = function (item) {
      $input.val(defaultValue = item[args.column.field]);
      $input.select();
    };

    this.serializeValue = function () {
      return $input.val();
    };

    this.applyValue = function (item, state) {
      item[args.column.field] = state;
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };

    this.validate = function () {
      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  };

  function DoubleEditor(args) {
    var $input;
    var defaultValue;
    var scope = this;
    var floatNum = 1000.1;
    var browserLocale = window.navigator.userLanguage || window.navigator.language;
    var decimalSep = Intl.NumberFormat(browserLocale).formatToParts(floatNum).find(part => part.type === 'decimal').value;
    var thousandsSep = Intl.NumberFormat(browserLocale).formatToParts(floatNum).find(part => part.type === 'group').value;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-text' />");

      $input.bind("keydown.nav", function (e) {
        if (e.keyCode === $.ui.keyCode.LEFT || e.keyCode === $.ui.keyCode.RIGHT) {
          e.stopImmediatePropagation();
        }
      });

      $input.appendTo(args.container);
      $input.focus().select();
    };

    this.destroy = function () {
      $input.remove();
    };

    this.focus = function () {
      $input.focus();
    };

    this.loadValue = function (item) {
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      defaultValue = parseFloat(item[args.column.field]).toLocaleString(browserLocale, {minimumFractionDigits: args.column._decimalPlaces, maximumFractionDigits: args.column._decimalPlaces, useGrouping: false});
      $input.val(defaultValue);
      $input[0].defaultValue = defaultValue;
      $input.select();
    };

    this.serializeValue = function () {
      var theVal = $input.val().replace(thousandsSep, "").replace(decimalSep, ".");
      return parseFloat(theVal).toFixed(args.column._decimalPlaces) || 0;
    };

    this.applyValue = function (item, state) {
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      var theVal = parseFloat(state).toLocaleString(browserLocale, {minimumFractionDigits: args.column._decimalPlaces, maximumFractionDigits: args.column._decimalPlaces, useGrouping: false}).replace(decimalSep, ".");
      item[args.column.field] = theVal;
    };

    this.isValueChanged = function () {
      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != defaultValue);
    };

    this.validate = function () {
      var theVal = $input.val().replace(thousandsSep, "").replace(decimalSep, ".");
      if (isNaN(theVal)) {
        return {
          valid: false,
          msg: "Please enter a valid double"
        };
      }

      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

   function DateEditor(args) {
    var $input;
    var defaultValue;
    var scope = this;
    var calendarOpen = false;

    this.init = function () {
      $input = $("<INPUT type=text class='editor-text' readonly />");
      $input.appendTo(args.container);
      $input.focus().select();
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      moment.locale(browserLocale);
      var theLocale = moment.localeData();
      var shortFormat = theLocale.longDateFormat('L').toLowerCase();
      
        if (typeof window.GSjQuery.fn.datepicker_bs == 'undefined') {
  			var dp_noconflict = window.GSjQuery.fn.datepicker.noConflict();
  			window.GSjQuery.fn.datepicker_bs = dp_noconflict;
  		}
  

      $input.datepicker_bs({
        showOnFocus: true,
        immediateUpdates: true,
        orientation: 'auto',
        format: shortFormat,
        language: browserLocale,
        autoclose: true,
        show: function () {
          calendarOpen = true
        },
        hide: function () {
          calendarOpen = false
        }
      });
      $input.width($input.width() - 18);
      $button = $("<img width=18 height=18 src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAYFBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD6T+iNAAAAH3RSTlMAAQIDBAkeJig1PFZiaXB5e4CCoKKwtMPFyNHk+fv9DpWzrAAAAGhJREFUGFe1zrsSgkAQRNE7qOODxVlFZZFH//9fGpBsQOoJb1VXNUCaM0CeWzbLSQam4wRAKmo0ljKKdUiApIMkSSatW6gAunjn/nL3u58FqOEKAdyw/fCMd8Q3Ij7x+Nukr4/2AFaBH9xFC/z5vDowAAAAAElFTkSuQmCC'>");
      $button.width(18).css('position','relative').css('top',0).css('right',0);
      $button.appendTo(args.container);
      $button.click(function() {
        var theInput = $(this).parent().find('input');
        $input.datepicker_bs('update', defaultValue);
        theInput.datepicker_bs('show');
      });
    };

    this.destroy = function () {
      $input.datepicker_bs('hide');
      $input.datepicker_bs('destroy');
      $input.remove();
    };

    this.show = function () {
      if (!calendarOpen) {
        $input.datepicker_bs('show');
        var maxZ = Math.max.apply(null,window.GSjQuery.map(window.GSjQuery('*'), function(e,n){
          if(window.GSjQuery(e).css('position')=='absolute')
            return parseInt(window.GSjQuery(e).css('z-index'))||1 ;
          })
        );
        var thePopup = window.GSjQuery('.datepicker-dropdown');
        thePopup.css('z-index', maxZ + 1);
      }
    };

    this.hide = function () {
      if (calendarOpen) {
        $input.datepicker_bs('hide');
      }
    };

    this.position = function (position) {
      if (!calendarOpen) {
        return;
      }
      $.datepicker_bs.dpDiv
          .css("top", position.top + 30)
          .css("left", position.left);
    };

    this.focus = function () {
      $input.focus();
    };

    this.loadValue = function (item) {
      defaultValue = item[args.column.field];
      
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      moment.locale(browserLocale);
      var theLocale = moment.localeData();
      var shortFormat = theLocale.longDateFormat('L');
      var theMoment = new moment(defaultValue);

      $input.val(theMoment.format(shortFormat));
      $input.datepicker_bs('setDate', theMoment.toDate())
      $input[0].defaultValue = defaultValue;
      $input.select();
    };

    this.serializeValue = function () {
      return $input.val();
    };

    this.applyValue = function (item, state) {
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      moment.locale(browserLocale);
      // var theLocale = moment.localeData();
      // var shortFormat = theLocale.longDateFormat('L');
      // var theMoment = new moment(state, shortFormat);
      // item[args.column.field] = theMoment.toDate();
      
      // Fix per Andrea Cinelli, Apr 17, 2018
      var date = moment.utc(state, 'L');
      if (date.isValid()) {
        var dateValue = date.format('YYYY-MM-DD');
	      item[args.column.field] = dateValue
      }
    };

    this.isValueChanged = function () {
      var browserLocale = window.navigator.userLanguage || window.navigator.language;
      moment.locale(browserLocale);
      var theLocale = moment.localeData();
      var shortFormat = theLocale.longDateFormat('L');
      var theMoment = new moment(defaultValue);

      return (!($input.val() == "" && defaultValue == null)) && ($input.val() != theMoment.format(shortFormat));
    };

    this.validate = function () {
      return {
        valid: true,
        msg: null
      };
    };

    this.init();
  }

})(jQuery);
