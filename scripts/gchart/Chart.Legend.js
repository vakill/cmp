function legend(parent, data) {
    parent.className = 'legend';
    var datas = data.hasOwnProperty('datasets') ? data.datasets : data;

    // remove possible children of the parent
    while(parent.hasChildNodes()) {
        parent.removeChild(parent.lastChild);
    }

    datas.forEach(function(d) {
        var item = document.createElement('div');
        var title = document.createElement('div');
        title.className = 'title';
        title.style.borderColor = d.hasOwnProperty('strokeColor') ? d.strokeColor : d.color;
        title.style.background = d.color;
        title.style.borderStyle = 'solid';
        title.style.width = '10px';
        title.style.height = '10px';
        title.style.float = 'left';
        title.style.margin = '0 5px 0 5px';
        parent.appendChild(item);
        item.appendChild(title);

        var text = document.createTextNode("  ");
        title.appendChild(text);
        text = document.createTextNode(d.label);
        item.appendChild(text);
    });
    var strFull;
    strFull = parent.innerHTML;
    return strFull;
}
