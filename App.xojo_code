#tag Class
Protected Class App
Inherits WebApplication
	#tag Method, Flags = &h0
		Sub errorLogging(userErrorMsg As String)
		  // ### Pending To Do
		  Dim f As FolderItem = New FolderItem("CMPLog.txt")
		  Dim d As New Date
		  If f <> Nil then
		    Try
		      Dim t as TextOutputStream = TextOutputStream.Append(f)
		      t.Write d.SQLDateTime + ": " + userErrorMsg + EndOfLine
		      t.Close()
		    Catch e as IOException
		      //handle error
		    End Try
		  End if
		  
		End Sub
	#tag EndMethod


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
