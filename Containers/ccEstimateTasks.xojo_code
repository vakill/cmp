#tag WebPage
Begin WebContainer ccEstimateTasks
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   520
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle wrcETBottom
      Cursor          =   0
      Enabled         =   True
      Height          =   417
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1531004927"
      TabOrder        =   -1
      Top             =   95
      VerticalCenter  =   0
      Visible         =   True
      Width           =   950
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle wrcETTop
      Cursor          =   0
      Enabled         =   True
      Height          =   84
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1531004927"
      TabOrder        =   -1
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   950
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETTitle1
      Caption         =   "<div class= 'materialTitles'><b>&nbsp;&nbsp;&nbsp;&nbsp;Πρόβλεψη Εργασίας</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   34
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "778278911"
      TabOrder        =   -1
      Top             =   9
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   172
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETTask1
      Caption         =   "<div class= 'estimateLabels'>Εργασία</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   39
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   50
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wtfETTask
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   126
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   45
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETFrom1
      Caption         =   "<div class= 'estimateLabels'>Από</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   279
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   48
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wtfETFrom
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   43
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   61
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETTo1
      Caption         =   "<div class= 'estimateLabels'>Έως</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   448
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   48
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wtfETTo
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   535
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   43
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   61
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETPrice1
      Caption         =   "<div class= 'estimateLabels'>Τιμή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   642
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   48
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField wtfETPrice
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   731
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1072697343"
      TabOrder        =   3
      Text            =   ""
      TextAlign       =   0
      Top             =   43
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivOk
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   879
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   226648063
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   12
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivCancel
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   916
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   25726975
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   12
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETTitle2
      Caption         =   "<div class= 'usersLabels'><b>&nbsp;&nbsp;&nbsp;&nbsp;Λίστα Εργασιών</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "778278911"
      TabOrder        =   -1
      Top             =   95
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   172
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETTask2
      Caption         =   "<div class= 'estimatesColumns'>Εργασία</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   61
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   143
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesTask3
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   254
      VerticalCenter  =   0
      Visible         =   True
      Width           =   922
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesTask4
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   297
      VerticalCenter  =   0
      Visible         =   True
      Width           =   922
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETFrom2
      Caption         =   "<div class= 'estimatesColumns'>Από</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   340
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   143
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETTo2
      Caption         =   "<div class= 'estimatesColumns'>Έως</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   476
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   143
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlbETPrice2
      Caption         =   "<div class= 'estimatesColumns'>Κόστος</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   739
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   143
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesTask1
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   168
      VerticalCenter  =   0
      Visible         =   True
      Width           =   922
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesTask2
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   15
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   211
      VerticalCenter  =   0
      Visible         =   True
      Width           =   922
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  ccEpimEntiesTask1.setUp("1     &emsp;  ΟΝΟΜΑ_ΕΡΓΑΣΙΑΣ  &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; &emsp; 01/06/2018 &emsp; &emsp;&emsp; 01/07/2018  &emsp; &emsp;&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; 1.200,00", 1)
		  ccEpimEntiesTask2.setUp("2   &emsp;    ΟΝΟΜΑ_ΕΡΓΑΣΙΑΣ  &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp;01/07/2018 &emsp; &emsp;&emsp; 01/08/2018  &emsp; &emsp;  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; 1.500,00", 1)
		  ccEpimEntiesTask3.setUp("3     &emsp;  ΟΝΟΜΑ_ΕΡΓΑΣΙΑΣ  &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp;01/05/2018 &emsp; &emsp;&emsp; 01/06/2018 &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; 2.000,00", 1)
		  ccEpimEntiesTask4.setUp("4   &emsp;    ΟΝΟΜΑ_ΕΡΓΑΣΙΑΣ &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp;15/08/2018 &emsp; &emsp;&emsp; 19/09/2018 &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; 1.000,00", 1)
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		parentContainer As ccEstimates
	#tag EndProperty


#tag EndWindowCode

#tag Events wtfETPrice
	#tag Event
		Sub LostFocus()
		  me.text = format(val(me.text),"#,###.00")
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  'if (me.text.Len > 0) Then
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivOk
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Ok")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivCancel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  me.Style = wsRoundedGreenFull
		  parentContainer.ccEpimEntiesCost1.Visible = true
		  parentContainer.ccEpimEntiesCost2.Visible = true
		  parentContainer.ccEpimEntiesCost3.Visible = true
		  parentContainer.ccEpimEntiesCost4.Visible = true
		  parentContainer.ccLabelButtonTasks.Visible = true
		  parentContainer.GraffitiWebLabel1.Visible = true
		  parentContainer.GraffitiWebLabel2.Visible = true
		  parentContainer.GraffitiWebLabel3.Visible = true
		  parentContainer.GraffitiWebLabel4.Visible = true
		  parentContainer.GraffitiWebLabel5.Visible = true
		  parentContainer.GraffitiWebLabel6.Visible = true
		  parentContainer.GraffitiWebLabel7.Visible = true
		  parentContainer.GraffitiWebLabel8.Visible = true
		  parentContainer.GraffitiWebLabel9.Visible = true
		  parentContainer.GraffitiWebLabel10.Visible = true
		  parentContainer.ImageView1.Visible = true
		  parentContainer.Rectangle1.Visible = true
		  parentContainer.Style = wsWhite
		  self.close
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
