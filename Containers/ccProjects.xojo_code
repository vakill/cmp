#tag WebPage
Begin WebContainer ccProjects
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   620
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin ccEpimetriseis ccEpimetriseisTab
      Cursor          =   0
      Enabled         =   True
      Height          =   542
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   56
      VerticalCenter  =   0
      Visible         =   False
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccCalendar ccCalendarTab
      Cursor          =   0
      Enabled         =   True
      Height          =   542
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   2
      Top             =   56
      VerticalCenter  =   0
      Visible         =   False
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccCost ccCostTab
      Cursor          =   0
      Enabled         =   True
      Height          =   542
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   3
      Top             =   56
      VerticalCenter  =   0
      Visible         =   False
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccBalance ccBalanceTab
      Cursor          =   0
      Enabled         =   True
      Height          =   542
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   4
      Top             =   56
      VerticalCenter  =   0
      Visible         =   False
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEstimates ccEstimatesTab
      Cursor          =   0
      Enabled         =   True
      Height          =   542
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   5
      Top             =   56
      VerticalCenter  =   0
      Visible         =   False
      Width           =   970
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccProjectTabButton ccProjectTabButtonEpim
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   176
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccProjectTabButton ccProjectTabButtonCal
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   206
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   176
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccProjectTabButton ccProjectTabButtonCost
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   404
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   176
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccProjectTabButton ccProjectTabButtonBalance
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   598
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   176
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccProjectTabButton ccProjectTabButtonEstimates
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   36
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   795
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   176
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  ccProjectTabButtonEpim.setUp("ΕΠΙΜΕΤΡΗΣΕΙΣ")
		  ccProjectTabButtonCal.setUp("ΗΜΕΡΟΛΟΓΙΟ")
		  ccProjectTabButtonCost.setUp("ΚΟΣΤΟΣ")
		  ccProjectTabButtonBalance.setUp("ΛΟΓΑΡΙΑΣΜΟΣ")
		  ccProjectTabButtonEstimates.setUp("ΠΡΟΒΛΕΨΕΙΣ")
		  ccEstimatesTab.parentContainer = self
		  SetState("Επιμετρήσεις")
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub setState(tab As String)
		  select case tab 
		  case "Επιμετρήσεις"
		    ccProjectTabButtonEpim.toggleRec(true)
		    ccProjectTabButtonCal.toggleRec(false)
		    ccProjectTabButtonCost.toggleRec(false)
		    ccProjectTabButtonBalance.toggleRec(false)
		    ccProjectTabButtonEstimates.toggleRec(false)
		    ccEpimetriseisTab.Visible = True
		    ccCalendarTab.Visible = False
		    ccCostTab.Visible = False
		    ccBalanceTab.Visible = False
		    ccEstimatesTab.Visible = False
		  case "Ημερολόγιο"
		    ccProjectTabButtonEpim.toggleRec(false)
		    ccProjectTabButtonCal.toggleRec(true)
		    ccProjectTabButtonCost.toggleRec(false)
		    ccProjectTabButtonBalance.toggleRec(false)
		    ccProjectTabButtonEstimates.toggleRec(false)
		    ccEpimetriseisTab.Visible = False
		    ccCalendarTab.Visible = True
		    ccCostTab.Visible = False
		    ccBalanceTab.Visible = False
		    ccEstimatesTab.Visible = False
		  case "Κόστος"
		    ccProjectTabButtonEpim.toggleRec(false)
		    ccProjectTabButtonCal.toggleRec(false)
		    ccProjectTabButtonCost.toggleRec(True)
		    ccProjectTabButtonBalance.toggleRec(false)
		    ccProjectTabButtonEstimates.toggleRec(false)
		    ccEpimetriseisTab.Visible = False
		    ccCalendarTab.Visible = False
		    ccCostTab.Visible = True
		    ccBalanceTab.Visible = False
		    ccEstimatesTab.Visible = False
		  case "Λογαριασμός"
		    ccProjectTabButtonEpim.toggleRec(false)
		    ccProjectTabButtonCal.toggleRec(false)
		    ccProjectTabButtonCost.toggleRec(false)
		    ccProjectTabButtonBalance.toggleRec(true)
		    ccProjectTabButtonEstimates.toggleRec(false)
		    ccEpimetriseisTab.Visible = false
		    ccCalendarTab.Visible = False
		    ccCostTab.Visible = False
		    ccBalanceTab.Visible = True
		    ccEstimatesTab.Visible = False
		  case "Προβλέψεις"
		    ccProjectTabButtonEpim.toggleRec(false)
		    ccProjectTabButtonCal.toggleRec(false)
		    ccProjectTabButtonCost.toggleRec(false)
		    ccProjectTabButtonBalance.toggleRec(false)
		    ccProjectTabButtonEstimates.toggleRec(true)
		    ccEpimetriseisTab.Visible = False
		    ccCalendarTab.Visible = False
		    ccCostTab.Visible = False
		    ccBalanceTab.Visible = False
		    ccEstimatesTab.Visible = true
		  end select
		  
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag Events ccProjectTabButtonEpim
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  setState("Επιμετρήσεις")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccProjectTabButtonCal
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  setState("Ημερολόγιο")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccProjectTabButtonCost
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  setState("Κόστος")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccProjectTabButtonBalance
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Λογαριασμός")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccProjectTabButtonEstimates
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  setState("Προβλέψεις")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
