#tag WebPage
Begin WebContainer ccUsers
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   620
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle6
      Cursor          =   0
      Enabled         =   True
      Height          =   190
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   418
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel2
      Caption         =   "<div class= 'users2Labels'><b>&nbsp;&nbsp;&nbsp;&nbsp;Προσβάσεις Ομάδων</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   805
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "1591676927"
      TabOrder        =   -1
      Top             =   418
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   165
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel3
      Caption         =   "<div class= 'user2Labels'>Όνομα Ομάδας</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   89
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   466
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel4
      Caption         =   "<div class= 'user2Labels'>Περιγραφή Ομάδας</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   588
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   466
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesGroup1
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   488
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesGroup2
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   525
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel10
      Caption         =   "<h5 class= 'user2Labels'><b>Ομάδες Χρηστών</b></h5> "
      Cursor          =   0
      Enabled         =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   45
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   426
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   166
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesGroup3
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   562
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle7
      Cursor          =   0
      Enabled         =   True
      Height          =   407
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel12
      Caption         =   "<div class= 'user2Labels'>Χρήστης</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   89
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   57
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel5
      Caption         =   "<div class= 'user2Labels'>Ενεργός</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   586
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   57
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesUser1
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   79
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesUser2
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   116
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel13
      Caption         =   "<h5 class= 'user2Labels'><b>Χρήστες</b></h5> "
      Cursor          =   0
      Enabled         =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   45
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   166
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesUser3
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   153
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel14
      Caption         =   "<div class= 'user2Labels'>Όνομα Χρήστη</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   271
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   57
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel6
      Caption         =   "<div class= 'user2Labels'>Σχόλια</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   734
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   57
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccLabelButton ccLabelButtonUsers
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   805
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1591676927"
      TabOrder        =   2
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   165
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  ccEpimEntiesGroup1.setUp("     &nbsp;  &emsp;  &emsp;   Admin  &emsp;  &emsp;  &emsp;&emsp;  &nbsp;&nbsp;    &emsp; &emsp;   &emsp;  &emsp; &emsp; &emsp;  &emsp;&emsp;  &emsp; &emsp; &emsp;  &emsp;&emsp;  &emsp;  &emsp;&emsp;  &emsp;  &emsp; Διαχειριστής του συστήματος", 1)
		  ccEpimEntiesGroup2.setUp("", 1)
		  ccEpimEntiesGroup3.setUp("", 1)
		  ccEpimEntiesUser1.setUp("  &nbsp; &emsp;  &emsp;   Geok  &emsp;  &nbsp;&emsp;  &emsp;  &emsp;&emsp;&emsp;  &emsp; Κατερίνα Γεωργιάδη &emsp;&emsp;  &nbsp;&emsp;  &emsp;&emsp;  &nbsp;&nbsp;  &emsp;&emsp;  &emsp;Ναι", 1)
		  ccEpimEntiesUser2.setUp("", 1)
		  ccEpimEntiesUser3.setUp("", 1)
		  ccLabelButtonUsers.setUp("<div class= 'users2Labels'><b>&nbsp;&nbsp;&nbsp;Προσβάσεις Χειριστών</b></div> ")'("<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Βασικά Στοιχεία</b></p>")
		  ccLabelButtonUsers.SetSize(164, 23)
		End Sub
	#tag EndEvent


#tag EndWindowCode

#tag Events ccEpimEntiesUser1
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  'me.style = wsGreenBorderRounded
		End Sub
	#tag EndEvent
	#tag Event
		Sub DoubleClick(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ccEpimEntiesGroup1.Visible = false
		  ccEpimEntiesGroup2.Visible = false
		  ccEpimEntiesGroup3.Visible = false
		  ccEpimEntiesUser1.Visible = false
		  ccEpimEntiesUser2.Visible = false
		  ccEpimEntiesUser3.Visible = false
		  GraffitiWebLabel2.Visible = false
		  GraffitiWebLabel3.Visible = false
		  GraffitiWebLabel4.Visible = false
		  GraffitiWebLabel5.Visible = false
		  GraffitiWebLabel6.Visible = false
		  GraffitiWebLabel10.Visible = false
		  ccLabelButtonUsers.Visible = false
		  GraffitiWebLabel12.Visible = false
		  GraffitiWebLabel13.Visible = false
		  GraffitiWebLabel14.Visible = false
		  Rectangle6.Visible = false
		  Rectangle7.Visible = false
		  dim userInfo As new ccUsersInfo
		  userInfo.EmbedWithin(self, 0, 0, 1000, 620)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccLabelButtonUsers
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  ccEpimEntiesGroup1.Visible = false
		  ccEpimEntiesGroup2.Visible = false
		  ccEpimEntiesGroup3.Visible = false
		  ccEpimEntiesUser1.Visible = false
		  ccEpimEntiesUser2.Visible = false
		  ccEpimEntiesUser3.Visible = false
		  GraffitiWebLabel2.Visible = false
		  GraffitiWebLabel3.Visible = false
		  GraffitiWebLabel4.Visible = false
		  GraffitiWebLabel5.Visible = false
		  GraffitiWebLabel6.Visible = false
		  GraffitiWebLabel10.Visible = false
		  ccLabelButtonUsers.Visible = false
		  GraffitiWebLabel12.Visible = false
		  GraffitiWebLabel13.Visible = false
		  GraffitiWebLabel14.Visible = false
		  Rectangle6.Visible = false
		  Rectangle7.Visible = false
		  dim userInfo As new ccUserPrivilages
		  userInfo.EmbedWithin(self, 0, 0, 1000, 620)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
