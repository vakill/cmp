#tag WebPage
Begin WebContainer ccPrCalendar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   620
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle7
      Cursor          =   0
      Enabled         =   True
      Height          =   116
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle6
      Cursor          =   0
      Enabled         =   True
      Height          =   420
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   191
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnICS
      AutoDisable     =   False
      Caption         =   "Import ICS"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   527
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "2144602111"
      TabOrder        =   8
      Top             =   33
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnGoogle
      AutoDisable     =   False
      Caption         =   "Google Cal"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   404
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "2144602111"
      TabOrder        =   5
      Top             =   76
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnRem
      AutoDisable     =   False
      Caption         =   "Remove All"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   281
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "2144602111"
      TabOrder        =   6
      Top             =   76
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnRestrict
      AutoDisable     =   False
      Caption         =   "Restrict Complex"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   527
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "2144602111"
      TabOrder        =   7
      Top             =   76
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnScrollTime
      AutoDisable     =   False
      Caption         =   "ScrollTime"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   281
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "2144602111"
      TabOrder        =   9
      Top             =   33
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnTimeRestrict
      AutoDisable     =   False
      Caption         =   "Restrict Simple"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   406
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "2144602111"
      TabOrder        =   7
      Top             =   33
      VerticalCenter  =   0
      Visible         =   True
      Width           =   109
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox chkHidePast
      Caption         =   "Hide Past"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   145
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   11
      Top             =   76
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox chkTheme
      Caption         =   "Theme"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   4
      Top             =   33
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCalendar gwcMain
      CalendarView    =   ""
      Cursor          =   0
      CustomTheme     =   False
      Editable        =   True
      Enabled         =   True
      Height          =   399
      HelpTag         =   ""
      HidePastEvents  =   False
      HorizontalCenter=   0
      Index           =   -2147483648
      Language        =   "en"
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockUpdate      =   False
      LockVertical    =   False
      RestrictEventDisplay=   False
      RestrictSelection=   False
      RestrictView    =   False
      Scope           =   0
      ShowDayButton   =   True
      ShowMonthButton =   True
      ShowNavButtons  =   True
      ShowTitle       =   True
      ShowTodayButton =   True
      ShowWeekButton  =   True
      ShowYearButtons =   True
      Style           =   "1072697343"
      TabOrder        =   10
      Top             =   201
      VerticalCenter  =   0
      Visible         =   True
      Width           =   960
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmLang
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   20
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      Top             =   33
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebPopupMenu pmView
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "Month\nAgenda\nAgendaWeek\nAgendaDay\nBasic\nBasicWeek\nBasicDay\nList\nListYear\nListMonth\nListWeek\nListDay"
      Left            =   20
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      Top             =   76
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Function InvertColor(c as Color) As Color
		  Return RGB( 255 - c.Red, 255 - c.Green, 255 - c.Blue )
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RandomColor() As Color
		  dim r as new Random
		  Return RGB( r.InRange( 0, 255 ), r.InRange( 0, 255 ), r.InRange( 0, 255 ), r.InRange( 0, 200 ) )
		End Function
	#tag EndMethod


	#tag Constant, Name = sampleICS, Type = String, Dynamic = False, Default = \"BEGIN:VCALENDAR\rVERSION:2.0\rPRODID:GraffitiWebCalendar\rBEGIN:VEVENT\rUID:20180326T103501Z@com.graffitisuite.demo\rSUMMARY:Static Event\rCOLOR:#FFFFFF\rX-RSCV-TEXTCOLOR:#000000\rX-RSCV-BORDERCOLOR:#000000\rDTSTAMP:20180326T103501Z\rDTSTART:20180326T103501Z\rDTEND:20180326T000000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180328T100001Z@com.graffitisuite.demo\rSUMMARY:Meeting\rCOLOR:#F044AA\rX-RSCV-TEXTCOLOR:#0FBB55\rX-RSCV-BORDERCOLOR:#700DDE\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180328T100001Z\rDTSTART:20180328T100001Z\rRRULE:FREQ\x3DWEEKLY;INTERVAL\x3D1;UNTIL\x3D21000326T110001Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180326T080001Z@com.graffitisuite.demo\rSUMMARY:Daily Briefing\rCOLOR:#9501E7\rX-RSCV-TEXTCOLOR:#6AFE18\rX-RSCV-BORDERCOLOR:#50B204\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180326T080001Z\rDTSTART:20180326T080001Z\rRRULE:FREQ\x3DDAILY;INTERVAL\x3D1;UNTIL\x3D20180330T090001Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180326T130001Z@com.graffitisuite.demo\rSUMMARY:Long Titled Event Item Which Should Wrap.\rOh\x2C and there\'s an EOL in there.\rActually\x2C a couple of them.\rCOLOR:#A650C1\rX-RSCV-TEXTCOLOR:#59AF3E\rX-RSCV-BORDERCOLOR:#9BE8C8\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180326T130001Z\rDTSTART:20180326T130001Z\rDTEND:20180328T140001Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180330T000001Z@com.graffitisuite.demo\rSUMMARY:Multi All-Day\rCOLOR:#7C254A\rX-RSCV-TEXTCOLOR:#FFFFFF\rX-RSCV-BORDERCOLOR:#FEA8B7\rDTSTAMP:20180330T000001Z\rDTSTART:20180330T000001Z\rDTEND:20180401T000000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180329T050000Z@com.graffitisuite.demo\rSUMMARY:New Event I\rDESCRIPTION:Description I\rCOLOR:#3A87AD\rX-RSCV-TEXTCOLOR:#FFFFFF\rX-RSCV-BORDERCOLOR:#3A87AD\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180329T050000Z\rDTSTART:20180329T050000Z\rDTEND:20180329T053000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180326T103501Z@com.graffitisuite.demo\rSUMMARY:Static Event\rCOLOR:#0FFFFF\rX-RSCV-TEXTCOLOR:#000000\rX-RSCV-BORDERCOLOR:#000000\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180326T103501Z\rDTSTART:20180326T103501Z\rDTEND:20180326T000000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180328T100001Z@com.graffitisuite.demo\rSUMMARY:Meeting\rCOLOR:#0F044A\rX-RSCV-TEXTCOLOR:#00FBB5\rX-RSCV-BORDERCOLOR:#0700DD\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180328T100001Z\rDTSTART:20180328T100001Z\rRRULE:FREQ\x3DWEEKLY;INTERVAL\x3D1;UNTIL\x3D21000326T110001Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180326T080001Z@com.graffitisuite.demo\rSUMMARY:Daily Briefing\rCOLOR:#09501E\rX-RSCV-TEXTCOLOR:#06AFE1\rX-RSCV-BORDERCOLOR:#050B20\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180326T080001Z\rDTSTART:20180326T080001Z\rRRULE:FREQ\x3DDAILY;INTERVAL\x3D1;UNTIL\x3D20180330T090001Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180326T130001Z@com.graffitisuite.demo\rSUMMARY:Long Titled Event Item Which Should Wrap.\rCOLOR:#0A650C\rX-RSCV-TEXTCOLOR:#059AF3\rX-RSCV-BORDERCOLOR:#09BE8C\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180326T130001Z\rDTSTART:20180326T130001Z\rDTEND:20180328T000000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180330T000001Z@com.graffitisuite.demo\rSUMMARY:Multi All-Day\rCOLOR:#07C254\rX-RSCV-TEXTCOLOR:#0FFFFF\rX-RSCV-BORDERCOLOR:#0FEA8B\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180330T000001Z\rDTSTART:20180330T000001Z\rDTEND:20180401T000000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180329T050000Z@com.graffitisuite.demo\rSUMMARY:New Event I\rDESCRIPTION:Description I\rCOLOR:#03A87A\rX-RSCV-TEXTCOLOR:#0FFFFF\rX-RSCV-BORDERCOLOR:#03A87A\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180329T050000Z\rDTSTART:20180329T050000Z\rDTEND:20180329T053000Z\rEND:VEVENT\rBEGIN:VEVENT\rUID:20180226T000000Z@com.graffitisuite.demo\rSUMMARY:New Event II\rDESCRIPTION:DescriptionII\rCOLOR:#F71121\rX-RSCV-TEXTCOLOR:#FFFFFF\rX-RSCV-BORDERCOLOR:#3A87AD\rX-RSCV-CANMOVE:True\rX-RSCV-CANRESIZE:True\rDTSTAMP:20180226T000000Z\rDTSTART:20180226T000000Z\rDTEND:20180227T124900Z\rEND:VEVENT\rEND:VCALENDAR\r", Scope = Private
	#tag EndConstant


#tag EndWindowCode

#tag Events btnICS
	#tag Event
		Sub Action()
		  gwcMain.RemoveAllEvents()
		  gwcMain.ICSImport( sampleICS )
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnGoogle
	#tag Event
		Sub Action()
		  gwcMain.AddExternalSource( "en.usa#holiday@group.v.calendar.google.com", "AIzaSyBiCAW32QvKauzihk9goGnQcXVPYmaE6ys" )
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnRem
	#tag Event
		Sub Action()
		  gwcMain.RemoveAllEvents()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnRestrict
	#tag Event
		Sub Action()
		  if gwcMain.RestrictView then
		    gwcMain.RemoveAllRestrictions
		  else
		    dim dMorningStart as new Date
		    dMorningStart.Hour = 8
		    dMorningStart.Minute = 0
		    
		    dim dMorningEnd as new Date
		    dMorningEnd.Hour = 12
		    dMorningEnd.Minute = 0
		    
		    dim dAfternoonStart as new date
		    dAfternoonStart.Hour = 13
		    dAfternoonStart.Minute = 0
		    
		    dim dAfternoonEnd as new date
		    dAfternoonEnd.Hour = 17
		    dAfternoonEnd.Minute = 0
		    
		    dim resMorning as new GraffitiWebCalendarRestriction( "Morning", dMorningStart, dMorningEnd, 1, 2, 3, 4, 5 )
		    dim resAfternoon as new GraffitiWebCalendarRestriction( "Afternoon", dAfternoonStart, dAfternoonEnd, 1, 2, 3, 4, 5 )
		    
		    gwcMain.AddRestriction( resMorning )
		    gwcMain.AddRestriction( resAfternoon )
		  end if
		  
		  gwcMain.RestrictView = not gwcMain.RestrictView
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnScrollTime
	#tag Event
		Sub Action()
		  dim r as new Random
		  dim d as new Date
		  d.Hour = r.InRange( 0, 23 )
		  d.Minute = r.InRange( 0, 59 )
		  d.Second = r.InRange( 0, 59 )
		  gwcMain.ScrollTime( d )
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnTimeRestrict
	#tag Event
		Sub Action()
		  if gwcMain.RestrictView then
		    gwcMain.TimeMinimum = nil
		    gwcMain.TimeMaximum = nil
		  else
		    dim dStart as new Date
		    dStart.Hour = 6
		    dStart.Minute = 0
		    
		    dim dEnd as new Date
		    dEnd.Hour = 18
		    dEnd.Minute = 59
		    
		    gwcMain.TimeMinimum = dStart
		    gwcMain.TimeMaximum = dEnd
		  end if
		  gwcMain.RestrictView = not gwcMain.RestrictView
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkHidePast
	#tag Event
		Sub ValueChanged()
		  gwcMain.HidePastEvents = me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkTheme
	#tag Event
		Sub ValueChanged()
		  gwcMain.LoadTheme( "//code.jquery.com/ui/1.11.4/themes/blitzer/jquery-ui.css" )
		  gwcMain.CustomTheme = me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events gwcMain
	#tag Event
		Sub DateSelected(DateStart as Date, DateEnd as Date, StartTime as Boolean, EndTime as Boolean, mouseX as Integer, mouseY as Integer)
		  'dim d as new dlgCal
		  'd.ccEditEvent.myCaller = me
		  'd.Show
		  '
		  'Session.LogMessage( me.ControlID, "DateSelected", DateStart.ShortDate + _
		  '" " + _
		  'DateStart.ShortTime + _
		  'if( not IsNull( DateEnd ), " thru " + DateEnd.ShortDate + " " + DateEnd.ShortTime, "" ) )
		  'd.ccEditEvent.AddEvent( me, DateStart, DateEnd )
		End Sub
	#tag EndEvent
	#tag Event
		Sub EventMouseEnter(theEvent as GraffitiWebCalendarEvent, X as Integer, Y as Integer)
		  'Session.LogMessage( me.ControlID, "EventMouseEnter", theEvent.Title + "(" + Str( X, "#" ) + "," + Str( Y, "#" ) + ")" )
		End Sub
	#tag EndEvent
	#tag Event
		Sub EventMouseLeave(theEvent as GraffitiWebCalendarEvent)
		  'Session.LogMessage( me.ControlID, "EventMouseLeave", theEvent.Title )
		End Sub
	#tag EndEvent
	#tag Event
		Sub EventSelected(theEvent as GraffitiWebCalendarEvent)
		  'Session.LogMessage( me.ControlID, "EventSelected", theEvent.Title )
		  'dim d as new dlgCal
		  'd.Show
		  '
		  'd.ccEditEvent.EditEvent( me, theEvent )
		End Sub
	#tag EndEvent
	#tag Event
		Sub GoogleCalendarError(Domain as String, Reason as String, Message as String)
		  'Session.LogMessage( me.Name + " [" + me.ControlID + "]", "GoogleCalendarError", chr(34) + Domain + chr(34) + "," + chr(34) + Reason + chr(34) + "," + chr(34) + Message + chr(34) )
		End Sub
	#tag EndEvent
	#tag Event
		Sub LanguagesLoaded()
		  'Session.LogMessage( me.ControlID, "LanguagesLoaded", Str( gwcMain.Languages.Ubound + 1, "#" ) + " languages" )
		  '
		  'pmLang.DeleteAllRows()
		  '
		  'dim intMax as Integer = gwcMain.Languages.Ubound
		  'for intCycle as Integer = 0 to intMax
		  'pmLang.AddRow( gwcMain.Languages(intCycle) )
		  'next
		  '
		  'pmLang.ListIndex = 0
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  dim currEvent as new GraffitiWebCalendarEvent( "Static Event", new Date )
		  currEvent.CanMove = False
		  currEvent.CanResize = False
		  currEvent.BackgroundColor = rgb( 255, 255, 255 )
		  currEvent.BorderColor = rgb( 0, 0, 0 )
		  currEvent.TextColor = rgb( 0, 0, 0 )
		  me.AddEvent( currEvent )
		  
		  dim cBG as Color = RandomColor
		  dim dStartStatic as new Date
		  dStartStatic.Hour = 0
		  dStartStatic.Minute = 0
		  dStartStatic.Second = 0
		  dim dEndStatic as new Date( dStartStatic )
		  dEndStatic.Day = dEndStatic.Day + 1
		  dim staticEvent as new GraffitiWebCalendarEvent( "Static Event II", dStartStatic, dEndStatic )
		  staticEvent.AllDay = True
		  me.AddEvent( staticEvent )
		  
		  cBG = RandomColor
		  dim dStart as new Date
		  dim dEnd as new Date
		  dStart.Hour = 10
		  dStart.Minute = 0
		  dEnd.Year = 2100
		  dEnd.Hour = 11
		  dEnd.Minute = 0
		  me.AddEvent( new GraffitiWebCalendarEvent( "Meeting", dStart, dEnd, GraffitiWebCalendarEvent.RepeatWeekly, 1, cBG, RandomColor, InvertColor( cBG ) ) )
		  
		  cBG = RandomColor
		  dim dStart2 as new date
		  dim dEnd2 as new Date
		  dStart2.Hour = 8
		  dStart2.Minute = 0
		  dEnd2.Hour = 9
		  dEnd2.Minute = 0
		  dEnd2.Day = dEnd2.Day + 4
		  me.AddEvent( new GraffitiWebCalendarEvent( "Daily Briefing", dStart2, dEnd2, GraffitiWebCalendarEvent.RepeatDaily, 1, cBG, RandomColor, InvertColor( cBG ) ) )
		  
		  cBG = RandomColor
		  dim dStart3 as new Date
		  dim dEnd3 as new Date
		  dStart3.Hour = 13
		  dStart3.Minute = 0
		  dEnd3.Hour = 14
		  dEnd3.Minute = 0
		  dStart.Day = dStart.Day + 2
		  dEnd3.Day = dEnd3.Day + 2
		  me.AddEvent( new GraffitiWebCalendarEvent( "Long Titled Event Item Which Should Wrap." + EndOfLine + "Oh, and there's an EOL in there." + EndOfLine + "Actually, a couple of them.", dStart3, dEnd3, GraffitiWebCalendarEvent.RepeatNever, 1, cBG, RandomColor, InvertColor( cBG ) ) )
		  
		  Dim dStart4 As New Date
		  Dim dEnd4 As New Date
		  dStart4.Hour = 0
		  dStart4.Minute = 0
		  dEnd4.Hour = 23
		  dEnd4.Minute = 0
		  dStart4.Day = dStart.Day + 2
		  dEnd4.Day = dEnd3.Day + 4
		  Dim cEvent As New GraffitiWebCalendarEvent( "Multi All-Day", dStart4, dEnd4 )
		  cEvent.BackgroundColor = RandomColor
		  cEvent.BorderColor = RandomColor
		  cEvent.CanMove = False
		  cEvent.CanResize = False
		  cEvent.AllDay = True
		  me.AddEvent(cEvent)
		End Sub
	#tag EndEvent
	#tag Event
		Sub ViewChanged(NewView as GraffitiWebCalendar.Views, StartDate as Date, EndDate as Date)
		  dim viewIndex as Integer = CType( NewView, Integer )
		  'Session.LogMessage( me.ControlID, "ViewChanged", Str( viewIndex ) )
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmLang
	#tag Event
		Sub SelectionChanged()
		  gwcMain.Language = me.Text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmView
	#tag Event
		Sub SelectionChanged()
		  gwcMain.CalendarView = CType( me.ListIndex, GraffitiWebCalendar.Views )
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
