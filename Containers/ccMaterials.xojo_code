#tag WebPage
Begin WebContainer ccMaterials
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   620
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle6
      Cursor          =   0
      Enabled         =   True
      Height          =   461
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   150
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle Rectangle7
      Cursor          =   0
      Enabled         =   True
      Height          =   139
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel2
      Caption         =   "<div class= 'materialTitles'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Λίστα Υλικών</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "1591676927"
      TabOrder        =   -1
      Top             =   150
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   132
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel3
      Caption         =   "<div class= 'materialLabels'>Κωδικός</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   65
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   196
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel4
      Caption         =   "<div class= 'materialLabels'>Όνομα</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   178
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   196
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel5
      Caption         =   "<div class= 'materialTitles'><b>&nbsp;&nbsp;&nbsp;&nbsp;Στοιχεία Υλικών</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "1591676927"
      TabOrder        =   -1
      Top             =   9
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   132
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel6
      Caption         =   "<div class= 'materialLabels'>Κωδικός</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   64
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel7
      Caption         =   "<div class= 'materialLabels'>Ονομασία</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   280
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   62
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel8
      Caption         =   "<div class= 'materialLabels'>Σημειώσεις</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   104
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel9
      Caption         =   "<div class= 'materialLabels'>Τιμή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   643
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   62
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesCat1
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   219
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesCat2
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   256
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivOk
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   904
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   226648063
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivCancel
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   941
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   25726975
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   127
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   59
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField2
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   367
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   57
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   230
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField3
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   127
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   99
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   848
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField4
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   732
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   3
      Text            =   ""
      TextAlign       =   0
      Top             =   57
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesCat3
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   293
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccEpimEnties ccEpimEntiesCat4
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   330
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel10
      Caption         =   "<div class= 'materialLabels'>Τιμή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   629
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   196
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   83
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  ccEpimEntiesCat1.setUp("1    &emsp;  &emsp;   P001  &emsp; &emsp;  &emsp;  &emsp; ΥΑΛΟΠΛΕΓΜΑΤΑ   &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp;  #.###,##", 1)
		  ccEpimEntiesCat2.setUp("2    &emsp;  &emsp;   P015  &emsp;  &emsp; &emsp;  &emsp; ΑΣΦΑΛΤΟΠΑΝΑ     &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;   #.###,##", 1)
		  ccEpimEntiesCat3.setUp("3    &emsp;  &emsp;   P031  &emsp; &emsp;  &emsp;  &emsp; ΤΟΥΒΛΑ  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&emsp;   &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp;  #.###,##", 1)
		  ccEpimEntiesCat4.setUp("4    &emsp;  &emsp;   P041  &emsp;  &emsp; &emsp;  &emsp; ΤΣΙΜΕΝΤΟΛΙΘΟΙ     &nbsp;  &nbsp; &nbsp;&nbsp;&nbsp;&emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  &emsp; &emsp;  #.###,##", 1)
		  
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		parentPage As wpMain
	#tag EndProperty


#tag EndWindowCode

#tag Events wivOk
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Ok")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivCancel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Cancel")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TextField4
	#tag Event
		Sub LostFocus()
		  me.text = format(val(me.text),"#,###.00")
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  'if (me.text.Len > 0) Then
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
