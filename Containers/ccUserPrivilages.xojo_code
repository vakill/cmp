#tag WebPage
Begin WebContainer ccUserPrivilages
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   620
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle7
      Cursor          =   0
      Enabled         =   True
      Height          =   487
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   121
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel11
      Caption         =   "<div class= 'users3Labels'><b>&nbsp;&nbsp;&nbsp;Προσβάσεις Χρήστη:</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "1591676927"
      TabOrder        =   -1
      Top             =   20
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   165
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel12
      Caption         =   "<div class= 'user3Labels'>Περιγραφή Οθόνης</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   65
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   100
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel5
      Caption         =   "<div class= 'user3Labels'>Μεταβολή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   785
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   90
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel14
      Caption         =   "<div class= 'user3Labels'>Εισαγωγή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   683
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   89
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel6
      Caption         =   "<div class= 'user3Labels'>Διαγραφή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   887
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   89
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivCancel
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   941
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   25726975
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivOk
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   904
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   226648063
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel15
      Caption         =   "<div class= 'user3Labels'>Geok</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   213
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   81
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel16
      Caption         =   "<div class= 'user3Labels'>Κατερίνα Γεωργιάδη</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   313
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel17
      Caption         =   "<div class= 'user3Labels'>Επιλογή Όλων:</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   467
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   59
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   128
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel7
      Caption         =   "<div class= 'user3Labels'>Προβολή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   581
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   89
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   90
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccUserPrivilegesEntry ccUserPrivilegesEntry1
      Cursor          =   0
      Enabled         =   True
      Height          =   37
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   129
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccUserPrivilegesEntry ccUserPrivilegesEntry2
      Cursor          =   0
      Enabled         =   True
      Height          =   37
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1072697343"
      TabOrder        =   0
      Top             =   168
      VerticalCenter  =   0
      Visible         =   True
      Width           =   940
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCheckbox gwcDelete
      Cursor          =   0
      Enabled         =   True
      HasLabel        =   False
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LabelFalse      =   ""
      LabelTrue       =   ""
      Left            =   905
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   24
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCheckbox gwcEdit
      Cursor          =   0
      Enabled         =   True
      HasLabel        =   False
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LabelFalse      =   ""
      LabelTrue       =   ""
      Left            =   807
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   24
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCheckbox gwcInsert
      Cursor          =   0
      Enabled         =   True
      HasLabel        =   False
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LabelFalse      =   ""
      LabelTrue       =   ""
      Left            =   706
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   24
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCheckbox gwcView
      Cursor          =   0
      Enabled         =   True
      HasLabel        =   False
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LabelFalse      =   ""
      LabelTrue       =   ""
      Left            =   602
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   24
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  ccUserPrivilegesEntry1.setUp("  &emsp; Επιμετρήσεις Έργων")
		  ccUserPrivilegesEntry2.setUp("  &emsp; Επιμετρήσεις Λογαριασμών")
		End Sub
	#tag EndEvent


#tag EndWindowCode

#tag Events wivCancel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  session.activePage.ccUsersTab.ccEpimEntiesGroup1.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesGroup2.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesGroup3.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesUser1.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesUser2.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesUser3.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel2.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel3.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel4.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel5.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel6.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel10.Visible = true
		  session.activePage.ccUsersTab.ccLabelButtonUsers.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel12.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel13.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel14.Visible = true
		  session.activePage.ccUsersTab.Rectangle6.Visible = true
		  session.activePage.ccUsersTab.Rectangle7.Visible = true
		  self.Close()
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivOk
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Ok")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
