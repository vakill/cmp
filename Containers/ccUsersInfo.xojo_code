#tag WebPage
Begin WebContainer ccUsersInfo
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   620
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "278206463"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1000
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle7
      Cursor          =   0
      Enabled         =   True
      Height          =   602
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "245030911"
      TabOrder        =   -1
      Top             =   9
      VerticalCenter  =   0
      Visible         =   True
      Width           =   981
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel5
      Caption         =   "<div class= 'usersLabels'><b>&nbsp;&nbsp;&nbsp;&nbsp;Πληροφορίες Χρήστη</b></div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "1591676927"
      TabOrder        =   -1
      Top             =   9
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   172
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel6
      Caption         =   "<div class= 'usersLabels'>Log In:</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   56
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel7
      Caption         =   "<div class= 'usersLabels'>Όνομα Χρήστη:</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   88
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel8
      Caption         =   "<div class= 'usersLabels'>Κωδικός:</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   136
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel9
      Caption         =   "<div class= 'usersLabels'>Κωδικός<br>(Επιβεβαίωση):</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   158
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivOk
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   904
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   226648063
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivCancel
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   941
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   25726975
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   20
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel10
      Caption         =   "<div class= 'usersLabels'>Μ.Μ.:</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   200
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel11
      Caption         =   "<div class= 'usersLabels'>Σχόλια:</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   30
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   232
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCheckbox GraffitiWebCheckbox1
      Cursor          =   0
      Enabled         =   True
      HasLabel        =   True
      Height          =   28
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LabelFalse      =   "Ανενεργός"
      LabelTrue       =   "Ενεργός"
      Left            =   787
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   50
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   129
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebCheckbox GraffitiWebCheckbox2
      Cursor          =   0
      Enabled         =   True
      HasLabel        =   True
      Height          =   28
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      LabelFalse      =   "Αλλαγή Κωδικού"
      LabelTrue       =   "Αλλαγή Κωδικού"
      Left            =   787
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   81
      Value           =   False
      VerticalCenter  =   0
      Visible         =   True
      Width           =   129
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebGrid GraffitiWebGrid1
      AutoEdit        =   False
      Cursor          =   0
      Editable        =   False
      Editing         =   False
      EnableColumnReorder=   True
      Enabled         =   True
      EnableRowReorder=   False
      ForceFitColumns =   True
      HeaderRowHeight =   25
      HeaderRowVisible=   True
      Height          =   218
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   40
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockUpdate      =   False
      LockVertical    =   False
      MultiSelectRows =   False
      RenderedBottomRow=   0
      RenderedTopRow  =   0
      RowHeight       =   25
      Scope           =   0
      SelectedCell    =   0
      SelectionType   =   "0"
      Style           =   "1072697343"
      TabOrder        =   1
      Top             =   382
      VerticalCenter  =   0
      Visible         =   True
      Width           =   437
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebGrid GraffitiWebGrid2
      AutoEdit        =   False
      Cursor          =   0
      Editable        =   False
      Editing         =   False
      EnableColumnReorder=   True
      Enabled         =   True
      EnableRowReorder=   False
      ForceFitColumns =   True
      HeaderRowHeight =   25
      HeaderRowVisible=   True
      Height          =   218
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   529
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockUpdate      =   False
      LockVertical    =   False
      MultiSelectRows =   False
      RenderedBottomRow=   0
      RenderedTopRow  =   0
      RowHeight       =   25
      Scope           =   0
      SelectedCell    =   0
      SelectionType   =   "0"
      Style           =   "1072697343"
      TabOrder        =   1
      Top             =   382
      VerticalCenter  =   0
      Visible         =   True
      Width           =   437
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel12
      Caption         =   "<div class= 'usersLabels'>Όνομα Ομάδας</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   99
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   360
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   127
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel13
      Caption         =   "<div class= 'usersLabels'>Περιγραφή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   231
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   360
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   127
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel14
      Caption         =   "<div class= 'usersLabels'>Περιγραφή</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   724
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   360
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   127
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel GraffitiWebLabel15
      Caption         =   "<div class= 'usersLabels'>Όνομα Ομάδας</div> "
      Cursor          =   0
      Enabled         =   True
      Height          =   23
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   592
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   360
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   127
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivMoveRight
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   485
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1992179711
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   409
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   35
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivMoveLeft
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   35
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   485
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   542883839
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   454
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   35
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField1
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   51
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   242
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea TextArea1
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   109
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   2
      ScrollPosition  =   0
      Style           =   "1072697343"
      TabOrder        =   3
      Text            =   ""
      TextAlign       =   0
      Top             =   227
      VerticalCenter  =   0
      Visible         =   True
      Width           =   489
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField2
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   83
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   489
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField3
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   195
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   489
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField4
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   131
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   242
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField TextField5
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   143
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   2
      Text            =   ""
      TextAlign       =   0
      Top             =   163
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   242
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  'ccLabelButtonPrivilages.setUp("<h6><b>&nbsp;&nbsp;&nbsp;&nbsp;Προσβάσεις <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Χρήστη</b></h6>")
		  'ccLabelButtonPrivilages.SetSize(131, 47)
		End Sub
	#tag EndEvent


#tag EndWindowCode

#tag Events wivOk
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Ok")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivCancel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  session.activePage.ccUsersTab.ccEpimEntiesGroup1.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesGroup2.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesGroup3.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesUser1.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesUser2.Visible = true
		  session.activePage.ccUsersTab.ccEpimEntiesUser3.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel2.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel3.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel4.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel5.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel6.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel10.Visible = true
		  session.activePage.ccUsersTab.ccLabelButtonUsers.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel12.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel13.Visible = true
		  session.activePage.ccUsersTab.GraffitiWebLabel14.Visible = true
		  session.activePage.ccUsersTab.Rectangle6.Visible = true
		  session.activePage.ccUsersTab.Rectangle7.Visible = true
		  self.Close()
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivMoveRight
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Move Entry")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivMoveLeft
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  MsgBox("Pressed Move Entry")
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
