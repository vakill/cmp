#tag WebStyle
WebStyle wsWhiteFullRoundedBordered
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid FFFFFFFF
		corner-topright=10px
		corner-bottomright=10px
		corner-bottomleft=10px
		corner-topleft=10px
		border-right=2px solid B6B6B6FF
		border-bottom=2px solid B6B6B6FF
		border-top=2px solid B6B6B6FF
		border-left=2px solid B6B6B6FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsWhiteFullRoundedBordered
#tag EndWebStyle

