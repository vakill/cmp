#tag WebStyle
WebStyle wsRoundedGreen
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid BBD35C81
		text-color=3A3A3AFF
		corner-bottomright=10px
		corner-topright=10px
		border-top=1px none B6B6B6FF
		border-left=1px none B6B6B6FF
		border-bottom=1px none B6B6B6FF
		border-right=1px none B6B6B6FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsRoundedGreen
#tag EndWebStyle

