#tag WebStyle
WebStyle wsRoundedGreenFull
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid BBD35C81
		corner-bottomleft=10px
		corner-bottomright=10px
		corner-topright=10px
		border-top=1px none B6B6B6FF
		border-left=1px none B6B6B6FF
		border-bottom=1px none B6B6B6FF
		border-right=1px none B6B6B6FF
		corner-topleft=10px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsRoundedGreenFull
#tag EndWebStyle

