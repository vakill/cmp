#tag WebStyle
WebStyle wsGreenBorderRounded
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid FFFFFFFF
		border-top=2px solid BBD35CFF
		border-left=2px solid BBD35CFF
		border-bottom=2px solid BBD35CFF
		border-right=2px solid BBD35CFF
		corner-topleft=25px
		corner-bottomleft=25px
		corner-bottomright=25px
		corner-topright=25px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsGreenBorderRounded
#tag EndWebStyle

