#tag WebStyle
WebStyle wsGreyFullRounded
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid D8D8D8FF
		corner-topright=30px
		corner-bottomright=30px
		corner-bottomleft=30px
		corner-topleft=30px
		border-right=1px none B6B6B6FF
		border-bottom=1px none B6B6B6FF
		border-left=1px none B6B6B6FF
		border-top=1px none B6B6B6FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsGreyFullRounded
#tag EndWebStyle

