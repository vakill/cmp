#tag WebStyle
WebStyle wsWhiteFullRounded
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid FFFFFFFF
		corner-topright=10px
		corner-bottomright=10px
		corner-bottomleft=10px
		corner-topleft=10px
		border-right=1px none B6B6B6FF
		border-bottom=1px none B6B6B6FF
		border-top=1px none B6B6B6FF
		border-left=1px none B6B6B6FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsWhiteFullRounded
#tag EndWebStyle

