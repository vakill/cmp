#tag WebStyle
WebStyle wsRoundedWhite
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid E5E5E5FF
		text-color=3A3A3AFF
		corner-bottomright=5px
		corner-topright=5px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsRoundedWhite
#tag EndWebStyle

