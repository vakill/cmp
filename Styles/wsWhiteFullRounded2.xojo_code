#tag WebStyle
WebStyle wsWhiteFullRounded2
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid FFFFFFFF
		corner-topright=50px
		corner-bottomright=50px
		corner-bottomleft=50px
		corner-topleft=50px
		border-right=1px none B6B6B6FF
		border-bottom=1px none B6B6B6FF
		border-top=1px none B6B6B6FF
		border-left=1px none B6B6B6FF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsWhiteFullRounded2
#tag EndWebStyle

