#tag WebStyle
WebStyle wsLightGeyFullRounded
Inherits WebStyle
	#tag WebStyleStateGroup
		misc-background=solid E8E8E8FF
		corner-topright=30px
		corner-bottomright=30px
		border-top=1px none B6B6B6FF
		border-left=1px none B6B6B6FF
		border-bottom=1px none B6B6B6FF
		border-right=1px none B6B6B6FF
		corner-bottomleft=30px
		corner-topleft=30px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle wsLightGeyFullRounded
#tag EndWebStyle

