#tag WebPage
Begin WebPage wpMain
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   800
   HelpTag         =   ""
   HorizontalCenter=   0
   ImplicitInstance=   True
   Index           =   -2147483648
   IsImplicitInstance=   False
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   800
   MinWidth        =   1280
   Style           =   "None"
   TabOrder        =   0
   Title           =   "CMP"
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1280
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _ImplicitInstance=   False
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle wrcBG
      Cursor          =   0
      Enabled         =   True
      Height          =   800
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "856176639"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1280
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField tfSearch
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   28
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   865
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   2
      Style           =   "1072697343"
      TabOrder        =   18
      Text            =   ""
      TextAlign       =   0
      Top             =   136
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   167
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle wrcTop
      Cursor          =   0
      Enabled         =   True
      Height          =   125
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1739372543"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1280
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle wrcSide
      Cursor          =   0
      Enabled         =   True
      Height          =   665
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1739372543"
      TabOrder        =   -1
      Top             =   135
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivCompanyLogo
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   125
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   804990975
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   0
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   400
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle wrcMain
      Cursor          =   0
      Enabled         =   True
      Height          =   630
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   172
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1005
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivTitle
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   120
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   708
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   163956735
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   1
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   302
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccPrCalendar ccPrCalendarTab
      Cursor          =   0
      Enabled         =   True
      Height          =   620
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   6
      Top             =   175
      VerticalCenter  =   0
      Visible         =   False
      Width           =   1004
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccContractors ccContractorsTab
      Cursor          =   0
      Enabled         =   True
      Height          =   620
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   7
      Top             =   175
      VerticalCenter  =   0
      Visible         =   False
      Width           =   1004
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccHome ccHomeTab
      Cursor          =   0
      Enabled         =   True
      Height          =   620
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   11
      Top             =   175
      VerticalCenter  =   0
      Visible         =   False
      Width           =   1004
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonHome
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   177
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonErga
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   227
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonCalendar
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   277
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonYperer
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   327
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonItems
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   377
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonUsers
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   527
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivSearch
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   828
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   9727999
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivAdd
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1037
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   756965375
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivEdit
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1069
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   439126015
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivDelete
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1101
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1290446847
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivPrint
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1166
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1426419711
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView wivExcel
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1200
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   438691839
      ProtectImage    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlCSS
      Caption         =   "<style> \r\n.right\r\n{\r\ntext-align: right;\r\n}\r\n.itemEntry\r\n{\r\nfont-size: 97%;\r\ncolor = #3A3A3AFF\r\nfont-weight: bold;\r\n}\r\n.materialLabels\r\n{\r\ntext-align: right;\r\n}\r\n.ItemLabels\r\n{\r\ntext-align: right;\r\n}\r\n.constructorAddLabels\r\n{\r\ntext-align: right;\r\n}\r\n.estimatesCard\r\n{\r\ntext-align: right;\r\n}\r\n.userTools\r\n{\r\nfont-weight: bold;\r\n}\r\n.center\r\n{\r\ntext-align: center;\r\n}\r\n.MenuUButton \r\n{ \r\ncolor: White;\r\ntext-align: left;  \r\n}\r\n.MenuPButton \r\n{\r\ncolor: Black;\r\ntext-align: left; \r\n}\r\n.tabTitle\r\n{\r\ncolor: Black;\r\ntext-align: left; \r\nfont-weight: bold;\r\n}\r\n.tabDate\r\n{\r\ncolor: Black;\r\ntext-align: left; \r\nfont-weight: bold;\r\n}\r\n.tabTitleWhite\r\n{\r\ncolor: White;\r\ntext-align: left; \r\nfont-weight: bold;\r\n}\r\n.tabDateWhite\r\n{\r\ncolor: White;\r\ntext-align: left; \r\nfont-weight: bold;\r\n}\r\n.HomeTabTitle \r\n{\r\ncolor: Grey;\r\nfont-weight: bold;\r\ntext-align: left;	\r\n}\r\n.ProjectButton\r\n{\r\ntext-align: center;\r\n}\r\n.EpimColumns\r\n{\r\ntext-align: center;\r\n}\r\n.extraButton\r\n{\r\nfont-size: 80%;\r\n}\r\n.entry\r\n( \r\nfont-size: 90%;\r\n}\r\n.user\r\n{\r\ncolor: white;\r\nfont-weight: bold;\r\n}\r\n</style> "
      Cursor          =   0
      Enabled         =   True
      Height          =   36
      HelpTag         =   "Used for the CSS code of the project!"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   577
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   812
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlTabTitle
      Caption         =   ""
      Cursor          =   0
      Enabled         =   True
      Height          =   28
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   292
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   137
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   False
      Width           =   226
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GraffitiWebLabel gwlTabDate
      Caption         =   ""
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   530
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      MaxHeight       =   200
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   141
      TrueHeight      =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   266
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ccProjectList ccProjectListTab
      Cursor          =   0
      Enabled         =   True
      Height          =   620
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   13
      Top             =   175
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1004
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccUsers ccUsersTab
      Cursor          =   0
      Enabled         =   True
      Height          =   620
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   17
      Top             =   175
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1000
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccUserButton ccUserButtonArea
      Cursor          =   0
      Enabled         =   True
      Height          =   31
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1107
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   14
      Top             =   82
      VerticalCenter  =   0
      Visible         =   True
      Width           =   108
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccUserTool ccUserToolArea
      Cursor          =   0
      Enabled         =   True
      Height          =   116
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1074
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "1015267327"
      TabOrder        =   15
      Top             =   118
      VerticalCenter  =   0
      Visible         =   False
      Width           =   159
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccUserProfile ccUserProfileWindow
      Cursor          =   0
      Enabled         =   True
      Height          =   645
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   276
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   16
      Top             =   141
      VerticalCenter  =   0
      Visible         =   False
      Width           =   813
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonMaterials
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   427
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin ccMenuButton ccMenuButtonOffers
      active          =   False
      Cursor          =   0
      Enabled         =   True
      Height          =   38
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      pvLabel         =   ""
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "0"
      TabOrder        =   12
      Top             =   477
      VerticalCenter  =   0
      Visible         =   True
      Width           =   250
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  'if(ccUserToolArea.Visible and( X< ccUserToolArea.Left or X> ccUserToolArea.Left+ccUserToolArea.Width or Y < ccUserToolArea.Top or Y> ccUserToolArea.Top+ ccUserToolArea.Height) ) Then ccUserToolArea.VIsible = False
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  dim d As new Date
		  gwlTabDate.Caption = "<h6 class = 'tabDate'> Ημερομηνία: "+ d.ShortDate + "</h6>"
		  ccMenuButtonHome.setUp("Αρχική", rsz_home2)
		  ccMenuButtonErga.setUp("Έργα", rsz_building2)
		  ccMenuButtonCalendar.setUp("Ημερολόγιο Έργων", rsz_calendar2)
		  ccMenuButtonYperer.setUp("Υπερεργολάβοι", rsz_workersmale2)
		  'ccMenuButtonCategories.setUp("Κατηγορίες", rsz_menu2)
		  ccMenuButtonItems.setUp("Είδη", rsz_hammer2)
		  ccMenuButtonMaterials.setUp("Υλικά", rsz_materials1)
		  ccMenuButtonOffers.setUp("Προσφορές", rsz_offer)
		  ccMenuButtonUsers.setUp("Προσβάσεις", rsz_group2)
		  ccUserToolArea.parentPage = self
		  SetState("Αρχική")
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub add()
		  select case pvState 
		  case "Αρχική"
		    'Call ccHomeTab add method
		  case "Υπερεργολάβοι"
		    'Call ccContractorsTab add method
		  case "Είδη"
		    ccItemsTab.addItem()
		  case "Ημερολόγιο"
		    'Call ccPrCalendarTab add method
		  case "Διαχείριση Έργων"
		    'Call ccProjectListTab add method
		  case "Υλικά"
		    'Call ccMaterialsTab add method
		  case "Προσφορές"
		    '??
		  case "Προσβάσεις"
		    'Call ccUsersTab add method
		  end select
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub delete()
		  select case pvState 
		  case "Αρχική"
		    ' Call ccHomeTab delete method
		  case "Υπερεργολάβοι"
		    
		  case "Είδη"
		    ccItemsTab.deleteItem()
		  case "Ημερολόγιο"
		    'Call ccPrCalendarTab delete method
		  case "Διαχείριση Έργων"
		    'Call ccProjectListTab delete method
		  case "Υλικά"
		    'Call ccMaterialsTab delete method
		  case "Προσφορές"
		    '??
		  case "Προσβάσεις"
		    'Call ccUsersTab delete method
		  end select
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub edit()
		  select case pvState 
		  case "Αρχική"
		    'Call ccHomeTab  edit method
		  case "Υπερεργολάβοι"
		    'Call ccContractorsTab  edit method
		  case "Είδη"
		    ccItemsTab.editItem()
		  case "Ημερολόγιο"
		    'Call ccPrCalendarTab  edit method
		  case "Διαχείριση Έργων"
		    'Call ccProjectListTab  edit method
		  case "Υλικά"
		    'Call ccMaterialsTab  edit method
		  case "Προσφορές"
		    '??
		  case "Προσβάσεις"
		    'Call ccUsersTab  edit method
		  end select
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub excelPrint()
		  select case pvState 
		  case "Αρχική"
		    '??
		  case "Υπερεργολάβοι"
		    'Call ccContractorsTab print method
		  case "Είδη"
		    '??
		  case "Ημερολόγιο"
		    'Call ccPrCalendarTab print method
		  case "Διαχείριση Έργων"
		    'Different Prints depending on the active window
		  case "Υλικά"
		    'Call ccMaterialsTab print method
		  case "Προσφορές"
		    '??
		  case "Προσβάσεις"
		    '??
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub hideProfile()
		  ccUserProfileWindow.Visible = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub print()
		  select case pvState 
		  case "Αρχική"
		    '??
		  case "Υπερεργολάβοι"
		    'Call ccContractorsTab excelPrint method
		  case "Είδη"
		    '??
		  case "Ημερολόγιο"
		    'Call ccPrCalendarTab excelPrint method
		  case "Διαχείριση Έργων"
		    'Different Prints depending on the active window
		  case "Υλικά"
		    'Call ccMaterialsTab excelPrint method
		  case "Προσφορές"
		    '??
		  case "Προσβάσεις"
		    '??
		  end select
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub search()
		  select case pvState 
		  case "Αρχική"
		    'Call ccHomeTab  edit method
		  case "Υπερεργολάβοι"
		    'Call ccContractorsTab  edit method
		  case "Είδη"
		    ccItemsTab.searchItem()
		  case "Ημερολόγιο"
		    'Call ccPrCalendarTab  edit method
		  case "Διαχείριση Έργων"
		    'Call ccProjectListTab  edit method
		  case "Υλικά"
		    'Call ccMaterialsTab  edit method
		  case "Προσφορές"
		    '??
		  case "Προσβάσεις"
		    'Call ccUsersTab  edit method
		  end select
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub setState(tab As String)
		  setTitleText(tab)
		  pvState = tab
		  select case tab 
		  case "Αρχική"
		    toggleColors(true, tab)
		    ccMenuButtonHome.toggleRec(true)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = True
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		  case "Υπερεργολάβοι"
		    toggleColors(true, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(true)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = True
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		  case "Είδη"
		    toggleColors(false, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(true)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		    ccItemsTab = new ccItems
		    ccItemsTab.parentPage = self
		    ccItemsTab.ZIndex = 10
		    ccItemsTab.EmbedWithin(self, 276, 175, 1004, 620)
		    activeContainer = ccItemsTab
		    ccItemsTab.setState("Initial")
		    
		  case "Ημερολόγιο"
		    toggleColors(true, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(true)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = True
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		  case "Διαχείριση Έργων"
		    toggleColors(true, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(true)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = True
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		  case "Υλικά"
		    toggleColors(false, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(true)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		    ccMaterialsTab = new ccMaterials
		    ccMaterialsTab.parentPage = self
		    ccMaterialsTab.ZIndex = 10
		    ccMaterialsTab.EmbedWithin(self, 276, 175, 1004, 620)
		    activeContainer = ccMaterialsTab
		  case "Προσφορές"
		    toggleColors(false, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(false)
		    ccMenuButtonOffers.toggleRec(true)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = False
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		  case "Προσβάσεις"
		    toggleColors(false, tab)
		    ccMenuButtonHome.toggleRec(false)
		    ccMenuButtonYperer.toggleRec(false)
		    ccMenuButtonItems.toggleRec(false)
		    ccMenuButtonCalendar.toggleRec(false)
		    ccMenuButtonErga.toggleRec(false)
		    ccMenuButtonMaterials.toggleRec(false)
		    ccMenuButtonUsers.toggleRec(true)
		    ccMenuButtonOffers.toggleRec(false)
		    ccHomeTab.Visible = False
		    ccContractorsTab.Visible = False
		    ccPrCalendarTab.Visible = False
		    ccProjectListTab.Visible = False
		    ccUsersTab.Visible = True
		    if not(activeContainer = nil) then
		      activeContainer.Close
		    end if
		  end select
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub setTitleText(titleText As String)
		  if Not(titleText = "Αρχική") Then
		    Dim d As new Date
		    gwlTabTitle.Caption = "<h5 class = 'tabTitle'> "+ titleText + "</h5>"
		    gwlTabTitle.Visible = True
		  else
		    gwlTabTitle.Caption = ""
		    gwlTabTitle.Visible = False
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub toggleColors(green As Boolean, tab As String)
		  dim d As new Date
		  if(green) then
		    wrcBG.Style = wsGreen
		    gwlTabTitle.Caption = "<h5 class = 'tabTitle'>"+tab+"</h5>"
		    gwlTabDate.Caption = "<h6 class = 'tabDate'> Ημερομηνία: "+ d.ShortDate + "</h6>"
		    wivSearch.Picture = rsz_search2
		    wivAdd.Picture = rsz_plus2
		    wivDelete.Picture = rsz_trash2
		    wivEdit.Picture = rsz_edit21
		    wivExcel.Picture = rsz_excelfile2
		    wivPrint.Picture = rsz_printingtool2
		  else
		    wrcBG.Style = wsRed
		    gwlTabTitle.Caption = "<h5 class = 'tabTitleWhite'> "+tab+" </h5>"
		    gwlTabDate.Caption = "<h6 class = 'tabDateWhite'> Ημερομηνία: "+ d.ShortDate + "</h6>"
		    wivSearch.Picture = searchwhite20x20
		    wivAdd.Picture = pluswhite20x20
		    wivDelete.Picture = trashwhite20x20
		    wivEdit.Picture = editwhite20x20
		    wivExcel.Picture = excelwhite20x20
		    wivPrint.Picture = printingwhite20x20
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		activeContainer As WebContainer
	#tag EndProperty

	#tag Property, Flags = &h0
		ccItemsTab As ccItems
	#tag EndProperty

	#tag Property, Flags = &h0
		ccMaterialsTab As ccMaterials
	#tag EndProperty

	#tag Property, Flags = &h0
		pvState As String
	#tag EndProperty


#tag EndWindowCode

#tag Events ccMenuButtonHome
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Αρχική")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonErga
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Διαχείριση Έργων")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonCalendar
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Ημερολόγιο")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonYperer
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  setState("Υπερεργολάβοι")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonItems
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Είδη")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonUsers
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Προσβάσεις")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivSearch
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  search()
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivAdd
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  add()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivEdit
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  edit()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivDelete
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  delete()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivPrint
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  print
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events wivExcel
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  excelPrint
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Cursor = System.WebCursors.FingerPointer
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccUserButtonArea
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If(ccUserToolArea.Visible) then
		    ccUserToolArea.Visible = False
		  else
		    ccUserToolArea.Visible = True
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonMaterials
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Υλικά")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ccMenuButtonOffers
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  SetState("Προσφορές")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		InitialValue="-2147483648 "
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IsImplicitInstance"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ImplicitInstance"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="pvState"
		Group="Behavior"
		Type="String"
	#tag EndViewProperty
#tag EndViewBehavior
